/*
 *  tslib/tests/ts_check_linear.c
 *
 *  Copyright (C) 2010 Xianwei Zeng
 *
 * This file is placed under the GPL.  Please see the file
 * COPYING for more details.
 *
 * Basic test program for check the touch screen linear property
 */
#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <linux/kd.h>
#include <linux/vt.h>
#include <linux/fb.h>

#include "tslib.h"

#include "fbutils.h"
#include "testutils.h"

#define XRES           1024
#define YRES           768
#define SZ_STEP        32
#define START_X        16
#define START_Y        16

#define NR_POINTS_X    (XRES / SZ_STEP)
#define NR_POINTS_Y    (YRES / SZ_STEP)

static struct sample_data {
	int x;
	int y;
} sample_point[NR_POINTS_Y][NR_POINTS_X];

static int palette [] =
{
	0x000000, 0xffe080, 0xffffff, 0xe0c0a0
};
#define NR_COLORS (sizeof (palette) / sizeof (palette [0]))

static void sig(int sig)
{
	close_framebuffer ();
	fflush (stderr);
	printf ("signal %d caught\n", sig);
	fflush (stdout);
	exit (1);
}

static void get_sample (struct tsdev *ts, int x, int y, int *sx, int *sy)
{
	put_cross(x, y, 2 | XORMODE);
	getxy (ts, sx, sy);
	put_cross(x, y, 2 | XORMODE);

	printf("== X = %4d Y = %4d --- %4d %4d\n", x, y, *sx, *sy);
}

static int clearbuf(struct tsdev *ts)
{
	int fd = ts_fd(ts);
	fd_set fdset;
	struct timeval tv;
	int nfds;
	struct ts_sample sample;

	while (1) {
		FD_ZERO(&fdset);
		FD_SET(fd, &fdset);

		tv.tv_sec = 0;
		tv.tv_usec = 0;

		nfds = select(fd + 1, &fdset, NULL, NULL, &tv);
		if (nfds == 0) break;

		if (ts_read_raw(ts, &sample, 1) < 0) {
			perror("ts_read");
			exit(1);
		}
	}

	return 0;
}

int main()
{
	struct tsdev *ts;
	char *tsdevice = NULL;
	unsigned int i, j;
	unsigned int x, y;

	signal(SIGSEGV, sig);
	signal(SIGINT, sig);
	signal(SIGTERM, sig);

	if( (tsdevice = getenv("TSLIB_TSDEVICE")) != NULL ) {
		ts = ts_open(tsdevice,0);
	} else {
		if (!(ts = ts_open("/dev/input/event0", 0)))
			ts = ts_open("/dev/touchscreen/ucb1x00", 0);
	}

	if (!ts) {
		perror("ts_open");
		exit(1);
	}
	if (ts_config(ts)) {
		perror("ts_config");
		exit(1);
	}

	if (open_framebuffer()) {
		close_framebuffer();
		exit(1);
	}

	for (i = 0; i < NR_COLORS; i++)
		setcolor (i, palette [i]);

	put_string_center (xres / 2, yres / 4,
			   "TSLIB check linear utility", 1);
	put_string_center (xres / 2, yres / 4 + 20,
			   "Touch crosshair to check", 2);

	printf("xres = %d, yres = %d\n", xres, yres);

	y = START_Y;
	x = START_X;
	for (y = START_Y; y < yres - SZ_STEP; y += SZ_STEP) {
		for (x = START_X; x < xres - SZ_STEP; x += SZ_STEP) {
			int sx, sy;
			clearbuf(ts);
			get_sample (ts, x, y, &sx, &sy );
		}
	}

	// Clear the buffer
	clearbuf(ts);

	close_framebuffer();
	return i;
}
