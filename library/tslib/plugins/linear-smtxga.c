/*
 *  tslib/plugins/linear.c
 *
 *  Copyright (C) 2001 Russell King.
 *  Copyright (C) 2005 Alberto Mardegan <mardy@sourceforge.net>
 *
 * This file is placed under the LGPL.  Please see the file
 * COPYING for more details.
 *
 * $Id: linear.c,v 1.10 2005/02/26 01:47:23 kergoth Exp $
 *
 * Linearly scale touchscreen values
 */
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <stdio.h>

#include "config.h"
#include "tslib-private.h"
#include "tslib-filter.h"

/* #define DEBUG */

struct tslib_linear_smtxga {
	struct tslib_module_info module;

	int     xres;
	int     yres;
	int     min_x;
	int     max_x;
	int     min_y;
	int     max_y;
	
// Linear scaling and offset parameters for x,y (can include rotation)
	int	a[7];

// Screen resolution at the time when calibration was performed
	unsigned int cal_res_x;
	unsigned int cal_res_y;
};

#define ABS(x)         (((x) > 0) ? (x) : (-(x)))

static int xfactor[16] = {105, 100, 100,  98,  95,  94,  94,  95, 95, 95, 96, 97, 100, 102, 103, 105}; 
static int yfactor[16] = {100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 102, 102}; 
#define XIDX(x)      (((ABS(x - lin->min_x)) / 130) % 16)
#define YIDX(y)      (((ABS(y - lin->min_y)) / 120) % 16)

static int
linear_smtxga_read(struct tslib_module_info *info, struct ts_sample *samp, int nr)
{
	struct tslib_linear_smtxga *lin = (struct tslib_linear_smtxga *)info;
	int ret;
	int xtemp,ytemp;

	ret = info->next->ops->read(info->next, samp, nr);
	if (ret >= 0) {
		int nr;

		int xdelta = (lin->max_x - lin->min_x) * 100;
		int ydelta = (lin->max_y - lin->min_y) * 100;
		for (nr = 0; nr < ret; nr++, samp++) {
#ifdef DEBUG
			fprintf(stderr,"BEFORE CALIB--------------------> %d %d %d\n",samp->x, samp->y, samp->pressure);
#endif /*DEBUG*/
			xtemp = samp->x; ytemp = samp->y;
#if 0
			samp->x = 	( lin->a[2] +
					lin->a[0]*xtemp + 
					lin->a[1]*ytemp ) / lin->a[6];
			samp->y =	( lin->a[5] +
					lin->a[3]*xtemp +
					lin->a[4]*ytemp ) / lin->a[6];
			if (info->dev->res_x && lin->cal_res_x)
				samp->x = samp->x * info->dev->res_x / lin->cal_res_x;
			if (info->dev->res_y && lin->cal_res_y)
				samp->y = samp->y * info->dev->res_y / lin->cal_res_y;
#else
			samp->x = (ABS(xtemp - lin->min_x) * lin->xres * xfactor[XIDX(xtemp)]) / xdelta;
			samp->y = (ABS(ytemp - lin->min_y) * lin->yres * yfactor[YIDX(ytemp)]) / xdelta;
			/* samp->x = (ABS(xtemp - lin->min_x)) * lin->xres  / xdelta; */
			/* samp->y = (ABS(ytemp - lin->min_y)) * lin->yres / ydelta; */
#ifdef DEBUG
			fprintf(stderr,"xidx %d yidx %d\n", XIDX(xtemp),  YIDX(ytemp));
#endif /*DEBUG*/
#endif
		}
	}

	return ret;
}

static int linear_smtxga_fini(struct tslib_module_info *info)
{
	free(info);
	return 0;
}

static const struct tslib_ops linear_smtxga_ops =
{
	.read	= linear_smtxga_read,
	.fini	= linear_smtxga_fini,
};

TSAPI struct tslib_module_info *linear_smtxga_mod_init(struct tsdev *dev, const char *params)
{

	struct tslib_linear_smtxga *lin;
	struct stat sbuf;
	FILE *pcal_fd;
	int index;
	char *calfile;

	lin = malloc(sizeof(struct tslib_linear_smtxga));
	if (lin == NULL)
		return NULL;

	lin->module.ops = &linear_smtxga_ops;

// Use default values that leave ts numbers unchanged after transform
	lin->a[0] = 1;
	lin->a[1] = 0;
	lin->a[2] = 0;
	lin->a[3] = 0;
	lin->a[4] = 1;
	lin->a[5] = 0;
	lin->a[6] = 1;
	lin->min_x = 600;
	lin->max_x = 2800;
	lin->min_y = 1050;
	lin->max_y = 3200;
	lin->xres  = 1024;
	lin->yres  = 768;

	/*
	 * Check calibration file
	 */
	if( (calfile = getenv("TSLIB_CALIBFILE")) == NULL) calfile = TS_POINTERCAL;
	if (stat(calfile, &sbuf)==0) {
		pcal_fd = fopen(calfile, "r");
		for (index = 0; index < 7; index++)
			if (fscanf(pcal_fd, "%d", &lin->a[index]) != 1) break;
		fscanf(pcal_fd, "%d %d", &lin->cal_res_x, &lin->cal_res_y);
#ifdef DEBUG
		printf("Linear calibration constants: ");
		for(index=0;index<7;index++) printf("%d ",lin->a[index]);
		printf("\n");
#endif /*DEBUG*/
		fclose(pcal_fd);
	}
		
	return &lin->module;
}

#ifndef TSLIB_STATIC_LINEAR_SMTXGA_MODULE
	TSLIB_MODULE_INIT(linear_smtxga_mod_init);
#endif
