/*
   (c) Copyright 2000-2002  convergence integrated media GmbH.
   All rights reserved.

   Written by Denis Oliver Kropp <dok@directfb.org>,
              Andreas Hundt <andi@fischlustig.de> and
              Sven Neumann <neo@directfb.org>.
              
   This file is subject to the terms and conditions of the MIT License:

   Permission is hereby granted, free of charge, to any person
   obtaining a copy of this software and associated documentation
   files (the "Software"), to deal in the Software without restriction,
   including without limitation the rights to use, copy, modify, merge,
   publish, distribute, sublicense, and/or sell copies of the Software,
   and to permit persons to whom the Software is furnished to do so,
   subject to the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
   CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <directfb.h>

static IDirectFB              *dfb;
static IDirectFBDisplayLayer  *layer;

static IDirectFBVideoProvider *videoprovider;
static IDirectFBWindow        *videowindow;
static IDirectFBSurface       *videosurface;

static IDirectFBEventBuffer   *keybuffer;

int err;

/* macro for a safe call to DirectFB functions */
#define DFBCHECK(x...) \
     {                                                                \
          err = x;                                                    \
          if (err != DFB_OK) {                                        \
               fprintf( stderr, "%s <%d>:\n\t", __FILE__, __LINE__ ); \
               DirectFBErrorFatal( #x, err );                         \
          }                                                           \
     }


int main( int argc, char *argv[] )
{
     DFBDisplayLayerConfig  layer_config;
     DFBInputDeviceKeyState quit = DIKS_UP;
     DFBInputEvent evt;

     DFBCHECK(DirectFBInit( &argc, &argv ));

     if (argc < 2) {
          fprintf(stderr, "%s: you must specify a video source\n", argv[0]);
          return 1;
     }

     DFBCHECK(DirectFBSetOption("bg-none", NULL));
     DFBCHECK(DirectFBCreate( &dfb ));

     DFBCHECK(dfb->CreateInputEventBuffer(dfb, DICAPS_KEYS,
                                          DFB_FALSE, &keybuffer));

     DFBCHECK(dfb->GetDisplayLayer( dfb, DLID_PRIMARY, &layer ));

     {
          DFBSurfaceDescription sdsc;
          DFBWindowDescription desc;

          DFBCHECK(dfb->CreateVideoProvider( dfb, argv[1], &videoprovider ));
          videoprovider->GetSurfaceDescription( videoprovider, &sdsc );

          desc.flags = DWDESC_POSX | DWDESC_POSY | DWDESC_WIDTH | DWDESC_HEIGHT;
          desc.posx = 0;
          desc.posy = 0;
          desc.width = sdsc.width;
          desc.height = sdsc.height;

          DFBCHECK(layer->CreateWindow( layer, &desc, &videowindow ) );
          DFBCHECK(videowindow->GetSurface( videowindow, &videosurface ) );
	  /* Set window opaque */
          videowindow->SetOpacity( videowindow, 0xFF );

          DFBCHECK(videoprovider->PlayTo( videoprovider, videosurface,
                                          NULL, NULL, NULL ));
	  /* Loop playback mode */
          DFBCHECK(videoprovider->SetPlaybackFlags( videoprovider,
						    DVPLAY_LOOPING));
     }

     layer->GetConfiguration( layer, &layer_config );

     while (1) {

          keybuffer->WaitForEvent(keybuffer);

          while (keybuffer->GetEvent(keybuffer, DFB_EVENT(&evt)) == DFB_OK) {
	       if (evt.type == DIET_KEYPRESS) {
                    switch (DFB_LOWER_CASE (evt.key_symbol)) {
                         case DIKS_ESCAPE:
                         case DIKS_EXIT:
                         case DIKS_END:
                         case '0':
                              return EXIT_SUCCESS;
                         default:
                              ;
		    }
	       }
	  }
     }

     videoprovider->Release( videoprovider );
     layer->Release( layer );
     dfb->Release( dfb );

     return EXIT_SUCCESS;
}
