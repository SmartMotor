#---------------------------------------------------------------------------
# Copyright (C) 2009 FreedomXura
#
# This file is part of the Input Method for Qt 4.5.1,
# Which can be used in arm , X86 and others.
#
# This file may be distributed and/or modified under the terms of the
# GNU General Public License version 3 as published by the Free Software
# Foundation
#
# This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
# WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
#
#
# Author:      FreedomXura
# email:       freedomxura@gmail.com,mxh20999@163.com
# bolg:        http://xurafreedom.cublog.cn/
# Create Date: May , 2009
# License:     GPL version 3 or later
#
# Welcome to comunicate with me ,if you found there are bugs or you have
# better suggestion,please send email to me
#---------------------------------------------------------------------------
QT += sql
TARGET = inputmethod
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp
HEADERS += mainwindow.h
FORMS += mainwindow.ui
OTHER_FILES += zhpy_table.db
