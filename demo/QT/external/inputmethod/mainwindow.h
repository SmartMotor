/**********************************************************************
** Copyright (C) 2009 FreedomXura
**
** This file is part of the Input Method for Qt 4.5.1,
** Which can be used in arm , X86 and others.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 3 as published by the Free Software
** Foundation 
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** 
** Author:      FreedomXura
** email:       freedomxura@gmail.com,mxh20999@163.com
** bolg:        http://xurafreedom.cublog.cn/
** Create Date: May , 2009
** License:     GPL version 3 or later
** 
** Welcome to comunicate with me ,if you found there are bugs or you have
** better suggestion,please send email to me
**********************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include <QtGui/QMessageBox>
#include <QString>
#include <QtSql>
#include <QList>

class QWidget;
class QPalette;
class QPixmap;
class QIcon;
class QToolButton;

namespace Ui
{
    class MainWindowClass;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QWidget *widget;
    QPalette palette;
    QPixmap pixmap;
    QIcon *icon;
    QToolButton *button;
public slots:   //槽函数声明
    //*************************************************************
    //字母消息
    //*************************************************************
    void accept_A();
    void accept_B();
    void accept_C();
    void accept_D();
    void accept_E();
    void accept_F();
    void accept_G();
    void accept_H();
    void accept_I();
    void accept_J();
    void accept_K();
    void accept_L();
    void accept_M();
    void accept_N();
    void accept_O();
    void accept_P();
    void accept_Q();
    void accept_R();
    void accept_S();
    void accept_T();
    void accept_U();
    void accept_V();
    void accept_W();
    void accept_X();
    void accept_Y();
    void accept_Z();

    //*************************************************************
    //删除，确定，上翻，下翻
    //*************************************************************
    void accept_del();
    void accept_enter();
    void accept_up();
    void accept_down();
    void accept_reset();

    //*************************************************************
    //汉字选择
    //*************************************************************
    void accept_select_1();
    void accept_select_2();
    void accept_select_3();
    void accept_select_4();
    void accept_select_5();
    void accept_select_6();
    void accept_select_7();
    void accept_select_8();
    void accept_select_9();
    void accept_select_10();

    //*************************************************************
    //符号选择
    //*************************************************************
    void accept_select_symbol_1();
    void accept_select_symbol_2();
    void accept_select_symbol_3();
    void accept_select_symbol_4();

    //*************************************************************
    //数据库查询
    //*************************************************************
    int zh_click();

private:
    Ui::MainWindowClass *ui;
};

#endif // MAINWINDOW_H
