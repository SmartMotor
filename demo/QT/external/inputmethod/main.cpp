/**********************************************************************
** Copyright (C) 2009 FreedomXura
**
** This file is part of the Input Method for Qt 4.5.1,
** Which can be used in arm , X86 and others.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 3 as published by the Free Software
** Foundation 
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** 
** Author:      FreedomXura
** email:       freedomxura@gmail.com, mxh20999@163.com
** bolg:        http://xurafreedom.cublog.cn/
** Create Date: May , 2009
** License:     GPL version 3 or later
** 
** Welcome to comunicate with me ,if you found there are bugs or you have
** better suggestion,please send email to me
**********************************************************************/

#include <QtGui/QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    // button->setIcon(QIcon("1.ico"));
    w.show();
    return a.exec();
}
