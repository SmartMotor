/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Sat Apr 24 11:11:02 2010
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QListView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QScrollArea>
#include <QtGui/QStatusBar>
#include <QtGui/QTextEdit>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindowClass
{
public:
    QWidget *centralWidget;
    QListView *listView;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QPushButton *pushButton_A;
    QPushButton *pushButton_B;
    QPushButton *pushButton_C;
    QPushButton *pushButton_D;
    QPushButton *pushButton_F;
    QPushButton *pushButton_E;
    QPushButton *pushButton_H;
    QPushButton *pushButton_G;
    QPushButton *pushButton_I;
    QPushButton *pushButton_O;
    QPushButton *pushButton_P;
    QPushButton *pushButton_J;
    QPushButton *pushButton_L;
    QPushButton *pushButton_K;
    QPushButton *pushButton_N;
    QPushButton *pushButton_M;
    QPushButton *pushButton_R;
    QPushButton *pushButton_Q;
    QPushButton *pushButton_S;
    QPushButton *pushButton_V;
    QPushButton *pushButton_U;
    QPushButton *pushButton_W;
    QPushButton *pushButton_Y;
    QPushButton *pushButton_X;
    QPushButton *pushButton_Z;
    QPushButton *pushButton_Symbol_2;
    QPushButton *pushButton_Symbol_1;
    QPushButton *pushButton_Symbol_3;
    QPushButton *pushButton_T;
    QPushButton *pushButton_Symbol_4;
    QTextEdit *textEdit;
    QPushButton *pushButton_Reset;
    QPushButton *pushButton_Enter;
    QPushButton *pushButton_Up;
    QPushButton *pushButton_Down;
    QScrollArea *scrollArea_2;
    QWidget *scrollAreaWidgetContents_3;
    QLineEdit *lineEdit;
    QPushButton *pushButton_Dis_1;
    QPushButton *pushButton_Dis_2;
    QPushButton *pushButton_Dis_3;
    QPushButton *pushButton_Dis_5;
    QPushButton *pushButton_Dis_6;
    QPushButton *pushButton_Dis_4;
    QPushButton *pushButton_Dis_10;
    QPushButton *pushButton_Dis_7;
    QPushButton *pushButton_Dis_8;
    QPushButton *pushButton_Dis_9;
    QLineEdit *lineEdit_2;
    QPushButton *pushButton_Del;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindowClass)
    {
        if (MainWindowClass->objectName().isEmpty())
            MainWindowClass->setObjectName(QString::fromUtf8("MainWindowClass"));
        MainWindowClass->resize(692, 437);
        centralWidget = new QWidget(MainWindowClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        listView = new QListView(centralWidget);
        listView->setObjectName(QString::fromUtf8("listView"));
        listView->setGeometry(QRect(0, 0, 761, 431));
        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setGeometry(QRect(10, 140, 551, 231));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 547, 227));
        pushButton_A = new QPushButton(scrollAreaWidgetContents);
        pushButton_A->setObjectName(QString::fromUtf8("pushButton_A"));
        pushButton_A->setGeometry(QRect(20, 20, 41, 28));
        pushButton_B = new QPushButton(scrollAreaWidgetContents);
        pushButton_B->setObjectName(QString::fromUtf8("pushButton_B"));
        pushButton_B->setGeometry(QRect(70, 20, 41, 28));
        pushButton_C = new QPushButton(scrollAreaWidgetContents);
        pushButton_C->setObjectName(QString::fromUtf8("pushButton_C"));
        pushButton_C->setGeometry(QRect(120, 20, 41, 28));
        pushButton_D = new QPushButton(scrollAreaWidgetContents);
        pushButton_D->setObjectName(QString::fromUtf8("pushButton_D"));
        pushButton_D->setGeometry(QRect(170, 20, 41, 28));
        pushButton_F = new QPushButton(scrollAreaWidgetContents);
        pushButton_F->setObjectName(QString::fromUtf8("pushButton_F"));
        pushButton_F->setGeometry(QRect(270, 20, 41, 28));
        pushButton_E = new QPushButton(scrollAreaWidgetContents);
        pushButton_E->setObjectName(QString::fromUtf8("pushButton_E"));
        pushButton_E->setGeometry(QRect(220, 20, 41, 28));
        pushButton_H = new QPushButton(scrollAreaWidgetContents);
        pushButton_H->setObjectName(QString::fromUtf8("pushButton_H"));
        pushButton_H->setGeometry(QRect(370, 20, 41, 28));
        pushButton_G = new QPushButton(scrollAreaWidgetContents);
        pushButton_G->setObjectName(QString::fromUtf8("pushButton_G"));
        pushButton_G->setGeometry(QRect(320, 20, 41, 28));
        pushButton_I = new QPushButton(scrollAreaWidgetContents);
        pushButton_I->setObjectName(QString::fromUtf8("pushButton_I"));
        pushButton_I->setGeometry(QRect(420, 20, 41, 28));
        pushButton_O = new QPushButton(scrollAreaWidgetContents);
        pushButton_O->setObjectName(QString::fromUtf8("pushButton_O"));
        pushButton_O->setGeometry(QRect(220, 70, 41, 28));
        pushButton_P = new QPushButton(scrollAreaWidgetContents);
        pushButton_P->setObjectName(QString::fromUtf8("pushButton_P"));
        pushButton_P->setGeometry(QRect(270, 70, 41, 28));
        pushButton_J = new QPushButton(scrollAreaWidgetContents);
        pushButton_J->setObjectName(QString::fromUtf8("pushButton_J"));
        pushButton_J->setGeometry(QRect(470, 20, 41, 28));
        pushButton_L = new QPushButton(scrollAreaWidgetContents);
        pushButton_L->setObjectName(QString::fromUtf8("pushButton_L"));
        pushButton_L->setGeometry(QRect(70, 70, 41, 28));
        pushButton_K = new QPushButton(scrollAreaWidgetContents);
        pushButton_K->setObjectName(QString::fromUtf8("pushButton_K"));
        pushButton_K->setGeometry(QRect(20, 70, 41, 28));
        pushButton_N = new QPushButton(scrollAreaWidgetContents);
        pushButton_N->setObjectName(QString::fromUtf8("pushButton_N"));
        pushButton_N->setGeometry(QRect(170, 70, 41, 28));
        pushButton_M = new QPushButton(scrollAreaWidgetContents);
        pushButton_M->setObjectName(QString::fromUtf8("pushButton_M"));
        pushButton_M->setGeometry(QRect(120, 70, 41, 28));
        pushButton_R = new QPushButton(scrollAreaWidgetContents);
        pushButton_R->setObjectName(QString::fromUtf8("pushButton_R"));
        pushButton_R->setGeometry(QRect(370, 70, 41, 28));
        pushButton_Q = new QPushButton(scrollAreaWidgetContents);
        pushButton_Q->setObjectName(QString::fromUtf8("pushButton_Q"));
        pushButton_Q->setGeometry(QRect(320, 70, 41, 28));
        pushButton_S = new QPushButton(scrollAreaWidgetContents);
        pushButton_S->setObjectName(QString::fromUtf8("pushButton_S"));
        pushButton_S->setGeometry(QRect(420, 70, 41, 28));
        pushButton_V = new QPushButton(scrollAreaWidgetContents);
        pushButton_V->setObjectName(QString::fromUtf8("pushButton_V"));
        pushButton_V->setGeometry(QRect(70, 120, 41, 28));
        pushButton_U = new QPushButton(scrollAreaWidgetContents);
        pushButton_U->setObjectName(QString::fromUtf8("pushButton_U"));
        pushButton_U->setGeometry(QRect(20, 120, 41, 28));
        pushButton_W = new QPushButton(scrollAreaWidgetContents);
        pushButton_W->setObjectName(QString::fromUtf8("pushButton_W"));
        pushButton_W->setGeometry(QRect(120, 120, 41, 28));
        pushButton_Y = new QPushButton(scrollAreaWidgetContents);
        pushButton_Y->setObjectName(QString::fromUtf8("pushButton_Y"));
        pushButton_Y->setGeometry(QRect(220, 120, 41, 28));
        pushButton_X = new QPushButton(scrollAreaWidgetContents);
        pushButton_X->setObjectName(QString::fromUtf8("pushButton_X"));
        pushButton_X->setGeometry(QRect(170, 120, 41, 28));
        pushButton_Z = new QPushButton(scrollAreaWidgetContents);
        pushButton_Z->setObjectName(QString::fromUtf8("pushButton_Z"));
        pushButton_Z->setGeometry(QRect(270, 120, 41, 28));
        pushButton_Symbol_2 = new QPushButton(scrollAreaWidgetContents);
        pushButton_Symbol_2->setObjectName(QString::fromUtf8("pushButton_Symbol_2"));
        pushButton_Symbol_2->setGeometry(QRect(370, 120, 41, 28));
        pushButton_Symbol_1 = new QPushButton(scrollAreaWidgetContents);
        pushButton_Symbol_1->setObjectName(QString::fromUtf8("pushButton_Symbol_1"));
        pushButton_Symbol_1->setGeometry(QRect(320, 120, 41, 28));
        pushButton_Symbol_3 = new QPushButton(scrollAreaWidgetContents);
        pushButton_Symbol_3->setObjectName(QString::fromUtf8("pushButton_Symbol_3"));
        pushButton_Symbol_3->setGeometry(QRect(420, 120, 41, 28));
        pushButton_T = new QPushButton(scrollAreaWidgetContents);
        pushButton_T->setObjectName(QString::fromUtf8("pushButton_T"));
        pushButton_T->setGeometry(QRect(470, 70, 41, 28));
        pushButton_Symbol_4 = new QPushButton(scrollAreaWidgetContents);
        pushButton_Symbol_4->setObjectName(QString::fromUtf8("pushButton_Symbol_4"));
        pushButton_Symbol_4->setGeometry(QRect(470, 120, 41, 28));
        textEdit = new QTextEdit(scrollAreaWidgetContents);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setGeometry(QRect(20, 160, 491, 51));
        scrollArea->setWidget(scrollAreaWidgetContents);
        pushButton_Reset = new QPushButton(centralWidget);
        pushButton_Reset->setObjectName(QString::fromUtf8("pushButton_Reset"));
        pushButton_Reset->setGeometry(QRect(600, 162, 71, 28));
        pushButton_Enter = new QPushButton(centralWidget);
        pushButton_Enter->setObjectName(QString::fromUtf8("pushButton_Enter"));
        pushButton_Enter->setGeometry(QRect(600, 212, 71, 28));
        pushButton_Up = new QPushButton(centralWidget);
        pushButton_Up->setObjectName(QString::fromUtf8("pushButton_Up"));
        pushButton_Up->setGeometry(QRect(600, 27, 71, 28));
        pushButton_Down = new QPushButton(centralWidget);
        pushButton_Down->setObjectName(QString::fromUtf8("pushButton_Down"));
        pushButton_Down->setGeometry(QRect(600, 64, 71, 28));
        scrollArea_2 = new QScrollArea(centralWidget);
        scrollArea_2->setObjectName(QString::fromUtf8("scrollArea_2"));
        scrollArea_2->setGeometry(QRect(30, 20, 511, 81));
        scrollArea_2->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 507, 77));
        lineEdit = new QLineEdit(scrollAreaWidgetContents_3);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(6, 10, 241, 31));
        pushButton_Dis_1 = new QPushButton(scrollAreaWidgetContents_3);
        pushButton_Dis_1->setObjectName(QString::fromUtf8("pushButton_Dis_1"));
        pushButton_Dis_1->setGeometry(QRect(6, 43, 41, 28));
        pushButton_Dis_2 = new QPushButton(scrollAreaWidgetContents_3);
        pushButton_Dis_2->setObjectName(QString::fromUtf8("pushButton_Dis_2"));
        pushButton_Dis_2->setGeometry(QRect(56, 43, 41, 28));
        pushButton_Dis_3 = new QPushButton(scrollAreaWidgetContents_3);
        pushButton_Dis_3->setObjectName(QString::fromUtf8("pushButton_Dis_3"));
        pushButton_Dis_3->setGeometry(QRect(106, 43, 41, 28));
        pushButton_Dis_5 = new QPushButton(scrollAreaWidgetContents_3);
        pushButton_Dis_5->setObjectName(QString::fromUtf8("pushButton_Dis_5"));
        pushButton_Dis_5->setGeometry(QRect(206, 43, 41, 28));
        pushButton_Dis_6 = new QPushButton(scrollAreaWidgetContents_3);
        pushButton_Dis_6->setObjectName(QString::fromUtf8("pushButton_Dis_6"));
        pushButton_Dis_6->setGeometry(QRect(256, 43, 41, 28));
        pushButton_Dis_4 = new QPushButton(scrollAreaWidgetContents_3);
        pushButton_Dis_4->setObjectName(QString::fromUtf8("pushButton_Dis_4"));
        pushButton_Dis_4->setGeometry(QRect(156, 43, 41, 28));
        pushButton_Dis_10 = new QPushButton(scrollAreaWidgetContents_3);
        pushButton_Dis_10->setObjectName(QString::fromUtf8("pushButton_Dis_10"));
        pushButton_Dis_10->setGeometry(QRect(456, 43, 41, 28));
        pushButton_Dis_7 = new QPushButton(scrollAreaWidgetContents_3);
        pushButton_Dis_7->setObjectName(QString::fromUtf8("pushButton_Dis_7"));
        pushButton_Dis_7->setGeometry(QRect(306, 43, 41, 28));
        pushButton_Dis_8 = new QPushButton(scrollAreaWidgetContents_3);
        pushButton_Dis_8->setObjectName(QString::fromUtf8("pushButton_Dis_8"));
        pushButton_Dis_8->setGeometry(QRect(356, 43, 41, 28));
        pushButton_Dis_9 = new QPushButton(scrollAreaWidgetContents_3);
        pushButton_Dis_9->setObjectName(QString::fromUtf8("pushButton_Dis_9"));
        pushButton_Dis_9->setGeometry(QRect(406, 43, 41, 28));
        lineEdit_2 = new QLineEdit(scrollAreaWidgetContents_3);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(256, 10, 241, 31));
        scrollArea_2->setWidget(scrollAreaWidgetContents_3);
        pushButton_Del = new QPushButton(centralWidget);
        pushButton_Del->setObjectName(QString::fromUtf8("pushButton_Del"));
        pushButton_Del->setGeometry(QRect(600, 312, 71, 28));
        MainWindowClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindowClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 692, 25));
        MainWindowClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindowClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindowClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindowClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindowClass->setStatusBar(statusBar);

        retranslateUi(MainWindowClass);

        QMetaObject::connectSlotsByName(MainWindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindowClass)
    {
        MainWindowClass->setWindowTitle(QApplication::translate("MainWindowClass", "MainWindow", 0, QApplication::UnicodeUTF8));
        pushButton_A->setText(QApplication::translate("MainWindowClass", "A", 0, QApplication::UnicodeUTF8));
        pushButton_B->setText(QApplication::translate("MainWindowClass", "B", 0, QApplication::UnicodeUTF8));
        pushButton_C->setText(QApplication::translate("MainWindowClass", "C", 0, QApplication::UnicodeUTF8));
        pushButton_D->setText(QApplication::translate("MainWindowClass", "D", 0, QApplication::UnicodeUTF8));
        pushButton_F->setText(QApplication::translate("MainWindowClass", "F", 0, QApplication::UnicodeUTF8));
        pushButton_E->setText(QApplication::translate("MainWindowClass", "E", 0, QApplication::UnicodeUTF8));
        pushButton_H->setText(QApplication::translate("MainWindowClass", "H", 0, QApplication::UnicodeUTF8));
        pushButton_G->setText(QApplication::translate("MainWindowClass", "G", 0, QApplication::UnicodeUTF8));
        pushButton_I->setText(QApplication::translate("MainWindowClass", "I", 0, QApplication::UnicodeUTF8));
        pushButton_O->setText(QApplication::translate("MainWindowClass", "O", 0, QApplication::UnicodeUTF8));
        pushButton_P->setText(QApplication::translate("MainWindowClass", "P", 0, QApplication::UnicodeUTF8));
        pushButton_J->setText(QApplication::translate("MainWindowClass", "J", 0, QApplication::UnicodeUTF8));
        pushButton_L->setText(QApplication::translate("MainWindowClass", "L", 0, QApplication::UnicodeUTF8));
        pushButton_K->setText(QApplication::translate("MainWindowClass", "K", 0, QApplication::UnicodeUTF8));
        pushButton_N->setText(QApplication::translate("MainWindowClass", "N", 0, QApplication::UnicodeUTF8));
        pushButton_M->setText(QApplication::translate("MainWindowClass", "M", 0, QApplication::UnicodeUTF8));
        pushButton_R->setText(QApplication::translate("MainWindowClass", "R", 0, QApplication::UnicodeUTF8));
        pushButton_Q->setText(QApplication::translate("MainWindowClass", "Q", 0, QApplication::UnicodeUTF8));
        pushButton_S->setText(QApplication::translate("MainWindowClass", "S", 0, QApplication::UnicodeUTF8));
        pushButton_V->setText(QApplication::translate("MainWindowClass", "V", 0, QApplication::UnicodeUTF8));
        pushButton_U->setText(QApplication::translate("MainWindowClass", "U", 0, QApplication::UnicodeUTF8));
        pushButton_W->setText(QApplication::translate("MainWindowClass", "W", 0, QApplication::UnicodeUTF8));
        pushButton_Y->setText(QApplication::translate("MainWindowClass", "Y", 0, QApplication::UnicodeUTF8));
        pushButton_X->setText(QApplication::translate("MainWindowClass", "X", 0, QApplication::UnicodeUTF8));
        pushButton_Z->setText(QApplication::translate("MainWindowClass", "Z", 0, QApplication::UnicodeUTF8));
        pushButton_Symbol_2->setText(QApplication::translate("MainWindowClass", ".", 0, QApplication::UnicodeUTF8));
        pushButton_Symbol_1->setText(QApplication::translate("MainWindowClass", ",", 0, QApplication::UnicodeUTF8));
        pushButton_Symbol_3->setText(QApplication::translate("MainWindowClass", "?", 0, QApplication::UnicodeUTF8));
        pushButton_T->setText(QApplication::translate("MainWindowClass", "T", 0, QApplication::UnicodeUTF8));
        pushButton_Symbol_4->setText(QApplication::translate("MainWindowClass", "!", 0, QApplication::UnicodeUTF8));
        pushButton_Reset->setText(QApplication::translate("MainWindowClass", "Reset", 0, QApplication::UnicodeUTF8));
        pushButton_Enter->setText(QApplication::translate("MainWindowClass", "Enter", 0, QApplication::UnicodeUTF8));
        pushButton_Up->setText(QApplication::translate("MainWindowClass", "Up", 0, QApplication::UnicodeUTF8));
        pushButton_Down->setText(QApplication::translate("MainWindowClass", "Down", 0, QApplication::UnicodeUTF8));
        pushButton_Dis_1->setText(QString());
        pushButton_Dis_2->setText(QString());
        pushButton_Dis_3->setText(QString());
        pushButton_Dis_5->setText(QString());
        pushButton_Dis_6->setText(QString());
        pushButton_Dis_4->setText(QString());
        pushButton_Dis_10->setText(QString());
        pushButton_Dis_7->setText(QString());
        pushButton_Dis_8->setText(QString());
        pushButton_Dis_9->setText(QString());
        pushButton_Del->setText(QApplication::translate("MainWindowClass", "Del", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindowClass: public Ui_MainWindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
