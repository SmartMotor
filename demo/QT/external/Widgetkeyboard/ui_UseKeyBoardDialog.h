/********************************************************************************
** Form generated from reading UI file 'UseKeyBoardDialog.ui'
**
** Created: Sun Apr 18 22:18:24 2010
**      by: Qt User Interface Compiler version 4.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USEKEYBOARDDIALOG_H
#define UI_USEKEYBOARDDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTextEdit>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UseKeyBoardDialog
{
public:
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit_3;
    QTextEdit *textEdit;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_3;
    QPushButton *btnShow;
    QPushButton *btnHide;

    void setupUi(QWidget *UseKeyBoardDialog)
    {
        if (UseKeyBoardDialog->objectName().isEmpty())
            UseKeyBoardDialog->setObjectName(QString::fromUtf8("UseKeyBoardDialog"));
        UseKeyBoardDialog->resize(417, 269);
        verticalLayout_4 = new QVBoxLayout(UseKeyBoardDialog);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        lineEdit_2 = new QLineEdit(UseKeyBoardDialog);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));

        verticalLayout_2->addWidget(lineEdit_2);

        lineEdit_3 = new QLineEdit(UseKeyBoardDialog);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));

        verticalLayout_2->addWidget(lineEdit_3);

        textEdit = new QTextEdit(UseKeyBoardDialog);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));

        verticalLayout_2->addWidget(textEdit);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);


        verticalLayout->addLayout(verticalLayout_2);


        verticalLayout_4->addLayout(verticalLayout);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        btnShow = new QPushButton(UseKeyBoardDialog);
        btnShow->setObjectName(QString::fromUtf8("btnShow"));

        verticalLayout_3->addWidget(btnShow);

        btnHide = new QPushButton(UseKeyBoardDialog);
        btnHide->setObjectName(QString::fromUtf8("btnHide"));

        verticalLayout_3->addWidget(btnHide);


        horizontalLayout->addLayout(verticalLayout_3);


        verticalLayout_4->addLayout(horizontalLayout);


        retranslateUi(UseKeyBoardDialog);

        QMetaObject::connectSlotsByName(UseKeyBoardDialog);
    } // setupUi

    void retranslateUi(QWidget *UseKeyBoardDialog)
    {
        UseKeyBoardDialog->setWindowTitle(QApplication::translate("UseKeyBoardDialog", "Using Virtual Keyboard", 0, QApplication::UnicodeUTF8));
        btnShow->setText(QApplication::translate("UseKeyBoardDialog", "Open VK", 0, QApplication::UnicodeUTF8));
        btnHide->setText(QApplication::translate("UseKeyBoardDialog", "Hide VK", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class UseKeyBoardDialog: public Ui_UseKeyBoardDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USEKEYBOARDDIALOG_H
