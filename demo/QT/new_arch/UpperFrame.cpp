#include <QVBoxLayout>
#include <QPainter>
#include <QLabel>
#include <QPushButton>
#include <iostream>

#include "UpperFrame.h"

UpperFrame::UpperFrame(void)
{
	QGridLayout *layout = new QGridLayout;

	QLabel *timeLabel = new QLabel(tr("time"));

	QLabel *programStepLabel = new QLabel(tr("程序:xx 步骤:xx"));
	programStepLabel->setObjectName("programStepLabel");

	/* paper pusher backgroud */
	QLabel *paperPusher = new QLabel;
	QPixmap *paperPusherPixmap = new QPixmap("images/推纸器图标.bmp");
	paperPusher -> setPixmap(*paperPusherPixmap);

	/* paper pusher text */
	QLabel *lengthLabel = new QLabel;
	lengthLabel -> setText("1550.00m");

	/* 2 buttons on the right */
	QLabel *cutter = new QLabel();
	QLabel *sand   = new QLabel();

	QPixmap *cutterPixmap = new QPixmap("images/1.bmp");
	QPixmap *sandPixmap = new QPixmap("images/2.bmp");

	cutter -> setPixmap(*cutterPixmap);
	sand -> setPixmap(*sandPixmap);

	layout -> addWidget(timeLabel, 0, 1, Qt::AlignHCenter);
	layout -> addWidget(programStepLabel, 2, 0, Qt::AlignHCenter);
	layout -> addWidget(paperPusher, 1, 1, 2, 1, Qt::AlignHCenter);
	layout -> addWidget(lengthLabel, 1, 1, 2, 1, Qt::AlignHCenter);
	layout -> addWidget(cutter, 1, 2, Qt::AlignHCenter);
	layout -> addWidget(sand, 2, 2, Qt::AlignHCenter);

	setLayout(layout);
	setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
}

void UpperFrame::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	QSize mySize = this->size();

	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.setPen(QPen(Qt::yellow, 2, Qt::SolidLine, Qt::RoundCap, Qt::MiterJoin));

	QLinearGradient gradient(mySize.width() / 2, mySize.height(), mySize.width() / 2, 0);
	gradient.setColorAt(0.0, Qt::yellow);
	gradient.setColorAt(1.0, Qt::white);

	painter.setBrush(QBrush(gradient));

	painter.drawPie(0, 0, mySize.width(), mySize.height() * 2, 0 * 16, 180 * 16);
}

QSize UpperFrame::sizeHint() const
{
	QSize size(1024, 128);
	return size;
}

