#ifndef LOWERCENTERFRAME_H
#define LOWERCENTERFRAME_H

class LowerCenterFrame : public QWidget
{
public:
	LowerCenterFrame(void);
	void paintEvent(QPaintEvent *event);
	QSize sizeHint() const;
private:
	QGridLayout *layout;
};

#endif
