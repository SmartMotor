#include <QVBoxLayout>
#include <QPainter>
#include <QLabel>
#include <QPushButton>
#include <QListView>
#include <QTableView>
#include <QStandardItemModel>
#include <QHeaderView>
#include <QScrollBar>
#include <QRect>

#include "LowerCenterFrame.h"
#include "ImageButton.h"

// class UpperRectLabel : public QWidget
// {
// public:
// 	UpperRectLabel(void);
// 	void paintEvent(QPaintEvent *);
// 	QSize sizeHint() const;
// };

// void UpperRectLabel:paintEvent(QPaintEvent *)
// {
// }

LowerCenterFrame::LowerCenterFrame(void)
{

	layout = new QGridLayout;
	layout->setRowStretch(0, 1);
	layout->setRowStretch(1, 1);
	layout->setRowStretch(2, 0.5);
	layout->setRowStretch(3, 0.5);
	layout->setRowStretch(4, 0.8);

	layout->setColumnStretch ( 0, 1);
	layout->setColumnStretch ( 1, 0.8);
	layout->setColumnStretch ( 2, 1);

	QLabel *pos_size = new QLabel(tr("位置尺寸"));
	pos_size->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	pos_size->setObjectName("pos_size");

	ImageButton *CButton = new ImageButton("images/C黄色-凸副本.bmp");
	ImageButton *equalButton = new ImageButton("images/黄色-=.bmp");

	QLabel *hintLabel = new QLabel(tr("提示栏"));
	hintLabel->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	hintLabel->setObjectName("hintLabel");

	QLabel *hint      = new QLabel(tr("手动"));
	hint->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	hint->setObjectName("hint");

	QLabel *status    = new QLabel(tr("状态栏"));
	status->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	status->setObjectName("status");

	QLabel *speaker = new QLabel;
	QPixmap *speakerPixmap = new QPixmap("images/喇叭-有声.bmp");
	speaker->setPixmap(*speakerPixmap);

	QLabel *infrared = new QLabel;
	QPixmap *infraredPixmap = new QPixmap("images/红外.bmp");
	infrared->setPixmap(*infraredPixmap);

	layout->addWidget(pos_size, 0, 0, 1, 1);

	layout->addWidget(CButton, 0, 3, 1, 1);
	layout->addWidget(equalButton, 1, 3, 1, 1);

	layout->addWidget(hintLabel, 2, 1, 1, 1, Qt::AlignHCenter);

	layout->addWidget(hint, 3, 1,  1, 1, Qt::AlignHCenter);

	layout->addWidget(status, 4, 0, Qt::AlignRight);

	layout->addWidget(speaker, 4, 1, Qt::AlignHCenter);

	layout->addWidget(infrared, 4, 2, Qt::AlignHCenter);

	setLayout(layout);

	setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
}

void LowerCenterFrame::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QSize mySize = this->size();

	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.setPen(QPen(Qt::yellow, 3, Qt::SolidLine, Qt::RoundCap, Qt::MiterJoin));

	/* get the 2nd row's position, and draw these shapes */
	QRect row_2_rect = layout->cellRect(2, 0);
	painter.drawLine(0, row_2_rect.top(), mySize.width(), row_2_rect.top());
	painter.drawLine(0, row_2_rect.bottom(), mySize.width(), row_2_rect.bottom());

	QRect rect(0, 0, mySize.width(), row_2_rect.bottom());
	painter.drawRoundedRect(rect, 10, 10);

	/* draw pie */
	int pieHeight = (mySize.height() - row_2_rect.bottom()) * 2;
	painter.drawPie(0, mySize.height() - pieHeight, mySize.width(), pieHeight, 180 * 16, 180 * 16);
}

QSize LowerCenterFrame::sizeHint() const
{
	QSize size(410, 290);
	return size;
}
