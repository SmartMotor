#ifndef IMAGEBUTTON_H
#define IMAGEBUTTON_H

#include <QPushButton>
#include <QSize>

/*
 * Button with image background.
 * Initialize with the image file name.
 * Button size will be set to image size.
 */
class ImageButton: public QPushButton
{
public:
	ImageButton(QString imageFile);
	QSize sizeHint() const;
private:
	QSize imageSize;
};

#endif
