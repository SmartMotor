#include <QString>
#include <QImage>

#include "ImageButton.h"

ImageButton::ImageButton(QString imageFile)
{
	QImage image(imageFile);
	QString styleSheet = "background-image: url(" + imageFile + ");";

	setStyleSheet(styleSheet);
	imageSize = image.size();
	setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

QSize ImageButton::sizeHint() const
{
	return imageSize;
}
