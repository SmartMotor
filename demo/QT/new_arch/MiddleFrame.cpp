#include <QVBoxLayout>
#include <QPainter>
#include <QLabel>
#include <QPushButton>
#include <QListView>
#include <QTableView>
#include <QStandardItemModel>
#include <QHeaderView>
#include <QScrollBar>
#include <QRect>

#include "MiddleFrame.h"
#include "ImageButton.h"

MiddleFrame::MiddleFrame(void)
{

	QGridLayout *layout = new QGridLayout;

	QStandardItemModel *model = new QStandardItemModel(4, 4);
	for (int row = 0; row < 40; ++row) {
		for (int column = 0; column < 4; ++column) {
			QStandardItem *item = new QStandardItem(QString("row %0, column %1").arg(row).arg(column));
			model -> setItem(row, column, item);
		}
	}

	QTableView *tableView = new QTableView;
	tableView -> setModel(model);

	//tableView -> verticalHeader() -> hide();
	//tableView -> horizontalHeader() -> hide();

	QLabel *order = new QLabel;
	QString string =tr(" 订\n 单\n80g\n 牛\n 皮\n 纸\n");
	order -> setObjectName("orderLabel");
	order -> setText(string);

	/* 2 buttons on top right */
	ImageButton *cutterUp   = new ImageButton("images/刀上-凸副本.bmp");
	ImageButton *cutterDown = new ImageButton("images/刀下-凸副本.bmp");

	/* 2 buttons on bottom right */
	ImageButton *pageUp   = new ImageButton("images/18.bmp");
	ImageButton *pageDown = new ImageButton("images/17.bmp");

	/* Scroll bar */
	QScrollBar *scrollBar = new QScrollBar();
	tableView -> setVerticalScrollBar(scrollBar);

	layout -> addWidget(order,     0, 0, 5, 1);
	layout -> addWidget(tableView, 0, 1, 5, 15);

	layout -> addWidget(cutterUp,   0, 16, 1, 1);
	layout -> addWidget(cutterDown, 0, 17, 1, 1);

	layout -> addWidget(pageUp,   4, 16, 1, 1);
	layout -> addWidget(pageDown, 4, 17, 1, 1);

	layout -> addWidget(scrollBar, 0, 18, 5, 1);

	setLayout(layout);

	setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
}

void MiddleFrame::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QSize mySize = this->size();

	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.setPen(QPen(Qt::yellow, 5, Qt::SolidLine, Qt::RoundCap, Qt::MiterJoin));

	QRect rect(0, 0, mySize.width(), mySize.height());
	painter.drawRoundedRect(rect, 10, 10);
}

QSize MiddleFrame::sizeHint() const
{
	QSize size(1024, 350);
	return size;
}
