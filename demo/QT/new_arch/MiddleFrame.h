#ifndef MIDDLEFRAME_H
#define MIDDLEFRAME_H

class MiddleFrame : public QWidget
{
public:
	MiddleFrame(void);
	void paintEvent(QPaintEvent *event);
	QSize sizeHint() const;
};

#endif
