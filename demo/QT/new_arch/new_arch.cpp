#include <QtGui/QApplication>
#include <QVBoxLayout>
#include <QPainter>
#include <QLabel>
#include <QPushButton>
#include <QTextCodec>

#include <iostream>

#include "UpperFrame.h"
#include "MiddleFrame.h"
#include "LowerCenterFrame.h"

using namespace std;

#define DEFAUT_MAIN_W 1024
#define DEFAUT_MAIN_H 768

#define DEBUG 1

int main(int argc, char **argv)
{
	QApplication app(argc, argv);

	/* Set text codec. This should be done more flexibly. */
	QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
	QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));

	QWidget mainFrame;
	mainFrame.resize(DEFAUT_MAIN_W, DEFAUT_MAIN_H);

	QVBoxLayout *vLayout = new QVBoxLayout;

	UpperFrame  *upperFrame  = new UpperFrame;
	MiddleFrame *middleFrame = new MiddleFrame;


	vLayout->addWidget(upperFrame);
	vLayout->addWidget(middleFrame);

	QHBoxLayout *lowerFrameHLayout = new QHBoxLayout;
	QPushButton *dummy1 = new QPushButton;
	QPushButton *dummy2 = new QPushButton;

	LowerCenterFrame *lowerCenterFrame = new LowerCenterFrame;

	lowerFrameHLayout->addWidget(dummy1);
	lowerFrameHLayout->addWidget(lowerCenterFrame);
	lowerFrameHLayout->addWidget(dummy2);

	vLayout->addLayout(lowerFrameHLayout);
	vLayout->addStretch();

	mainFrame.setLayout(vLayout);

#ifndef DEBUG
	mainFrame.setWindowFlags( Qt::FramelessWindowHint);
#endif
	mainFrame.show();

	return app.exec();
}
