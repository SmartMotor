#ifndef UPPERFRAME_H
#define UPPERFRAME_H

class UpperFrame : public QWidget
{
public:
	UpperFrame(void);
	void paintEvent(QPaintEvent *event);
	QSize sizeHint() const;
};

#endif
