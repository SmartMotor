#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>

//! [0]
class MainWindow : public QDialog
{
    Q_OBJECT

public:
    MainWindow();

public:
    QTranslator appTranslator;

private slots:
    void switchSysLanguage();

private:
    void retranslateUi();

    int sysLanguage;
    QLabel *currentLanguageLabel;
    QLabel *setLanuageLabel;
    QPushButton *setLanuageButton;
    QPushButton *exitButton;
};
//! [0]

#endif
