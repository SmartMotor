#ifndef SYSLANDIALOG_H
#define SYSLANDIALOG_H

#include <QDialog>

class QPushButton;

class SysLanDialog : public QDialog
{
    Q_OBJECT

public:
    SysLanDialog(QWidget *parent = 0);
    int getSysLan();
    static int setSysLan(QWidget *parent = 0);

private slots:
    void setEnglish();
    void setChinese();
    void setJapanese();

private:
    void retranslateUi();

    int syslanID;

    enum { NumSysLanguages = 3 };
    QPushButton *syslanButtons[NumSysLanguages];
    QPushButton *exitButton;
};

#endif
