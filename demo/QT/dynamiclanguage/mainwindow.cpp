/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include <iostream>
#include <QtGui>

#include "mainwindow.h"
#include "syslandialog.h"

//! [0]
MainWindow::MainWindow()
{
  sysLanguage = 0;

  qApp->installTranslator(&appTranslator);

  currentLanguageLabel = new QLabel;
  setLanuageLabel = new QLabel;
  setLanuageButton = new QPushButton;
  connect(setLanuageButton, SIGNAL(clicked()), this, SLOT(switchSysLanguage()));

  exitButton = new QPushButton;
  connect(exitButton, SIGNAL(clicked()), this, SLOT(close()));

  QGridLayout *mainLayout = new QGridLayout;
  mainLayout->addWidget(currentLanguageLabel, 0, 0, 1, 2);
  mainLayout->addWidget(setLanuageLabel, 1, 0);
  mainLayout->addWidget(setLanuageButton, 1, 1);
  mainLayout->addWidget(exitButton, 2, 1);
  setLayout(mainLayout);

  //showFullScreen();

  retranslateUi();
}
//! [0]

void MainWindow::switchSysLanguage()
{
  int lanID;
  QString qmFile;

  lanID = SysLanDialog::setSysLan(this);

  if (lanID == sysLanguage)
    return;

  sysLanguage = lanID;
  
  if (sysLanguage == 0) {
    qmFile = "english.qm";
  }

  if (sysLanguage == 1) {
    qmFile = "chinese.qm";
  }

  if (sysLanguage == 2) {
    qmFile = "japanese.qm";
  }

  appTranslator.load(qmFile);
  retranslateUi();
}

void MainWindow::retranslateUi()
{
  currentLanguageLabel->setText(tr("Current System Language:"));
  setLanuageLabel->setText(tr("Set System Language:"));
  setLanuageButton->setText(tr("Set System Language"));
  exitButton->setText(tr("Exit"));

  setWindowTitle(tr("Main"));
}
