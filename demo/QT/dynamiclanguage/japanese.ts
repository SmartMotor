<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ja_JP">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="103"/>
        <source>Current System Language:</source>
        <translation type="unfinished">システム言語</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="104"/>
        <source>Set System Language:</source>
        <translation type="unfinished">言語設定</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="105"/>
        <source>Set System Language</source>
        <translation type="unfinished">言語設定</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="106"/>
        <source>Exit</source>
        <translation type="unfinished">閉じる</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="108"/>
        <source>Main</source>
        <translation type="unfinished">主要</translation>
    </message>
</context>
<context>
    <name>SysLanDialog</name>
    <message>
        <location filename="syslandialog.cpp" line="108"/>
        <source>English</source>
        <translation type="unfinished">English</translation>
    </message>
    <message>
        <location filename="syslandialog.cpp" line="109"/>
        <source>Chinese</source>
        <translation type="unfinished">中文</translation>
    </message>
    <message>
        <location filename="syslandialog.cpp" line="110"/>
        <source>Japanese</source>
        <translation type="unfinished">日本語</translation>
    </message>
    <message>
        <location filename="syslandialog.cpp" line="111"/>
        <source>Exit</source>
        <translation type="unfinished">閉じる</translation>
    </message>
    <message>
        <location filename="syslandialog.cpp" line="113"/>
        <source>Choose System Language</source>
        <translation type="unfinished">言語設定</translation>
    </message>
</context>
</TS>
