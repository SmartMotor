/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtGui>

#include "syslandialog.h"

SysLanDialog::SysLanDialog(QWidget *parent)
    : QDialog(parent)
{

  syslanID = 0;

  // 0 : english
  syslanButtons[0] = new QPushButton;
  connect(syslanButtons[0], SIGNAL(clicked()), this, SLOT(setEnglish()));

  // 1 : Chinese
  syslanButtons[1] = new QPushButton;
  connect(syslanButtons[1], SIGNAL(clicked()), this, SLOT(setChinese()));

  // 2 : Japanese
  syslanButtons[2] = new QPushButton;
  connect(syslanButtons[2], SIGNAL(clicked()), this, SLOT(setJapanese()));

  exitButton = new QPushButton;
  connect(exitButton, SIGNAL(clicked()), this, SLOT(close()));

  QHBoxLayout *mainLayout = new QHBoxLayout;
  mainLayout->addWidget(syslanButtons[0]);
  mainLayout->addWidget(syslanButtons[1]);
  mainLayout->addWidget(syslanButtons[2]);
  mainLayout->addWidget(exitButton);
  setLayout(mainLayout);

  retranslateUi();
}

int SysLanDialog::getSysLan()
{
  return syslanID;
}

int SysLanDialog::setSysLan(QWidget *parent)
{
  SysLanDialog dlg(parent);

  dlg.exec();

  return dlg.syslanID;
}

void SysLanDialog::setEnglish()
{
  syslanID = 0;
}

void SysLanDialog::setChinese()
{
  syslanID = 1;
}

void SysLanDialog::setJapanese()
{
  syslanID = 2;
}

void SysLanDialog::retranslateUi()
{
  syslanButtons[0]->setText(tr("English"));
  syslanButtons[1]->setText(tr("Chinese"));
  syslanButtons[2]->setText(tr("Japanese"));
  exitButton->setText(tr("Exit"));

  setWindowTitle(tr("Choose System Language"));
}
