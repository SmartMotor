/*
  (c) Copyright 2000-2002  convergence integrated media GmbH.
  (c) Copyright 2002       convergence GmbH.
  All rights reserved.

  Written by Denis Oliver Kropp <dok@directfb.org>,
  Andreas Hundt <andi@fischlustig.de> and
  Sven Neumann <neo@directfb.org>.

  This file is subject to the terms and conditions of the MIT License:

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation
  files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/*
 * DirectFB Picture Viewer Demo Requirements:
 * 
 *   - Show full screen pictures
 *   - User switch picture using keyboard number keys (1 - 9)
 *     or arrow keys (Left/Right)
 *     - 1 shows the first picture and so on
 *   - Number key 0 show an "E" to center of screen, black "E"
 *     and white background
 *     - +/- keys keys to change font size
 *     - Up/Down keys to rotate font
 *
 *   - Picture file names are "1.jpg 2.jpg ... 9.jpg"
 *
 * TODO:
 *   - Rotate text "E"
 *   - Scan all images (bmp, jpg, jpeg, png) in IMAGEDIR
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <directfb.h>
#include <directfb_keynames.h>

#define CLAMP(x, low, high)					\
    (((x) > (high)) ? (high) : (((x) < (low)) ? (low) : (x)))

#define NUM_IMAGES  9  /* 9 images at most */

#define FONT_SIZE_MIN  16
#define FONT_SIZE_MAX  1024

/* the super interface */
static IDirectFB            *dfb;

/* the primary surface (surface of primary layer) */
static IDirectFBSurface     *primary;

/* images */
static IDirectFBSurface     *images[NUM_IMAGES];

/* input interfaces: device and its buffer */
static IDirectFBEventBuffer *events;

static IDirectFBFont        *font_large;
static IDirectFBFont        *font_small;

/* screen info */
static int screen_width, screen_height;

/* font info */
static int font_rotate, font_size;

static int image_index, current_image_index;

/* macro for a safe call to DirectFB functions */
#define DFBCHECK(x...)							\
    {									\
	err = x;							\
	if (err != DFB_OK) {						\
	    fprintf( stderr, "%s <%d>:\n\t", __FILE__, __LINE__ );	\
	    DirectFBErrorFatal( #x, err );				\
	}								\
    }

#define ROTATE_ENABLE

#ifdef ROTATE_ENABLE
static void
show_font (int size, int rotate)
{
    DFBResult               err;
    DFBFontDescription      fontdesc;
    const char             *fontfile = FONT;
    IDirectFBFont          *font;
    IDirectFBSurface       *surface = NULL;
    DFBSurfaceDescription   dsc;
    DFBSurfaceBlittingFlags flags;
    int                     w, h, x, y;
    /* char                    msg[48]; */

    fontdesc.flags = DFDESC_HEIGHT | DFDESC_ATTRIBUTES;
    fontdesc.attributes = 0;
    fontdesc.height = font_size;

    DFBCHECK(dfb->CreateFont (dfb, fontfile, &fontdesc, &font));

    /* White background */
    primary->Clear(primary, 0xFF, 0xFF, 0xFF, 0xff);
    primary->SetFont(primary, font);
    /* Black font */
    primary->SetColor(primary, 0x00, 0x00, 0x00, 0xff);

    font->GetHeight(font, &h);
    font->GetMaxAdvance(font, &w);

    dsc.flags = DSDESC_WIDTH | DSDESC_HEIGHT | DSDESC_PIXELFORMAT;
    /* dsc.flags = DSDESC_PIXELFORMAT; */
    dsc.width = w;
    dsc.height = h;
    dsc.pixelformat = DSPF_ARGB;

    if (dfb->CreateSurface( dfb, &dsc, &surface ) == DFB_OK) {
	/* White background */
	surface->Clear(surface, 0xFF, 0xFF, 0xFF, 0xff);
	surface->SetFont(surface, font);
	/* Black font */
	surface->SetColor(surface, 0x00, 0x00, 0x00, 0xff);
	
	surface->DrawGlyph(surface, (int)'E', 0, 0, DSTF_TOPLEFT);

	switch (rotate) {
	case 90:
	    x = (screen_width  - h) / 2 - size / 16;
	    y = (screen_height - w) / 2 - size / 3;
	    flags = DSBLIT_BLEND_ALPHACHANNEL | DSBLIT_ROTATE90;
	    break;
	case 180:
	    x = (screen_width  - w) / 2 - size / 3;
	    y = (screen_height - h) / 2 - size / 16;
	    flags = DSBLIT_BLEND_ALPHACHANNEL | DSBLIT_ROTATE180;
	    break;
	case 270:
	    x = (screen_width  - h) / 2 - size / 16;
	    y = (screen_height - w) / 2 + size / 4;
	    flags = DSBLIT_BLEND_ALPHACHANNEL | DSBLIT_ROTATE270;
	    break;
	case 0:
	default:
	    x = (screen_width  - size) / 2 + size / 4;
	    y = (screen_height - size) / 2 - size / 8;
	    flags = DSBLIT_BLEND_ALPHACHANNEL;
	    break;
	}

	/* primary->DrawString(primary, "E", -1, x, y, DSTF_TOPLEFT); */
	/* printf("font %d w %d h %d x %d y %d\n", font_size, w, h, x , y); */
    
	primary->SetBlittingFlags( primary, flags );
	primary->Blit( primary, surface, NULL, x, y );

	surface->Release (surface);
    }

#if 0
    /* Size */
    primary->SetFont(primary, font_small);
    primary->SetColor(primary, 0xff, 0x00, 0x00, 0xff);
    snprintf(msg, 48, "Current font size: %d pixel", font_size);
    primary->DrawString(primary, msg, -1, x, screen_height - 48,
			DSTF_CENTER);
#endif
	 
    font->Release(font);
}
#else
static void
show_font (int size, int rotate)
{
    DFBResult              err;
    DFBFontDescription     fontdesc;
    const char            *fontfile = FONT;
    IDirectFBFont         *font;
    int                    x, y;
    char                   msg[48];

    fontdesc.flags = DFDESC_HEIGHT | DFDESC_ATTRIBUTES;
    fontdesc.attributes = 0;
    fontdesc.height = font_size;

    DFBCHECK(dfb->CreateFont (dfb, fontfile, &fontdesc, &font));

    /* White background */
    primary->Clear(primary, 0xFF, 0xFF, 0xFF, 0xff);
    primary->SetFont(primary, font);
    /* Black font */
    primary->SetColor(primary, 0x00, 0x00, 0x00, 0xff);

    x = (screen_width  - font_size) / 2 + font_size / 4;
    y = (screen_height - font_size) / 2 - font_size / 8;

    /* primary->DrawString(primary, "E", -1, x, y, DSTF_TOPLEFT); */
    primary->DrawGlyph(primary, (int)'E', x, y, DSTF_TOPLEFT);

    /* Size */
    primary->SetFont(primary, font_small);
    primary->SetColor(primary, 0xff, 0x00, 0x00, 0xff);
    snprintf(msg, 48, "Current font size: %d pixel", font_size);
    primary->DrawString(primary, msg, -1, x, screen_height - 48,
			DSTF_CENTER);
	 
    font->Release(font);
}
#endif


#ifdef LOAD_ALL
static int
load_images (const char *filepath)
{
    IDirectFBImageProvider *provider;
    IDirectFBSurface       *tmp     = NULL;
    IDirectFBSurface       *surface = NULL;
    DFBSurfaceDescription   dsc;
    DFBResult               err;
    int                     i, j;
    char                    filename[256];

    for (i = 1, j = 0; i <= NUM_IMAGES; i++) {
	memset(filename, 0, 256);
	images[i-1] = NULL;

	snprintf (filename, strlen(filepath) + 8, "%s/%d.jpg",
		  filepath, i);

	err = dfb->CreateImageProvider( dfb, filename, &provider );
	if (err != DFB_OK) {
	    fprintf( stderr, "Couldn't load image from file '%s': %s\n",
		     filename, DirectFBErrorString( err ));
	    return -1;
	}

	provider->GetSurfaceDescription( provider, &dsc );
	dsc.flags = DSDESC_WIDTH | DSDESC_HEIGHT | DSDESC_PIXELFORMAT;
	dsc.pixelformat = DSPF_ARGB;
	if (dfb->CreateSurface( dfb, &dsc, &tmp ) == DFB_OK)
	    provider->RenderTo( provider, tmp, NULL );

	provider->Release( provider );

	if (tmp) {
	    primary->GetPixelFormat( primary, &dsc.pixelformat );
	    if (dfb->CreateSurface( dfb, &dsc, &surface ) == DFB_OK) {
		surface->Clear( surface, 0, 0, 0, 0xFF );
		surface->SetBlittingFlags( surface, DSBLIT_BLEND_ALPHACHANNEL );
		surface->Blit( surface, tmp, NULL, 0, 0 );
	    }
	    tmp->Release( tmp );

	    images[i-1] = surface;
	    j++;
	}
    }

    return j;
}

#else /* LOAD_ALL */

static int
load_image (const char *filepath, int index)
{
    IDirectFBImageProvider *provider;
    IDirectFBSurface       *tmp     = NULL;
    IDirectFBSurface       *surface = NULL;
    DFBSurfaceDescription   dsc;
    DFBResult               err;
    char                    filename[256];

    if (images[index] == NULL ) {
	memset(filename, 0, 256);

	snprintf (filename, strlen(filepath) + 8, "%s/%d.jpg",
		  filepath, index+1);

	err = dfb->CreateImageProvider( dfb, filename, &provider );
	if (err != DFB_OK) {
	    fprintf( stderr, "Couldn't load image from file '%s': %s\n",
		     filename, DirectFBErrorString( err ));
	    return -1;
	}

	provider->GetSurfaceDescription( provider, &dsc );
	dsc.flags = DSDESC_WIDTH | DSDESC_HEIGHT | DSDESC_PIXELFORMAT;
	dsc.pixelformat = DSPF_ARGB;
	if (dfb->CreateSurface( dfb, &dsc, &tmp ) == DFB_OK)
	    provider->RenderTo( provider, tmp, NULL );

	provider->Release( provider );

	if (tmp) {
	    primary->GetPixelFormat( primary, &dsc.pixelformat );
	    if (dfb->CreateSurface( dfb, &dsc, &surface ) == DFB_OK) {
		surface->Clear( surface, 0, 0, 0, 0xFF );
		surface->SetBlittingFlags( surface, DSBLIT_BLEND_ALPHACHANNEL );
		surface->Blit( surface, tmp, NULL, 0, 0 );
	    }
	    tmp->Release( tmp );

	    images[index] = surface;
	}
    }

    return index;
}
#endif

static void
show_image (int index)
{
    int w, h, x, y;
    int ret;
    IDirectFBSurface *this_image;

    if (index < 0 || index > NUM_IMAGES)
	return;

    if (images[index] == NULL) {
	ret = load_image(IMAGEDIR, index);
	if (ret < 0) {
	    char msg[32];

	    primary->SetFont(primary, font_large);
	    /* Red font */
	    primary->SetColor(primary, 0xff, 0x00, 0x00, 0xff);
						
	    snprintf(msg, 32, "Image file %d.jpg not found.", index+1);
	    primary->DrawString(primary, msg, -1, 
				screen_width/2, screen_height/2, DSTF_CENTER);
	    return;
	}
    }

    this_image = images[index];
    if (this_image == NULL)
	return;

    this_image->GetSize(this_image, &w, &h);
		
    x = (screen_width  - w) / 2;
    y = (screen_height - h) / 2;

    primary->SetBlittingFlags( primary, DSBLIT_BLEND_ALPHACHANNEL );
    primary->Blit(primary, this_image, NULL, x, y);

    /* We only hold one buffer */
    this_image->Release(this_image);
    images[index] = NULL;
}

static void
release_images(void)
{
    int i;

    for (i = 0; i < NUM_IMAGES; i++) {
	if (images[i])
	    images[i]->Release(images[i]);
    }
}

static void
init_images_ptr(void)
{
    int i;

    for (i = 0; i < NUM_IMAGES; i++)
	images[i] = NULL;
}

int
main( int argc, char *argv[] )
{
    DFBResult              err;
    DFBSurfaceDescription  sdsc;
    DFBInputEvent          evt;
    DFBFontDescription     fontdesc;
    const char            *fontfile = FONT;
    int                    update  = 1;
    int                    mode    = 0; /* 0: image 1: Show "E" */

    image_index = 0;  /* show the first picture at start */
    current_image_index = -1;
    font_rotate = 0;
    font_size = 128;

    DFBCHECK(DirectFBInit( &argc, &argv ));
    DirectFBSetOption ("bg-none", NULL);

    /* create the super interface */
    DFBCHECK(DirectFBCreate( &dfb ));

    /* create an event buffer for all devices */
    DFBCHECK(dfb->CreateInputEventBuffer( dfb, DICAPS_ALL,
					  DFB_FALSE, &events ));

    /* set our cooperative level to DFSCL_FULLSCREEN
       for exclusive access to the primary layer */
    dfb->SetCooperativeLevel( dfb, DFSCL_FULLSCREEN );

    /* get the primary surface, i.e. the surface of the
       primary layer we have exclusive access to */
    sdsc.flags = DSDESC_CAPS;
    sdsc.caps  = DSCAPS_PRIMARY | DSCAPS_DOUBLE;

    DFBCHECK(dfb->CreateSurface( dfb, &sdsc, &primary ));

    primary->GetSize( primary, &screen_width, &screen_height );

    /* Large font */
    fontdesc.flags = DFDESC_HEIGHT | DFDESC_ATTRIBUTES;
    fontdesc.attributes = 0;
    fontdesc.height = 96;
    DFBCHECK(dfb->CreateFont (dfb, fontfile, &fontdesc, &font_large));
	 
    /* Small font */
    fontdesc.height = 32;
    DFBCHECK(dfb->CreateFont (dfb, fontfile, &fontdesc, &font_small));

    primary->Clear( primary, 0, 0, 0, 0 );
    /* primary->Flip( primary, NULL, 0 ); */

    init_images_ptr();

#ifdef LOAD_ALL
    load_images(IMAGEDIR);
#endif

    while (1) {
	if (update) {
	    primary->Clear (primary, 0xff, 0xff, 0xff, 0xff);

	    if (mode == 0)
		show_image(image_index);
	    else
		show_font(font_size, font_rotate);

	    primary->Flip (primary, NULL, DSFLIP_WAITFORSYNC);

	    update = 0;
	}

	events->WaitForEvent(events);

	while (events->GetEvent(events, DFB_EVENT(&evt)) == DFB_OK) {
	    if (evt.type == DIET_KEYPRESS) {
		switch (DFB_LOWER_CASE (evt.key_symbol)) {
		case DIKS_ESCAPE:
		case DIKS_EXIT:
		case DIKS_END:   /* Cancel key in SmartMotor Keyboard */
		case 'q':
		    return EXIT_SUCCESS;

		case DIKS_CURSOR_RIGHT:
		    if (mode == 0) {
			if (image_index <= NUM_IMAGES - 2)
			    image_index++;
		    } else {
			font_rotate = 0;
		    }
		    update = 1;
		    break;

		case DIKS_CURSOR_LEFT:
		    if (mode == 0) {
			if (image_index > 0)
			    image_index--;
		    } else {
			font_rotate = 180;
		    }
		    update = 1;
		    break;

		case DIKS_CURSOR_UP:
		    if (mode == 0) {
			if (image_index > 0)
			    image_index--;
		    } else {
			font_rotate = 90;
		    }
		    update = 1;
		    break;

		case DIKS_CURSOR_DOWN:
		    if (mode == 0) {
			if (image_index <= NUM_IMAGES - 2)
			    image_index++;
		    } else {
			font_rotate = 270;
		    }
		    update = 1;
		    break;

		case DIKS_0:
		    mode = 1;  /* Show E */		 
		    update = 1;
		    break;

		case DIKS_1:
		case DIKS_2:
		case DIKS_3:
		case DIKS_4:
		case DIKS_5:
		case DIKS_6:
		case DIKS_7:
		case DIKS_8:
		case DIKS_9:
		    image_index = evt.key_symbol - DIKS_1;
		    if (image_index < 0 || image_index >= NUM_IMAGES)
			image_index = 0;

		    if ((current_image_index == image_index) && (mode == 0))
			break;
		    else {
			mode = 0;		 /* Show 0 */
			update = 1;
			current_image_index = image_index;
		    }
		    break;

		case DIKS_MINUS_SIGN:
		    if (font_size > FONT_SIZE_MIN)
			font_size -= 16;
		    update = 1;
		    break;

		case DIKS_PLUS_SIGN:
		    if (font_size < FONT_SIZE_MAX)
			font_size += 16;
		    update = 1;
		    break;

		default:
		    ;

		}
	    }
	}
    }

    release_images();
    font_large->Release ( font_large );
    font_small->Release ( font_small );
    primary->Release( primary );
    events->Release( events );
    dfb->Release( dfb );

    return EXIT_SUCCESS;
}


/* Local Variables:  */
/* c-basic-offset: 4 */
/* End:              */
