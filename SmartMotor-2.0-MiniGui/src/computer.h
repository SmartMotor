/*
 * src/computer.h
 *
 * Copyright 2008 @ xianweizeng@gmail.com
 *
 */

#ifndef _SRC_COMPUTER_H_
#define _SRC_COMPUTER_H_

/* eg: a + b = result */
typedef struct tagComputer_t{
	int a;               /* operation A */
	int b;               /* operation B */
	int result;          /* operation result */
	int method;	     /* 1+ 2- 3*  4/ 0 NULL */
	int status;	     /* 0: input a; 1: input b */
	int isInputChanged;
	int isDataFloat;     /* the input is a float number */
	int isLastDataFloat; /* last input data is float */
	char inputBuf[16];   /* input and display buffer */
} tagComputer;

/*  the bmp structure
 */
typedef struct tagButtonBmp_t{
	int x;			/* X */
	int y;			/* Y */
	int index;		/* index number */
} tagButtonBmp;

/* defined in computer.c */
extern tagComputer computerData;

int EnterComputer(HWND hWnd);

#endif /* _SRC_COMPUTER_H_ */
