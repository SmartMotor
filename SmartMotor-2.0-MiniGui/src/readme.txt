编辑测试按键说明:

smartmotor-editV1:
	不带微调功能
修改数据		F7
插入		Insert
删除		F11
全部删除		先按左边shift,松开后按F11
程序		F8
推纸		F9
删除推纸标志   	shift + F9
输入程序名	shift + F10

smartmotor-editV2:
	带微调功能 ,不带修改和删除功能
微调加		F7   
微调减		F11

smartmotor-run:
自动/手动/半自动/输入切换		F7
RUN				Home
取消				End
前进		F9
后退		F10		(步进5mm)
