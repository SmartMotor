/*
 * src/configure.h
 *
 * Copyright 2008 @ xianweizeng@gmail.com
 *
 */
#ifndef _RTC_H_
#define _RTC_H_

char * convertSysTime(char *dispBuf);

int init_rtc_time(void);
int update_systime(void);
int update_rtctime(void);

#endif
