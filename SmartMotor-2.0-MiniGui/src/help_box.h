#ifndef _HELP_BOX_H_
#define _HELP_BOX_H_ 

static unsigned char caption_bkgnd[]="背景";
static unsigned char caption_help[]="帮助";
static unsigned char caption_file[]="文件列表";
static unsigned char caption_path[]="路径";
static unsigned char caption_waring[] ="警告";
static unsigned char message_name_length[]="用户名最大长度为8，请重新输入！";
static unsigned char message_psw_length[]="密码最大长度为10，请重新输入！";
static unsigned char message_name_null[]="用户名不能为空，请重新输入";
static unsigned char message_name_exit[]="用户名已经存在，请重新输入";
static unsigned char message_default_user[]="默认用户帅哥不能删除";
static unsigned char message_max_user[]="已达最大用户数目8，不能创建新用户";
static unsigned char message_password_wrong[]="密码错误，请重新输入或按ESC返回";
static unsigned char message_password_length[]="密码最大长度为15，请重新输入";
static unsigned char message_password_second[]="请再次输入密码";
static unsigned char message_password_user[]="创建新用户成功";
static unsigned char message_password_two[]="两次输入的密码不一致，请重新输入。";
static unsigned char message_deluser_ok[]="删除用户成功！";
static unsigned char message_deluser_confirm[] = "确认删除用户？";
static unsigned char message_password_wrong_three[]="密码错误超过3次，返回主界面！";
static unsigned char message_programlist_header[]="   序号      名称      第一刀尺寸  使用次数  使用时间";
static unsigned char message_programlist_null[] ="   无           空          0 ";
static unsigned char message_cutslist_header[] ="切刀序号         切刀位置  [单位：毫米]  ";
static unsigned char message_delprogram_confirm[] ="确认删除程序？";
static unsigned char message_delprogram_ok[] ="删除程序成功！";
static unsigned char message_enterprogram_confirm[] ="进入该程序？";
static unsigned char message_cutsdata_overflow[] ="输入尺寸超出范围，请重新输入。";
static unsigned char message_step_overflow[] ="输入步进量超出范围，必须为1mm的整数倍。前进步进范围为1mm到5mm，后退不限。\n请重新输入。";
static unsigned char message_save_userprogram_confirm[] ="是否保存程序文件？";
static unsigned char message_save_userprogram_ok[] ="保存程序文件成功！";
static unsigned char message_delcutdata_confirm[] ="确认删除该刀数据？";
static unsigned char message_delalldata_confirm[] ="确认删除所有数据？";
static unsigned char message_proname_change[] ="是否修改程序说明？";
static unsigned char message_configure_ok[] ="设置成功。";
static unsigned char message_just_waring[] ="微调后有数据溢出，是否继续？";
static unsigned char message_welcome[] ="系统正在启动，请等待 1 分钟...";

static unsigned char message_helpbox_login_main[]="使用帮助：\n\n F1:新建用户 \nF2:删除用户 \nF3:选择用户 \nF4:机器设置  \n\n\n版权所有，翻版必纠。\n";
static unsigned char message_helpbox_psw[]="使用帮助：\n\n 输入1到10位数数字作为用户密码。注意密码设定后不可更改。\n\n\n版权所有，翻版必纠。\n";
static unsigned char message_userprogram_name[]= "使用帮助：\n\n 可以最多输入16个中英文字符简单介绍这个程序，输入完毕按确认键；\n如果不想输入，可以直接按确认键（此时程序名为空）。\n\n\n版权所有，翻版必纠。\n";
static unsigned char message_userprogram_cuts[]= "使用帮助：\n\n 输入尺寸，单位为毫米（mm），可以保留小数点后两位。\n\n\n版权所有，翻版必纠。\n";
static unsigned char message_programlist_help[] ="使用帮助：\n\n 使用翻页键翻页，上下键选择程序；\n选中后按确认键进入程序。\n\n\n版权所有，翻版必纠。\n";
static unsigned char message_computer_help[] ="计算器使用帮助：\n\n F1:清除(CE) \nEnd: 退出(OFF)\n 其它按键如键盘所示 \n\n\n版权所有，翻版必纠。\n";
static unsigned char message_t9_help[] ="输入法使用帮助：\n\n使用数字键2-9输入对应的拼音，按翻页键移动所需拼音到第一位置，\n按数字键1确认拼音,然后按数字键0-9选择所需汉字，如果不在当前显示页，按翻页键查看。\n如需输入英文或数字，按插入键切换输入法。\n\n\n版权所有，翻版必纠。\n";
static unsigned char message_helpbox_config[]="使用帮助：\n\n 按上下键选择设置选项，确定键进入。\n\n\n版权所有，翻版必纠。\n";
static unsigned char message_helpbox_date[]="使用帮助：\n\n 输入当前日期，年月日。\n\n\n版权所有，翻版必纠。\n";
static unsigned char message_helpbox_time[]="使用帮助：\n\n 输入当前时间，秒数不用设置。\n\n\n版权所有，翻版必纠。\n";
static unsigned char message_helpbox_lang[]="使用帮助：\n\n 按上下键选择语言，确定键确定。\n\n\n版权所有，翻版必纠。\n";
static unsigned char message_helpbox_flash[]="使用帮助：\n\n 上下键选择是否显示开机动画。\n\n\n版权所有，翻版必纠。\n";
static unsigned char message_helpbox_cutpos[]="使用帮助：\n\n输入尺寸按运行键马达运行，按修改键修正当前尺寸。\n\n\n版权所有，翻版必纠。\n";
static unsigned char message_helpbox_limit[]="使用帮助：\n\n输入马达运行极限值，确定键确定。\n\n\n版权所有，翻版必纠。\n";
static unsigned char message_helpbox_step[]="使用帮助：\n\n输入马达点动前进和后退步进量，确定键确定。输入0表示连续模式。\n\n\n版权所有，翻版必纠。\n";
static unsigned char message_helpbox_bkgnd[]="使用帮助：\n\n使用上下键选择背景图片，按确定键确定。\n\n\n版权所有，翻版必纠。\n";

#endif