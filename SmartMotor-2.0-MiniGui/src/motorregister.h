/*
 *  motorregister.h
 *
 *  Copyright 2008 @ xianweizeng@gmail.com
 *
 */
#ifndef _MOTORREGISTER_H_
#define _MOTORREGISTER_H_

#define MAX_USER_NUMBER	8

struct userinfor_t{		/* User profile */
	int userid;             /* the user global ID */
	int recent_programe;
	int language;		/* 0:CN 1:EN */
	int unit;		/* 0:mm 1:mil ( 1/1000 inch)*/
	int frontlimit;
	int middlelimit;
	int backlimit;
	int forwardstep;
	int backwardstep;
	int speed;	       /* Run Speed 8 - 12 m/min */
	char name[20];
	char password[10];
};


#define USER_EDIT_SELECT    0   /* Normal select (ENTER select one user) */
#define USER_EDIT_NEW       1   /* new user */
#define USER_EDIT_DELETE    2   /* delete user */
#define USER_EDIT_MODIFY    3   /* modify user name */
#define USER_EDIT_PWD       4   /* passwork check??? */

/* the user related data will be moved to this structure */
struct user_manage_t {
	int status;
};

#define PSW_CANCEL    0
#define PSW_OK        1
#define PSW_FAIL      2

/* global variables defined in motorregister.c */
extern struct userinfor_t userinformation[MAX_USER_NUMBER];
extern struct userinfor_t *currentuser;
extern int  currentid;
extern int  usernumber;

int isUserNameValid(HWND hWnd,char *nameBuff,int len);
int initUserDataAndPrograms(HWND hWnd, int userid);

int EnterRegisterDialog(void);
int loadUserConfigFile(void);
int saveUserConfigFile(int delete_user);

int CheckPassword(HWND hWnd);

#endif /* _MOTORREGISTER_H_ */
