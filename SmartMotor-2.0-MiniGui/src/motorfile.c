/*
 * src/motorfile.c
 *
 * The program list window implementation
 *
 * Copyright 2007-2008 @ xianweizeng@gmail.com
 *
 * Author:
 *   Zeng Xianwei
 *
 *  File Coding system: gb2312
 *    set-buffer-file-coding-system
 *
 * ChangeLog:
 *	V0.1	  Quan Dongxiao
 *	V0.2	  Zeng Xianwei @ 2005-12-28
 *		  Rewrite Most of the code;
 *	V0.3	  Zeng Xianwei @ 2006-01-17
 *   2008-09-24   Change code indent
 *   2008-10-11   This window is deleted. But this file is used to
 *                provide the file operation functions.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <errno.h>


#include <minigui/common.h>
#include <minigui/minigui.h>
#include <minigui/gdi.h>
#include <minigui/window.h>
#include <minigui/control.h>

//#include "help_box.h"
#include "bkgndEdit.h"
#include "motor.h"
#include "userprogram.h"
#include "motorfile.h"
#include "motorregister.h"
#include "configure.h"
#include "help.h"



static int lastProFile,curProFile;

static enum {
	ARRAY_BY_NUM = 0,
	ARRAY_BY_DATE
} proListArrayFlag;             /* array list flag */

#define MAX_PROGRAM_FILES       99
#define MAX_FILENAME_LENGTH     30

static int proListArrayID[MAX_PROGRAM_FILES];
static struct programe_link_t *proLinkHead;	/* Link head pointer */

static char filepath[MAX_FILENAME_LENGTH];	/* "./programe/userxx/99.pro" */

extern void onefile_new(const char*filename,int i);

struct filelist_help_t{
	unsigned char * msg_prolist_header;
	unsigned char * msg_prolist_null;
};

static struct filelist_help_t filelist_help[SYSLANG_NO] =
{
	{
		msg_prolist_header_cn,
		msg_prolist_null_cn,
	},

	{
		msg_prolist_header_en,
		msg_prolist_null_en,
	}
};

/*****************************************************************************/
/*   Program File List Handle Functions
 */

static int initFilePathName(int userid)
{
	char *str = filepath;

	memset(str, 0, MAX_FILENAME_LENGTH);
	sprintf(str, "./programe/user%02d/", userid);

#if 0
	printf("data path = %s length=%d\n", filepath, strlen(str));

#endif

	return 0;
}

/* load specified program file
 *
 * Input:
 *   id: file id
 *   pFileData: file data pointer
 * Output:
 *   status: 0: OK, else error
 *
 */
int loadProgramFile(int id, struct fromProListToPro_t *pFileData)
{
	char pathname[48],name[8];
	FILE *fp;
	int ret;

	initFilePathName(currentuser->userid);

	sprintf(name,"%d.pro",id);
	strcpy(pathname, filepath);
	strcat(pathname, name);

	fp = fopen(pathname,"rb");
	if(fp == NULL){
		printf("Open program file %s error.\n",pathname);
		return (-1);
	}

	ret = fread(&(pFileData->proHeader),
		    sizeof(struct userProFileHeader_t), 1, fp);
	if (ret <= 0 ){
		printf("Read program file %s error.\n",pathname);
		fclose(fp);
		return -1;
	}
	fclose(fp);

	pFileData->isFileNew = pFileData->proHeader.isnull;
	strcpy(pFileData->filePathname, pathname);

	printf("Load file: %s OK.\n",pathname);
	return 0;
}


static int freeAllProgramFileNodes(struct programe_link_t *pHead)
{
	int index = 0;
	struct programe_link_t *p1 = pHead->next, *p2 = NULL;

	while(p1 != NULL){
		p2 = p1->next;
		free(p1);
		p1 = p2;
		index++;
	}

	return index;
}


/*   Read All files, store in link
 */
static struct programe_link_t * loadProgramFiles(void)
{
	char pathname[30],name[8];
	int index=0;
	FILE *fp;
	struct programe_link_t *p1, *p2 = NULL;

	/* link list already loaded
	 * This would happen if we switch to another user and reload
	 * all the program file of that user.
	 */
	if(proLinkHead != NULL){
		freeAllProgramFileNodes(proLinkHead);
		proLinkHead = NULL;
	}

	for(index = 1;index < 100; index++) {
		strcpy(pathname, filepath);
		sprintf(name,"%d.pro",index);
		strcat(pathname,name);

		fp = fopen(pathname,"rb");
		if(fp == NULL){
			printf("open file error: %s \n", pathname);
			perror("fopen");
		}
		p1 = (struct programe_link_t *)malloc(sizeof(struct programe_link_t));
		fread(&(p1->prog),sizeof(struct userProFileHeader_t),1,fp);

		if(proLinkHead == NULL){
			proLinkHead = p1;
			p2 = p1;
		} else {
			p2->next = p1;
		}
		p2 = p1;

		fclose(fp);
	}
	p2->next = NULL;	// Last node set to null

	return(proLinkHead);
}

static struct programe_link_t * getProLinkItem(struct programe_link_t *pHead,int index)
{
	int id;
	struct programe_link_t *pPro = pHead;
	for(id=1;id<index;id++){
		pPro = pPro->next;
	}
	return(pPro);
}

struct programe_link_t * getProfileListItem(int cursel)
{
	int id;
	int file_index;

	struct programe_link_t *pPro = proLinkHead;   /* the list head */

	if(proListArrayFlag == 0){
		file_index = cursel + 1;
	} else {
		file_index = proListArrayID[cursel];
	}

	for(id = 1; id < file_index; id++){
		pPro = pPro->next;
	}

	return(pPro);
}

int getFilePathName(char *strPathName, int file_id)
{
	int len;
	memset(strPathName, 0, MAX_FILE_PATH_NAME);
	len = sprintf(strPathName,"%s%d.pro", filepath, file_id);

	return len;
}


/*   list by date
 */
static int arrayProLinkByDate(struct programe_link_t *pHead)
{
	int id,ret;
	int index=1,lest=1;
	int tmpid[99];
	int tmp;
	int nFile=0;	// non-empty files number
	struct programe_link_t *pPro = pHead;
	struct programe_link_t *p1,*p2;
	for(index=0;index<99;index++){
		proListArrayID[index] = 0;
		tmpid[index] = index+1;
	}
	index = 1;
	while(pPro != NULL){// find non-empty file
		if(pPro->prog.isnull == 0){
			proListArrayID[nFile] = index;
			tmpid[index-1] = 0;
			nFile++;
		}
		index++;
		pPro = pPro->next;
	}
	// List them
	index=0;
	while(proListArrayID[index] != 0){
		p1 = getProLinkItem(pHead,proListArrayID[index]);
		lest = 1;
		while(proListArrayID[index+lest] != 0){
			p2 = getProLinkItem(pHead,proListArrayID[index+lest]);
			ret = strcmp(p1->prog.latestDate,p2->prog.latestDate);
			if(ret < 0){	// p1 < p2
				tmp = proListArrayID[index];
				proListArrayID[index] = proListArrayID[index+lest];
				proListArrayID[index+lest] = tmp;

				p1 = getProLinkItem(pHead,proListArrayID[index]);
			}
			lest++;
		}
		index++;
	}

	// fill other proListArrayID
	id = nFile;
	for(index=0;index<99;index++){
		if(tmpid[index] != 0){
			proListArrayID[id++] = tmpid[index];
		}
	}
	return 0;
}

/*   Refresh list
 *   hwnd: main hwnd
 *   flag: 0 by number  1  by date
 */
static int refreshProgramList(HWND hwnd, int curSel, int flag)
{
	int index=1;
	int id;
	float firstCut;
	char dispBuf[60];
	struct programe_link_t *pPro = proLinkHead;
	HWND hwndFlieList=GetDlgItem(hwnd,IDC_PROFILE_LIST);

	SendDlgItemMessage(hwnd, IDC_PROFILE_LIST, LB_RESETCONTENT,0,0);

	switch(flag){
	case ARRAY_BY_NUM:
		while(pPro != NULL){
			if(pPro->prog.isnull){
				sprintf(dispBuf,"  %4d    %s ",pPro->prog.id,filelist_help[*curSysLang].msg_prolist_null);
			} else {
				firstCut = (float)((pPro->prog.first & 0x00ffffff) / 100.0);
#if 0
				if(firstCut < 5){	/* firstCut == 0.0, not display */
					sprintf(dispBuf,"  %4d    %s ", pPro->prog.id,
						filelist_help[*curSysLang].msg_prolist_null);
				} else
#endif
				{
					sprintf(dispBuf,"  %4d     %-10s             ", pPro->prog.id,
						pPro->prog.introduction);
					sprintf(&dispBuf[24],"%8.2f        ",firstCut);
					sprintf(&dispBuf[33],"%6d     %s",pPro->prog.usedTimes,pPro->prog.latestDate);
				}
			}
			SendDlgItemMessage(hwnd,IDC_PROFILE_LIST, LB_ADDSTRING,0,(LPARAM)dispBuf);
			pPro = pPro->next;
			index++;
		}
		break;
	case ARRAY_BY_DATE:
		arrayProLinkByDate(proLinkHead);
		for(index=0;index<99;index++){
			pPro = proLinkHead;
			for(id=1;id<proListArrayID[index];id++){
				pPro = pPro->next;
			}
			if(pPro->prog.isnull){
				sprintf(dispBuf,"  %4d    %s ",pPro->prog.id,filelist_help[*curSysLang].msg_prolist_null);
			}
			else{
				firstCut = (float)((pPro->prog.first & 0x00ffffff) / 100.0);
				sprintf(dispBuf,"  %4d     %-10s             ",pPro->prog.id,pPro->prog.introduction);
				sprintf(&dispBuf[24],"%8.2f        ",firstCut);
				sprintf(&dispBuf[33],"%6d     %s",pPro->prog.usedTimes,pPro->prog.latestDate);
			}
			SendDlgItemMessage(hwnd,IDC_PROFILE_LIST,LB_ADDSTRING,0,(LPARAM)dispBuf);
		}
		break;
	}

	SendMessage(hwndFlieList,LB_SETCURSEL,curSel,0);

	return 0;
}

int refreshProgramListByDefault(HWND hwnd, int curSel)
{
	return refreshProgramList(hwnd, curSel, proListArrayFlag);
}

int listProgramFileByNum(HWND hwnd, int curSel)
{
	if(proListArrayFlag == ARRAY_BY_DATE){
		proListArrayFlag = ARRAY_BY_NUM;
		refreshProgramList(hwnd, curSel, proListArrayFlag);
	}

	return 0;
}

int listProgramFileByDate(HWND hwnd, int curSel)
{
	if(proListArrayFlag == ARRAY_BY_NUM){
		proListArrayFlag = ARRAY_BY_DATE;
		refreshProgramList(hwnd, curSel, proListArrayFlag);
	}

	return 0;
}


/*  Only delete data in this file, the file is not deleted
 *
 *  Note: when One file is deleted, it is necessary to refresh
 *        the list.
 */
int deleteOneProFile(HWND mainhwnd, int curSel)
{
	char pathname[30],name[8];
	int id = curSel + 1;
	struct programe_link_t *pPro = proLinkHead;

	/* update file */
	strcpy(pathname, filepath);
	sprintf(name, "%d.pro", id);
	strcat(pathname,name);
	onefile_new(pathname, id); /* this function will reset all the data in this file */

	// delete content in list
	if(proListArrayFlag == ARRAY_BY_NUM){
		pPro = getProLinkItem(proLinkHead, curSel+1);
	} else {
		pPro = getProLinkItem(proLinkHead, proListArrayID[curSel]);
	}
	pPro->prog.isnull = 1;
	pPro->prog.introduction[0] = '\0';
	pPro->prog.latestDate[0] = '\0';
	pPro->prog.usedTimes = 0;

	refreshProgramList(mainhwnd, curSel, proListArrayFlag);

	return id;
}

/*   initiallize: read files and dispaly them
 *
 *   hwnd: the main HWND
 */
int initProgramList(HWND hwnd, int cursel)
{
	int index;
	proLinkHead = NULL;
	lastProFile = 0;
	curProFile = 0;

	proListArrayFlag = ARRAY_BY_NUM;

	/* Although filepath is already initiallized
	 * we re-init it because the current user maybe changed
	 */
	initFilePathName(currentuser->userid);

	/* head */
	SetDlgItemText(hwnd, IDC_PROFILE_HEAD, filelist_help[*curSysLang].msg_prolist_header);

	loadProgramFiles();
	refreshProgramList(hwnd, cursel, proListArrayFlag);

	for(index=0;index<99;index++){
		proListArrayID[index] = 0;
	}

	return 0;
}
