/*
 * src/keyboard.h
 *
 * Copyright 2008 @ xianweizeng@gmail.com
 *
 */
#ifndef _KEYBOARD_H_
#define _KEYBOARD_H_

int EnterKeyboardCheck(HWND hWnd);

#endif /* _KEYBOARD_H_ */
