/*
 * src/rtc.c
 *
 * RTC functions
 *
 * Copyright 2007-2008 @ xianweizeng@gmail.com
 *
 * Author:
 *   Zeng Xianwei
 *
 *  File Coding system: gb2312
 *    set-buffer-file-coding-system
 *
 * ChangeLog:
 *   2009-01-12   V0.1
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <errno.h>
#include <time.h>

#include <unistd.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/select.h>

#include <minigui/common.h>
#include <minigui/minigui.h>
#include <minigui/gdi.h>
#include <minigui/window.h>
#include <minigui/control.h>

#include "motor.h"

struct rtc_tm {
  	int tm_sec;			/* Seconds.	[0-60] (1 leap second) */
  	int tm_min;			/* Minutes.	[0-59] */
  	int tm_hour;			/* Hours.	[0-23] */
  	int tm_mday;			/* Day.		[1-31] */
  	int tm_mon;			/* Month.	[0-11] */
  	int tm_year;			/* Year	- 1900.  */
  	int tm_wday;			/* Day of week.	[0-6] */
};


/* PCF8563 device */
#define RTC_DEVICE	"/dev/rtc"


/* RTC ioctl commands macro
 *   See kernel driver source code
 */
#define RTC_GETDATETIME	    0
#define RTC_SETDATE	    1
#define RTC_SETTIME	    2
#define RTC_SETDATETIME	    3
#define RTC_GETCTRL	    4
#define RTC_SETCTRL	    5

/*
 *get system time from RTC chip
 */
static int getSysRtcTime(struct rtc_tm *tm)
{
	int fd,ret;

	fd = open(RTC_DEVICE, O_RDONLY);
	if(fd < 0){
		perror("error:can not open rtc devicen");
		return(-1);
	}

	ret = ioctl(fd, RTC_GETDATETIME, tm);
	if(ret < 0){
		perror("Get time error.\n");
		close(fd);
		return -1;
	}

	close(fd);
	return(0);
}

/*
 * Set system time to RTC chip
 */
static int setSysRtcTime(struct rtc_tm *tm)
{
	int fd, ret;

	fd = open(RTC_DEVICE, O_RDWR);
	if(fd < 0){
		perror("error: can not open rtc devicen.");
		return -1;
	}

	ret = ioctl(fd, RTC_SETDATETIME, tm);
	if(ret < 0){
		perror("Get time error.\n");
		close(fd);
		return -1;
	}

	close(fd);
	return 0;
}


#if 0
/*
 * This function is obsoleted by setSysRtcTime()
 */

/*  set RTC date or time.
 *
 *  flag = 0 time   1 date
 */
static int setSysRtcDateTime(struct rtc_tm *tm,int flag)
{
	int fd, ret;

	/* printf("Open RTC devices.\n"); */
	fd=open(RTC_DEVICE, O_RDWR);
	if(fd < 0){
		perror("error:can not open rtc device: %s\n");
		return(-1);
	}

	ret = ioctl(fd,flag,tm);
	if(ret < 0){
		perror("Set system datetime error.\n");
		close(fd);
		return -1;
	}

	close(fd);
	return 0;
}
#endif

/*
 * set rtc time to 2009-01-01 00:00:01
 * call this when do a new release
 */
int init_rtc_time(void)
{
	struct rtc_tm tm;
	int ret;

	tm.tm_sec = 1;
	tm.tm_min = 0;
	tm.tm_hour = 0;
	tm.tm_mday = 1;
	tm.tm_mon = 1;
	tm.tm_year = 2009 - 1900;

	/* set RTC time */
	ret = setSysRtcTime(&tm);
	if(!ret) {
		printf("Set RTC time error: %d \n", ret);
		return -1;
	}

	return 0;
}

#ifndef CONFIG_DEBUG_ON_PC
/*
 * update Linux system time by RTC time.
 * read RTC and then call settimeofday().
 *
 * This should be done in Linux kernel. But we can do it here
 * if the RTC driver is not well writen.
 */
int update_systime(void)
{
	struct rtc_tm rtc_tm;
	struct tm tm;
	struct timeval tv;
	int ret;

	/* get system time */
	gettimeofday(&tv, NULL);

	ret = getSysRtcTime(&rtc_tm);
	if(ret < 0) {
		printf("Read RTC time error.\n");

		init_rtc_time();
		return -1;
	}

	tm.tm_sec  = rtc_tm.tm_sec;
	tm.tm_min  = rtc_tm.tm_min;
	tm.tm_hour = rtc_tm.tm_hour;
	tm.tm_mday = rtc_tm.tm_mday;
	tm.tm_mon  = rtc_tm.tm_mon - 1;
	tm.tm_year = rtc_tm.tm_year - 1900;

	tv.tv_sec = mktime(&tm);

	if (tv.tv_sec == -1) {
		printf("mktime error\n");
	}

	/* tv.tv_usec = 0; */

	ret = settimeofday(&tv, NULL);

	if(ret == 0) {
		printf("update system time successful.\n");
	} else {
		printf("update system time failed.\n");
		return -1;
	}

	return 0;
}

/*
 *  update RTC time by system time
 */
int update_rtctime(void)
{
	struct rtc_tm rtc_tm;
	struct tm * tm;
	struct timeval tv;
	int ret;
	time_t curtime;

	/* get system time */
	ret = gettimeofday(&tv, NULL);
	if(ret < 0) {
		printf("get system time error.\n");
		return -1;
	}

	curtime = tv.tv_sec;
	tm = localtime(&curtime);

	rtc_tm.tm_sec  = tm->tm_sec;
	rtc_tm.tm_min  = tm->tm_min;
	rtc_tm.tm_hour = tm->tm_hour;
	rtc_tm.tm_mday = tm->tm_mday;
	rtc_tm.tm_mon  = tm->tm_mon + 1;
	rtc_tm.tm_year = tm->tm_year;

	ret = setSysRtcTime(&rtc_tm);
	if(ret < 0) {
		printf("set RTC time error.\n");
		return -1;
	}

	return 0;
}
#else
int update_systime(void)
{
	return 0;
}

int update_rtctime(void)
{
	return 0;
}
#endif


/*
 * convert time to string
 *
 */
char * convertSysTime(char *dispBuf)
{
	struct timeval tv;
	time_t curtime;

	gettimeofday(&tv, NULL);
	curtime = tv.tv_sec;

	strftime(dispBuf, 30, "%Y-%m-%d  %T", localtime(&curtime));

	return(dispBuf);
}
