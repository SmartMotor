/*
 * src/configure.h
 *
 * Copyright 2008 @ xianweizeng@gmail.com
 *
 */
#ifndef _MOTORCONFIGURE_H_
#define _MOTORCONFIGURE_H_

int setCutPos (HWND hwnd);
int enterMachineConfig(HWND hwnd);
#endif
