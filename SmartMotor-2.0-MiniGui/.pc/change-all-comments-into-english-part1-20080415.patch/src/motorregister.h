/*
 *  motorregister.h
 *
 *  Copyright 2008 @ xianweizeng@gmail.com
 *
 */
#ifndef _MOTORREGISTER_H_
#define _MOTORREGISTER_H_

#define MAX_USER_NUMBER	8

struct userinfor_t{		/* User profile */
	char name[20];
	char password[10];
	int recent_programe;
	int language;		/* 0:CN 1:EN */
	int unit;		/* 0:mm 1:mil ( 1/1000 inch)*/
	int frontlimit;
	int middlelimit;
	int backlimit;
	int forwardstep;
	int backwardstep;
	int speed;	       /* Run Speed 8 - 12 m/min */
};

/* global variables defined in motorregister.c */
extern struct userinfor_t userinformation[MAX_USER_NUMBER];
extern struct userinfor_t *currentuser;
extern int  currentid;
extern int  usernumber;

/*
struct registerTag{
	struct userinfor_t userinformation[8];
				// currentuser = userinformation[currentid]
	int  currentID;		// 当是选择一个用户登录时,记录是第几个用户
	int  usernumber;  		// 一共有的用户数目
	int  registerStatus;		// 登录界面状态 	0 选则已已有用户登录,输入密码
				//		1 新建用户  2 第一密码
				//	       	3 新建时第二密码     4 密码正确	5 密码错误
				//		6 删除用户 	7 删除用户验证超级密码 
				//		8 超级密码正确(4) 	9 密码错误
};

struct registerTag registerInfo;
*/

int EnterRegisterDialog(void);
int loadUserConfigFile(void);
int saveUserConfigFile(int flags);
#endif
