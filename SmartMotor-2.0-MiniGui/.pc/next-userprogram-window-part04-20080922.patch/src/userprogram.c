/*
 * src/userprogram.c
 *
 * The work window implementation
 *
 * Copyright 2007-2008 @ xianweizeng@gmail.com
 *
 * Author:
 *   Zeng Xianwei
 *
 *  File Coding system: gb2312
 *    set-buffer-file-coding-system
 *
 * ChangeLog:
 *   2008-09-06 Next generation
 *   2008-09-21 list icon, buttons, split files
 *              this file mainly deal with the "interface"
 *              data edit functions are moved to program_edit.c
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <minigui/common.h>
#include <minigui/minigui.h>
#include <minigui/gdi.h>
#include <minigui/window.h>
#include <minigui/control.h>

#include "userprogram.h"
#include "motorfile.h"
#include "bkgndEdit.h"
#include "motor.h"
#include "configure.h"
#include "computer.h"
#include "motorregister.h"
#include "motormain.h"
#include "help.h"


/*
 * controls ID definition
 */
#define IDC_PROGRAMDESC		515
#define IDC_PROGRAMDATASTEP     516
#define IDC_STATUSBAR           517

#define IDC_STATUSMODE          518
#define IDC_INPUTDESC           519

#define IDC_CURPOSDESC          535


/* buttons ID
 */
#define IDC_BUTTON_EDIT_F1      721   /* F1 */
#define IDC_BUTTON_EDIT_F2      722   /* F2 */
#define IDC_BUTTON_EDIT_F3      723   /* F3 */
#define IDC_BUTTON_EDIT_F4      724   /* F4 */
#define IDC_BUTTON_EDIT_F5      725   /* F5 */
#define IDC_BUTTON_EDIT_F6      726   /* F6 */

#define IDC_BUTTON_REGISTER     527   /* user window */
#define IDC_BUTTON_PROLIST      528   /* program list window */
#define IDC_BUTTON_MD_EDIT      529   /* Edit Mode */
#define IDC_BUTTON_MD_AUTO      530   /* AutoRun Mode */
#define IDC_BUTTON_MD_MANUAL    531   /* MannualRun Mode */
#define IDC_BUTTON_MD_TRAINING  532   /* Training Mode */

#define IDC_BUTTON_ENTER        540   /* Enter */
#define IDC_BUTTON_CLEAR        541   /* Clear */
#define IDC_BUTTON_RUN          542   /* Run */




/*
 * Window layout definition
 */
#define BMP_INPUT_X             2640    // X坐标
#define BMP_AUTO_X              2720    // X坐标
#define BMP_AUTOCUT_X           2800
#define BMP_MANNUAL_X           3040
#define BMP_RUN_X               2880
#define BMP_STOP_X              2960
#define BMP_PUSHER_X            3040
#define BMP_SHIFT_X             3120
#define BMP_CUTUP_X             2320
#define BMP_CUTDOWN_X           2400
#define BMP_PUSHUP_X            2480
#define BMP_PUSHDOWN_X          2560


#define AUTO_X          200     //  在背景上X坐标
#define HALFAUTO_X      200
#define MANNUAL_X       200
#define INPUT_X         200     // X坐标
#define RUN_X           280
#define STOP_X          280
#define PUSHER_X                360
#define SHIFT_X         360
#define CUTUP_X         40
#define CUTDOWN_X       40
#define PUSHUP_X                120
#define PUSHDOWN_X      120

#define ENTER_X         260
#define ENTER_Y         435
#define CANCEL_X                320
#define CANCEL_Y                435

#define CURPOS_X                230
#define CURPOS_Y                75

/* Data extra flag
 * the higher 8 bits are for data flag
 */
#define PUSHER_FLAG             0x01000000    /* pusher on  */
#define AUTOCUT_FLAG            0x02000000    /* autocut on */
#define AIR_FLAG                0x04000000    /* Air off    */
#define PRESS_FLAG              0x08000000    /* press on   */

#define AIR_ICON_BIT            0x80000000    /* on/off bit */
#define PUSHER_ICON_BIT         0x40000000
#define PRESS_ICON_BIT          0x20000000
#define AUTOCUT_ICON_BIT        0x10000000

#define AIR_ICON_SW_BIT         0x08000000    /* icon switch bit */
#define PUSHER_ICON_SW_BIT      0x04000000
#define PRESS_ICON_SW_BIT       0x02000000
#define AUTOCUT_ICON_SW_BIT     0x01000000


/* Machine state mask
 */
#define CUT_STATUS_MASK         0x0001
#define PUSHER_STATUS_MASK      0x0002
#define WHICH_LIMIT_MASK        0x0004
#define MACHINE_STOP_MASK       0x0008

/* Motor running macro
 */
#define MOTOR_FORWARD           0
#define MOTOR_BACKWARD          1
#define MOTOR_FORWARD_HS        2
#define MOTOR_BACKWARD_HS       3


BITMAP userProgramEditBkgnd;            /* window background */

static BITMAP userProgramEditButton;    /* buttons */
static BITMAP largeNumsIcon;            /* Number 0 - 9 */
static BITMAP lableEditBkgnd;

static LISTBOXITEM_ICON myicons;
static BITMAP flagicon[5];

static BITMAP cutPosIcon, cutCounterIcon;  /* cut pos and cut counter */

/* button bitmaps
 */
static BITMAP btBmpEditFn[6];     /* Edit Mode: F1 - F6*/
static BITMAP btBmpCommonLeft[6]; /* common 6 icons in all modes */
				  /* on the left of screen */
static BITMAP btBmpEnter, btBmpClear, btBmpRun;

/* global valiables
 */
userProgramEditData programEdit;                /* program data edit */
struct fromProListToPro_t programCreate;        /* program create/old status */
static userProgramRunStatus proRunStatus;       /* program run status */

static int userProExitFlag = 0;
static struct comCmdData_t cmdData;


struct userpro_help_t{
	unsigned char * caption_help;
	unsigned char * caption_waring;
	unsigned char * msg_cutdata_overflow;
	unsigned char * msg_save_pro_confirm;
	unsigned char * msg_save_pro_ok;
	unsigned char * msg_delcutdata_confirm;
	unsigned char * msg_delalldata_confirm;
	unsigned char * msg_proname_change;
	unsigned char * msg_just_confirm;
	unsigned char * msg_cutslist_header;
	unsigned char * msg_cutslist_header_inch;
	unsigned char * help_userpro_name;
	unsigned char * help_userpro_cuts;
	unsigned char * help_t9;

};

static struct userpro_help_t userpro_help[SYSLANG_NO] =
{
	{       caption_help_cn,
		caption_waring_cn,
		msg_cutdata_overflow_cn,
		msg_save_pro_confirm_cn,
		msg_save_pro_ok_cn,
		msg_delcutdata_confirm_cn,
		msg_delalldata_confirm_cn,
		msg_proname_change_cn,
		msg_just_confirm_cn,
		msg_cutslist_header_cn,
		msg_cutslist_header_inch_cn,
		help_userpro_name_cn,
		help_userpro_cuts_cn,
		help_t9_cn
	},

	{       caption_help_en,
		caption_waring_en,
		msg_cutdata_overflow_en,
		msg_save_pro_confirm_en,
		msg_save_pro_ok_en,
		msg_delcutdata_confirm_en,
		msg_delalldata_confirm_en,
		msg_proname_change_en,
		msg_just_confirm_en,
		msg_cutslist_header_en,
		msg_cutslist_header_inch_en,
		help_userpro_name_en,
		help_userpro_cuts_en,
		help_t9_en
	}
};

static char strProgramDesc[] = "程序";
static char strProgramStep[] = "步骤";

static char strMachineMode[4][8] = {{"编程"},{"自动"},{"手动"},{"自刀"}};
static char strInputDataDesc[] = "输入尺寸";
static char strCurrentPosDesc[] = "当前实际尺寸";


static DLGTEMPLATE UserProgromDlgInitProgress =
{
	WS_BORDER,
	WS_EX_NONE,
	0, 0, 640, 480,
	"program window",
	0, 0,
	13, NULL,
	0
};

static CTRLDATA CtrlInitProgress [] =
{
	{
		CTRL_STATIC,
		WS_VISIBLE | SS_CENTER,
		400, 5, 180, 30,
		IDC_TIME,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC,
		WS_VISIBLE |ES_READONLY,
		25 ,55, 120, 30,
		IDC_PROGRAMNUM,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC,
		WS_VISIBLE |ES_READONLY,
		145 ,55, 45, 30,
		IDC_PROGRAMDATASTEP,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC, /* set program name in program list window */
		WS_VISIBLE | SS_SIMPLE,
		40, 7, 200, 28,
		IDC_PROGRAMNAME,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{       /* current position */
		CTRL_STATIC,
		WS_VISIBLE | SS_SIMPLE | ES_READONLY,
		130, 80, 200, 28,
		IDC_CURPOSDESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC,
		WS_VISIBLE | SS_SIMPLE |WS_BORDER| ES_READONLY,
		58 , 145, 300,20,
		IDC_PROGRAMCUTSLISTHEAD,
		NULL,
		0
	},
	{
		CTRL_LISTBOX,
		WS_VISIBLE | WS_BORDER|WS_VSCROLL | LBS_NOTIFY | LBS_EXTRA_STYLE1,
		58, 165, 300, 200,
		IDL_PROGRAMCUTSLIST,
		NULL,
		200
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE |WS_BORDER,
		165, 406, 130, 32,
		IDC_INPUTCUTDATA,
		NULL,
		0
	},
	{
		CTRL_STATIC,  /* position before cut */
		WS_VISIBLE | ES_READONLY,
		450, 40, 110, 32,
		IDC_CURPOS,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC,  /* cut counter */
		WS_VISIBLE | ES_READONLY,
		450, 75, 110, 30,
		IDC_CUTCOUNTER,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC, /* input data description */
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		50, 410, 108, 28,
		IDC_INPUTDESC,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC, /* current mode */
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		390, 410, 90, 28,
		IDC_STATUSMODE,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC, /* STATUS BAR */
		WS_VISIBLE | ES_READONLY,
		140, 452, 340, 28,
		IDC_STATUSBAR,
		NULL,
		0,
		WS_EX_TRANSPARENT
	}

};

/********************************************************************/
static DLGTEMPLATE lableEditDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	8, 90, 350, 280,
	"lableEdit",
	0, 0,
	3, NULL,
	0
};

static CTRLDATA lableEditCtrlsInit[] =
{
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE|WS_BORDER,
		130, 40, 120, 30,
		IDC_LABLETOTALLEN,
		"",
		0
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE|WS_BORDER,
		130, 100, 120, 30,
		IDC_LABLELEN,
		"",
		0
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE|WS_BORDER,
		130, 160, 120, 30,
		IDC_UNUSEDLEN,
		"",
		0
	}
};

/********************************************************************/
static DLGTEMPLATE averageEditDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	8, 90, 350, 280,
	"averageEdit",
	0, 0,
	2, NULL,
	0
};

static CTRLDATA averageEditCtrlsInit[] =
{
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE|WS_BORDER,
		130, 40, 120, 30,
		IDC_HALFTOTALLEN,
		"",
		0
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE|WS_BORDER,
		130, 100, 120, 30,
		IDC_HALFNUMBER,
		"",
		0
	}
};


static void ResetUserProgramEdit(HWND hwnd,userProgramEditData *pProEdit,int func);
static void refreshUserProgramRunStatus(HWND hwnd,HDC hdc,userProgramRunStatus *pStatus,int type,int flag);
static void releaseSecondFunction(HWND hwnd);

/********************************************************************/
/* Global Functiones, may be called in other window
 */

/* refresh the current position
 *   hwnd: the IDC_CUTPOS edit box handler
 *   data: the pos
 */
void refreshCurrentPos(HWND hwnd,int data)
{
	float curPos;
	char curPosBuf[10];
	curPos = (float)data/100.0;
	sprintf(curPosBuf,"%7.2f",curPos);
	SetWindowText(hwnd,curPosBuf);

}

/* refresh the current pos by icons
 *   hdc:  draw DC
 *   data: the current postion
 */
void refreshCurrentPosByIcon(HDC hdc,int data)
{
	float curPos;
	char curPosBuf[10];
	int len,i,numX;
	curPos = (float)data/100.0;
	sprintf(curPosBuf,"%7.2f",curPos);
	len = strlen(curPosBuf);
	//printf("pos = %s len = %d \n",curPosBuf,len);
	for(i=0;i<len;i++){
		if(curPosBuf[i] >= '0' && (curPosBuf[i] <= '9')){
			numX = ((int)(curPosBuf[i] - 0x30) * 20);
		} else if(curPosBuf[i] == '.'){
			numX = 200;  /* dot */
		} else {
			numX = 220;  /* space */
		}

		RefreshBoxArea (hdc,&largeNumsIcon,CURPOS_X+i*20,CURPOS_Y,20,32,numX,0);
	}
}

/* read cut counter from file
 *   return: counter number
 */
static int loadCutCounter(void)
{
	int num;
	FILE *fp;
	if((fp=fopen("./user/cuts.cfg","r"))==NULL)
	{
		printf("loadCutCounter Error!\n");
		return 0;
	}

	while(!feof(fp)){
		fscanf(fp,"%d\n",&num);
	}
	fclose(fp);

	return(num);
}

/* save cut counter to file
 *   num: counter number
 */
static int saveCutCounter(int num)
{
	FILE *fp;
	if((fp=fopen("./user/cuts.cfg","w"))==NULL)
	{
		printf("saveCutCounter Error!\n");
		return 0;
	}

	fprintf(fp,"%d\n",num);
	fclose(fp);

	return(num);
}


/* refresh current position
 *  hDlg:   main window
 *  data:   current pos
 *  range:  update the trackbar?
 */
static void refreshCurrentPosArea(HWND hDlg,HDC hdc,int data,int range)
{
	refreshCurrentPosByIcon(hdc,data);
}

/*  refresh frontCutLen
 */
static void refreshForCutLenArea(HWND hwnd,userProgramRunStatus *pStatus,int curPos)
{
	int len ;
	int lastPos = pStatus->lastCutPos;
	if(curPos < lastPos){
		len = lastPos - curPos;
		pStatus->forCutLen = len;
	} else {
		pStatus->forCutLen = 0;
	}
	refreshCurrentPos(hwnd,pStatus->forCutLen);
	//printf("refreshCurPos:last=%d cur=%d len=%d\n",lastPos,curPos,pStatus->forCutLen);
}

/* refresh cut counter
 */
static void refreshCutCounter(HWND hwnd)
{
	char dispbuf[8];
	HWND hwndCutCnt = GetDlgItem(hwnd,IDC_CUTCOUNTER);
	memset(dispbuf,0,8);
	sprintf(dispbuf,"%6d",motorStatus.cutCounter);
	SetWindowText(hwndCutCnt,dispbuf);

	/* save value to file */
	saveCutCounter(motorStatus.cutCounter);
}


/* Get valid front limit
 */
static int getValidLimit(void)
{
	int frontLimit = currentuser->frontlimit;
	int whichLimit = proRunStatus.whichLimit;

	if(whichLimit)
		frontLimit= currentuser->middlelimit;

	return(frontLimit);
}

static void releaseSecondFunction(HWND hwnd)
{
	HDC hdc;
	if(proRunStatus.secondFunc == 1){
		proRunStatus.secondFunc = 0;
		hdc = BeginPaint(hwnd);
		refreshUserProgramRunStatus(hwnd,hdc,&proRunStatus,3,0);
		EndPaint(hwnd,hdc);
	}

}

/********************************************************************/
/*
 *   File Operations
 */

/* Save Program Data to file
 *   pathname :  file name and path
 *   fileHeader: file infomation, got from program loader
 *   pProEdit:   the program data
 *
 *   return:
 *     0: OK 1: Error
 *
 */
static int saveUserProgramToFile(char *pathname,
				 struct userProFileHeader_t *fileHeader,
				 userProgramEditData *pProEdit)
{
	int nInputCuts = pProEdit->nInputCutsNum;
	FILE *fp;
	char pathtmp[16] = "./default.pro";

	//printf("file name=%s\n",programCreate.filePathname);

	if(strlen(pathname) == 0){   /* file name is empty */
		pathname = pathtmp;
	}

	fp = fopen(pathname,"wb+");  /* open as binary, write */
	if(fp == NULL){
		perror("File open error.\n");
		return(-1);
	}

	/* file header */
	fwrite(fileHeader,sizeof(struct userProFileHeader_t),1,fp);
	/* program data */
	fwrite(&(pProEdit->userProCuts[1]),sizeof(int)*nInputCuts,1,fp);

	fclose(fp);

	return 1;
}

/* Read Program Data from file
 *   pathname :  file name and path
 *   fileHeader: file infomation, got from program loader
 *   pProEdit:   the program data
 *
 *   return:
 *     0: OK -1: Error
 *
 */
static int loadUserProgramFromFile(char *pathname,
				   struct userProFileHeader_t *fileHeader,
				   userProgramEditData *pProEdit)
{
	FILE *fp;
	int ret;
	int index=1;

	//printf("open filename=%s\n",programCreate.filePathname);
	fp = fopen(pathname,"r");  /* binary file, read */

	if(fp == NULL){
		perror("File open erreo.\n");
		return(-1);
	}

	/* read file header */
	ret = fread(fileHeader,sizeof(struct userProFileHeader_t),1,fp);

	//printf("%x %d %d \n",fileHeader->fileflags,fileHeader->usedTimes,fileHeader->first);
	while(!(feof(fp))){
		ret = fread(&(pProEdit->userProCuts[index]),4,1,fp);
		//printf("ret=%d \n",pProEdit->userProCuts[index]);
		index++;

	}
	pProEdit->userProCuts[0] = fileHeader->first;

	return(index);
}

/* update file header structure, NOT reallly save to file
 *
 */
static void UpdateFileHeader(struct fromProListToPro_t *pProCreate,
			     userProgramEditData *pProEdit,
			     int times)
{
#ifndef CONFIG_DEBUG_ON_PC
	char date[12];
	struct rtc_tm tm;

	int ret = getSysRtcTime(&tm);
	if (ret == 0){
		sprintf(date,"%02d-%02d-%02d",
			tm.tm_year-2000, tm.tm_mon+1, tm.tm_mday);
		memset(pProCreate->proHeader.latestDate,0,12);
		strncpy(pProCreate->proHeader.latestDate,date,strlen(date));
	} else {
		printf("get system RTC time error.\n");
	}
#endif

	pProCreate->proHeader.isnull = 0;
	pProCreate->proHeader.first = pProEdit->userProCuts[0];
	if(times){
		pProCreate->proHeader.usedTimes = pProCreate->proHeader.usedTimes +1;
	}

}


#if 1
static int dumpProgramData(userProgramEditData *pProEdit)
{
	int totalCuts = pProEdit->nInputCutsNum ;
	int i;

	for(i=0; i< totalCuts -1; i++){
		printf("%3d: %8d\n",i, pProEdit->userProCuts[i] & 0x00ffffff);
	}

	return 0;
}
#endif


/* update listbox with given data
 *   hwnd: main window handler
 *   pProEdit: prodram data edit structure
 *   type: 0: modify data @ pProEdit->currentCutsNum
 *         1: add data to the last
 */
static int UpdateProListWithData(HWND hwnd,userProgramEditData *pProEdit,int type)
{
	int data,flag;
	float fdata;
	int iconflag = 0;
	unsigned int currentCut = pProEdit->currentCutsNum;
	int unit = currentuser->unit;
	HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);
	char dispbuf[40];

	memset(dispbuf,0,40);

	if(currentCut < MAX_PROGRAM_CUTS){
		data = pProEdit->userProCuts[currentCut] & 0x00ffffff;
		fdata = (float)(data / 100.0);
		if(unit){
			sprintf(dispbuf," %03d      %06.2f ",currentCut+1,fdata);
		} else {
			sprintf(dispbuf," %03d      %07.2f ",currentCut+1,fdata);
		}
		flag = pProEdit->userProCuts[currentCut] & AIR_FLAG;
		if(!flag){              // flag = 0, ON
			iconflag |= AIR_ICON_BIT;
		}else{
			iconflag |= AIR_ICON_BIT | AIR_ICON_SW_BIT;  /* off icon */
		}

		if(type == 0){
			/* Fisrt delete current item */
			SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_DELETESTRING,currentCut,0);
			/* Insert new item to current place */
			SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_INSERTSTRING, (WPARAM)(currentCut | iconflag),
					   (LPARAM)dispbuf);
		} else {
			SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_ADDSTRING, (WPARAM)iconflag,(LPARAM)dispbuf);
		}
		/* SendMessage(hwndProCutList,LB_SETICONFLAG,currentCut,(DWORD)iconflag); */
		SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);

	} else {
		pProEdit->currentCutsNum = 0;
	}


	return 0;
}


/* update listbox with given data
 *   hwnd: main window handler
 *   pProEdit: prodram data edit structure
 *
 * When finished, highlight the item @ pProEdit->currentCutsNum
 *
 */
static int UpdateProListWithFileData(HWND hwnd,userProgramEditData *pProEdit)
{
	int data;
	float fdata;
	int flag=0;
	int iconflag = 0;
	int index = 0;
	int totalCuts = pProEdit->nInputCutsNum;
	HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);
	char dispbuf[40];
	int unit = currentuser->unit;

	memset(dispbuf,0,40);

	/* clear listbox */
	SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_RESETCONTENT,0,0);
	for(index=0;index<totalCuts;index++){
		iconflag = 0;
		data = pProEdit->userProCuts[index] & 0x00ffffff;
		fdata = (float)(data /100.0);
		flag = pProEdit->userProCuts[index] & AIR_FLAG;
		if(!flag)  /* flag == 0 -> ON */
			iconflag |= AIR_ICON_BIT;
		else
			iconflag |= AIR_ICON_BIT | AIR_ICON_SW_BIT;


		flag = pProEdit->userProCuts[index] & PUSHER_FLAG;
		if(flag){
			iconflag |= PUSHER_ICON_BIT;
		}
		flag = pProEdit->userProCuts[index] & AUTOCUT_FLAG;
		if(flag){
			iconflag |= AUTOCUT_ICON_BIT;
		}
		flag = pProEdit->userProCuts[index] & PRESS_FLAG;
		if(flag){
			iconflag |= PRESS_ICON_BIT;
		}

		if(unit){
			sprintf(dispbuf," %03d      %06.2f ",index+1,fdata);
		} else {
			sprintf(dispbuf," %03d      %07.2f ",index+1,fdata);
		}

		SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_ADDSTRING, (WPARAM)iconflag,(LPARAM)dispbuf);
		/* SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_SETICONFLAG, index,(DWORD)iconflag); */
	}
	//pProEdit->currentCutsNum = 0;
	SendMessage(hwndProCutList,LB_SETCURSEL,pProEdit->currentCutsNum,0);
	return 0;
}

/* update list item flag
 * use pProEdit->currentCutsNum as the item index
 */
static int UpdateProListItemFlags(HWND hwnd,userProgramEditData *pProEdit)
{

	int data,flag;
	int iconflag = 0;
	unsigned int currentCut = pProEdit->currentCutsNum;
	HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);

	if(currentCut < MAX_PROGRAM_CUTS){
		data = pProEdit->userProCuts[currentCut] & 0x00ffffff ; /* get data */

		flag = pProEdit->userProCuts[currentCut] & AIR_FLAG;
		if(!flag)  /* flag = 0, ON */
			iconflag |= AIR_ICON_BIT;
		else
			iconflag |= AIR_ICON_BIT | AIR_ICON_SW_BIT;

		flag = pProEdit->userProCuts[currentCut] & PUSHER_FLAG;
		if(flag){
			iconflag |= PUSHER_ICON_BIT;
		}

		flag = pProEdit->userProCuts[currentCut] & AUTOCUT_FLAG;
		if(flag){
			iconflag |= AUTOCUT_ICON_BIT;
		}

		flag = pProEdit->userProCuts[currentCut] & PRESS_FLAG;
		if(flag){
			iconflag |= PRESS_ICON_BIT;
		}

		SendMessage(hwndProCutList,LB_SETICONFLAG, currentCut, (DWORD)iconflag);
		/* SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0); */

	} else {
		pProEdit->currentCutsNum = 0;
	}

	return 0;
}

static int UpdateProListAllItemFlags(HWND hwnd,userProgramEditData *pProEdit)
{
	int index;
	int data, flag;
	int iconflag;
	int currentCut = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum;
	HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);

	for(index=0; index < totalCuts; index++){
		iconflag = 0;
		data = pProEdit->userProCuts[index] & 0x00ffffff; /* get data */

		flag = pProEdit->userProCuts[index] & AIR_FLAG;
		if(!flag)  /* flag = 0, ON */
			iconflag |= AIR_ICON_BIT;
		else
			iconflag |= AIR_ICON_BIT | AIR_ICON_SW_BIT;

		flag = pProEdit->userProCuts[index] & PUSHER_FLAG;
		if(flag){
			iconflag |= PUSHER_ICON_BIT;
		}

		flag = pProEdit->userProCuts[index] & AUTOCUT_FLAG;
		if(flag){
			iconflag |= AUTOCUT_ICON_BIT;
		}

		flag = pProEdit->userProCuts[index] & PRESS_FLAG;
		if(flag){
			iconflag |= PRESS_ICON_BIT;
		}

		/* refresh flag */
		SendMessage(hwndProCutList,LB_SETICONFLAG, (WPARAM)index, (DWORD)iconflag);
	}

	SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);

	return 0;
}

/* delete last 0.00 item when enter into AUTO/MANUAL mode
 */
static int deleteLastListItem(HWND hwndList,
			      userProgramEditData *pProEdit,
			      char *buffer)
{
	int index;
	int totalCuts = pProEdit->nInputCutsNum;   /* the last item */

	index = SendMessage(hwndList,LB_GETCOUNT,0,0);
	if(index == totalCuts){
		SendMessage(hwndList,LB_GETTEXT,index-1,(LPARAM)buffer);
		//printf("index = %d total=%d:%s \n",index,totalCuts,buffer);
		SendMessage(hwndList,LB_DELETESTRING,index-1,0);
	}

	return(index);
}

static int addLastListItem(HWND hwndList,userProgramEditData *pProEdit,char *buffer)
{
	int totalCuts = pProEdit->nInputCutsNum;   /* the last item */
	int index;
	index = SendMessage(hwndList,LB_GETCOUNT,0,0);
	//printf("add:index = %d total = %d :%s \n",index,totalCuts,buffer);
	if(index == totalCuts -1){
		SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)buffer);
	}
	return(index);
}


/*  检查输入的尺寸是否合法,合法的话进行转换,将字符串转成响应的数字
 *  pBuf : 输入的字符串
 *  返回值: 输入的字符串长度
 */
/* check the input data string. If it is legal, convert it to data.
 *   pBuf: the input string
 *   len:  the string length
 *   return:
 *         the data if valid
 */
int isInputCutsDataValid(char *pBuf,int len)
{

	int data,i,j;
	char strBuf[10];

	if(len > 8){
		return(-1);
	}

	//printf("input: %s  %d --> ",pBuf,len);
	i = 0;
	j = -1; // j = -1,no point
	while(i<len){
		if(pBuf[i] == '.'){
			j = i;  // Find the first point position
			break;
		}
		if(((pBuf[i]<'0') || (pBuf[i] > '9')) && (pBuf[i] != '.')){
			return(-1);
		}
		i++;
	}
	memset(strBuf,0,10);

	if(j>=0){
		strncpy(strBuf,pBuf,j);  /* int */
		strncat(strBuf,"00",2); // * 100
		if((pBuf[j+1] >= '0') && (pBuf[j+1] <= '9')){
			strBuf[j] = pBuf[j+1];
			if((pBuf[j+2] >= '0') && (pBuf[j+2] <= '9'))
				strBuf[j+1] = pBuf[j+2];
		}

	}else{  // j < 0, no point
		strncpy(strBuf,pBuf,len);
		strncat(strBuf,"00",2); // * 100
	}

	data = atoi(strBuf);
	//printf("strbuf = %s data = %d \n",strBuf,data);
	return(data);

}

/******************************************************************************/

#include "program_edit.c"

/******************************************************************************/



/* reset program edit status
 *
 */
static void ResetUserProgramEdit(HWND hwnd,userProgramEditData *pProEdit,int func)
{
	pProEdit->userEditStatus = 0;

}

static void exitTrainingMode(userProgramEditData *pProEdit)
{
	pProEdit->userEditStatus = 0;
}


/***********************************************************/
/****************        Local Functions        ********************/

/* callback functions
 */
static void editProgramNameCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd = GetMainWindowHandle(hwnd);
	HWND hwndInputCutData = GetDlgItem(mainHwnd,IDC_INPUTCUTDATA);
	char proNameBuf[40];
	int  proNameLen=0;

	switch(nc){
	case EN_SETFOCUS:
		/* if we got the focus, change IME to chinese */
		SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)1);
		if(isIMEOpenInMotor == 0){
			ShowWindow(myIMEWindow,SW_SHOW);
			isIMEOpenInMotor =1;
		}
		break;

	case EN_ENTER: /* done */
		programEdit.isProNameNull = 0; /* clear flag */
		proNameLen = GetWindowTextLength(hwnd);
		if(proNameLen == 0){
			strcpy(proNameBuf,"default");
		}else{
			GetWindowText(hwnd,proNameBuf,proNameLen);
		}

		memset(programCreate.proHeader.introduction,0,sizeof(programCreate.proHeader.introduction));
		strncpy(programCreate.proHeader.introduction,proNameBuf,proNameLen);

		/* updat file header */
		UpdateFileHeader(&programCreate,&programEdit,0);

		releaseSecondFunction(mainHwnd);
		SetFocusChild(hwndInputCutData);
		break;
	}
}

static void editInputCutDataCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	char cutDataBuf[10];
	int  cutDataLen=0;
	int ret;
	userProgramEditData *pProEdit = &programEdit;
	int status = pProEdit ->userEditStatus;
	mainHwnd = GetMainWindowHandle(hwnd);

	switch(nc){
	case EN_SETFOCUS:
		/* got focus, change IME to number input */
		SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);
		if(isIMEOpenInMotor == 1){
			ShowWindow(myIMEWindow,SW_HIDE);
			isIMEOpenInMotor = 0;
		}

		break;
	case EN_CHANGE: /* number key is pressed */
		cutDataLen = GetWindowTextLength(hwnd);
		if(cutDataLen > 7){  /* overflow */
			SetFocusChild(hwnd);
			SetWindowText(hwnd,"");
		}
		break;
	case EN_ENTER:
		if(proRunStatus.runStatus){
			break;
		}
		// 先检查输入数据的有效性
		// 将有效数据写入userProCuts[currentCutsNum],currentCutsNum递增,nInputCutsNum递增
		// 更新列表框数据,当前输入下移到下一刀位置
		cutDataLen = GetWindowTextLength(hwnd); // 获得输入的字符
		GetWindowText(hwnd,cutDataBuf,cutDataLen);
		//memset(programEdit.inputbuf,0,10);
		//strncpy(programEdit.inputbuf,cutDataBuf,cutDataLen);
		//printf("%s len=%d\n",cutDataBuf,cutDataLen);
		ret=isInputCutsDataValid(cutDataBuf,cutDataLen);        // 判断输入是否有效
		if(ret >= 0 ){                                  // 输入有效,返回转换后的数据
			switch(status){
			case 0: // 正常编辑
			case 7: // 示教
				printf("editOneCutData %d \n",ret);
				editOneCutData(mainHwnd,pProEdit,ret);
				break;
			case 1: // 标签编辑
				//userProgramLableEdit(mainHwnd,pProEdit,ret);
				break;
			case 2: // 等分编辑
				//userProgramHalfEdit(mainHwnd,pProEdit,ret);
				break;
			case 3: // 修改数据,修改完退出
				//modifyOneCutData(mainHwnd,pProEdit,ret);
				break;
			case 4: // 插入数据
				//insertOneCutData(mainHwnd,pProEdit,ret);
				break;
			case 6: // 微调数据
				justAllCutDatas(mainHwnd,pProEdit,ret);
				break;
			case 8: // 加
				compareAddDecInput(mainHwnd,pProEdit,ret,0);
				break;
			case 9: // 减
				compareAddDecInput(mainHwnd,pProEdit,ret,1);
				break;
			}
		} else {        // 数据非法
			MessageBox(mainHwnd,userpro_help[*curSysLang].msg_cutdata_overflow,
				   userpro_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		}
		memset(cutDataBuf,0,sizeof(cutDataBuf));
		SetWindowText(hwnd,cutDataBuf);

		break;
	}
}

/*  callback function of the listbox
 *  currently it is NOT used
 */
static void listProgramCutsCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	mainHwnd = GetMainWindowHandle(hwnd);
	switch(nc){
	case LBN_SETFOCUS:      /* get the focus */

		break;
	case LBN_SELCHANGE:     /* current select item changed */
		break;
	case LBN_ENTER:         /* enter key is pressed */

		break;
	}
}

/*********************** 机器运行控制函数 ************************/
/*   机器步进,  dir = 0 前进
 *              dir = 1 后退
 *              step 步进量  单位0.01mm
 */
void motorRunStep(int dir,int step)
{
	int data = step;
	char cmdSendBuf[13];
	memset(cmdSendBuf,0,12);
	cmdData.flag = 'F';
	cmdData.data = data;
	switch(dir){
	case MOTOR_FORWARD:             // 前进
		cmdData.status = 0x30;
		break;
	case MOTOR_BACKWARD:            // 后退
		cmdData.status = 0x31;
		break;
	case MOTOR_FORWARD_HS:          // 前进
		cmdData.status = 0x32;
		break;
	case MOTOR_BACKWARD_HS:         // 后退
		cmdData.status = 0x33;
		break;

	}
	setCmdFrame(&cmdData,cmdSendBuf);       // 组帧 step*0.01毫米
	sendCommand(cmdSendBuf);                // Send
}

/*  处理状态帧,收到状态帧之后首先调用该函数处理
 *  功能: 刷新当前切刀和推纸器状态
 *  返回: 切刀动作是否完成 1:完成 0:没有
 */
static int getDevStatus(userProgramRunStatus *pStatus,int status)
{
	int ret = 0;
	int isStatusChanged = 0;
	int cutOldStatus = pStatus->cutStatus;
	int pusherOldStatus = pStatus->pusherStatus;

	if(status & CUT_STATUS_MASK){
		pStatus->cutStatus = 1; // 切刀切下
		pStatus->lastCutPos = motorStatus.currentCutPos;        // 更新上次切下位置
		pStatus->forCutLen = 0; // 置位刀前尺寸
	} else {
		pStatus->cutStatus = 0;
	}

	if(status & PUSHER_STATUS_MASK){
		pStatus->pusherStatus = 1;
	} else {
		pStatus->pusherStatus = 0;
	}

	if(status & WHICH_LIMIT_MASK){
		pStatus->whichLimit = 1;        // 中极限有效
	} else {
		pStatus->whichLimit = 0;        // 前极限有效
	}

	if(status & MACHINE_STOP_MASK){
		pStatus->machineStop = 0;       // 该bit为0标志停止,1表示移动中
		pStatus->runStop = 1;   // 运行
	} else {
		pStatus->machineStop = 1;
		pStatus->runStop = 0;   // 停止
	}

	if(cutOldStatus != pStatus->cutStatus){
		pStatus->cutActTimes++;
		isStatusChanged = 1;
	}
	if(pusherOldStatus != pStatus->pusherStatus){
		pStatus->pusherActTimes++;
		isStatusChanged = 1;
	}
	if(isStatusChanged){
		//printf("getStatus 1:%d \n",pStatus->cutPusherActTimes );
		if((pStatus->pusherStatus == 1) &&(pStatus->cutStatus == 1)
		   && (pStatus->cutPusherActTimes == 0)){
			pStatus->cutPusherActTimes = 1;
		}
		//printf("getStatus 2 :%d \n",pStatus->cutPusherActTimes );
		if((pStatus->pusherStatus == 0) &&(pStatus->cutStatus == 0)
		   && (pStatus->cutPusherActTimes == 1)){
			pStatus->cutPusherActTimes = 2;
			if(pStatus->cutActTimes >= 2){          // 切刀裁切动作完成,刀前尺寸清零
								//pStatus->forCutLen = 0;
								// 裁切动作完成
				ret = 1;
			}
		}
		//printf("getStatus 3 :%d \n",pStatus->cutPusherActTimes );
	}

	return(ret);

}


/*   重置运行状态,即将发送一条运行指令前调用
 */
static void resetMotorRunStatus(userProgramRunStatus *pStatus)
{
	pStatus->isOneCutDone = 0;      // 更新状态
	pStatus->cutActTimes = 0;
	pStatus->pusherActTimes = 0;
	pStatus->cutPusherActTimes = 0;
}

/*   发送一刀运行指令
 *   返回       0  该帧为正常帧
 *              1  推纸帧
 */
static int motorRunOneCut(userProgramEditData *pProEdit,userProgramRunStatus *pStatus)
{
	int ret = 0;
	char cmdSendBuf[13];
	int currentCut = pProEdit->currentCutsNum;
	int data = pProEdit->userProCuts[currentCut];   // 取出数据
	int runStatus = 0;      // So, 在自切模式下 runStatus = 1 否则 = 0
	int unit = currentuser->unit;
	int tmp = data & 0x00ffffff;

	if((tmp < currentuser->frontlimit) || (tmp > currentuser->backlimit)){
		return 0;
	}

	if(pStatus->runStatus == 3){
		runStatus = 1;
	}
	if(data & PUSHER_FLAG){ // 数据包含推纸信息,发送推纸帧
		ret = 1;
		runStatus |= 0x02;              // Bit 1
	}
	//if((data & AUTOCUT_FLAG) && (pStatus->runStatus == 3)){// 只有在自动切刀模式才发出该标志
	if(data & AUTOCUT_FLAG){
		runStatus |= 0x04;              // Bit2
	}
	if(data & AIR_FLAG){    //
		runStatus |= 0x08;              // Bit3
	}
	if(data & PRESS_FLAG){
		runStatus |= 0x10;              // Bit4
	}
	data = data & 0x00ffffff;       // 取出有效数据
	if(unit){
		data = inch_to_mm(data);
	}
	cmdData.flag = 'G';
	cmdData.data = data;
	cmdData.status = 0xE0 | runStatus;      // Bit 5..7: default = 1
	if((runStatus & 0x04)){ // Should not be this,But the lower machine is Outand cann't be changed
				//cmdData.status = 0x36;
		cmdData.status |= 0x06; // can not change AIR_FLAG
	}
	setCmdFrame(&cmdData,cmdSendBuf);       // 组帧
	sendCommand(cmdSendBuf);                // Send
	//printf("motorRunOneCut %d %s\n",data,cmdSendBuf);
	return(ret);

}
/*   自动程序运行控制,检测切刀和推纸器动作变化作为主要控制标志,下位机发送的停止信号
 *   仅用于推纸刀的运行
 */
static int motorRunAuto(HWND hwnd,userProgramEditData *pProEdit,userProgramRunStatus *pStatus)
{
	HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);
	int currentCut = pProEdit->currentCutsNum;
	int totalCut = pProEdit->nInputCutsNum;
	int curCutPos = motorStatus.currentCutPos;      // 当前切刀位置
	int destCut = pStatus->destCutNum;
	int ret;
	int data = pProEdit->userProCuts[currentCut];   // 取出数据
	int curCutPushFlag = 0;

	if(data & PUSHER_FLAG){ // 数据包含推纸信息,发送推纸帧
		curCutPushFlag = 1;
	}
/*
  printf("Auto Run !currentCut = %d times = %d %d %d \n",currentCut,pStatus->cutActTimes,
  pStatus->pusherActTimes,pStatus->cutPusherActTimes);
  printf("Auto Run !machineStop = %d curCutPushFlag = %d \n",pStatus->machineStop,curCutPushFlag);
  printf("DstCutPos = %d curCutPos = %d\n",pStatus->lastCutPos,curCutPos);
*/
	// 判断一次动作是否完成,完成则发送下一刀指令,否则直接返回
	// 如果该刀为推纸刀,则不用等待状态信息,收到当前切刀位置与发送数据相等时运行到下一刀
//    if(((pStatus->cutActTimes == 2) && (pStatus->pusherActTimes >= 2)) ||
	if(((pStatus->cutPusherActTimes == 2) && (pStatus->cutActTimes >= 2))||  // 防止运行前两者都在下方
	   ((pStatus->machineStop == 1) && (curCutPushFlag == 1) && (curCutPos == pStatus->curDestPos))){
		pStatus->isOneCutDone = 1;
		// 处理刀前尺寸
		pStatus->lastCutPos = pStatus->curDestPos;      // 更新参考位置为上回目标位置
		pStatus->forCutLen = 0;                 // clear

		//pProEdit->currentCutsNum++;                   // 自动运行到下一刀
		//if(pProEdit->currentCutsNum == totalCut-1){   // 倒数第一刀
		//     pProEdit->currentCutsNum = 0;
		//}
		pStatus->destCutNum++;
		if(pStatus->destCutNum == totalCut - 1){        // 倒数第一刀
			pStatus->destCutNum = 0;
		}

		currentCut = pProEdit->currentCutsNum;
		destCut = pStatus->destCutNum;
		//SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);         // 高亮当前运行项
	}

	if(pStatus->isOneCutDone){      // 前一刀已经完成
		currentCut = pProEdit->currentCutsNum;
		destCut = pStatus->destCutNum;
		if(destCut == 0){       // 是最后一刀,则重头开始
			pProEdit->currentCutsNum = 0;
			SendMessage(hwndProCutList,LB_SETCURSEL,0,0);    // 高亮当前运行项
		}

		currentCut = pProEdit->currentCutsNum;
		//SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);         // 高亮当前运行项
		if(destCut == (currentCut + 1) ){
			SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);  // Reset Before the next
			SendMessage(hwnd,MSG_KEYDOWN,SCANCODE_CURSORBLOCKDOWN,0); //  Move to next
			pProEdit->currentCutsNum++;
		}
		currentCut = pProEdit->currentCutsNum;
		pStatus->curDestPos = pProEdit->userProCuts[currentCut] & 0x00ffffff;   // 更新保存当前目的位置
		ret = motorRunOneCut(pProEdit,pStatus); // 继续运行
		pStatus->machineStop = 0;               // 移动中
		resetMotorRunStatus(pStatus);           // 更新状态,times,isOneCutDone清零
	}
	//printf("Enter Auto Run !currentCut = %d \n",pProEdit->currentCutsNum);
	//currentCut = pProEdit->currentCutsNum;
	//SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);         // 高亮当前运行项

	return(pProEdit->currentCutsNum);

}

/*   半自动程序运行控制
 */
static int motorRunHalfAuto(HWND hwnd,userProgramEditData *pProEdit,userProgramRunStatus *pStatus)
{
	int currentCut;
	int totalCut = pProEdit->nInputCutsNum;
	HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);

	currentCut = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);// 得到当前列表框选项
	if(currentCut >= totalCut-1){   // 是最后一刀,数据为0
		return(0);
	}
	pProEdit->currentCutsNum = currentCut;
	motorRunOneCut(pProEdit,pStatus);       // 继续运行
	resetMotorRunStatus(pStatus);           // 更新状态

	return(currentCut);

}
/*   手动程序运行控制
 */
static int motorRunMannual(HWND hwnd,userProgramEditData *pProEdit,userProgramRunStatus *pStatus)
{
	char dataBuf[10];
	char cmdSendBuf[13];
	int data,len;
	int unit = currentuser->unit;
	int frontLimit = getValidLimit();
	HWND  hwndInputCutData = GetDlgItem(hwnd,IDC_INPUTCUTDATA);

	len = GetWindowTextLength(hwndInputCutData);    // 获得输入的字符
	GetWindowText(hwndInputCutData,dataBuf,len);
	data = isInputCutsDataValid(dataBuf,len);       // 判断输入是否有效

	switch(pStatus->mannualFlag){
	case 1: // ADD
		data = motorStatus.currentCutPos + data;
		break;
	case 2: // SUB
		data = motorStatus.currentCutPos - data;
		break;
	default:
		break;
	}
	if(pStatus->mannualFlag){
		pStatus->mannualFlag = 0;       // Clear
		// 将结果更新到输入框
		SetWindowText(hwndInputCutData,"");     // clear
		refreshCurrentPos(hwndInputCutData,data);
	}

	//printf("data = %d \n",data);
	if((data >= frontLimit) && (data <= currentuser->backlimit)){   // 输入有效,在极限范围内
		// 更新刀前尺寸
		pStatus->curDestPos = data;
		pStatus->forCutLen = 0;         // What should i do here?

		cmdData.flag = 'G';
		cmdData.status = 0x33;
		if(unit){
			data = inch_to_mm(data);
		}
		cmdData.data = data;
		setCmdFrame(&cmdData,cmdSendBuf);       // 组帧
		sendCommand(cmdSendBuf);                // Send
	} else {        // 数据非法
		MessageBox(hwnd,userpro_help[*curSysLang].msg_cutdata_overflow,
			   userpro_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		SetWindowText(hwndInputCutData,"");     // clear

	}

	return(data);

}

static void motorRunProcess(HWND hDlg,userProgramEditData *pProEdit,userProgramRunStatus *pStatus)
{
	int runStatus = pStatus->runStatus;

	switch(runStatus){
	case 1:         // 自动运行
		//case 3:
		motorRunAuto(hDlg,pProEdit,pStatus);
		break;
	case 0:         // 编辑模式下的示教编辑
		//printf("Trainning Run...\n");
		motorRunMannual(hDlg,pProEdit,pStatus);
		break;
	case 2:         // 手动运行
		//printf("Enter Mannual Run...\n");
		motorRunMannual(hDlg,pProEdit,pStatus);
		break;
	}
}

/* 列表初始化函数,读出文件内容
 * 如果文件为空,则新建程序文件,初始化文件头
 */
static int InitProCutsList(HWND hwnd,
			   userProgramEditData *pProEdit,
			   struct fromProListToPro_t *pProCreate)
{
	UpdateProListWithFileData(hwnd,pProEdit);
	// 初始化列表头(序号,尺寸)
	//SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_ADDSTRING,0,(LPARAM)message_cutslist_header);

	return 0;
}

/* check F1 - F6 is valid
 */
static BOOL isFunctionButtonValid(userProgramRunStatus *pStatus)
{
	if(pStatus->runStop > 0){   /* Not valid if running */
		return FALSE;
	}
	if(pStatus->runStatus > 0){
		return FALSE;
	}

	return TRUE;
}

static int loadUserProBmpFiles(void)
{
	int langtype = *curSysLang;
	switch(langtype){
	case 1: /* english */
		if (LoadBitmap (HDC_SCREEN, &userProgramEditBkgnd, "./res/userProgramRun1.jpg"))
			return 1;
		if (LoadBitmap (HDC_SCREEN, &userProgramEditButton, "./res/userProEdit_en.jpg"))
			return 1;
		break;
	case 0:
	default:
		if (LoadBitmap (HDC_SCREEN, &userProgramEditBkgnd, "./res/userProgramRun0.jpg"))
			return 1;
		if (LoadBitmap (HDC_SCREEN, &userProgramEditButton, "./res/userProEdit.jpg"))
			return 1;
		break;
	}

	/* load button icons
	 */
	if (LoadBitmap (HDC_SCREEN, &btBmpEditFn[0], "./res/buttonEditF1.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpEditFn[1], "./res/buttonEditF2.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpEditFn[2], "./res/buttonEditF3.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpEditFn[3], "./res/buttonEditF4.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpEditFn[4], "./res/buttonEditF5.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpEditFn[5], "./res/buttonEditF6.bmp"))
		return 1;

	if (LoadBitmap (HDC_SCREEN, &btBmpCommonLeft[0], "./res/buttonRegister.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpCommonLeft[1], "./res/buttonProList.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpCommonLeft[2], "./res/buttonModeEdit.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpCommonLeft[3], "./res/buttonModeAuto.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpCommonLeft[4], "./res/buttonModeManual.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpCommonLeft[5], "./res/buttonModeTraining.bmp"))
		return 1;

	if (LoadBitmap (HDC_SCREEN, &btBmpEnter, "./res/buttonEnter.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpClear, "./res/buttonClear.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpRun, "./res/buttonRun.bmp"))
		return 1;

	/* cut position and cut counter icons */
	if (LoadBitmap (HDC_SCREEN, &cutPosIcon, "./res/cutfrontpos.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &cutCounterIcon, "./res/cutcounter.bmp"))
		return 1;



	if (LoadBitmap (HDC_SCREEN, &largeNumsIcon, "./res/0_9.bmp"))
		return 1;
	/* background for label and average edit window */
	if (LoadBitmap (HDC_SCREEN, &lableEditBkgnd, "./res/configItemBkgnd.jpg"))
		return 1;

	if (LoadBitmap (HDC_SCREEN, &flagicon[0], "./res/flag01.bmp"))  /* Air flag*/
		return 1;
	if (LoadBitmap (HDC_SCREEN, &flagicon[1], "./res/flag02.bmp"))  /* Pusher flag */
		return 1;
	if (LoadBitmap (HDC_SCREEN, &flagicon[2], "./res/flag03.bmp"))  /* Press flag */
		return 1;
	if (LoadBitmap (HDC_SCREEN, &flagicon[3], "./res/flag04.bmp"))  /* Autocut flag */
		return 1;
	if (LoadBitmap (HDC_SCREEN, &flagicon[4], "./res/flag05.bmp"))  /* No Air flag */
		return 1;

	return(0);
}


static int unloadUserProBmpFiles(void)
{
	int i = 0;

	UnloadBitmap(&userProgramEditBkgnd);
	UnloadBitmap(&userProgramEditButton);

	for(i = 0; i < 6; i++)
		UnloadBitmap(&btBmpEditFn[i]);

	for(i = 0; i < 6; i++)
		UnloadBitmap(&btBmpCommonLeft[i]);

	UnloadBitmap (&btBmpEnter);
	UnloadBitmap (&btBmpClear);
	UnloadBitmap (&btBmpRun);

	UnloadBitmap (&cutPosIcon);
	UnloadBitmap (&cutCounterIcon);

	UnloadBitmap (&largeNumsIcon);

	UnloadBitmap (&lableEditBkgnd);

	for(i = 0; i < 5; i++)
		UnloadBitmap(&flagicon[i]);

	return 0;
}


/* update program Data setp
 * called from listbox CUR_CHANGED message
 */
static void updateProgramDataStep(HWND mainhwnd, userProgramEditData *pProEdit)
{
	char stepBuf[6];

	sprintf(stepBuf, "%4d", pProEdit->currentCutsNum);
	SetDlgItemText(mainhwnd,IDC_PROGRAMDATASTEP,stepBuf);
}

/* update current mode */
static void updateCurrentModeStatus(HWND mainhwnd, userProgramRunStatus *pStatus)
{

	SetDlgItemText(mainhwnd,IDC_STATUSMODE, strMachineMode[pStatus->runStatus]);

}


/* refresh all run status icons
 * type:       which part of icon
 * setfocus:   should we reset the focus
 */
static void refreshUserProgramRunStatus(HWND hwnd,HDC hdc,userProgramRunStatus *pStatus,int type,int setfocus)
{
	int currentCut = programEdit.currentCutsNum;
	HWND  hwndInputCutData = GetDlgItem(hwnd,IDC_INPUTCUTDATA);
	HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);
	HWND  hwndCutCnt = GetDlgItem(hwnd,IDC_CUTCOUNTER);
	int status;

	switch(type){
	case 0: /* edit/auto/maunal handle */
		status = pStatus->runStatus;
		switch(status){
		case 0: /* edit */
			ShowWindow(hwndInputCutData,SW_SHOW);
			ShowWindow(hwndCutCnt,SW_HIDE);
			if(setfocus){
				SetFocusChild(hwndInputCutData);
				SetWindowText(hwndInputCutData,"");     //clear
			}
			break;
		case 1: /* Autorun */
			ShowWindow(hwndInputCutData,SW_HIDE);
			ShowWindow(hwndCutCnt,SW_SHOW);
			if(setfocus){
				SetFocusChild(hwndProCutList);
				SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);
			}

			break;
		case 2: /* Manual run */
			ShowWindow(hwndInputCutData,SW_SHOW);
			ShowWindow(hwndCutCnt,SW_SHOW);
			if(setfocus){
				SetFocusChild(hwndInputCutData);
				SetWindowText(hwndInputCutData,"");     //clear
			}
			break;
		}

		break;
	case 1: /* run/stop flag */
		status = pStatus->runStop;
		switch(status){
		case 0: /* stop */
			break;
		case 1: /* run */
			break;
		}
		break;
	case 2: /* pusher flag area */
		if(pStatus->pusher){

		} else {
		}
		break;
	case 3: /* shift area */
		if(pStatus->secondFunc){
		} else {
		}
		break;
	case 4: /* machine status area */
		//printf("refreshUserProgramRunStatus...%d %d\n",pStatus->cutStatus,pStatus->pusherStatus);
		if(pStatus->cutStatus){ /* cut down */
		} else {                /* cut up */
		}

		if(pStatus->pusherStatus){ /* pusher down */
		} else {                   /* pusher up */
		}
		break;
	}
}

/* init background
 *
 */
static void initUserproEditDlgBkgnd(HWND hwnd,HDC hdc,userProgramRunStatus *pStatus)
{
	int status = pStatus->runStatus;
	int curPos = motorStatus.currentCutPos;

	FillBoxWithBitmap (hdc, 0, 0, 0, 0, &userProgramEditBkgnd); /* main background */

	FillBoxWithBitmap (hdc, 560, 40, 0, 0, &cutPosIcon);     /* cut front pos */
	FillBoxWithBitmap (hdc, 560, 75, 0, 0, &cutCounterIcon); /* cut counter   */

	/* handle mode-specified fair here */
	switch(status){
	case 0: /* Edit */
		break;
	case 1: /* AutoRun */
		break;
	case 2: /* Manual Run */
		break;
	default:
		break;
	}

	/* the current pos */
	refreshCurrentPosByIcon(hdc,curPos);

	refreshUserProgramRunStatus(hwnd,hdc,pStatus,0,0);
	refreshUserProgramRunStatus(hwnd,hdc,pStatus,1,0);
	getDevStatus(&proRunStatus,motorStatus.devStatus);  /* get machine status */
	refreshUserProgramRunStatus(hwnd,hdc,pStatus,4,0);
}


static int initUserProEditDlg(HWND hwnd,
			      userProgramEditData *pProEdit,
			      struct fromProListToPro_t *pProCreate)
{
	int ret;
	int id = pProCreate->proHeader.id;
	char numBuf[4];

	pProEdit->userEditStatus = 0;   /* default edit state */
	pProEdit->currentCutsNum = 0;
	pProEdit->inputbuf[0] = '\0';
	pProEdit->justEdit.action = 0;  /* clear just flag */

	proRunStatus.runStatus = 0;     /* edit mode */
	proRunStatus.runStop = 0;       /* machine is stopped */
	proRunStatus.pusher = 0;        /* pusher: no action */
	proRunStatus.secondFunc = 0;    /* clear shift function */
	proRunStatus.whichLimit = 0;    /* limit is set to front limit */
	proRunStatus.machineStop = 1;   /* machine is stopped */

	proRunStatus.curDestPos = 0;
	proRunStatus.lastCutPos = 0;
	proRunStatus.forCutLen = 0;
	proRunStatus.mannualFlag = 0;

	sprintf(numBuf,"%s %2d  %s",strProgramDesc, id, strProgramStep);
	SetDlgItemText(hwnd,IDC_PROGRAMNUM,numBuf); /* program number */

	SetDlgItemText(hwnd,IDC_CURPOSDESC, strCurrentPosDesc); /* current pos */

	updateProgramDataStep(hwnd, pProEdit);

	SetDlgItemText(hwnd,IDC_INPUTDESC, strInputDataDesc); /* input data */

	updateCurrentModeStatus(hwnd, &proRunStatus);



	if(pProCreate->isFileNew){              /* file is empty -> new file */
		pProEdit->nInputCutsNum = 1;    /* only 0.0 is displayed */
		pProEdit->currentCutsNum = 0;
		pProEdit->isProNameNull = 1;    /* program name is NULL */
		/* clear all data in this file */
		memset(pProEdit->userProCuts, 0, sizeof(pProEdit->userProCuts));

	} else {
		/* load program data */
		ret = loadUserProgramFromFile(pProCreate->filePathname,&(pProCreate->proHeader),pProEdit);
		pProEdit->nInputCutsNum = ret - 2;
		if(strlen(pProCreate->proHeader.introduction) == 0){
			pProEdit->isProNameNull = 1;
		} else{
			pProEdit->isProNameNull = 0;
			SetDlgItemText(hwnd,IDC_PROGRAMNAME,pProCreate->proHeader.introduction);
		}
	}

	/* since we get the data, so init the data list */
	InitProCutsList(hwnd,pProEdit,pProCreate);

	return 0;
}

static char lastListItem[40];
static int mannualEditFlag = 0;   /* the edit status flag in mannual mode */
static int autoCutChangeFlag = 0;

/* static char testbuffer[13]; */

static int InitUserProDialogBoxProc (HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND  hWnd;
	int currentSel;
	int curPos;
	int ret;
	char cmdRecvBuf[13];
	char timeBuff[40];
	int frontlimit;
	int unit = currentuser->unit;

	HWND  hwndProName = GetDlgItem(hDlg,IDC_PROGRAMNAME);
	HWND  hwndInputCutData = GetDlgItem(hDlg,IDC_INPUTCUTDATA);
	HWND  hwndProCutList = GetDlgItem(hDlg,IDL_PROGRAMCUTSLIST);
	HWND  hwndCurrentPos = GetDlgItem(hDlg,IDC_CURPOS);
	HWND  hwndCutCounter = GetDlgItem(hDlg,IDC_CUTCOUNTER);
	HWND  hwndClock = GetDlgItem(hDlg,IDC_TIME);

	HWND  hwndButtonEditFn[6], hwndButtonCommon[6];
	HWND  hwndButtonEnter, hwndButtonClear, hwndButtonRun;

	switch (message) {
	case MSG_INITDIALOG:
		SetNotificationCallback(hwndInputCutData, editInputCutDataCallback);
		SetNotificationCallback(hwndProName, editProgramNameCallback);
		/* SetNotificationCallback(hwndProCutList, listProgramCutsCallback); */

		if(unit){
			SetDlgItemText(hDlg,IDC_PROGRAMCUTSLISTHEAD,
				       userpro_help[*curSysLang].msg_cutslist_header_inch);
		} else {
			SetDlgItemText(hDlg,IDC_PROGRAMCUTSLISTHEAD,
				       userpro_help[*curSysLang].msg_cutslist_header);
		}

		hwndButtonEditFn[0] =
			CreateWindow (CTRL_BUTTON,
				      " F1 ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE,
				      IDC_BUTTON_EDIT_F1,
				      594, 120, 42, 42, hDlg, (DWORD)(&btBmpEditFn[0]));


		/* set window fonts */
		SetWindowFont(hwndInputCutData,userFont24);
		SetWindowFont(hwndProCutList,userFont24);
		/* SetWindowFont(hwndCurrentPos,userFont24); */
		SetWindowFont(hwndCutCounter,userFont24);

		/* set font color */
		SetWindowElementColorEx(hwndInputCutData,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 0, 0)); //Red
		SetWindowElementColorEx(hwndCurrentPos,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 255, 0)); //Yellow
		SetWindowElementColorEx(hwndClock,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 255, 255)); //White
		SetWindowElementColorEx(hwndCutCounter,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 0, 0)); //Red

		userProExitFlag = 0;

		SetTimer(hDlg,IDC_TIMER,50); /* timer set to 100ms */

		/* init the window, display data */
		initUserProEditDlg(hDlg,&programEdit,&programCreate);
		curPos = motorStatus.currentCutPos;

		/* display the current pos */
		refreshCurrentPos(hwndCurrentPos,proRunStatus.forCutLen);

		motorStatus.cutCounter = loadCutCounter();
		refreshCutCounter(hDlg);
		ShowWindow(hwndCutCounter,SW_HIDE);

		/* update and save the file used date and used times */
		UpdateFileHeader(&programCreate,&programEdit,1);
		saveUserProgramToFile(programCreate.filePathname,&(programCreate.proHeader),&programEdit);

		/* init listbox icon structure */
		myicons.dwIcons[0] = (DWORD)&flagicon[0];
		myicons.dwIcons[1] = (DWORD)&flagicon[1];
		myicons.dwIcons[2] = (DWORD)&flagicon[2];
		myicons.dwIcons[3] = (DWORD)&flagicon[3];
		myicons.dwIcons[4] = (DWORD)&flagicon[4];
		myicons.dwIcons[5] = (DWORD)&flagicon[1];
		myicons.dwIcons[6] = (DWORD)&flagicon[2];
		myicons.dwIcons[7] = (DWORD)&flagicon[3];
		SendMessage(hwndProCutList,LB_SETICONDATA, 0, (DWORD)&myicons);

		/* create button controls */

		hwndButtonEditFn[1] =
			CreateWindow (CTRL_BUTTON,
				      " F2 ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE,
				      IDC_BUTTON_EDIT_F2,
				      594, 166, 42, 42, hDlg, (DWORD)(&btBmpEditFn[1]));
		hwndButtonEditFn[2] =
			CreateWindow (CTRL_BUTTON,
				      " F3 ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE,
				      IDC_BUTTON_EDIT_F3,
				      594, 212, 42, 42, hDlg, (DWORD)(&btBmpEditFn[2]));
		hwndButtonEditFn[3] =
			CreateWindow (CTRL_BUTTON,
				      " F4 ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE,
				      IDC_BUTTON_EDIT_F4,
				      594, 258, 42, 42, hDlg, (DWORD)(&btBmpEditFn[3]));
		hwndButtonEditFn[4] =
			CreateWindow (CTRL_BUTTON,
				      " F5 ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE,
				      IDC_BUTTON_EDIT_F5,
				      594, 304, 42, 42, hDlg, (DWORD)(&btBmpEditFn[4]));
		hwndButtonEditFn[5] =
			CreateWindow (CTRL_BUTTON,
				      " F6 ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE,
				      IDC_BUTTON_EDIT_F6,
				      594, 350, 42, 42, hDlg, (DWORD)(&btBmpEditFn[5]));

		/* left side buttons */
		hwndButtonCommon[0] =
			CreateWindow (CTRL_BUTTON,
				      " User ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE,
				      IDC_BUTTON_REGISTER,
				      04, 120, 42, 42, hDlg, (DWORD)(&btBmpCommonLeft[0]));
		hwndButtonCommon[1] =
			CreateWindow (CTRL_BUTTON,
				      " ProLst ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE,
				      IDC_BUTTON_PROLIST,
				      04, 166, 42, 42, hDlg, (DWORD)(&btBmpCommonLeft[1]));
		hwndButtonCommon[2] =
			CreateWindow (CTRL_BUTTON,
				      " Edit ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE,
				      IDC_BUTTON_MD_EDIT,
				      04, 212, 42, 42, hDlg, (DWORD)(&btBmpCommonLeft[2]));
		hwndButtonCommon[3] =
			CreateWindow (CTRL_BUTTON,
				      " Auto ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE,
				      IDC_BUTTON_MD_AUTO,
				      04, 258, 42, 42, hDlg, (DWORD)(&btBmpCommonLeft[3]));
		hwndButtonCommon[4] =
			CreateWindow (CTRL_BUTTON,
				      " Manual ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE,
				      IDC_BUTTON_MD_MANUAL,
				      04, 304, 42, 42, hDlg, (DWORD)(&btBmpCommonLeft[4]));
		hwndButtonCommon[5] =
			CreateWindow (CTRL_BUTTON,
				      " Training ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE,
				      IDC_BUTTON_MD_TRAINING,
				      04, 350, 42, 42, hDlg, (DWORD)(&btBmpCommonLeft[5]));

		/* Enter, Clear, Run buttons */
		hwndButtonEnter =
			CreateWindow (CTRL_BUTTON,
				      " enter ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE,
				      IDC_BUTTON_ENTER,
				      485, 406, 32, 32, hDlg, (DWORD)(&btBmpEnter));
		hwndButtonClear =
			CreateWindow (CTRL_BUTTON,
				      " clear ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE,
				      IDC_BUTTON_CLEAR,
				      521, 406, 32, 32, hDlg, (DWORD)(&btBmpClear));
		hwndButtonRun =
			CreateWindow (CTRL_BUTTON,
				      " run ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE,
				      IDC_BUTTON_RUN,
				      557, 406, 32, 32, hDlg, (DWORD)(&btBmpRun));


		/* set focus to data input edit box */
		SetFocusChild(hwndInputCutData);

		return 1;

	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}
		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}
		initUserproEditDlgBkgnd(hDlg,hdc,&proRunStatus);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}

	case MSG_TIMER:
		SetDlgItemText(hDlg, IDC_TIME, convertSysTime(timeBuff));
		return 0;

	case MSG_CHAR:
		ret = LOWORD(wParam);
		if((ret > 0x2b) &&(ret < 0x3e) ){ /* it is number key or "." */
			if(mannualEditFlag == 1){ /* clear the editbox everytime it changed */
				SetWindowText(hwndInputCutData,"");
				mannualEditFlag = 0;
			}
		}
		break;

	case MSG_KEYDOWN:
		hdc=BeginPaint(hDlg);
		switch(LOWORD(wParam)){
		case SCANCODE_F1:
			/* Functions:
			 *   Edit Mode: label edit
			 *   Auto Mode:
			 *   Manual Mode:
			 */
			printf("SCANCODE_F1 msg \n");

			if(proRunStatus.runStop > 0){    /* motor is running, edit is NOT allowed */
				break;
			}

			if(proRunStatus.runStatus == 0){        /* edit mode */
				hWnd=GetFocusChild(hDlg);       /* only do when focus is right */
				if(hWnd != hwndInputCutData){
					printf("cur focus error \n");
					break;
				}

				if(programEdit.userEditStatus == 0){
					programEdit.userEditStatus = 1;  /* edit method is label */
					currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
					programEdit.currentCutsNum = currentSel;
					enterLableEdit(hDlg);
					//printf("enterLableEdit exit...%d \n",programEdit.lableEdit.lableStatus);
					if(programEdit.lableEdit.lableStatus == 7){
						/* Edit OK, update to listbox */
						UpdateProListWithFileData(hDlg,&programEdit);
					}
					programEdit.lableEdit.lableStatus = 0;
					ResetUserProgramEdit(hDlg,&programEdit,0);
					SetFocusChild(hwndInputCutData);
				}
				break;
			}

			/* in AutoRun/Manual mode, F1 key is used to clear the cut counter */
			if(proRunStatus.runStatus > 0){
				motorStatus.cutCounter = 0;
				refreshCutCounter(hDlg);
			}
			break;

		case SCANCODE_F2:
			/* Functions:
			 *   Edit Mode: average edit
			 *   Auto Mode:
			 *   Manual Mode:
			 */
			if(proRunStatus.runStop > 0){
				break;
			}

			if(proRunStatus.runStatus == 0){
				hWnd=GetFocusChild(hDlg);
				if(hWnd != hwndInputCutData){
					break;
				}

				if(programEdit.userEditStatus == 0){
					programEdit.userEditStatus = 2; /* edit method is Average */
					currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
					programEdit.currentCutsNum = currentSel;
					enterAverageEdit(hDlg);
					if(programEdit.halfEdit.halfStatus == 3){
						UpdateProListWithFileData(hDlg,&programEdit);
					}
					programEdit.halfEdit.halfStatus = 0;
					ResetUserProgramEdit(hDlg,&programEdit,0);
					SetFocusChild(hwndInputCutData);
				}
				break;
			}

			/* deal with AUTO/MANUAL mode here */
			break;



		case SCANCODE_F3:
			/* Functions:
			 *   Edit Mode:   modify AIR flag
			 *   Auto Mode:
			 *   Manual Mode: computer
			 */
			if(proRunStatus.runStop > 0){
				break;
			}

			hWnd=GetFocusChild(hDlg);
			if(hWnd != hwndInputCutData)
				break;

			/* change air flag */
			if((proRunStatus.runStatus == 0) && (programEdit.userEditStatus == 0)){
				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				programEdit.currentCutsNum = currentSel;
				curPos = programEdit.userProCuts[currentSel];
				changeAirFlagOfData(hDlg,&programEdit);
				break;
			}

			if(proRunStatus.runStatus == 2){  /* manual run mode */
				EnterComputer(hDlg);
				char *pBuf = computerData.inputBuf; /* dispaly the result to the input box */
				int resultLen = strlen(pBuf);
				SetWindowText(hwndInputCutData,pBuf);
				/* set the caret to the last so that user can input */
				SendMessage(hwndInputCutData,EM_SETCARETPOS,0,resultLen);
			}

			break;

			/* === TODO: training mode */
			if(proRunStatus.runStatus == 0){
				if(programEdit.userEditStatus == 0){
					resetMotorRunStatus(&proRunStatus);
					programEdit.userEditStatus = 7;

				}else if(programEdit.userEditStatus == 7){  /* Exit Traing Mode */
					resetMotorRunStatus(&proRunStatus);
					programEdit.userEditStatus = 0;     /* back to normal edit */
				}
			}
			break;


		case SCANCODE_F4:
			/* Functions:
			 *   Edit Mode: modify PUSHER flag
			 *   Auto Mode:
			 *   Manual Mode:
			 */
			if(proRunStatus.runStop > 0)
				break;

			hWnd=GetFocusChild(hDlg);
			if(hWnd != hwndInputCutData)
				break;

			if((proRunStatus.runStatus == 0) && (programEdit.userEditStatus == 0)){
				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				programEdit.currentCutsNum = currentSel;
				changePusherFlagOfData(hDlg,&programEdit);
			}
			break;

		case SCANCODE_F5:
			/* Functions:
			 *   Edit Mode: modify AUTOCUT flag
			 *   Auto Mode:
			 *   Manual Mode:
			 */
			if(proRunStatus.runStop > 0)
				break;

			hWnd=GetFocusChild(hDlg);
			if(hWnd != hwndInputCutData)
				break;

			if((proRunStatus.runStatus == 0) && (programEdit.userEditStatus == 0)){
				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				programEdit.currentCutsNum = currentSel;
				curPos = programEdit.userProCuts[currentSel];
				if(curPos & AUTOCUT_FLAG){
					delAutoCutFlagFromData(hDlg,&programEdit);
				} else {
					addAutoCutFlagToData(hDlg,&programEdit);
				}
				break;
			}

			break;

			/* === TODO: enter configure window */
			EnterConfigureUser(hDlg);
			curPos = motorStatus.currentCutPos;
			refreshCurrentPosArea(hDlg,hdc,curPos,1);

			break;

		case SCANCODE_F6:
			/* Functions:
			 *   Edit Mode:   computer
			 *   Auto Mode:   RFU
			 *   Manual Mode: RFU
			 */
			if(proRunStatus.runStop > 0){
				break;
			}

			hWnd=GetFocusChild(hDlg);
			if(hWnd != hwndInputCutData)
				break;

			if(proRunStatus.runStatus == 0){  /* edit mode */
				EnterComputer(hDlg);
				char *pBuf = computerData.inputBuf;
				int resultLen = strlen(pBuf);
				/* dispaly the result to the input box */
				SetWindowText(hwndInputCutData,pBuf);
				/* set the caret to the last so that user can input */
				SendMessage(hwndInputCutData,EM_SETCARETPOS,0,resultLen);
			}

			break;

		case SCANCODE_ADD:     /* data just: Add */
			if(proRunStatus.runStop > 0){
				break;
			}
			if((proRunStatus.secondFunc == 1)&&(proRunStatus.runStatus == 0)
			   &&(programEdit.userEditStatus == 0)){
				// 改变当前数据切刀标志
				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				programEdit.currentCutsNum = currentSel;
				curPos = programEdit.userProCuts[currentSel];   // 取出数据
				if(curPos & AUTOCUT_FLAG){      // 数据包含自动切刀信息,则删除
					delAutoCutFlagFromData(hDlg,&programEdit);
				} else {
					addAutoCutFlagToData(hDlg,&programEdit);
				}
				break;
			}

			if((proRunStatus.runStatus == 0)&&(programEdit.userEditStatus == 0)){
				/* Add the input data to the last data, the sum is new */
				programEdit.userEditStatus = 8;
			}else if((proRunStatus.runStatus == 0)&&(programEdit.userEditStatus == 8)){
				// Exit this mode
				programEdit.userEditStatus = 0;
				RefreshBoxArea (hdc,&userProgramEditBkgnd,88,395,40,40,88,395); // clear "+" flag
			}
			if((proRunStatus.runStatus == 2)&&(proRunStatus.runStop == 0)){
				if(proRunStatus.mannualFlag == 0){
					proRunStatus.mannualFlag = 1;
				}else if(proRunStatus.mannualFlag == 1){
					proRunStatus.mannualFlag = 0;
					ResetUserProgramEdit(hDlg,&programEdit,0); //Clear Add flag
				}
				break;
			}
			break;

		case SCANCODE_SUB:      /* sub */
			if(proRunStatus.runStop > 0){           // Running
				break;
			}
			if((proRunStatus.secondFunc == 1)&&(proRunStatus.runStatus == 0)
			   &&(programEdit.userEditStatus == 0)){
				/* change all the data's autocut flag */
				autoCutChangeFlag ++;
				if(autoCutChangeFlag%2){
					addAutoCutFlagToAllDatas(hDlg,&programEdit);
				}else{
					delAutoCutFlagFromAllDatas(hDlg,&programEdit);
				}
				break;
			}

			if((proRunStatus.runStatus == 0)&&(programEdit.userEditStatus == 0)){
				programEdit.userEditStatus = 9;
				// 最后一刀数据加上输入的数据,结果新增一刀
			}else if((proRunStatus.runStatus == 0)&&(programEdit.userEditStatus == 9)){
				// Exit this mode
				programEdit.userEditStatus = 0;
				RefreshBoxArea (hdc,&userProgramEditBkgnd,88,395,40,40,88,395); // clear "-" flag
			}

			if((proRunStatus.runStatus == 2)&&(proRunStatus.runStop == 0)){
				if(proRunStatus.mannualFlag == 0){
					proRunStatus.mannualFlag = 2;
				}else if(proRunStatus.mannualFlag == 2){
					proRunStatus.mannualFlag = 0;
					ResetUserProgramEdit(hDlg,&programEdit,0); //Clear Add flag
				}
				break;
			}
			break;

		case SCANCODE_MODIFY:  /* modify the current data */
			/* Functions:
			 *   Edit Mode:   Modify current data
			 *   Auto Mode:   RFU
			 *   Manual Mode: RFU
			 * Usage:
			 *   Input the data and press MODIFY key, the current selected data
			 *   in listbox will be changed
			 */
			if(proRunStatus.runStop > 0)
				break;

			if(proRunStatus.runStatus > 0)  /* Not edit mode */
				break;

			hWnd=GetFocusChild(hDlg);
			if(hWnd != hwndInputCutData)
				break;

			if((proRunStatus.runStatus == 0) && (programEdit.userEditStatus == 0)){
				programEdit.userEditStatus = 3;  /* set edit mothod to modify */
				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				programEdit.currentCutsNum = currentSel;

				modifyOneCutData(hDlg,&programEdit);
				ResetUserProgramEdit(hDlg,&programEdit,0);
				SetFocusChild(hwndInputCutData);
			}
			break;

		case SCANCODE_INSERTDATA:
			/* Functions:
			 *   Edit Mode:   Modify current data.
			 *                IME switch
			 *   Auto Mode:   RFU
			 *   Manual Mode: RFU
			 * Usage:
			 *   Input the data then press insert key, the data will be inserted
			 *   to the before current item.
			 */
			if(proRunStatus.runStatus > 0)
				break;

			hWnd=GetFocusChild(hDlg);
			if(hWnd != hwndInputCutData){
				if(hWnd == hwndProName){
					/* Now used as IME change KEY */
					break;
				}
				SetFocusChild(hwndInputCutData);
				break;
			}

			if((proRunStatus.runStatus == 0) && (programEdit.userEditStatus == 0)){
				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				programEdit.currentCutsNum = currentSel;
				programEdit.userEditStatus = 4; /* set edit method to insert */

				/* insert inputed data to current position */
				insertOneCutData(hDlg,&programEdit);
				ResetUserProgramEdit(hDlg,&programEdit,0);
				SetFocusChild(hwndInputCutData);
			}

			break;

		case SCANCODE_DELETE:
			if(!isFunctionButtonValid(&proRunStatus)){
				break;
			}

			if( proRunStatus.secondFunc == 1){      // 第二功能按下,全部删除
				releaseSecondFunction(hDlg);
				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				programEdit.currentCutsNum = currentSel;
				//deleteAllCutDatas(hDlg,&programEdit);
				deleteBehindCutDatas(hDlg,&programEdit);

			} else {
				programEdit.userEditStatus = 5;  // 设置编辑方法为删除
				// 得到当前列表框选项
				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				int totalCount = SendMessage(hwndProCutList,LB_GETCOUNT,0,0); // 列表条目数
				programEdit.currentCutsNum = currentSel;
				// 删除该数据
				if(currentSel < (totalCount-1)){        // 正常删除
					deleteOneCutData(hDlg,&programEdit);
				} else if(currentSel == (totalCount-1)){  //  试图删除最后一刀0.0
					SendMessage(hwndProCutList,LB_SETCURSEL,0,0);    // 高亮输入项
					programEdit.userEditStatus = 0;
				}
			}
			break;

		case SCANCODE_PUSHER:
			/* Functions:
			 *   Edit Mode:   modify PUSHER flag
			 *   Auto Mode:   RFU
			 *   Manual Mode: RFU
			 */
			if(proRunStatus.runStop > 0)
				break;

			hWnd = GetFocusChild(hDlg);
			if(hWnd != hwndInputCutData)
				break;

			if((proRunStatus.runStatus == 0) && (programEdit.userEditStatus == 0)){
				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				programEdit.currentCutsNum = currentSel;
				changePusherFlagOfData(hDlg,&programEdit);
			}
			break;

		case SCANCODE_MYEQUAL:  //  shift + equal -> Add Y flag
			if(proRunStatus.runStop > 0){   // Disable when RUNNING
				break;
			}
			if(proRunStatus.secondFunc != 1){
				break;
			}
			if((proRunStatus.runStatus == 0) && (programEdit.userEditStatus == 0)){
				hWnd=GetFocusChild(hDlg);       // Only when focus in edit box
				if(hWnd != hwndInputCutData){
					break;
				}
				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				programEdit.currentCutsNum = currentSel;
				curPos = programEdit.userProCuts[currentSel];
				changePressSheetFlagOfData(hDlg,&programEdit);
			}
			releaseSecondFunction(hDlg);            // 刷新第二功能标志
			break;


		case SCANCODE_PAGEUP :  // 当当前焦点在输入框时可以移动列表框选项
			if((proRunStatus.runStatus==1)||(proRunStatus.runStatus==3)||(isIMEOpenInMotor==1)){
				// 自动模式以及输入法打开时,按此键无效
				break;
			}
			hWnd=GetFocusChild(hDlg);               // 在KEYUP消息中给回焦点到输入框
			if(hWnd == hwndInputCutData){
				SetFocusChild(hwndProCutList);
			}
			break;

		case SCANCODE_PAGEDOWN :                // 自动模式,按此键无效
			if((proRunStatus.runStatus==1)||(proRunStatus.runStatus==3)||(isIMEOpenInMotor==1)){
				break;
			}
			hWnd=GetFocusChild(hDlg);               // 得到当前焦点,移动后给回焦点
			if(hWnd == hwndInputCutData){
				SetFocusChild(hwndProCutList);
			}
			break;
		case SCANCODE_CURSORBLOCKUP :   // 当当前焦点在输入框时可以移动列表框选项
			if((proRunStatus.runStatus == 1) || proRunStatus.runStatus == 3){
				// 自动模式,按此键无效
				break;
			}
			hWnd=GetFocusChild(hDlg);               // 在KEYUP消息中给回焦点到输入框
			if(hWnd == hwndInputCutData){
				SetFocusChild(hwndProCutList);
			}
			break;

		case SCANCODE_CURSORBLOCKDOWN :         //
			if((proRunStatus.runStatus == 1) || proRunStatus.runStatus == 3){
				// 自动模式,按此键无效
				break;
			}
			hWnd=GetFocusChild(hDlg);               // 得到当前焦点,移动后给回焦点
			if(hWnd == hwndInputCutData){
				SetFocusChild(hwndProCutList);
			}
			break;

		case SCANCODE_PROG:
			/* Functions:
			 *   Edit Mode:   enter program list window in any mode
			 *   Auto Mode:   RFU
			 *   Manual Mode: RFU
			 */
			if(proRunStatus.runStatus > 0)
				break;

			/* send exit message */
			userProExitFlag = 0;
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_FORWARD:
			frontlimit = getValidLimit();
			if(motorStatus.currentCutPos > frontlimit){
				motorRunStep(MOTOR_FORWARD,0);
			}
			break;

		case SCANCODE_BACKWARD:
			if(motorStatus.currentCutPos < currentuser->backlimit){
				motorRunStep(MOTOR_BACKWARD,0);
			}
			break;

		case SCANCODE_FORWARD_HS:        /* Highspeed Backward */
			frontlimit = getValidLimit();
			if(motorStatus.currentCutPos > frontlimit){
				motorRunStep(MOTOR_FORWARD_HS,0);
			}
			break;

		case SCANCODE_BACKWARD_HS:      /* Highspeed Backward */
			if(motorStatus.currentCutPos < currentuser->backlimit){
				motorRunStep(MOTOR_BACKWARD_HS,0);
			}
			break;

		case SCANCODE_FUNCTION:  /* left shift */
			if(proRunStatus.secondFunc == 0){
				proRunStatus.secondFunc = 1;
			} else {
				proRunStatus.secondFunc = 0;
			}
			refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,3,0);
			break;

		case SCANCODE_EDIT: /* Edit Mode */
			if(proRunStatus.runStop > 0)
				break;

			if(proRunStatus.runStatus == 0){
				/* already in edit mode */
				break;
			}else{
				proRunStatus.runStatus = 0;
				/* add the last "0.0" data item */
				addLastListItem(hwndProCutList,&programEdit,lastListItem);
				programEdit.currentCutsNum = 0;
				SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
				resetMotorRunStatus(&proRunStatus);
				InvalidateRect(hDlg,NULL,TRUE);
				/* update status  */
				refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);
				/* regresh mode  */
				updateCurrentModeStatus(hDlg, &proRunStatus);

			}

			break;

		case SCANCODE_MANUAL:  /* maunal mode */
			if(proRunStatus.runStop > 0)
				break;
			if(proRunStatus.runStatus == 2){
				/* already in manual mode */
				break;
			}
			if (proRunStatus.runStatus == 0){
				proRunStatus.runStatus = 2;
				exitTrainingMode(&programEdit);
				deleteLastListItem(hwndProCutList,&programEdit,lastListItem);
				programEdit.currentCutsNum = 0;
				SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
				resetMotorRunStatus(&proRunStatus);
				InvalidateRect(hDlg,NULL,TRUE);
				refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);
			}

			if (proRunStatus.runStatus == 1){
				proRunStatus.runStatus = 2;
				programEdit.currentCutsNum = 0;
				SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
				resetMotorRunStatus(&proRunStatus);
				InvalidateRect(hDlg,NULL,TRUE);
				refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);
			}

			/* regresh mode  */
			updateCurrentModeStatus(hDlg, &proRunStatus);
			break;

		case SCANCODE_AUTORUN:  /* autorun mode */
			if(proRunStatus.runStop > 0) break;
			if(proRunStatus.runStatus == 1){
				break;
			}
			if (proRunStatus.runStatus == 0){
				proRunStatus.runStatus = 1;
				exitTrainingMode(&programEdit);
				deleteLastListItem(hwndProCutList,&programEdit,lastListItem);
				programEdit.currentCutsNum = 0;
				SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
				resetMotorRunStatus(&proRunStatus);
				InvalidateRect(hDlg,NULL,TRUE);
				refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);
			}
			if (proRunStatus.runStatus == 2){
				proRunStatus.runStatus = 1;
				programEdit.currentCutsNum = 0;
				SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
				resetMotorRunStatus(&proRunStatus);
				InvalidateRect(hDlg,NULL,TRUE);
				refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);
			}
			/* regresh mode  */
			updateCurrentModeStatus(hDlg, &proRunStatus);
			break;

		case SCANCODE_RUN:  /* run this program */
			/* handle in KEYUP message */
			break;

		case SCANCODE_ENTER:
			if(proRunStatus.runStatus>0){
				break;
			}
			break;

		case SCANCODE_CANCEL: /* clear */
			if((proRunStatus.runStatus == 1) || (proRunStatus.runStatus == 3)){
				break;
			}

			hWnd=GetFocusChild(hDlg);
			if(hWnd == hwndInputCutData){
				SetFocusChild(hwndInputCutData);
				SetWindowText(hWnd,"");
			}

			break;

#if 0
		case SCANCODE_HELP:  /* get help */
			hWnd=GetFocusChild(hDlg);
			if(hWnd == hwndProName){
				MessageBox(hDlg,userpro_help[*curSysLang].help_userpro_name,
					   userpro_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			}else if(hWnd == hwndInputCutData){
				MessageBox(hDlg,userpro_help[*curSysLang].help_userpro_cuts,
					   userpro_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			}
			break;
#endif
		}
		EndPaint(hDlg,hdc);
		break;

	case MSG_KEYUP:
		hdc=BeginPaint(hDlg);
		switch(LOWORD(wParam)){
		case SCANCODE_F6:      /* save data to file */

			break;

			/* === TODO: not used now */
			if(!isFunctionButtonValid(&proRunStatus)){
				break;
			}

			hWnd=GetFocusChild(hDlg);    /* focus is in edit box, not else */
			if(hWnd != hwndInputCutData){
				break;
			}
			printf("Save:%s\n",programCreate.filePathname);
			/* first update file header */
			UpdateFileHeader(&programCreate,&programEdit,0);
			saveUserProgramToFile(programCreate.filePathname,&(programCreate.proHeader),&programEdit);

			/* save lastuser and last program */
			saveUserConfigFile(0);
			/* refresh lastuser as current user */
			startupParams.lastuser = currentid;
			saveInitConfigFile(&startupParams);

			MessageBox(hDlg,userpro_help[*curSysLang].msg_save_pro_ok,
				   userpro_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
			SetFocusChild(hwndInputCutData);
			break;

		case SCANCODE_CURSORBLOCKUP:   /* UP arrow */
			if((proRunStatus.runStatus == 1) || proRunStatus.runStatus == 3){
				/* disabled in Autorun mode */
				break;
			}
			/* set focus back to edit box after listbox item move */
			SetFocusChild(hwndInputCutData);
			break;

		case SCANCODE_CURSORBLOCKDOWN: /* DOWN arrow */
			if((proRunStatus.runStatus == 1) || proRunStatus.runStatus == 3){
				break;
			}
			SetFocusChild(hwndInputCutData);
			break;

		case SCANCODE_PAGEUP:
			if((proRunStatus.runStatus==1)||(proRunStatus.runStatus==3)||(isIMEOpenInMotor==1)){
				break;
			}
			SetFocusChild(hwndInputCutData);
			break;

		case SCANCODE_PAGEDOWN :
			if((proRunStatus.runStatus==1)||(proRunStatus.runStatus==3)||(isIMEOpenInMotor==1)){
				break;
			}
			SetFocusChild(hwndInputCutData);
			break;

		case SCANCODE_RUN:    /* run this program */

			if(proRunStatus.machineStop == 0){      // Running
				break;
			}

			/* In training mode, this key is enabled */
			if((proRunStatus.runStatus == 0) && (programEdit.userEditStatus != 7)){
				break;
			}

			/* printf("Run status =%d %d \n",proRunStatus.runStatus,programEdit.nInputCutsNum); */
			if(proRunStatus.runStatus == 1){
				if(programEdit.nInputCutsNum == 1){
					/* No program data to run */
					break;
				}else{
					/* save this program automatically */
					UpdateFileHeader(&programCreate,&programEdit,0);
					saveUserProgramToFile(programCreate.filePathname,
							      &(programCreate.proHeader),&programEdit);
				}
			}

			if((proRunStatus.runStatus == 2) &&(proRunStatus.runStop == 0)){
				proRunStatus.runStop = 1;   /* set machine is running */
			}
			if(proRunStatus.runStatus == 2){    /* handle the edit flag in manual mode */
				mannualEditFlag = 1;
				if(proRunStatus.mannualFlag){  /* add or sub */
					ResetUserProgramEdit(hDlg,&programEdit,0);
				}
			}

			currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
			SendMessage(hwndProCutList,LB_SETCURSEL,currentSel,0);
			programEdit.currentCutsNum = currentSel;
			proRunStatus.destCutNum = currentSel;
			refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,1,0);

			proRunStatus.cutActTimes = 0;
			proRunStatus.pusherActTimes = 0;
			proRunStatus.cutPusherActTimes = 0;
			proRunStatus.isOneCutDone = 1;     /* emulate the last cut is done */
			proRunStatus.machineStop = 0;
			//printf("motorRunProcess %d \n",currentSel);
			motorRunProcess(hDlg,&programEdit,&proRunStatus);  /* begin to run this program */
			break;

		case SCANCODE_ENTER:
			if(proRunStatus.runStatus > 0){
				break;
			}
			break;

		case SCANCODE_CANCEL:   /* clear the data input edit box */
			if((proRunStatus.runStatus == 1) || (proRunStatus.runStatus == 3)){
				break;
			}
			break;
		}
		EndPaint(hDlg,hdc);
		break;

	case MSG_RECV:  /* got data from machine */
		recvCommand(cmdRecvBuf); /* First copy to user space */

		// For test
		//strcpy(cmdRecvBuf,testbuffer);
		//ret = isCmdValid(cmdRecvBuf);  /* do checksum */
		//if(ret){
		memset(&cmdData,0,sizeof(cmdData));
		getCmdFrame(&cmdData,cmdRecvBuf);   /* get data */
		if(cmdData.flag == 'G'){            /* data frame */
			hdc=BeginPaint(hDlg);
			if(unit){
				cmdData.data = mm_to_inch(cmdData.data);
			}
			if( motorStatus.currentCutPos != cmdData.data){
				/* only refresh data if it is changed */
				motorStatus.currentCutPos = cmdData.data;
				refreshCurrentPosArea(hDlg,hdc,cmdData.data,0);
				//printf("Refresh forCutLen...\n");
				refreshForCutLenArea(hwndCurrentPos,&proRunStatus,cmdData.data);
			}

			/* get machine status */
			if(motorStatus.devStatus != cmdData.status){
				motorStatus.devStatus = cmdData.status;  /* update status */
				ret = getDevStatus(&proRunStatus,cmdData.status);
				refreshCurrentPos(hwndCurrentPos,proRunStatus.forCutLen);
				/* regresh pusher and cut status */
				refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,4,0);
				/* refresh run/stop status */
				refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,1,0);
				if(ret){
					motorStatus.cutCounter++;
					refreshCutCounter(hDlg);
					if((proRunStatus.runStatus == 0)&&(programEdit.userEditStatus == 7)){
						/* in trainig mode, record this data */
						editOneCutData(hDlg,&programEdit,motorStatus.currentCutPos);
						resetMotorRunStatus(&proRunStatus);
					}
					if(proRunStatus.runStatus == 2){
						/* in maual mode, reset for next run */
						resetMotorRunStatus(&proRunStatus);
					}
				}

			}
			EndPaint(hDlg,hdc);

			/* in Autorun mode, go to next data */
			if(proRunStatus.runStatus == 1){
				motorRunAuto(hDlg,&programEdit,&proRunStatus);
				//motorRunProcess(hDlg,&programEdit,&proRunStatus);
			}
		}
		//}
		break;

	case MSG_COMMAND:
		/* Fisrt set focus back to the edit box */
		SetFocusChild(hwndInputCutData);
		switch(wParam){
		/*
		 * Right side F1 - F6
		 */
		case IDC_BUTTON_EDIT_F1:
			printf("IDC_BUTTON_EDIT_F1 push\n");
			SetFocusChild(hwndInputCutData);
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_BUTTON_EDIT_F2:
			printf("IDC_BUTTON_EDIT_F2 push\n");
			SetFocusChild(hwndInputCutData);
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F2, 0L);
			break;
		case IDC_BUTTON_EDIT_F3:
			printf("IDC_BUTTON_EDIT_F3 push\n");
			SetFocusChild(hwndInputCutData);
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F3, 0L);
			break;
		case IDC_BUTTON_EDIT_F4:
			printf("IDC_BUTTON_EDIT_F4 push\n");
			SetFocusChild(hwndInputCutData);
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F4, 0L);
			break;
		case IDC_BUTTON_EDIT_F5:
			printf("IDC_BUTTON_EDIT_F5 push\n");
			SetFocusChild(hwndInputCutData);
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F5, 0L);
			break;
		case IDC_BUTTON_EDIT_F6:
			printf("IDC_BUTTON_EDIT_F6 push\n");
			SetFocusChild(hwndInputCutData);
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F6, 0L);
			break;
		/*
		 * Left side common button
		 */
		case IDC_BUTTON_REGISTER:
			/* back to user login window */
			/* === TODO */
			break;
		case IDC_BUTTON_PROLIST:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_PROG, 0L);
			break;
		case IDC_BUTTON_MD_EDIT:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_EDIT, 0L);
			break;
		case IDC_BUTTON_MD_AUTO:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_AUTORUN, 0L);
			break;
		case IDC_BUTTON_MD_MANUAL:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_MANUAL, 0L);
			break;
		case IDC_BUTTON_MD_TRAINING:
			/* === TODO */
			break;

		/*
		 * Enter, clear and run
		 */
		case IDC_BUTTON_ENTER:
			printf("IDC_BUTTON_ENTER push\n");
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_ENTER, 0L);
			break;

		case IDC_BUTTON_CLEAR:
			printf("IDC_BUTTON_CLEAR push\n");
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_CANCEL, 0L);
			break;

		case IDC_BUTTON_RUN:
			printf("IDC_BUTTON_CLEAR push\n");
			SendMessage(hDlg, MSG_KEYUP, SCANCODE_RUN, 0L);
			break;

		case IDCANCEL:
			KillTimer(hDlg,IDC_TIMER);
			EndDialog (hDlg, wParam);
			break;
		}
		break;

	}


	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void InitUserProDialogBox (HWND hWnd)
{
	UserProgromDlgInitProgress.controls = CtrlInitProgress;

	DialogBoxIndirectParam (&UserProgromDlgInitProgress, hWnd, InitUserProDialogBoxProc, 0L);
}

int EnterUserProgram(void)
{

	/* load bmp files */
	if(loadUserProBmpFiles())
		return(1);

	InitUserProDialogBox (HWND_DESKTOP);            // Should not return except receive CLOSE MSG


	printf("EnterUserProgram()...UnloadBitmap...\n");
	unloadUserProBmpFiles();

	if(!userProExitFlag){
		/* goto program list window */
		EnterProListDialog();
	}
	return 0;
}
