/* 
** $Id: animation.c,v 1.3 2004/09/24 05:02:15 suowei Exp $
**
** Listing 36.1
**
** animation.c: Sample program for MiniGUI Programming Guide
**      Usage of ANIMATION control and GIF89a loader.
**
** Copyright (C) 2004 Feynman Software.
**
** License: GPL
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <minigui/common.h>
#include <minigui/minigui.h>
#include <minigui/gdi.h>
#include <minigui/window.h>
#include <minigui/control.h>
#include <minigui/mgext.h>

#include "motormain.h"
#include "motorregister.h"
#include "motorfile.h"
#include "motor.h"
#include "bkgndEdit.h"
#include  "help.h"	

static int animationFlag;		// 是否需要开机动画
static int bkgndFlag;

//#define __DEBUG_MODE_
  
struct welcome_help_t{
	unsigned char * msg_welcome;
};

static struct welcome_help_t welcome_help[SYSLANG_NO] = 
{
{msg_welcome_cn},
{msg_welcome_en}
};


/*   加载初始化文件,读出配置参数
 */
int loadInitConfigFile(int *pFlash,int *pBkgnd)
{
    //int flag=0;	
    FILE *fp;
    if((fp=fopen("./user/init.cfg","r"))==NULL)
    {
	printf("Open init file error!\n");
	return -1;
    }

    while(!feof(fp)){
	fscanf(fp,"%d %d\n",pFlash,pBkgnd);
    }
    fclose(fp);

    return(0);
    
}

int saveInitConfigFile(int *pFlash,int *pBkgnd)
{
    FILE *fp;
    if((fp=fopen("./user/init.cfg","w"))==NULL)
    {
	printf("Open init file error!\n");
	return(-1);	
    } 

    fprintf(fp,"%d %d\n",*pFlash,*pBkgnd);
    fclose(fp);

    return(0);

}

/*
static void initSysTime(void)
{
    time_t t;
    struct tm *tmGet,tmNow;
    time(&t);	// 得到当前时间
    tmGet = localtime(&t);	// 转化为本地时间
    tmNow = *tmGet;	// 保存
    printf("year = %d \n",tmNow.tm_year);
    if(tmNow.tm_year == 70){	// 1970
	tmNow.tm_year = 106;	// 1900 +106 = 2006
	tmNow.tm_mon = 0;	// 1 月
	tmNow.tm_mday = 1;	// 1 日
	tmNow.tm_hour = 12;
	tmNow.tm_min = 0;
	tmNow.tm_sec = 0; 
	t = timelocal(&tmNow);
        stime(&t);
	printf("Set Syetem Time Done.\n");
    }   
}
*/
static struct comCmdData_t cmdData;

static int AnimationWinProc(HWND hWnd, int message, WPARAM wParam, LPARAM lParam)
{
    char cmdbuf[12];
    unsigned char *pWelcomeText = welcome_help[*curSysLang].msg_welcome;
    int curPos; 
    HDC hdc;
    switch (message) {
    case MSG_CREATE:
    {

	if(!animationFlag){
	    hdc = BeginPaint (hWnd);
	    BITMAP welcomeBkgnd;
            if (LoadBitmap (HDC_SCREEN, &welcomeBkgnd, "./res/welcome.jpg"))
           	return 1;
	    FillBoxWithBitmap (hdc, 0, 0, 0, 0, &welcomeBkgnd);
	    UnloadBitmap (&welcomeBkgnd);
            
	    break;
	}

	hdc = BeginPaint (hWnd);
        TextOut (hdc, 210, 220, pWelcomeText);
	EndPaint (hWnd, hdc);
        ANIMATION* anim = CreateAnimationFromGIF89aFile (HDC_SCREEN, "./res/welcome.gif");
        if (anim == NULL)
            return 1;

        SetWindowAdditionalData (hWnd, (DWORD) anim);
        CreateWindow (CTRL_ANIMATION, 
                          "", 
                          WS_VISIBLE | ANS_AUTOLOOP, 
                          800, 					// ID
                          0, 0, 640, 480, hWnd, (DWORD)anim);
        SendMessage (GetDlgItem (hWnd, 800), ANM_STARTPLAY, 0, 0);


        break;
    }

    case MSG_KEYDOWN:		// anykey press,pause
	switch(LOWORD(wParam)){
	    case SCANCODE_RUN:
		memset(cmdbuf,0,12);
		cmdData.flag = 'R';
		cmdData.status = 0x30;
		cmdData.data = 80000;
		setCmdFrame(&cmdData,cmdbuf);	// 组帧
		sendCommand(cmdbuf);		// 发送
#ifdef __DEBUG_MODE_
		if(animationFlag){
		   SendMessage (GetDlgItem (hWnd, 800), ANM_STOPPLAY, 0, 0);
		}
#endif
		break;
	}
	break;

#ifdef __DEBUG_MODE_
    case MSG_KEYUP:		// anykey press,go out
	switch(LOWORD(wParam)){
	    case SCANCODE_RUN:
		SendMessage(hWnd,MSG_CLOSE,0,0);
		break;
	}   
        break; 
#endif

    case MSG_RECV:		// 收到下位机发送的数据,退出动画,进入下一个界面
	recvCommand(cmdbuf);
        //int ret = isCmdValid(cmdbuf);	// 测试,不用校验
	//if(ret){
	    memset(&cmdData,0,sizeof(cmdData));
	    getCmdFrame(&cmdData,cmdbuf);	// 获得数据
	    if(cmdData.flag == 'R'){		
	    	motorStatus.currentCutPos = cmdData.data;	// 更新数据
		motorStatus.devStatus = cmdData.status;		// 更新状态,低四位有效 
		if(animationFlag){
	    	   SendMessage (GetDlgItem (hWnd, 800), ANM_STOPPLAY, 0, 0);
		}
	    	SendMessage(hWnd,MSG_CLOSE,0,0);
	    }
	//}
	break;

    case MSG_DESTROY:
	if(animationFlag){
           DestroyAnimation ((ANIMATION*)GetWindowAdditionalData (hWnd), TRUE);
           DestroyAllControls (hWnd);
	}
        return 0;

    case MSG_CLOSE:
        DestroyMainWindow (hWnd);
        PostQuitMessage (hWnd);
        return 0;
    }

    return DefaultMainWinProc(hWnd, message, wParam, lParam); 
}


int MiniGUIMain (int argc, const char* argv[])
{
    MSG Msg;
    HWND hMainWnd;
    MAINWINCREATE CreateInfo;

    isIMEOpenInMotor = 0;
    motorStatus.currentCutPos = 0;	// 初始化
    motorStatus.newCutPos = 0;	// 初始化
    motorStatus.devStatus = 0;
    motorStatus.flags = 0;   
    motorStatus.cutCounter = 0;     

#ifdef _IME_GB2312
    myIMEWindow=GBIMEWindow (HWND_DESKTOP);	// 输入发窗口的句柄
    if(myIMEWindow){
	    MoveWindow(myIMEWindow,170,110,365,40,0);
	    ShowWindow(myIMEWindow,SW_HIDE);	// Hide 输入法 window
	    isIMEOpenInMotor = 0;
    }
#endif

	// 创建字体
    userFont24 = CreateLogFont (NULL, "song", "GB2312", 
                        FONT_WEIGHT_BOOK, FONT_SLANT_ROMAN, FONT_SETWIDTH_NORMAL,
                        FONT_SPACING_CHARCELL, FONT_UNDERLINE_NONE,
		 	FONT_STRUCKOUT_NONE, 24, 0);
    userFont16 = CreateLogFont (NULL, "song", "GB2312", 
                        FONT_WEIGHT_BOOK, FONT_SLANT_ROMAN, FONT_SETWIDTH_NORMAL,
                        FONT_SPACING_CHARCELL, FONT_UNDERLINE_NONE,
			FONT_STRUCKOUT_NONE, 16, 0);
    //systemDefaultFont = GetSystemFont(SYSLOGFONT_DEFAULT);

#ifndef __DEBUG_MODE_
    initSerialCommunication();		// 初始化串口通信
#endif
    //initSysTime();			// 初始化时间为2006-01-01 12:00:00
    loadInitConfigFile(&animationFlag,&bkgndFlag);
    //printf("animationFlag = %d \n",animationFlag);
    loadUserConfigFile();
    printf("Unit Flag = %d \n",userinformation[0].unit);
    curSysLang = &userinformation[0].language;

#ifdef _LITE_VERSION
    SetDesktopRect(0, 0, 1024, 768);
#endif

    if (!InitMiniGUIExt()) {
        return 1;
    }

    CreateInfo.dwStyle = WS_VISIBLE;
    CreateInfo.dwExStyle = WS_EX_NONE;
    CreateInfo.spCaption = "" ;
    CreateInfo.hMenu = 0;
    CreateInfo.hCursor = 0;
    CreateInfo.hIcon = 0;
    CreateInfo.MainWindowProc = AnimationWinProc;
    CreateInfo.lx = 0; 
    CreateInfo.ty = 0;
    CreateInfo.rx = 640;
    CreateInfo.by = 480;
    CreateInfo.iBkColor = PIXEL_lightwhite;
    CreateInfo.dwAddData = 0;
    CreateInfo.dwReserved = 0;
    CreateInfo.hHosting = HWND_DESKTOP;

    hMainWnd = CreateMainWindow (&CreateInfo);
    if (hMainWnd == HWND_INVALID)
        return -1;

    ShowWindow (hMainWnd, SW_SHOWNORMAL);

    while (GetMessage(&Msg, hMainWnd)) {
        TranslateMessage(&Msg);
        DispatchMessage(&Msg);
    }

    MainWindowThreadCleanup (hMainWnd);
    MiniGUIExtCleanUp();

    /*进入程序*/
    EnterRegisterDialog(); 			//登录界面

    return 0;
} 

#ifndef _LITE_VERSION 
#include <minigui/dti.c>
#endif

