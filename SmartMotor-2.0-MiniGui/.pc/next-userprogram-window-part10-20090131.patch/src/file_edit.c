/*
 * src/file_edit.c
 *
 * The file attribute modify window implementation
 *
 * Copyright 2007-2008 @ xianweizeng@gmail.com
 *
 * Author:
 *   Zeng Xianwei
 *
 *  File Coding system: gb2312
 *    set-buffer-file-coding-system
 *
 * ChangeLog:
 *   2008-10-13 split from motorfile.c
 *
 */

/*****************************************************************************/
/*   Change Program File Attribute Dialog Window
 */


static char strFileIDDesc[] = "程序序号";
static char strFileNameDesc[] = "程序描述";
static char strFileDateDesc[] = "修改时间";
/* static char strFileUsedTimesDesc[] = "使用次数"; */

#define IDC_FILE_ATTR_ID_DESC         (IDC_FILE_ATTR_START + 0)    /* ID number */
#define IDC_FILE_ATTR_NAME_DESC       (IDC_FILE_ATTR_START + 1)    /* Name or Description */
#define IDC_FILE_ATTR_DATE_DESC       (IDC_FILE_ATTR_START + 2)    /* Modify Date*/
#define IDC_FILE_ATTR_ID              (IDC_FILE_ATTR_START + 3)    /* ID number */
#define IDC_FILE_ATTR_NAME            (IDC_FILE_ATTR_START + 4)    /* Name or Description */
#define IDC_FILE_ATTR_DATE            (IDC_FILE_ATTR_START + 5)    /* Modify Date*/

#define IDC_FILE_ATTR_F1              (IDC_FILE_ATTR_START + 6)
#define IDC_FILE_ATTR_F2              (IDC_FILE_ATTR_START + 7)

static DLGTEMPLATE showProFileAttrDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	LISTBOX_X , LISTBOX_Y, LISTBOX_FILE_W, LISTBOX_H,
	"FileAttr",
	0, 0,
	6, NULL,
	0
};

static CTRLDATA showProFileAttrCtrlsInit[] =
{
	{       /* ID number */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		30, 46, 100, 30,
		IDC_FILE_ATTR_ID_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		130, 46, 120, 30,
		IDC_FILE_ATTR_ID,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{       /* Name or Description */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		30, 96, 100, 30,
		IDC_FILE_ATTR_NAME_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER,
		130, 90, 250, 30,
		IDC_FILE_ATTR_NAME,
		"",
		0
	},
	{       /* Modify Date */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		30, 146, 100, 30,
		IDC_FILE_ATTR_DATE_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		130, 146, 120, 30,
		IDC_FILE_ATTR_DATE,
		"",
		0,
		WS_EX_TRANSPARENT
	}
};

static void initProFileAttrBkgnd(HDC hdc)
{
	FillBoxWithBitmap (hdc, 0, 0, LISTBOX_FILE_W, LISTBOX_H, &lableEditBkgnd);
}

static void initProFileAttrDlg(HWND hwnd)
{
	char strbuf[4];

	SetDlgItemText(hwnd,IDC_FILE_ATTR_ID_DESC, strFileIDDesc);
	SetDlgItemText(hwnd,IDC_FILE_ATTR_NAME_DESC, strFileNameDesc);
	SetDlgItemText(hwnd,IDC_FILE_ATTR_DATE_DESC, strFileDateDesc);

	sprintf(strbuf, "%2d", programFileAttr.proHeader.id);
	SetDlgItemText(hwnd,IDC_FILE_ATTR_ID, strbuf);
	SetDlgItemText(hwnd,IDC_FILE_ATTR_DATE, programFileAttr.proHeader.latestDate);
	SetDlgItemText(hwnd,IDC_FILE_ATTR_NAME, programFileAttr.proHeader.introduction);
}

static int getSaveProFileName(HWND hwndNameEdit)
{
	char proNameBuf[40];
	int  proNameLen=0;

	proNameLen = GetWindowTextLength(hwndNameEdit);
	if(proNameLen == 0){
		strcpy(proNameBuf,"default");
	}else{
		programFileAttrData.isProNameNull = 0; /* clear flag */
		GetWindowText(hwndNameEdit, proNameBuf, proNameLen);
	}

	memset(programFileAttr.proHeader.introduction,0,sizeof(programFileAttr.proHeader.introduction));
	strncpy(programFileAttr.proHeader.introduction,proNameBuf,proNameLen);

	/* updat file header */
	UpdateFileHeader(&programFileAttr,&programFileAttrData,0);
	saveUserProgramToFile(programFileAttr.filePathname,&(programFileAttr.proHeader),&programFileAttrData);

	return 0;
}

static void editProFileNameCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd = GetMainWindowHandle(hwnd);

	switch(nc){
	case EN_SETFOCUS:
		/* if we got the focus, change IME to chinese */
		SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)1);
		if(isIMEOpenInMotor == 0){
			ShowWindow(myIMEWindow,SW_SHOW);
			isIMEOpenInMotor =1;
		}
		break;

	case EN_ENTER: /* done */
		getSaveProFileName(hwnd);
		SendMessage(mainHwnd, MSG_COMMAND, IDCANCEL, 0);
		break;
	}
}

static int showProFileAttrDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndNameEdit;
	hwndNameEdit = GetDlgItem(hDlg,IDC_FILE_ATTR_NAME);

	switch (message) {
	case MSG_INITDIALOG:
		SetNotificationCallback(hwndNameEdit, editProFileNameCallback);

		/* create buttons: F1 and F2 */
		CreateWindow (CTRL_BUTTON,
			      " F1 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_FILE_ATTR_F1,
			      80, 195, 95, 42, hDlg, (DWORD)(&btBmpLableFn[0]));

		CreateWindow (CTRL_BUTTON,
			      " F2 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_FILE_ATTR_F2,
			      250, 195, 95, 42, hDlg, (DWORD)(&btBmpLableFn[1]));

		initProFileAttrDlg(hDlg);

		SetFocusChild(hwndNameEdit);

		return(1);

	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initProFileAttrBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_F1: /* Finish: count data and exit  */
			getSaveProFileName(hwndNameEdit);
			SendMessage(hDlg, MSG_COMMAND, IDCANCEL, 0);
			break;
		case SCANCODE_F2:  /* cancel and edit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_CURSORBLOCKDOWN:  /* change input focus */

			break;
		case SCANCODE_CURSORBLOCKUP:
			break;

		case SCANCODE_CANCEL: /* exit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		}
		break;

	case MSG_COMMAND:
		switch(wParam){
		case IDC_FILE_ATTR_F1:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_FILE_ATTR_F2:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F2, 0L);
			break;

		case IDOK:
		case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;
	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void showProgramFileAttrDialogBox (HWND hwnd)
{
	showProFileAttrDlgBox.controls = showProFileAttrCtrlsInit;
	DialogBoxIndirectParam (&showProFileAttrDlgBox, hwnd, showProFileAttrDialogBoxProc, 0L);
}

/*  Inside this function, we update programFileAttr file header,
 *  and save it to file if the header is changed.
 *
 *  Note: Only the file NAME( or should be called DESCRIPTION, they are the same)
 *        can be changed.
 */
static int showProgramFileAttr (HWND hwnd)
{
	showProgramFileAttrDialogBox(hwnd);
	return 0;
}
