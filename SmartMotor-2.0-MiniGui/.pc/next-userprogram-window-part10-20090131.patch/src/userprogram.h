/*
 *  src/userprogram.h
 *
 *  Copyright 2008 @ xianweizeng@gmail.com
 *
 */


#ifndef _SRC_USERPROGRAM_H_
#define _SRC_USERPROGRAM_H_

#define MAX_PROGRAM_CUTS	999	/* Max 999 cut datas in one program */
#define LIMIT_FRONT		3000	/* default front limit: 30.00 */

/* These value will also be used in label/average edit window */
#define LISTBOX_X               60     /* data listbox header */
#define LISTBOX_Y               130    /* data listbox header */
#define LISTBOX_W               320    /* include header */
#define LISTBOX_H               260    /* include header */

#define LISTBOX_FILE_W          450

#define CONFIG_WIN_W            524
#define CONFIG_WIN_H            LISTBOX_H


/* lable edit structure */
struct userProgramEditTypeLable_t{
	int totalLen;		/* total length */
	int lableLen;		/* lable length */
	int unusedLen;		/* unused length */
	int lableStatus;	/* edit status: 0: Not yet input */
				/*              1: Total length input done */
				/*              2: Lable length input done */
				/*              3: Unused length input done */
};

/* Average edit structure */
struct userProgramEditTypeHalf_t{
	int totalLen;		/* total length */
	int num;		/* divided numbers */
	int halfStatus;		/* edit status: 0: Not yet input */
				/*              1: Total length input done */
				/*              2: num input done */
};


/* just edit */
struct userProgramEditTypeJust_t{
	int action;		/* operation type: 0 exit 1 add 2 sub */
	int justLen;		/* length to just */
};

/* Main data structure used in program edit window */
typedef struct userProgramEditData_t {
	int  isProNameNull;		/* 1: the program name is NULL */

	int  userEditStatus;		/* Edit box status */
					/* 0  Normal input */
					/* 1  Label */
					/* 2  Average */
					/* 3  Normal input */
					/* 4  Insert input */
					/* 5  Delete input */
					/* 6  Just input */
					/* 7  Training input */
					/* 8  Add input */
					/* 9  Sub input */
	struct userProgramEditTypeLable_t lableEdit;
	struct userProgramEditTypeHalf_t  halfEdit;
	struct userProgramEditTypeJust_t  justEdit;

	int editFlag;			/* edit flag */
	int userProCuts[MAX_PROGRAM_CUTS];
	int currentCutsNum;	     /* the current editing number,
				      * 0 means the first data
				      */
	int nInputCutsNum;	     /* Inputed data number, should be >= 1,
				      * because it includes the last 0.0 data.
				      *
				      * Note:  currentCutsNum <= nInputCutsNum
				      */
	unsigned char inputbuf[10];  /* input buffer, used in computer window */
}userProgramEditData;

/* run status control structure
 */
enum sysWorkMode {
	MD_EDIT = 0,    /* Edit Mode */
	MD_AUTO = 1,    /* Auto RUN Mode */
	MD_MANUAL = 2,  /* Manual Run Mode */
	MD_AUTOCUT = 3, /* AutoCut Run Mode */
	MD_PROLIST = 4, /* Program List Mode, edit program file property */
	MD_USER = 5,    /* User Edit Mode, create/delete user */
	MD_TRAINING = 6,
	MD_MAX
};


typedef struct userProgramRunStatus_t{
	enum sysWorkMode runStatus;
	enum sysWorkMode lastRunStatus;
	int runStop;		     /* 0: STOP 1: RUNNING */
 	int pusher;		     /* Pusher flag */
	int secondFunc;		     /* Shift Key flag 1:shift active */
	int destCutNum;		     /* The destination cut number in Auto Mode */
	int whichLimit;		     /* 0: Front Limit 1: Middle Limit */

	int cutStatus;		     /* Cut position/status: 0:UP 1:Down */
	int cutActTimes;	     /* Cut action times, should be 2 in one loop */
	int pusherStatus;	     /* Pusher position/status 0:UP 1:Down */
	int pusherActTimes;

	int cutPusherActTimes;	     /* used to determine whether one loop done */

	int machineStop;	     /* Get from motor, 0:Running 1: Stop */
  	int isOneCutDone;	     /* 0: Not finished, 1: Done, can go next */
	int curDestPos;		     /* current running destination position */
	int lastCutPos;		     /* The last cut down position */
	int forCutLen;		     /* forCut length */

	int mannualFlag;	     /* 0: Normal Manual Run
				      * 1: Add
				      * 2: Sub
				      */
}userProgramRunStatus;


void refreshCurrentPos(HWND hwnd,int data);
int isInputCutsDataValid(char *pBuf,int len);
int EnterUserProgram(void);
#endif
