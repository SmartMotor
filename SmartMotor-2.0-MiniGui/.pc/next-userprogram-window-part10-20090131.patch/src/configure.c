/*
 * src/configure.c
 *
 * The setup window implementation
 *
 * Copyright 2007-2008 @ xianweizeng@gmail.com
 *
 * Author:
 *   Zeng Xianwei
 *
 *  File Coding system: gb2312
 *    set-buffer-file-coding-system
 *
 * ChangeLog:
 *	V0.1	  Quan Dongxiao
 *	V0.2	  Zeng Xianwei @ 2005-12-15
 *   2008-09-24   Change code indent
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

#include <unistd.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/select.h>

#include <minigui/common.h>
#include <minigui/minigui.h>
#include <minigui/gdi.h>
#include <minigui/window.h>
#include <minigui/control.h>

#include "motor.h"
#include "bkgndEdit.h"
#include "configure.h"
#include "motorregister.h"
#include "userprogram.h"
#include "motormain.h"
#include "help.h"

#include "rtc.h"

/*
 *  setCutPos
 */
#define IDC_CUT_POS_DESC         (IDC_CONFIG_WINDOW_START + 0)
#define IDC_CUT_POS_EDIT         (IDC_CONFIG_WINDOW_START + 1)
#define IDC_CURRENT_POS_DESC     (IDC_CONFIG_WINDOW_START + 2)
#define IDC_CURRENT_POS_EDIT     (IDC_CONFIG_WINDOW_START + 3)
#define IDC_BUTTON_RUN           (IDC_CONFIG_WINDOW_START + 4)
#define IDC_BUTTON_MODIFY        (IDC_CONFIG_WINDOW_START + 5)
#define IDC_BUTTON_EXIT          (IDC_CONFIG_WINDOW_START + 6)

/*
 *   Machine Configure
 */
#define IDC_MACH_CFG_STATUS      (IDC_CONFIG_WINDOW_START + 9)
#define IDC_LIMIT_FRONT_DESC     (IDC_CONFIG_WINDOW_START + 10)  /* current value */
#define IDC_LIMIT_MIDDLE_DESC    (IDC_CONFIG_WINDOW_START + 11)
#define IDC_LIMIT_BACK_DESC      (IDC_CONFIG_WINDOW_START + 12)
#define IDC_SPEED_SHOW           (IDC_CONFIG_WINDOW_START + 13)

#define IDC_LIMIT_FRONT_NOTE     (IDC_CONFIG_WINDOW_START + 30)
#define IDC_LIMIT_MIDDLE_NOTE    (IDC_CONFIG_WINDOW_START + 31)

/*
 * Common
 */
#define IDC_CFG_ITEM_STATUS      (IDC_CONFIG_WINDOW_START + 8)
#define IDC_CFG_BUTTON_F1        (IDC_CONFIG_WINDOW_START + 20)  /* done */
#define IDC_CFG_BUTTON_F2        (IDC_CONFIG_WINDOW_START + 21)  /* exit */


/*
 * SetPosLimit
 */
#define IDC_LIMIT_EDIT           (IDC_CONFIG_WINDOW_START + 22)
#define IDC_LIMIT_EDIT_DESC      (IDC_CONFIG_WINDOW_START + 23)

/*
 * SetSysTime
 */
#define IDC_TIME_HOUR_EDIT       (IDC_CONFIG_WINDOW_START + 24)
#define IDC_TIME_MIN_EDIT        (IDC_CONFIG_WINDOW_START + 25)
#define IDC_TIME_DESC            (IDC_CONFIG_WINDOW_START + 26)
#define IDC_TIME_SPLIT_HM        (IDC_CONFIG_WINDOW_START + 27)
#define IDC_TIME_FORMAT          (IDC_CONFIG_WINDOW_START + 28)  /* (H:M) */

/*
 * SetSysDate
 */
#define IDC_DATE_DESC            (IDC_CONFIG_WINDOW_START + 14)
#define IDC_DATE_YEAR_EDIT       (IDC_CONFIG_WINDOW_START + 24)
#define IDC_DATE_MON_EDIT        (IDC_CONFIG_WINDOW_START + 25)
#define IDC_DATE_DAY_EDIT        (IDC_CONFIG_WINDOW_START + 26)
#define IDC_DATE_SPLIT_YM        (IDC_CONFIG_WINDOW_START + 27)  /* 2008-11-03 */
#define IDC_DATE_SPLIT_MD        (IDC_CONFIG_WINDOW_START + 28)
#define IDC_DATE_FORMAT_DESC     (IDC_CONFIG_WINDOW_START + 29)  /* (Y-M-D) */

/*
 * SetSysLang
 */
#define IDC_LANG_DESC            (IDC_CONFIG_WINDOW_START + 15)
#define IDC_LANG_BT_0            (IDC_CONFIG_WINDOW_START + 16)
#define IDC_LANG_BT_1            (IDC_CONFIG_WINDOW_START + 17)

#define IDC_SPEAKER_DESC         (IDC_CONFIG_WINDOW_START + 15)
#define IDC_SPEAKER_BT_0         (IDC_CONFIG_WINDOW_START + 16)
#define IDC_SPEAKER_BT_1         (IDC_CONFIG_WINDOW_START + 17)

#define IDC_SPEED_DESC         (IDC_CONFIG_WINDOW_START + 15)
#define IDC_SPEED_BT_0         (IDC_CONFIG_WINDOW_START + 16)
#define IDC_SPEED_BT_1         (IDC_CONFIG_WINDOW_START + 17)
#define IDC_SPEED_LIST         (IDC_CONFIG_WINDOW_START + 18)
#define IDC_SPEED_CURRENT      (IDC_CONFIG_WINDOW_START + 19)


/* common window */
#define IDC_BT_LIMIT_FRONT       (IDC_CONFIG_WINDOW_START + 37)
#define IDC_BT_LIMIT_MIDDLE      (IDC_CONFIG_WINDOW_START + 38)
#define IDC_BT_LIMIT_BACK        (IDC_CONFIG_WINDOW_START + 39)
#define IDC_BT_DATE              (IDC_CONFIG_WINDOW_START + 40)
#define IDC_BT_TIME              (IDC_CONFIG_WINDOW_START + 41)
#define IDC_BT_LANG              (IDC_CONFIG_WINDOW_START + 42)
#define IDC_BT_SPEAKER           (IDC_CONFIG_WINDOW_START + 43)
#define IDC_BT_SPEED             (IDC_CONFIG_WINDOW_START + 44)
#define IDC_BT_EXIT              (IDC_CONFIG_WINDOW_START + 45)

#define IDC_BT_START             IDC_BT_LIMIT_FRONT
#define MACH_CONFIG_ITEM         9

#define CFG_ITEM_F_LIMIT         0
#define CFG_ITEM_M_LIMIT         1
#define CFG_ITEM_B_LIMIT         2
#define CFG_ITEM_DATE            3
#define CFG_ITEM_TIME            4
#define CFG_ITEM_LANG            5
#define CFG_ITEM_SPEAKER         6
#define CFG_ITEM_SPEED           7
#define CFG_ITEM_EXIT            8

#define CFG_BT_W                 100
#define CFG_BT_H                 30

#define CFG_ITEM_WIN_X           220
#define CFG_ITEM_WIN_Y           140
#define CFG_ITEM_WIN_W           340
#define CFG_ITEM_WIN_H           200

static char strConfigItems[SYSLANG_NO][MACH_CONFIG_ITEM][10] =
{
	/* 0: Chinese */
	{
		"前极限","中极限","后极限","系统日期","系统时间","语言选择","语音提示", "速度选择", "退出"
	},
	/* 1: English */
	{
		"F Limit","M Limit","B Limit","Date","Time","Language","Speaker","Speed", "Exit"
	}
};

static char strMachConfigHelp[SYSLANG_NO][48] =
{
	{"使用↑/↓/←/→ 选择设置项目"},
	{"Use U/D/L/R arrow to select config items"}
};

static char strCutPosDesc[SYSLANG_NO][10] =
{
	{"切刀位置"},
	{"Cut Pos"}
};

static char strCurrentPosDesc[SYSLANG_NO][10] =
{
	{"当前位置"},
	{"Curr Pos"}
};

static char strSysDateHelp[SYSLANG_NO][48] =
{
	{"使用↑和↓选择年/月/日"},
	{"Use Up/Down arrow to choose Y/M/D"}
};

static char strSysTimeHelp[SYSLANG_NO][32] =
{
	{"使用↑和↓选择小时或分钟"},
	{"Use Up/Down arrow to choose"}
};

static char strSysLangHelp[SYSLANG_NO][40] =
{
	{"使用↑和↓选择系统语言"},
	{"Use ↑ and ↓to choose language"}
};

static char strSysSpeakerHelp[SYSLANG_NO][40] =
{
	{"使用↑和↓选择开关"},
	{"Use ↑ and ↓to turn on/off"}
};

static char strSysSpeedHelp[SYSLANG_NO][40] =
{
	{"使用↑和↓选择马达速度(米/分钟)"},
	{"Use ↑ and ↓to select speed"}
};

static char strSpeedFactor[SYSLANG_NO][10] =
{
	{"米/分钟"},
	{"M/Min"}
};

static BITMAP configureBkgnd;
static BITMAP btBmpFileRun, btBmpFileModify, btBmpFileExit;
static BITMAP btBmpCfgFn[2];   /* F1: done  F2: exit */
static BITMAP configureListBkgnd;

static int currentConfig;      /* CFG_ITEM_XXX */

static struct comCmdData_t cmdData;

struct config_help_t{
	unsigned char * caption_help;
	unsigned char * caption_waring;
	unsigned char * caption_bkgnd;
	unsigned char * msg_cutdata_overflow;
	unsigned char * msg_step_overflow;
	unsigned char * msg_config_ok;
	unsigned char * help_config;
	unsigned char * help_date;
	unsigned char * help_time;
	unsigned char * help_lang;
	unsigned char * help_flash;
	unsigned char * help_cutpos;
	unsigned char * help_limit;
	unsigned char * help_step;
	unsigned char * help_bkgnd;
};

static struct config_help_t config_help[SYSLANG_NO] =
{
	{	caption_help_cn,
		caption_waring_cn,
		caption_bkgnd_cn,
		msg_cutdata_overflow_cn,
		msg_step_overflow_cn,
		msg_config_ok_cn,
		help_config_cn,
		help_date_cn,
		help_time_cn,
		help_lang_cn,
		help_flash_cn,
		help_cutpos_cn,
		help_limit_cn,
		help_step_cn,
		help_bkgnd_cn
	},

	{	caption_help_en,
		caption_waring_en,
		caption_bkgnd_cn,
		msg_cutdata_overflow_en,
		msg_step_overflow_en,
		msg_config_ok_en,
		help_config_en,
		help_date_en,
		help_time_en,
		help_lang_en,
		help_flash_en,
		help_cutpos_en,
		help_limit_en,
		help_step_en,
		help_bkgnd_en
	}
};


/*******************************************************************/
/*               GLOBAL RTC FUNCTIONS                              */

/*****************************************************************************************/

/*
 *  Cut position config
 *
 *  Called in MD_MANUAL and MD_AUTO.
 */

static DLGTEMPLATE setCutPosDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	LISTBOX_X , LISTBOX_Y, CONFIG_WIN_W, CONFIG_WIN_H,
	"setCutPos",
	0, 0,
	4, NULL,
	0
};

static CTRLDATA setCutPosCtrlsInit[] =
{
	{
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		130, 50, 100, 30,
		IDC_CUT_POS_DESC,
		"",
		0,
		WS_EX_TRANSPARENT,
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER,
		240, 50, 150, 30,
		IDC_CUT_POS_EDIT,
		"",
		0
	},
	{
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		130, 100, 100, 30,
		IDC_CURRENT_POS_DESC,
		"",
		0,
		WS_EX_TRANSPARENT,
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER | ES_READONLY,
		240, 100, 150, 30,
		IDC_CURRENT_POS_EDIT,
		"",
		0,
		WS_EX_TRANSPARENT
	}
};

static int loadPosConfigBkgndFiles(void)
{
	if (LoadBitmap (HDC_SCREEN, &configureBkgnd, "./res/configItemBkgnd.jpg"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpFileRun, "./res/buttonRunBig.bmp"))  /* Run */
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpFileModify, "./res/buttonModifyBig.bmp"))  /* Modify */
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpFileExit, "./res/buttonClearBig.bmp"))  /* Exit */
		return 1;

	return 0;
}

static void unloadPosConfigBkgndFiles(void)
{

	UnloadBitmap(&configureBkgnd);
	UnloadBitmap(&btBmpFileRun);
	UnloadBitmap(&btBmpFileModify);
	UnloadBitmap(&btBmpFileExit);
}

static void initSetCutPosBkgnd(HDC hdc)
{
	FillBoxWithBitmap (hdc, 0, 0, CONFIG_WIN_W, CONFIG_WIN_H, &configureBkgnd);

}

/*
 * Modify current position
 */
static void modifyCurrentCutPos(HWND mainWnd,HWND editWnd)
{
	int dataLen;
	char dataBuf[10];
	char cmdbuf[13];
	int unit = currentuser->unit;
	int ret,data;
	dataLen = GetWindowTextLength(editWnd); /* get input data */
	GetWindowText(editWnd,dataBuf,dataLen);
	ret = isInputCutsDataValid(dataBuf,dataLen);
	if(ret >= 0 ){
		cmdData.flag = 'P';
		cmdData.status = 0x30;
		if(unit){
			data = inch_to_mm(ret);
			ret = data;
		}
		cmdData.data = ret;
		setCmdFrame(&cmdData,cmdbuf);
		sendCommand(cmdbuf);  /* cut position modify */

	}else {
		MessageBox(mainWnd,config_help[*curSysLang].msg_cutdata_overflow,
			   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		SetWindowText(editWnd,""); /* clear input box */
	}
}

static int runToInputPos(HWND editHwnd)
{
	int dataLen;
	char dataBuf[10];
	char cmdbuf[13];
	int unit = currentuser->unit;
	int ret,data;
	dataLen = GetWindowTextLength(editHwnd);
	GetWindowText(editHwnd,dataBuf,dataLen);
	ret=isInputCutsDataValid(dataBuf,dataLen);
	if(ret >= 0 ){
		cmdData.flag = 'G';
		cmdData.status = 0x33;
		if(unit){
			data = inch_to_mm(ret);
			ret = data;
		}
		cmdData.data = ret;
		setCmdFrame(&cmdData,cmdbuf);
		sendCommand(cmdbuf);
	}else {
		MessageBox(editHwnd,config_help[*curSysLang].msg_cutdata_overflow,
			   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		SetWindowText(editHwnd,"");
	}
	return(ret);
}

#if 0      /* invalidate */
static void setCutPosEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	int dataLen;
	char dataBuf[10];
	char cmdbuf[13];
	int unit = currentuser->unit;
	int ret,data;

	mainHwnd = GetMainWindowHandle(hwnd);
	switch(nc){
	case EN_ENTER:
		dataLen = GetWindowTextLength(hwnd);
		GetWindowText(hwnd,dataBuf,dataLen);
		ret=isInputCutsDataValid(dataBuf,dataLen);
		if(ret >= 0 ){
			cmdData.flag = 'G';
			cmdData.status = 0x33;
			if(unit){
				data = inch_to_mm(ret);
				ret = data;
			}
			cmdData.data = ret;
			setCmdFrame(&cmdData,cmdbuf);
			sendCommand(cmdbuf);
		}else {
			MessageBox(hwnd,config_help[*curSysLang].msg_cutdata_overflow,
				   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
			SetWindowText(hwnd,"");
		}

		break;
	}
}
#endif

static void initSetCutPosDlg(HWND hwnd)
{
	int lang = *curSysLang;
	HWND hwndCutPosDesc = GetDlgItem(hwnd,IDC_CUT_POS_DESC);
	HWND hwndCurrentPosDesc = GetDlgItem(hwnd,IDC_CURRENT_POS_DESC);

	SetWindowText(hwndCutPosDesc, strCutPosDesc[lang]);
	SetWindowText(hwndCurrentPosDesc, strCurrentPosDesc[lang]);

#if 0
	/* set background color */
	SetWindowBkColor(hwndCutPosDesc, PIXEL_yellow);
	InvalidateRect(hwndCutPosDesc, NULL, TRUE);

	SetWindowBkColor(hwndCurrentPosDesc, PIXEL_yellow);
	InvalidateRect(hwndCurrentPosDesc, NULL, TRUE);
#endif
}

static int modify_and_exit = 0;  /* when the Cut Pos is set succefully,
				  * exit this window.
				  */

static int setCutPosDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	char cmdbuf[12];
	int data;
	int curPos;
	int unit = currentuser->unit;
	HWND hwndCutPosEdit = GetDlgItem(hDlg,IDC_CUT_POS_EDIT);
	HWND hwndCurrentPosEdit = GetDlgItem(hDlg,IDC_CURRENT_POS_EDIT);

	switch (message)
	{
    	case MSG_INITDIALOG:
		/* SetNotificationCallback(hwndCutPosEdit, setCutPosEditCallback); */
		curPos = motorStatus.currentCutPos;
		refreshCurrentPos(hwndCurrentPosEdit,curPos);

		initSetCutPosDlg(hDlg);
		CreateWindow (CTRL_BUTTON,
			      " run ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_BUTTON_RUN,
			      100, 160, 94, 42, hDlg, (DWORD)(&btBmpFileRun));

		CreateWindow (CTRL_BUTTON,
			      " modify ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_BUTTON_MODIFY,
			      210, 160, 94, 42, hDlg, (DWORD)(&btBmpFileModify));

		CreateWindow (CTRL_BUTTON,
			      " exit ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_BUTTON_EXIT,
			      320, 160, 94, 42, hDlg, (DWORD)(&btBmpFileExit));

		modify_and_exit = 0;

		SetFocusChild(hwndCutPosEdit);

		return(1);

    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initSetCutPosBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_FORWARD:         /* Forward command */
			memset(cmdbuf,0,12);
			cmdData.flag = 'F';
			cmdData.status = 0x30;
			cmdData.data = 0;
			setCmdFrame(&cmdData,cmdbuf);
			sendCommand(cmdbuf);
			break;
		case SCANCODE_BACKWARD:         /* Backward command */
			memset(cmdbuf,0,12);
			cmdData.flag = 'F';
			cmdData.status = 0x31;
			cmdData.data = 0;
			setCmdFrame(&cmdData,cmdbuf);
			sendCommand(cmdbuf);
			break;
		case SCANCODE_FORWARD_HS:         /* High speed forward command */
			memset(cmdbuf,0,12);
			cmdData.flag = 'F';
			cmdData.status = 0x32;
			cmdData.data = 0;
			setCmdFrame(&cmdData,cmdbuf);
			sendCommand(cmdbuf);
			break;
		case SCANCODE_BACKWARD_HS:        /* High speed backward command */
			memset(cmdbuf,0,12);
			cmdData.flag = 'F';
			cmdData.status = 0x33;
			cmdData.data = 0;
			setCmdFrame(&cmdData,cmdbuf);
			sendCommand(cmdbuf);
			break;

		case SCANCODE_MODIFY:             /* Modify Position(just) */
			modifyCurrentCutPos(hDlg,hwndCutPosEdit);
			break;

		case SCANCODE_ENTER:              /* modify and exit */
			modifyCurrentCutPos(hDlg,hwndCutPosEdit);
			modify_and_exit = 1;      /* set exit flag, handled in MSG_RECV */
#if 0
			for(index=0;index<50;index++)  /* wait for lower machine */
				usleep(10000);
			SendMessage(hDlg,MSG_RECV,0,0); /* update data mannualy */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
#endif
			break;

		case SCANCODE_DELETE:           /* clear */
			SetFocusChild(hwndCutPosEdit);
			SetWindowText(hwndCutPosEdit,"");
			break;

		case SCANCODE_RUN:
			runToInputPos(hwndCutPosEdit);

			break;
		case SCANCODE_HELP:
			MessageBox(hDlg,config_help[*curSysLang].help_cutpos,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		case SCANCODE_CANCEL:           /* exit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		}
		break;

    	case MSG_RECV:
		recvCommand(cmdbuf);            /* copy to user buffer */
		//ret = isCmdValid(cmdbuf);	/* no checksum currently */
		//if(ret){
	    	memset(&cmdData,0,sizeof(cmdData));
	    	getCmdFrame(&cmdData,cmdbuf);
	    	if(cmdData.flag == 'G'){        /* Normal data frame */
			data = cmdData.data;
			if(unit){
				data = mm_to_inch(cmdData.data);
			}
			motorStatus.currentCutPos = data;          /* update data */
			motorStatus.devStatus = cmdData.status;	   /* update status */
			refreshCurrentPos(hwndCurrentPosEdit,data);/* show */
		}
	    	if(cmdData.flag == 'P'){        /* modify frame */
			data = cmdData.data;
			if(unit){
				data = mm_to_inch(cmdData.data);
			}
			motorStatus.currentCutPos = data;
			motorStatus.devStatus = cmdData.status;
			refreshCurrentPos(hwndCurrentPosEdit,data);

			if(modify_and_exit == 1){
				/* maybe we should check whether the received data
				 * is same as the data user inputed.
				 */
				SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			}
		}
		//}
		break;

    	case MSG_COMMAND:
		switch(wParam){
		case IDC_BUTTON_RUN:
			SetFocusChild(hwndCutPosEdit);
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_RUN, 0L);
			break;

		case IDC_BUTTON_MODIFY:
			SetFocusChild(hwndCutPosEdit);
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_MODIFY, 0L);
			break;

		case IDC_BUTTON_EXIT:
	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;


	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}


static void setCutPosDialogBox (HWND hwnd)
{
	setCutPosDlgBox.controls = setCutPosCtrlsInit;
	DialogBoxIndirectParam (&setCutPosDlgBox, hwnd,setCutPosDialogBoxProc, 0L);
}

int setCutPos (HWND hwnd)
{
	//printf("setCutPos().\n");
	loadPosConfigBkgndFiles();

	setCutPosDialogBox(hwnd);		// Should not return except receive CLOSE MSG

	unloadPosConfigBkgndFiles();
	return 0;

}

/*****************************************************************************************/

/*
 *  Machine Config Window
 */

/*****************************************************************************************/

static DLGTEMPLATE setPosLimitDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	CFG_ITEM_WIN_X, CFG_ITEM_WIN_Y, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H,
	"setPosLimit",
	0, 0,
	2, NULL,
	0
};

static CTRLDATA setPosLimitCtrlsInit[] =
{
	{       /* description */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		30, 44, 100, 30,
		IDC_LIMIT_EDIT_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER,
		140, 40, 120, 30,
		IDC_LIMIT_EDIT,
		"",
		0
	}
};

static void initSetPosLimitBkgnd(HDC hdc)
{
	FillBoxWithBitmap (hdc, 0, 0, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H, &configureListBkgnd);
}


static void setPosLimitEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	int dataLen,data;
	char dataBuf[10];
	int status = currentConfig;
	int unit = currentuser->unit;
	char cmdSendBuf[13];
	memset(cmdSendBuf,0,12);
	cmdData.flag = 'L';

	mainHwnd = GetMainWindowHandle(hwnd);
	switch(nc){
	case EN_SETFOCUS:
		// Do nothing
		break;
	case EN_ENTER:
		dataLen = GetWindowTextLength(hwnd);
		GetWindowText(hwnd,dataBuf,dataLen);
		int ret=isInputCutsDataValid(dataBuf,dataLen);
		if(ret >= 0 ){
			switch(status){
			case CFG_ITEM_F_LIMIT:
				currentuser->frontlimit = ret;
				saveUserConfigFile(0);
				cmdData.status = 0x30;
				break;
			case CFG_ITEM_M_LIMIT:
				currentuser->middlelimit = ret;
				saveUserConfigFile(0);
				cmdData.status = 0x31;
				break;
			case CFG_ITEM_B_LIMIT:
				currentuser->backlimit = ret;
				saveUserConfigFile(0);
				cmdData.status = 0x32;
				break;
			}

			if(unit){
				data = inch_to_mm(ret);
			} else {
				data = ret;
			}
			cmdData.data = data;
			setCmdFrame(&cmdData,cmdSendBuf);
			sendCommand(cmdSendBuf);
			MessageBox(hwnd,config_help[*curSysLang].msg_config_ok,
				   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
			SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0); /* exit */
		}else {
			MessageBox(hwnd,config_help[*curSysLang].msg_cutdata_overflow,
				   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
			SetWindowText(hwnd,""); /* clear */
		}
		break;
	}
}

static void initSetPosLimitDlg(HWND hDlg)
{
	int lang = *curSysLang;
	char strBuf[16];

	int len = strlen(strConfigItems[lang][currentConfig]);

	if(len > 15)
		len = 15;

	strncpy(strBuf, strConfigItems[lang][currentConfig], len);
	strBuf[len] = 0;

	SetDlgItemText(hDlg, IDC_LIMIT_EDIT_DESC, strBuf);
}

static int setPosLimitDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndCutPosEdit = GetDlgItem(hDlg,IDC_LIMIT_EDIT);

	switch (message)
	{
    	case MSG_INITDIALOG:
		SetNotificationCallback(hwndCutPosEdit, setPosLimitEditCallback);

		CreateWindow (CTRL_BUTTON,
			      " F1 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F1,
			      40, 120, 94, 42, hDlg, (DWORD)(&btBmpCfgFn[0]));

		CreateWindow (CTRL_BUTTON,
			      " F2 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F2,
			      190, 120, 94, 42, hDlg, (DWORD)(&btBmpCfgFn[1]));

		initSetPosLimitDlg(hDlg);
		SetFocusChild(hwndCutPosEdit);
		return(1);

    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}
		initSetPosLimitBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_F1: /* done  */
			SetFocusChild(hwndCutPosEdit);
			SendMessage(hwndCutPosEdit,MSG_KEYDOWN,SCANCODE_ENTER,0L);

			break;
		case SCANCODE_F2:  /* cancel and edit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_CANCEL:
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_HELP:
			MessageBox(hDlg,config_help[*curSysLang].help_limit,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		}
		break;
    	case MSG_COMMAND:
		switch(wParam){
		case IDC_CFG_BUTTON_F1:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_CFG_BUTTON_F2:  /* Exit, call EndDialog() directly */
			/* SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0); */
			EndDialog(hDlg,IDOK);
			break;
	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;


	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}


static void setPosLimitDialogBox (HWND hwnd)
{
	setPosLimitDlgBox.controls = setPosLimitCtrlsInit;
	DialogBoxIndirectParam (&setPosLimitDlgBox, hwnd,setPosLimitDialogBoxProc, 0L);
}
static int setPosLimit (HWND hwnd)
{
	//printf("setPosLimit().\n");
	setPosLimitDialogBox(hwnd);		// Should not return except receive CLOSE MSG
	return 0;

}


static DLGTEMPLATE setSysDateDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	CFG_ITEM_WIN_X, CFG_ITEM_WIN_Y, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H,
	"setSysDate",
	0, 0,
	8, NULL,
	0
};

static CTRLDATA setSysDateCtrlsInit[] =
{
	{       /* description */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		30, 44, 100, 30,
		IDC_DATE_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER,
		140, 40, 50, 30,
		IDC_DATE_YEAR_EDIT,
		"",
		0
	},
	{       /* - */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		190, 44, 20, 30,
		IDC_DATE_SPLIT_YM,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER,
		210, 40, 25, 30,
		IDC_DATE_MON_EDIT,
		"",
		0
	},
	{       /* - */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		235, 44, 20, 30,
		IDC_DATE_SPLIT_MD,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER,
		255, 40, 32, 30,
		IDC_DATE_DAY_EDIT,
		"",
		0
	},
	{       /* Y-M-D */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		160, 70, 120, 30,
		IDC_DATE_FORMAT_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{       /* operation help */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		10, 175, 320, 24,
		IDC_CFG_ITEM_STATUS,
		"",
		0,
		WS_EX_TRANSPARENT
	}
};


static void initSetSysDateBkgnd(HDC hdc)
{
	FillBoxWithBitmap(hdc, 0, 0, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H, &configureListBkgnd);
}

/*
 * Date check
 *   flag:  0 Year
 *          1 Month
 *          2 Day
 *
 *   return  0:  error
 *           >0: right data
 */
static int isInputDateValid(char *pBuf,int flag)
{
	int data = atoi(pBuf);

	switch(flag){
	case 0: /* Year */
		if((data < 2000)|| (data>2099)){
			return(0);
		}
		break;
	case 1:	/* Month */
		if((data < 1)|| (data>12)){
			return(0);
		}
		break;
	case 2:	/* Day */
		if((data < 1)|| (data>31)){
			return(0);
		}
		break;
	}

	return(data);
}

/* get user input and update system time
 */
static int get_update_system_date(HWND hDlg)
{
	int index,ret;
	char cutDataBuf[10];
	int  cutDataLen = 0;
	HWND hwndEdit[3];

	struct tm *tm;
	struct timeval tv;
	time_t curtime;
	int status = 0;

	hwndEdit[0] = GetDlgItem(hDlg,IDC_DATE_YEAR_EDIT);
	hwndEdit[1] = GetDlgItem(hDlg,IDC_DATE_MON_EDIT);
	hwndEdit[2] = GetDlgItem(hDlg,IDC_DATE_DAY_EDIT);

	/* get system time */
	gettimeofday(&tv, NULL);

	curtime = tv.tv_sec;
	tm = localtime(&curtime);

	for(index = 0; index < 3; index++){
		memset(cutDataBuf,0,10);
		cutDataLen = GetWindowTextLength(hwndEdit[index]);
		GetWindowText(hwndEdit[index],cutDataBuf,cutDataLen);
		ret = isInputDateValid(cutDataBuf,index);
		if(ret > 0 ){
			switch(index){
			case 0:
				tm->tm_year = ret - 1900;
				status |= 0x1;
				break;
			case 1:
				tm->tm_mon = ret - 1;
				status |= 0x2;
				break;
			case 2:
				tm->tm_mday = ret;
				status |= 0x4;
				break;
			}
		}else{
			SetWindowText(hwndEdit[index],"");
			SetFocusChild(hwndEdit[index]);
			//printf("getlabledata status=%d \n",pProEdit->lableEdit.lableStatus);
			return(-1);
		}
	}

	if(status != 7) {
		printf("get_update_system_date error.\n");
		return -1;
	}

#if 0
	printf("set tm: %04d-%02d-%02d %02d:%02d:%02d\n", tm->tm_year,tm->tm_mon,tm->tm_mday,
	       tm->tm_hour,tm->tm_min,tm->tm_sec);
#endif

	tv.tv_sec = mktime(tm);
	if(tv.tv_sec == -1){
		printf("get_update_system_date: mktime failed.\n");
		return -1;
	}

	ret = settimeofday(&tv, NULL);
	if(ret != 0) {
		printf("update system date failed: %d\n", ret);
		return -1;
	}

	return 0;
}

static void setSysYearEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	HWND monthHwnd;
	int year;
	int dataLen;
	char dataBuf[6];

	mainHwnd = GetMainWindowHandle(hwnd);
	monthHwnd = GetDlgItem(mainHwnd,IDC_DATE_MON_EDIT);

	switch(nc){
	case EN_CHANGE:
		dataLen = GetWindowTextLength(hwnd);
		if(dataLen == 4){  /* Year done */
			GetWindowText(hwnd,dataBuf,dataLen);
			year = isInputDateValid(dataBuf,0);
			if(!year){
				SetWindowText(hwnd,"");
			} else {
				SetFocusChild(monthHwnd);
			}
		}else if(dataLen > 4){
			SetWindowText(hwnd,"");
		}

		break;
	}
}
static void setSysMonthEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	HWND dayHwnd;
	int month;
	int dataLen;
	char dataBuf[6];

	mainHwnd = GetMainWindowHandle(hwnd);
	dayHwnd = GetDlgItem(mainHwnd,IDC_DATE_DAY_EDIT);

	switch(nc){
	case EN_CHANGE:
		dataLen = GetWindowTextLength(hwnd);
		if(dataLen == 2){
			GetWindowText(hwnd,dataBuf,dataLen);
			month = isInputDateValid(dataBuf,1);
			if(!month){
				SetWindowText(hwnd,"");
			} else {
				SetFocusChild(dayHwnd);
			}
		}else if(dataLen > 2){
			SetWindowText(hwnd,"");
		}

		break;
	}
}

static void setSysDayEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	int day;
	int dataLen;
	char dataBuf[6];

	mainHwnd = GetMainWindowHandle(hwnd);

	switch(nc){
	case EN_CHANGE:
		dataLen = GetWindowTextLength(hwnd);
		if(dataLen == 2){
			GetWindowText(hwnd,dataBuf,dataLen);
			day = isInputDateValid(dataBuf,2);
			if(!day){
				SetWindowText(hwnd,"");	// clear
			}
		} else if(dataLen > 2){
			SetWindowText(hwnd,"");
		}

		break;
	}
}

static void initSetSysDateDlg(HWND hDlg)
{
	int lang = *curSysLang;
	char strBuf[16];

	int len = strlen(strConfigItems[lang][currentConfig]);

	if(len > 15)
		len = 15;

	strncpy(strBuf, strConfigItems[lang][currentConfig], len);
	strBuf[len] = 0;

	SetDlgItemText(hDlg, IDC_DATE_DESC, strBuf);

	/* - */
	SetDlgItemText(hDlg, IDC_DATE_SPLIT_YM, "-");
	SetDlgItemText(hDlg, IDC_DATE_SPLIT_MD, "-");

	/* Y-M-D */
	SetDlgItemText(hDlg, IDC_DATE_FORMAT_DESC, "(Y - M - D)");

	SetDlgItemText(hDlg, IDC_CFG_ITEM_STATUS, strSysDateHelp[lang]);
	/* Also change its bkcolor */
}

static int setSysDateDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndTmp,hwndEdit[3];
	hwndEdit[0] = GetDlgItem(hDlg,IDC_DATE_YEAR_EDIT);
	hwndEdit[1] = GetDlgItem(hDlg,IDC_DATE_MON_EDIT);
	hwndEdit[2] = GetDlgItem(hDlg,IDC_DATE_DAY_EDIT);

	char dispBuf[6];
	struct tm * tm;
	struct timeval tv;
	int ret;

	switch (message)
	{
    	case MSG_INITDIALOG:
		SetNotificationCallback(hwndEdit[0], setSysYearEditCallback);
		SetNotificationCallback(hwndEdit[1], setSysMonthEditCallback);
		SetNotificationCallback(hwndEdit[2], setSysDayEditCallback);

		gettimeofday(&tv, NULL);
		tm = localtime(&tv.tv_sec);

		sprintf(dispBuf,"%04d",tm->tm_year + 1900);
		SetWindowText(hwndEdit[0],dispBuf);
		SendMessage(hwndEdit[0], EM_SETCARETPOS, 0, 4);

		sprintf(dispBuf,"%02d",tm->tm_mon + 1);
		SetWindowText(hwndEdit[1],dispBuf);
		SendMessage(hwndEdit[1], EM_SETCARETPOS, 0, 2);

		sprintf(dispBuf,"%02d",tm->tm_mday);
		SetWindowText(hwndEdit[2],dispBuf);
		SendMessage(hwndEdit[2], EM_SETCARETPOS, 0, 2);

		CreateWindow (CTRL_BUTTON,
			      " F1 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F1,
			      40, 120, 94, 42, hDlg, (DWORD)(&btBmpCfgFn[0]));

		CreateWindow (CTRL_BUTTON,
			      " F2 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F2,
			      190, 120, 94, 42, hDlg, (DWORD)(&btBmpCfgFn[1]));

		initSetSysDateDlg(hDlg);

		SetFocusChild(hwndEdit[0]);
		return(1);

    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initSetSysDateBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_CURSORBLOCKUP:
			hwndTmp=GetFocusChild(hDlg);
			if(hwndTmp == hwndEdit[0]){
				break;
			}
			if(hwndTmp == hwndEdit[2]){
				SetFocusChild(hwndEdit[1]);
			}
			if(hwndTmp == hwndEdit[1]){
				SetFocusChild(hwndEdit[0]);
			}
			break;
		case SCANCODE_CURSORBLOCKDOWN:
			hwndTmp=GetFocusChild(hDlg);
			if(hwndTmp == hwndEdit[2]){
				break;
			}
			if(hwndTmp == hwndEdit[0]){
				SetFocusChild(hwndEdit[1]);
			}
			if(hwndTmp == hwndEdit[1]){
				SetFocusChild(hwndEdit[2]);
			}
			break;

		case SCANCODE_F2:  /* cancel and edit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_F1:  /* done  */
		case SCANCODE_ENTER:
			ret = get_update_system_date(hDlg);
			if(ret < 0) { /* error, retry */
				SetFocusChild(hwndEdit[0]);
				break;
			}

			/* write to RTC */
			update_rtctime();

			/* exit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_CANCEL:
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_HELP:
			MessageBox(hDlg,config_help[*curSysLang].help_date,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		}
		break;
    	case MSG_COMMAND:
		switch(wParam){
		case IDC_CFG_BUTTON_F1:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_CFG_BUTTON_F2:  /* Exit, call EndDialog() directly */
			EndDialog(hDlg,IDOK);
			break;

	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;


	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setSysDateDialogBox (HWND hwnd)
{
	setSysDateDlgBox.controls = setSysDateCtrlsInit;
	DialogBoxIndirectParam (&setSysDateDlgBox, hwnd,setSysDateDialogBoxProc, 0L);
}

static int setSysDate (HWND hwnd)
{
	setSysDateDialogBox(hwnd);
	return 0;
}

/***********************************************************/

static DLGTEMPLATE setSysTimeDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	CFG_ITEM_WIN_X, CFG_ITEM_WIN_Y, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H,
	"setSysTime",
	0, 0,
	6, NULL,
	0
};

static CTRLDATA setSysTimeCtrlsInit[] =
{
	{       /* description */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		30, 44, 100, 30,
		IDC_TIME_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER,
		140, 40, 40, 30,
		IDC_TIME_HOUR_EDIT,
		"",
		0
	},
	{       /* : */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		180, 44, 20, 30,
		IDC_TIME_SPLIT_HM,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER,
		200, 40, 40, 30,
		IDC_TIME_MIN_EDIT,
		"",
		0
	},
	{       /* Hour:Min */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		140, 75, 100, 30,
		IDC_TIME_FORMAT,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{       /* operation help */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		10, 175, 320, 24,
		IDC_CFG_ITEM_STATUS,
		"",
		0,
		WS_EX_TRANSPARENT
	}
};


static void initSetSysTimeBkgnd(HDC hdc)
{
	FillBoxWithBitmap(hdc, 0, 0, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H, &configureListBkgnd);
}

static int isInputTimeValid(char *pBuf,int flag)
{
	int data = atoi(pBuf);

	switch(flag){
	case 0:
		if((data < 0)|| (data>23)){
			return(-1);
		}
		break;
	case 1:
		if((data < 0)|| (data>59)){
			return(-1);
		}
		break;
	}

	return(data);

}
static void setSysHourEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	int hour;
	int dataLen;
	char dataBuf[6];

	HWND mainHwnd = GetMainWindowHandle(hwnd);
	HWND minuteHwnd = GetDlgItem(mainHwnd,IDC_TIME_MIN_EDIT);

	switch(nc){
	case EN_CHANGE:
		dataLen = GetWindowTextLength(hwnd);
		if(dataLen == 2){
			GetWindowText(hwnd,dataBuf,dataLen);
			hour = isInputTimeValid(dataBuf,0);
			if(hour < 0){
				SetWindowText(hwnd,"");
			} else {
				SetFocusChild(minuteHwnd);
			}
		} else if(dataLen > 2){
			SetWindowText(hwnd,"");
		}
		break;
	}
}
static void setSysMinuteEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	int minute;
	int dataLen;
	char dataBuf[6];

	switch(nc){
	case EN_CHANGE: /* 0 - 9 pressed */
		dataLen = GetWindowTextLength(hwnd);
		if(dataLen == 2){
			GetWindowText(hwnd,dataBuf,dataLen);
			minute = isInputTimeValid(dataBuf,1);
			if(minute < 0){
				SetWindowText(hwnd,"");
			}
		}else if(dataLen > 2){
			SetWindowText(hwnd,"");
		}
		break;
	}
}

static int get_update_system_time(HWND hDlg)
{
	char dataBuf[10];
	int dataLen = 0;
	int ret;

	struct tm *tm;
	struct timeval tv;
	time_t curtime;
	int status = 0;

	HWND hwndHourEdit = GetDlgItem(hDlg,IDC_TIME_HOUR_EDIT);
	HWND hwndMinEdit = GetDlgItem(hDlg,IDC_TIME_MIN_EDIT);

	/* get system time */
	gettimeofday(&tv, NULL);

	curtime = tv.tv_sec;
	tm = localtime(&curtime);

	dataLen = GetWindowTextLength(hwndHourEdit);
	if(dataLen == 2){
		GetWindowText(hwndHourEdit,dataBuf,dataLen);
		ret = isInputTimeValid(dataBuf,1);
		if(ret < 0){
			SetWindowText(hwndHourEdit,"");
			return -1;
		} else {
			tm->tm_hour = ret;
			status |= 0x01;
		}
	}else if(dataLen > 2){  /* Overflow */
		SetWindowText(hwndHourEdit,"");
		return -1;
	}

	dataLen = GetWindowTextLength(hwndMinEdit);
	if(dataLen == 2){
		GetWindowText(hwndMinEdit,dataBuf,dataLen);
		ret = isInputTimeValid(dataBuf,1);
		if(ret < 0){
			SetWindowText(hwndMinEdit,"");
			return -1;
		} else {
			tm->tm_min = ret;
			status |= 0x02;
		}
	}else if(dataLen > 2){
		SetWindowText(hwndMinEdit,"");
		return -1;
	}

	if(status != 3) {
		printf("get_update_system_time error.\n");
		return -1;
	}

#if 0
	printf("set tm: %04d-%02d-%02d %02d:%02d:%02d\n", tm->tm_year,tm->tm_mon,tm->tm_mday,
	       tm->tm_hour,tm->tm_min,tm->tm_sec);
#endif

	tv.tv_sec = mktime(tm);
	if(tv.tv_sec == -1){
		printf("get_update_system_time: mktime failed.\n");
		return -1;
	}

	ret = settimeofday(&tv, NULL);
	if(ret != 0) {
		printf("update system time failed: %d\n", ret);
		return -1;
	}

	return 0;
}

static void initSetSysTimeDlg(HWND hDlg)
{
	int lang = *curSysLang;
	char strBuf[16];

	int len = strlen(strConfigItems[lang][currentConfig]);

	if(len > 15)
		len = 15;

	strncpy(strBuf, strConfigItems[lang][currentConfig], len);
	strBuf[len] = 0;

	SetDlgItemText(hDlg, IDC_TIME_DESC, strBuf);

	/* : */
	SetDlgItemText(hDlg, IDC_TIME_SPLIT_HM, ":");

	/* Hour : Min */
	SetDlgItemText(hDlg, IDC_TIME_FORMAT, "(H : M)");

	SetDlgItemText(hDlg, IDC_CFG_ITEM_STATUS, strSysTimeHelp[lang]);
	/* Also change its bkcolor */
}

static int setSysTimeDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndHourEdit = GetDlgItem(hDlg,IDC_TIME_HOUR_EDIT);
	HWND hwndMinEdit = GetDlgItem(hDlg,IDC_TIME_MIN_EDIT);

	char dispBuf[6];
	struct tm * tm;
	struct timeval tv;
	int ret;

	switch (message)
	{
    	case MSG_INITDIALOG:
		SetNotificationCallback(hwndHourEdit, setSysHourEditCallback);
		SetNotificationCallback(hwndMinEdit, setSysMinuteEditCallback);

		gettimeofday(&tv, NULL);
		tm = localtime(&tv.tv_sec);

		sprintf(dispBuf,"%02d",tm->tm_hour);
		SetWindowText(hwndHourEdit,dispBuf);
		SendMessage(hwndHourEdit, EM_SETCARETPOS, 0, 2);
		sprintf(dispBuf,"%02d",tm->tm_min);
		SetWindowText(hwndMinEdit,dispBuf);
		SendMessage(hwndMinEdit, EM_SETCARETPOS, 0, 2);

		CreateWindow (CTRL_BUTTON,
			      " F1 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F1,
			      40, 120, 94, 42, hDlg, (DWORD)(&btBmpCfgFn[0]));

		CreateWindow (CTRL_BUTTON,
			      " F2 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F2,
			      190, 120, 94, 42, hDlg, (DWORD)(&btBmpCfgFn[1]));

		initSetSysTimeDlg(hDlg);

		SetFocusChild(hwndHourEdit);
		return(1);

    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initSetSysTimeBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_CURSORBLOCKUP:
			SetFocusChild(hwndHourEdit);
			break;

		case SCANCODE_CURSORBLOCKDOWN:
			SetFocusChild(hwndMinEdit);
			break;

		case SCANCODE_F2:  /* cancel and edit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_F1: /* done  */
		case SCANCODE_ENTER:
			ret = get_update_system_time(hDlg);
			if(ret < 0){
				SetFocusChild(hwndHourEdit);
				break;
			}

			/* write to RTC */
			update_rtctime();

			/* exit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_CANCEL:
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_HELP:
			MessageBox(hDlg,config_help[*curSysLang].help_time,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		}
		break;
    	case MSG_COMMAND:
		switch(wParam){
		case IDC_CFG_BUTTON_F1:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_CFG_BUTTON_F2:  /* Exit, call EndDialog() directly */
			EndDialog(hDlg,IDOK);
			break;
	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;


	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setSysTimeDialogBox (HWND hwnd)
{
	setSysTimeDlgBox.controls = setSysTimeCtrlsInit;
	DialogBoxIndirectParam (&setSysTimeDlgBox, hwnd,setSysTimeDialogBoxProc, 0L);
}

static int setSysTime (HWND hwnd)
{
	setSysTimeDialogBox(hwnd);
	return 0;
}

/***********************************************************/

static DLGTEMPLATE setSysLangDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	CFG_ITEM_WIN_X, CFG_ITEM_WIN_Y, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H,
	"setSysLang",
	0, 0,
	2, NULL,
	0
};

static CTRLDATA setSysLangCtrlsInit[] =
{
	{       /* description */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		30, 24, 100, 30,
		IDC_LANG_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{       /* operation help */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		10, 175, 320, 24,
		IDC_CFG_ITEM_STATUS,
		"",
		0,
		WS_EX_TRANSPARENT
	}
};

static int current_sel_lang;  /* */

static void initSetSysLangBkgnd(HDC hdc)
{
	FillBoxWithBitmap(hdc, 0, 0, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H, &configureListBkgnd);
}

static void initSetSysLangDlg(HWND hDlg)
{
	int lang = *curSysLang;
	char strBuf[16];

	int len = strlen(strConfigItems[lang][currentConfig]);

	if(len > 15)
		len = 15;

	strncpy(strBuf, strConfigItems[lang][currentConfig], len);
	strBuf[len] = 0;
	SetDlgItemText(hDlg, IDC_LANG_DESC, strBuf);

	SetDlgItemText(hDlg, IDC_CFG_ITEM_STATUS, strSysLangHelp[lang]);
}

static void setSysLangButtonCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	if(nc == BN_CLICKED)
		current_sel_lang = id - IDC_LANG_BT_0;
}

static int setSysLangDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndRadioButton[2];

	switch (message)
	{
    	case MSG_INITDIALOG:

		CreateWindow (CTRL_BUTTON,
			      " F1 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F1,
			      40, 120, 94, 42, hDlg, (DWORD)(&btBmpCfgFn[0]));

		CreateWindow (CTRL_BUTTON,
			      " F2 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F2,
			      190, 120, 94, 42, hDlg, (DWORD)(&btBmpCfgFn[1]));

		/* Single select button */
		hwndRadioButton[0]= CreateWindow(CTRL_BUTTON,
						 "简体中文",
						 WS_CHILD | BS_AUTORADIOBUTTON | WS_VISIBLE | WS_GROUP,
						 IDC_LANG_BT_0,
						 140, 20, 100, 30, hDlg, 0);

		hwndRadioButton[1]= CreateWindow(CTRL_BUTTON,
						 "English",
						 WS_CHILD | BS_AUTORADIOBUTTON | WS_VISIBLE,
						 IDC_LANG_BT_1,
						 140, 60, 100, 30, hDlg, 0);

		SetNotificationCallback(hwndRadioButton[0], setSysLangButtonCallback);
		SetNotificationCallback(hwndRadioButton[1], setSysLangButtonCallback);

		current_sel_lang = *curSysLang;

		if(current_sel_lang < SYSLANG_NO)
			SendMessage(hwndRadioButton[current_sel_lang], BM_SETCHECK, BST_CHECKED, 0);

		initSetSysLangDlg(hDlg);
		SetFocusChild(hwndRadioButton[current_sel_lang]);

		return(1);

    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initSetSysLangBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_CURSORBLOCKUP:
		case SCANCODE_CURSORBLOCKLEFT:
			break;

		case SCANCODE_CURSORBLOCKDOWN:
		case SCANCODE_CURSORBLOCKRIGHT:
			break;

		case SCANCODE_F2:  /* cancel and edit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_F1:  /* done  */
		case SCANCODE_ENTER:
			if(currentConfig == CFG_ITEM_LANG){
#if 0
				printf("lang orig=%d now=%d\n", *curSysLang, current_sel_lang);
#endif
				if(*curSysLang != current_sel_lang){
					/* exchange to keep them not the same */
					int orig_lang = *curSysLang;
					*curSysLang = current_sel_lang;
					current_sel_lang = orig_lang;
					saveUserConfigFile(0);
				}
			}
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);

			break;
		case SCANCODE_CANCEL:
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_HELP:
			MessageBox(hDlg,config_help[*curSysLang].help_lang,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		}
		break;
    	case MSG_COMMAND:
		switch(wParam){
		case IDC_CFG_BUTTON_F1:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_CFG_BUTTON_F2:  /* Exit, call EndDialog() directly */
			EndDialog(hDlg,IDOK);
			break;
	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;


	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setSysLangDialogBox (HWND hwnd)
{
	setSysLangDlgBox.controls = setSysLangCtrlsInit;
	DialogBoxIndirectParam (&setSysLangDlgBox, hwnd,setSysLangDialogBoxProc, 0L);
}
static int setSysLang (HWND hwnd)
{
	//printf("setSysLanguage().\n");
	setSysLangDialogBox(hwnd);		// Should not return except receive CLOSE MSG
	return 0;

}

/***********************************************************/

static DLGTEMPLATE setSysSpeakerDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	CFG_ITEM_WIN_X, CFG_ITEM_WIN_Y, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H,
	"setSysSpeaker",
	0, 0,
	2, NULL,
	0
};

static CTRLDATA setSysSpeakerCtrlsInit[] =
{
	{       /* description */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		30, 24, 100, 30,
		IDC_SPEAKER_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{       /* operation help */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		10, 175, 320, 24,
		IDC_CFG_ITEM_STATUS,
		"",
		0,
		WS_EX_TRANSPARENT
	}
};

static int current_sel_speaker = 0;

static void initSetSysSpeakerBkgnd(HDC hdc)
{
	FillBoxWithBitmap(hdc, 0, 0, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H, &configureListBkgnd);
}

static void initSetSysSpeakerDlg(HWND hDlg)
{
	int lang = *curSysLang;
	char strBuf[16];

	int len = strlen(strConfigItems[lang][currentConfig]);

	if(len > 15)
		len = 15;

	strncpy(strBuf, strConfigItems[lang][currentConfig], len);
	strBuf[len] = 0;
	SetDlgItemText(hDlg, IDC_SPEAKER_DESC, strBuf);

	SetDlgItemText(hDlg, IDC_CFG_ITEM_STATUS, strSysSpeakerHelp[lang]);
}

static void setSysSpeakerButtonCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
}

static int setSysSpeakerDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndRadioButton[2];

	switch (message)
	{
    	case MSG_INITDIALOG:

		CreateWindow (CTRL_BUTTON,
			      " F1 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F1,
			      40, 120, 94, 42, hDlg, (DWORD)(&btBmpCfgFn[0]));

		CreateWindow (CTRL_BUTTON,
			      " F2 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F2,
			      190, 120, 94, 42, hDlg, (DWORD)(&btBmpCfgFn[1]));

		/* Single select button */
		hwndRadioButton[0]= CreateWindow(CTRL_BUTTON,
						 "ON",
						 WS_CHILD | BS_AUTORADIOBUTTON | WS_VISIBLE | WS_GROUP,
						 IDC_SPEAKER_BT_0,
						 140, 20, 100, 30, hDlg, 0);

		hwndRadioButton[1]= CreateWindow(CTRL_BUTTON,
						 "OFF",
						 WS_CHILD | BS_AUTORADIOBUTTON | WS_VISIBLE,
						 IDC_SPEAKER_BT_1,
						 140, 60, 100, 30, hDlg, 0);

		SetNotificationCallback(hwndRadioButton[0], setSysSpeakerButtonCallback);
		SetNotificationCallback(hwndRadioButton[1], setSysSpeakerButtonCallback);


		SendMessage(hwndRadioButton[current_sel_speaker], BM_SETCHECK, BST_CHECKED, 0);

		initSetSysSpeakerDlg(hDlg);
		SetFocusChild(hwndRadioButton[current_sel_speaker]);

		return(1);

    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initSetSysSpeakerBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_CURSORBLOCKUP:
		case SCANCODE_CURSORBLOCKLEFT:
			break;

		case SCANCODE_CURSORBLOCKDOWN:
		case SCANCODE_CURSORBLOCKRIGHT:
			break;

		case SCANCODE_F2:  /* cancel and edit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_F1:  /* done  */
		case SCANCODE_ENTER:
			if(currentConfig == CFG_ITEM_SPEAKER){

			}
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);

			break;
		case SCANCODE_CANCEL:
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		}
		break;
    	case MSG_COMMAND:
		switch(wParam){
		case IDC_CFG_BUTTON_F1:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_CFG_BUTTON_F2:  /* Exit, call EndDialog() directly */
			EndDialog(hDlg,IDOK);
			break;
	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;


	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setSysSpeakerDialogBox (HWND hwnd)
{
	setSysSpeakerDlgBox.controls = setSysSpeakerCtrlsInit;
	DialogBoxIndirectParam (&setSysSpeakerDlgBox, hwnd,setSysSpeakerDialogBoxProc, 0L);
}

static int setSysSpeaker (HWND hwnd)
{
	//printf("setSysSpeakeruage().\n");
	setSysSpeakerDialogBox(hwnd);		// Should not return except receive CLOSE MSG
	return 0;
}

/***********************************************************/
/* setSysSpeed */

#define RunSpeedNumber		8

static DLGTEMPLATE setRunSpeedDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	CFG_ITEM_WIN_X, CFG_ITEM_WIN_Y, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H,
	"setRunSpeed",
	0, 0,
	4, NULL,
	0
};

static CTRLDATA setRunSpeedCtrlsInit[] =
{
	{       /* description */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		30, 15, 100, 30,
		IDC_SPEED_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{       /* operation help */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		10, 175, 320, 24,
		IDC_CFG_ITEM_STATUS,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{	CTRL_STATIC,
		WS_VISIBLE | WS_BORDER,
		130, 10, 140, 28,
		IDC_SPEED_CURRENT,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{	CTRL_LISTBOX,
		WS_VISIBLE | WS_BORDER | WS_VSCROLL | LBS_NOTIFY,
		130, 43, 140, 68,
		IDC_SPEED_LIST,
		NULL,
		0
	}
};

static void initSetRunSpeedBkgnd(HDC hdc)
{
	FillBoxWithBitmap(hdc, 0, 0, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H, &configureListBkgnd);
}

static void show_current_speed(HWND hwnd)
{
	int speed = currentuser->speed;
	char strBuf[16];
	int lang = *curSysLang;

	sprintf(strBuf,"%4d  %s", speed, strSpeedFactor[lang]);
	SetWindowText(hwnd, strBuf); /* show current speed */
}

static void initRunSpeedList(HWND hDlg, int curSel)
{
	int index;
	char strBuf[16];
	int lang = *curSysLang;
	int len;
	HWND hwndList, hwndSpeed;

	len = strlen(strConfigItems[lang][currentConfig]);
	if(len > 15)
		len = 15;

	strncpy(strBuf, strConfigItems[lang][currentConfig], len);
	strBuf[len] = 0;
	SetDlgItemText(hDlg, IDC_SPEED_DESC, strBuf);

	hwndSpeed = GetDlgItem(hDlg,IDC_SPEED_CURRENT);
	show_current_speed(hwndSpeed);
	/* sprintf(strBuf,"%4d  %s", curSel, strSpeedFactor[lang]); */
	/* SetDlgItemText(hwndSpeed, strBuf); /\* show current speed *\/ */

	hwndList = GetDlgItem(hDlg,IDC_SPEED_LIST);
	SendMessage(hwndList,LB_RESETCONTENT,0,0); /* clear list */

	for(index = 0; index < RunSpeedNumber; index++){
		sprintf(strBuf,"%4d  %s",8+index, strSpeedFactor[lang]);
		SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)strBuf);
	}
	SendMessage(hwndList,LB_SETCURSEL, (curSel-8), 0); /* Highlight */

	SetDlgItemText(hDlg, IDC_CFG_ITEM_STATUS, strSysSpeedHelp[lang]);
}


static int set_motor_speed(HWND hDlg)
{
	int curSel;
	char cmdSendBuf[13];
	HWND hwndList = GetDlgItem(hDlg,IDC_SPEED_LIST);

	curSel = SendMessage(hwndList,LB_GETCURSEL,0,0);
	currentuser->speed = curSel + 8;
	saveUserConfigFile(0);

	cmdData.data = currentuser->speed;
	setCmdFrame(&cmdData,cmdSendBuf);
	sendCommand(cmdSendBuf);

	MessageBox(hDlg, config_help[*curSysLang].msg_config_ok,
		   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);

	return 0;
}

static void RunSpeedListCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	char cmdSendBuf[13];

	mainHwnd = GetMainWindowHandle(hwnd);

	memset(cmdSendBuf,0,12);
	cmdData.flag = 'S';
	cmdData.status = 0x30;
	switch(nc){
	case LBN_ENTER:
		set_motor_speed(mainHwnd);
		SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0);
		break;
	}
}

static int setRunSpeedDialogBoxProc(HWND hDlg, int message,
				    WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndSpeedList = GetDlgItem(hDlg,IDC_SPEED_LIST);

	switch (message)
	{
    	case MSG_INITDIALOG:
		CreateWindow (CTRL_BUTTON,
			      " F1 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F1,
			      40, 120, 94, 42, hDlg, (DWORD)(&btBmpCfgFn[0]));

		CreateWindow (CTRL_BUTTON,
			      " F2 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F2,
			      190, 120, 94, 42, hDlg, (DWORD)(&btBmpCfgFn[1]));

		SetNotificationCallback(hwndSpeedList, RunSpeedListCallback);
		initRunSpeedList(hDlg, currentuser->speed);

		SetFocusChild(hwndSpeedList);
		return(1);
    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initSetRunSpeedBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_F1:  /* done  */
		case SCANCODE_ENTER:
			if(currentConfig == CFG_ITEM_SPEED){
				set_motor_speed(hDlg);
			}
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);

			break;

		case SCANCODE_F2:  /* cancel and edit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_CANCEL:
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_HELP:
			break;
		}
		break;
    	case MSG_COMMAND:
		switch(wParam){
		case IDC_CFG_BUTTON_F1:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_CFG_BUTTON_F2:  /* Exit, call EndDialog() directly */
			EndDialog(hDlg,IDOK);
			break;
	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;
	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setRunSpeedDialogBox (HWND hwnd)
{
	setRunSpeedDlgBox.controls = setRunSpeedCtrlsInit;
	DialogBoxIndirectParam (&setRunSpeedDlgBox, hwnd,setRunSpeedDialogBoxProc, 0L);
}
static int setRunSpeed (HWND hwnd)
{
	setRunSpeedDialogBox(hwnd);
	return 0;

}

/***********************************************************************************/
static DLGTEMPLATE machConfigDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	LISTBOX_X , LISTBOX_Y, CONFIG_WIN_W, CONFIG_WIN_H,
	"configure",
	0, 0,
	5, NULL,
	0
};

static CTRLDATA machConfigCtrlsInit[] =
{
	{
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		160, 10, 100, 30,
		IDC_LIMIT_FRONT_DESC,
		"value",
		0
	},
	{
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY ,
		160, 45, 100, 30,
		IDC_LIMIT_MIDDLE_DESC,
		"value",
		0
	},
	{
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		160, 80, 100, 30,
		IDC_LIMIT_BACK_DESC,
		"value",
		0
	},
	{       /* speed */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		390, 45, 120, 30,
		IDC_SPEED_SHOW,
		"speed",
		0
	},
	{       /* operation help */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		10, 230, 500, 24,
		IDC_MACH_CFG_STATUS,
		"status",
		0,
		WS_EX_TRANSPARENT
	}
};

static void setConfigItemsName(HWND hDlg)
{
	int i;
	HWND hwndTemp;
	int lang = *curSysLang;

	for(i = 0; i < MACH_CONFIG_ITEM; i++){
		hwndTemp = GetDlgItem(hDlg, IDC_BT_START + i);

		SendMessage(hwndTemp, MSG_SETTEXT, 0, (LPARAM)strConfigItems[lang][i]);
	}
}

static int loadMachConfigBkgndFiles(void)
{
	if (LoadBitmap (HDC_SCREEN, &configureBkgnd, "./res/configItemBkgnd.jpg"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &configureListBkgnd, "./res/configItemBkgnd.jpg"))
		return 1;

	/* F1 and F2 */
	if (LoadBitmap (HDC_SCREEN, &btBmpCfgFn[0], "./res/buttonLableF1.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpCfgFn[1], "./res/buttonLableF2.bmp"))
		return 1;

	return 0;
}

static void unloadMachConfigBkgndFiles(void)
{
	UnloadBitmap(&configureBkgnd);
	UnloadBitmap(&configureListBkgnd);
	UnloadBitmap(&btBmpCfgFn[0]);
	UnloadBitmap(&btBmpCfgFn[1]);
}

static void refreshConfigItem(HWND hDlg,int sel)
{
	HWND  hwndItem;
	int data;

	switch(sel){
	case CFG_ITEM_F_LIMIT:
		data = currentuser->frontlimit;
		hwndItem = GetDlgItem(hDlg,IDC_LIMIT_FRONT_DESC);
		refreshCurrentPos(hwndItem, data);
		break;
	case CFG_ITEM_M_LIMIT:
		data = currentuser->middlelimit;
		hwndItem = GetDlgItem(hDlg,IDC_LIMIT_MIDDLE_DESC);
		refreshCurrentPos(hwndItem, data);
		break;
	case CFG_ITEM_B_LIMIT:
		data = currentuser->backlimit;
		hwndItem = GetDlgItem(hDlg,IDC_LIMIT_BACK_DESC);
		refreshCurrentPos(hwndItem, data);
		break;
	case CFG_ITEM_SPEED:
		hwndItem = GetDlgItem(hDlg,IDC_SPEED_SHOW);
		show_current_speed(hwndItem);
		return;
	default:
		return;

	}
}

static void enterMachConfigItem(HWND hwnd, int sel)
{
#if 0
	printf("select: %d\n", sel);
#endif
	switch(sel){
	case CFG_ITEM_F_LIMIT:  /* Front Limit */
	case CFG_ITEM_M_LIMIT:  /* Middle Limit */
	case CFG_ITEM_B_LIMIT:  /* Back Limit */
		setPosLimit(hwnd);
		refreshConfigItem(hwnd,sel);
		break;
	case CFG_ITEM_DATE:
		setSysDate(hwnd);
		break;
	case CFG_ITEM_TIME:
		setSysTime(hwnd);
		break;
	case CFG_ITEM_LANG:
		setSysLang(hwnd);
		if(current_sel_lang != *curSysLang)
			setConfigItemsName(hwnd);
		break;
	case CFG_ITEM_SPEAKER:
		setSysSpeaker(hwnd);
		break;
	case CFG_ITEM_SPEED:
		setRunSpeed(hwnd);
		refreshConfigItem(hwnd, sel);
		break;

	default:
		SendMessage(hwnd,MSG_COMMAND,IDCANCEL,0);
		break;
	}

}

/*  refresh config item list
 *
 *  HighLight current select item
 *
 */
static int refreshMachConfigItems(HWND hDlg, int sel)
{
	int i;
	HWND hwndTemp;

#if 0
	printf("sel = %d\n", sel);
#endif
	if((sel >= MACH_CONFIG_ITEM) || (sel < 0))
		return (-1);

	for(i = 0; i < MACH_CONFIG_ITEM; i++){
		hwndTemp = GetDlgItem(hDlg, IDC_BT_START + i);

		if(i == sel){
			SetWindowBkColor(hwndTemp, PIXEL_yellow);
			InvalidateRect(hwndTemp, NULL, TRUE);
		} else {
			SetWindowBkColor(hwndTemp, PIXEL_lightgray);
			InvalidateRect(hwndTemp, NULL, TRUE);
		}
	}

	return sel;
}

static void initMachConfigBkgnd(HDC hdc)
{
	FillBoxWithBitmap (hdc, 0, 0, CONFIG_WIN_W, CONFIG_WIN_H, &configureBkgnd);
}

static void initMachConfigDialog(HWND hDlg)
{
	HWND  hwndFrontLimit = GetDlgItem(hDlg,IDC_LIMIT_FRONT_DESC);
	HWND  hwndMiddlelimit = GetDlgItem(hDlg,IDC_LIMIT_MIDDLE_DESC);
	HWND  hwndBackLimit = GetDlgItem(hDlg,IDC_LIMIT_BACK_DESC);
	HWND  hwndItem = GetDlgItem(hDlg,IDC_SPEED_SHOW);
	int lang = *curSysLang;

	/* config item names */
	setConfigItemsName(hDlg);

	/* show the current config */
	refreshCurrentPos(hwndFrontLimit,currentuser->frontlimit);
	refreshCurrentPos(hwndMiddlelimit,currentuser->middlelimit);
	refreshCurrentPos(hwndBackLimit,currentuser->backlimit);

	show_current_speed(hwndItem);

	SetDlgItemText(hDlg, IDC_MACH_CFG_STATUS, strMachConfigHelp[lang]);

	refreshMachConfigItems(hDlg, 0);
}

//*********************************************************//
static int configureDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndFrontLimit = GetDlgItem(hDlg,IDC_LIMIT_FRONT_DESC);
	HWND hwndButton[MACH_CONFIG_ITEM];
	int ret;

	switch (message)
	{
    	case MSG_INITDIALOG:

		currentConfig = 0;

		SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);
		if(isIMEOpenInMotor == 1){
			ShowWindow(myIMEWindow,SW_HIDE);
			isIMEOpenInMotor = 0;
		}

		hwndButton[0] = CreateWindow (CTRL_BUTTON,
 				" 0 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_LIMIT_FRONT,
				50, 10, CFG_BT_W, CFG_BT_H, hDlg, 0);
		hwndButton[1] = CreateWindow (CTRL_BUTTON,
 				" 1 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_LIMIT_MIDDLE,
				50, 45, CFG_BT_W, CFG_BT_H, hDlg, 0);
		hwndButton[2] = CreateWindow (CTRL_BUTTON,
 				" 2 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_LIMIT_BACK,
				50, 80, CFG_BT_W, CFG_BT_H, hDlg, 0);
		hwndButton[3] = CreateWindow (CTRL_BUTTON,
 				" 3 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_DATE,
				50, 115, CFG_BT_W, CFG_BT_H, hDlg, 0);
		hwndButton[4] = CreateWindow (CTRL_BUTTON,
 				" 4 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_TIME,
				50, 150, CFG_BT_W, CFG_BT_H, hDlg, 0);
		hwndButton[5] = CreateWindow (CTRL_BUTTON,
 				" 5 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_LANG,
				50, 185, CFG_BT_W, CFG_BT_H, hDlg, 0);
		hwndButton[6] = CreateWindow (CTRL_BUTTON,
 				" 6 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_SPEAKER,
				280, 10, CFG_BT_W, CFG_BT_H, hDlg, 0);
		hwndButton[7] = CreateWindow (CTRL_BUTTON,
 				" 7 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_SPEED,
				280, 45, CFG_BT_W, CFG_BT_H, hDlg, 0);

		hwndButton[8] = CreateWindow (CTRL_BUTTON,
 				" 8 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_EXIT,
				400, 185, CFG_BT_W, CFG_BT_H, hDlg, 0);

		initMachConfigDialog(hDlg);

		return(1);

    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initMachConfigBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}

	case MSG_CHAR:
		ret = LOWORD(wParam);
#if 0
		printf("Key = %x \n",ret);
#endif
		if((ret >= 0x30) &&(ret < 0x3a)){  /* 0 - 9 */
			ret -= 0x30;

			if((ret >=0) && (ret <= MACH_CONFIG_ITEM - 1)){
				currentConfig = ret;
				refreshMachConfigItems(hDlg, currentConfig);
				enterMachConfigItem(hDlg, currentConfig);
			}
		}
		break;

    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_ENTER:
			enterMachConfigItem(hDlg, currentConfig);
			break;

		case SCANCODE_CURSORBLOCKUP:
			if(currentConfig > 0){
				currentConfig--;
			}else{
				currentConfig = 0;
			}
			refreshMachConfigItems(hDlg, currentConfig);
			break;
		case SCANCODE_CURSORBLOCKDOWN:
			if(currentConfig < (MACH_CONFIG_ITEM - 1)){
				currentConfig++;
			}else{
				currentConfig = MACH_CONFIG_ITEM - 1;
			}
			refreshMachConfigItems(hDlg, currentConfig);
			break;
		case SCANCODE_CURSORBLOCKLEFT:
			if(currentConfig > 5){
				currentConfig -= 6;
				refreshMachConfigItems(hDlg, currentConfig);
			}
			break;

		case SCANCODE_CURSORBLOCKRIGHT:  /* to EXIT button */
			if(currentConfig <= 5){
				currentConfig += 6;
				if(currentConfig >= MACH_CONFIG_ITEM - 1)
					currentConfig = MACH_CONFIG_ITEM - 1;

				refreshMachConfigItems(hDlg, currentConfig);
			}
			break;

		case SCANCODE_CANCEL: /* Exit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_HELP:
			MessageBox(hDlg,config_help[*curSysLang].help_config,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		}
		break;

    	case MSG_COMMAND:
		switch(wParam){
		case IDC_BT_LIMIT_FRONT:
		case IDC_BT_LIMIT_MIDDLE:
		case IDC_BT_LIMIT_BACK:
		case IDC_BT_DATE:
		case IDC_BT_TIME:
		case IDC_BT_LANG:
		case IDC_BT_SPEAKER:
		case IDC_BT_SPEED:
			currentConfig = wParam - IDC_BT_START;
			if((currentConfig < 0) || currentConfig > (MACH_CONFIG_ITEM - 1))
				break;

			/* change focus to other window */
			SetFocusChild(hwndFrontLimit);

			refreshMachConfigItems(hDlg, currentConfig);
			enterMachConfigItem(hDlg, currentConfig);
			break;

		case IDC_BT_EXIT:
	    	case IDOK:
	    	case IDCANCEL:
			KillTimer(hDlg,IDC_TIMER);
			EndDialog(hDlg,IDOK);
			break;
		}
		break;

	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void configureDialogBox (HWND hwnd)
{
	machConfigDlgBox.controls = machConfigCtrlsInit;
	DialogBoxIndirectParam (&machConfigDlgBox, hwnd,configureDialogBoxProc, 0L);
}

int enterMachineConfig (HWND hwnd)
{
	loadMachConfigBkgndFiles();

	printf("Enter System Configure...\n");
	configureDialogBox(hwnd);		// Should not return except receive CLOSE MSG

	unloadMachConfigBkgndFiles();

	return 0;
}
