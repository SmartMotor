/*
 *  motormain.h
 *
 *  Copyright 2008 @ xianweizeng@gmail.com
 *
 */
#ifndef _MOTORMAIN_H_
#define _MOTORMAIN_H_

int loadInitConfigFile(int *pFlash,int *pBkgnd);
int saveInitConfigFile(int *pFlash,int *pBkgnd);

#endif /* _MOTORMAIN_H_ */
