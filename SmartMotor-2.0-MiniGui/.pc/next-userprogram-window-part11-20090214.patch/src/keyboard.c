/*
 * src/keyboard.c
 *
 * Copyright 2008 @ xianweizeng@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <minigui/common.h>
#include <minigui/minigui.h>
#include <minigui/gdi.h>
#include <minigui/window.h>
#include <minigui/control.h>

#include "motor.h"
#include "keyboard.h"

static BITMAP keyboardBkgnd;

/* Resources */
static DLGTEMPLATE KeyboardDlgInitProgress =
{
    WS_BORDER ,
    WS_EX_NONE,
    0, 0, 640, 480,
    "Keyboard",
    0, 0,
    1, NULL,
    0
};


static CTRLDATA KeyboardCtrls [] =
{
    {
        CTRL_SLEDIT,
        WS_VISIBLE | ES_READONLY ,
        455, 10, 175, 28,
        IDC_TIME,
        NULL,
        0
    }
};

static void initKeyboardBkgnd(HDC hdc)
{
	/* Background */
	FillBoxWithBitmap (hdc, 0, 0, 0, 0, &keyboardBkgnd);
}


static int KeyboardDlgBoxProc (HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{

    switch (message) {
    case MSG_INITDIALOG:
        return 1;
    case MSG_ERASEBKGND:
        {
	   HDC hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp;
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            }
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
            }

	    initKeyboardBkgnd(hdc);

            if (fGetDC)
                ReleaseDC (hdc);

	    //SetFocusChild(hwndComputerData);

	return 0;
	}

    case MSG_KEYDOWN:
	    switch(LOWORD(wParam)){
	    case SCANCODE_CANCEL:    /* exit */
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
	    }

	    break;

    case MSG_KEYUP:
	    break;

    case MSG_COMMAND:
	switch(wParam){
	case IDOK:
	case IDCANCEL:
		EndDialog(hDlg,IDOK);
		break;
	}
	break;

    }

    return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void InitKeyboardDialogBox (HWND hWnd)
{
	KeyboardDlgInitProgress.controls = KeyboardCtrls;
	DialogBoxIndirectParam (&KeyboardDlgInitProgress,
		            hWnd, KeyboardDlgBoxProc, 0L);
}


int EnterKeyboardCheck(HWND hWnd)
{
	/* Load background and other resources */
	if (LoadBitmap (HDC_SCREEN, &keyboardBkgnd, "./res/configBkgnd.jpg"))
		return 1;

	printf("Enter Keyboard Check ...\n");
	InitKeyboardDialogBox (hWnd);		// Should not return except receive CLOSE MSG

	UnloadBitmap (&keyboardBkgnd);	// unload background
	return 0;
}

