/*
 * src/motorregister.c
 *
 * The login window implementation
 *
 * Copyright 2007-2008 @ xianweizeng@gmail.com
 *
 * Author:
 *   Zeng Xianwei
 *
 *  File Coding system: gb2312
 *    set-buffer-file-coding-system
 *
 * ChangeLog:
 *      V0.1	     Created by Quan Dongxiao
 *	V0.2	     Modified by Zeng Xianwei @ 2005-12-27
 *      2008-09-24   Change code indent
 *      2008-12-30   Add passwork check dialog. Remove unneed code.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <ctype.h>
#include <unistd.h>
#include <pwd.h>
#include <errno.h>

#include <minigui/common.h>
#include <minigui/minigui.h>
#include <minigui/gdi.h>
#include <minigui/window.h>
#include <minigui/control.h>

#include "motorregister.h"
#include "motor.h"
#include "motorfile.h"
#include "help.h"

/* define global variables */
struct userinfor_t userinformation[MAX_USER_NUMBER];
struct userinfor_t *currentuser;    /* pointer to current user */
				    /* currentuser = userinformation[currentid] */
int currentid;			    /* current user id */
int usernumber;	 		    /* current total user number */
int motor_usermask = 0;             /* user mask */

static BITMAP passwordBkgnd;
static BITMAP btBmpPswFn[2];          /* F1: done F2: exit */

struct login_help_t{
	unsigned char * caption_help;
	unsigned char * caption_waring;
	unsigned char * msg_name_len;
	unsigned char * msg_name_null;
	unsigned char * msg_name_exist;
	unsigned char * msg_default_user;
	unsigned char * msg_max_user;
	unsigned char * msg_psw_len;
	unsigned char * msg_psw_wrong;
	unsigned char * msg_psw_again;
	unsigned char * msg_psws_diff;
	unsigned char * msg_psw_wrong_three;
	unsigned char * msg_newuser_ok;
	unsigned char * msg_deluser_ok;
	unsigned char * msg_deluser_confirm;
	unsigned char * help_login_main;
	unsigned char * help_psw;
	unsigned char * help_t9;
};

static struct login_help_t login_help[SYSLANG_NO] =
{
	{	caption_help_cn,
		caption_waring_cn,
		msg_name_len_cn,
		msg_name_null_cn,
		msg_name_exit_cn,
		msg_default_user_cn,
		msg_max_user_cn,
		msg_psw_len_cn,
		msg_psw_wrong_cn,
		msg_psw_again_cn,
		msg_psws_diff_cn,
		msg_psw_wrong_three_cn,
		msg_newuser_ok_cn,
		msg_deluser_ok_cn,
		msg_deluser_confirm_cn,
		help_login_main_cn,
		help_psw_cn,
		help_t9_cn
	},

	{	caption_help_en,
		caption_waring_en,
		msg_name_len_en,
		msg_name_null_en,
		msg_name_exit_en,
		msg_default_user_en,
		msg_max_user_en,
		msg_psw_len_en,
		msg_psw_wrong_en,
		msg_psw_again_en,
		msg_psws_diff_en,
		msg_psw_wrong_three_en,
		msg_newuser_ok_en,
		msg_deluser_ok_en,
		msg_deluser_confirm_en,
		help_login_main_en,
		help_psw_en,
		help_t9_en
	}
};
/*******************************************************************/
/*
 * Create a new data file for user
 */
void onefile_new(const char*filename, int i)
{
	FILE *fp;

	struct userProFileHeader_t programe;

	if((fp=fopen(filename,"wb"))==NULL){
		printf("openfile wrong: %s\n", filename);
		return;
	}

	programe.fileflags = 1234;
	programe.isnull = 1;
	programe.id = i;
	programe.introduction[0] = '\0';
	programe.latestDate[0] = '\0';
	programe.usedTimes = 0;
	programe.first = 0;
	fwrite(&programe,sizeof(struct userProFileHeader_t),1,fp);
	fclose(fp);
}

static void create_data_files(const char* path)
{
 	char pathname[40];
	int length;
	int i;

	length = strlen(path);
    	strncpy(pathname, path, length);


#if 0
	printf("create files: %s\n", pathname);
#endif

	for(i = 1; i < 100; i++) /* every user: 99 files */
	{
		sprintf(pathname, "%s%d.pro", path, i);
		onefile_new(pathname, i);
	}

}


/*************************************/
/*	Local Functions For Register Dialog	****/
/*************************************/


/*  read user config file, store in struct userinfor *userInfo[]
 *
 */
int loadUserConfigFile(void)
{
	int index = 0;
	char buff[12][20];
	FILE *fp;

	memset(userinformation, 0, MAX_USER_NUMBER * sizeof(struct userinfor_t));

	if((fp=fopen("./user/user.cfg","r"))==NULL)
	{
		printf("openfile wrong!\n");
		return 0;
	}

	while(!feof(fp))
	{
		fscanf(fp,"%s %s %s %s %s %s %s %s %s %s %s %s\n",
		       buff[0],buff[1],buff[2],buff[3],
		       buff[4],buff[5],buff[6],buff[7],
		       buff[8],buff[9],buff[10],buff[11]);

		userinformation[index].userid=atoi(buff[0]);
		userinformation[index].recent_programe=atoi(buff[1]);
		userinformation[index].language=atoi(buff[2]);
		userinformation[index].unit=atoi(buff[3]);
		userinformation[index].frontlimit=atoi(buff[4]);
		userinformation[index].middlelimit=atoi(buff[5]);
		userinformation[index].backlimit=atoi(buff[6]);
		userinformation[index].forwardstep=atoi(buff[7]);
		userinformation[index].backwardstep=atoi(buff[8]);
		userinformation[index].speed=atoi(buff[9]);

		strcpy(userinformation[index].name,buff[10]);
		strcpy(userinformation[index].password,buff[11]);  // super user's PWD is super PWD

		index++;

		if(index >= MAX_USER_NUMBER)
			break;
	}
	fclose(fp);

	usernumber = index;

#if 0
	/* dump userinformation[] */
	printf("======================================\n");
	printf("Total user: %d \n",usernumber);
	for(index = 0; index < usernumber; index++)
		printf("index: %d, UserID: %d\n",index, userinformation[index].userid);
	printf("======================================\n");
#endif

	return(usernumber);
}

/*  flags: delete flag
 *  flag > 0
 *		  flags = 0 ȫ������
 */
int saveUserConfigFile(int delete_user)
{
	int i;
	FILE *fp;

	if((fp = fopen("./user/user.cfg","w")) == NULL) {
		printf("openfile wrong!\n");
		return 0;
	}

	for(i = 0; i < usernumber; i++)
	{
		/* user 0 and 1 can be deleted */
		if((i > 1) && (delete_user > 1) && (i == delete_user)){
			continue;
		}

		fprintf(fp,"%d %d %d %d %d %d %d %d %d %d %s %s\n",
			userinformation[i].userid,
			userinformation[i].recent_programe,userinformation[i].language,
			userinformation[i].unit,userinformation[i].frontlimit,
			userinformation[i].middlelimit,userinformation[i].backlimit,
			userinformation[i].forwardstep,userinformation[i].backwardstep,
			userinformation[i].speed,
			userinformation[i].name,userinformation[i].password);
	}

	fclose(fp);

	if(delete_user){
		usernumber = usernumber -1;
	}

	return(usernumber);
}

/* Find an unused user id for the user to be created.
 * user id is universal.
 *
 * User ID is used to find the user programs direcotry,
 * named like "user00" or "user01".
 *
 * User ID has no related to the user sequence in the user list.
 * We display users according to user's place in config file.
 *
 * return value:
 *   user id. (not 0 nor 1)
 */
static int get_new_user_id(void)
{
	int i,j;
	int nr_user = usernumber;
	int found, id = usernumber;

	for (i = nr_user - 1 ; i < (nr_user + MAX_USER_NUMBER - 1); i++) {
		found = 1;
		for(j = 0; j < nr_user; j++) {
			if ( i == userinformation[j].userid){
				found = 0;
				continue;
			}
		}

		if(found == 1){
			id = i;
			break;
		}
	}

	return id;
}

/*
 * initialize user structure and create program files
 * for this user.
 *
 * hWnd:  main HWND
 * index: user index in userinformation[]array
 *
 */
int initUserDataAndPrograms(HWND hWnd, int index)
{
	char cmdbuff[40];
	char path[40];
	int userid = get_new_user_id();

	printf("create a new user: id = %d \n", userid);

	userinformation[index].userid = userid;
	userinformation[index].recent_programe = 1;
	userinformation[index].language = 0;
	userinformation[index].unit = 0;
	userinformation[index].frontlimit = 3000;
	userinformation[index].middlelimit = 8000;
	userinformation[index].backlimit = 145500;
	userinformation[index].forwardstep = 100;
	userinformation[index].backwardstep = 100;
	userinformation[index].speed = 8;	// default speed 8m/min

	memset(userinformation[index].password, 0, 10);
	strncpy(userinformation[index].password, "791379", 6);

	memset(cmdbuff, 0, 40);
	sprintf(cmdbuff, "mkdir -p ./programe/user%02d", userid);
	system(cmdbuff);

	memset(path, 0, 40);
	sprintf(path, "./programe/user%02d/", userid);
	create_data_files(path);

	MessageBox(hWnd,login_help[*curSysLang].msg_newuser_ok,
		   login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
	return(0);
}

int isUserNameValid(HWND hWnd,char *nameBuff,int len)
{
	int i;

	if(len == 0){  /* NULL */
		MessageBox(hWnd,login_help[*curSysLang].msg_name_null,
			   login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		return 0;
	}
	if(len >= 18){
		MessageBox(hWnd,login_help[*curSysLang].msg_name_len,
			   login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		return 0;
	}

	for(i = 0; i < usernumber; i++){
		if(!strcmp(userinformation[i].name,nameBuff)){
			MessageBox(hWnd,login_help[*curSysLang].msg_name_exist,
				   login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
			return 0;
		}
	}

	return 1;
}

/**************************************************************************/
#define PSWEDIT_X      210
#define PSWEDIT_Y      160
#define PSWEDIT_W      320
#define PSWEDIT_H      160

#define IDC_PASSWORD		150
#define IDC_PASSWORD_DESC	151
#define IDC_BUTTON_PSW_F1	152
#define IDC_BUTTON_PSW_F2	153

static char strInputPassword[] = "����������";

static DLGTEMPLATE CheckPasswordDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	PSWEDIT_X , PSWEDIT_Y, PSWEDIT_W, PSWEDIT_H,
	"PSW",
	0, 0,
	2, NULL,
	0
};

static CTRLDATA CheckPasswordCtrls [] =
{
	{       /* Please input password: */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		10, 10, 300, 30,
		IDC_PASSWORD_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_EDIT,
		WS_VISIBLE | WS_BORDER | ES_PASSWORD,
		80,50, 150, 30,
		IDC_PASSWORD,
		NULL,
		0
    	}
};

static int check_psw_status;  /* Check Password Return Value */

/*
 * Check super password.
 * return: 0:  OK
 *         1:  FAIL
 */
static int checkSuperPassword(char *pswBuf, int pswlen)
{
	int len = strlen(userinformation[0].password);

	if (len != pswlen)
		return 1;

	if(!strncmp(pswBuf, userinformation[0].password, len))
		return(0);  /* OK */

	return 1;
}

static int check_password(HWND hDlg)
{
	int ret, pswlen;
	char buff[20];
	HWND hwndPSW = GetDlgItem(hDlg, IDC_PASSWORD);

	pswlen = GetWindowTextLength(hwndPSW);
	GetWindowText(hwndPSW, buff, pswlen);

	ret = checkSuperPassword(buff, pswlen);

	if(ret) { /* FAIL */
		check_psw_status = PSW_FAIL;
		MessageBox(hDlg,login_help[*curSysLang].msg_psw_wrong,
			   login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		SetWindowText(hwndPSW,"");
	} else {
		check_psw_status = PSW_OK;
		SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
	}

	return ret;
}

static void password_notif_proc(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainhwnd=GetMainWindowHandle(hwnd);

	switch(nc){
	case EN_SETFOCUS:
	    	SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);
	     	if(isIMEOpenInMotor == 1){
			ShowWindow(myIMEWindow,SW_HIDE);
			isIMEOpenInMotor = 0;
	    	}
	    	break;

	case EN_ENTER:
		check_password(mainhwnd);
		break;
	}
}

static int loadPasswordBkgnd(void)
{
	if (LoadBitmap (HDC_SCREEN, &passwordBkgnd, "./res/configItemBkgnd.jpg"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpPswFn[0], "./res/buttonLableF1.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpPswFn[1], "./res/buttonLableF2.bmp"))
		return 1;

	return 0;

}

static void unloadPasswordBkgnd(void)
{
	UnloadBitmap(&passwordBkgnd);
	UnloadBitmap(&btBmpPswFn[0]);
	UnloadBitmap(&btBmpPswFn[1]);
}

static void initPasswordDlg(HWND hwnd)
{
	check_psw_status = PSW_CANCEL;

	SetDlgItemText(hwnd, IDC_PASSWORD_DESC, strInputPassword);
}

static int checkPasswordDlgBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HWND hwndPSW = GetDlgItem(hDlg, IDC_PASSWORD);

	switch (message)
	{
	case MSG_INITDIALOG:
		SetNotificationCallback(hwndPSW, password_notif_proc);


		/* create buttons: F1 and F2 */
		CreateWindow (CTRL_BUTTON,
			      " F1 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_BUTTON_PSW_F1,
			      30, 100, 95, 42, hDlg, (DWORD)(&btBmpPswFn[0]));

		CreateWindow (CTRL_BUTTON,
			      " F2 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_BUTTON_PSW_F2,
			      190, 100, 95, 42, hDlg, (DWORD)(&btBmpPswFn[1]));

		initPasswordDlg(hDlg);

		SetFocusChild(hwndPSW);
		return 1;

	case MSG_ERASEBKGND:
	{
		HDC hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}
		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		FillBoxWithBitmap (hdc, 0, 0, PSWEDIT_W, PSWEDIT_H, &passwordBkgnd);

		if (fGetDC)
			ReleaseDC (hdc);

		return 0;
	}

	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_F1: /* Finish */
			check_password(hDlg);
			break;
		case SCANCODE_F2:
			check_psw_status = PSW_CANCEL;
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_CANCEL:
			check_psw_status = PSW_CANCEL;
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_HELP:
			MessageBox(hDlg,login_help[*curSysLang].help_psw,
				   login_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		}

	case MSG_COMMAND:
		switch(wParam){
		case IDC_BUTTON_PSW_F1:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_BUTTON_PSW_F2:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F2, 0L);
			break;

		case IDCANCEL:
			EndDialog (hDlg, wParam);
			break;
		}
		break;
	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void CheckPasswordDialogBox (HWND hWnd)
{
	CheckPasswordDlgBox.controls = CheckPasswordCtrls;
	DialogBoxIndirectParam (&CheckPasswordDlgBox, hWnd, checkPasswordDlgBoxProc, 0L);

}

int CheckPassword(HWND hWnd)
{
	printf("Check Password Dialog...\n");

	loadPasswordBkgnd();

	CheckPasswordDialogBox(hWnd);

	unloadPasswordBkgnd();

	printf("password status: %d \n", check_psw_status);
	return(check_psw_status);
}
