/*
 *  motor.h
 *
 *  Copyright 2008 @ xianweizeng@gmail.com
 *
 *  defined globle structure and use "extern" to declaim global variables
 *
 */

#ifndef _MOTOR_H_
#define _MOTOR_H_

/* debug configs, comment out other level */

/* 1 for debug on pc */
#define CONFIG_DEBUG_ON_PC     1

/* 2 for debug on target */
/* #define CONFIG_DEBUG_ON_TARGET    1*/

/* 3 for release */
/* #undef CONFIG_DEBUG_ON_PC */
/* #undef CONFIG_DEBUG_ON_TARGET */

#define SYSLANG_NO	2	/* system supported language number */


/*   MotorStatus structure
 */
struct motorStatus_t{
	int currentCutPos;	/* current position */
	int newCutPos;		/* new position */
	int devStatus;		/* motor status */
	int flags;              /* motor flags */
	int cutCounter;		/* cut counter */
};

/* defined in motormain.c */
extern struct motorStatus_t motorStatus;
extern HWND myIMEWindow;
extern int isIMEOpenInMotor;	/* 0: disappear 1: display */
extern PLOGFONT systemDefaultFont,userFont24,userFont16;
extern int *curSysLang;


#define IME_NUMBER   0  /* 123 */
#define IME_PINYIN   1  /* T9 Pinyin */
#define IME_ABC      2  /* ABC and mark */

/* defined IDC resources */
#define IDC_STATE	100
#define IDC_TIME 	110
#define IDC_TIMER    	120
#define IDC_EDITNAME	130
#define IDL_LISTNAME    140
#define IDC_PASSWORD	150
#define  IDL_FILE		160
#define 	IDC_PATH	170
#define _ID_TIMERP      180
#define IDC_COMSTOR    190
#define IDC_USTOR	200
#define IDC_RECENTPROGRAME	210
#define IDC_PROGRAME_HEAD    	220

#define IDC_CUT_POS		230
#define IDC_CURRENT_POS		235
#define IDC_POS_LIMIT		240
#define IDC_CHINESE		270
#define IDC_ENGLISH		280

#define IDC_YEAR		290
#define IDC_MONTH		291
#define IDC_DAY	        	292
#define IDC_HOUR		293
#define IDC_MINUTE		294

#define IDC_LIMIT_F		241
#define IDC_LIMIT_M		242
#define IDC_LIMIT_B		243
#define IDC_FORWARD_STEP	244
#define IDC_BACKWARD_STEP	245
#define IDC_RUN_SPEED		246
#define IDC_CURRENT_SPEED	247

#define IDC_USERBKGND_LIST	320
#define IDC_RUN_SPEED_LIST	321

#define IDC_LABLE_WINDOW_START  580
#define IDC_USER_WINDOW_START   600
#define IDC_FILE_ATTR_START     680
#define IDC_CONFIG_WINDOW_START 450

#define IDC_CURPOS		401

#define IDC_COMPUTERDATA	400

#define IDC_PROFILE_LIST	(IDC_USER_WINDOW_START + 50)
#define IDC_PROFILE_HEAD	(IDC_USER_WINDOW_START + 51)


#endif /* _MOTOR_H_ */

