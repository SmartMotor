/*
 * src/configure.c
 *
 * The setup window implementation
 *
 * Copyright 2007-2008 @ xianweizeng@gmail.com
 *
 * Author:
 *   Zeng Xianwei
 *
 *  File Coding system: gb2312
 *    set-buffer-file-coding-system
 *
 * ChangeLog:
 *	V0.1	  Quan Dongxiao
 *	V0.2	  Zeng Xianwei @ 2005-12-15
 *   2008-09-24   Change code indent
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <errno.h>
#include <time.h>

#include <unistd.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/select.h>

#include <minigui/common.h>
#include <minigui/minigui.h>
#include <minigui/gdi.h>
#include <minigui/window.h>
#include <minigui/control.h>

#include "motor.h"
#include "bkgndEdit.h"
#include "configure.h"
#include "motorregister.h"		// For 用户配置结构体
#include "userprogram.h"
#include "motormain.h"
#include "help.h"

/*
 *  setCutPos
 */
#define IDC_CUT_POS_DESC         (IDC_CONFIG_WINDOW_START + 0)
#define IDC_CUT_POS_EDIT         (IDC_CONFIG_WINDOW_START + 1)
#define IDC_CURRENT_POS_DESC     (IDC_CONFIG_WINDOW_START + 2)
#define IDC_CURRENT_POS_EDIT     (IDC_CONFIG_WINDOW_START + 3)
#define IDC_BUTTON_RUN           (IDC_CONFIG_WINDOW_START + 4)
#define IDC_BUTTON_MODIFY        (IDC_CONFIG_WINDOW_START + 5)
#define IDC_BUTTON_EXIT          (IDC_CONFIG_WINDOW_START + 6)

/*
 *   Machine Configure
 */
#define IDC_LIMIT_FRONT_DESC     (IDC_CONFIG_WINDOW_START + 10)  /* current value */
#define IDC_LIMIT_MIDDLE_DESC    (IDC_CONFIG_WINDOW_START + 11)
#define IDC_LIMIT_BACK_DESC      (IDC_CONFIG_WINDOW_START + 12)

#define IDC_LIMIT_FRONT_NOTE     (IDC_CONFIG_WINDOW_START + 30)
#define IDC_LIMIT_MIDDLE_NOTE    (IDC_CONFIG_WINDOW_START + 31)

/*
 * Common
 */
#define IDC_CFG_ITEM_STATUS      (IDC_CONFIG_WINDOW_START + 8)
#define IDC_CFG_BUTTON_F1        (IDC_CONFIG_WINDOW_START + 20)  /* done */
#define IDC_CFG_BUTTON_F2        (IDC_CONFIG_WINDOW_START + 21)  /* exit */


/*
 * SetPosLimit
 */
#define IDC_LIMIT_EDIT           (IDC_CONFIG_WINDOW_START + 22)
#define IDC_LIMIT_EDIT_DESC      (IDC_CONFIG_WINDOW_START + 23)

/*
 * SetSysTime
 */
#define IDC_TIME_HOUR_EDIT       (IDC_CONFIG_WINDOW_START + 24)
#define IDC_TIME_MIN_EDIT        (IDC_CONFIG_WINDOW_START + 25)
#define IDC_TIME_DESC            (IDC_CONFIG_WINDOW_START + 26)
#define IDC_TIME_SPLIT_HM        (IDC_CONFIG_WINDOW_START + 27)
#define IDC_TIME_FORMAT          (IDC_CONFIG_WINDOW_START + 28)  /* (H:M) */

/*
 * SetSysDate
 */
#define IDC_DATE_DESC            (IDC_CONFIG_WINDOW_START + 14)
#define IDC_DATE_YEAR_EDIT       (IDC_CONFIG_WINDOW_START + 24)
#define IDC_DATE_MON_EDIT        (IDC_CONFIG_WINDOW_START + 25)
#define IDC_DATE_DAY_EDIT        (IDC_CONFIG_WINDOW_START + 26)
#define IDC_DATE_SPLIT_YM        (IDC_CONFIG_WINDOW_START + 27)  /* 2008-11-03 */
#define IDC_DATE_SPLIT_MD        (IDC_CONFIG_WINDOW_START + 28)
#define IDC_DATE_FORMAT_DESC     (IDC_CONFIG_WINDOW_START + 29)  /* (Y-M-D) */

/*
 * SetSysLang
 */
#define IDC_LANG_DESC            (IDC_CONFIG_WINDOW_START + 15)
#define IDC_LANG_BT_0            (IDC_CONFIG_WINDOW_START + 16)
#define IDC_LANG_BT_1            (IDC_CONFIG_WINDOW_START + 17)

#define IDC_SPEAKER_DESC         (IDC_CONFIG_WINDOW_START + 15)
#define IDC_SPEAKER_BT_0         (IDC_CONFIG_WINDOW_START + 16)
#define IDC_SPEAKER_BT_1         (IDC_CONFIG_WINDOW_START + 17)


/* common window */
#define IDC_BT_LIMIT_FRONT       (IDC_CONFIG_WINDOW_START + 37)
#define IDC_BT_LIMIT_MIDDLE      (IDC_CONFIG_WINDOW_START + 38)
#define IDC_BT_LIMIT_BACK        (IDC_CONFIG_WINDOW_START + 39)
#define IDC_BT_DATE              (IDC_CONFIG_WINDOW_START + 40)
#define IDC_BT_TIME              (IDC_CONFIG_WINDOW_START + 41)
#define IDC_BT_LANG              (IDC_CONFIG_WINDOW_START + 42)
#define IDC_BT_SPEAKER           (IDC_CONFIG_WINDOW_START + 43)
#define IDC_BT_EXIT              (IDC_CONFIG_WINDOW_START + 44)

#define IDC_BT_START             IDC_BT_LIMIT_FRONT
#define MACH_CONFIG_ITEM         8

#define CFG_ITEM_F_LIMIT         0
#define CFG_ITEM_M_LIMIT         1
#define CFG_ITEM_B_LIMIT         2
#define CFG_ITEM_DATE            3
#define CFG_ITEM_TIME            4
#define CFG_ITEM_LANG            5
#define CFG_ITEM_SPEAKER         6
#define CFG_ITEM_EXIT            7

#define CFG_BT_W                 100
#define CFG_BT_H                 30

#define CFG_ITEM_WIN_X           220
#define CFG_ITEM_WIN_Y           140
#define CFG_ITEM_WIN_W           340
#define CFG_ITEM_WIN_H           200

static char strConfigItems[SYSLANG_NO][MACH_CONFIG_ITEM][10] =
{
	/* 0: Chinese */
	{
		"前极限","中极限","后极限","系统日期","系统时间","语言选择","语音提示","退出"
	},
	/* 1: English */
	{
		"F Limit","M Limit","B Limit","Date","Time","Language","Speaker","Exit"
	}
};


static BITMAP configureBkgnd;
static BITMAP btBmpFileRun, btBmpFileModify, btBmpFileExit;

static BITMAP btBmpCfgFn[2];   /* F1: done  F2: exit */

static BITMAP configureListBkgnd;

static int currentConfig;      /* CFG_ITEM_XXX */

static struct comCmdData_t cmdData;

struct config_help_t{
	unsigned char * caption_help;
	unsigned char * caption_waring;
	unsigned char * caption_bkgnd;
	unsigned char * msg_cutdata_overflow;
	unsigned char * msg_step_overflow;
	unsigned char * msg_config_ok;
	unsigned char * help_config;
	unsigned char * help_date;
	unsigned char * help_time;
	unsigned char * help_lang;
	unsigned char * help_flash;
	unsigned char * help_cutpos;
	unsigned char * help_limit;
	unsigned char * help_step;
	unsigned char * help_bkgnd;
};

static struct config_help_t config_help[SYSLANG_NO] =
{
	{	caption_help_cn,
		caption_waring_cn,
		caption_bkgnd_cn,
		msg_cutdata_overflow_cn,
		msg_step_overflow_cn,
		msg_config_ok_cn,
		help_config_cn,
		help_date_cn,
		help_time_cn,
		help_lang_cn,
		help_flash_cn,
		help_cutpos_cn,
		help_limit_cn,
		help_step_cn,
		help_bkgnd_cn
	},

	{	caption_help_en,
		caption_waring_en,
		caption_bkgnd_cn,
		msg_cutdata_overflow_en,
		msg_step_overflow_en,
		msg_config_ok_en,
		help_config_en,
		help_date_en,
		help_time_en,
		help_lang_en,
		help_flash_en,
		help_cutpos_en,
		help_limit_en,
		help_step_en,
		help_bkgnd_en
	}
};

/* PCF8563 device */
#define RTC_DEVICE	"/dev/rtc"


/* RTC ioctl commands macro
 *   See kernel driver source code
 */
#define RTC_GETDATETIME	    0
#define RTC_SETDATE	    1
#define RTC_SETTIME	    2
#define RTC_SETDATETIME	    3
#define RTC_GETCTRL	    4
#define RTC_SETCTRL	    5

/*******************************************************************/
/*               GLOBAL RTC FUNCTIONS                              */

/*
 *get system time from RTC chip
 */
int getSysRtcTime(struct rtc_tm *tm)
{
	int fd,ret;

	fd = open(RTC_DEVICE,O_RDONLY);
	if(fd < 0){
		perror("error:can not open rtc devicen");
		return(-1);
	}

	ret = ioctl(fd,RTC_GETDATETIME,tm);
	if(ret < 0){
		perror("Get time error.\n");
		close(fd);
		return -1;
	}

	close(fd);
	return(0);
}


/*
 * convert time to string
 */
char * convertSysTime(char *dispBuf)
{
#ifdef CONFIG_DEBUG_ON_PC
	sprintf(dispBuf,"2008-01-01 22:12:50");
	return(dispBuf);
#endif
	struct rtc_tm tm;
	getSysRtcTime(&tm);

	sprintf(dispBuf,"%04d-%02d-%02d %02d:%02d:%02d", tm.tm_year,tm.tm_mon+1,tm.tm_mday,
		tm.tm_hour,tm.tm_min,tm.tm_sec);

	return(dispBuf);
}


/*
 *  flag = 0 time   1 date
 */
static int setSysRtcDateTime(struct rtc_tm *tm,int flag)
{
#ifdef CONFIG_DEBUG_ON_PC

	printf("Set RTC OK.\n");
	return 0;
#else
	int fd, ret;

	/* printf("Open RTC devices.\n"); */
	fd=open(RTC_DEVICE,O_RDWR);
	if(fd < 0){
		perror("error:can not open rtc device: %s\n");
		return(-1);
	}

	ret = ioctl(fd,flag,tm);
	if(ret < 0){
		perror("Set system datetime error.\n");
		close(fd);
		return -1;
	}

	close(fd);
	return 0;
#endif
}

/*****************************************************************************************/

/*
 *  Cut position config
 *
 *  Called in MD_MANUAL and MD_AUTO.
 */

static DLGTEMPLATE setCutPosDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	LISTBOX_X , LISTBOX_Y, CONFIG_WIN_W, CONFIG_WIN_H,
	"setCutPos",
	0, 0,
	4, NULL,
	0
};

static CTRLDATA setCutPosCtrlsInit[] =
{
	{
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		130, 50, 100, 30,
		IDC_CUT_POS_DESC,
		"",
		0,
		WS_EX_TRANSPARENT,
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER,
		240, 50, 150, 30,
		IDC_CUT_POS_EDIT,
		"",
		0
	},
	{
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		130, 100, 100, 30,
		IDC_CURRENT_POS_DESC,
		"",
		0,
		WS_EX_TRANSPARENT,
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER | ES_READONLY,
		240, 100, 150, 30,
		IDC_CURRENT_POS_EDIT,
		"",
		0,
		WS_EX_TRANSPARENT
	}
};

static int loadPosConfigBkgndFiles(void)
{
	if (LoadBitmap (HDC_SCREEN, &configureBkgnd, "./res/configItemBkgnd.jpg"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpFileRun, "./res/buttonRun.bmp"))  /* Run */
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpFileModify, "./res/buttonModify.bmp"))  /* Modify */
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpFileExit, "./res/buttonClear.bmp"))  /* Exit */
		return 1;

	return 0;
}

static void unloadPosConfigBkgndFiles(void)
{

	UnloadBitmap(&configureBkgnd);
	UnloadBitmap(&btBmpFileRun);
	UnloadBitmap(&btBmpFileModify);
	UnloadBitmap(&btBmpFileExit);
}

static void initSetCutPosBkgnd(HDC hdc)
{
	FillBoxWithBitmap (hdc, 0, 0, CONFIG_WIN_W, CONFIG_WIN_H, &configureBkgnd);

}

/*
 * Modify current position
 */
static void modifyCurrentCutPos(HWND mainWnd,HWND editWnd)
{
	int dataLen;
	char dataBuf[10];
	char cmdbuf[13];
	int unit = currentuser->unit;
	int ret,data;
	dataLen = GetWindowTextLength(editWnd); /* get input data */
	GetWindowText(editWnd,dataBuf,dataLen);
	ret = isInputCutsDataValid(dataBuf,dataLen);
	if(ret >= 0 ){
		cmdData.flag = 'P';
		cmdData.status = 0x30;
		if(unit){
			data = inch_to_mm(ret);
			ret = data;
		}
		cmdData.data = ret;
		setCmdFrame(&cmdData,cmdbuf);
		sendCommand(cmdbuf);  /* cut position modify */

	}else {
		MessageBox(mainWnd,config_help[*curSysLang].msg_cutdata_overflow,
			   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		SetWindowText(editWnd,""); /* clear input box */
	}
}

static int runToInputPos(HWND editHwnd)
{
	int dataLen;
	char dataBuf[10];
	char cmdbuf[13];
	int unit = currentuser->unit;
	int ret,data;
	dataLen = GetWindowTextLength(editHwnd);
	GetWindowText(editHwnd,dataBuf,dataLen);
	ret=isInputCutsDataValid(dataBuf,dataLen);
	if(ret >= 0 ){
		cmdData.flag = 'G';
		cmdData.status = 0x33;
		if(unit){
			data = inch_to_mm(ret);
			ret = data;
		}
		cmdData.data = ret;
		setCmdFrame(&cmdData,cmdbuf);
		sendCommand(cmdbuf);
	}else {
		MessageBox(editHwnd,config_help[*curSysLang].msg_cutdata_overflow,
			   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		SetWindowText(editHwnd,"");
	}
	return(ret);
}

#if 0      /* invalidate */
static void setCutPosEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	int dataLen;
	char dataBuf[10];
	char cmdbuf[13];
	int unit = currentuser->unit;
	int ret,data;

	mainHwnd = GetMainWindowHandle(hwnd);
	switch(nc){
	case EN_ENTER:
		dataLen = GetWindowTextLength(hwnd);
		GetWindowText(hwnd,dataBuf,dataLen);
		ret=isInputCutsDataValid(dataBuf,dataLen);
		if(ret >= 0 ){
			cmdData.flag = 'G';
			cmdData.status = 0x33;
			if(unit){
				data = inch_to_mm(ret);
				ret = data;
			}
			cmdData.data = ret;
			setCmdFrame(&cmdData,cmdbuf);
			sendCommand(cmdbuf);
		}else {
			MessageBox(hwnd,config_help[*curSysLang].msg_cutdata_overflow,
				   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
			SetWindowText(hwnd,"");
		}

		break;
	}
}
#endif

static char strCutPosDesc[] = "切刀位置";
static char strCurrentPosDesc[] = "当前位置";


static void initSetCutPosDlg(HWND hwnd)
{
	HWND hwndCutPosDesc = GetDlgItem(hwnd,IDC_CUT_POS_DESC);
	HWND hwndCurrentPosDesc = GetDlgItem(hwnd,IDC_CURRENT_POS_DESC);

	SetWindowText(hwndCutPosDesc, strCutPosDesc);
	SetWindowText(hwndCurrentPosDesc, strCurrentPosDesc);

#if 0
	/* set background color */
	SetWindowBkColor(hwndCutPosDesc, PIXEL_yellow);
	InvalidateRect(hwndCutPosDesc, NULL, TRUE);

	SetWindowBkColor(hwndCurrentPosDesc, PIXEL_yellow);
	InvalidateRect(hwndCurrentPosDesc, NULL, TRUE);
#endif
}

static int setCutPosDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	char cmdbuf[12];
	int data,index;
	int curPos;
	int unit = currentuser->unit;
	HWND hwndCutPosEdit = GetDlgItem(hDlg,IDC_CUT_POS_EDIT);
	HWND hwndCurrentPosEdit = GetDlgItem(hDlg,IDC_CURRENT_POS_EDIT);

	switch (message)
	{
    	case MSG_INITDIALOG:
		/* SetNotificationCallback(hwndCutPosEdit, setCutPosEditCallback); */
		curPos = motorStatus.currentCutPos;
		refreshCurrentPos(hwndCurrentPosEdit,curPos);

		initSetCutPosDlg(hDlg);
		CreateWindow (CTRL_BUTTON,
			      " run ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_BUTTON_RUN,
			      100, 160, 94, 42, hDlg, (DWORD)(&btBmpFileRun));

		CreateWindow (CTRL_BUTTON,
			      " modify ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_BUTTON_MODIFY,
			      210, 160, 94, 42, hDlg, (DWORD)(&btBmpFileModify));

		CreateWindow (CTRL_BUTTON,
			      " exit ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_BUTTON_EXIT,
			      320, 160, 94, 42, hDlg, (DWORD)(&btBmpFileExit));

		SetFocusChild(hwndCutPosEdit);

		return(1);

    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initSetCutPosBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_FORWARD:		//  切刀前进, 发送前进命令
			memset(cmdbuf,0,12);
			cmdData.flag = 'F';
			cmdData.status = 0x30;		// 状态字节含有前进/后退标志
			cmdData.data = 0;
			setCmdFrame(&cmdData,cmdbuf);
			sendCommand(cmdbuf);
			break;
		case SCANCODE_BACKWARD:	//  切刀后退,发送后退命令
			memset(cmdbuf,0,12);
			cmdData.flag = 'F';
			cmdData.status = 0x31;
			cmdData.data = 0;
			setCmdFrame(&cmdData,cmdbuf);
			sendCommand(cmdbuf);
			break;
		case SCANCODE_FORWARD_HS:
			memset(cmdbuf,0,12);
			cmdData.flag = 'F';
			cmdData.status = 0x32;		// 状态字节含有前进/后退标志
			cmdData.data = 0;
			setCmdFrame(&cmdData,cmdbuf);
			sendCommand(cmdbuf);
			break;
		case SCANCODE_BACKWARD_HS:	//  切刀后退,发送后退命令
			memset(cmdbuf,0,12);
			cmdData.flag = 'F';
			cmdData.status = 0x33;
			cmdData.data = 0;
			setCmdFrame(&cmdData,cmdbuf);
			sendCommand(cmdbuf);
			break;

		case SCANCODE_MODIFY:		//  修改位置(校正)
			modifyCurrentCutPos(hDlg,hwndCutPosEdit);
			break;

		case SCANCODE_ENTER:		//  确定键,修改位置(校正)+退出
			modifyCurrentCutPos(hDlg,hwndCutPosEdit);
			for(index=0;index<50;index++)	// 等待接收下位机返回修正信息
				usleep(10000);
			SendMessage(hDlg,MSG_RECV,0,0);	// 手动接收信息
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_DELETE:		//  删除键,清除
			SetFocusChild(hwndCutPosEdit);
			SetWindowText(hwndCutPosEdit,"");
			break;

		case SCANCODE_RUN:		//  运行,跟Enter一样的功能
			runToInputPos(hwndCutPosEdit);

			break;
		case SCANCODE_HELP:		//   帮助
			MessageBox(hDlg,config_help[*curSysLang].help_cutpos,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		case SCANCODE_CANCEL:		//   取消
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		}
		break;

    	case MSG_RECV:		// 收到位置信息,更新当前位置显示区
		recvCommand(cmdbuf);		// 复制到用户缓冲区
		//ret = isCmdValid(cmdbuf);	// 测试,不用校验
		//if(ret){
	    	memset(&cmdData,0,sizeof(cmdData));
	    	getCmdFrame(&cmdData,cmdbuf);	// 获得数据
	    	if(cmdData.flag == 'G'){			// 正常数据帧
			data = cmdData.data;
			if(unit){
				data = mm_to_inch(cmdData.data);
			}
			motorStatus.currentCutPos = data;	// 更新数据
			motorStatus.devStatus = cmdData.status;		// 更新状态,低四位有效
			refreshCurrentPos(hwndCurrentPosEdit,data);	// 设置当前位置
		}
	    	if(cmdData.flag == 'P'){			// 调整帧
			data = cmdData.data;
			if(unit){
				data = mm_to_inch(cmdData.data);
			}
			motorStatus.currentCutPos = data;			// 更新数据
			motorStatus.devStatus = cmdData.status;		// 更新状态,低四位有效
			refreshCurrentPos(hwndCurrentPosEdit,data);	// 设置当前位置
		}
		//}
		break;

    	case MSG_COMMAND:
		switch(wParam){

		case IDC_BUTTON_RUN:
			SetFocusChild(hwndCutPosEdit);
			printf("IDC_BUTTON_RUN push\n");
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_RUN, 0L);
			break;

		case IDC_BUTTON_MODIFY:
			SetFocusChild(hwndCutPosEdit);
			printf("IDC_BUTTON_MODIFY push\n");
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_MODIFY, 0L);
			break;

		case IDC_BUTTON_EXIT:
	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;


	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}


static void setCutPosDialogBox (HWND hwnd)
{
	setCutPosDlgBox.controls = setCutPosCtrlsInit;
	DialogBoxIndirectParam (&setCutPosDlgBox, hwnd,setCutPosDialogBoxProc, 0L);
}

int setCutPos (HWND hwnd)
{
	//printf("setCutPos().\n");
	loadPosConfigBkgndFiles();

	setCutPosDialogBox(hwnd);		// Should not return except receive CLOSE MSG

	unloadPosConfigBkgndFiles();
	return 0;

}

/*****************************************************************************************/

/*
 *  Machine Config Window
 */

/*****************************************************************************************/

static DLGTEMPLATE setPosLimitDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	CFG_ITEM_WIN_X, CFG_ITEM_WIN_Y, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H,
	"setPosLimit",
	0, 0,
	2, NULL,
	0
};

static CTRLDATA setPosLimitCtrlsInit[] =
{
	{       /* description */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		30, 44, 100, 30,
		IDC_LIMIT_EDIT_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER,
		140, 40, 120, 30,
		IDC_LIMIT_EDIT,
		"",
		0
	}
};

static void initSetPosLimitBkgnd(HDC hdc)
{
	FillBoxWithBitmap (hdc, 0, 0, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H, &configureListBkgnd);
}


static void setPosLimitEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	int dataLen,data;
	char dataBuf[10];
	int status = currentConfig;
	int unit = currentuser->unit;
	char cmdSendBuf[13];
	memset(cmdSendBuf,0,12);
	cmdData.flag = 'L';

	mainHwnd = GetMainWindowHandle(hwnd);
	switch(nc){
	case EN_SETFOCUS:
		// Do nothing
		break;
	case EN_ENTER:
		dataLen = GetWindowTextLength(hwnd);
		GetWindowText(hwnd,dataBuf,dataLen);
		int ret=isInputCutsDataValid(dataBuf,dataLen);
		if(ret >= 0 ){
			switch(status){
			case CFG_ITEM_F_LIMIT:
				currentuser->frontlimit = ret;
				saveUserConfigFile(0);
				cmdData.status = 0x30;
				break;
			case CFG_ITEM_M_LIMIT:
				currentuser->middlelimit = ret;
				saveUserConfigFile(0);
				cmdData.status = 0x31;
				break;
			case CFG_ITEM_B_LIMIT:
				currentuser->backlimit = ret;
				saveUserConfigFile(0);
				cmdData.status = 0x32;
				break;
			}

			if(unit){
				data = inch_to_mm(ret);
			} else {
				data = ret;
			}
			cmdData.data = data;
			setCmdFrame(&cmdData,cmdSendBuf);
			sendCommand(cmdSendBuf);
			MessageBox(hwnd,config_help[*curSysLang].msg_config_ok,
				   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
			SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0); /* exit */
		}else {
			MessageBox(hwnd,config_help[*curSysLang].msg_cutdata_overflow,
				   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
			SetWindowText(hwnd,""); /* clear */
		}
		break;
	}
}

static void initSetPosLimitDlg(HWND hDlg)
{
	int lang = *curSysLang;
	char strBuf[16];

	int len = strlen(strConfigItems[lang][currentConfig]);

	if(len > 15)
		len = 15;

	strncpy(strBuf, strConfigItems[lang][currentConfig], len);
	strBuf[len] = 0;

	SetDlgItemText(hDlg, IDC_LIMIT_EDIT_DESC, strBuf);
}

static int setPosLimitDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndCutPosEdit = GetDlgItem(hDlg,IDC_LIMIT_EDIT);

	switch (message)
	{
    	case MSG_INITDIALOG:
		SetNotificationCallback(hwndCutPosEdit, setPosLimitEditCallback);

		CreateWindow (CTRL_BUTTON,
			      " F1 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F1,
			      40, 120, 95, 42, hDlg, (DWORD)(&btBmpCfgFn[0]));

		CreateWindow (CTRL_BUTTON,
			      " F2 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F2,
			      190, 120, 95, 42, hDlg, (DWORD)(&btBmpCfgFn[1]));

		initSetPosLimitDlg(hDlg);
		SetFocusChild(hwndCutPosEdit);
		return(1);

    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}
		initSetPosLimitBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_F1: /* done  */
			SetFocusChild(hwndCutPosEdit);
			SendMessage(hwndCutPosEdit,MSG_KEYDOWN,SCANCODE_ENTER,0L);

			break;
		case SCANCODE_F2:  /* cancel and edit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_CANCEL:
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_HELP:
			MessageBox(hDlg,config_help[*curSysLang].help_limit,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		}
		break;
    	case MSG_COMMAND:
		switch(wParam){
		case IDC_CFG_BUTTON_F1:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_CFG_BUTTON_F2:  /* Exit, call EndDialog() directly */
			/* SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0); */
			EndDialog(hDlg,IDOK);
			break;
	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;


	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}


static void setPosLimitDialogBox (HWND hwnd)
{
	setPosLimitDlgBox.controls = setPosLimitCtrlsInit;
	DialogBoxIndirectParam (&setPosLimitDlgBox, hwnd,setPosLimitDialogBoxProc, 0L);
}
static int setPosLimit (HWND hwnd)
{
	//printf("setPosLimit().\n");
	setPosLimitDialogBox(hwnd);		// Should not return except receive CLOSE MSG
	return 0;

}


static DLGTEMPLATE setSysDateDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	CFG_ITEM_WIN_X, CFG_ITEM_WIN_Y, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H,
	"setSysDate",
	0, 0,
	8, NULL,
	0
};

static CTRLDATA setSysDateCtrlsInit[] =
{
	{       /* description */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		30, 44, 100, 30,
		IDC_DATE_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER,
		140, 40, 50, 30,
		IDC_DATE_YEAR_EDIT,
		"",
		0
	},
	{       /* - */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		190, 44, 20, 30,
		IDC_DATE_SPLIT_YM,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER,
		210, 40, 25, 30,
		IDC_DATE_MON_EDIT,
		"",
		0
	},
	{       /* - */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		235, 44, 20, 30,
		IDC_DATE_SPLIT_MD,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER,
		255, 40, 32, 30,
		IDC_DATE_DAY_EDIT,
		"",
		0
	},
	{       /* Y-M-D */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		160, 70, 120, 30,
		IDC_DATE_FORMAT_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{       /* operation help */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		10, 175, 320, 24,
		IDC_CFG_ITEM_STATUS,
		"",
		0,
		WS_EX_TRANSPARENT
	}
};

static struct rtc_tm tmSet;

static int dateInputStatus = 0;

static char strSysDateHelp[SYSLANG_NO][48] =
{
	{"使用↑和↓选择年/月/日"},
	{"Use Up/Down arrow to choose Y/M/D"}
};


/*
 * Check the date or time is valid
 */
static int isDateTimeValid(struct rtc_tm *tm,int flag)
{
	switch(flag){
	case RTC_SETDATE: /* date */

		break;
	case RTC_SETTIME: /* time */

		break;
	}

	return(0);
}

static void initSetSysDateBkgnd(HDC hdc)
{
	FillBoxWithBitmap(hdc, 0, 0, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H, &configureListBkgnd);
}

/*
 * Date check
 *   flag:  0 Year
 *          1 Month
 *          2 Day
 *
 *   return  0:  error
 *           >0: right data
 */
static int isInputDateValid(char *pBuf,int flag)
{
	int data = atoi(pBuf);

	switch(flag){
	case 0: /* Year */
		if((data < 2000)|| (data>2099)){
			return(0);
		}
		break;
	case 1:	/* Month */
		if((data < 1)|| (data>12)){
			return(0);
		}
		break;
	case 2:	/* Day */
		if((data < 1)|| (data>31)){
			return(0);
		}
		break;
	}

	return(data);
}

static int getDateEditData(HWND hDlg)
{
	int index,ret;
	char cutDataBuf[10];
	int  cutDataLen = 0;
	HWND hwndEdit[3];
	hwndEdit[0] = GetDlgItem(hDlg,IDC_DATE_YEAR_EDIT);
	hwndEdit[1] = GetDlgItem(hDlg,IDC_DATE_MON_EDIT);
	hwndEdit[2] = GetDlgItem(hDlg,IDC_DATE_DAY_EDIT);

	if(dateInputStatus == 7){
		return 0;
	}

	//tmSet.tm_year = 0;
	//tmSet.tm_mon = 0;
	//tmSet.tm_mday = 0;
	dateInputStatus = 0;

	for(index=0;index<3;index++){
		memset(cutDataBuf,0,10);
		cutDataLen = GetWindowTextLength(hwndEdit[index]);
		GetWindowText(hwndEdit[index],cutDataBuf,cutDataLen);
		ret = isInputDateValid(cutDataBuf,index);
		if(ret > 0 ){
			switch(index){
			case 0:
				tmSet.tm_year = ret - 1900;
				dateInputStatus |= 0x1;	//bit 0
				break;
			case 1:
				tmSet.tm_mon = ret - 1;
				dateInputStatus |= 0x2;
				break;
			case 2:
				tmSet.tm_mday = ret;
				dateInputStatus |= 0x4;
				break;
			}
		}else{
			SetWindowText(hwndEdit[index],"");
			SetFocusChild(hwndEdit[index]);
			//printf("getlabledata status=%d \n",pProEdit->lableEdit.lableStatus);
			return(-1);
		}

	}
	return 0;


}
static void setSysYearEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	HWND monthHwnd;
	int year;
	int dataLen;
	char dataBuf[6];

	mainHwnd = GetMainWindowHandle(hwnd);
	monthHwnd = GetDlgItem(mainHwnd,IDC_DATE_MON_EDIT);

	switch(nc){
	case EN_CHANGE:
		dataLen = GetWindowTextLength(hwnd);
		if(dataLen == 4){  /* Year done */
			GetWindowText(hwnd,dataBuf,dataLen);
			year = isInputDateValid(dataBuf,0);
			if(!year){
				SetWindowText(hwnd,"");
			} else {
				dateInputStatus |= 0x1;
				/* printf("year = %d \n",year); */
				tmSet.tm_year = year - 1900;
				SetFocusChild(monthHwnd);
			}
		}else if(dataLen > 4){
			SetWindowText(hwnd,"");
		}

		break;
	}
}
static void setSysMonthEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	HWND dayHwnd;
	int month;
	int dataLen;
	char dataBuf[6];

	mainHwnd = GetMainWindowHandle(hwnd);
	dayHwnd = GetDlgItem(mainHwnd,IDC_DATE_DAY_EDIT);

	switch(nc){
	case EN_CHANGE:
		dataLen = GetWindowTextLength(hwnd);
		if(dataLen == 2){
			GetWindowText(hwnd,dataBuf,dataLen);
			month = isInputDateValid(dataBuf,1);
			if(!month){
				SetWindowText(hwnd,"");
			} else {
				dateInputStatus |= 0x2;
				//printf("month = %d \n",month);
				tmSet.tm_mon = month - 1;
				SetFocusChild(dayHwnd);
			}
		}else if(dataLen > 2){
			SetWindowText(hwnd,"");
		}

		break;
	}
}

static void setSysDayEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	int day;
	int dataLen;
	char dataBuf[6];

	mainHwnd = GetMainWindowHandle(hwnd);
	switch(nc){
	case EN_CHANGE:
		dataLen = GetWindowTextLength(hwnd);
		if(dataLen == 2){
			GetWindowText(hwnd,dataBuf,dataLen);
			day = isInputDateValid(dataBuf,2);
			if(!day){
				SetWindowText(hwnd,"");	// clear
			} else {
				dateInputStatus |= 0x4;
				//printf("day = %d \n",day);
				tmSet.tm_mday = day;
			}
		} else if(dataLen > 2){
			SetWindowText(hwnd,"");
		}

		break;
	}
}

static void initSetSysDateDlg(HWND hDlg)
{
	int lang = *curSysLang;
	char strBuf[16];

	int len = strlen(strConfigItems[lang][currentConfig]);

	if(len > 15)
		len = 15;

	strncpy(strBuf, strConfigItems[lang][currentConfig], len);
	strBuf[len] = 0;

	SetDlgItemText(hDlg, IDC_DATE_DESC, strBuf);

	/* - */
	SetDlgItemText(hDlg, IDC_DATE_SPLIT_YM, "-");
	SetDlgItemText(hDlg, IDC_DATE_SPLIT_MD, "-");

	/* Y-M-D */
	SetDlgItemText(hDlg, IDC_DATE_FORMAT_DESC, "(Y - M - D)");

	SetDlgItemText(hDlg, IDC_CFG_ITEM_STATUS, strSysDateHelp[lang]);
	/* Also change its bkcolor */
}

static int setSysDateDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndTmp,hwndEdit[3];
	hwndEdit[0] = GetDlgItem(hDlg,IDC_DATE_YEAR_EDIT);
	hwndEdit[1] = GetDlgItem(hDlg,IDC_DATE_MON_EDIT);
	hwndEdit[2] = GetDlgItem(hDlg,IDC_DATE_DAY_EDIT);

	struct rtc_tm tm;
	char dispBuf[6];

	switch (message)
	{
    	case MSG_INITDIALOG:
		SetNotificationCallback(hwndEdit[0], setSysYearEditCallback);
		SetNotificationCallback(hwndEdit[1], setSysMonthEditCallback);
		SetNotificationCallback(hwndEdit[2], setSysDayEditCallback);

#ifdef CONFIG_DEBUG_ON_PC
		tm.tm_year = 2006;
		tm.tm_mon = 9;
		tm.tm_mday = 1;
#else
		getSysRtcTime(&tm);
#endif
		sprintf(dispBuf,"%04d",tm.tm_year);
		SetWindowText(hwndEdit[0],dispBuf);
		SendMessage(hwndEdit[0], EM_SETCARETPOS, 0, 4);

		sprintf(dispBuf,"%02d",tm.tm_mon+1);
		SetWindowText(hwndEdit[1],dispBuf);
		SendMessage(hwndEdit[1], EM_SETCARETPOS, 0, 2);

		sprintf(dispBuf,"%02d",tm.tm_mday);
		SetWindowText(hwndEdit[2],dispBuf);
		SendMessage(hwndEdit[2], EM_SETCARETPOS, 0, 2);

		CreateWindow (CTRL_BUTTON,
			      " F1 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F1,
			      40, 120, 95, 42, hDlg, (DWORD)(&btBmpCfgFn[0]));

		CreateWindow (CTRL_BUTTON,
			      " F2 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F2,
			      190, 120, 95, 42, hDlg, (DWORD)(&btBmpCfgFn[1]));

		dateInputStatus = 0;
		initSetSysDateDlg(hDlg);

		SetFocusChild(hwndEdit[0]);
		return(1);

    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initSetSysDateBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_CURSORBLOCKUP:
			hwndTmp=GetFocusChild(hDlg);
			if(hwndTmp == hwndEdit[0]){
				break;
			}
			if(hwndTmp == hwndEdit[2]){
				SetFocusChild(hwndEdit[1]);
			}
			if(hwndTmp == hwndEdit[1]){
				SetFocusChild(hwndEdit[0]);
			}
			break;
		case SCANCODE_CURSORBLOCKDOWN:
			hwndTmp=GetFocusChild(hDlg);
			if(hwndTmp == hwndEdit[2]){
				break;
			}
			if(hwndTmp == hwndEdit[0]){
				SetFocusChild(hwndEdit[1]);
			}
			if(hwndTmp == hwndEdit[1]){
				SetFocusChild(hwndEdit[2]);
			}
			break;

		case SCANCODE_F2:  /* cancel and edit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_F1:  /* done  */
		case SCANCODE_ENTER:
			getDateEditData(hDlg);
#if 0
			printf("Y-M-D:%4d-%2d-%2d \n",tmSet.tm_year,tmSet.tm_mon,tmSet.tm_mday);
#endif
			if(dateInputStatus != 7){
				SetFocusChild(hwndEdit[0]);
				break;
			}
			if(!isDateTimeValid(&tmSet, RTC_SETDATE)){
				int ret = setSysRtcDateTime(&tmSet, RTC_SETDATE);
				if(!ret){
					SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
				}
			}
			break;
		case SCANCODE_CANCEL:
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_HELP:
			MessageBox(hDlg,config_help[*curSysLang].help_date,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		}
		break;
    	case MSG_COMMAND:
		switch(wParam){
		case IDC_CFG_BUTTON_F1:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_CFG_BUTTON_F2:  /* Exit, call EndDialog() directly */
			EndDialog(hDlg,IDOK);
			break;

	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;


	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setSysDateDialogBox (HWND hwnd)
{
	setSysDateDlgBox.controls = setSysDateCtrlsInit;
	DialogBoxIndirectParam (&setSysDateDlgBox, hwnd,setSysDateDialogBoxProc, 0L);
}

static int setSysDate (HWND hwnd)
{
	setSysDateDialogBox(hwnd);
	return 0;
}

/***********************************************************/

static DLGTEMPLATE setSysTimeDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	CFG_ITEM_WIN_X, CFG_ITEM_WIN_Y, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H,
	"setSysTime",
	0, 0,
	6, NULL,
	0
};

static CTRLDATA setSysTimeCtrlsInit[] =
{
	{       /* description */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		30, 44, 100, 30,
		IDC_TIME_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER,
		140, 40, 40, 30,
		IDC_TIME_HOUR_EDIT,
		"",
		0
	},
	{       /* : */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		180, 44, 20, 30,
		IDC_TIME_SPLIT_HM,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE | WS_BORDER,
		200, 40, 40, 30,
		IDC_TIME_MIN_EDIT,
		"",
		0
	},
	{       /* Hour:Min */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		140, 75, 100, 30,
		IDC_TIME_FORMAT,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{       /* operation help */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		10, 175, 320, 24,
		IDC_CFG_ITEM_STATUS,
		"",
		0,
		WS_EX_TRANSPARENT
	}
};

static char strSysTimeHelp[SYSLANG_NO][32] =
{
	{"使用↑和↓选择小时或分钟"},
	{"Use Up/Down arrow to choose"}
};

static int timeInputStatus = 0;

static void initSetSysTimeBkgnd(HDC hdc)
{
	FillBoxWithBitmap(hdc, 0, 0, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H, &configureListBkgnd);
}

static int isInputTimeValid(char *pBuf,int flag)
{
	int data = atoi(pBuf);

	switch(flag){
	case 0:
		if((data < 0)|| (data>23)){
			return(-1);
		}
		break;
	case 1:
		if((data < 0)|| (data>59)){
			return(-1);
		}
		break;
	}

	return(data);

}
static void setSysHourEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	int hour;
	int dataLen;
	char dataBuf[6];

	HWND mainHwnd = GetMainWindowHandle(hwnd);
	HWND minuteHwnd = GetDlgItem(mainHwnd,IDC_TIME_MIN_EDIT);

	switch(nc){
	case EN_CHANGE:
		dataLen = GetWindowTextLength(hwnd);
		if(dataLen == 2){
			GetWindowText(hwnd,dataBuf,dataLen);
			hour = isInputTimeValid(dataBuf,0);
			if(hour < 0){
				SetWindowText(hwnd,"");
			} else {
				/* printf("hour = %d \n",hour); */
				timeInputStatus |= 0x01;
				tmSet.tm_hour = hour;
				SetFocusChild(minuteHwnd);
			}
		} else if(dataLen > 2){
			SetWindowText(hwnd,"");
		}
		break;
	}
}
static void setSysMinuteEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	int minute;
	int dataLen;
	char dataBuf[6];

	switch(nc){
	case EN_CHANGE: /* 0 - 9 pressed */
		dataLen = GetWindowTextLength(hwnd);
		if(dataLen == 2){
			GetWindowText(hwnd,dataBuf,dataLen);
			minute = isInputTimeValid(dataBuf,1);
			if(minute < 0){
				SetWindowText(hwnd,"");
			} else {
				timeInputStatus |= 0x02;
				tmSet.tm_min = minute;
				tmSet.tm_sec = 0;
				/* printf("min = %d \n",minute); */
			}
		}else if(dataLen > 2){
			SetWindowText(hwnd,"");
		}
		break;
	}
}

static int getTimeEditData(HWND hDlg)
{
	char dataBuf[10];
	int dataLen = 0;
	int ret;
	HWND hwndHourEdit = GetDlgItem(hDlg,IDC_TIME_HOUR_EDIT);
	HWND hwndMinEdit = GetDlgItem(hDlg,IDC_TIME_MIN_EDIT);

	timeInputStatus = 0;
	tmSet.tm_hour = 0;
	tmSet.tm_min = 0;
	tmSet.tm_sec = 0;

	dataLen = GetWindowTextLength(hwndHourEdit);
	if(dataLen == 2){
		GetWindowText(hwndHourEdit,dataBuf,dataLen);
		ret = isInputTimeValid(dataBuf,1);
		if(ret < 0){
			SetWindowText(hwndHourEdit,"");
		} else {
			timeInputStatus |= 0x01;
			tmSet.tm_hour = ret;
		}
	}else if(dataLen > 2){  /* Overflow */
		SetWindowText(hwndHourEdit,"");
	}

	dataLen = GetWindowTextLength(hwndMinEdit);
	if(dataLen == 2){
		GetWindowText(hwndMinEdit,dataBuf,dataLen);
		ret = isInputTimeValid(dataBuf,1);
		if(ret < 0){
			SetWindowText(hwndMinEdit,"");
		} else {
			timeInputStatus |= 0x02;
			tmSet.tm_min = ret;
		}
	}else if(dataLen > 2){
		SetWindowText(hwndMinEdit,"");
	}

	return 0;
}

static void initSetSysTimeDlg(HWND hDlg)
{
	int lang = *curSysLang;
	char strBuf[16];

	int len = strlen(strConfigItems[lang][currentConfig]);

	if(len > 15)
		len = 15;

	strncpy(strBuf, strConfigItems[lang][currentConfig], len);
	strBuf[len] = 0;

	SetDlgItemText(hDlg, IDC_TIME_DESC, strBuf);

	/* : */
	SetDlgItemText(hDlg, IDC_TIME_SPLIT_HM, ":");

	/* Hour : Min */
	SetDlgItemText(hDlg, IDC_TIME_FORMAT, "(H : M)");

	SetDlgItemText(hDlg, IDC_CFG_ITEM_STATUS, strSysTimeHelp[lang]);
	/* Also change its bkcolor */
}

static int setSysTimeDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndHourEdit = GetDlgItem(hDlg,IDC_TIME_HOUR_EDIT);
	HWND hwndMinEdit = GetDlgItem(hDlg,IDC_TIME_MIN_EDIT);
	struct rtc_tm tm;
	char dispBuf[6];

	switch (message)
	{
    	case MSG_INITDIALOG:
		SetNotificationCallback(hwndHourEdit, setSysHourEditCallback);
		SetNotificationCallback(hwndMinEdit, setSysMinuteEditCallback);

		timeInputStatus = 0;

#ifdef CONFIG_DEBUG_ON_PC
		tm.tm_hour = 23;
		tm.tm_min = 30;
#else
		getSysRtcTime(&tm);
#endif
		sprintf(dispBuf,"%02d",tm.tm_hour);
		SetWindowText(hwndHourEdit,dispBuf);
		SendMessage(hwndHourEdit, EM_SETCARETPOS, 0, 2);
		sprintf(dispBuf,"%02d",tm.tm_min);
		SetWindowText(hwndMinEdit,dispBuf);
		SendMessage(hwndMinEdit, EM_SETCARETPOS, 0, 2);

		CreateWindow (CTRL_BUTTON,
			      " F1 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F1,
			      40, 120, 95, 42, hDlg, (DWORD)(&btBmpCfgFn[0]));

		CreateWindow (CTRL_BUTTON,
			      " F2 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F2,
			      190, 120, 95, 42, hDlg, (DWORD)(&btBmpCfgFn[1]));

		initSetSysTimeDlg(hDlg);

		SetFocusChild(hwndHourEdit);
		return(1);

    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}
		initSetSysTimeBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_CURSORBLOCKUP:
			SetFocusChild(hwndHourEdit);
			break;

		case SCANCODE_CURSORBLOCKDOWN:
			SetFocusChild(hwndMinEdit);
			break;

		case SCANCODE_F2:  /* cancel and edit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_F1: /* done  */
		case SCANCODE_ENTER:
			getTimeEditData(hDlg);
			if(timeInputStatus != 3 ){
				SetFocusChild(hwndHourEdit);
				break;
			}
			if(!isDateTimeValid(&tmSet,RTC_SETTIME)){
				int ret = setSysRtcDateTime(&tmSet,RTC_SETTIME);
				if(!ret){
					SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
				}
			}
			break;
		case SCANCODE_CANCEL:
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_HELP:
			MessageBox(hDlg,config_help[*curSysLang].help_time,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		}
		break;
    	case MSG_COMMAND:
		switch(wParam){
		case IDC_CFG_BUTTON_F1:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_CFG_BUTTON_F2:  /* Exit, call EndDialog() directly */
			EndDialog(hDlg,IDOK);
			break;
	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;


	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setSysTimeDialogBox (HWND hwnd)
{
	setSysTimeDlgBox.controls = setSysTimeCtrlsInit;
	DialogBoxIndirectParam (&setSysTimeDlgBox, hwnd,setSysTimeDialogBoxProc, 0L);
}

static int setSysTime (HWND hwnd)
{
	setSysTimeDialogBox(hwnd);
	return 0;
}

/***********************************************************/

static DLGTEMPLATE setSysLangDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	CFG_ITEM_WIN_X, CFG_ITEM_WIN_Y, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H,
	"setSysLang",
	0, 0,
	2, NULL,
	0
};

static CTRLDATA setSysLangCtrlsInit[] =
{
	{       /* description */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		30, 24, 100, 30,
		IDC_LANG_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{       /* operation help */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		10, 175, 320, 24,
		IDC_CFG_ITEM_STATUS,
		"",
		0,
		WS_EX_TRANSPARENT
	}
};

static char strSysLangHelp[SYSLANG_NO][40] =
{
	{"使用↑和↓选择系统语言"},
	{"Use ↑ and ↓to choose language"}
};

static int current_sel_lang;  /* */

static void initSetSysLangBkgnd(HDC hdc)
{
	FillBoxWithBitmap(hdc, 0, 0, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H, &configureListBkgnd);
}

static void initSetSysLangDlg(HWND hDlg)
{
	int lang = *curSysLang;
	char strBuf[16];

	int len = strlen(strConfigItems[lang][currentConfig]);

	if(len > 15)
		len = 15;

	strncpy(strBuf, strConfigItems[lang][currentConfig], len);
	strBuf[len] = 0;
	SetDlgItemText(hDlg, IDC_LANG_DESC, strBuf);

	SetDlgItemText(hDlg, IDC_CFG_ITEM_STATUS, strSysLangHelp[lang]);
}

static void setSysLangButtonCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	if(nc == BN_CLICKED)
		current_sel_lang = id - IDC_LANG_BT_0;
}

static int setSysLangDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndRadioButton[2];

	switch (message)
	{
    	case MSG_INITDIALOG:

		CreateWindow (CTRL_BUTTON,
			      " F1 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F1,
			      40, 120, 95, 42, hDlg, (DWORD)(&btBmpCfgFn[0]));

		CreateWindow (CTRL_BUTTON,
			      " F2 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F2,
			      190, 120, 95, 42, hDlg, (DWORD)(&btBmpCfgFn[1]));

		/* Single select button */
		hwndRadioButton[0]= CreateWindow(CTRL_BUTTON,
						 "简体中文",
						 WS_CHILD | BS_AUTORADIOBUTTON | WS_VISIBLE | WS_GROUP,
						 IDC_LANG_BT_0,
						 140, 20, 100, 30, hDlg, 0);

		hwndRadioButton[1]= CreateWindow(CTRL_BUTTON,
						 "English",
						 WS_CHILD | BS_AUTORADIOBUTTON | WS_VISIBLE,
						 IDC_LANG_BT_1,
						 140, 60, 100, 30, hDlg, 0);

		SetNotificationCallback(hwndRadioButton[0], setSysLangButtonCallback);
		SetNotificationCallback(hwndRadioButton[1], setSysLangButtonCallback);

		current_sel_lang = *curSysLang;

		if(current_sel_lang < SYSLANG_NO)
			SendMessage(hwndRadioButton[current_sel_lang], BM_SETCHECK, BST_CHECKED, 0);

		initSetSysLangDlg(hDlg);
		SetFocusChild(hwndRadioButton[current_sel_lang]);

		return(1);

    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initSetSysLangBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_CURSORBLOCKUP:
		case SCANCODE_CURSORBLOCKLEFT:
			break;

		case SCANCODE_CURSORBLOCKDOWN:
		case SCANCODE_CURSORBLOCKRIGHT:
			break;

		case SCANCODE_F2:  /* cancel and edit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_F1:  /* done  */
		case SCANCODE_ENTER:
			if(currentConfig == CFG_ITEM_LANG){
#if 0
				printf("lang orig=%d now=%d\n", *curSysLang, current_sel_lang);
#endif
				if(*curSysLang != current_sel_lang){
					/* exchange to keep them not the same */
					int orig_lang = *curSysLang;
					*curSysLang = current_sel_lang;
					current_sel_lang = orig_lang;
					saveUserConfigFile(0);
				}
			}
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);

			break;
		case SCANCODE_CANCEL:
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_HELP:
			MessageBox(hDlg,config_help[*curSysLang].help_lang,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		}
		break;
    	case MSG_COMMAND:
		switch(wParam){
		case IDC_CFG_BUTTON_F1:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_CFG_BUTTON_F2:  /* Exit, call EndDialog() directly */
			EndDialog(hDlg,IDOK);
			break;
	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;


	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setSysLangDialogBox (HWND hwnd)
{
	setSysLangDlgBox.controls = setSysLangCtrlsInit;
	DialogBoxIndirectParam (&setSysLangDlgBox, hwnd,setSysLangDialogBoxProc, 0L);
}
static int setSysLang (HWND hwnd)
{
	//printf("setSysLanguage().\n");
	setSysLangDialogBox(hwnd);		// Should not return except receive CLOSE MSG
	return 0;

}

/***********************************************************/

static DLGTEMPLATE setSysSpeakerDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	CFG_ITEM_WIN_X, CFG_ITEM_WIN_Y, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H,
	"setSysSpeaker",
	0, 0,
	2, NULL,
	0
};

static CTRLDATA setSysSpeakerCtrlsInit[] =
{
	{       /* description */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		30, 24, 100, 30,
		IDC_SPEAKER_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{       /* operation help */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		10, 175, 320, 24,
		IDC_CFG_ITEM_STATUS,
		"",
		0,
		WS_EX_TRANSPARENT
	}
};

static char strSysSpeakerHelp[SYSLANG_NO][40] =
{
	{"使用↑和↓选择开关"},
	{"Use ↑ and ↓to turn on/off"}
};

static int current_sel_speaker = 0;

static void initSetSysSpeakerBkgnd(HDC hdc)
{
	FillBoxWithBitmap(hdc, 0, 0, CFG_ITEM_WIN_W, CFG_ITEM_WIN_H, &configureListBkgnd);
}

static void initSetSysSpeakerDlg(HWND hDlg)
{
	int lang = *curSysLang;
	char strBuf[16];

	int len = strlen(strConfigItems[lang][currentConfig]);

	if(len > 15)
		len = 15;

	strncpy(strBuf, strConfigItems[lang][currentConfig], len);
	strBuf[len] = 0;
	SetDlgItemText(hDlg, IDC_LANG_DESC, strBuf);

	SetDlgItemText(hDlg, IDC_CFG_ITEM_STATUS, strSysSpeakerHelp[lang]);
}

static void setSysSpeakerButtonCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
}

static int setSysSpeakerDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndRadioButton[2];

	switch (message)
	{
    	case MSG_INITDIALOG:

		CreateWindow (CTRL_BUTTON,
			      " F1 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F1,
			      40, 120, 95, 42, hDlg, (DWORD)(&btBmpCfgFn[0]));

		CreateWindow (CTRL_BUTTON,
			      " F2 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_CFG_BUTTON_F2,
			      190, 120, 95, 42, hDlg, (DWORD)(&btBmpCfgFn[1]));

		/* Single select button */
		hwndRadioButton[0]= CreateWindow(CTRL_BUTTON,
						 "ON",
						 WS_CHILD | BS_AUTORADIOBUTTON | WS_VISIBLE | WS_GROUP,
						 IDC_SPEAKER_BT_0,
						 140, 20, 100, 30, hDlg, 0);

		hwndRadioButton[1]= CreateWindow(CTRL_BUTTON,
						 "OFF",
						 WS_CHILD | BS_AUTORADIOBUTTON | WS_VISIBLE,
						 IDC_SPEAKER_BT_1,
						 140, 60, 100, 30, hDlg, 0);

		SetNotificationCallback(hwndRadioButton[0], setSysSpeakerButtonCallback);
		SetNotificationCallback(hwndRadioButton[1], setSysSpeakerButtonCallback);


		SendMessage(hwndRadioButton[current_sel_speaker], BM_SETCHECK, BST_CHECKED, 0);

		initSetSysSpeakerDlg(hDlg);
		SetFocusChild(hwndRadioButton[current_sel_speaker]);

		return(1);

    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initSetSysSpeakerBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_CURSORBLOCKUP:
		case SCANCODE_CURSORBLOCKLEFT:
			break;

		case SCANCODE_CURSORBLOCKDOWN:
		case SCANCODE_CURSORBLOCKRIGHT:
			break;

		case SCANCODE_F2:  /* cancel and edit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_F1:  /* done  */
		case SCANCODE_ENTER:
			if(currentConfig == CFG_ITEM_SPEAKER){

			}
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);

			break;
		case SCANCODE_CANCEL:
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		}
		break;
    	case MSG_COMMAND:
		switch(wParam){
		case IDC_CFG_BUTTON_F1:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_CFG_BUTTON_F2:  /* Exit, call EndDialog() directly */
			EndDialog(hDlg,IDOK);
			break;
	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;


	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setSysSpeakerDialogBox (HWND hwnd)
{
	setSysSpeakerDlgBox.controls = setSysSpeakerCtrlsInit;
	DialogBoxIndirectParam (&setSysSpeakerDlgBox, hwnd,setSysSpeakerDialogBoxProc, 0L);
}

static int setSysSpeaker (HWND hwnd)
{
	//printf("setSysSpeakeruage().\n");
	setSysSpeakerDialogBox(hwnd);		// Should not return except receive CLOSE MSG
	return 0;
}



/***********************************************************************************/
static DLGTEMPLATE machConfigDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	LISTBOX_X , LISTBOX_Y, CONFIG_WIN_W, CONFIG_WIN_H,
	"configure",
	0, 0,
	3, NULL,
	0
};

static CTRLDATA machConfigCtrlsInit[] =
{
	{
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		160, 10, 100, 30,
		IDC_LIMIT_FRONT_DESC,
		"value",
		0
	},
	{
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY ,
		160, 45, 100, 30,
		IDC_LIMIT_MIDDLE_DESC,
		"value",
		0
	},
	{
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		160, 80, 100, 30,
		IDC_LIMIT_BACK_DESC,
		"value",
		0
	}
};

static void setConfigItemsName(HWND hDlg)
{
	int i;
	HWND hwndTemp;
	int lang = *curSysLang;

	for(i = 0; i < MACH_CONFIG_ITEM; i++){
		hwndTemp = GetDlgItem(hDlg, IDC_BT_START + i);

		SendMessage(hwndTemp, MSG_SETTEXT, 0, (LPARAM)strConfigItems[lang][i]);
	}
}

static int loadMachConfigBkgndFiles(void)
{
	if (LoadBitmap (HDC_SCREEN, &configureBkgnd, "./res/configItemBkgnd.jpg"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &configureListBkgnd, "./res/configItemBkgnd.jpg"))
		return 1;

	/* F1 and F2 */
	if (LoadBitmap (HDC_SCREEN, &btBmpCfgFn[0], "./res/buttonLableF1.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpCfgFn[1], "./res/buttonLableF2.bmp"))
		return 1;

	return 0;
}

static void unloadMachConfigBkgndFiles(void)
{
	UnloadBitmap(&configureBkgnd);
	UnloadBitmap(&configureListBkgnd);
	UnloadBitmap(&btBmpCfgFn[0]);
	UnloadBitmap(&btBmpCfgFn[1]);
}

static void refreshConfigItem(HWND hDlg,int sel)
{
	HWND  hwndItem;
	int data;

	switch(sel){
	case CFG_ITEM_F_LIMIT:
		data = currentuser->frontlimit;
		hwndItem = GetDlgItem(hDlg,IDC_LIMIT_FRONT_DESC);
		break;
	case CFG_ITEM_M_LIMIT:
		data = currentuser->middlelimit;
		hwndItem = GetDlgItem(hDlg,IDC_LIMIT_MIDDLE_DESC);
		break;
	case CFG_ITEM_B_LIMIT:
		data = currentuser->backlimit;
		hwndItem = GetDlgItem(hDlg,IDC_LIMIT_BACK_DESC);
		break;
	default:
		return;

	}
	refreshCurrentPos(hwndItem,data);

}

static void enterMachConfigItem(HWND hwnd, int sel)
{
#if 0
	printf("select: %d\n", sel);
#endif
	switch(sel){
	case CFG_ITEM_F_LIMIT:  /* Front Limit */
	case CFG_ITEM_M_LIMIT:  /* Middle Limit */
	case CFG_ITEM_B_LIMIT:  /* Back Limit */
		setPosLimit(hwnd);
		refreshConfigItem(hwnd,sel);
		break;
	case CFG_ITEM_DATE:
		setSysDate(hwnd);
		break;
	case CFG_ITEM_TIME:
		setSysTime(hwnd);
		break;
	case CFG_ITEM_LANG:
		setSysLang(hwnd);
		if(current_sel_lang != *curSysLang)
			setConfigItemsName(hwnd);
		break;
	case CFG_ITEM_SPEAKER:
		setSysSpeaker(hwnd);
		break;

	default:
		SendMessage(hwnd,MSG_COMMAND,IDCANCEL,0);
		break;
	}

}

/*  refresh config item list
 *
 *  HighLight current select item
 *
 */
static int refreshMachConfigItems(HWND hDlg, int sel)
{
	int i;
	HWND hwndTemp;

#if 0
	printf("sel = %d\n", sel);
#endif
	if((sel >= MACH_CONFIG_ITEM) || (sel < 0))
		return (-1);

	for(i = 0; i < MACH_CONFIG_ITEM; i++){
		hwndTemp = GetDlgItem(hDlg, IDC_BT_START + i);

		if(i == sel){
			SetWindowBkColor(hwndTemp, PIXEL_yellow);
			InvalidateRect(hwndTemp, NULL, TRUE);
		} else {
			SetWindowBkColor(hwndTemp, PIXEL_lightgray);
			InvalidateRect(hwndTemp, NULL, TRUE);
		}
	}

	return sel;
}

static void initMachConfigBkgnd(HDC hdc)
{
	FillBoxWithBitmap (hdc, 0, 0, CONFIG_WIN_W, CONFIG_WIN_H, &configureBkgnd);
}

static void initMachConfigDialog(HWND hDlg)
{
	HWND  hwndFrontLimit = GetDlgItem(hDlg,IDC_LIMIT_FRONT_DESC);
	HWND  hwndMiddlelimit = GetDlgItem(hDlg,IDC_LIMIT_MIDDLE_DESC);
	HWND  hwndBackLimit = GetDlgItem(hDlg,IDC_LIMIT_BACK_DESC);

	/* config item names */
	setConfigItemsName(hDlg);

	/* show the current config */
	refreshCurrentPos(hwndFrontLimit,currentuser->frontlimit);
	refreshCurrentPos(hwndMiddlelimit,currentuser->middlelimit);
	refreshCurrentPos(hwndBackLimit,currentuser->backlimit);

	refreshMachConfigItems(hDlg, 0);
}

//*********************************************************//
static int configureDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndFrontLimit = GetDlgItem(hDlg,IDC_LIMIT_FRONT_DESC);
	HWND hwndButton[MACH_CONFIG_ITEM];
	int ret;

	switch (message)
	{
    	case MSG_INITDIALOG:

		currentConfig = 0;

		SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);
		if(isIMEOpenInMotor == 1){
			ShowWindow(myIMEWindow,SW_HIDE);
			isIMEOpenInMotor = 0;
		}

		hwndButton[0] = CreateWindow (CTRL_BUTTON,
 				" 0 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_LIMIT_FRONT,
				50, 10, CFG_BT_W, CFG_BT_H, hDlg, 0);
		hwndButton[1] = CreateWindow (CTRL_BUTTON,
 				" 1 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_LIMIT_MIDDLE,
				50, 45, CFG_BT_W, CFG_BT_H, hDlg, 0);
		hwndButton[2] = CreateWindow (CTRL_BUTTON,
 				" 2 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_LIMIT_BACK,
				50, 80, CFG_BT_W, CFG_BT_H, hDlg, 0);
		hwndButton[3] = CreateWindow (CTRL_BUTTON,
 				" 3 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_DATE,
				50, 115, CFG_BT_W, CFG_BT_H, hDlg, 0);
		hwndButton[4] = CreateWindow (CTRL_BUTTON,
 				" 4 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_TIME,
				50, 150, CFG_BT_W, CFG_BT_H, hDlg, 0);
		hwndButton[5] = CreateWindow (CTRL_BUTTON,
 				" 5 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_LANG,
				50, 185, CFG_BT_W, CFG_BT_H, hDlg, 0);
		hwndButton[6] = CreateWindow (CTRL_BUTTON,
 				" 6 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_SPEAKER,
				50, 220, CFG_BT_W, CFG_BT_H, hDlg, 0);
		hwndButton[7] = CreateWindow (CTRL_BUTTON,
 				" 7 ",
				WS_CHILD | BS_PUSHBUTTON | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				IDC_BT_EXIT,
				400, 220, CFG_BT_W, CFG_BT_H, hDlg, 0);

		initMachConfigDialog(hDlg);

		return(1);

    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initMachConfigBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}

	case MSG_CHAR:
		ret = LOWORD(wParam);
#if 0
		printf("Key = %x \n",ret);
#endif
		if((ret >= 0x30) &&(ret < 0x3a)){  /* 0 - 9 */
			ret -= 0x30;

			if((ret >=0) && (ret <= MACH_CONFIG_ITEM - 1)){
				currentConfig = ret;
				refreshMachConfigItems(hDlg, currentConfig);
				enterMachConfigItem(hDlg, currentConfig);
			}
		}
		break;

    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_ENTER:
			enterMachConfigItem(hDlg, currentConfig);
			break;

		case SCANCODE_CURSORBLOCKUP:
			if(currentConfig > 0){
				currentConfig--;
			}else{
				currentConfig = 0;
			}
			refreshMachConfigItems(hDlg, currentConfig);
			break;
		case SCANCODE_CURSORBLOCKDOWN:
			if(currentConfig < (MACH_CONFIG_ITEM - 1)){
				currentConfig++;
			}else{
				currentConfig = MACH_CONFIG_ITEM - 1;
			}
			refreshMachConfigItems(hDlg, currentConfig);
			break;
		case SCANCODE_CURSORBLOCKLEFT:
			if(currentConfig - 6 > 0){
				currentConfig = 6;
				refreshMachConfigItems(hDlg, currentConfig);
			}
			break;

		case SCANCODE_CURSORBLOCKRIGHT:  /* to EXIT button */
			if(currentConfig < 7){
				currentConfig = 7;
				refreshMachConfigItems(hDlg, currentConfig);
			}
			break;

		case SCANCODE_CANCEL: /* Exit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_HELP:
			MessageBox(hDlg,config_help[*curSysLang].help_config,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		}
		break;

    	case MSG_COMMAND:
		switch(wParam){
		case IDC_BT_LIMIT_FRONT:
		case IDC_BT_LIMIT_MIDDLE:
		case IDC_BT_LIMIT_BACK:
		case IDC_BT_DATE:
		case IDC_BT_TIME:
		case IDC_BT_LANG:
		case IDC_BT_SPEAKER:
			currentConfig = wParam - IDC_BT_START;
			if((currentConfig < 0) || currentConfig > (MACH_CONFIG_ITEM - 1))
				break;

			/* change focus to other window */
			SetFocusChild(hwndFrontLimit);

			refreshMachConfigItems(hDlg, currentConfig);
			enterMachConfigItem(hDlg, currentConfig);
			break;

		case IDC_BT_EXIT:
	    	case IDOK:
	    	case IDCANCEL:
			KillTimer(hDlg,IDC_TIMER);
			EndDialog(hDlg,IDOK);
			break;
		}
		break;

	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void configureDialogBox (HWND hwnd)
{
	machConfigDlgBox.controls = machConfigCtrlsInit;
	DialogBoxIndirectParam (&machConfigDlgBox, hwnd,configureDialogBoxProc, 0L);
}

int enterMachineConfig (HWND hwnd)
{
	loadMachConfigBkgndFiles();

	printf("Enter System Configure...\n");
	configureDialogBox(hwnd);		// Should not return except receive CLOSE MSG

	unloadMachConfigBkgndFiles();

	return 0;
}
