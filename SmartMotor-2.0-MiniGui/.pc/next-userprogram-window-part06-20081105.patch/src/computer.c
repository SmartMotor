/*
 * src/computer.c
 *
 * The common bmp functions and serial communication thread
 * implementation
 *
 * Copyright 2005-2008 @ xianweizeng@gmail.com
 *
 * Author:
 *   Zeng Xianwei
 *
 *  File Coding system: gb2312
 *    set-buffer-file-coding-system
 *
 * ChangeLog:
 *	V0.1	  Zeng Xianwei @ 2005-12-15
 *   2008-09-24   Change code indent
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <minigui/common.h>
#include <minigui/minigui.h>
#include <minigui/gdi.h>
#include <minigui/window.h>
#include <minigui/control.h>

#include "computer.h"
#include "bkgndEdit.h"
#include "motor.h"
#include "motorregister.h"
#include "help.h"

/* define globle variables */
tagComputer computerData;

static BITMAP computerBkgnd;
static BITMAP buttonsNums;     /* Enter and Cancel */

#define ComputerStartX	24
#define ComputerStartY	50

struct computer_help_t{
	unsigned char * caption_help;
	unsigned char * caption_waring;
	unsigned char * help_computer;
};

static struct computer_help_t computer_help[SYSLANG_NO] =
{
	{	caption_help_cn,
		caption_waring_cn,
		help_computer_cn
	},

	{	caption_help_en,
		caption_waring_en,
		help_computer_en,
	}
};

/* the position order in BMP file */
static int numBmpsList[20] = {1,2,3,10,4,5,6,11,7,8,9,12,16,0,14,13,0,0,15,17};
/*
 *	1  2  3  /
 *   	4  5  6  *
 *	7  8  9  -
 *     off  0  .  +
 *           ce  =
 */

/*
 * Scancode to BMP table
 *
 * if scancodeToBmp[i] == SCANCODE then position = i
 */
static int scancodeToBmp[20] = {0x31,0x32,0x33,SCANCODE_ADD,
				0x34,0x35,0x36,SCANCODE_SUB,
				0x37,0x38,0x39,SCANCODE_MUL,
				SCANCODE_CANCEL,0x30,0x2e,SCANCODE_DIV,
				0x30,0x30,SCANCODE_F1,SCANCODE_MYEQUAL};

static tagButtonBmp computerButtons;

/* Resources */
static DLGTEMPLATE ComputerDlgInitProgress =
{
	WS_BORDER ,
	WS_EX_NONE,
	200, 80, 240, 320,
	"Computer",
	0, 0,
	1, NULL,
	0
};


static CTRLDATA CtrlInitProgress [] =
{
	{
		CTRL_SLEDIT,
		WS_VISIBLE |WS_BORDER,
		5, 2, 220, 28,
		IDC_COMPUTERDATA,
		NULL,
		0
	}
};

/* get button position in BMP file by scancode */
static int getButtonPos(tagButtonBmp *button,int scancode)
{
	int x,y,index = -1;
	int xBase = ComputerStartX;
	int yBase = ComputerStartY;
	int i=0;

	do{
		if(scancodeToBmp[i] == scancode){
			index = numBmpsList[i];     /* found */
			break;
		}
		i++;
	}while(i<20);

	if(index < 0){    /* NOt in list? */
		return index;
	}

	/* count out the position in window */
	x = xBase + (i%4)*48;
	y = yBase + (i/4)*48;

	button->x = x;
	button->y = y;
	button->index = index;

	return index;
}

static void initComputerBkgnd(HDC hdc)
{
	int i,j;
	int xBase = ComputerStartX;
	int yBase = ComputerStartY;
	int x,y,index;

	FillBoxWithBitmap (hdc, 0, 0, 0, 0, &computerBkgnd); /* Background */

	for(i=0;i<5;i++){
		for(j=0;j<4;j++){
			if((i==4) && ((j == 0) || (j == 1))){
				continue;
			}
			x = xBase + j*48;
			y = yBase + i*48;
			index = numBmpsList[i*4+j];	/* get posision */
			RefreshButtonArea (hdc,&buttonsNums,MSG_KEYUP,x,y,38,38, index);
		}
	}

}

static void resetConputer(HWND hDlg)
{
	char *pBuf = computerData.inputBuf;
    	computerData.a = 0;
 	computerData.b = 0;
	computerData.result = 0;
	computerData.method = 0;
	computerData.status = 0;
	computerData.isInputChanged = 0;
	computerData.isDataFloat = 0;
	memset(pBuf,0,16);
	sprintf(pBuf,"%d",computerData.result);

	/* display the result in Editbox */
	SetDlgItemText(hDlg,IDC_COMPUTERDATA,pBuf);
}

/* compute and display the result*/
static int doCompute(HWND hWnd,tagComputer *pData)
{
	int method = pData->method;
	int result = 0;
	double fResult;
	char *pBuf = pData->inputBuf;

	switch(method){
	case 1:		/* Add */
		result = pData->a + pData->b;
		break;
	case 2:		/* Sub */
		result = pData->a - pData->b;
		break;
	case 3:		/* Mul */
		result = (int)pData->a * pData->b;
		result = result / 100;
		break;
	case 4:		/* Div */
		fResult = (float)pData->a / (float)pData->b;
		/* Actually, we handle all the float number using int, NOT float */
		result = (int)(fResult * 100);
		if(result%100){
			/* set the float flag */
			pData->isDataFloat = 1;
		}
		break;
	}
	pData->result = result;
	/* printf("a=%d b=%d result=%d \n",pData->a,pData->b,result); */
	memset(pBuf,0,16);
	if(pData->isDataFloat){
		sprintf(pBuf,"%.2f",(float)result/100);
	}else {
		sprintf(pBuf,"%d",result/100);
	}
	SetWindowText(hWnd,pBuf); /* display */
	return result;

}

/* Check the data */
static int isInputDatavalid(tagComputer *pData)
{
	int ret;
	double input;
	char *pBuf = pData->inputBuf;
	input = atof(pBuf);
	ret = (int)(input * 100);
	if(ret%100) {
		/* data is float, this flag just affect display */
		pData->isLastDataFloat = 1;
	} else {
		pData->isLastDataFloat = 0;
	}

	return ret;

}

/* Display the input data */
static void dispInputData(HWND hWnd,int data,tagComputer *pData)
{
	int isFloat = pData->isDataFloat;
	float fData;
	char *pBuf = pData->inputBuf;
	fData = (float)data / 100.0;
	memset(pBuf,0,16);
	if(isFloat){
		sprintf(pBuf,"%.2f",fData);
	} else {
		sprintf(pBuf,"%d",data/100);
	}
	SetWindowText(hWnd,pBuf);

}

/*   我们不能在编辑框的EN_ENTER消息中响应执行这些功能
 */
static int conputeKeyPressed(HWND hwnd,tagComputer *pData)
{
	int  inputDataLen=0;
	int ret;
	char *pBuf = pData->inputBuf;
	pData->isLastDataFloat = 0;
	int status = pData->status;
	computerData.isInputChanged = 0;

	inputDataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	memset(pBuf,0,16);
	GetWindowText(hwnd,pBuf,inputDataLen);
	ret = isInputDatavalid(pData);
	memset(pBuf,0,16);
	switch(status){
	case 0:
		pData->isDataFloat = pData->isLastDataFloat;	// 设置小数运算标志位
		computerData.a = ret;
		//printf("a= %d \n",ret);
		dispInputData(hwnd,ret,pData);

		break;
	case 1:
		if(!pData->isDataFloat && pData->isLastDataFloat){	// 只要a和b有一个是小数,则进行小数运算
			pData->isDataFloat = 1;			// 设置小数运算标志位
		}
		computerData.b = ret;
		//printf("b= %d \n",ret);
		dispInputData(hwnd,ret,pData);
		pData->status = 0;			// 可以进行下一次计算
		break;
	}

	return 0;
}

/*	程序名输入编辑框回调函数
 */
static void editComputerCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
	switch(nc){
	case EN_SETFOCUS:	// 编辑框得到焦点
		// 切换输入法到数字输入状态
		SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);
		if(isIMEOpenInMotor == 1){
			ShowWindow(myIMEWindow,SW_HIDE);
			isIMEOpenInMotor = 0;
		}
		break;
	case EN_CHANGE:		// 编辑框内容改变

		break;
	case EN_ENTER:		// Enter key 按下,设置a或者b

		break;
	}

}


static int InitDialogBoxProc (HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	int ret;
	int changed = computerData.isInputChanged;
	HWND  hwndComputerData = GetDlgItem(hDlg,IDC_COMPUTERDATA);

	switch (message) {
	case MSG_INITDIALOG:

		SetNotificationCallback(hwndComputerData, editComputerCallback);
		resetConputer(hDlg);
		SetFocusChild(hwndComputerData);	// Must be here
		return 1;
	case MSG_ERASEBKGND:
	{
		HDC hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}
		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}
		initComputerBkgnd(hdc);  // 初始化背景

		if (fGetDC)
			ReleaseDC (hdc);

		//SetFocusChild(hwndComputerData);

		return 0;
	}
	case MSG_CHAR:
		hdc=BeginPaint(hDlg);
		int key = LOWORD(wParam);
		//printf("0x%x \n",key);
		if((key > 0x2b) &&(key < 0x3e) ){	// 如果是数字键或者小数点安下
			ret = getButtonPos(&computerButtons,LOWORD(wParam));
			if( ret >= 0){
				RefreshButtonArea (hdc,&buttonsNums,MSG_KEYDOWN,computerButtons.x,computerButtons.y,
						   38,38, computerButtons.index);
				usleep(100000);	// 150ms
				RefreshButtonArea (hdc,&buttonsNums,MSG_KEYUP,computerButtons.x,computerButtons.y,
						   38,38, computerButtons.index);
			}
			if(changed == 0){	// 每次内容改变时清空输入框
				memset(computerData.inputBuf,0,16);
				SetWindowText(hwndComputerData,"");
				computerData.isInputChanged = 1;
			}
		}
		EndPaint(hDlg,hdc);
		break;

	case MSG_KEYDOWN:
		hdc=BeginPaint(hDlg);
		ret = getButtonPos(&computerButtons,LOWORD(wParam));
		if( ret > 0){
			RefreshButtonArea (hdc,&buttonsNums,MSG_KEYDOWN,computerButtons.x,computerButtons.y,
					   38,38, computerButtons.index);
		}
		EndPaint(hDlg,hdc);
		break;
	case MSG_KEYUP:
		hdc=BeginPaint(hDlg);
		switch(LOWORD(wParam)){
		case SCANCODE_ADD:	// +
			conputeKeyPressed(hwndComputerData,&computerData);
			computerData.method = 1;	//设置运算方法
			computerData.status = 1;	//设置状态
			computerData.isInputChanged = 0;
			break;
		case SCANCODE_SUB:	// -
			conputeKeyPressed(hwndComputerData,&computerData);
			computerData.method = 2;	//设置运算方法
			computerData.status = 1;
			computerData.isInputChanged = 0;
			break;

		case SCANCODE_MUL:	// *
			conputeKeyPressed(hwndComputerData,&computerData);
			computerData.method = 3;	//设置运算方法
			computerData.status = 1;
			computerData.isInputChanged = 0;
			break;

		case SCANCODE_DIV: 	// /
			conputeKeyPressed(hwndComputerData,&computerData);
			computerData.method = 4;	//设置运算方法
			computerData.status = 1;
			computerData.isInputChanged = 0;
			break;

		case SCANCODE_MYEQUAL:	// =
			conputeKeyPressed(hwndComputerData,&computerData);
			computerData.status = 0;
			computerData.isInputChanged = 0;
			// 计算结果并显示
			doCompute(hwndComputerData,&computerData);

			break;
		case SCANCODE_F1:	// CE
			conputeKeyPressed(hwndComputerData,&computerData);
			resetConputer(hDlg);
			break;

		case SCANCODE_CANCEL:	//Exit
			// 退出我的程序,如果用户只输入了操作数A,也要将A复制到程序画面的输入框(其实没有必要)
			ret = GetWindowTextLength(hwndComputerData);	// 获得输入的字符
			memset(computerData.inputBuf,0,16);
			GetWindowText(hwndComputerData,computerData.inputBuf,ret);

			// 发送退出消息
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

		case SCANCODE_HELP:	// Help
			MessageBox(hDlg,computer_help[*curSysLang].help_computer,
				   computer_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;

		}
		ret = getButtonPos(&computerButtons,LOWORD(wParam));
		if( ret >= 0){
			RefreshButtonArea (hdc,&buttonsNums,MSG_KEYUP,computerButtons.x,computerButtons.y,
					   38,38, computerButtons.index);
		}
		EndPaint(hDlg,hdc);

		break;
	case MSG_COMMAND:
		switch(wParam){
		case IDOK:
		case IDCANCEL:
			EndDialog(hDlg,IDCANCEL);
			break;
		}
		break;

	}


	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void InitDialogBox (HWND hWnd)
{
	ComputerDlgInitProgress.controls = CtrlInitProgress;

	DialogBoxIndirectParam (&ComputerDlgInitProgress, hWnd, InitDialogBoxProc, 0L);
}


int EnterComputer(HWND hWnd)
{
	// Load background and other pics
	if (LoadBitmap (HDC_SCREEN, &computerBkgnd, "./res/computerBkgnd.jpg"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &buttonsNums, "./res/buttonsNums.jpg"))
		return 1;

	printf("Enter Computer...\n");
	InitDialogBox (HWND_DESKTOP);		// Should not return except receive CLOSE MSG

	UnloadBitmap (&computerBkgnd);	// unload background
	UnloadBitmap (&buttonsNums);	// unload background
	return 0;
}
