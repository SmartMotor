/*
 * src/program_edit.c
 *
 * The work window implementation
 *
 * Copyright 2007-2008 @ xianweizeng@gmail.com
 *
 * Author:
 *   Zeng Xianwei
 *
 *  File Coding system: gb2312
 *    set-buffer-file-coding-system
 *
 * ChangeLog:
 *   2008-09-21 split from userprogram.c
 *
 */


/********************************************************************/
#define IDC_LABLETOTALLEN	(IDC_LABLE_WINDOW_START)
#define IDC_LABLELEN		(IDC_LABLE_WINDOW_START + 1)
#define IDC_UNUSEDLEN		(IDC_LABLE_WINDOW_START + 2)
#define IDC_HALFTOTALLEN	(IDC_LABLE_WINDOW_START + 3)
#define IDC_HALFNUMBER		(IDC_LABLE_WINDOW_START + 4)

/* desriptiones */
#define IDC_TOTAL_DESC		(IDC_LABLE_WINDOW_START + 5)
#define IDC_LABLELEN_DESC	(IDC_LABLE_WINDOW_START + 6)
#define IDC_UNUSED_DESC		(IDC_LABLE_WINDOW_START + 7)
#define IDC_AVER_DESC		(IDC_LABLE_WINDOW_START + 8)

#define IDC_BUTTON_LABLE_F1	(IDC_LABLE_WINDOW_START + 9)
#define IDC_BUTTON_LABLE_F2	(IDC_LABLE_WINDOW_START + 10)


static char strInputTotalDesc[] = "输入总长";
static char strInputLableDesc[] = "输入标签";
static char strInputUnusedDesc[] = "输入废边";
static char strInputAverDesc[] = "输入等分";

static DLGTEMPLATE lableEditDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	LISTBOX_X , LISTBOX_Y, LISTBOX_W, LISTBOX_H,
	"lableEdit",
	0, 0,
	6, NULL,
	0
};

static CTRLDATA lableEditCtrlsInit[] =
{
	{       /* input total len */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		30, 46, 100, 30,
		IDC_TOTAL_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE|WS_BORDER,
		130, 40, 120, 30,
		IDC_LABLETOTALLEN,
		"",
		0
	},
	{       /* input lable len */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		30, 96, 100, 30,
		IDC_LABLELEN_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE|WS_BORDER,
		130, 90, 120, 30,
		IDC_LABLELEN,
		"",
		0
	},
	{       /* input unused len */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		30, 146, 100, 30,
		IDC_UNUSED_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE|WS_BORDER,
		130, 140, 120, 30,
		IDC_UNUSEDLEN,
		"",
		0
	}
};

/********************************************************************/
static DLGTEMPLATE averageEditDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	LISTBOX_X , LISTBOX_Y, LISTBOX_W, LISTBOX_H,
	"averageEdit",
	0, 0,
	4, NULL,
	0
};

static CTRLDATA averageEditCtrlsInit[] =
{
	{       /* input total len */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		30, 46, 100, 30,
		IDC_TOTAL_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE|WS_BORDER,
		130, 40, 120, 30,
		IDC_HALFTOTALLEN,
		"",
		0
	},
	{       /* input average number */
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY,
		30, 106, 100, 30,
		IDC_AVER_DESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE|WS_BORDER,
		130, 100, 120, 30,
		IDC_HALFNUMBER,
		"",
		0
	}
};


/*******************************************************************************************/


/* count the data for lable edit.
 * The results is save to the data buffer, from the current data
 *
 * Be careful: the orignal data behind current one maybe overwriten.
 *
 */
static int CountLableData(userProgramEditData *pProEdit)
{
	int frontLimit = getValidLimit();
	int totalLen = pProEdit->lableEdit.totalLen;
	int lableLen = pProEdit->lableEdit.lableLen;
	int unusedLen = pProEdit->lableEdit.unusedLen;

	int currentCut = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum;
	int cut = totalLen;

#if 0
	printf("CountLableData...totalCuts=%d curCut=%d\n",totalCuts,currentCut);
#endif

	/* we have checked totalLen when input, so it is validate */
	if((currentCut < totalCuts) && (currentCut < MAX_PROGRAM_CUTS)){
		pProEdit->userProCuts[currentCut++] = cut;
	}else {
		return totalCuts;
	}

	while(cut >= frontLimit){
		cut = cut - lableLen;
		if((cut >= frontLimit) && (currentCut < MAX_PROGRAM_CUTS)){
			pProEdit->userProCuts[currentCut++] = cut;
		} else {
			break;
		}

		cut = cut-unusedLen;
		if((cut >= frontLimit) && (currentCut<MAX_PROGRAM_CUTS)){
			pProEdit->userProCuts[currentCut++] = cut;
		} else {
			break;
		}
	}

	if(currentCut >= totalCuts){
		totalCuts = currentCut+1;
		pProEdit->userProCuts[totalCuts-1] = 0;
		pProEdit->userProCuts[totalCuts] = 0;
	}

	pProEdit->nInputCutsNum = totalCuts;

	return totalCuts;
}

/* count the data for average edit.
 * The results is save to the data buffer, from the current data
 *
 * Be careful: the orignal data behind current one maybe overwriten.
 *
 */
static int CountHalfData(userProgramEditData *pProEdit)
{
	int frontLimit = getValidLimit();
	int totalLen = pProEdit->halfEdit.totalLen;
	int num = pProEdit->halfEdit.num;
	int currentCut = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum;
	int cutBase = totalLen / num ;
	int cut = totalLen;

	if((currentCut < totalCuts) && (currentCut < MAX_PROGRAM_CUTS)){
		pProEdit->userProCuts[currentCut++] = cut;
	} else {
		return totalCuts;
	}

	while(cut >= frontLimit){
		cut = cut-cutBase;
		if((cut >= frontLimit) && (currentCut < MAX_PROGRAM_CUTS)){
			pProEdit->userProCuts[currentCut++] = cut;
		} else {
			break;
		}
	}

	if(currentCut >= totalCuts){
		totalCuts = currentCut+1;
		pProEdit->userProCuts[totalCuts-1] = 0;
		pProEdit->userProCuts[totalCuts] = 0;
	}

	pProEdit->nInputCutsNum = totalCuts;
	return totalCuts;
}


/*
 * Label edit window
 */
static void initLableEditBkgnd(HDC hdc)
{
	FillBoxWithBitmap (hdc, 0, 0, LISTBOX_W, LISTBOX_H, &lableEditBkgnd);
}

static void initLableEditDlg(HWND hwnd)
{
	SetDlgItemText(hwnd,IDC_TOTAL_DESC, strInputTotalDesc);
	SetDlgItemText(hwnd,IDC_LABLELEN_DESC, strInputLableDesc);
	SetDlgItemText(hwnd,IDC_UNUSED_DESC, strInputUnusedDesc);
}

static int getLableEditData(HWND *hwndEdit,userProgramEditData *pProEdit)
{
	int index = 0;
	char cutDataBuf[10];
	int  cutDataLen=0;
	int ret;

	pProEdit->lableEdit.lableStatus = 0;
	pProEdit->lableEdit.totalLen = 0;
	pProEdit->lableEdit.lableLen = 0;
	pProEdit->lableEdit.unusedLen = 0;

	for(index = 0; index < 3; index++){ /* check 3 edit boxes */
		memset(cutDataBuf,0,10);
		cutDataLen = GetWindowTextLength(hwndEdit[index]);
		GetWindowText(hwndEdit[index],cutDataBuf,cutDataLen);
		ret = isInputCutsDataValid(cutDataBuf,cutDataLen);
		if(ret > 0 ){
			switch(index){
			case 0:
				pProEdit->lableEdit.totalLen = ret;
				pProEdit->lableEdit.lableStatus |= 0x1; /* bit 0 */
				break;
			case 1:
				pProEdit->lableEdit.lableLen = ret;
				pProEdit->lableEdit.lableStatus |= 0x2;
				break;
			case 2:
				pProEdit->lableEdit.unusedLen = ret;
				pProEdit->lableEdit.lableStatus |= 0x4;
				break;
			}
		}else{
			SetWindowText(hwndEdit[index],"");
			SetFocusChild(hwndEdit[index]);
			return(-1);
		}
	}

	return(0);
}

static void lableTotalLenEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd,nextHwnd;
	int dataLen,ret;
	char dataBuf[10];
	mainHwnd = GetMainWindowHandle(hwnd);
	nextHwnd = GetDlgItem(mainHwnd,IDC_LABLELEN);

	switch(nc){
	case EN_CHANGE:
		dataLen = GetWindowTextLength(hwnd);
		if(dataLen > 7){
			SetFocusChild(hwnd);
			SetWindowText(hwnd,"");
		}
		break;
	case EN_ENTER:
		dataLen = GetWindowTextLength(hwnd);
		GetWindowText(hwnd,dataBuf,dataLen);
		ret = isInputCutsDataValid(dataBuf,dataLen);
		if(ret > 0 ){
			programEdit.lableEdit.totalLen = ret;
			programEdit.lableEdit.lableStatus |= 0x1;
			if(programEdit.lableEdit.lableStatus == 7) {
				CountLableData(&programEdit);
				SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0);
			}else{
				SetFocusChild(nextHwnd);
			}
		}else {
			SetWindowText(hwnd,"");
		}
		break;
	}
}

static void lableLenEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd,nextHwnd;
	int dataLen,ret;
	char dataBuf[10];
	mainHwnd = GetMainWindowHandle(hwnd);
	nextHwnd = GetDlgItem(mainHwnd,IDC_UNUSEDLEN);

	switch(nc){
	case EN_CHANGE:
		dataLen = GetWindowTextLength(hwnd);
		if(dataLen > 7){
			SetFocusChild(hwnd);
			SetWindowText(hwnd,"");
		}
		break;
	case EN_ENTER:
		dataLen = GetWindowTextLength(hwnd);
		GetWindowText(hwnd,dataBuf,dataLen);
		ret = isInputCutsDataValid(dataBuf,dataLen);
		if(ret > 0 ){
			programEdit.lableEdit.lableLen = ret;
			programEdit.lableEdit.lableStatus |= 0x2;
			if(programEdit.lableEdit.lableStatus == 7){
				CountLableData(&programEdit);
				SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0);
			}else{
				SetFocusChild(nextHwnd);
			}
		}else {
			SetWindowText(hwnd,"");
		}
		break;
	}
}
static void lableUnusedLenEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd,nextHwnd;
	int dataLen,ret;
	char dataBuf[10];
	mainHwnd = GetMainWindowHandle(hwnd);
	nextHwnd = GetDlgItem(mainHwnd,IDC_LABLETOTALLEN);

	switch(nc){
	case EN_CHANGE:
		dataLen = GetWindowTextLength(hwnd);
		if(dataLen > 7){
			SetFocusChild(hwnd);
			SetWindowText(hwnd,"");
		}
		break;
	case EN_ENTER:
		dataLen = GetWindowTextLength(hwnd);
		GetWindowText(hwnd,dataBuf,dataLen);
		ret=isInputCutsDataValid(dataBuf,dataLen);
		if(ret > 0 ){
			programEdit.lableEdit.unusedLen = ret;
			programEdit.lableEdit.lableStatus |= 0x4;
			if(programEdit.lableEdit.lableStatus == 7){
				CountLableData(&programEdit);
				SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0);
			}else{
				SetFocusChild(nextHwnd);
			}
		}else {
			SetWindowText(hwnd,"");
		}
		break;
	}
}

static int enterLableEditDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndEdit[3],hwndTemp;;
	hwndEdit[0] = GetDlgItem(hDlg,IDC_LABLETOTALLEN);
	hwndEdit[1] = GetDlgItem(hDlg,IDC_LABLELEN);
	hwndEdit[2] = GetDlgItem(hDlg,IDC_UNUSEDLEN);

	switch (message) {
	case MSG_INITDIALOG:
		SetNotificationCallback(hwndEdit[0], lableTotalLenEditCallback);
		SetNotificationCallback(hwndEdit[1], lableLenEditCallback);
		SetNotificationCallback(hwndEdit[2], lableUnusedLenEditCallback);

		programEdit.lableEdit.lableStatus = 0;
		programEdit.lableEdit.totalLen = 0;
		programEdit.lableEdit.lableLen = 0;
		programEdit.lableEdit.unusedLen = 0;

		/* create buttons: F1 and F2 */
		CreateWindow (CTRL_BUTTON,
			      " F1 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_BUTTON_LABLE_F1,
			      30, 195, 95, 42, hDlg, (DWORD)(&btBmpLableFn[0]));

		CreateWindow (CTRL_BUTTON,
			      " F2 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_BUTTON_LABLE_F2,
			      190, 195, 95, 42, hDlg, (DWORD)(&btBmpLableFn[1]));

		initLableEditDlg(hDlg);
		SetFocusChild(hwndEdit[0]);

		return(1);

	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initLableEditBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_F1: /* Finish: count data and exit  */
			getLableEditData(hwndEdit,&programEdit);
			if(programEdit.lableEdit.lableStatus == 7){
				CountLableData(&programEdit);
				SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			}
			break;
		case SCANCODE_F2:  /* cancel and edit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;

#if 0  /* SetWindowBkColor() test */
		case SCANCODE_F3:  /* cancel and edit */
			SetWindowBkColor(hwndEdit[0], PIXEL_blue);
			InvalidateRect(hwndEdit[0], NULL, TRUE);
			break;

		case SCANCODE_F4:  /* cancel and edit */
			SetWindowBkColor(hwndEdit[0], PIXEL_red);
			InvalidateRect(hwndEdit[0], NULL, TRUE);
			break;
#endif

		case SCANCODE_CURSORBLOCKDOWN:  /* change input focus */
			hwndTemp = GetFocusChild(hDlg);
			if(hwndTemp == hwndEdit[0]){
				SetFocusChild(hwndEdit[1]);
			} else {
				SetFocusChild(hwndEdit[2]);
			}
			break;
		case SCANCODE_CURSORBLOCKUP:
			hwndTemp = GetFocusChild(hDlg);
			if(hwndTemp == hwndEdit[2]){
				SetFocusChild(hwndEdit[1]);
			} else {
				SetFocusChild(hwndEdit[0]);
			}
			break;

		case SCANCODE_CANCEL: /* exit */
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		}
		break;

	case MSG_COMMAND:
		switch(wParam){
		case IDC_BUTTON_LABLE_F1:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_BUTTON_LABLE_F2:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F2, 0L);
			break;

		case IDOK:
		case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;
	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void enterLableEditDialogBox (HWND hwnd)
{
	lableEditDlgBox.controls = lableEditCtrlsInit;
	DialogBoxIndirectParam (&lableEditDlgBox, hwnd,enterLableEditDialogBoxProc, 0L);
}
static int enterLableEdit (HWND hwnd)
{
	/* printf("enterLableEdit().\n"); */
	enterLableEditDialogBox(hwnd);
	return 0;
}

/*******************************************************************************************/
/*
 * Average edit window
 */
static void initAverageEditBkgnd(HDC hdc)
{
	FillBoxWithBitmap (hdc, 0, 0, LISTBOX_W, LISTBOX_H, &lableEditBkgnd);
}

static void initAverageEditDlg(HWND hwnd)
{
	SetDlgItemText(hwnd,IDC_TOTAL_DESC, strInputTotalDesc);
	SetDlgItemText(hwnd,IDC_AVER_DESC, strInputAverDesc);
}


static int getAverageEditData(HWND *hwndEdit,userProgramEditData *pProEdit)
{
	int index = 0;
	char cutDataBuf[10];
	int  cutDataLen=0;
	int ret;
	pProEdit->halfEdit.halfStatus = 0;
	pProEdit->halfEdit.totalLen = 0;
	pProEdit->halfEdit.num = 0;

	for(index = 0; index < 2; index++){
		memset(cutDataBuf,0,10);
		cutDataLen = GetWindowTextLength(hwndEdit[index]);
		GetWindowText(hwndEdit[index],cutDataBuf,cutDataLen);
		ret=isInputCutsDataValid(cutDataBuf,cutDataLen);
		if(ret > 0 ){
			switch(index){
			case 0:
				pProEdit->halfEdit.totalLen = ret;
				pProEdit->halfEdit.halfStatus |= 0x1;  /* bit 0 */
				break;
			case 1:
				pProEdit->halfEdit.num = ret/100;
				pProEdit->halfEdit.halfStatus |= 0x2;
				break;
			}
		}else{
			SetWindowText(hwndEdit[index],"");
			SetFocusChild(hwndEdit[index]);
			return(-1);
		}
	}

	return(0);
}

static void averageTotalLenEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd,nextHwnd;
	int dataLen,ret;
	char dataBuf[10];
	mainHwnd = GetMainWindowHandle(hwnd);
	nextHwnd = GetDlgItem(mainHwnd,IDC_HALFNUMBER);

	switch(nc){
	case EN_CHANGE: /* 0 - 9 or "." keys is pressed */
		dataLen = GetWindowTextLength(hwnd);
		if(dataLen > 7){
			SetFocusChild(hwnd);
			SetWindowText(hwnd,"");
		}
		break;
	case EN_ENTER:
		dataLen = GetWindowTextLength(hwnd);
		GetWindowText(hwnd,dataBuf,dataLen);
		ret = isInputCutsDataValid(dataBuf,dataLen);
		if(ret > 0 ){
			programEdit.halfEdit.totalLen = ret;
			programEdit.halfEdit.halfStatus |= 0x1; /* bit 0 */
			if(programEdit.halfEdit.halfStatus == 3){
				CountHalfData(&programEdit);
				SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0); //exit
			}else{
				SetFocusChild(nextHwnd);
			}
		}else {
			SetWindowText(hwnd,"");
		}
		break;
	}
}

static void averageNumEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd,nextHwnd;
	int dataLen,ret;
	char dataBuf[10];

	mainHwnd = GetMainWindowHandle(hwnd);
	nextHwnd = GetDlgItem(mainHwnd,IDC_HALFTOTALLEN);

	switch(nc){
	case EN_CHANGE:
		dataLen = GetWindowTextLength(hwnd);
		if(dataLen > 5){
			SetFocusChild(hwnd);
			SetWindowText(hwnd,"");
		}
		break;
	case EN_ENTER:
		dataLen = GetWindowTextLength(hwnd);
		GetWindowText(hwnd,dataBuf,dataLen);
		ret = isInputCutsDataValid(dataBuf,dataLen);
		if((ret > 0) && (ret %100 == 0) ){   /* It must be int */
			programEdit.halfEdit.num = ret/100;
			programEdit.halfEdit.halfStatus |= 0x2; /* bit 1 */
			if(programEdit.halfEdit.halfStatus == 3){
				CountHalfData(&programEdit);
				SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0);
			}else{
				SetFocusChild(nextHwnd);
			}
		}else {
			SetWindowText(hwnd,"");
		}
		break;
	}
}

static int enterAverageEditDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndEdit[2];
	hwndEdit[0] = GetDlgItem(hDlg,IDC_HALFTOTALLEN);
	hwndEdit[1] = GetDlgItem(hDlg,IDC_HALFNUMBER);

	switch (message)
	{
	case MSG_INITDIALOG:
		SetNotificationCallback(hwndEdit[0], averageTotalLenEditCallback);
		SetNotificationCallback(hwndEdit[1], averageNumEditCallback);
		programEdit.halfEdit.halfStatus = 0;
		programEdit.halfEdit.totalLen = 0;
		programEdit.halfEdit.num = 0;

		/* create buttons: F1 and F2 */
		CreateWindow (CTRL_BUTTON,
			      " F1 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_BUTTON_LABLE_F1,
			      30, 195, 95, 42, hDlg, (DWORD)(&btBmpLableFn[0]));

		CreateWindow (CTRL_BUTTON,
			      " F2 ",
			      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
			      IDC_BUTTON_LABLE_F2,
			      190, 195, 95, 42, hDlg, (DWORD)(&btBmpLableFn[1]));

		initAverageEditDlg(hDlg);
		SetFocusChild(hwndEdit[0]);

		return(1);

	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initAverageEditBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_F1: /* Finish */
			getAverageEditData(hwndEdit,&programEdit);
			if(programEdit.halfEdit.halfStatus == 3){
				CountHalfData(&programEdit);
				SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			}
			break;
		case SCANCODE_F2:
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_CURSORBLOCKDOWN:
			SetFocusChild(hwndEdit[1]);
			break;
		case SCANCODE_CURSORBLOCKUP:
			SetFocusChild(hwndEdit[0]);
			break;
		case SCANCODE_CANCEL:
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		}
		break;
	case MSG_COMMAND:
		switch(wParam){
		case IDC_BUTTON_LABLE_F1:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_BUTTON_LABLE_F2:
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F2, 0L);
			break;

		case IDOK:
		case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;
	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void enterAverageEditDialogBox (HWND hwnd)
{
	averageEditDlgBox.controls = averageEditCtrlsInit;
	DialogBoxIndirectParam (&averageEditDlgBox, hwnd,enterAverageEditDialogBoxProc, 0L);
}
static int enterAverageEdit (HWND hwnd)
{
	/* printf("enterAverageEdit().\n"); */
	enterAverageEditDialogBox(hwnd);
	return 0;
}

/*******************************************************************************************/
/* ____________ data edit functions ____________ */

/* check whether the data is legal: inside the limits
 * return 0: validate
 *           invalidate
 */
int isDataOutOfLimits(int data)
{
	int frontLimit = getValidLimit();
	int backlimit = currentuser->backlimit;

	if((data < frontLimit) || (data > currentuser->backlimit))
		return 1;

	return 0;
}


/* Normal Edit: the data is added as the last data in file.
 * It take two step to do this:
 *    1. modify the last 0.0 data to current input one
 *    2. add a new 0.0
 *
 * hwnd: main dialog hwnd
 * return: total number of data after add
 *
 */
static int editOneCutData(HWND hwnd, userProgramEditData *pProEdit, int data)
{
	int frontLimit = getValidLimit();
	int currentSel = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum ;

#if 0
	printf("total=%d current=%d \n",totalCuts,currentSel);
#endif

	if(currentSel != totalCuts-1){
		currentSel = totalCuts-1; /* data is added to the last one */
	}

	/* check data validation */
	if(isDataOutOfLimits(data)){
		MessageBox(hwnd,userpro_help[*curSysLang].msg_cutdata_overflow,
			   userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
		return totalCuts;
	}

	/* input data is OK */
	pProEdit->currentCutsNum = currentSel;
	pProEdit->userProCuts[currentSel] = data;
	UpdateProListWithData(hwnd,pProEdit,0);   /* type 0 means DELETE and then INSERT */

	/* show the 0.0 data */
	currentSel = currentSel + 1;
	totalCuts = totalCuts +1;
	pProEdit->userProCuts[currentSel] = 0;

	pProEdit->nInputCutsNum = totalCuts;
	pProEdit->currentCutsNum = currentSel;
	UpdateProListWithData(hwnd,pProEdit,1);   /* Add to last */

#if 0
	dumpProgramData(pProEdit);
#endif

	return totalCuts;

}

/* compare inpput: Add/sub a input data to the last valid data, the new data
 *                 is added as the last one.
 * delta: input data to be add or sub
 * flag: 0: ADD
 *       1: SUB
 * return: total number of data after add
 *
 */
static int compareAddDecInput(HWND hwnd, userProgramEditData *pProEdit, int delta, int flag)
{
	int data;
	int frontLimit = getValidLimit();
	int currentSel = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum ;

	if(currentSel != totalCuts-1){
		currentSel = totalCuts-1;
	}

	/* (totalCuts - 1) is 0.0 */
	if(!flag){
		data = pProEdit->userProCuts[totalCuts - 2] + delta;
	}else{
		data = pProEdit->userProCuts[totalCuts - 2] - delta;
	}

	if(isDataOutOfLimits(data)) {
		ResetUserProgramEdit(hwnd,pProEdit,0);
		MessageBox(hwnd,userpro_help[*curSysLang].msg_cutdata_overflow,
			   userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
		return totalCuts;
	}

	pProEdit->currentCutsNum = currentSel;
	pProEdit->userProCuts[currentSel] = data;
	UpdateProListWithData(hwnd,pProEdit,0);

	/* Add 0.0 */
	currentSel = currentSel + 1;
	totalCuts = totalCuts +1;
	pProEdit->userProCuts[currentSel] = 0;
	pProEdit->nInputCutsNum = totalCuts;
	pProEdit->currentCutsNum = currentSel;

	UpdateProListWithData(hwnd,pProEdit,1);

	ResetUserProgramEdit(hwnd,pProEdit,0);
	return totalCuts;
}


/* modify the current data
 *   read the current input data and modify to current selected data
 *   in listbox
 */
static int modifyOneCutData(HWND hDlg,userProgramEditData *pProEdit)
{
	HWND hwndInputEdit = GetDlgItem(hDlg,IDC_INPUTCUTDATA);
	int frontLimit = getValidLimit();
	int currentSel = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum;
	int tmp;
	char cutDataBuf[10];
	int  cutDataLen=0;
	int data;

	if(currentSel == totalCuts -1){  /* 0.0 */
		SetWindowText(hwndInputEdit,"");
		return(-1);
	}

	cutDataLen = GetWindowTextLength(hwndInputEdit);
	memset(cutDataBuf,0,10);
	GetWindowText(hwndInputEdit,cutDataBuf,cutDataLen);
	data = isInputCutsDataValid(cutDataBuf,cutDataLen);
	if(data < 0){
		SetWindowText(hwndInputEdit,"");
		return -1;
	}

	if(isDataOutOfLimits(data)) {
		MessageBox(hDlg,userpro_help[*curSysLang].msg_cutdata_overflow,
			   userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
		SetWindowText(hwndInputEdit,"");
		return totalCuts;
	}

	tmp = pProEdit->userProCuts[currentSel] & 0xff000000;  /* Do NOT change the data flags */
	tmp |= data;
	pProEdit->userProCuts[currentSel] = tmp;
	UpdateProListWithData(hDlg,pProEdit,0);

	SetWindowText(hwndInputEdit,"");

	return totalCuts;
}

/*  insert one data to current place
 *
 *  Input the data and then press INSERT key.
 *
 */
static int insertOneCutData(HWND hDlg,userProgramEditData *pProEdit)
{
	HWND hwndInputEdit = GetDlgItem(hDlg,IDC_INPUTCUTDATA);
	int frontLimit = getValidLimit();
	int currentSel = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum;
	char cutDataBuf[10];
	int  cutDataLen=0;
	int data;
	int index;

	if(currentSel == totalCuts -1){  /* the last one */
		SetWindowText(hwndInputEdit,"");
		return(-1);
	}

	cutDataLen = GetWindowTextLength(hwndInputEdit);
	memset(cutDataBuf,0,10);
	GetWindowText(hwndInputEdit,cutDataBuf,cutDataLen);
	data = isInputCutsDataValid(cutDataBuf,cutDataLen);
	if(data < 0){
		SetWindowText(hwndInputEdit,"");
		return -1;
	}

	if(isDataOutOfLimits(data)) {
		MessageBox(hDlg,userpro_help[*curSysLang].msg_cutdata_overflow,
			   userpro_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		SetWindowText(hwndInputEdit,"");
		return totalCuts;
	}

	/* data is validate, so we move all the data behind this one */
	totalCuts = totalCuts + 1;
	for(index = totalCuts-1; index > currentSel; index--){
		pProEdit->userProCuts[index] = pProEdit->userProCuts[index-1];
	}

	pProEdit->nInputCutsNum = totalCuts;
	pProEdit->userProCuts[currentSel] = data;

	/* since the data NO is changed, we have to update the entire list */
	UpdateProListWithFileData(hDlg,pProEdit);

	SetWindowText(hwndInputEdit,"");

	return totalCuts;
}

static int deleteOneCutData(HWND hwnd,userProgramEditData *pProEdit)
{
	int currentSel = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum - 1; /* total after deleted */
	int index, ret;

	if(totalCuts == 0){
		pProEdit->nInputCutsNum = 1;
		pProEdit->userProCuts[totalCuts] = 0;
		pProEdit->userEditStatus = 0;
		UpdateProListWithFileData(hwnd,pProEdit);

		return(1);
	}

	ret = MessageBox(hwnd,userpro_help[*curSysLang].msg_delcutdata_confirm,
			   userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
	switch(ret){
	case IDOK:
		/* delete confirmed. Do it */
		for(index = currentSel; index < totalCuts; index++){
			pProEdit->userProCuts[index] = pProEdit->userProCuts[index+1];
		}
		pProEdit->userProCuts[totalCuts] = 0;
		pProEdit->nInputCutsNum = totalCuts;

		UpdateProListWithFileData(hwnd,pProEdit);
		break;
	default:
		/* user cancled. Do nothing */
		break;
	}

	pProEdit->userEditStatus = 0;
	return totalCuts;
}

/* Delete all cut data behind current one
 */
static int deleteBehindCutDatas(HWND hwnd, userProgramEditData *pProEdit)
{
	int currentSel = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum;

	int ret = MessageBox(hwnd,userpro_help[*curSysLang].msg_delalldata_confirm,
			     userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
	switch(ret){
	case IDOK:
		memset(&(pProEdit->userProCuts[currentSel]), 0, totalCuts-currentSel);
		pProEdit->nInputCutsNum = currentSel + 1;

		UpdateProListWithFileData(hwnd,pProEdit);
		break;
	default:
		/* Do nothing */
		break;
	}

	pProEdit->userEditStatus = 0;
	return 1;

}

#if 0
/* Delete all datas
 */
static int deleteAllCutDatas(HWND hwnd, userProgramEditData *pProEdit)
{

	int ret = MessageBox(hwnd,userpro_help[*curSysLang].msg_delalldata_confirm,
			     userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
	switch(ret){
	case IDOK:
		memset(pProEdit->userProCuts, 0 ,sizeof(pProEdit->userProCuts));
		pProEdit->nInputCutsNum = 1;
		pProEdit->currentCutsNum = 0;

		UpdateProListWithFileData(hwnd,pProEdit);
		break;
	default:
		break;
	}

	pProEdit->userEditStatus = 0;
	return 1;
}
#endif

/* delete one data
 */
static int deleteOneData(int currentSel,int totalCuts,userProgramEditData *pProEdit)
{
	int index;

	for(index = currentSel; index < totalCuts; index++){
		pProEdit->userProCuts[index] = pProEdit->userProCuts[index+1];
	}
	pProEdit->userProCuts[totalCuts-1] = 0;
	return totalCuts;
}

/* Just all datas: Add/Sub a delta to/from all datas
 *
 * Must keep the data validation after just
 *
 */
static int justAllCutDatas(HWND hwnd, userProgramEditData *pProEdit, int delta)
{
	int frontLimit = getValidLimit();
	int totalCuts = pProEdit->nInputCutsNum;
	int index, action;
	int overNum = 0;
	int ret, saveFlag = 1;

	action = pProEdit->justEdit.action;

	switch(action){
	case 0:         /* reset */
		ResetUserProgramEdit(hwnd, pProEdit, 3);
		return(0);
	case 1:         /* Add */
		for(index = 0; index < totalCuts-1; index++){
			int tmp = pProEdit->userProCuts[index] + delta;
			if(isDataOutOfLimits(tmp)) {
				overNum++;
			}
		}
		if(overNum > 0){ /* some datas are invalidate if just, tell user and confirm */
			ret = MessageBox(hwnd,userpro_help[*curSysLang].msg_just_confirm,
					 userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
			if(ret == IDCANCEL){
				saveFlag = 0;
			}
		}

		if(saveFlag) {  /* just and save */
			index = 0;
			while(index < totalCuts-1){
				pProEdit->userProCuts[index] = pProEdit->userProCuts[index] + delta;
				int tmp = pProEdit->userProCuts[index];
				if(isDataOutOfLimits(tmp)) {
					deleteOneData(index,totalCuts,pProEdit);
					totalCuts--;
				} else {
					index++;
				}
			}
		}
		break;
	case 2:         /* Sub */
		for(index = 0; index < totalCuts - 1; index++){
			int tmp = pProEdit->userProCuts[index] - delta;
			if(isDataOutOfLimits(tmp)) {
				overNum++;
			}
		}

		if(overNum > 0){
			ret = MessageBox(hwnd,userpro_help[*curSysLang].msg_just_confirm,
					 userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
			if(ret == IDCANCEL){
				saveFlag = 0;
			}
		}

		if(saveFlag) {
			index = 0;
			while(index < totalCuts-1){
				pProEdit->userProCuts[index] = pProEdit->userProCuts[index] - delta;
				int tmp = pProEdit->userProCuts[index];
				if(isDataOutOfLimits(tmp)) {
					deleteOneData(index,totalCuts,pProEdit);
					totalCuts--;
				} else {
					index++;
				}
			}
		}
		break;
	}

	pProEdit->nInputCutsNum = totalCuts;
	programEdit.justEdit.action = 0;

	ResetUserProgramEdit(hwnd, pProEdit, 3);
	UpdateProListWithFileData(hwnd,pProEdit);

	return(totalCuts);
}


/* update list item flag
 * use pProEdit->currentCutsNum as the item index
 */
static int UpdateProListItemFlags(HWND hwnd,userProgramEditData *pProEdit)
{

	int data,flag;
	int iconflag = 0;
	unsigned int currentCut = pProEdit->currentCutsNum;
	HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);

	if(currentCut < MAX_PROGRAM_CUTS){
		data = pProEdit->userProCuts[currentCut] & 0x00ffffff ; /* get data */

		flag = pProEdit->userProCuts[currentCut] & AIR_FLAG;
		if(!flag)  /* flag = 0, ON */
			iconflag |= AIR_ICON_BIT;
		else
			iconflag |= AIR_ICON_BIT | AIR_ICON_SW_BIT;

		flag = pProEdit->userProCuts[currentCut] & PUSHER_FLAG;
		if(flag){
			iconflag |= PUSHER_ICON_BIT;
		}

		flag = pProEdit->userProCuts[currentCut] & AUTOCUT_FLAG;
		if(flag){
			iconflag |= AUTOCUT_ICON_BIT;
		}

		flag = pProEdit->userProCuts[currentCut] & PRESS_FLAG;
		if(flag){
			iconflag |= PRESS_ICON_BIT;
		}

		SendMessage(hwndProCutList,LB_SETICONFLAG, currentCut, (DWORD)iconflag);
		/* SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0); */

	} else {
		pProEdit->currentCutsNum = 0;
	}

	return 0;
}

static int UpdateProListAllItemFlags(HWND hwnd,userProgramEditData *pProEdit)
{
	int index;
	int data, flag;
	int iconflag;
	int currentCut = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum;
	HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);

	for(index=0; index < totalCuts; index++){
		iconflag = 0;
		data = pProEdit->userProCuts[index] & 0x00ffffff; /* get data */

		flag = pProEdit->userProCuts[index] & AIR_FLAG;
		if(!flag)  /* flag = 0, ON */
			iconflag |= AIR_ICON_BIT;
		else
			iconflag |= AIR_ICON_BIT | AIR_ICON_SW_BIT;

		flag = pProEdit->userProCuts[index] & PUSHER_FLAG;
		if(flag){
			iconflag |= PUSHER_ICON_BIT;
		}

		flag = pProEdit->userProCuts[index] & AUTOCUT_FLAG;
		if(flag){
			iconflag |= AUTOCUT_ICON_BIT;
		}

		flag = pProEdit->userProCuts[index] & PRESS_FLAG;
		if(flag){
			iconflag |= PRESS_ICON_BIT;
		}

		/* refresh flag */
		SendMessage(hwndProCutList,LB_SETICONFLAG, (WPARAM)index, (DWORD)iconflag);
	}

	SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);

	return 0;
}

static void changePusherFlagOfData(HWND hwnd,userProgramEditData *pProEdit)
{
	int currentSel = pProEdit->currentCutsNum;

	pProEdit->userProCuts[currentSel] ^=  PUSHER_FLAG;      /* Reverse PRESS_FLAG */
	pProEdit->userProCuts[currentSel] &=  ~AUTOCUT_FLAG;    /* Clear AUTOCUT_FLAG */
	pProEdit->userProCuts[currentSel] &=  ~PRESS_FLAG;      /* Clear PRESS_FLAG */

	UpdateProListItemFlags(hwnd, pProEdit);
}


static void changeAirFlagOfData(HWND hwnd,userProgramEditData *pProEdit)
{
	int currentSel = pProEdit->currentCutsNum;

	pProEdit->userProCuts[currentSel] ^=  AIR_FLAG; /* Reverse AIR_FLAG */

	UpdateProListItemFlags(hwnd, pProEdit);
}

static void changePressSheetFlagOfData(HWND hwnd,userProgramEditData *pProEdit)
{
	int currentSel = pProEdit->currentCutsNum;

	pProEdit->userProCuts[currentSel] ^=  PRESS_FLAG;       /* Reverse PRESS_FLAG */
	pProEdit->userProCuts[currentSel] &=  ~AUTOCUT_FLAG;    /* Clear AUTOCUT_FLAG */
	pProEdit->userProCuts[currentSel] &=  ~PUSHER_FLAG;     /* Clear PUSHER_FLAG */

	UpdateProListItemFlags(hwnd, pProEdit);
}


/* To add AUTOCUT flag to one data, the data shoule be:
 *    1: The data is smaller than the one before it.
 *    2: It is NOT the first data in this file
 *    3. AUTOCUT should not be together with PUSHER flag
 */
static void addAutoCutFlagToData(HWND hwnd,userProgramEditData *pProEdit)
{
	int currentSel = pProEdit->currentCutsNum;
	int foredata;
	int curdata;

	if(currentSel == 0) /* The first data */
		return;

	foredata = pProEdit->userProCuts[currentSel-1] & 0x00ffffff;
	curdata = pProEdit->userProCuts[currentSel] & 0x00ffffff;

	if(curdata < foredata){
		pProEdit->userProCuts[currentSel] |=  AUTOCUT_FLAG;
		pProEdit->userProCuts[currentSel] &=  ~PRESS_FLAG;      /* Clear PRESS_FLAG */
		pProEdit->userProCuts[currentSel] &=  ~PUSHER_FLAG;     /* Clear PUSHER_FLAG */
		UpdateProListItemFlags(hwnd,pProEdit);                  /* refresh flag */
	}
}

static void delAutoCutFlagFromData(HWND hwnd,userProgramEditData *pProEdit)
{
	int currentSel = pProEdit->currentCutsNum;

	pProEdit->userProCuts[currentSel] &=  ~AUTOCUT_FLAG;

	UpdateProListItemFlags(hwnd,pProEdit);
}

static void addAutoCutFlagToAllDatas(HWND hwnd,userProgramEditData *pProEdit)
{
	int totalCuts = pProEdit->nInputCutsNum;
	int index;
	int foredata, curdata;

	if(totalCuts == 1)
		return;

	/* The first data is passed */
	for(index = 1; index < totalCuts - 1; index++){
		foredata = pProEdit->userProCuts[index - 1] & 0x00ffffff;
		curdata = pProEdit->userProCuts[index] & 0x00ffffff;
		if(curdata < foredata){
			pProEdit->userProCuts[index] |=  AUTOCUT_FLAG;
			pProEdit->userProCuts[index] &=  ~PRESS_FLAG;   /* Clear PRESS_FLAG */
			pProEdit->userProCuts[index] &=  ~PUSHER_FLAG;  /* Clear PUSHER_FLAG */
		}
	}

	/* refresh all item flag */
	UpdateProListAllItemFlags(hwnd, pProEdit);
}

static void delAutoCutFlagFromAllDatas(HWND hwnd,userProgramEditData *pProEdit)
{
	int totalCuts = pProEdit->nInputCutsNum;
	int index;

	for(index = 0; index < totalCuts; index++){
		pProEdit->userProCuts[index] &=  ~AUTOCUT_FLAG;
	}

	/* refresh all item flag */
	UpdateProListAllItemFlags(hwnd, pProEdit);
}
