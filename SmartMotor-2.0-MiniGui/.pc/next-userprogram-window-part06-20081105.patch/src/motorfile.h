/*
 *  motorfile.h
 *
 *  Copyright 2008 @ xianweizeng@gmail.com
 *
 */
#ifndef _MOTORFILE_H_
#define _MOTORFILE_H_

/* porgram file descriptor */
struct userProFileHeader_t{
	int fileflags;          /* fileflags */
	int isnull;             /* this file contain NO data */
	int id;			/* id, file number */
	char introduction[20];  /* file description */
	char latestDate[12];    /* last modified/used date */
	int usedTimes;          /* used times */
	int first;              /* fisrt data */
};

#define MAX_FILE_PATH_NAME      48
struct fromProListToPro_t {
	struct userProFileHeader_t proHeader ;
	int isFileNew;	        /* file is new (NO data) */
	char filePathname[MAX_FILE_PATH_NAME];
};

extern struct fromProListToPro_t programCreate;

/* file link table structure */
struct programe_link_t{
	struct userProFileHeader_t prog;  /* porgram file descriptor */
	struct programe_link_t *next;
};

int listProgramFileByNum(HWND hwnd, int curSel);
int listProgramFileByDate(HWND hwnd, int curSel);
int deleteOneProFile(HWND mainhwnd, int curSel);
int initProgramList(HWND hwnd, int cursel);
struct programe_link_t * getProfileListItem(int cursel);
int getFilePathName(char *strPathName, int file_id);
int loadProgramFile(int id, struct fromProListToPro_t *pFileData);
int refreshProgramListByDefault(HWND hwnd, int curSel);

#endif /* _MOTORFILE_H_ */
