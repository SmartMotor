#ifndef _HELP_H_
#define _HELP_H_ 

/* CN */
extern unsigned char caption_bkgnd_cn[];
extern unsigned char caption_help_cn[];
extern unsigned char caption_file_cn[];
extern unsigned char caption_path_cn[];
extern unsigned char caption_waring_cn[];
extern unsigned char msg_name_len_cn[];
extern unsigned char msg_psw_len_cn[];
extern unsigned char msg_name_null_cn[];
extern unsigned char msg_name_exit_cn[];
extern unsigned char msg_default_user_cn[];
extern unsigned char msg_max_user_cn[];
extern unsigned char msg_psw_wrong_cn[];
extern unsigned char msg_psw_again_cn[];
extern unsigned char msg_newuser_ok_cn[];
extern unsigned char msg_psws_diff_cn[];
extern unsigned char msg_deluser_ok_cn[];
extern unsigned char msg_deluser_confirm_cn[];
extern unsigned char msg_psw_wrong_three_cn[];
extern unsigned char msg_prolist_header_cn[];
extern unsigned char msg_prolist_null_cn[];
extern unsigned char msg_cutslist_header_cn[];
extern unsigned char msg_cutslist_header_inch_cn[];
extern unsigned char msg_delpro_confirm_cn[];
extern unsigned char msg_delpro_ok_cn[];
extern unsigned char msg_enterpro_confirm_cn[];
extern unsigned char msg_cutdata_overflow_cn[];
extern unsigned char msg_step_overflow_cn[];
extern unsigned char msg_save_pro_confirm_cn[];
extern unsigned char msg_save_pro_ok_cn[];
extern unsigned char msg_delcutdata_confirm_cn[];
extern unsigned char msg_delalldata_confirm_cn[];
extern unsigned char msg_proname_change_cn[];
extern unsigned char msg_config_ok_cn[];
extern unsigned char msg_just_confirm_cn[];
extern unsigned char msg_welcome_cn[];

extern unsigned char help_login_main_cn[];
extern unsigned char help_psw_cn[];
extern unsigned char help_userpro_name_cn[];
extern unsigned char help_userpro_cuts_cn[];
extern unsigned char help_prolist_cn[];
extern unsigned char help_computer_cn[];
extern unsigned char help_t9_cn[];
extern unsigned char help_config_cn[];
extern unsigned char help_date_cn[];
extern unsigned char help_time_cn[];
extern unsigned char help_lang_cn[];
extern unsigned char help_flash_cn[];
extern unsigned char help_cutpos_cn[];
extern unsigned char help_limit_cn[];
extern unsigned char help_step_cn[];
extern unsigned char help_bkgnd_cn[];

/* English */
extern unsigned char caption_bkgnd_en[];
extern unsigned char caption_help_en[];
extern unsigned char caption_file_en[];
extern unsigned char caption_path_en[];
extern unsigned char caption_waring_en[];
extern unsigned char msg_name_len_en[];
extern unsigned char msg_psw_len_en[];
extern unsigned char msg_name_null_en[];
extern unsigned char msg_name_exit_en[];
extern unsigned char msg_default_user_en[];
extern unsigned char msg_max_user_en[];
extern unsigned char msg_psw_wrong_en[];
extern unsigned char msg_psw_again_en[];
extern unsigned char msg_newuser_ok_en[];
extern unsigned char msg_psws_diff_en[];
extern unsigned char msg_deluser_ok_en[];
extern unsigned char msg_deluser_confirm_en[];
extern unsigned char msg_psw_wrong_three_en[];
extern unsigned char msg_prolist_header_en[];
extern unsigned char msg_prolist_null_en[];
extern unsigned char msg_cutslist_header_en[];
extern unsigned char msg_cutslist_header_inch_en[];
extern unsigned char msg_delpro_confirm_en[];
extern unsigned char msg_delpro_ok_en[];
extern unsigned char msg_enterpro_confirm_en[];
extern unsigned char msg_cutdata_overflow_en[];
extern unsigned char msg_step_overflow_en[];
extern unsigned char msg_save_pro_confirm_en[];
extern unsigned char msg_save_pro_ok_en[];
extern unsigned char msg_delcutdata_confirm_en[];
extern unsigned char msg_delalldata_confirm_en[];
extern unsigned char msg_proname_change_en[];
extern unsigned char msg_config_ok_en[];
extern unsigned char msg_just_confirm_en[];
extern unsigned char msg_welcome_en[];

extern unsigned char help_login_main_en[];
extern unsigned char help_psw_en[];
extern unsigned char help_userpro_name_en[];
extern unsigned char help_userpro_cuts_en[];
extern unsigned char help_prolist_en[];
extern unsigned char help_computer_en[];
extern unsigned char help_t9_en[];
extern unsigned char help_config_en[];
extern unsigned char help_date_en[];
extern unsigned char help_time_en[];
extern unsigned char help_lang_en[];
extern unsigned char help_flash_en[];
extern unsigned char help_cutpos_en[];
extern unsigned char help_limit_en[];
extern unsigned char help_step_en[];
extern unsigned char help_bkgnd_en[];

#endif
