/*
 * src/configure-old.c
 *
 * Some unused configure window
 */


static BITMAP configurebuttonBkgnd;
static BITMAP *pSetUserBkgnds;	// tmp
static BITMAP setUserBkgnds;		// tmp
extern BITMAP userProgramEditBkgnd;

static int flashFlag = 0;
static int bkgndFlag = 0;

#define CommonConfigItem	5
#define UserConfigItem		6	// 6 - 5 no speed

#define CommonConfigStartX	80
#define CommonConfigStartY	120


#define UserBkgndScaleX 	130
#define UserBkgndScaleY		40
#define UserBkgndScaleW 	160
#define UserBkgndScaleH		120

#define UserBkgndNumber		2	// 2种选择
#define RunSpeedNumber		8	// 8种速度

static int userConfigList[UserConfigItem] = {0,1,2,3,4,7}; // 0,1,2,3,4,7
/*
 *	SetCutPos
 *	SetFrontLimit
 *	SetMiddleLimit
 *	SetBackLimit
 *	SetRunSpeed
 *	Back(exit)
 */
static int commonConfigList[CommonConfigItem] = {0,1,2,3,4};
/*
 *	SetSysDate
 *	SetSysTime
 *	SetInitFlash
 *	SetSysLanguage
 *	Back(exit)
 */

/**********************通用设置对话框*******************************/
static DLGTEMPLATE commonConfigureDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	0, 0, 640, 480,
	"commonSet",
	0, 0,
	1, NULL,
	0
};

static CTRLDATA commonConfigureCtrlsInit[] =
{
	{
		CTRL_STATIC,
		WS_VISIBLE | ES_READONLY ,
		455, 10, 175, 28,
		IDC_TIME,
		NULL,
		0,
		WS_EX_TRANSPARENT,
	}
};
/****************** 推纸器位置设定  ********************************/




/*****************************************************************************************/

/******************  系统语言设置  ********************************/
/******************  用户背景设置  ********************************/
static DLGTEMPLATE setUserBkgndDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	160, 120, 320, 240,
	"setUserBkgnd",
	0, 0,
	1, NULL,
	0
};

static CTRLDATA setUserBkgndCtrlsInit[] =
{
	{	CTRL_LISTBOX,
		WS_VISIBLE|WS_BORDER|WS_VSCROLL | LBS_NOTIFY,
		30, 40, 80, 120,
		IDC_USERBKGND_LIST,
		NULL,
		0
	}
};

/******************  运行速度设置  ********************************/
static DLGTEMPLATE setRunSpeedDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	160, 120, 300, 140,
	"setRunSpeed",
	0, 0,
	2, NULL,
	0
};

static CTRLDATA setRunSpeedCtrlsInit[] =
{
	{	CTRL_SLEDIT,
		WS_VISIBLE | WS_BORDER | ES_READONLY,
		130, 10, 140, 28,
		IDC_CURRENT_SPEED,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{	CTRL_LISTBOX,
		WS_VISIBLE|WS_BORDER|WS_VSCROLL | LBS_NOTIFY,
		130, 43, 140, 85,
		IDC_RUN_SPEED_LIST,
		NULL,
		0
	}
};


/*********************************************************/
/*   初始化背景,步进量设置
 */
static int currentStepMode;	// 当前步进模式 0 连续 1 步进
static int isStepDataEditWindowShow;
static int getCurrentStepMode(int stepSetStatus)
{
	int mode = 1;
	int curStep = 0;
	switch(stepSetStatus){
	case 4:
		curStep = currentuser->forwardstep;
		break;
   	case 5:
		curStep = currentuser->backwardstep;
		break;
	}
	if(curStep == 0){	// 连续
		mode = 0;
	}
	return(mode);
}
static void initSetRunStepBkgnd(HDC hdc,HWND hWndStepEdit,int stepMode)
{
	int startx=1940;
	FillBoxWithBitmap (hdc, 0, 0, 0, 0, &configureListBkgnd); 	// 背景
	RefreshBoxArea (hdc,&configurebuttonBkgnd,130,60,60,32,1600,0);	// 确定
	RefreshBoxArea (hdc,&configurebuttonBkgnd,210,60,60,32,1660,0);	// 退出

	if(stepMode){		// 步进模式
		RefreshBoxArea (hdc,&configurebuttonBkgnd,10,60,100,32,startx,0);	// 连续
		RefreshBoxArea (hdc,&configurebuttonBkgnd,10,15,100,32,startx+300,0);	// 选择步进
	} else {		// 连续模式
		if(isStepDataEditWindowShow){
			ShowWindow(hWndStepEdit,SW_HIDE);
			isStepDataEditWindowShow = 0;
		}
		RefreshBoxArea (hdc,&configurebuttonBkgnd,10,60,100,32,startx+100,0);	// 连续
		RefreshBoxArea (hdc,&configurebuttonBkgnd,10,15,100,32,startx+200,0);	// 选择步进
	}

}

static void refreshSetRunStepBkgnd(HDC hdc,HWND hWndStepEdit,int stepMode)
{
	int startx=1940;
	if(stepMode){		// 步进模式
		RefreshBoxArea (hdc,&configurebuttonBkgnd,10,60,100,32,startx,0);	// 连续
		RefreshBoxArea (hdc,&configurebuttonBkgnd,10,15,100,32,startx+300,0);	// 选择步进
		if(isStepDataEditWindowShow == 0){
			ShowWindow(hWndStepEdit,SW_SHOW);
			isStepDataEditWindowShow = 1;
			SetFocusChild(hWndStepEdit);
		}
	} else {		// 连续模式
		if(isStepDataEditWindowShow){
			ShowWindow(hWndStepEdit,SW_HIDE);
			isStepDataEditWindowShow = 0;
		}
		RefreshBoxArea (hdc,&configurebuttonBkgnd,10,60,100,32,startx+100,0);	// 连续
		RefreshBoxArea (hdc,&configurebuttonBkgnd,10,15,100,32,startx+200,0);	// 选择步进
	}
}

static void setRunStepEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	int dataLen;
	char dataBuf[10];
	int status = currentConfig;

	mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
	switch(nc){
	case EN_ENTER:		// Enter key 按下
		dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
		GetWindowText(hwnd,dataBuf,dataLen);
		int ret=isInputCutsDataValid(dataBuf,dataLen); 	// 判断输入是否有效
		if((ret >= 0) &&(dataLen>0)){	// 输入有效,返回转换后的数据
			//dataLen = 0说明用户没有输入直接输入确定
			switch(status){
			case 4:	// 前进步进量
				if((ret>500) || ((ret%100) != 0)){
					MessageBox(hwnd,config_help[*curSysLang].msg_step_overflow,
						   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
					SetWindowText(hwnd,"");	// 清除
					return;
				}
				currentuser->forwardstep = ret ;	// 1mm的整数倍
				saveUserConfigFile(0);	// 保存
				break;
			case 5:	// 后退步进量
				if((ret%100) != 0){	// 后退步进量没有大小限制
					MessageBox(hwnd,config_help[*curSysLang].msg_step_overflow,
						   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
					SetWindowText(hwnd,"");	// 清除
					return;			// 返回
				}
				currentuser->backwardstep = ret;
				saveUserConfigFile(0);	// 保存
				break;

			}
			MessageBox(hwnd,config_help[*curSysLang].msg_config_ok,
				   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
			SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0);	// 退出
		}else {
			MessageBox(hwnd,config_help[*curSysLang].msg_step_overflow,
				   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
			SetWindowText(hwnd,"");	// 清除
		}
		break;
	}
}

static int setRunStepDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndCutPosEdit = GetDlgItem(hDlg,IDC_POS_LIMIT);

	switch (message)
	{
    	case MSG_INITDIALOG:
		SetNotificationCallback(hwndCutPosEdit, setRunStepEditCallback);
		SetFocusChild(hwndCutPosEdit);
		currentStepMode = getCurrentStepMode(currentConfig);
		isStepDataEditWindowShow = 1;
		return(1);
    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}
		//初始化背景
		initSetRunStepBkgnd(hdc,hwndCutPosEdit,currentStepMode);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_CURSORBLOCKUP:		// 向上选择
			hdc=BeginPaint(hDlg);
			if(currentStepMode==0){
				currentStepMode = 1;
				refreshSetRunStepBkgnd(hdc,hwndCutPosEdit,currentStepMode);
			}
			EndPaint(hDlg,hdc);
			break;
		case SCANCODE_CURSORBLOCKDOWN:	// 向下选择
			hdc=BeginPaint(hDlg);
			if(currentStepMode==1){
				currentStepMode = 0;
				refreshSetRunStepBkgnd(hdc,hwndCutPosEdit,currentStepMode);
			}
			EndPaint(hDlg,hdc);
			break;
		case SCANCODE_ENTER:	//   取消
			if(currentStepMode==1){
				break;
			}
			switch(currentConfig){	// 用户确认连续模式
		    	case 4:	// 步进量
				currentuser->forwardstep = 0 ;
				saveUserConfigFile(0);	// 保存
				break;
		    	case 5:	// 步进量
				currentuser->backwardstep = 0;
				saveUserConfigFile(0);	// 保存
				break;
			}
			MessageBox(hDlg,config_help[*curSysLang].msg_config_ok,
				   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_CANCEL:	//   取消
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_HELP:	//帮助
			MessageBox(hDlg,config_help[*curSysLang].help_step,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		}
		break;
    	case MSG_COMMAND:
		switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;


	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}


static void setRunStepDialogBox (HWND hwnd)
{
	setPosLimitDlgBox.controls = setPosLimitCtrlsInit;
	DialogBoxIndirectParam (&setPosLimitDlgBox, hwnd,setRunStepDialogBoxProc, 0L);
}
static int setRunStep (HWND hwnd)	// 设置步进量
{
	//printf("setRunStep().\n");
	setRunStepDialogBox(hwnd);		// Should not return except receive CLOSE MSG
	return 0;

}


/***********************************************************/


/***********************************************************/
/**  系统开机动画设置 **/

static void refreshSetInitFlashBkgnd(HDC hdc,int flag)
{
	int x=30,y=15;

	if(flag){	// 使用动画
		RefreshButtonArea (hdc,&configurebuttonBkgnd,MSG_KEYDOWN,x,y,100,32, 5);	// 使用
		RefreshButtonArea (hdc,&configurebuttonBkgnd,MSG_KEYUP,x+110,y,100,32, 6);	// 不使用
	} else {
		RefreshButtonArea (hdc,&configurebuttonBkgnd,MSG_KEYUP,x,y,100,32, 5);
		RefreshButtonArea (hdc,&configurebuttonBkgnd,MSG_KEYDOWN,x+110,y,100,32, 6);
	}

}


/*   初始化背景
 */
static void initSetInitFlashBkgnd(HDC hdc)
{
	FillBoxWithBitmap (hdc, 0, 0, 0, 0, &configureListBkgnd); 	// 背景
	refreshSetInitFlashBkgnd(hdc,flashFlag);
	RefreshBoxArea (hdc,&configurebuttonBkgnd,60,60,60,32,2200,0);			// 确定
	RefreshBoxArea (hdc,&configurebuttonBkgnd,150,60,60,32,2260,0);		// 退出
}

static int setInitFlashDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;

	switch (message)
	{
    	case MSG_INITDIALOG:
		flashFlag = startupParams.isFlashOn;

		//SetNotificationCallback(hwndBackLimitEdit, setBackLimitEditCallback);
		//SetFocusChild(hwndBackLimitEdit);
		return(1);
    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}
		//初始化背景
		initSetInitFlashBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_CURSORBLOCKUP:		//  选择
		case SCANCODE_CURSORBLOCKLEFT:
			hdc=BeginPaint(hDlg);
			flashFlag = 1;
			refreshSetInitFlashBkgnd(hdc,flashFlag);
			EndPaint(hDlg,hdc);
			break;
		case SCANCODE_CURSORBLOCKDOWN:	//  选择
		case SCANCODE_CURSORBLOCKRIGHT:
			hdc=BeginPaint(hDlg);
			flashFlag = 0;
			refreshSetInitFlashBkgnd(hdc,flashFlag);
			EndPaint(hDlg,hdc);
			break;
		case SCANCODE_ENTER:	//  设置
			if(currentConfig == 2){
				startupParams.isFlashOn = flashFlag;
				saveInitConfigFile(&startupParams);
				SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			}

			break;
		case SCANCODE_CANCEL:	//   取消
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_HELP:	//帮助
			MessageBox(hDlg,config_help[*curSysLang].help_flash,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		}
		break;
    	case MSG_COMMAND:
		switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;


	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setInitFlashDialogBox (HWND hwnd)
{
	setSysLangDlgBox.controls = setSysLangCtrlsInit;
	DialogBoxIndirectParam (&setSysLangDlgBox, hwnd,setInitFlashDialogBoxProc, 0L);
}
static int setInitFlash (HWND hwnd)
{
	//printf("setInitFlash().\n");
	setInitFlashDialogBox(hwnd);		// Should not return except receive CLOSE MSG
	return 0;

}


/***********************************************************/

static int isScaleBmpLoaded = 0;
/*   初始化背景
 */
static void initSetUserBkgndBkgnd(HDC hdc)
{

	FillBoxWithBitmap (hdc, 0, 0, 320, 240, &configureListBkgnd); 	// 背景
	FillBoxWithBitmap (hdc, UserBkgndScaleX, UserBkgndScaleY,
			   UserBkgndScaleW,UserBkgndScaleH, pSetUserBkgnds); // 当前背景
	RefreshBoxArea (hdc,&configurebuttonBkgnd,60,180,60,32,1600,0);		// 确定
	RefreshBoxArea (hdc,&configurebuttonBkgnd,150,180,60,32,1660,0);	// 退出
}

static void initUserBkgndList(HWND hwndList,int curSel)
{
	int index;
	char dispBuf[16];
	char tmp[4];
	SendMessage(hwndList,LB_RESETCONTENT,0,0);	// 清空列表
	for(index=0;index<UserBkgndNumber;index++){
		strcpy(dispBuf,config_help[*curSysLang].caption_bkgnd);
		sprintf(tmp,"%d ",index);
		strcat(dispBuf,tmp);
		SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)dispBuf);
	}
	SendMessage(hwndList,LB_SETCURSEL,curSel,0);	 // 高亮当前
}

int loadUserBkgndScale(BITMAP *pBkgnd,int curSel)
{
	switch(curSel){
	case 0:
		LoadBitmap (HDC_SCREEN, pBkgnd, "./res/userProgramRun0.jpg");
		break;
	case 1:
		LoadBitmap (HDC_SCREEN, pBkgnd, "./res/userProgramRun1.jpg");
		break;
	default:
		LoadBitmap (HDC_SCREEN, pBkgnd, "./res/userProgramRun0.jpg");
		break;
	}
	isScaleBmpLoaded = 1;
	return 0;
}
static void userBkgndListCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	HDC hdc;
	mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
	switch(nc){
	case LBN_SELCHANGE:	// 列表框选项有变化,增加小箭头
		bkgndFlag = SendMessage(hwnd,LB_GETCURSEL,0,0);
		//printf("sel change...%d \n",bkgndFlag);
		if(isScaleBmpLoaded){
			UnloadBitmap (&setUserBkgnds);	// unload background
		}
		loadUserBkgndScale(&setUserBkgnds,bkgndFlag);
		hdc = BeginPaint(mainHwnd);
		FillBoxWithBitmap (hdc, UserBkgndScaleX, UserBkgndScaleY,
				   UserBkgndScaleW,UserBkgndScaleH, &setUserBkgnds); // 当前背景
		EndPaint(mainHwnd,hdc);
		break;
	case LBN_ENTER:		// Enter key 按下

		break;
	}
}


static int setUserBkgndDialogBoxProc(HWND hDlg, int message,
				     WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndBkgndList = GetDlgItem(hDlg,IDC_USERBKGND_LIST);
	switch (message)
	{
    	case MSG_INITDIALOG:
		bkgndFlag = startupParams.whichBkgnd;
		SetNotificationCallback(hwndBkgndList, userBkgndListCallback);
		initUserBkgndList(hwndBkgndList,bkgndFlag);
		pSetUserBkgnds = &userProgramEditBkgnd;

		SetFocusChild(hwndBkgndList);
		return(1);
    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}
		//初始化背景
		initSetUserBkgndBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){

		case SCANCODE_ENTER:	//  设置
			if(currentConfig == 6){
				loadUserBkgndScale(&userProgramEditBkgnd,bkgndFlag);
				startupParams.whichBkgnd = bkgndFlag;
				saveInitConfigFile(&startupParams);
				if(isScaleBmpLoaded){
					UnloadBitmap (&setUserBkgnds);	// unload background
				}
				SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			}
			break;
		case SCANCODE_CANCEL:	//   取消
			if(isScaleBmpLoaded){
				UnloadBitmap (&setUserBkgnds);	// unload background
			}
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_HELP:	//帮助
			MessageBox(hDlg,config_help[*curSysLang].help_bkgnd,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		}
		break;
    	case MSG_COMMAND:
		switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;


	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setUserBkgndDialogBox (HWND hwnd)
{
	setUserBkgndDlgBox.controls = setUserBkgndCtrlsInit;
	DialogBoxIndirectParam (&setUserBkgndDlgBox, hwnd,setUserBkgndDialogBoxProc, 0L);
}
static int setUserBkgnd (HWND hwnd)
{

	//printf("setUserBkgnd().\n");
	setUserBkgndDialogBox(hwnd);		// Should not return except receive CLOSE MSG
	UnloadBitmap (&setUserBkgnds);	// unload background
	return 0;

}

/*************设置运行速度********************************/
static void initSetRunSpeedBkgnd(HDC hdc)
{

	int sel = currentConfig;
	FillBoxWithBitmap (hdc, 0, 0, 300,140, &configureListBkgnd); 	// 背景
	RefreshButtonArea (hdc,&configurebuttonBkgnd,MSG_KEYUP,10,10,100,32, sel);// 输入提示
	RefreshBoxArea (hdc,&configurebuttonBkgnd,30,55,60,32,1600,0);		// 确定
	RefreshBoxArea (hdc,&configurebuttonBkgnd,30,95,60,32,1660,0);	// 退出
}

static void initRunSpeedList(HWND hwndList,HWND hwndCurSpeed,int curSel)
{
	int index;
	char dispBuf[16];
	sprintf(dispBuf,"%4d   M/Min",curSel);
	SetWindowText(hwndCurSpeed,dispBuf);	// refresh current speed

	SendMessage(hwndList,LB_RESETCONTENT,0,0);	// 清空列表
	for(index=0;index<RunSpeedNumber;index++){
		sprintf(dispBuf,"%4d   M/Min",8+index);
		SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)dispBuf);
	}
	SendMessage(hwndList,LB_SETCURSEL,(curSel-8),0);	 // 高亮当前
}


static void RunSpeedListCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	int curSel;
	mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
	char cmdSendBuf[13];
	memset(cmdSendBuf,0,12);
	cmdData.flag = 'S';
	cmdData.status = 0x30;
	switch(nc){
	case LBN_ENTER:		// Enter key 按下
		curSel = SendMessage(hwnd,LB_GETCURSEL,0,0);
		currentuser->speed = curSel + 8;
		saveUserConfigFile(0);	// 保存
		cmdData.data = currentuser->speed;
		setCmdFrame(&cmdData,cmdSendBuf);		// 组帧
		sendCommand(cmdSendBuf);  		// Send
		MessageBox(hwnd,config_help[*curSysLang].msg_config_ok,
			   config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0);	// 退出
		break;
	}
}


static int setRunSpeedDialogBoxProc(HWND hDlg, int message,
				    WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndSpeedList = GetDlgItem(hDlg,IDC_RUN_SPEED_LIST);
	HWND hwndCurSpeed = GetDlgItem(hDlg,IDC_CURRENT_SPEED);
	switch (message)
	{
    	case MSG_INITDIALOG:
		SetNotificationCallback(hwndSpeedList, RunSpeedListCallback);
		initRunSpeedList(hwndSpeedList,hwndCurSpeed,currentuser->speed);



		SetFocusChild(hwndSpeedList);
		return(1);
    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}
		//初始化背景
		initSetRunSpeedBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_CANCEL:	//   取消
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_HELP:	//帮助
			MessageBox(hDlg,config_help[*curSysLang].help_bkgnd,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		}
		break;
    	case MSG_COMMAND:
		switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;


	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setRunSpeedDialogBox (HWND hwnd)
{
	setRunSpeedDlgBox.controls = setRunSpeedCtrlsInit;
	DialogBoxIndirectParam (&setRunSpeedDlgBox, hwnd,setRunSpeedDialogBoxProc, 0L);
}
static int setRunSpeed (HWND hwnd)
{

	//printf("setRunSpeed().\n");
	setRunSpeedDialogBox(hwnd);		// Should not return except receive CLOSE MSG
	//UnloadBitmap (&setUserBkgnds);	// unload background
	return 0;

}


/*************************************************************/
/*   初始化背景及设置选项
 */

static int loadConfigCommonIcon(void)
{
	int langtype = *curSysLang;
	switch(langtype){
	case 1:	// 英文
		if (LoadBitmap (HDC_SCREEN, &configurebuttonBkgnd, "./res/configcommon_en.jpg"))
			return 1;
		break;
	case 0:
	default:
		if (LoadBitmap (HDC_SCREEN, &configurebuttonBkgnd, "./res/configcommon.jpg"))
			return 1;
		break;
	}
	return(0);
}

static int EnterCommonConfigDialog(HWND hwnd,int sel)
{
	switch(sel){
	case 0:	// 日期设置
		setSysDate(hwnd);
		break;
	case 1:	// 时间设置
		setSysTime(hwnd);
		break;
	case 2:	// 开机动画
		setInitFlash(hwnd);
		break;
	case 3:	// 语言设置
		printf("setSysLang...\n");
		setSysLang(hwnd);
		loadConfigCommonIcon();		// 重新载入
		return(1);
		break;
	case 4:	// 退出
		SendMessage(hwnd,MSG_COMMAND,IDCANCEL,0);
		break;
	}
	return 0;
}

static int RefreshCommonConfigListBkgnd(HDC hdc,int total,int sel)
{
	int i;
	int xBase = CommonConfigStartX;
	int yBase = CommonConfigStartY;
	int x,y,index;
	int flag;
	for(i=0;i<total;i++){
		flag = ((i == sel)? MSG_KEYDOWN : MSG_KEYUP);
		index = commonConfigList[i];	// 得到位置信息
		x = xBase;
		y = yBase + i*34;
		RefreshButtonArea (hdc,&configurebuttonBkgnd,flag,x,y,100,32, index);
	}

	return sel;
}

static void initCommonConfigBkgnd(HDC hdc)
{
	FillBoxWithBitmap (hdc, 0, 0, 0, 0, &configureBkgnd); 	// 背景
	RefreshCommonConfigListBkgnd(hdc,CommonConfigItem,currentConfig);
}


static int commonConfigureDialogBoxProc(HWND hDlg, int message, WPARAM wParam,
					LPARAM lParam)
{
	HDC hdc;
	HWND  hwndClock = GetDlgItem(hDlg,IDC_TIME);
	char timeBuff[24];		// 时钟显示,当前位置显示
	int ret;
	switch (message)
	{
    	case MSG_INITDIALOG:
		currentConfig = 0;
		SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);
		if(isIMEOpenInMotor == 1){
			ShowWindow(myIMEWindow,SW_HIDE);
			isIMEOpenInMotor = 0;
		}
		SetWindowElementColorEx(hwndClock,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 255, 255)); //White
		SetTimer(hDlg,IDC_TIMER,50);	// 时钟显示
		SetFocusChild(hwndClock);
		return(1);
    	case MSG_TIMER:
		SetWindowText(hwndClock,convertSysTime(timeBuff));
		return 0;
    	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		//初始化背景
		initCommonConfigBkgnd(hdc);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
    	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_ENTER:
			ret = EnterCommonConfigDialog(hDlg,currentConfig);
			if(ret){
				InvalidateRect(hDlg,NULL,TRUE);	// 引发窗口重绘
			}
			break;

		case SCANCODE_CURSORBLOCKUP:		// 向上选择
			hdc=BeginPaint(hDlg);
			// 单向选择
			if(currentConfig>0){
				currentConfig--;
			}else{
				currentConfig = 0;
			}
			RefreshCommonConfigListBkgnd(hdc,CommonConfigItem,currentConfig);
			EndPaint(hDlg,hdc);
			break;
		case SCANCODE_CURSORBLOCKDOWN:	// 向下选择
			hdc=BeginPaint(hDlg);
			// 单向选择
			if(currentConfig<CommonConfigItem-1){
				currentConfig++;
			}else{
				currentConfig = CommonConfigItem-1;
			}
			RefreshCommonConfigListBkgnd(hdc,CommonConfigItem,currentConfig);
			EndPaint(hDlg,hdc);
			break;
		case SCANCODE_CANCEL:	//   取消
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_HELP:	//帮助
			MessageBox(hDlg,config_help[*curSysLang].help_config,
				   config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			break;
		}
		break;

    	case MSG_COMMAND:
		switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
			KillTimer(hDlg,IDC_TIMER);
			EndDialog(hDlg,IDOK);
			break;
		}
		break;

	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void commonConfigureDialogBox (HWND hwnd)
{
	commonConfigureDlgBox.controls = commonConfigureCtrlsInit;
	DialogBoxIndirectParam (&commonConfigureDlgBox, hwnd,commonConfigureDialogBoxProc, 0L);
}

int EnterConfigureCommon (HWND hwnd)
{
	if (LoadBitmap (HDC_SCREEN, &configureBkgnd, "./res/configBkgnd.jpg"))	// 主背景
		return 1;
	if(loadConfigCommonIcon())	// 配置选项
		return(1);
	if (LoadBitmap (HDC_SCREEN, &configureListBkgnd, "./res/configItemBkgnd.jpg"))// 配置界面背景
		return 1;

	printf("Enter System Configure...\n");
	commonConfigureDialogBox(hwnd);		// Should not return except receive CLOSE MSG

	//printf("Unload configure res...\n");
	UnloadBitmap (&configureBkgnd);	// unload background
	UnloadBitmap (&configureListBkgnd);	// unload background
	UnloadBitmap (&configurebuttonBkgnd);	// unload background
	return 0;

}

