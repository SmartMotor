/*  src/computer.h
 *  2005-11-30
 *  基于MiniGUI的计算器
 */

#ifndef _SRC_COMPUTER_H_
#define _SRC_COMPUTER_H_

// eg: a + b = result
typedef struct tagComputer_t{
    int a;
    int b;
    int result;
    int method;	// 1+ 2- 3*  4/ 0 NULL
    int status;	// 0 : input a; 1 : input b; 
    int isInputChanged;
    int isDataFloat;		// 输入是否为小数标志位
    int isLastDataFloat;	// 上一次输入是否为小数
    char inputBuf[16];	// 编辑框输入,显示缓冲区
} tagComputer;

/*	图标结构体
 */
typedef struct tagButtonBmp_t{
    int x;			// 显示的X坐标
    int y;			// 显示的Y坐标
    int index;			// 索引号
} tagButtonBmp;


tagComputer computerData;

int EnterComputer(HWND hWnd);
char * convertSysTime(char *dispBuf);

#endif
