/*  src/userprogram.h
 *  2005-11-12
 */

#ifndef _SRC_BKGNDEDIT_H_
#define _SRC_BKGNDEDIT_H_

struct functionButton_t{
    int bx;	// 在屏幕的位置
    int by;
    int bw;	// 宽度
    int bh;
    int pos;	// 0 -> F1,由此计算按钮在图片的位置
    int textbase;
    int textpos;	// 由此计算文字在图片的位置
    int textw;
};

//minigui/windows.h
//#define MSG_USER	0x0800	
#define MSG_RECV	0x0801

struct comCmdFrame_t{	// 通信帧结构体
    char header;		// 起始标志 %
    char flag;		// 帧标志
    char addr[2];		// 地址
    char data[6];		// 数据
    char checksum[2];	// 校验和
};

struct comCmdData_t{	// 通信帧结构体
    char flag;		// 帧标志
    int status;		// 地址
    int data;		// 数据
};

int RefreshButtonArea (HDC hdc, const BITMAP *pBitmap, int msg,int bx,int by,int bw,int bh, int pos );
int RefreshBoxArea (HDC hdc, const BITMAP *pBitmap,int bx,int by,int bw,int bh, int x ,int y );
int RefreshFunctionButton (HDC hdc, const BITMAP *pBitmap, int down,struct functionButton_t *pButton,int textflag);

int initSerialCommunication(void);
int sendCommand(char *cmd);
int recvCommand(char *recvBuf);
int isCmdValid(char *cmdbuf);
//int getCmdFrameData(char flag,int addr,char *cmdbuf);
//int setCmdFrameBuf(char flag,int addr,int data,char *cmdbuf);

int getCmdFrame(struct comCmdData_t *pData,char *cmdbuf);
int setCmdFrame(struct comCmdData_t *pData,char *cmdbuf);

int mm_to_inch(int mm);
int inch_to_mm(int mil);

#endif
