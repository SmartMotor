/*   motorfile.h
 *   2005-11-12
 */

#ifndef _MOTORFILE_H_
#define _MOTORFILE_H_


struct programe_t{		// file
    int fileflags;
    int isnull;
    int id;			// id, file number
    char introduction[20];
    char recent_date[12];
    int times;
    int first;
}programe;

struct programe_link_t{	 	// for file
   struct programe_t prog;
   struct programe_link_t *next;
};



int EnterProListDialog(void);

#endif
