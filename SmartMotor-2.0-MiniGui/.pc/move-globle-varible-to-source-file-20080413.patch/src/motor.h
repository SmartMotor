
#ifndef _MOTOR_H_
#define _MOTOR_H_


HWND myIMEWindow;	// 输入法窗口句柄
int isIMEOpenInMotor;	// 输入法窗口显示? 0 :隐藏 1: 显示

#define SYSLANG_NO	2	// 支持两种语言

PLOGFONT systemDefaultFont,userFont24,userFont16;	// 定义字体

/*   机器属性结构体
 */
struct motorStatus_t{
   int currentCutPos;		// 切刀的当前位置
   int newCutPos;			// 设定的切刀位置,当newCutPos=currentCutPos时设定成功
   int devStatus;			// 设备状态
   int flags;
   int cutCounter;			// 下刀计数
};

struct motorStatus_t motorStatus;	// 定义全局变量


#define IDC_STATE	100
#define IDC_TIME 	110
#define IDC_TIMER    	120
#define IDC_EDITNAME	130
#define IDL_LISTNAME    140
#define IDC_PASSWORD	150
#define  IDL_FILE		160
#define 	IDC_PATH	170
#define _ID_TIMERP      180
#define IDC_COMSTOR    190
#define IDC_USTOR	200
#define IDC_RECENTPROGRAME	210
#define IDC_PROGRAME_HEAD    	220

#define IDC_CUT_POS		230
#define IDC_CURRENT_POS		235
#define IDC_POS_LIMIT		240
#define IDC_CHINESE		270
#define IDC_ENGLISH		280

#define IDC_YEAR			290
#define IDC_MONTH		291
#define IDC_DAY	        	292
#define IDC_HOUR			293
#define IDC_MINUTE		294

#define IDC_LIMIT_F		241
#define IDC_LIMIT_M		242
#define IDC_LIMIT_B		243
#define IDC_FORWARD_STEP	244
#define IDC_BACKWARD_STEP	245
#define IDC_RUN_SPEED		246
#define IDC_CURRENT_SPEED	247

#define IDC_USERBKGND_LIST	520
#define IDC_RUN_SPEED_LIST	521

/**User Program**/
//#define IDC_UPSTATE		500
//#define IDC_UPTIME 		501
#define IDC_PROGRAMCUTSLISTHEAD	502
#define IDC_PROGRAMNUM		503
#define IDC_PROGRAMNAME    	504
#define IDC_INPUTCUTDATA    	505
#define IDL_PROGRAMCUTSLIST	506
#define IDC_CURPOS		507	// 当前位置
#define IDC_CUTCOUNTER		508

#define IDC_LABLETOTALLEN	510
#define IDC_LABLELEN		511
#define IDC_UNUSEDLEN		512
#define IDC_HALFTOTALLEN	513
#define IDC_HALFNUMBER		514

#define IDC_TRACKBAR		600
#define IDC_COMPUTERDATA		510

#endif

