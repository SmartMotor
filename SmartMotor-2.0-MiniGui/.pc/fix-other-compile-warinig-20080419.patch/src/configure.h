/*
 * src/configure.h
 *
 * Copyright 2008 @ xianweizeng@gmail.com
 *
 */
#ifndef _MOTORCONFIGURE_H_
#define _MOTORCONFIGURE_H_

struct configure{
	int language;
	int frontlimit;
	int middlelimit;
	int backlimit;

};

struct configure_date{
	int year;
	int month;
	int day;
};

struct configure_time{
	int hour;
	int min;
	int sec;
};

struct rtc_tm {
  	int tm_sec;			/* Seconds.	[0-60] (1 leap second) */
  	int tm_min;			/* Minutes.	[0-59] */
  	int tm_hour;			/* Hours.	[0-23] */
  	int tm_mday;			/* Day.		[1-31] */
  	int tm_mon;			/* Month.	[0-11] */
  	int tm_year;			/* Year	- 1900.  */
  	int tm_wday;			/* Day of week.	[0-6] */
};


int EnterConfigure (HWND hwnd);
int getSysRtcTime(struct rtc_tm *tm);
char * convertSysTime(char *dispBuf);
int loadUserBkgndScale(BITMAP *pBkgnd,int curSel);

#endif
