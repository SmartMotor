/*
 *  motor.h
 *
 *  Copyright 2008 @ xianweizeng@gmail.com
 *
 *  defined globle structure and use "extern" to declaim global variables
 *
 */

#ifndef _MOTOR_H_
#define _MOTOR_H_

/* debug configs, comment out other level */

/* 1 for debug on pc */
#define CONFIG_DEBUG_ON_PC

/* 2 for debug on target */
/* #define CONFIG_DEBUG_ON_TARGET */

/* 3 for release */
/* #undef CONFIG_DEBUG_ON_PC */
/* #undef CONFIG_DEBUG_ON_TARGET */

#define SYSLANG_NO	2	/* system supported language number */


/*   MotorStatus structure
 */
struct motorStatus_t{
	int currentCutPos;	/* current position */
	int newCutPos;		/* new position */
	int devStatus;		/* motor status */
	int flags;              /* motor flags */
	int cutCounter;		/* cut counter */
};

/* defined in motormain.c */
extern struct motorStatus_t motorStatus;
extern HWND myIMEWindow;
extern int isIMEOpenInMotor;	/* 0: disappear 1: display */
extern PLOGFONT systemDefaultFont,userFont24,userFont16;
extern int *curSysLang;

/* defined IDC resources */
#define IDC_STATE	100
#define IDC_TIME 	110
#define IDC_TIMER    	120
#define IDC_EDITNAME	130
#define IDL_LISTNAME    140
#define IDC_PASSWORD	150
#define  IDL_FILE		160
#define 	IDC_PATH	170
#define _ID_TIMERP      180
#define IDC_COMSTOR    190
#define IDC_USTOR	200
#define IDC_RECENTPROGRAME	210
#define IDC_PROGRAME_HEAD    	220

#define IDC_CUT_POS		230
#define IDC_CURRENT_POS		235
#define IDC_POS_LIMIT		240
#define IDC_CHINESE		270
#define IDC_ENGLISH		280

#define IDC_YEAR		290
#define IDC_MONTH		291
#define IDC_DAY	        	292
#define IDC_HOUR		293
#define IDC_MINUTE		294

#define IDC_LIMIT_F		241
#define IDC_LIMIT_M		242
#define IDC_LIMIT_B		243
#define IDC_FORWARD_STEP	244
#define IDC_BACKWARD_STEP	245
#define IDC_RUN_SPEED		246
#define IDC_CURRENT_SPEED	247

#define IDC_USERBKGND_LIST	520
#define IDC_RUN_SPEED_LIST	521

/**User Program**/
//#define IDC_UPSTATE		500
//#define IDC_UPTIME 		501
#define IDC_PROGRAMCUTSLISTHEAD	502
#define IDC_PROGRAMNUM		503
#define IDC_PROGRAMNAME    	504
#define IDC_INPUTCUTDATA    	505
#define IDL_PROGRAMCUTSLIST	506
#define IDC_CURPOS		507	// 当前位置
#define IDC_CUTCOUNTER		508

#define IDC_LABLETOTALLEN	510
#define IDC_LABLELEN		511
#define IDC_UNUSEDLEN		512
#define IDC_HALFTOTALLEN	513
#define IDC_HALFNUMBER		514

#define IDC_TRACKBAR		600
#define IDC_COMPUTERDATA	510

#endif /* _MOTOR_H_ */

