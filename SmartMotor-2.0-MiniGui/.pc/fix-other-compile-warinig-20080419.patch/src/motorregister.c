/*  src/motorregister.c
 *  
 *  History:
 *	V0.1	Created by Quan Dongxiao
 *	V0.2	Modified by Zeng Xianwei @ 2005-12-27
 *
 *	I rewrite this program @2005-12-27 for some bugs
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <ctype.h> 
#include <unistd.h>
#include <pwd.h>
#include <errno.h>


#include <minigui/common.h>
#include <minigui/minigui.h>
#include <minigui/gdi.h>
#include <minigui/window.h>
#include <minigui/control.h>

#include "motorregister.h"
#include "motor.h"
#include "motorfile.h"
#include "configure.h"
#include "help.h"

/* define global variables */
struct userinfor_t userinformation[MAX_USER_NUMBER];
struct userinfor_t *currentuser;    /* pointer to current user */
                                    /* currentuser = userinformation[currentid] */
int  currentid;			    /* current user id */
int  usernumber;  		    /* current total user number */


static BITMAP registerSelectBkgnd;
static BITMAP registerNewBkgnd;
static BITMAP passwordBkgnd;
static BITMAP passwordAgainBkgnd;
static int isPSWAgainBkgndLoaded = 0;

static int registerStatus = 0;	
static int createUserFlag = 0;	// if the seleted user is new created one, then
				// no password is needed

static int refreshUsersList(HWND hWnd);
static int initRegisterDialog(HWND hWnd);
static int EnterPasswordDialog(HWND hWnd);

extern char * convertSysTime(char *dispBuf);

struct login_help_t{
	unsigned char * caption_help;
	unsigned char * caption_waring;
	unsigned char * msg_name_len;
	unsigned char * msg_name_null;
	unsigned char * msg_name_exist;
	unsigned char * msg_default_user;
	unsigned char * msg_max_user;
	unsigned char * msg_psw_len;
	unsigned char * msg_psw_wrong;
	unsigned char * msg_psw_again;
	unsigned char * msg_psws_diff;
	unsigned char * msg_psw_wrong_three;
	unsigned char * msg_newuser_ok;
	unsigned char * msg_deluser_ok;
	unsigned char * msg_deluser_confirm;
	unsigned char * help_login_main;
	unsigned char * help_psw;
	unsigned char * help_t9;	
};

static struct login_help_t login_help[SYSLANG_NO] = 
{
    {	caption_help_cn,
	caption_waring_cn,
	msg_name_len_cn,
	msg_name_null_cn,
	msg_name_exit_cn,
	msg_default_user_cn,
	msg_max_user_cn,
	msg_psw_len_cn,
	msg_psw_wrong_cn,
 	msg_psw_again_cn,
	msg_psws_diff_cn,
	msg_psw_wrong_three_cn,
	msg_newuser_ok_cn,
	msg_deluser_ok_cn,
	msg_deluser_confirm_cn,
	help_login_main_cn,
	help_psw_cn,
	help_t9_cn
   },

   {	caption_help_en,
	caption_waring_en,
	msg_name_len_en,
	msg_name_null_en,
	msg_name_exit_en,
	msg_default_user_en,
	msg_max_user_en,
	msg_psw_len_en,
	msg_psw_wrong_en,
 	msg_psw_again_en,
	msg_psws_diff_en,
	msg_psw_wrong_three_en,
	msg_newuser_ok_en,
	msg_deluser_ok_en,
	msg_deluser_confirm_en,
	help_login_main_en,
	help_psw_en,
	help_t9_en
   }
};


/**************************************************************/
/* Login Window*/
static DLGTEMPLATE DlgBoxregister =
{
    WS_BORDER , 
    WS_EX_NONE,
    0, 0,640, 480, 
    "Register",
    0, 0,
  3, NULL,
    0
};

static CTRLDATA Ctrlregister [] =                   
{ 
	{
        CTRL_STATIC,
        WS_VISIBLE | SS_CENTER,
        460, 5, 180, 30,
        IDC_TIME,
        NULL,
        0,
	WS_EX_TRANSPARENT
    	},
	 {
        CTRL_SLEDIT,
        WS_VISIBLE | SS_SIMPLE |WS_BORDER,
        170,70, 150, 30,
        IDC_EDITNAME,
        "",
        0
    	},
	{
	CTRL_LISTBOX,
	WS_VISIBLE|WS_BORDER|LBS_NOTIFY,
	322,70,180,150,
	IDL_LISTNAME,
	"",
	0
	}
};
/**************************************************************/
/* Passwoed Window*/
static DLGTEMPLATE DlgBoxpassword =					
{
    WS_BORDER , 
    WS_EX_NONE,
    0, 0,640, 480, 
    "",
    0, 0,
    1, NULL,
    0
};

static CTRLDATA Ctrlpassword [] =                   
{
	{
        CTRL_EDIT,
        WS_VISIBLE | WS_BORDER | ES_PASSWORD,
        135,210, 150, 30,
        IDC_PASSWORD,
        NULL,
        0
    	}
};


/*******************************************************************/
void onefile_new(const char*filename,int i)
{
	FILE *fp;
	struct userProFileHeader_t programe;

	if((fp=fopen(filename,"wb"))==NULL){
		printf("openfile wrong!\n");
		return;
	}

	programe.fileflags=1234;
	programe.isnull=1;
	programe.id=i;
	programe.introduction[0]='\0';
	programe.latestDate[0]='\0';
	programe.usedTimes=0;
	programe.first=0;
	fwrite(&programe,sizeof(struct userProFileHeader_t),1,fp);
	fclose(fp);
}
/*********************************************************************/
static void file_new(const char* path)
{ 	char   file [80];
	char   temp[8];
	struct userProFileHeader_t programe;

    	strcpy(file,path);
    	strcat(file,"/");
	int length;
	length=strlen(file);
	int i=0;
	for(i=1;i<100;i++)	// Create 99 files
	{
		file[length]='\0';
		sprintf(temp,"%d%s",i,".pro");
		strcat(file,temp);
		onefile_new(file,i);
	}

}


/*************************************/
/*	Local Functions For Register Dialog	****/
/*************************************/


/*  read user config file, store in struct userinfor *userInfo[]
 *
 */
int loadUserConfigFile(void)
{
    int index=0;		// user number
    char buff[11][16];	
    FILE *fp;
    if((fp=fopen("./user/user.cfg","r"))==NULL)
    {
	printf("openfile wrong!\n");
	return 0;
    }

    while(!feof(fp))			// Should make better performance here
    {
	fscanf(fp,"%s %s %s %s %s %s %s %s %s %s %s\n",
		buff[0],buff[1],buff[2],buff[3],buff[4],buff[5],buff[6],buff[7],buff[8],buff[9],buff[10]);

	strcpy(userinformation[index].name,buff[0]);
	strcpy(userinformation[index].password,buff[1]);  // super user's PWD is super PWD
	userinformation[index].recent_programe=atoi(buff[2]);
	userinformation[index].language=atoi(buff[3]);
	userinformation[index].unit=atoi(buff[4]);
	userinformation[index].frontlimit=atoi(buff[5]);
	userinformation[index].middlelimit=atoi(buff[6]);
	userinformation[index].backlimit=atoi(buff[7]);
	userinformation[index].forwardstep=atoi(buff[8]);
	userinformation[index].backwardstep=atoi(buff[9]);
	userinformation[index].speed=atoi(buff[10]);

	index++;
    }
    fclose(fp);
    usernumber = index;
    currentid = 0;	// current user 
    // printf("Total user number %d \n",usernumber);	
    return(usernumber);

}

/*  flags: delete flag
 *  flag > 0 
 *		  flags = 0 全部保存 
 */
int saveUserConfigFile(int flags)
{
    int i;
    FILE *fp;
    if((fp=fopen("./user/user.cfg","w"))==NULL)
    {
	printf("openfile wrong!\n");
	return 0;	
    } 

    for(i=0;i<usernumber;i++)
    {
        if((i>0) && (flags>0) && (i==flags)){
  	    continue;	// 退出本次循环
        }
	fprintf(fp,"%s %s %d %d %d %d %d %d %d %d %d\n",
			userinformation[i].name,userinformation[i].password,
			userinformation[i].recent_programe,userinformation[i].language,
			userinformation[i].unit,userinformation[i].frontlimit,
			userinformation[i].middlelimit,userinformation[i].backlimit,
			userinformation[i].forwardstep,userinformation[i].backwardstep,
			userinformation[i].speed);
    }
    fclose(fp);	
    if(flags){
    	usernumber = usernumber -1;
    }

    return(usernumber);
}

static int createOneUser(HWND hWnd)
{
   char buff[16];
   HWND hwndName=GetDlgItem(hWnd,IDC_EDITNAME);
   if(usernumber >= MAX_USER_NUMBER){	
	MessageBox(hWnd,login_help[*curSysLang].msg_max_user,
		  login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
	return 0;
   }

   buff[0]='\0';
   SetDlgItemText(hWnd,IDC_EDITNAME,buff);	// 清空输入框
   SetFocusChild(hwndName);		// 焦点在输入框
   sprintf(buff,"%d.  ",usernumber+1);	// 列表框加入空格,这样就可以反显
   SendDlgItemMessage(hWnd,IDL_LISTNAME,LB_ADDSTRING,0,(LPARAM)buff);
   SendDlgItemMessage(hWnd,IDL_LISTNAME,LB_SETCURSEL,usernumber,0);
 
   currentid = usernumber;		// 设置当前用户为该新建用户
   createUserFlag = currentid;   	// 记录该用户为新建的用户

   return currentid;
}

static int createUserPrograms(HWND hWnd)
{
    char cmdbuff[80];
    char path[40];		//建立99个空文件					
    strcpy(cmdbuff,"mkdir ./programe/");	//创建程序文件夹
    strcat(cmdbuff,userinformation[currentid].name);
    system(cmdbuff);
    strcpy(path,"./programe/");
    strcat(path,userinformation[currentid].name);
    file_new(path);
   
    userinformation[currentid].recent_programe = 1;
    userinformation[currentid].language = 0;
    userinformation[currentid].unit = 0;
    userinformation[currentid].frontlimit = 3000;
    userinformation[currentid].middlelimit = 8000;
    userinformation[currentid].backlimit = 145500;	  
    userinformation[currentid].forwardstep = 100;
    userinformation[currentid].backwardstep = 100;
    userinformation[currentid].speed = 8;	// default speed 8m/min
    MessageBox(hWnd,login_help[*curSysLang].msg_newuser_ok,
		   login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
    return(0);
}

/*  删除用户,curSel为删除的用户在列表中的序号
 */
static int deleteOneUser(HWND hWnd) 
{
    int ret;
    int curSel = currentid;
    char cmdbuff[40];
    if((curSel == 0) || (curSel == 1)){	// 超级用户不能删除
	MessageBox(hWnd,login_help[*curSysLang].msg_default_user,
		       login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
	registerStatus = 0;
        return 0;
    }
    ret=MessageBox(hWnd,login_help[*curSysLang].msg_deluser_confirm,
		 login_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);

    if(ret==IDOK){
    	// 验证超级密码
	registerStatus = 7;	// 状态标志
	EnterPasswordDialog(hWnd);
	if(registerStatus == 8){	// 超级密码正确
		//printf("curSel = %d \n",curSel);
	   	saveUserConfigFile(curSel);	// 删除用户配置文件
		//删除相应用户的程序文件夹
		strcpy(cmdbuff,"rm -rf  ./programe/");
		strcat(cmdbuff,userinformation[curSel].name);
		system(cmdbuff);
		MessageBox(hWnd,login_help[*curSysLang].msg_deluser_ok,
		 	  login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
	}
	// 超级密码错误
	initRegisterDialog(hWnd);
    }
    else{				// 取消删除
	registerStatus = 0;	
	refreshUsersList(hWnd);	// 刷新列表		
    }
    return 1;
}

static int refreshUsersList(HWND hWnd)
{
    char disbuff[16];
    int index;
    int totaluser = usernumber;
    int id = currentid;

    SendDlgItemMessage (hWnd, IDL_LISTNAME, LB_RESETCONTENT, 0, (LPARAM)0);

    for(index=0;index<totaluser;index++)
    {
  	memset(disbuff,0,16);	// First clear it
	sprintf(disbuff,"%d.  ",index+1);
	strcat(disbuff,userinformation[index].name);
	if(index == 0){
	    strcat(disbuff,"  **(mm)");
	}else if(index == 1){
	    strcat(disbuff,"  **(In)");
	}else{
	    strcat(disbuff," ");
	}
	SendDlgItemMessage(hWnd,IDL_LISTNAME,LB_ADDSTRING,0,(LPARAM)disbuff);
    }
 
    SetDlgItemText(hWnd,IDC_EDITNAME,userinformation[id].name);	
    SendDlgItemMessage(hWnd,IDL_LISTNAME,LB_SETCURSEL,id,0);

    //currentid=10; //?

    return 0;
}  

static int initRegisterDialog(HWND hWnd)
{
    registerStatus = 0;

    //loadUserConfigFile();
    refreshUsersList(hWnd);	
    return 0;
}

static int resetRegisterDialog(HWND hWnd)
{
    registerStatus = 0;
    currentid = 0;
    loadUserConfigFile();
    refreshUsersList(hWnd);	
    return 0;
}

/*  判断输入用户名是否合法,是返回1,否则返回0
 */
static int isUserNameValid(HWND hWnd,char *nameBuff,int len)
{
    int i;
    if(len==0){		//如果用户名为空
	MessageBox(hWnd,login_help[*curSysLang].msg_name_null,
		  login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
	return 0;
    }
    if(len>=17){		//如果用户名长度超出限制 
	MessageBox(hWnd,login_help[*curSysLang].msg_name_len,
		  login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
	return 0;
    }
		// 比较是否重名
    for(i=0;i<usernumber;i++){
	if(!strcmp(userinformation[i].name,nameBuff)){		//如果用户名已经存在
	    MessageBox(hWnd,login_help[*curSysLang].msg_name_exist,
		      login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		return 0;
	 }
    }

    return 1;
}
/*******************************************************************/
/*创建新用户时,在列表框同步显示*/
static void editname_notif_proc(HWND hwnd,int id,int nc,DWORD add_data)				
{
	int namelen;
	char buff[40];
	HWND mainwnd=GetMainWindowHandle(hwnd);
	HWND hwndListName=GetDlgItem(mainwnd,IDL_LISTNAME);
	//先在列表框显示的前面加上标号
	sprintf(buff,"%d.  ",currentid+1);

	switch(nc){
	   case EN_SETFOCUS:	// 只有在新建用户时才获得焦点
	    	// 切换输入法到 拼音输入状态
	       	SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)1);
	       	if(isIMEOpenInMotor == 0){	// 打开输入法窗口
	           ShowWindow(myIMEWindow,SW_SHOW);
		   isIMEOpenInMotor = 1;
	       	}
	     	break;	

	   case EN_CHANGE:
		//printf("Name: Change...\n");
		namelen=GetWindowTextLength(hwnd);
		GetWindowText(hwnd,&buff[4],namelen);
		//删除原来的字符串
		SendDlgItemMessage(mainwnd,IDL_LISTNAME,LB_DELETESTRING,currentid,0);
		//插入更新字符串
	        SendDlgItemMessage(mainwnd,IDL_LISTNAME,LB_ADDSTRING,0,(LPARAM)buff);		
		SendDlgItemMessage(mainwnd,IDL_LISTNAME,LB_SETCURSEL,currentid,0);
	 	break;

	   case EN_ENTER:

		//printf("Hi,password..\n");
		namelen=GetWindowTextLength(hwnd);
		GetWindowText(hwnd,buff,namelen);
		int ret = isUserNameValid(mainwnd,buff,namelen);
		if(!ret){	// 用户名非法
		    // 删除列表项目
		    SendDlgItemMessage(mainwnd,IDL_LISTNAME,LB_DELETESTRING,currentid,0);
		    createOneUser(mainwnd);	// 重新初始化输入
		    break;
		}
		// 用户名确认成功输入
		strcpy(userinformation[currentid].name,buff);
		// 进入密码设置对话框
		registerStatus = 2 ;
		EnterPasswordDialog(mainwnd);		
		if(registerStatus == 4){
		    //printf("usernumber=%d id=%d\n",usernumber,currentid);
		    usernumber++;			// 总用户数增加
		    SetFocusChild(hwndListName);	// Put it here better
		    createUserPrograms(mainwnd);
		    saveUserConfigFile(0);
		    initRegisterDialog(mainwnd);  
		}
		break;
	}
}

/*************************************************************/
/*列表框选项变化时,在编辑框显示*/
static void listname_notif_proc(HWND hwnd,int id,int nc,DWORD add_data)
{
	int index1;
	char buffer[20];
	HWND mainhwnd=GetMainWindowHandle(hwnd);
	switch(nc){
	   case LBN_SETFOCUS:
	       	//SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);
	       	if(isIMEOpenInMotor == 1){	// 打开输入法窗口
	           ShowWindow(myIMEWindow,SW_HIDE);
		   isIMEOpenInMotor = 0;
	       	}
		break;
	   case LBN_SELCHANGE:	//当选中项发生变化时,更新编辑框的内容
	      	index1=SendMessage(hwnd,LB_GETCURSEL,0,0L);	
		SendMessage(hwnd,LB_GETTEXT,index1,(LPARAM)buffer);
		if((index1==0) || (index1==1)){
		   SetDlgItemText(mainhwnd,IDC_EDITNAME,userinformation[index1].name);
		}
		else {
		   SetDlgItemText(mainhwnd,IDC_EDITNAME,&buffer[4]);
		}
		break;
	   case LBN_ENTER:	
		index1=SendMessage(hwnd,LB_GETCURSEL,0,0L);	
		SendMessage(hwnd,LB_GETTEXT,index1,(LPARAM)buffer);
		currentid=index1;
		currentuser = &userinformation[currentid];		// 初始化当前用户指针
		
		if((index1 == 0 ) ||(index1 == 1) || (index1 == createUserFlag)){	
								// 刚新建的用户不要密码
			registerStatus = 4;	// 不用验证密码,所以直接人为密码正确	
		}else {
			registerStatus = 0;  
			EnterPasswordDialog(mainhwnd);	// 密码界面
		}

		if(registerStatus == 4){
			SendMessage(mainhwnd,MSG_COMMAND,IDCANCEL,0);	// 退出,进入文件列表
		}
		break;			
	}
	
}

 
/************************************************/
/*用户登录界面*/
static int loadRegisterBkgnd(void)
{
    int langtype = *curSysLang;
    printf("loadRegisterBkgnd...\n");
    switch(langtype){	// 中文
	case 0:
	    if (LoadBitmap (HDC_SCREEN, &registerNewBkgnd, "./res/inputUserName.jpg"))
         	return 1;
    	    if (LoadBitmap (HDC_SCREEN, &registerSelectBkgnd, "./res/loginSelectUser.jpg"))
        	return 1;
	    break;
	case 1:
	    if (LoadBitmap (HDC_SCREEN, &registerNewBkgnd, "./res/inputUserName_en.jpg"))
         	return 1;
    	    if (LoadBitmap (HDC_SCREEN, &registerSelectBkgnd, "./res/loginSelectUser_en.jpg"))
        	return 1;
	    break;
	default:
	    if (LoadBitmap (HDC_SCREEN, &registerNewBkgnd, "./res/inputUserName.jpg"))
         	return 1;
    	    if (LoadBitmap (HDC_SCREEN, &registerSelectBkgnd, "./res/loginSelectUser.jpg"))
        	return 1;
	    break;    
    }
    return 0;

}
static int registerDialogBoxProc (HWND hDlg, int message, WPARAM wParam, LPARAM lParam)		
{	char buff[40];
	int cursel;
	HWND hWnd;
	HWND hwndName=GetDlgItem(hDlg,IDC_EDITNAME);
	HWND hwndClock=GetDlgItem(hDlg,IDC_TIME);
	HWND hwndListName=GetDlgItem(hDlg,IDL_LISTNAME);

    switch (message) 
    {
    case MSG_INITDIALOG:
	SetNotificationCallback(hwndName,editname_notif_proc);
	SetNotificationCallback(hwndListName,listname_notif_proc);
	initRegisterDialog(hDlg);		//读出用户名,显示在列表框中
	createUserFlag = 0;
	SetTimer(hDlg,IDC_TIMER,50);	// 时钟显示

	//SetWindowFont(hwndClock,userFont24);
	SetWindowElementColorEx(hwndClock,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 255, 255)); //White

	SetFocusChild(hwndListName);
        return 1;
        
    case MSG_ERASEBKGND:
        {   
	   HDC hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            }            
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
            }
    
     	    // Load background and other pics
            if(registerStatus == 1){
		FillBoxWithBitmap (hdc, 0, 0, 0, 0, &registerNewBkgnd); 
	    } else {
		FillBoxWithBitmap (hdc, 0, 0, 0, 0, &registerSelectBkgnd); 
   	    }	

            if (fGetDC)
                ReleaseDC (hdc);
	
	return 0;	
	}
    case MSG_TIMER:
	{
	//SetDlgItemText(hDlg,IDC_TIME,convertSysTime(buff));
	SetWindowText(hwndClock,convertSysTime(buff));
	break;
	}
    case MSG_KEYDOWN:		
	switch(LOWORD(wParam)){
	    case SCANCODE_F1:		//F1,新建用户
		 if(registerStatus > 0){	// 正在创建用户或者删除用户...
		    break;
		 }
		 registerStatus = 1;
		 InvalidateRect(hDlg,NULL,TRUE);	// 引发窗口重绘
		 createOneUser(hDlg);
       		 break;

	    case SCANCODE_F2:		// F2 :删除用户
		 if(registerStatus > 0){	// 正在创建用户或者删除用户...
		    break;
		 }
		 registerStatus = 6;		// 状态标志
		 cursel=SendDlgItemMessage(hDlg,IDL_LISTNAME,LB_GETCURSEL,0,0L);
		 currentid = cursel;		// 更新全局变量
		 deleteOneUser(hDlg);	// 删除用户
		 cursel=SendDlgItemMessage(hDlg,IDL_LISTNAME,LB_SETCURSEL,0,0L);
		 cursel=SendDlgItemMessage(hDlg,IDL_LISTNAME,LBN_SELCHANGE,0,0L);
       		 break;

	    case SCANCODE_F3:		//F3被按下,选择一个用户登录
		 if(registerStatus > 0){	// 正在创建用户或者删除用户...
		    break;
		 }
		 SetFocusChild(hwndListName);
		 SendMessage(hwndListName,MSG_KEYDOWN,SCANCODE_ENTER,0L);
		 break;
	   
   	    case SCANCODE_F4:
		 EnterConfigureCommon(hDlg);
		 loadRegisterBkgnd();
		 InvalidateRect(hDlg,NULL,TRUE);	// 引发窗口重绘
        	 break;
   	    case SCANCODE_CANCEL:	// 取消,刷新界面
		 SetFocusChild(hwndListName);
		 resetRegisterDialog(hDlg);
		 InvalidateRect(hDlg,NULL,TRUE);	// 引发窗口重绘
		 SendMessage(hwndListName,LB_SETCURSEL,0,0);
        	 break;
		
	    case SCANCODE_HELP:		// HELP
		hWnd=GetFocusChild(hDlg);	//焦点在不同的输入框时有不同的提示
		if(hWnd == hwndName){
		     MessageBox(hDlg,login_help[*curSysLang].help_t9,
		           login_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		}else {
		     MessageBox(hDlg,login_help[*curSysLang].help_login_main,
		           login_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		}

        	 break;
	}

     case MSG_COMMAND:
	    switch(wParam){
		case IDCANCEL:
	    	    KillTimer(hDlg,IDC_TIMER);
		    EndDialog (hDlg, wParam);
		    break;
	    }
	 break;
	
     }
     return DefaultDialogProc (hDlg, message, wParam, lParam);
}


static void registerDialogBox (HWND hWnd)
{	
   DlgBoxregister.controls = Ctrlregister;
   DialogBoxIndirectParam (&DlgBoxregister, hWnd, registerDialogBoxProc, 0L);

}
/*登录界面*/
int EnterRegisterDialog(void)
{	
    printf("Enter Register Dialog...\n");

    if(loadRegisterBkgnd())
	return(1);
    registerDialogBox (HWND_DESKTOP);	// Should not return except receive CLOSE MSG
    UnloadBitmap (&registerSelectBkgnd);	// unload background
    UnloadBitmap (&registerNewBkgnd);		// unload background  

    // 进入程序列表界面;
    //printf("Enter Filelist.currentid=%d \n",currentid);
    if(currentid<8){
	EnterProListDialog();	        //文件对话框
    }	
  
    return 0;
}

/**********************密码编辑框的消息循环*************************/
/*	Local Function For Password Check
 */
static int checkLoginPassword(HWND hwnd,char *pswBuf)
{  
     int id = currentid;
     if(!strcmp(pswBuf,userinformation[id].password)){	// 正确
	return(1);
     }
     else{
	MessageBox(hwnd,login_help[*curSysLang].msg_psw_wrong,
		  login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
     }
     return 0;
}

static char pswtemp[10];

static int checkCreateUserPassword(HWND hwnd,char *pswBuf)
{
     int id = currentid;
     if(!strcmp(pswBuf,pswtemp)){	// 两次输入的密码相比较
	strcpy(userinformation[id].password,pswBuf);
	return(1);
     }
     else{
	MessageBox(hwnd,login_help[*curSysLang].msg_psws_diff,
		  login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
     }
     return 0;

}

static int checkSuperPassword(HWND hwnd,char *pswBuf)
{  
     if(!strcmp(pswBuf,userinformation[0].password)){	// 正确
	return(1);
     }
     else{
	MessageBox(hwnd,login_help[*curSysLang].msg_psw_wrong,
		  login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
     }
     return 0;
}

static void password_notif_proc(HWND hwnd,int id,int nc,DWORD add_data)
{	
	int ret;
	int pswlen;
	char buff[20];
	HWND mainhwnd=GetMainWindowHandle(hwnd);
	
	switch(nc){
	    case EN_SETFOCUS:
	    	SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);	// 数字输入
	     	if(isIMEOpenInMotor == 1){
	            ShowWindow(myIMEWindow,SW_HIDE);
		    isIMEOpenInMotor = 0;
	    	}
	    	break;	
	    case EN_ENTER:
		pswlen=GetWindowTextLength(hwnd);
		GetWindowText(hwnd,buff,pswlen);
		//printf("PSW=%s registerStatus=%d\n",buff,registerStatus);

		switch(registerStatus){
		    case 0:	// 正常登录,密码验证
			ret = checkLoginPassword(mainhwnd,buff);
			if(ret){
			    registerStatus = 4;
			}else{
			    SetWindowText(hwnd,"");	// clear
			    registerStatus = 0;	//  密码错误
			}
			break;
		    case 2:	// 新建用户时第一此输入密码,保存到缓从区
			if((pswlen>10) || (pswlen==0)){
		   	   MessageBox(hwnd,login_help[*curSysLang].msg_psw_len,
		  	 	login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
			   SetWindowText(hwnd,"");	// clear
			   break;
			}
			strcpy(pswtemp,buff);
     			SetWindowText(hwnd,"");	// clear
			registerStatus = 3;	// 须再次输入
			InvalidateRect(mainhwnd,NULL,TRUE);	// 引发窗口重绘
			break;
		    case 3:	// 新建用户时第二此输入密码,进行比较
			if((pswlen>10) || (pswlen==0)){
		   	   MessageBox(hwnd,login_help[*curSysLang].msg_psw_len,
		  	 	login_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
			   SetWindowText(hwnd,"");	// clear
			   break;
			}
			ret = checkCreateUserPassword(mainhwnd,buff);
			if(ret){
			    registerStatus = 4;
			}else{
			    SetWindowText(hwnd,"");	// clear
			    registerStatus = 2;	//  密码错误,重新输入
			    InvalidateRect(mainhwnd,NULL,TRUE);	// 引发窗口重绘
			}
			break;
		    case 7:	// 验证超级密码
			ret = checkSuperPassword(mainhwnd,buff);
			if(ret){
			    registerStatus = 8;	// 使用状态4也可以
			}else{
			    SetWindowText(hwnd,"");	// clear
			    registerStatus = 7;	//  密码错误
			}
			break;
		}

		if((registerStatus == 4) || (registerStatus == 8)){
		   SendMessage(mainhwnd,MSG_COMMAND,IDCANCEL,0);	// 退出,进入用户界面
		}
		break;
	}
	
}

static int loadPasswordBkgnd(int pswStatus)
{
    int langtype = *curSysLang;
    switch(pswStatus){
	case 0:		// 正常登录
	case 2:		// 创建用户
	    switch(langtype){
	      	case 1:
		    if (LoadBitmap (HDC_SCREEN, &passwordBkgnd, "./res/password_en.jpg"))
            		return 1;
		    if(pswStatus == 2){	// 创建用户时需要两次输入密码,界面不一样
			if (LoadBitmap (HDC_SCREEN, &passwordAgainBkgnd, "./res/passwordAgain_en.jpg"))
            		    return 1;
			isPSWAgainBkgndLoaded = 1;
		    }
		    break;
		case 0:
		default:
		    if (LoadBitmap (HDC_SCREEN, &passwordBkgnd, "./res/password.jpg"))
            		return 1;
		    if(pswStatus == 2){
			if (LoadBitmap (HDC_SCREEN, &passwordAgainBkgnd, "./res/passwordAgain.jpg"))
            		    return 1;
			isPSWAgainBkgndLoaded = 1;
		    }
		    break;
	    }
	    break;

	case 7:		// 删除用户
	    switch(langtype){
	      	case 1:	// 英文
		    if (LoadBitmap (HDC_SCREEN, &passwordBkgnd, "./res/superPassword_en.jpg"))
            		return 1;
		    break;
		case 0:	// 中文
		default:
		    if (LoadBitmap (HDC_SCREEN, &passwordBkgnd, "./res/superPassword.jpg"))
            		return 1;
		    break;
	    }
	    break;
    }
    return(0);
}
static int refreshPasswordBkgnd(HDC hdc,int status)
{
    if(status == 3){		// 再次输入密码
	FillBoxWithBitmap (hdc, 0, 0, 0, 0, &passwordAgainBkgnd);
    } else {		// 输入密码和输入超级密码
	FillBoxWithBitmap (hdc, 0, 0, 0, 0, &passwordBkgnd);
    }
    
    return 0;

}
static int passwordDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)		
{	
    HWND hwndPSW=GetDlgItem(hDlg,IDC_PASSWORD);

    switch (message) 
	{
    case MSG_INITDIALOG:
	SetNotificationCallback(hwndPSW,password_notif_proc);
	SetFocusChild(hwndPSW);
        return 1;
        
    case MSG_ERASEBKGND:
        { //  printf("erasebkgnd\n");
	   HDC hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            		}         
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
           	}

	    refreshPasswordBkgnd(hdc,registerStatus);         

            if (fGetDC)
                ReleaseDC (hdc);
	
	return 0;	
	}

     case MSG_KEYDOWN:				
	 switch(LOWORD(wParam)){
		case SCANCODE_CANCEL:	// 取消,返回用户界面
		    registerStatus = 0;
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
		
		case SCANCODE_HELP:	// 帮助
		    MessageBox(hDlg,login_help[*curSysLang].help_psw,
		  	 login_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		    break;
	}
		
    case MSG_COMMAND:
	switch(wParam){
	case IDCANCEL:
		EndDialog (hDlg, wParam);
		break;
	}	
	break;
    }
     return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void passwordDialogBox (HWND hWnd)
{
   DlgBoxpassword.controls = Ctrlpassword;
   DialogBoxIndirectParam (&DlgBoxpassword, hWnd,passwordDialogBoxProc, 0L);

}

/*   密码界面
 *   处理: 新建用户时的两次密码输入
 *	  登录时的密码确认输入
 *	   删除用户时的超级密码输入
 */
static int EnterPasswordDialog(HWND hWnd)
{
    isPSWAgainBkgndLoaded = 0;	
    printf("EnterPasswordDialog...\n");
    if(loadPasswordBkgnd(registerStatus))
	return(1);
    passwordDialogBox(hWnd);		// Should not return except receive CLOSE MSG
    UnloadBitmap (&passwordBkgnd);	// unload background
    if(isPSWAgainBkgndLoaded){
	UnloadBitmap (&passwordAgainBkgnd);	// unload background
	isPSWAgainBkgndLoaded = 0;
    }

    return 0;
}

