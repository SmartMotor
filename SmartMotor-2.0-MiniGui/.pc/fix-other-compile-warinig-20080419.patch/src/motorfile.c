/*  ./src/motorfile.c
 *
 * Version:
 *	V0.1	Quan Dongxiao
 *	V0.2	Zeng Xianwei @ 2005-12-28
 *		Rewrite Most of the code;	
 *	V0.3	Zeng Xianwei @ 2006-01-17
 *
 * Copyright Reserved
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <errno.h>


#include <minigui/common.h>
#include <minigui/minigui.h>
#include <minigui/gdi.h>
#include <minigui/window.h>
#include <minigui/control.h>

//#include "help_box.h"
#include "bkgndEdit.h"
#include "motor.h"
#include "userprogram.h"
#include "motorfile.h"
#include "motorregister.h"
#include "configure.h"
#include "help.h"


static BITMAP proListBkgnd;
//static BITMAP functionButtons;	// F1-F8

static int lastProFile,curProFile;
static int enterThisDialogFlag;		// Refresh Window only once
static int proListArrayFlag;		// List ways: 0 by name, 1 by time

static int proListArrayID[99];	
static struct programe_link_t *proLinkHead;	// Link head pointer

static char filepath[30];	/* "./programe/username/99.pro" */

static int proListExitFlag = 0;		// 0: enter next window, 1: back to login window

extern void onefile_new(const char*filename,int i);

struct filelist_help_t{
	unsigned char * caption_help;
	unsigned char * caption_waring;
	unsigned char * msg_delpro_confirm;
	unsigned char * msg_delpro_ok;
	unsigned char * msg_enterpro_confirm;
	unsigned char * msg_prolist_header;
	unsigned char * msg_prolist_null;
	unsigned char * help_prolist;
};

static struct filelist_help_t filelist_help[SYSLANG_NO] = 
{
    {	caption_help_cn,
	caption_waring_cn,
	msg_delpro_confirm_cn,
	msg_delpro_ok_cn,
	msg_enterpro_confirm_cn,
	msg_prolist_header_cn,
	msg_prolist_null_cn,
	help_prolist_cn
   },

   {	caption_help_en,
	caption_waring_en,
	msg_delpro_confirm_en,
	msg_delpro_ok_en,
	msg_enterpro_confirm_en,
	msg_prolist_header_en,
	msg_prolist_null_en,
	help_prolist_en
   }
};

/**************************************************************/
static DLGTEMPLATE DlgBoxProList =
{
    WS_BORDER ,
    WS_EX_NONE,
    0, 0, 640, 480,
    "",
    0, 0,
    4, NULL,
    0
};

static CTRLDATA CtrlProList[] =
{ 
	{
        CTRL_STATIC,
        WS_VISIBLE | SS_CENTER,
        460, 5, 180, 30,
        IDC_TIME,
        NULL,
        0,
	WS_EX_TRANSPARENT
    	},
	{
        CTRL_SLEDIT,
        WS_VISIBLE | ES_READONLY , 
        182, 35, 120, 28, 
        IDC_RECENTPROGRAME, 
      	NULL,
        0,
	WS_EX_TRANSPARENT
   	 },
	{
        CTRL_SLEDIT,
        WS_VISIBLE | ES_READONLY|WS_BORDER, 
        60, 70, 450, 25, 
        IDC_PROGRAME_HEAD, 
      	NULL,
        0
   	 },
	{
	CTRL_LISTBOX,
	WS_VISIBLE|WS_BORDER| WS_VSCROLL|LBS_NOTIFY|WS_TABSTOP,
	60, 94, 450, 275,
        IDL_FILE,
        "",
        0
    }
};

/* load specified program file
 *
 * Input:
 *   id: file id
 *   pFileData: file data pointer
 * Output:
 *   status: 0: OK, else error
 *
 */
int loadProgramFile(int id, struct fromProListToPro_t *pFileData)
{
	char pathname[48],name[8];
	FILE *fp;
	int ret;

	strcpy(pathname,"./programe/");
	strcat(pathname,currentuser->name);
	strcat(pathname,"/");
	sprintf(name,"%d.pro",id);
	strcat(pathname, name);

        fp = fopen(pathname,"rb");
	if(fp == NULL){
		printf("Open program file %s error.\n",pathname);
		return (-1);
	}

	ret = fread(&(pFileData->proHeader),
		    sizeof(struct userProFileHeader_t), 1, fp);
	if (ret <= 0 ){
		printf("Read program file %s error.\n",pathname);
		fclose(fp);
		return -1;
	}
	fclose(fp);

	pFileData->isFileNew = pFileData->proHeader.isnull;
	strcpy(pFileData->filePathname, pathname);

	printf("Load file: %s OK.\n",pathname);
	return 0;
}


/*   Read All files, store in link
 */
static struct programe_link_t *
loadProgramFiles(void)
{
     char pathname[30],name[8];
     int index=0;
     FILE *fp;
     struct programe_link_t *p1,*p2;

     for(index=1;index<100;index++)
     {
	strcpy(pathname,filepath);
	sprintf(name,"%d.pro",index);
	strcat(pathname,name);
        fp = fopen(pathname,"rb");
	if(fp == NULL){
	    perror("fopen error when loadProgramFiles.\n");
	}
	p1=(struct program_link_t *)malloc(sizeof(struct programe_link_t));
	fread(&(p1->prog),sizeof(struct userProFileHeader_t),1,fp);

	if(proLinkHead == NULL){
	    proLinkHead = p1; 
	    p2 = p1;
	} else {
	    p2->next = p1;
	}
	p2 = p1;

	fclose(fp);
     }
     p2->next = NULL;	// Last node set to null

     return(proLinkHead);	   
}

static struct programe_link_t * getProLinkItem(struct programe_link_t *pHead,int index)
{
    int id;
    struct programe_link_t *pPro = pHead;
    for(id=1;id<index;id++){
	   pPro = pPro->next; 
    }
    return(pPro);
}

/*   list by date
 */
static int arrayProLinkByDate(struct programe_link_t *pHead)
{
    int id,ret;
    int index=1,lest=1;
    int tmpid[99];
    int tmp;
    int nFile=0;	// non-empty files number
    struct programe_link_t *pPro = pHead;
    struct programe_link_t *p1,*p2;
    for(index=0;index<99;index++){
	proListArrayID[index] = 0;
	tmpid[index] = index+1;
    }
    index = 1;
    while(pPro != NULL){// find non-empty file
	if(pPro->prog.isnull == 0){
	    proListArrayID[nFile] = index;
	    tmpid[index-1] = 0;
	    nFile++;
	}
	index++; 
	pPro = pPro->next;   
    }
    // List them
    index=0;
    while(proListArrayID[index] != 0){
	p1 = getProLinkItem(pHead,proListArrayID[index]);
	lest = 1;
   	while(proListArrayID[index+lest] != 0){
	   p2 = getProLinkItem(pHead,proListArrayID[index+lest]);
	   ret = strcmp(p1->prog.latestDate,p2->prog.latestDate);
	   if(ret < 0){	// p1 < p2
		tmp = proListArrayID[index];
		proListArrayID[index] = proListArrayID[index+lest];
		proListArrayID[index+lest] = tmp;

		p1 = getProLinkItem(pHead,proListArrayID[index]);
	   }
	   lest++;
        }
	index++;
    }

    // fill other proListArrayID
    id = nFile;
    for(index=0;index<99;index++){
	if(tmpid[index] != 0){
	    proListArrayID[id++] = tmpid[index];
        }
    }
    return 0; 
}

/*   Refresh list
 *   flag:   0 by name  1  by date
 */
static int refreshProgramList(HWND hwnd,int curSel,int flag)
{
    int index=1;
    int id;
    float firstCut;
    char dispBuf[60];
    struct programe_link_t *pPro = proLinkHead;
    HWND hwndFlieList=GetDlgItem(hwnd,IDL_FILE);
    
    SendDlgItemMessage(hwnd,IDL_FILE,LB_RESETCONTENT,0,0);

    switch(flag){
    case 0:
    	while(pPro != NULL){
	   if(pPro->prog.isnull){
	      sprintf(dispBuf,"  %4d    %s ",pPro->prog.id,filelist_help[*curSysLang].msg_prolist_null);
	   } 
	   else{
	     firstCut = (float)((pPro->prog.first & 0x00ffffff) / 100.0);
	     if(firstCut < 5){	// firstCut == 0.0
		sprintf(dispBuf,"  %4d    %s ",pPro->prog.id,filelist_help[*curSysLang].msg_prolist_null);
	     } else {
	     	sprintf(dispBuf,"  %4d     %-10s             ",pPro->prog.id,pPro->prog.introduction);
	     	sprintf(&dispBuf[24],"%8.2f        ",firstCut);
	     	sprintf(&dispBuf[33],"%6d     %s",pPro->prog.usedTimes,pPro->prog.latestDate);
	     }
	   }
  	   SendDlgItemMessage(hwnd,IDL_FILE,LB_ADDSTRING,0,(LPARAM)dispBuf);
	   pPro = pPro->next;
	   index++;
        }
        break;
    case 1:
	arrayProLinkByDate(proLinkHead);
 	for(index=0;index<99;index++){
	    pPro = proLinkHead;
	    for(id=1;id<proListArrayID[index];id++){
		pPro = pPro->next;
	    }
 	    if(pPro->prog.isnull){
	      sprintf(dispBuf,"  %4d    %s ",pPro->prog.id,filelist_help[*curSysLang].msg_prolist_null);
	    } 
	    else{
	     firstCut = (float)((pPro->prog.first & 0x00ffffff) / 100.0);
	     sprintf(dispBuf,"  %4d     %-10s             ",pPro->prog.id,pPro->prog.introduction);
	     sprintf(&dispBuf[24],"%8.2f        ",firstCut);
	     sprintf(&dispBuf[33],"%6d     %s",pPro->prog.usedTimes,pPro->prog.latestDate);
	    }
  	    SendDlgItemMessage(hwnd,IDL_FILE,LB_ADDSTRING,0,(LPARAM)dispBuf);
        }	
	break;
    }
    SendMessage(hwndFlieList,LB_SETCURSEL,curSel,0);

    return 0;
}

/*   refresh list area
 */
static int refreshProgramListArea(HWND hwnd,int curSel,int lastSel)
{
    char dispBuf[60];
    HWND hwndFlieList=GetDlgItem(hwnd,IDL_FILE);

    if(curSel != lastSel){
	SendMessage(hwndFlieList,LB_GETTEXT,lastSel,(LPARAM)dispBuf);
	dispBuf[2]=' ';
	SendMessage(hwndFlieList,LB_DELETESTRING,lastSel,0);
	SendMessage(hwndFlieList,LB_INSERTSTRING,lastSel,(LPARAM)dispBuf);
	SendMessage(hwndFlieList,LB_GETTEXT,curSel,(LPARAM)dispBuf);
	dispBuf[2]='>';
	SendMessage(hwndFlieList,LB_DELETESTRING,curSel,0);
	SendMessage(hwndFlieList,LB_INSERTSTRING,curSel,(LPARAM)dispBuf);

    }

    SendMessage(hwndFlieList,LB_SETCURSEL,curSel,0);

    return 0;
}

/*  only delete data in this file, the file is not deleted
 */
static int deleteOneProFile(HWND hwnd,int curSel)
{
     int ret;
     char pathname[30],name[8];
     int id = curSel+1;
     struct programe_link_t *pPro = proLinkHead;

    ret=MessageBox(hwnd,filelist_help[*curSysLang].msg_delpro_confirm,
		  filelist_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
    if(ret == IDOK){
	// update file
	strcpy(pathname,filepath);
	sprintf(name,"%d.pro",id);
	strcat(pathname,name);
	onefile_new(pathname,id);	
	// delete content in list
	if(proListArrayFlag == 0){	// list by name
	    pPro = getProLinkItem(proLinkHead,curSel+1);
	} else {
	    pPro = getProLinkItem(proLinkHead,proListArrayID[curSel]);
	}	
	pPro->prog.isnull = 1;
	pPro->prog.introduction[0] = '\0';
	pPro->prog.latestDate[0] = '\0';
	pPro->prog.usedTimes = 0;

 	refreshProgramList(hwnd,curSel,0);   
    }
    // do nothing!

    return id;
}

/*   initiallize: read files and dispaly them
 *
 */
static void initProList(HWND hwnd)
{
    int index;
    proLinkHead = NULL;
    lastProFile = 0;
    curProFile = 0;
    // file path
    strcpy(filepath,"./programe/");
    strcat(filepath,currentuser->name);
    strcat(filepath,"/");
    loadProgramFiles();
    refreshProgramList(hwnd,0,0); 

    for(index=0;index<99;index++){
    	proListArrayID[index] = 0;     
    }
}


/*********************************************************/
/* List notify
 */
static void filelist_notif_proc(HWND hwnd,int id,int nc,DWORD add_data)
{
    int ret;
    int cursel;
    HWND mainwnd=GetMainWindowHandle(hwnd);
    struct programe_link_t *pPro;
    char filename[8];

    switch(nc){
    case LBN_SETFOCUS:
	if(isIMEOpenInMotor == 1){		// Close IME
	   ShowWindow(myIMEWindow,SW_HIDE);
	   isIMEOpenInMotor = 0;
	}
	break;

    case LBN_ENTER:
	// Delete the message at 2006-08-22
	//ret=MessageBox(mainwnd,filelist_help[*curSysLang].msg_enterpro_confirm,
	//	  filelist_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
	//if(ret==IDOK)
	//{
		//printf("ok!\n");
	     cursel=SendMessage(hwnd,LB_GETCURSEL,0,0L);
	     if(proListArrayFlag == 0){
		pPro = getProLinkItem(proLinkHead,cursel+1);
	     } else {
		pPro = getProLinkItem(proLinkHead,proListArrayID[cursel]);
	     }	
		memcpy(&programCreate.proHeader,&(pPro->prog),
		       sizeof(struct userProFileHeader_t));
		programCreate.isFileNew=pPro->prog.isnull;
		strcpy(programCreate.filePathname,"./programe/")	;
		strcat(programCreate.filePathname,currentuser->name);
		strcat(programCreate.filePathname,"/");
		sprintf(filename,"%d.pro",pPro->prog.id);
		strcat(programCreate.filePathname,filename);

		SendMessage(mainwnd,MSG_COMMAND,IDCANCEL,0);	// Exit

	//}	// do nothing!
	break;

    case LBN_SELCHANGE:		// Dispaly ">"
	curProFile = SendMessage(hwnd,LB_GETCURSEL,0,0L);
	//refreshProgramListArea(mainwnd,curProFile,lastProFile);	// Hide this function 2006-04-03
	lastProFile = curProFile;	
	break;
    }
	
}

static int loadProListBkgnd(void)
{
    int langtype = *curSysLang;
    switch(langtype){	
	case 1:
	    if (LoadBitmap (HDC_SCREEN, &proListBkgnd, "./res/userProgramList_en.jpg"))
         	return 1;	
	    break;
	case 0:
	default:
	    if (LoadBitmap (HDC_SCREEN, &proListBkgnd, "./res/userProgramList.jpg"))
         	return 1;	
	    break;    
    }
    return(0);

}
static void refreshProListBkgnd(HDC hdc,int flag)
{
    
    FillBoxWithBitmap (hdc, 0, 0, 0, 0, &proListBkgnd); 
/*    if(flag){
    	RefreshButtonArea (hdc,&functionButtons,MSG_KEYUP,90,395,64,32, 0);	// F1    
    	RefreshButtonArea (hdc,&functionButtons,MSG_KEYUP,180,395,64,32, 1);	// F2   
    	RefreshButtonArea (hdc,&functionButtons,MSG_KEYUP,265,395,64,32, 2);	// F3 
    	RefreshButtonArea (hdc,&functionButtons,MSG_KEYUP,350,395,64,32, 3);	// F4 
    	RefreshButtonArea (hdc,&functionButtons,MSG_KEYUP,435,395,64,32, 4);	// F5 
    }
*/
}

//*********************************************************//
static int fileDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)		
{
	char buff[24];
	int curSel;
	HWND hwndFlieList=GetDlgItem(hDlg,IDL_FILE);
	HWND hwndClock=GetDlgItem(hDlg,IDC_TIME);
    switch (message) 
	{
    case MSG_INITDIALOG:
	SetTimer(hDlg,IDC_TIMER,50);
	SetNotificationCallback(hwndFlieList,filelist_notif_proc);
	// head
	SetDlgItemText(hDlg,IDC_PROGRAME_HEAD,filelist_help[*curSysLang].msg_prolist_header);
	initProList(hDlg);
	enterThisDialogFlag = 1;
	proListArrayFlag = 0;
	proListExitFlag = 0;

	//SetWindowFont(hwndClock,userFont24);
	SetWindowElementColorEx(hwndClock,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 255, 255)); //White
	//SetFocusChild(hwndFlieList);
	// change to number input
	SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);	// number input
	
        return 0;
        
    case MSG_ERASEBKGND:
        {  
	   HDC hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            		}       
                    
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
           		 }
	   
           if(enterThisDialogFlag){
		refreshProListBkgnd(hdc,0);
		SetFocusChild(hwndFlieList);
		enterThisDialogFlag = 0;
           } else {
		refreshProListBkgnd(hdc,0);
	   }

            if (fGetDC)
                ReleaseDC (hdc);
	
	return 0;	
	}
    case MSG_TIMER:	
	SetDlgItemText(hDlg,IDC_TIME,convertSysTime(buff));
	break;

    case MSG_CHAR:
	if((wParam >= 0x30) && (wParam <= 0x39)){
	    curSel = (wParam - 0x30) *10;
	    SendMessage(hwndFlieList,LB_SETCURSEL,curSel,0L);	
	}
	break;

    case MSG_KEYDOWN:			
	switch(LOWORD(wParam)){
	    case SCANCODE_F1:
		SendMessage(hwndFlieList,MSG_KEYDOWN,SCANCODE_PAGEUP,0L);			
		break;
	    case SCANCODE_F2:
		SendMessage(hwndFlieList,MSG_KEYDOWN,SCANCODE_PAGEDOWN,0L);	
		break;

	    case SCANCODE_F3:
		curSel = SendMessage(hwndFlieList,LB_GETCURSEL,0,0L);
		//printf("Delete file %d.pro\n",curSel+1);
		deleteOneProFile(hDlg,curSel);
		break;
	    case SCANCODE_F4:	// list by name
		if(proListArrayFlag == 1){
		    proListArrayFlag = 0;
		    refreshProgramList(hDlg,0,proListArrayFlag); 
		}
		break;
	    case SCANCODE_F5:	// list by date
		if(proListArrayFlag == 0){
		    proListArrayFlag = 1;
		    refreshProgramList(hDlg,0,proListArrayFlag); 
		}
		break;
	    case SCANCODE_HELP:	// HELP
		MessageBox(hDlg,filelist_help[*curSysLang].help_prolist,
		  filelist_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		break;

	    case SCANCODE_CANCEL:	// back to
		proListExitFlag = 1;
		SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		break;
	break;
	}	
    case MSG_COMMAND:
	switch(wParam){
	case IDCANCEL:
		KillTimer(hDlg,IDC_TIMER);
		EndDialog (hDlg, wParam);
		break;
	}	
	break;
    }

     return DefaultDialogProc (hDlg, message, wParam, lParam);
}



static void fileDialogBox (HWND hWnd)
{ 
   DlgBoxProList.controls = CtrlProList;
   DialogBoxIndirectParam (&DlgBoxProList,HWND_DESKTOP,fileDialogBoxProc, 0L);
}

/*   program list window
 */
int EnterProListDialog(void)
{	
    if(loadProListBkgnd())
	return(1);
    printf("EnterProListDialog...\n");
    fileDialogBox(HWND_DESKTOP);		// Should not return except receive CLOSE MSG
    //printf("EnterProListDialog UnloadBitmap...\n");
    UnloadBitmap (&proListBkgnd);	
  
    if(!proListExitFlag){
    	EnterUserProgram();
    } else {
	EnterRegisterDialog(); 	// login window
    }
    return 0;
}
