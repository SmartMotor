/*
 *  motormain.h
 *
 *  Copyright 2008 @ xianweizeng@gmail.com
 *
 */
#ifndef _MOTORMAIN_H_
#define _MOTORMAIN_H_

struct startup_params_t {
	int isFlashOn;    /* 0:off 1:on */
	int whichBkgnd;   /* use which backgroud bmp */
	int lastuser;     /* last login user, currentid = lastuser */
};

extern struct startup_params_t startupParams;

int loadInitConfigFile(struct startup_params_t *pStatup_params);
int saveInitConfigFile(struct startup_params_t *pStatup_params);

#endif /* _MOTORMAIN_H_ */
