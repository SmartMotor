/*
 *  bkgnd.h
 *
 *  Copyright 2008 @ xianweizeng@gmail.com
 *
 */

#ifndef _SRC_BKGNDEDIT_H_
#define _SRC_BKGNDEDIT_H_

struct functionButton_t{
	int bx;	       /* x position */
	int by;        /* y position */
	int bw;	       /* width */
	int bh;        /* height */
	int pos;       /* position in resource file */
	int textbase;
	int textpos;   /* postion in resource file */
	int textw;     /* width */
};

/* define message number, according to minigui/windows.h */
#define MSG_RECV	0x0801

/* communication fram structure */
struct comCmdFrame_t{
	char header;    /* start flag: always "%" */
	char flag;	/* frame type */
	char addr[2];	/* frame addr */
	char data[6];	/* frame data */
	char checksum[2];  /* the checksum field */
};

/* communication command data structure */
struct comCmdData_t{
	char flag;	/* data flag*/
	int status;	/* address */
	int data;	/* data */
};

/* globle functions */
int RefreshButtonArea (HDC hdc, const BITMAP *pBitmap, int msg,int bx,int by,int bw,int bh, int pos );
int RefreshBoxArea (HDC hdc, const BITMAP *pBitmap,int bx,int by,int bw,int bh, int x ,int y );
int RefreshFunctionButton (HDC hdc, const BITMAP *pBitmap, int down,struct functionButton_t *pButton,int textflag);

int initSerialCommunication(void);
int sendCommand(char *cmd);
int recvCommand(char *recvBuf);
int isCmdValid(char *cmdbuf);
/* int getCmdFrameData(char flag,int addr,char *cmdbuf); */
/* int setCmdFrameBuf(char flag,int addr,int data,char *cmdbuf); */

int getCmdFrame(struct comCmdData_t *pData,char *cmdbuf);
int setCmdFrame(struct comCmdData_t *pData,char *cmdbuf);

int mm_to_inch(int mm);
int inch_to_mm(int mil);

#endif
