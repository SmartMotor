/*	bkgndEdit.c
 *	定义贴图函数
 * 	通信串口操作
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>		/*POSIX terminal control definition*/
#include <pthread.h>

#include <minigui/common.h>
#include <minigui/minigui.h>
#include <minigui/gdi.h>
#include <minigui/window.h>
#include <minigui/control.h>

#include "bkgndEdit.h"
#include "userprogram.h"

/*
	hdc:	句柄
	pBitmap:  位图指针
 	msg:	区分keydown或者keyup消息
  	bx:	box坐标
	by:	box坐标
	bw:	位图宽度(=box宽度)
	bh:	位图高度(=box高度)
	pos:	要使用的图片在pBitmap的位置
说明:
	该函数只实现对按钮的弹起,按下的刷图动作;
	每个按钮对应两副图.通过keydown和up来区分.
*/

int RefreshButtonArea (HDC hdc, const BITMAP *pBitmap, int msg,int bx,int by,int bw,int bh, int pos )
{
    int val=0;
    int boxx;
    //根据KEYDOWN或者KEYUP消息更新图片偏移量
    switch(msg){
	case MSG_KEYDOWN:	//load按钮按下图标
	    val = 1;
	    break;
	case MSG_KEYUP:		//load按钮弹起图标
	    val = 0;	
	    break;
    }

    boxx = (pos * 2  + val ) * bw;		
    FillBoxWithBitmapPart(hdc,bx,by,bw,bh,0,0,pBitmap,boxx,0);

    return 0;
}


int RefreshFunctionButton (HDC hdc, const BITMAP *pBitmap, int down,struct functionButton_t *pButton,int textflag)
{
    int boxx;
    int bx = pButton->bx;
    int by = pButton->by;
    int bw = pButton->bw;
    int bh = pButton->bh;

    boxx = (pButton->pos * 2  + down ) * bw;	
    FillBoxWithBitmapPart(hdc,bx,by,bw,bh,0,0,pBitmap,boxx,0);	// Fn
    if(textflag){
    	bx = bx + bw;
    	bw = pButton->textw;
    	boxx = pButton->textbase + pButton->textpos* bw;
    	FillBoxWithBitmapPart(hdc,bx,by,bw,bh,0,0,pBitmap,boxx,0);	// Fn
    }
    return 0;
}

/*
	hdc:	句柄
	pBitmap:  位图指针
  	bx:	box坐标
	by:	box坐标
	bw:	位图宽度(=box宽度)
	bh:	位图高度(=box高度)
说明:
	该BITMAP图片比要填从的box要大,因此需要指定bitmap的坐标,填  大小与box大小相同
*/


int RefreshBoxArea (HDC hdc, const BITMAP *pBitmap,int bx,int by,int bw,int bh, int x ,int y )
{
    FillBoxWithBitmapPart(hdc,bx,by,bw,bh,0,0,pBitmap,x,y); 		
    return 0;
}

/****************************************************/
/*****   Global Functions for Communication with Motor by Serial port         ****/

#define SERIALPORT	"/dev/ttyS0"
#define SERIAL_SEND	1
#define SERIAL_RECV	0

#define CMD_BUF_LEN	13
#define CMD_LEN		11
	 
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
static int comStatus;
char sendCmdBuffer[13];
char recvCmdBuffer[13];
 
/*   串口初始化函数,
 *   功能:  	完成对串口通信规则的设定,9600,8N1
 *   注意:	
 */
static int initSerialPort(void)
{
   int fd;
   struct termios serialOptions;

   // Open port
   fd = open(SERIALPORT,O_RDWR | O_NOCTTY | O_NDELAY);
   if(fd == -1){
	perror("openSerialPort: Unable to open serial port !");
   } else {
	fcntl(fd,F_SETFL,FASYNC);
   }
   
   // Configure serial port
   tcgetattr(fd,&serialOptions);		// Get current configure
   cfsetispeed(&serialOptions,B9600);	// Baudrate 9600
   cfsetospeed(&serialOptions,B9600);  
   serialOptions.c_cflag &= ~PARENB;  	// 8N1 
   serialOptions.c_cflag &= ~CSTOPB; 
   serialOptions.c_cflag &= ~CSIZE;    
   serialOptions.c_cflag |= CS8; 
   serialOptions.c_cflag &= ~CRTSCTS; 
   serialOptions.c_cflag |= (CLOCAL | CREAD);

   serialOptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);	// RAW input

   tcsetattr(fd,TCSANOW,&serialOptions);	// Set configure 
   
   close(fd);		// Close serial port

   return (fd);	
}
/*   串口通信发送函数
 *   功能:  	将用户缓冲区的数据复制到发送缓冲区
 *		并发送	
 */
int sendCommand(char *cmd)
{
     pthread_mutex_lock (&mutex);
     strcpy(sendCmdBuffer,cmd);
     comStatus = SERIAL_SEND;
     pthread_mutex_unlock (&mutex);   

     return(0);

}

/*   串口通信接收函数
 *   功能:  	将接收的数据从全局缓冲区recvCmdBuffer
 *		无冲突的复制到用户缓冲区recvBuf	
 */
int recvCommand(char *recvBuf)
{
     pthread_mutex_lock (&mutex);
     strcpy(recvBuf,recvCmdBuffer);
     pthread_mutex_unlock (&mutex);   

     return(0);

}

/*   串口通信主函数
 *   功能:  	时刻接收或者发送数据
 *   注意:	
 */
static void communicateWithMotor(void)
{
   int fd;
   int len=10;
   int status = 0;
   HWND currentWnd;
   unsigned char inbuf[13],temp;
   struct termios serialOptions;
   // Open port
   fd = open(SERIALPORT,O_RDWR | O_NOCTTY | O_NDELAY);
   if(fd == -1){
	perror("openSerialPort: Unable to open /dev/ttyS0.\n");
   }

   // Write data
   //len = write(fd,"Hello Motro!",12);		// Send

   // Read data
   memset(inbuf,0,13);
   while(1){
	pthread_mutex_lock (&mutex);
	status = comStatus;
	pthread_mutex_unlock (&mutex);

	switch(status){
	   case SERIAL_RECV:	// recv
		len = read(fd,&temp,1);
		//printf("Recv: %d %c \n",len ,temp);
   		if((len == 1)&&(temp == '%')){		// Start Flags
		   usleep(5000);
		   len = read(fd,&inbuf,CMD_LEN);
		   //printf("len=%d ",len);
		   if(len == CMD_LEN){
		       	inbuf[len] = 0;	
		       	printf("Recv:%s\r",inbuf);
		       	pthread_mutex_lock (&mutex);
		       	strcpy(recvCmdBuffer,inbuf);
		       	pthread_mutex_unlock (&mutex);
		       	currentWnd = GetActiveWindow();	// 得到当前活动窗口句柄
		       	PostMessage(currentWnd,MSG_RECV,len,0);	// 发送消息,并马上返回
		   }
		}
		break;
	   case SERIAL_SEND:
		len = strlen(sendCmdBuffer);
		printf("Send...%s %d\r",sendCmdBuffer,len);
		len = write(fd,sendCmdBuffer,len);	// Send
		pthread_mutex_lock (&mutex);
		comStatus = SERIAL_RECV;
		pthread_mutex_unlock (&mutex);
		break;	// send
	}
   }

   // Close port
   close(fd);
}

/*   串口通信初始化函数,
 *   功能:  	创建新线程
 *   注意:	只能被调用一次
 */
int initSerialCommunication(void)
{
    int ret;
    pthread_t comthread;
    printf("initSerialCommunication()...\n");
    comStatus = SERIAL_RECV;

    initSerialPort();		// 初始化通信串口

    // 创建通信线程
    ret = pthread_create(&comthread,NULL,(void *)communicateWithMotor,NULL);
    if(ret){
	perror("Create serial thread failed.");
	exit(0);
       }
    return 0;
}

/***********************************************************/
/*	命令格式:(12 Bytes total)
 *	StartByte + FlagByte + AddressBytes + DataBytes + ChecksumBytes
 *  	   1	     1		2	   6		2	(字节数)
 *	   %     W/R/L/...    35/30/...	 010000		EA	(示例)
 *  注意:
 *	所有命令帧都由'%'开头
 *	Flag字节标志该命令的操作类型
 *	Address字节指出命令地址
 *	Data字节不足6位则个高位补零	
 *	校验和字节为前面9字节('%'不参与校验和的计算)ASCII码之和,取最低两位;
 */

/*   检查命令是否有效
 */ 
int isCmdValid(char *cmdbuf)
{
   int sum=0;	
   int index;
   char tmp[2]; 
   char checksum[2];
   for(index=0;index<9;index++){
	sum = sum + cmdbuf[index];
   }
   tmp[0] = (char)(sum & 0xf);
   tmp[1] = (char)((sum >> 4 )& 0xf );
   for(index=0;index<2;index++){
	if(tmp[index] <=0x9){
	    checksum[index] = tmp[index] + 0x30;	// 0 -- 9
	} else {
	    checksum[index] = tmp[index] + 0x41 - 10;	// A -- F	
    	}
   }
   //printf("isCmdValid: checksum=%x %x sum=%x \n",checksum[1],checksum[0],sum);
   if((checksum[0] == cmdbuf[10]) && (checksum[1] == cmdbuf[9])){
  	return(1);
   }
  
   return(0);

}

/*   命令解析函数,从命令中得到有效数据
 *   参数:	flag:	命令标志
 *		addr:	命令地址
 *		cmdbuf:	接收到的命令帧
 *   返回:	有效数据
 */
/*
int getCmdFrameData(char flag,int addr,char *cmdbuf)
{
    int index;
    int data;
    char tmp[2];
    char databuf[7];
    // 判断标志
    if(flag != cmdbuf[0]){	// 标志
 	return(-1);
    }
    // 判断地址
    tmp[0] = (char)((addr & 0xf) + 0x30);	// 将十六进制数专为char
    tmp[1] = (char)(((addr >> 4 )& 0xf ) + 0x30);
    
    if((tmp[0] != cmdbuf[2]) || (tmp[1] != cmdbuf[1])){
  	return(-1);
    }
  
    strncpy(databuf,&cmdbuf[3],6);
    databuf[6] = 0;

    data = atoi(databuf);
    printf("getCmdFrameData :%s %d \n",databuf,data);
    return(data);

}
*/
/*    生成命令帧
 *   参数:	flag:	命令标志
 *		addr:	命令地址
 *		data:	命令数据
 *		cmdbuf:	接收到的命令帧
 *   返回:	成功返回1,否则返回0
 */
/*
int setCmdFrameBuf(char flag,int addr,int data,char *cmdbuf)
{
    int index,sum=0;
    char dataBuf[7];
    char checksum[2];
    char tmp[2];

    cmdbuf[0] = '%';
    cmdbuf[1] = flag;
    cmdbuf[2] = (char)(((addr >> 4 )& 0xf ) + 0x30);  
    cmdbuf[3] = (char)((addr & 0xf) + 0x30);	// 将十六进制数专为char
    
    sprintf(dataBuf,"%06d",data);		// %06d 不足6位补0
    strncpy(&cmdbuf[4],dataBuf,6);

    // 计算检验和
    for(index=1;index<10;index++){
	sum = sum + cmdbuf[index];
    }
    tmp[0] = (char)(sum & 0xf);
    tmp[1] = (char)((sum >> 4 )& 0xf );
    for(index=0;index<2;index++){
	if(tmp[index] <=0x9){
	    checksum[index] = tmp[index] + 0x30;	// 0 -- 9
	} else {
	    checksum[index] = tmp[index] + 0x41 - 10;	// A -- F	
    	}
    }
    cmdbuf[10] = checksum[1];
    cmdbuf[11] = checksum[0];
    cmdbuf[12] = 0;

    //printf("data = %s %s\n",dataBuf,cmdbuf);

    return 0;
}
*/
/***********************/
/*   命令解析函数,从命令中得到有效数据
 *   参数:	flag:	命令标志
 *		addr:	命令地址
 *		cmdbuf:	接收到的命令帧
 *   返回:	有效数据
 */
int getCmdFrame(struct comCmdData_t *pData,char *cmdbuf)
{
    int index;
    int data;
    int status = 0;
    char tmp[2];
    char databuf[7];
    // 得到标志
    pData->flag = cmdbuf[0];

    // 得到状态
    strncpy(tmp,&cmdbuf[1],2);
    if(tmp[0] <= '9' ){
	status = (tmp[0] - 0x30) * 0x10;
    } else {
	status = (tmp[0] - 0x41 + 10) * 0x10;	// A -- F	
    }
    if(tmp[1] <= '9' ){
	status += (tmp[1] - 0x30);
    } else {
	status += (tmp[1] - 0x41 + 10);	// A -- F	
    }
    pData->status = status;
    
    // 得到数据
    strncpy(databuf,&cmdbuf[3],6);
    databuf[6] = 0;
    data = atoi(databuf);
    pData->data = data;

    //printf("getCmdFrame :%c %s %d status=%x\n",cmdbuf[0],databuf,data,status);

    return(data);
}

/*    生成命令帧
 *   参数:	flag:	命令标志
 *		addr:	命令地址
 *		data:	命令数据
 *		cmdbuf:	接收到的命令帧
 *   返回:	成功返回1,否则返回0
 */
int setCmdFrame(struct comCmdData_t *pData,char *cmdbuf)
{
    int index,sum=0;
    char dataBuf[7];
    char checksum[2];
    char tmp[2];
    int status = pData->status;
    cmdbuf[0] = '%';
    cmdbuf[1] = pData->flag;
   
    if(((status >> 4) & 0x0f) <= 0x9){
	cmdbuf[2] = ((status >> 4) & 0x0f) + 0x30;		// 0 - 9
    } else {
	cmdbuf[2] = ((status >> 4) & 0x0f) + 0x41 - 10;	// A - F
    }

    if((status & 0x0f) <= 0x9){
	cmdbuf[3] = (status & 0x0f) + 0x30;
    } else {
	cmdbuf[3] = (status & 0x0f) + 0x41 - 10;
    }
    
    sprintf(dataBuf,"%06d",pData->data);		// %06d 不足6位补0
    strncpy(&cmdbuf[4],dataBuf,6);

    // 计算检验和
    for(index=1;index<10;index++){
	sum = sum + cmdbuf[index];
    }
    tmp[0] = (char)(sum & 0xf);
    tmp[1] = (char)((sum >> 4 )& 0xf );
    for(index=0;index<2;index++){
	if(tmp[index] <=0x9){
	    checksum[index] = tmp[index] + 0x30;	// 0 -- 9
	} else {
	    checksum[index] = tmp[index] + 0x41 - 10;	// A -- F	
    	}
    }
    cmdbuf[10] = checksum[1];
    cmdbuf[11] = checksum[0];
    cmdbuf[12] = 0;

    //printf("data = %s %s\n",dataBuf,cmdbuf);

    return 0;
}

// Change unit mm to mil
// 100 mil = 2.54 mm
// 1 mm = 39.37 mil
// 这里存储的都是 0.01mm精度的数据;且为整数;

// 输入0.01mm
int mm_to_inch(int mm)
{
    int inch;
    float tmp;
    tmp = mm / 254.0;
    inch = (int)((tmp * 1000) / 100);
    //printf("mm = %d -> inch = %d \n", mm,inch);
    return(inch);

}

int inch_to_mm(int inch)
{
    int mm;
    mm = (int)(((inch * 10 +5) * 254) / 100);
    //printf("inch = %d -> mm = %d \n", inch,mm);
    return(mm);

}
