/*
 * src/userprogram.c
 *
 * The work window implementation
 *
 * Copyright 2007-2008 @ xianweizeng@gmail.com
 *
 * Author:
 *   Zeng Xianwei
 *
 *  File Coding system: gb2312
 *    set-buffer-file-coding-system
 *
 * ChangeLog:
 *   2008-09-06 Next generation
 *   2008-09-21 list icon, buttons, split files
 *              this file mainly deal with the "interface"
 *              data edit functions are moved to program_edit.c
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <errno.h>
#include <time.h>

#include <minigui/common.h>
#include <minigui/minigui.h>
#include <minigui/gdi.h>
#include <minigui/window.h>
#include <minigui/control.h>

#include "userprogram.h"
#include "motorfile.h"
#include "bkgndEdit.h"
#include "motor.h"
#include "configure.h"
#include "computer.h"
#include "motorregister.h"
#include "motormain.h"
#include "help.h"
#include "rtc.h"

/*
 * controls ID definition
 */
#define IDC_PROGRAMDESC         (IDC_USER_WINDOW_START + 0)
#define IDC_PROGRAMDATASTEP     (IDC_USER_WINDOW_START + 1)
#define IDC_STATUSBAR           (IDC_USER_WINDOW_START + 2)
#define IDC_STATUSMODE          (IDC_USER_WINDOW_START + 3)
#define IDC_INPUTDESC           (IDC_USER_WINDOW_START + 4)
#define IDC_CURPOSDESC          (IDC_USER_WINDOW_START + 5)

/* buttons ID
 */
#define IDC_BUTTON_EDIT_F1      (IDC_USER_WINDOW_START + 6)    /* F1 */
#define IDC_BUTTON_EDIT_F2      (IDC_USER_WINDOW_START + 7)    /* F2 */
#define IDC_BUTTON_EDIT_F3      (IDC_USER_WINDOW_START + 8)    /* F3 */
#define IDC_BUTTON_EDIT_F4      (IDC_USER_WINDOW_START + 9)    /* F4 */
#define IDC_BUTTON_EDIT_F5      (IDC_USER_WINDOW_START + 10)   /* F5 */
#define IDC_BUTTON_EDIT_F6      (IDC_USER_WINDOW_START + 11)   /* F6 */

#define IDC_BUTTON_USERS        (IDC_USER_WINDOW_START + 12)   /* user window */
#define IDC_BUTTON_PROLIST      (IDC_USER_WINDOW_START + 13)   /* program list window */
#define IDC_BUTTON_MD_EDIT      (IDC_USER_WINDOW_START + 14)   /* Edit Mode */
#define IDC_BUTTON_MD_AUTO      (IDC_USER_WINDOW_START + 15)   /* AutoRun Mode */
#define IDC_BUTTON_MD_MANUAL    (IDC_USER_WINDOW_START + 16)   /* MannualRun Mode */
#define IDC_BUTTON_MD_TRAINING  (IDC_USER_WINDOW_START + 17)   /* Training Mode */

#define IDC_BUTTON_ENTER        (IDC_USER_WINDOW_START + 18)   /* Enter */
#define IDC_BUTTON_CLEAR        (IDC_USER_WINDOW_START + 19)   /* Clear */
#define IDC_BUTTON_RUN          (IDC_USER_WINDOW_START + 20)   /* Run */

/* controls */
#define IDC_PROGRAMCUTSLISTHEAD	(IDC_USER_WINDOW_START + 21)
#define IDC_PROGRAMNUM		(IDC_USER_WINDOW_START + 22)
#define IDC_PROGRAMNAME	   	(IDC_USER_WINDOW_START + 23)
#define IDC_INPUTCUTDATA    	(IDC_USER_WINDOW_START + 24)
#define IDL_PROGRAMCUTSLIST	(IDC_USER_WINDOW_START + 25)
#define IDC_CUTCOUNTER		(IDC_USER_WINDOW_START + 26)

/* user mode */
#define IDC_USER1_DESC		(IDC_USER_WINDOW_START + 27)
#define IDC_USER2_DESC		(IDC_USER_WINDOW_START + 28)
#define IDC_USER3_DESC		(IDC_USER_WINDOW_START + 29)
#define IDC_USER4_DESC		(IDC_USER_WINDOW_START + 30)
#define IDC_USER5_DESC		(IDC_USER_WINDOW_START + 31)
#define IDC_USER6_DESC		(IDC_USER_WINDOW_START + 32)
#define IDC_USER7_DESC		(IDC_USER_WINDOW_START + 33)
#define IDC_USER8_DESC		(IDC_USER_WINDOW_START + 34)

#define IDC_USER1_NAME		(IDC_USER_WINDOW_START + 35)
#define IDC_USER2_NAME		(IDC_USER_WINDOW_START + 36)
#define IDC_USER3_NAME		(IDC_USER_WINDOW_START + 37)
#define IDC_USER4_NAME		(IDC_USER_WINDOW_START + 38)
#define IDC_USER5_NAME		(IDC_USER_WINDOW_START + 39)
#define IDC_USER6_NAME		(IDC_USER_WINDOW_START + 40)
#define IDC_USER7_NAME		(IDC_USER_WINDOW_START + 41)
#define IDC_USER8_NAME		(IDC_USER_WINDOW_START + 42)


/*
 * Window layout definition
 */
#define CURPOS_X                230    /* current pos */
#define CURPOS_Y                75

/*  status icons layout
 */
#define STATUS_CP_X       20
#define STATUS_CP_Y       450

#define STATUS_SPK_X      75
#define STATUS_SPK_Y      450

#define STATUS_CUT_X      510
#define STATUS_CUT_Y      450

#define STATUS_PUSHER_X   565
#define STATUS_PUSHER_Y   450

#define STATUS_SHIFT_X    06
#define STATUS_SHIFT_Y    406


/* Data extra flag
 * the higher 8 bits are for data flag
 */
#define PUSHER_FLAG             0x01000000    /* pusher on  */
#define AUTOCUT_FLAG            0x02000000    /* autocut on */
#define AIR_FLAG                0x04000000    /* Air off    */
#define PRESS_FLAG              0x08000000    /* press on   */

#define AIR_ICON_BIT            0x80000000    /* on/off bit */
#define PUSHER_ICON_BIT         0x40000000
#define PRESS_ICON_BIT          0x20000000
#define AUTOCUT_ICON_BIT        0x10000000

#define AIR_ICON_SW_BIT         0x08000000    /* icon switch bit */
#define PUSHER_ICON_SW_BIT      0x04000000
#define PRESS_ICON_SW_BIT       0x02000000
#define AUTOCUT_ICON_SW_BIT     0x01000000


/* Machine state mask
 */
#define CUT_STATUS_MASK         0x0001
#define PUSHER_STATUS_MASK      0x0002
#define WHICH_LIMIT_MASK        0x0004
#define MACHINE_STOP_MASK       0x0008

/* Motor running macro
 */
#define MOTOR_FORWARD           0
#define MOTOR_BACKWARD          1
#define MOTOR_FORWARD_HS        2
#define MOTOR_BACKWARD_HS       3


/*
 *  Windon Fn button number definition
 */
#define EDIT_FN_NUM             6
#define MANUAL_FN_NUM           6
#define AUTO_FN_NUM             3
#define USER_FN_NUM             6
#define PROLIST_FN_NUM          6


static int isFileListLoaded = 0;        /* We don't want to load files everytime
					 * we enter to MD_PROLIST mode.
					 */

BITMAP userProgramEditBkgnd;            /* window background */

static BITMAP largeNumsIcon;            /* Number 0 - 9 */

/*
 * button used in Label/Average Edit window
 */
static BITMAP lableEditBkgnd;
static BITMAP btBmpLableFn[2];          /* F1: done F2: exit */

static LISTBOXITEM_ICON myicons;
static BITMAP flagicon[5];

static BITMAP cutPosIcon, cutCounterIcon;  /* cut pos and cut counter */

/*
 * button bitmaps
 */
static BITMAP btBmpEditFn[EDIT_FN_NUM];     /* Edit Mode: F1 - F6*/
static BITMAP btBmpManualFn[MANUAL_FN_NUM]; /* Manual Mode: F1 - F6 */
static BITMAP btBmpAutoFn[AUTO_FN_NUM];     /* Auto Mode: F1 - F3 */
static BITMAP btBmpFileFn[PROLIST_FN_NUM];  /* ProFile Mode: F1 - F6 */
static BITMAP btBmpUserFn[USER_FN_NUM];     /* User Mode: F1 - F6 */


static BITMAP btBmpCommonLeft[6];           /* common 6 icons in all modes */
					    /* on the left of screen */
static BITMAP btBmpEnter, btBmpClear, btBmpRun;


/*
 * status icons, every status has two state
 */
static BITMAP bmpStatusCutPusher[2];  /* 0: on  1: off */
static BITMAP bmpStatusSperker[2];    /* 0: off  1: on */
static BITMAP bmpStatusCut[2];        /* 1: low  0: high */
static BITMAP bmpStatusPusher[2];     /* 1: low  0: high */

static BITMAP bmpShiftFlag;

static BITMAP bmpTrackBar_button, bmpTrackBar_top;   /* track bar */


/* we want to show/hide this button */
static HWND  hwndButtonEditFn[6];
static HWND  hwndButtonCommon[6];

/* global valiables
 */
userProgramEditData programEdit;                /* program data edit */
struct fromProListToPro_t programCreate;        /* program create/old status */
static userProgramRunStatus proRunStatus;       /* program run status */

/* This two are used to edit the FILE ATTR */
static userProgramEditData programFileAttrData;
static struct fromProListToPro_t programFileAttr;   /* used to show/modify file attr
						     * programCreate holds the current edit file
						     * in data listbox.
						     */

/* For User mode */
static struct user_manage_t userModeStatus;     /* user edit/select/modify */


static int current_highlight_user;

/* exit flag */
static enum {
	STAY_HERE = 0,
	TO_PROLIST_WINDOW,
	TO_LOGIN_WINDOW
} userProExitFlag;

/* cmd data buffer */
static struct comCmdData_t cmdData;


struct userpro_help_t{
	unsigned char * caption_help;
	unsigned char * caption_waring;
	unsigned char * msg_cutdata_overflow;
	unsigned char * msg_save_pro_confirm;
	unsigned char * msg_save_pro_ok;
	unsigned char * msg_delcutdata_confirm;
	unsigned char * msg_delalldata_confirm;
	unsigned char * msg_proname_change;
	unsigned char * msg_just_confirm;
	unsigned char * msg_cutslist_header;
	unsigned char * msg_cutslist_header_inch;
	unsigned char * help_userpro_name;
	unsigned char * help_userpro_cuts;
	unsigned char * help_t9;

};

static struct userpro_help_t userpro_help[SYSLANG_NO] =
{
	{       caption_help_cn,
		caption_waring_cn,
		msg_cutdata_overflow_cn,
		msg_save_pro_confirm_cn,
		msg_save_pro_ok_cn,
		msg_delcutdata_confirm_cn,
		msg_delalldata_confirm_cn,
		msg_proname_change_cn,
		msg_just_confirm_cn,
		msg_cutslist_header_cn,
		msg_cutslist_header_inch_cn,
		help_userpro_name_cn,
		help_userpro_cuts_cn,
		help_t9_cn
	},

	{       caption_help_en,
		caption_waring_en,
		msg_cutdata_overflow_en,
		msg_save_pro_confirm_en,
		msg_save_pro_ok_en,
		msg_delcutdata_confirm_en,
		msg_delalldata_confirm_en,
		msg_proname_change_en,
		msg_just_confirm_en,
		msg_cutslist_header_en,
		msg_cutslist_header_inch_en,
		help_userpro_name_en,
		help_userpro_cuts_en,
		help_t9_en
	}
};

static char strProgramDesc[] = "程序";
static char strProgramStep[] = "步骤";
static char strInputDataDesc[] = "输入尺寸";
static char strCurrentPosDesc[] = "当前实际尺寸";

static char strMachineMode[MD_MAX][6] = {{"编程"},{"自动"},{"手动"},{"自刀"},{"程序"},{"用户"},{"示教"}};

static char strModeManual[SYSLANG_NO][MD_MAX][48] =
{
	/*  0: Chinese */
	{
		{"输入尺寸,编缉程序"},
		{"按运行键自动运行程序"},
		{"输入尺寸,按运行键手动运行程序"},
		{"按运行键自动运行程序"},
		{"程序列表,选择/删除/修改程序"},
		{"用户列表,选择/删除/增加用户"},
		{"输入尺寸,可以示教编缉程序"},
	},

	/* 1: English */
	{
		{"Input data to edit this program"},
		{"Press RUN to run it automatically"},
		{"Press RUN to run it manually"},
		{"Press RUN to run it automatically"},
		{"Program list, select/delete/modify program"},
		{"User list, select/delete/create user"},
		{"Trainning mode to edit this program"},
	}
};

static DLGTEMPLATE UserProgromDlgInitProgress =
{
	WS_BORDER,
	WS_EX_NONE,
	0, 0, 640, 480,
	"program window",
	0, 0,
	31, NULL,
	0
};

static CTRLDATA CtrlInitProgress [] =
{
	{
		CTRL_STATIC,
		WS_VISIBLE | SS_CENTER,
		400, 5, 180, 30,
		IDC_TIME,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC,
		WS_VISIBLE |ES_READONLY,
		25 ,55, 120, 30,
		IDC_PROGRAMNUM,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC,
		WS_VISIBLE |ES_READONLY,
		145, 55, 80, 30,
		IDC_PROGRAMDATASTEP,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC, /* set program name in program list window */
		WS_VISIBLE | SS_SIMPLE,
		40, 7, 200, 28,
		IDC_PROGRAMNAME,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{       /* current position */
		CTRL_STATIC,
		WS_VISIBLE | SS_SIMPLE | ES_READONLY,
		130, 80, 200, 28,
		IDC_CURPOSDESC,
		"",
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC,
		WS_VISIBLE | SS_SIMPLE |WS_BORDER| ES_READONLY,
		LISTBOX_X , LISTBOX_Y, LISTBOX_W, 20,
		IDC_PROGRAMCUTSLISTHEAD,
		NULL,
		0
	},
	{
		CTRL_LISTBOX,
		WS_VISIBLE | WS_BORDER|WS_VSCROLL | LBS_NOTIFY | LBS_EXTRA_STYLE1,
		LISTBOX_X , LISTBOX_Y + 20, LISTBOX_W, LISTBOX_H - 20,
		IDL_PROGRAMCUTSLIST,
		NULL,
		200
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE |WS_BORDER,
		165, 406, 130, 32,
		IDC_INPUTCUTDATA,
		NULL,
		0
	},
	{
		CTRL_STATIC,  /* position before cut */
		WS_VISIBLE | ES_READONLY,
		450, 40, 110, 32,
		IDC_CURPOS,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC,  /* cut counter */
		WS_VISIBLE | ES_READONLY,
		450, 75, 110, 30,
		IDC_CUTCOUNTER,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC, /* input data description */
		WS_VISIBLE | SS_CENTER,
		50, 410, 108, 28,
		IDC_INPUTDESC,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC, /* current mode */
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		390, 410, 90, 28,
		IDC_STATUSMODE,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC, /* STATUS BAR */
		WS_VISIBLE | ES_READONLY | SS_CENTER,
		140, 452, 340, 24,
		IDC_STATUSBAR,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{       /* program files list head */
		CTRL_STATIC,
		WS_VISIBLE | SS_SIMPLE |WS_BORDER| ES_READONLY,
		LISTBOX_FILE_X , LISTBOX_Y, LISTBOX_FILE_W, 20,
		IDC_PROFILE_HEAD,
		NULL,
		0
	},
	{       /* program file list */
		CTRL_LISTBOX,
		WS_VISIBLE | WS_BORDER|WS_VSCROLL | LBS_NOTIFY,
		LISTBOX_FILE_X , LISTBOX_Y + 20, LISTBOX_FILE_W, LISTBOX_H - 20,
		IDC_PROFILE_LIST,
		NULL,
		200
	},
	/* ______________________________________________ */
	{ 	/* USER MODE */
		CTRL_STATIC, /* USER 1 */
		WS_VISIBLE | ES_READONLY,
		140, 164, 60, 24,
		IDC_USER1_DESC,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC, /* USER 2 */
		WS_VISIBLE | ES_READONLY,
		140, 190, 60, 24,
		IDC_USER2_DESC,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC, /* USER 3 */
		WS_VISIBLE | ES_READONLY,
		140, 216, 60, 24,
		IDC_USER3_DESC,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC, /* USER 4 */
		WS_VISIBLE | ES_READONLY,
		140, 242, 60, 24,
		IDC_USER4_DESC,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC, /* USER 5 */
		WS_VISIBLE | ES_READONLY,
		140, 268, 60, 24,
		IDC_USER5_DESC,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC, /* USER 6 */
		WS_VISIBLE | ES_READONLY,
		140, 294, 60, 24,
		IDC_USER6_DESC,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC, /* USER 7 */
		WS_VISIBLE | ES_READONLY,
		140, 320, 60, 24,
		IDC_USER7_DESC,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	{
		CTRL_STATIC, /* USER 8 */
		WS_VISIBLE | ES_READONLY,
		140, 346, 60, 24,
		IDC_USER8_DESC,
		NULL,
		0,
		WS_EX_TRANSPARENT
	},
	/* ______________________________________________ */
	{
		CTRL_SLEDIT, /* USER 1 */
		WS_VISIBLE | ES_READONLY | WS_BORDER,
		210, 160, 120, 24,
		IDC_USER1_NAME,
		NULL,
		0
	},
	{
		CTRL_SLEDIT, /* USER 2 */
		WS_VISIBLE | ES_READONLY  | WS_BORDER,
		210, 186, 120, 24,
		IDC_USER2_NAME,
		NULL,
		0
	},
 	{
		CTRL_SLEDIT, /* USER 3 */
		WS_VISIBLE | ES_READONLY | WS_BORDER,
		210, 212, 120, 24,
		IDC_USER3_NAME,
		NULL,
		0
	},
	{
		CTRL_SLEDIT, /* USER 4 */
		WS_VISIBLE | ES_READONLY  | WS_BORDER,
		210, 238, 120, 24,
		IDC_USER4_NAME,
		NULL,
		0
	},
	{
		CTRL_SLEDIT, /* USER 5 */
		WS_VISIBLE | ES_READONLY | WS_BORDER,
		210, 264, 120, 24,
		IDC_USER5_NAME,
		NULL,
		0
	},
	{
		CTRL_SLEDIT, /* USER 6 */
		WS_VISIBLE | ES_READONLY  | WS_BORDER,
		210, 290, 120, 24,
		IDC_USER6_NAME,
		NULL,
		0
	},
	{
		CTRL_SLEDIT, /* USER 7 */
		WS_VISIBLE | ES_READONLY | WS_BORDER,
		210, 316, 120, 24,
		IDC_USER7_NAME,
		NULL,
		0
	},
	{
		CTRL_SLEDIT, /* USER 8 */
		WS_VISIBLE | ES_READONLY  | WS_BORDER,
		210, 342, 120, 24,
		IDC_USER8_NAME,
		NULL,
		0
	}
};

/* Local functions
 *
 */
static void ResetUserProgramEdit(HWND hwnd,userProgramEditData *pProEdit,int func);
static void refreshUserProgramRunStatus(HWND hwnd,HDC hdc,userProgramRunStatus *pStatus,int type,int flag);
static void releaseSecondFunction(HWND hwnd);


/* update user list action */
#define USERLIST_DEFAULT      0
#define USERLIST_HIDE_ALL     1
#define USERLIST_SEL_UP       2
#define USERLIST_SEL_DOWN     3
#define USERLIST_DELETE       4

static int updateUsersList(HWND hwnd, int curuser, int action);

/********************************************************************/
/* Global Functiones, may be called in other window
 */

/* refresh the current position
 *   hwnd: the IDC_CUTPOS edit box handler
 *   data: the pos
 */
void refreshCurrentPos(HWND hwnd,int data)
{
	float curPos;
	char curPosBuf[10];
	curPos = (float)data/100.0;
	sprintf(curPosBuf,"%7.2f",curPos);
	SetWindowText(hwnd,curPosBuf);

}

/* refresh the current pos by icons
 *   hdc:  draw DC
 *   data: the current postion
 */
void refreshCurrentPosByIcon(HDC hdc,int data)
{
	float curPos;
	char curPosBuf[10];
	int len,i,numX;
	curPos = (float)data/100.0;
	sprintf(curPosBuf,"%7.2f",curPos);
	len = strlen(curPosBuf);
	//printf("pos = %s len = %d \n",curPosBuf,len);
	for(i=0;i<len;i++){
		if(curPosBuf[i] >= '0' && (curPosBuf[i] <= '9')){
			numX = ((int)(curPosBuf[i] - 0x30) * 20);
		} else if(curPosBuf[i] == '.'){
			numX = 200;  /* dot */
		} else {
			numX = 220;  /* space */
		}

		RefreshBoxArea (hdc,&largeNumsIcon,CURPOS_X+i*20,CURPOS_Y,20,32,numX,0);
	}
}

/* read cut counter from file
 *   return: counter number
 */
static int loadCutCounter(void)
{
	int num;
	FILE *fp;
	if((fp=fopen("./user/cuts.cfg","r"))==NULL)
	{
		printf("loadCutCounter Error!\n");
		return 0;
	}

	while(!feof(fp)){
		fscanf(fp,"%d\n",&num);
	}
	fclose(fp);

	return(num);
}

/* save cut counter to file
 *   num: counter number
 */
static int saveCutCounter(int num)
{
	FILE *fp;
	if((fp=fopen("./user/cuts.cfg","w"))==NULL)
	{
		printf("saveCutCounter Error!\n");
		return 0;
	}

	fprintf(fp,"%d\n",num);
	fclose(fp);

	return(num);
}


/* refresh current position
 *  hDlg:   main window
 *  data:   current pos
 *  range:  update the trackbar?
 */
static void refreshCurrentPosArea(HWND hDlg,HDC hdc,int data,int range)
{
	refreshCurrentPosByIcon(hdc,data);
}

/*  refresh frontCutLen
 */
static void refreshForCutLenArea(HWND hwnd,userProgramRunStatus *pStatus,int curPos)
{
	int len ;
	int lastPos = pStatus->lastCutPos;
	if(curPos < lastPos){
		len = lastPos - curPos;
		pStatus->forCutLen = len;
	} else {
		pStatus->forCutLen = 0;
	}
	refreshCurrentPos(hwnd,pStatus->forCutLen);
	//printf("refreshCurPos:last=%d cur=%d len=%d\n",lastPos,curPos,pStatus->forCutLen);
}

/* refresh cut counter
 */
static void refreshCutCounter(HWND hwnd)
{
	char dispbuf[8];
	HWND hwndCutCnt = GetDlgItem(hwnd,IDC_CUTCOUNTER);
	memset(dispbuf,0,8);
	sprintf(dispbuf,"%6d",motorStatus.cutCounter);
	SetWindowText(hwndCutCnt,dispbuf);

	/* save value to file */
	saveCutCounter(motorStatus.cutCounter);
}

/*
 * reset cut counter
 * hwnd: main hwnd
 */
static void resetCutCounter(HWND hwnd)
{
	motorStatus.cutCounter = 0;
	refreshCutCounter(hwnd);
}


/* Get valid front limit
 */
static int getValidLimit(void)
{
	int frontLimit = currentuser->frontlimit;
	int whichLimit = proRunStatus.whichLimit;

	if(whichLimit)
		frontLimit= currentuser->middlelimit;

	return(frontLimit);
}

static int isSecondFunctionValid(void)
{
	return proRunStatus.secondFunc;
}

/* clear second function flag and refresh shift icon
 */
static void releaseSecondFunction(HWND hwnd)
{
	HDC hdc;
	if(proRunStatus.secondFunc == 1){
		proRunStatus.secondFunc = 0;
		hdc = BeginPaint(hwnd);
		refreshUserProgramRunStatus(hwnd,hdc,&proRunStatus,3,0);
		EndPaint(hwnd,hdc);
	}
}

/********************************************************************/
/*
 *   LOCK operations
 */
struct mdc_lock_t {
	int lock;
};

void init_mdc_lock(struct mdc_lock_t *mdclock)
{
	mdclock->lock = 0;
}

void mdc_lock(struct mdc_lock_t *mdclock)
{
	mdclock->lock++;
}

void mdc_unlock(struct mdc_lock_t *mdclock)
{
	mdclock->lock--;
}

int is_mdc_locked(struct mdc_lock_t *mdclock)
{
	return mdclock->lock > 0 ? 1:0;
}

struct mdc_lock_t mode_change_lock;

/********************************************************************/
/*
 *   File Operations
 */

/* Save Program Data to file
 *   pathname :  file name and path
 *   fileHeader: file infomation, got from program loader
 *   pProEdit:   the program data
 *
 *   return:
 *     0: OK 1: Error
 *
 */
static int saveUserProgramToFile(char *pathname,
				 struct userProFileHeader_t *fileHeader,
				 userProgramEditData *pProEdit)
{
	int nInputCuts = pProEdit->nInputCutsNum;
	FILE *fp;
	char pathtmp[16] = "./default.pro";

	//printf("file name=%s\n",programCreate.filePathname);

	if(strlen(pathname) == 0){   /* file name is empty */
		pathname = pathtmp;
	}

	fp = fopen(pathname,"wb+");  /* open as binary, write */
	if(fp == NULL){
		perror("File open error.\n");
		return(-1);
	}

	/* file header */
	fwrite(fileHeader,sizeof(struct userProFileHeader_t),1,fp);
	/* program data */
	fwrite(&(pProEdit->userProCuts[1]),sizeof(int)*nInputCuts,1,fp);

	fclose(fp);

	return 1;
}

/* Read Program Data from file
 *   pathname :  file name and path
 *   fileHeader: file infomation, got from program loader
 *   pProEdit:   the program data
 *
 *   return:
 *     0: OK -1: Error
 *
 */
static int loadUserProgramFromFile(char *pathname,
				   struct userProFileHeader_t *fileHeader,
				   userProgramEditData *pProEdit)
{
	FILE *fp;
	int ret;
	int index=1;

	//printf("open filename=%s\n",programCreate.filePathname);
	fp = fopen(pathname,"r");  /* binary file, read */

	if(fp == NULL){
		perror("File open erreo.\n");
		return(-1);
	}

	/* read file header */
	ret = fread(fileHeader,sizeof(struct userProFileHeader_t),1,fp);

	//printf("%x %d %d \n",fileHeader->fileflags,fileHeader->usedTimes,fileHeader->first);
	while(!(feof(fp))){
		ret = fread(&(pProEdit->userProCuts[index]),4,1,fp);
		//printf("ret=%d \n",pProEdit->userProCuts[index]);
		index++;

	}
	pProEdit->userProCuts[0] = fileHeader->first;

	return(index);
}

/* update file header structure, NOT reallly save to file
 *
 */
static void UpdateFileHeader(struct fromProListToPro_t *pProCreate,
			     userProgramEditData *pProEdit,
			     int times)
{
	char date[12];
	struct timeval tv;
	time_t curtime;

	gettimeofday(&tv, NULL);
	curtime = tv.tv_sec;

	/* get time string */
	strftime(date, 12, "%y-%m-%d", localtime(&curtime));

	memset(pProCreate->proHeader.latestDate,0,12);
	strncpy(pProCreate->proHeader.latestDate,date,strlen(date));

	pProCreate->proHeader.isnull = 0;
	pProCreate->proHeader.first = pProEdit->userProCuts[0];

	if(times){
		pProCreate->proHeader.usedTimes = pProCreate->proHeader.usedTimes +1;
	}
}


#if 0
static int dumpProgramData(userProgramEditData *pProEdit)
{
	int totalCuts = pProEdit->nInputCutsNum ;
	int i;

	for(i=0; i< totalCuts -1; i++){
		printf("%3d: %8d\n",i, pProEdit->userProCuts[i] & 0x00ffffff);
	}

	return 0;
}
#endif


/* update listbox with given data
 *   hwnd: main window handler
 *   pProEdit: prodram data edit structure
 *   type: 0: modify data @ pProEdit->currentCutsNum
 *         1: add data to the last
 */
static int UpdateProListWithData(HWND hwnd,userProgramEditData *pProEdit,int type)
{
	int data,flag;
	float fdata;
	int iconflag = 0;
	unsigned int currentCut = pProEdit->currentCutsNum;
	int unit = currentuser->unit;
	HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);
	char dispbuf[40];

	memset(dispbuf,0,40);

	if(currentCut < MAX_PROGRAM_CUTS){
		data = pProEdit->userProCuts[currentCut] & 0x00ffffff;
		fdata = (float)(data / 100.0);
		if(unit){
			sprintf(dispbuf," %03d      %06.2f ",currentCut+1,fdata);
		} else {
			sprintf(dispbuf," %03d      %07.2f ",currentCut+1,fdata);
		}
		flag = pProEdit->userProCuts[currentCut] & AIR_FLAG;
		if(!flag){              // flag = 0, ON
			iconflag |= AIR_ICON_BIT;
		}else{
			iconflag |= AIR_ICON_BIT | AIR_ICON_SW_BIT;  /* off icon */
		}

		if(type == 0){
			/* Fisrt delete current item */
			SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_DELETESTRING,currentCut,0);
			/* Insert new item to current place */
			SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_INSERTSTRING, (WPARAM)(currentCut | iconflag),
					   (LPARAM)dispbuf);
		} else {
			SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_ADDSTRING, (WPARAM)iconflag,(LPARAM)dispbuf);
		}
		/* SendMessage(hwndProCutList,LB_SETICONFLAG,currentCut,(DWORD)iconflag); */
		SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);

	} else {
		pProEdit->currentCutsNum = 0;
	}


	return 0;
}


/* update listbox with given data
 *   hwnd: main window handler
 *   pProEdit: prodram data edit structure
 *
 * When finished, highlight the item @ pProEdit->currentCutsNum
 *
 */
static int UpdateProListWithFileData(HWND hwnd,userProgramEditData *pProEdit)
{
	int data;
	float fdata;
	int flag=0;
	int iconflag = 0;
	int index = 0;
	int totalCuts = pProEdit->nInputCutsNum;
	HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);
	char dispbuf[40];
	int unit = currentuser->unit;

	memset(dispbuf,0,40);

	/* clear listbox */
	SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_RESETCONTENT,0,0);
	for(index=0;index<totalCuts;index++){
		iconflag = 0;
		data = pProEdit->userProCuts[index] & 0x00ffffff;
		fdata = (float)(data /100.0);
		flag = pProEdit->userProCuts[index] & AIR_FLAG;
		if(!flag)  /* flag == 0 -> ON */
			iconflag |= AIR_ICON_BIT;
		else
			iconflag |= AIR_ICON_BIT | AIR_ICON_SW_BIT;


		flag = pProEdit->userProCuts[index] & PUSHER_FLAG;
		if(flag){
			iconflag |= PUSHER_ICON_BIT;
		}

		flag = pProEdit->userProCuts[index] & AUTOCUT_FLAG;
		if(flag){
			iconflag |= AUTOCUT_ICON_BIT;
		}

		flag = pProEdit->userProCuts[index] & PRESS_FLAG;
		if(flag){
			iconflag |= PRESS_ICON_BIT;
		}

		if(unit){
			sprintf(dispbuf," %03d      %06.2f ",index+1,fdata);
		} else {
			sprintf(dispbuf," %03d      %07.2f ",index+1,fdata);
		}

		SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_ADDSTRING, (WPARAM)iconflag,(LPARAM)dispbuf);
		/* SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_SETICONFLAG, index,(DWORD)iconflag); */
	}
	//pProEdit->currentCutsNum = 0;
	SendMessage(hwndProCutList,LB_SETCURSEL,pProEdit->currentCutsNum,0);
	return 0;
}

/* delete last 0.00 item when enter into AUTO/MANUAL mode
 */
static int deleteLastListItem(HWND hwndList,
			      userProgramEditData *pProEdit,
			      char *buffer)
{
	int index;
	int totalCuts = pProEdit->nInputCutsNum;   /* the last item */

	index = SendMessage(hwndList,LB_GETCOUNT,0,0);
	if(index == totalCuts){
		SendMessage(hwndList,LB_GETTEXT,index-1,(LPARAM)buffer);
		//printf("index = %d total=%d:%s \n",index,totalCuts,buffer);
		SendMessage(hwndList,LB_DELETESTRING,index-1,0);
	}

	return(index);
}

static int addLastListItem(HWND hwndList,userProgramEditData *pProEdit,char *buffer)
{
	int totalCuts = pProEdit->nInputCutsNum;   /* the last item */
	int index;
	index = SendMessage(hwndList,LB_GETCOUNT,0,0);
	//printf("add:index = %d total = %d :%s \n",index,totalCuts,buffer);
	if(index == totalCuts -1){
		SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)buffer);
	}
	return(index);
}

/* check the input data string. If it is legal, convert it to data.
 *   pBuf: the input string
 *   len:  the string length
 *   return:
 *         the data if valid
 */
int isInputCutsDataValid(char *pBuf,int len)
{

	int data,i,j;
	char strBuf[10];

	if(len > 8){
		return(-1);
	}

	//printf("input: %s  %d --> ",pBuf,len);
	i = 0;
	j = -1; // j = -1,no point
	while(i<len){
		if(pBuf[i] == '.'){
			j = i;  // Find the first point position
			break;
		}
		if(((pBuf[i]<'0') || (pBuf[i] > '9')) && (pBuf[i] != '.')){
			return(-1);
		}
		i++;
	}
	memset(strBuf,0,10);

	if(j>=0){
		strncpy(strBuf,pBuf,j);  /* int */
		strncat(strBuf,"00",2);  // * 100
		if((pBuf[j+1] >= '0') && (pBuf[j+1] <= '9')){
			strBuf[j] = pBuf[j+1];
			if((pBuf[j+2] >= '0') && (pBuf[j+2] <= '9'))
				strBuf[j+1] = pBuf[j+2];
		}

	}else{  // j < 0, no point
		strncpy(strBuf,pBuf,len);
		strncat(strBuf,"00",2); // * 100
	}

	data = atoi(strBuf);
	//printf("strbuf = %s data = %d \n",strBuf,data);
	return(data);

}


/*  program edit functions are defined in file bellow
 */
#include "program_edit.c"

#include "file_edit.c"

/******************************************************************************/

static int showTrackBar(HWND hDlg, userProgramRunStatus *pStatus,
			userProgramEditData *pProEdit, int run);

/* reset program edit status
 *
 */
static void ResetUserProgramEdit(HWND hwnd,userProgramEditData *pProEdit,int func)
{
	pProEdit->userEditStatus = 0;

}

static void exitTrainingMode(userProgramEditData *pProEdit)
{
	pProEdit->userEditStatus = 0;
}


/****************  Local window Functions  ********************/

/*********************** Motor Running Control ************************/

/* Run motor in step mode
 *
 * dir: moving direction
 *      0  forward   1  backward
 * step: step data, in 0.01mm
 *       moving length = step * 0.01mm
 */
void motorRunStep(int dir,int step)
{
	int data = step;
	char cmdSendBuf[13];

	memset(cmdSendBuf,0,12);
	cmdData.flag = 'F';
	cmdData.data = data;
	switch(dir){
	case MOTOR_FORWARD:             /* Forward */
		cmdData.status = 0x30;
		break;
	case MOTOR_BACKWARD:            /* Backward */
		cmdData.status = 0x31;
		break;
	case MOTOR_FORWARD_HS:          /* High Speed Forward */
		cmdData.status = 0x32;
		break;
	case MOTOR_BACKWARD_HS:         /* High Speed Backward */
		cmdData.status = 0x33;
		break;

	}
	setCmdFrame(&cmdData,cmdSendBuf);
	sendCommand(cmdSendBuf);        /* Send command out */
}

/*  Status Frame handler, after receiving the status frame, this
 *  function will be called.
 *  In this function, the cut and pusher status will be updated.
 *
 *  pStatus: run status structure
 *  status:  the received status
 *
 *  return: 0: one cut is NOT done
 *          1: one cut is done
 *
 *  One cut is done means that one cut loop is finished.
 *
 */
static int getDevStatus(userProgramRunStatus *pStatus,int status)
{
	int ret = 0;
	int isStatusChanged = 0;
	int cutOldStatus = pStatus->cutStatus;
	int pusherOldStatus = pStatus->pusherStatus;

	if(status & CUT_STATUS_MASK){
		pStatus->cutStatus = 1; /* cut down */
		/* As one cut is done, so update the lastCutPos with
		 * the current cutting down position
		 */
		pStatus->lastCutPos = motorStatus.currentCutPos;
		 /* clear the front cut length because we arrived the
		  * position, so it should be 0
		  */
		pStatus->forCutLen = 0;
	} else {
		pStatus->cutStatus = 0;  /* cut is still up */
	}

	if(status & PUSHER_STATUS_MASK){
		pStatus->pusherStatus = 1;
	} else {
		pStatus->pusherStatus = 0;
	}

	if(status & WHICH_LIMIT_MASK){
		pStatus->whichLimit = 1;    /* middle limit validate */
	} else {
		pStatus->whichLimit = 0;    /* front limit validate */
	}

	if(status & MACHINE_STOP_MASK){
		pStatus->machineStop = 0;   /* machineStop = 0 mease running, should
					     * NOT confused with the name
					     */
		pStatus->runStop = 1;       /* runStop = 1: running */
	} else {
		pStatus->machineStop = 1;
		pStatus->runStop = 0;       /* runStop = 0: stop */
	}

	if(cutOldStatus != pStatus->cutStatus){
		pStatus->cutActTimes++;
		isStatusChanged = 1;
	}
	if(pusherOldStatus != pStatus->pusherStatus){
		pStatus->pusherActTimes++;
		isStatusChanged = 1;
	}
	if(isStatusChanged){
		//printf("getStatus 1:%d \n",pStatus->cutPusherActTimes );
		if((pStatus->pusherStatus == 1) &&(pStatus->cutStatus == 1)
		   && (pStatus->cutPusherActTimes == 0)){
			pStatus->cutPusherActTimes = 1;
		}
		//printf("getStatus 2 :%d \n",pStatus->cutPusherActTimes );
		if((pStatus->pusherStatus == 0) &&(pStatus->cutStatus == 0)
		   && (pStatus->cutPusherActTimes == 1)){
			pStatus->cutPusherActTimes = 2;
			if(pStatus->cutActTimes >= 2){   /* One cut loop is done
							  * so return 1
							  */
				ret = 1;
			}
		}
		//printf("getStatus 3 :%d \n",pStatus->cutPusherActTimes );
	}

	return(ret);

}


/* reset run status, called before sending a command out
 */
static void resetMotorRunStatus(userProgramRunStatus *pStatus)
{
	pStatus->isOneCutDone = 0;
	pStatus->cutActTimes = 0;
	pStatus->pusherActTimes = 0;
	pStatus->cutPusherActTimes = 0;
}

/* Run one cut data
 * return:  0: the data is normal data
 *          1: the data has PUSHER flag
 */
static int motorRunOneCut(userProgramEditData *pProEdit,userProgramRunStatus *pStatus)
{
	int ret = 0;
	char cmdSendBuf[13];
	int currentCut = pProEdit->currentCutsNum;
	int data = pProEdit->userProCuts[currentCut];
	int runStatus = 0;  /* AutoCut Mode, set runStatus = 1,
			     * else set to 0
			     */
	int unit = currentuser->unit;
	int tmp = data & 0x00ffffff;

	if((tmp < currentuser->frontlimit) || (tmp > currentuser->backlimit)){
		return 0;
	}

	if(pStatus->runStatus == MD_AUTOCUT){
		runStatus = 1;
	}
	if(data & PUSHER_FLAG){      /* PUSHER flag ON */
		ret = 1;
		runStatus |= 0x02;   /* bit 1 */
	}

	if(data & AUTOCUT_FLAG){
		runStatus |= 0x04;   /* bit 2 */
	}

	if(data & AIR_FLAG){
		runStatus |= 0x08;   /* bit 3 */
	}
	if(data & PRESS_FLAG){
		runStatus |= 0x10;   /* bit 4 */
	}
	data = data & 0x00ffffff;    /* the data */
	if(unit){
		data = inch_to_mm(data);
	}
	cmdData.flag = 'G';
	cmdData.data = data;
	cmdData.status = 0xE0 | runStatus; /* Bit 5..7: RFU, default = 1 */
	if((runStatus & 0x04)){ /* Should not be this,But the lower machine is sold out
				 * and cann't be changed, so make a trick to set
				 * cmdData.status = 0x36;
				 */
		cmdData.status |= 0x06;   /* can not change AIR_FLAG */
	}
	setCmdFrame(&cmdData,cmdSendBuf);
	sendCommand(cmdSendBuf);          /* send out */
	//printf("motorRunOneCut %d %s\n",data,cmdSendBuf);
	return(ret);

}

/*   自动程序运行控制,检测切刀和推纸器动作变化作为主要控制标志,下位机发送的停止信号
 *   仅用于推纸刀的运行
 */

/* Auto run control function
 */
static int motorRunAuto(HWND hwnd,userProgramEditData *pProEdit,userProgramRunStatus *pStatus)
{
	HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);
	int currentCut = pProEdit->currentCutsNum;
	int totalCut = pProEdit->nInputCutsNum;
	int curCutPos = motorStatus.currentCutPos;
	int destCut = pStatus->destCutNum;
	int ret;
	int data = pProEdit->userProCuts[currentCut];
	int curCutPushFlag = 0;

	if(data & PUSHER_FLAG){     /* PUSHER flag ON */
		curCutPushFlag = 1;
	}

#if 0
	printf("Auto Run !currentCut = %d times = %d %d %d \n",currentCut,pStatus->cutActTimes,
	       pStatus->pusherActTimes,pStatus->cutPusherActTimes);
	printf("Auto Run !machineStop = %d curCutPushFlag = %d \n", pStatus->machineStop,curCutPushFlag);
	printf("DstCutPos = %d curCutPos = %d\n", pStatus->lastCutPos,curCutPos);
#endif

	/*  Check whether one cut loop is done.
	 *    If current cut loop is done, then go to next cut data;
	 *    If not, just return.
	 *  If this cut is PUSHER(this information is received from motor), that means
	 *  it is no neccessary to do CUT action., just push. So we will go to next data
	 *  if the current position is reach the dest position.
	 */

	/* take care of that CUT and PUSHER are both down before running */
	if(((pStatus->cutPusherActTimes == 2) && (pStatus->cutActTimes >= 2)) ||
	   ((pStatus->machineStop == 1) && (curCutPushFlag == 1) && (curCutPos == pStatus->curDestPos))){

		/* One cut loop done */
		pStatus->isOneCutDone = 1;

		pStatus->lastCutPos = pStatus->curDestPos; /* update lastCutPos */
		pStatus->forCutLen = 0;                    /* clear front cut length */

		pStatus->destCutNum++;                     /* Move to the next data */
		if(pStatus->destCutNum == totalCut - 1){   /* it is the last data,
							    * so back to the first one
							    */
			pStatus->destCutNum = 0;
		}

		currentCut = pProEdit->currentCutsNum;     /* prepare for next loop run */
		destCut = pStatus->destCutNum;
	}

	if(pStatus->isOneCutDone){                         /* cut loop done */
		currentCut = pProEdit->currentCutsNum;
		destCut = pStatus->destCutNum;
		if(destCut == 0){                          /* the last cut */
			pProEdit->currentCutsNum = 0;
			SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
		}

		currentCut = pProEdit->currentCutsNum;
		if(destCut == (currentCut + 1) ){
			SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);    /* Reset Before the next */
			SendMessage(hwnd,MSG_KEYDOWN,SCANCODE_CURSORBLOCKDOWN,0); /* Move to next */
			pProEdit->currentCutsNum++;

			/* Show Track Bar, run = 1 */
			showTrackBar(hwnd, pStatus, pProEdit, 1);
		}
		currentCut = pProEdit->currentCutsNum;
		pStatus->curDestPos = pProEdit->userProCuts[currentCut] & 0x00ffffff;  /* update dest pos in current loop */
		ret = motorRunOneCut(pProEdit,pStatus);  /* continue running */
		pStatus->machineStop = 0;                /* motor is runnig */
		resetMotorRunStatus(pStatus);            /* refresh status, isOneCutDone will be cleared */
	}


	return(pProEdit->currentCutsNum);
}

/* Half automatical control function
 *
 * === TODO ===
 *   This function is obsoleted
 */
#if 0
static int motorRunHalfAuto(HWND hwnd,userProgramEditData *pProEdit,userProgramRunStatus *pStatus)
{
	int currentCut;
	int totalCut = pProEdit->nInputCutsNum;
	HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);

	currentCut = SendMessage(hwndProCutList,LB_GETCURSEL,0,0); /* get current item */
	if(currentCut >= totalCut-1){ /* current is the last 0.0 data */
		return(0);
	}

	pProEdit->currentCutsNum = currentCut;
	motorRunOneCut(pProEdit,pStatus);  /* continue running */
	resetMotorRunStatus(pStatus);      /* update status */

	return(currentCut);

}
#endif

/* Manual run control function
 */
static int motorRunMannual(HWND hwnd,userProgramEditData *pProEdit,userProgramRunStatus *pStatus)
{
	char dataBuf[10];
	char cmdSendBuf[13];
	int data,len;
	int unit = currentuser->unit;
	int frontLimit = getValidLimit();
	HWND  hwndInputCutData = GetDlgItem(hwnd,IDC_INPUTCUTDATA);

	/* First get the input data and check validation
	 */
	len = GetWindowTextLength(hwndInputCutData);
	GetWindowText(hwndInputCutData,dataBuf,len);
	data = isInputCutsDataValid(dataBuf,len);

	/* if mannual flag is set, it means add/sub the input data
	 * to the current postion
	 */
	switch(pStatus->mannualFlag){
	case 1: /* Add */
		data = motorStatus.currentCutPos + data;
		break;
	case 2: /* Sub */
		data = motorStatus.currentCutPos - data;
		break;
	default:
		break;
	}

	if(pStatus->mannualFlag){
		pStatus->mannualFlag = 0;           /* clear flag */
		SetWindowText(hwndInputCutData,""); /* clear input box */
		refreshCurrentPos(hwndInputCutData,data);  /* update current pos */
	}

	/* Check whether the data is between  the limits */
	if((data >= frontLimit) && (data <= currentuser->backlimit)){
		pStatus->curDestPos = data;
		pStatus->forCutLen = 0;         /* What should i do here? */

		cmdData.flag = 'G';
		cmdData.status = 0x33;
		if(unit){
			data = inch_to_mm(data);
		}
		cmdData.data = data;
		setCmdFrame(&cmdData,cmdSendBuf);
		sendCommand(cmdSendBuf);
	} else { /* invalidation */
		MessageBox(hwnd,userpro_help[*curSysLang].msg_cutdata_overflow,
			   userpro_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		SetWindowText(hwndInputCutData,"");

	}

	return(data);
}

static void motorRunProcess(HWND hDlg,userProgramEditData *pProEdit,userProgramRunStatus *pStatus)
{
	int runStatus = pStatus->runStatus;

	switch(runStatus){
	case MD_AUTO:  /* Autorun */
		motorRunAuto(hDlg,pProEdit,pStatus);
		break;
	case MD_EDIT:  /* Training mode in Edit mode */
		//printf("Trainning Run...\n");
		motorRunMannual(hDlg,pProEdit,pStatus);
		break;
	case MD_MANUAL:  /* Manual mode */
		//printf("Enter Mannual Run...\n");
		motorRunMannual(hDlg,pProEdit,pStatus);
		break;
	}
}

/* initialize the listbox by file data
 * if the data file is empty(new), then create this file and
 * update the file header
 */
static int InitProCutsList(HWND hwnd,
			   userProgramEditData *pProEdit,
			   struct fromProListToPro_t *pProCreate)
{
	UpdateProListWithFileData(hwnd,pProEdit);

	return 0;
}

/* check F1 - F6 is valid
 */
static BOOL isFunctionButtonValid(userProgramRunStatus *pStatus)
{
	if(pStatus->runStop > 0){   /* Not valid if running */
		return FALSE;
	}
	if(pStatus->runStatus > 0){
		return FALSE;
	}

	return TRUE;
}

static int loadUserProBmpFiles(void)
{
	int langtype = *curSysLang;
	switch(langtype){
	case 1: /* english */
		if (LoadBitmap (HDC_SCREEN, &userProgramEditBkgnd, "./res/userProgramRun1.jpg"))
			return 1;
		break;
	case 0:
	default:
		if (LoadBitmap (HDC_SCREEN, &userProgramEditBkgnd, "./res/userProgramRun0.jpg"))
			return 1;
		break;
	}

	/* load button icons
	 */
	if (LoadBitmap (HDC_SCREEN, &btBmpEditFn[0], "./res/buttonEditF1.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpEditFn[1], "./res/buttonEditF2.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpEditFn[2], "./res/buttonEditF3.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpEditFn[3], "./res/buttonEditF4.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpEditFn[4], "./res/buttonEditF5.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpEditFn[5], "./res/buttonEditF6.bmp"))
		return 1;

	/* Manual Mode */
	if (LoadBitmap (HDC_SCREEN, &btBmpManualFn[0], "./res/buttonManualF1.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpManualFn[1], "./res/buttonManualF2.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpManualFn[2], "./res/buttonManualF3.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpManualFn[3], "./res/buttonManualF4.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpManualFn[4], "./res/buttonManualF5.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpManualFn[5], "./res/buttonManualF6.bmp"))
		return 1;


	/* Auto Mode */
	if (LoadBitmap (HDC_SCREEN, &btBmpAutoFn[0], "./res/buttonAutoF1.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpAutoFn[1], "./res/buttonAutoF2.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpAutoFn[2], "./res/buttonAutoF3.bmp"))
		return 1;

	/* Program File Mode */
	if (LoadBitmap (HDC_SCREEN, &btBmpFileFn[0], "./res/buttonFileF1.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpFileFn[1], "./res/buttonFileF2.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpFileFn[2], "./res/buttonFileF3.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpFileFn[3], "./res/buttonFileF4.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpFileFn[4], "./res/buttonFileF5.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpFileFn[5], "./res/buttonFileF6.bmp"))
		return 1;

	/* User Mode */
	if (LoadBitmap (HDC_SCREEN, &btBmpUserFn[0], "./res/buttonUserF1.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpUserFn[1], "./res/buttonUserF2.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpUserFn[2], "./res/buttonUserF3.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpUserFn[3], "./res/buttonUserF4.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpUserFn[4], "./res/buttonUserF5.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpUserFn[5], "./res/buttonUserF6.bmp"))
		return 1;

	/* status icons */
	if (LoadBitmap (HDC_SCREEN, &bmpStatusCutPusher[0], "./res/cutPusherOn.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &bmpStatusCutPusher[1], "./res/cutPusherOff.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &bmpStatusSperker[0], "./res/speakerOff.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &bmpStatusSperker[1], "./res/speakerOn.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &bmpStatusCut[1], "./res/cutLow.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &bmpStatusCut[0], "./res/cutHigh.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &bmpStatusPusher[1], "./res/pusherLow.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &bmpStatusPusher[0], "./res/pusherHigh.bmp"))
		return 1;

	/* shift */
	if (LoadBitmap (HDC_SCREEN, &bmpShiftFlag, "./res/shift.bmp"))
		return 1;


	/* left side */
	if (LoadBitmap (HDC_SCREEN, &btBmpCommonLeft[0], "./res/buttonRegister.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpCommonLeft[1], "./res/buttonProList.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpCommonLeft[2], "./res/buttonModeEdit.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpCommonLeft[3], "./res/buttonModeAuto.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpCommonLeft[4], "./res/buttonModeManual.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpCommonLeft[5], "./res/buttonModeTraining.bmp"))
		return 1;

	if (LoadBitmap (HDC_SCREEN, &btBmpEnter, "./res/buttonEnter.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpClear, "./res/buttonClear.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpRun, "./res/buttonRun.bmp"))
		return 1;

	/* cut position and cut counter icons */
	if (LoadBitmap (HDC_SCREEN, &cutPosIcon, "./res/cutfrontpos.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &cutCounterIcon, "./res/cutcounter.bmp"))
		return 1;


	if (LoadBitmap (HDC_SCREEN, &largeNumsIcon, "./res/0_9.bmp"))
		return 1;
	/* background for label and average edit window */
	if (LoadBitmap (HDC_SCREEN, &lableEditBkgnd, "./res/configItemBkgnd.jpg"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpLableFn[0], "./res/buttonLableF1.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &btBmpLableFn[1], "./res/buttonLableF2.bmp"))
		return 1;

	if (LoadBitmap (HDC_SCREEN, &flagicon[0], "./res/flag01.bmp"))  /* Air flag*/
		return 1;
	if (LoadBitmap (HDC_SCREEN, &flagicon[1], "./res/flag02.bmp"))  /* Pusher flag */
		return 1;
	if (LoadBitmap (HDC_SCREEN, &flagicon[2], "./res/flag03.bmp"))  /* Press flag */
		return 1;
	if (LoadBitmap (HDC_SCREEN, &flagicon[3], "./res/flag04.bmp"))  /* Autocut flag */
		return 1;
	if (LoadBitmap (HDC_SCREEN, &flagicon[4], "./res/flag05.bmp"))  /* No Air flag */
		return 1;

	/* Track bar */
	if (LoadBitmap (HDC_SCREEN, &bmpTrackBar_button, "./res/bar.bmp"))
		return 1;
	if (LoadBitmap (HDC_SCREEN, &bmpTrackBar_top, "./res/bar-on.bmp"))
		return 1;

	return(0);
}


static int unloadUserProBmpFiles(void)
{
	int i = 0;

	UnloadBitmap(&userProgramEditBkgnd);

	for(i = 0; i < EDIT_FN_NUM; i++)
		UnloadBitmap(&btBmpEditFn[i]);

	for(i = 0; i < MANUAL_FN_NUM; i++)
		UnloadBitmap(&btBmpManualFn[i]);

	for(i = 0; i < AUTO_FN_NUM; i++)
		UnloadBitmap(&btBmpAutoFn[i]);

	for(i = 0; i < PROLIST_FN_NUM; i++)
		UnloadBitmap(&btBmpFileFn[i]);

	for(i = 0; i < USER_FN_NUM; i++)
		UnloadBitmap(&btBmpUserFn[i]);

	for(i = 0; i < 6; i++)
		UnloadBitmap(&btBmpCommonLeft[i]);


	/* status */
	UnloadBitmap(&bmpStatusCutPusher[0]);
	UnloadBitmap(&bmpStatusCutPusher[1]);
	UnloadBitmap(&bmpStatusSperker[0]);
	UnloadBitmap(&bmpStatusSperker[1]);
	UnloadBitmap(&bmpStatusCut[0]);
	UnloadBitmap(&bmpStatusCut[1]);
	UnloadBitmap(&bmpStatusPusher[0]);
	UnloadBitmap(&bmpStatusPusher[1]);

	UnloadBitmap(&bmpShiftFlag);

	UnloadBitmap(&btBmpEnter);
	UnloadBitmap(&btBmpClear);
	UnloadBitmap(&btBmpRun);

	UnloadBitmap(&cutPosIcon);
	UnloadBitmap(&cutCounterIcon);

	UnloadBitmap(&largeNumsIcon);

	UnloadBitmap(&lableEditBkgnd);
	UnloadBitmap(&btBmpLableFn[0]);
	UnloadBitmap(&btBmpLableFn[1]);

	for(i = 0; i < 5; i++)
		UnloadBitmap(&flagicon[i]);

	UnloadBitmap(&bmpTrackBar_button);
	UnloadBitmap(&bmpTrackBar_top);

	return 0;
}


/* update program Data setp
 * called from listbox CUR_CHANGED message
 */
static void updateProgramDataStep(HWND mainhwnd, userProgramEditData *pProEdit)
{
	char stepBuf[10];
	int totalCut = pProEdit->nInputCutsNum;
	int currentSel = pProEdit->currentCutsNum + 1;

	sprintf(stepBuf, "%d-%d", totalCut, currentSel);
	SetDlgItemText(mainhwnd,IDC_PROGRAMDATASTEP,stepBuf);
}

/* get button HWND from mode */
static int mode_to_hwnd[MD_MAX] = {2, 3, 4, -1, 1, 0, 5};

#define get_mode_button_hwnd(x)   (mode_to_hwnd[x])

static void updateModeButtonState(userProgramRunStatus *pStatus)
{
	int i, index;

	for (i = 0; i < MD_MAX; i++) {
		index = get_mode_button_hwnd(i);

		if((index < 0) || (index > 6))
			continue;

		if(i == pStatus->runStatus)
			SendMessage(hwndButtonCommon[index], BM_SETSTATE, BST_ONLINE, 0);
		else
			SendMessage(hwndButtonCommon[index], BM_SETSTATE, 0, 0);
	}
}

/* update current mode */
static void updateCurrentModeStatus(HWND mainhwnd, userProgramRunStatus *pStatus)
{
	int lang = *curSysLang;

	/* Mode Name */
	SetDlgItemText(mainhwnd,IDC_STATUSMODE, strMachineMode[pStatus->runStatus]);

	/* Mode Status bar description*/
	SetDlgItemText(mainhwnd,IDC_STATUSBAR, strModeManual[lang][pStatus->runStatus]);

	/* Highlight Mode icon */
	updateModeButtonState(pStatus);

	/* show Track bar, run = 0 */
	showTrackBar(mainhwnd, pStatus, &programEdit, 0);

}

static void showEditModeFnButtons(void)
{
	int i;

	for(i = 0; i < EDIT_FN_NUM; i++){
		SendMessage(hwndButtonEditFn[i], BM_SETIMAGE, BM_IMAGE_BITMAP, (LPARAM)&btBmpEditFn[i]);
		ShowWindow(hwndButtonEditFn[i], SW_SHOW);
	}
}

static void showManualModeFnButtons(void)
{
	int i;

	for(i = 0; i < MANUAL_FN_NUM; i++){
		SendMessage(hwndButtonEditFn[i], BM_SETIMAGE, BM_IMAGE_BITMAP, (LPARAM)&btBmpManualFn[i]);
		ShowWindow(hwndButtonEditFn[i], SW_SHOW);
	}
}

static void showAutoModeFnButtons(void)
{
	int i;

	for(i = 0; i < AUTO_FN_NUM; i++){
		SendMessage(hwndButtonEditFn[i], BM_SETIMAGE, BM_IMAGE_BITMAP, (LPARAM)&btBmpAutoFn[i]);
		ShowWindow(hwndButtonEditFn[i], SW_SHOW);
	}

	for(i = AUTO_FN_NUM; i < 6; i++)
		ShowWindow(hwndButtonEditFn[i], SW_HIDE);
}

static void showFileModeFnButtons(void)
{
	int i;

	for(i = 0; i < PROLIST_FN_NUM; i++){
		SendMessage(hwndButtonEditFn[i], BM_SETIMAGE, BM_IMAGE_BITMAP, (LPARAM)&btBmpFileFn[i]);
		ShowWindow(hwndButtonEditFn[i], SW_SHOW);
	}
}

static void showUserModeFnButtons(void)
{
	int i;

	for(i = 0; i < USER_FN_NUM; i++){
		SendMessage(hwndButtonEditFn[i], BM_SETIMAGE, BM_IMAGE_BITMAP, (LPARAM)&btBmpUserFn[i]);
		ShowWindow(hwndButtonEditFn[i], SW_SHOW);
	}
}

static int showModeButtons(int mode)
{
	int i;

	switch(mode){
	case MD_EDIT:
	case MD_AUTO:
	case MD_MANUAL:
		for(i = 3; i < PROLIST_FN_NUM; i++){
			ShowWindow(hwndButtonCommon[i], SW_SHOW);
		}
		break;
	case MD_PROLIST:
	case MD_USER:
		for(i = 3; i < PROLIST_FN_NUM; i++){
			ShowWindow(hwndButtonCommon[i], SW_HIDE);
		}
		break;
	default:
		break;
	}

	return mode;
}


/*******************************************************************************/
/* Move to motorregister.c later */


/* Select one use and then enter MD_PROLIST mode.
 * Reload the program file list.
 */
int selectOneUser(HWND hDlg, int sel, struct user_manage_t *pUserMode)
{
	if((sel < 0) || (sel >= MAX_USER_NUMBER))
		return(-1);

	if(currentid == sel){  /* the same user */
		/* do nothing ,just change to MD_PROLIST mode */

		/* == TODO == */

		/* Why send this message will enter MD_EDIT, not MD_PROLIST?
		 * The ENTER message seems valid again in MD_PROLIST, so it
		 * change to MD_EDIT.
		 */
		SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_PROG, 0L);

		return sel;
	}

	/* select the other user */

	currentid = sel;
	currentuser = &userinformation[currentid];

	/* First we load the recent file of this user,
	 * this will initialise the programCreate structure, eg
	 * the user file pathname will also be filled.
	 *
	 */
	loadProgramFile(currentuser->recent_programe, &programCreate);

	/* Reload the file list, change to program list mode */
	isFileListLoaded = 0;
	SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_PROG, 0L);

	return sel;
}

/* In normal, the user name box is READONLY.
 * If we want to modify/new the user name, we have to set it to
 * writable.
 *
 */
static int setUserNameEditboxReadonly(HWND hwnd, int readonly)
{
	if(readonly)
		SendMessage(hwnd, EM_SETREADONLY, TRUE, 0);
	else
		SendMessage(hwnd, EM_SETREADONLY, FALSE, 0);

	return 0;
}

int getIMEType(void)
{
	int lang = *curSysLang;

	if(lang == 0)/* Chinese */
		return IME_PINYIN;
	else
		return IME_ABC;
}

void changeIMEType(int ime_type)
{
	SendMessage(myIMEWindow,MSG_IME_SETSTATUS, (WPARAM)0, (LPARAM)ime_type);
}

int showIMEWindow(int show, int ime_type)
{
	changeIMEType(ime_type);

	if(show){
		isIMEOpenInMotor = 1;
		/* show the IME window */
		ShowWindow(myIMEWindow,SW_SHOW);
	} else {
		isIMEOpenInMotor = 0;
		/* show the IME window */
		ShowWindow(myIMEWindow,SW_HIDE);
	}
	return 0;
}

int prepareToModifyUserName(HWND hDlg, int seluser, struct user_manage_t *pUserMode)
{

	HWND hwndTemp;

	/* Check the number
	 * Note:
	 *   User 0 and User 1 can NOT be changed.
	 *
	 */
	if((seluser < 2) || (seluser >= usernumber)){
		return (-1);
	}

	pUserMode->status = USER_EDIT_MODIFY;

	hwndTemp = GetDlgItem(hDlg, IDC_USER1_NAME + seluser);

	/* Clear Readonly */
	setUserNameEditboxReadonly(hwndTemp, 0);

	SetFocusChild(hwndTemp);

	/* Also show the IME window and change IME type */
	showIMEWindow(1, getIMEType());

	return 0;
}

void resetUserNameEdit(HWND hDlg, int seluser, struct user_manage_t *pUserMode)
{
	HWND hwndTemp = GetDlgItem(hDlg, IDC_USER1_NAME + seluser);

	pUserMode->status = USER_EDIT_SELECT;

	/* set Readonly */
	setUserNameEditboxReadonly(hwndTemp, 1);

	SetFocusChild(hwndTemp);

	/* Also show the IME window and change IME type */
	showIMEWindow(0, IME_NUMBER);
}

/*
 * Modify the user name
 */
int modifyUserName(HWND hDlg, int seluser, struct user_manage_t *pUserMode)
{
	int namelen, ret;
	char strName[20];
	HWND hwndTemp = GetDlgItem(hDlg, IDC_USER1_NAME + seluser);

#if 0
	printf("modifyUserName: currentid=%d current_modify=%d\n",currentid, seluser);
#endif
	namelen = GetWindowTextLength(hwndTemp);
	GetWindowText(hwndTemp, strName, namelen);

	ret = isUserNameValid(hDlg, strName, namelen);
	if (ret == 0) {  /* invalid name */

#if 0
		printf("user name is invalid: len=%d user=%d\n",namelen, seluser);
#endif
		/* show back the original name */
		SetWindowText(hwndTemp, userinformation[seluser].name);
		SendMessage(hwndTemp, EM_SETCARETPOS, 0, 0);

		resetUserNameEdit(hDlg, seluser, pUserMode);
		return 1;
	}

	/* Input user name is valid, update userinformation[] structure
	 * and save to file
	 */
	strncpy(userinformation[seluser].name, strName, namelen);
	saveUserConfigFile(0);

	resetUserNameEdit(hDlg, seluser, pUserMode);
	return 0;
}

int prepareToCreateUser(HWND hDlg, struct user_manage_t *pUserMode)
{
	HWND hwndTemp;
	int total_user = usernumber;

	/* Check user number
	 */
	if((total_user + 1) > MAX_USER_NUMBER) {
		return (-1);
	}

	pUserMode->status = USER_EDIT_NEW;

	hwndTemp = GetDlgItem(hDlg, IDC_USER1_DESC + total_user);
	ShowWindow(hwndTemp, SW_SHOW);

	hwndTemp = GetDlgItem(hDlg, IDC_USER1_NAME + total_user);
	ShowWindow(hwndTemp, SW_SHOW);

	/* Clear Readonly */
	setUserNameEditboxReadonly(hwndTemp, 0);

	SetFocusChild(hwndTemp);

	/* Also show the IME window and change IME type */
	showIMEWindow(1, getIMEType());

	/* lock mode change lock */
	mdc_lock(&mode_change_lock);

	return total_user;
}

int createOneUser(HWND hDlg, struct user_manage_t *pUserMode)
{
	int namelen, ret;
	char strName[20];
	int seluser = usernumber;
	HWND hwndTemp = GetDlgItem(hDlg, IDC_USER1_NAME + seluser);

#if 1
	printf("CreateUser: currentid=%d current_modify=%d\n",currentid, seluser);
#endif
	namelen = GetWindowTextLength(hwndTemp);
	GetWindowText(hwndTemp, strName, namelen);

	ret = isUserNameValid(hDlg, strName, namelen);
	if (ret == 0) {  /* invalid name */

#if 1
		printf("user name is invalid: len=%d user=%d\n",namelen, seluser);
#endif
		/* Clear the editbox and wait user input */
		SetWindowText(hwndTemp, "");
		SendMessage(hwndTemp, EM_SETCARETPOS, 0, 0);
		return 0;
	}

	/* Input user name is valid, update userinformation[] structure
	 * and save to file
	 */
	usernumber++;        /* total user increase */

	strncpy(userinformation[seluser].name, strName, namelen);
	initUserDataAndPrograms(hDlg, seluser);

	saveUserConfigFile(0);

	resetUserNameEdit(hDlg, seluser, pUserMode);

	/* update list to highlight current created user */
	updateUsersList(hDlg, seluser, USERLIST_DEFAULT);

	/* unlock mode change lock */
	mdc_unlock(&mode_change_lock);

	return 0;
}

/* delete One User
 */
int DeleteOneUser(HWND hDlg, int seluser, struct user_manage_t *pUserMode)
{
	int total_user = usernumber;
	char cmdbuff[40];
	int userid = userinformation[seluser].userid;
	int ret;

	/* Check user number
	 */
	if((seluser < 2) || (seluser >= total_user)){
		return (-1);
	}

	pUserMode->status = USER_EDIT_DELETE;

	/* check super password */
	ret = CheckPassword(hDlg);

	if((ret == PSW_CANCEL) || (ret == PSW_FAIL)) {
		pUserMode->status = USER_EDIT_SELECT;
		updateUsersList(hDlg, seluser, USERLIST_DEFAULT);

		return -1;
	}


	/* super password is confirmed, so delete user */
	saveUserConfigFile(seluser);  /* delete in config file */

	/* delete user program files */
	sprintf(cmdbuff, "rm -rf ./programe/user%02d/", userid);
	system(cmdbuff);

	resetUserNameEdit(hDlg, seluser, pUserMode);

	/* reload the user config file to update userinformation[] */
	loadUserConfigFile();

	/* update list to hide the deleted user */
	updateUsersList(hDlg, 0, USERLIST_DELETE);

	return total_user;
}

int actionUserModeEnterKeyPressed(HWND hDlg, struct user_manage_t *pUserMode)
{
	int status = pUserMode->status;
	int ret = 0;
	int seluser = current_highlight_user;

	switch(status){
	case USER_EDIT_SELECT:
		ret = selectOneUser(hDlg, seluser, pUserMode);
		break;
	case USER_EDIT_NEW:
		ret = createOneUser(hDlg, pUserMode);
		break;
	case USER_EDIT_DELETE:
		/* handled at other place */
		break;
	case USER_EDIT_MODIFY:
		ret = modifyUserName(hDlg, seluser, pUserMode);
		break;
	default:
		break;
	}

	return ret;
}

/*******************************************************************************/

/*
 *  Show track bar only in Auto mode.
 *
 *  When switch to MD_AUTO, show the button track bar;
 *  When motor runs, show the top track bar;
 */
#define TK_BAR_X      500
#define TK_BAR_Y      150
#define TK_BAR_W      20
#define TK_BAR_H      240

static int showTrackBar(HWND hDlg, userProgramRunStatus *pStatus,
			userProgramEditData *pProEdit, int run)
{
	int currentCut = pProEdit->currentCutsNum + 1;
	int totalCut = pProEdit->nInputCutsNum;
	int runStatus = pStatus->runStatus;
	int draw_h;
	HDC hdc = BeginPaint(hDlg);

	/* show button graph */
#if 0
	if (runStatus != MD_USER && runStatus != MD_PROLIST) {  /* debug */
#else
	if (runStatus == MD_AUTO || runStatus == MD_AUTOCUT) {
#endif
		FillBoxWithBitmap (hdc, TK_BAR_X, TK_BAR_Y, TK_BAR_W, TK_BAR_H,
				   &bmpTrackBar_button);
	} else {
		RefreshBoxArea (hdc,&userProgramEditBkgnd, TK_BAR_X, TK_BAR_Y,
				TK_BAR_W, TK_BAR_H, TK_BAR_X, TK_BAR_Y);
	}

	if(run) {
		draw_h = (int)(((float)currentCut / (float)totalCut ) * (TK_BAR_H - 4));


#if 0
		printf("showTrackBar: cur = %d total = %d h = %d \n",
		       currentCut, totalCut, draw_h);
#endif
		FillBoxWithBitmap (hdc, TK_BAR_X + 2, TK_BAR_Y + 2, TK_BAR_W - 4, draw_h,
				   &bmpTrackBar_top);
	}

	EndPaint(hDlg, hdc);

	return 0;
}


/* refresh all run status icons
 *   pStatus:    program  run status
 *   type:       which part of icon
 *   setfocus:   should we reset the focus
 */
static void refreshUserProgramRunStatus(HWND hwnd,HDC hdc,userProgramRunStatus *pStatus,int type,int setfocus)
{
	int currentCut = programEdit.currentCutsNum;
	HWND  hwndInputCutData = GetDlgItem(hwnd,IDC_INPUTCUTDATA);
	HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);
	/* HWND  hwndCutCnt = GetDlgItem(hwnd,IDC_CUTCOUNTER); */
	int status;

#if 0
	printf("refresh status: type=%d\n",type);
#endif

	switch(type){
	case 0: /* edit/auto/maunal handle */
		status = pStatus->runStatus;
		switch(status){
		case MD_EDIT: /* edit */
			ShowWindow(hwndInputCutData,SW_SHOW);
			/* ShowWindow(hwndCutCnt,SW_SHOW); */

			if(setfocus){
				SetFocusChild(hwndInputCutData);
				SetWindowText(hwndInputCutData,"");
			}
			break;

		case MD_AUTO: /* Autorun */
			ShowWindow(hwndInputCutData,SW_HIDE);
			/* ShowWindow(hwndCutCnt,SW_SHOW); */

			if(setfocus){
				SetFocusChild(hwndProCutList);
				SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);
			}

			break;

		case MD_MANUAL: /* Manual run */
			ShowWindow(hwndInputCutData,SW_SHOW);
			/* ShowWindow(hwndCutCnt,SW_SHOW); */

			if(setfocus){
				SetFocusChild(hwndInputCutData);
				SetWindowText(hwndInputCutData,"");     //clear
			}
			break;

		case MD_PROLIST: /* program file list */
		case MD_USER:    /* User Mode */
			ShowWindow(hwndInputCutData,SW_HIDE);
			/* ShowWindow(hwndCutCnt,SW_HIDE); */
			break;
		}

		break;

	case 1: /* run/stop flag */
		status = pStatus->runStop;

		/* 0: Machine Stop, CutPusher is enabled(on)
		 * 1: Machine Running, CutPusher is disabled(off)
		 */
		FillBoxWithBitmap (hdc, STATUS_CP_X, STATUS_CP_Y, 50, 24,
				   &bmpStatusCutPusher[status]);

		switch(status){
		case 0: /* stop */
			break;
		case 1: /* run */
			break;
		}
		break;

	case 2: /* pusher flag area */
		if(pStatus->pusher){

		} else {
		}
		break;

	case 3: /* shift area */
		if(pStatus->secondFunc){
			FillBoxWithBitmap (hdc, STATUS_SHIFT_X, STATUS_SHIFT_Y, 32, 32,
					   &bmpShiftFlag);
		} else {
			FillBoxWithBitmapPart (hdc, STATUS_SHIFT_X, STATUS_SHIFT_Y, 32, 32, 0, 0,
					       &userProgramEditBkgnd, STATUS_SHIFT_X, STATUS_SHIFT_Y);
		}
		break;

	case 4: /* machine status area */
		//printf("refreshUserProgramRunStatus...%d %d\n",pStatus->cutStatus,pStatus->pusherStatus);

		/* refresh cut status
		 *   0: cut up,    high
		 *   1: cut down,  low
		 */
		FillBoxWithBitmap (hdc, STATUS_CUT_X, STATUS_CUT_Y, 50, 24,
				   &bmpStatusCut[pStatus->cutStatus]);

		/* refresh pusher status
		 *   0: up,    high
		 *   1: down,  low
		 */
		FillBoxWithBitmap (hdc, STATUS_PUSHER_X, STATUS_PUSHER_Y, 50, 24,
				   &bmpStatusPusher[pStatus->pusherStatus]);

		/* refresh speaker status
		 * When cut moves to the dest position, speaker on
		 * else, speaker off
		 * So I can use isOneCutDone to do this
		 *   isOneCutDone = 1 -> Speaker On
		 *   isOneCutDone = 0 -> Speaker Off
		 */
		FillBoxWithBitmap (hdc, STATUS_SPK_X, STATUS_SPK_Y, 50, 24,
				   &bmpStatusSperker[pStatus->isOneCutDone]);

		break;
	}
}

/* init background
 * This function is called in MSG_ERASEBKGND, so it will called frequently
 * becasue timer is active.
 *
 * So we should do as less things here as possible in normal routine.
 *
 * I divide this function into 2 parts:
 *   part 1: only fill the bitmap of background, called in MSG_ERASEBKGND.
 *   part 2: show status icon and other backgound pictures
 *
 */
static int initUserproEditDlgBkgnd(HWND hwnd,HDC hdc,userProgramRunStatus *pStatus)
{
	int status = pStatus->runStatus;
	int curPos = motorStatus.currentCutPos;

	FillBoxWithBitmap (hdc, 0, 0, 0, 0, &userProgramEditBkgnd); /* main background */

	FillBoxWithBitmap (hdc, 560, 40, 0, 0, &cutPosIcon);     /* cut front pos */
	FillBoxWithBitmap (hdc, 560, 75, 0, 0, &cutCounterIcon); /* cut counter   */


	/* handle mode-specified fair here */
	switch(status){
	case 0: /* Edit */
		break;
	case 1: /* AutoRun */
		break;
	case 2: /* Manual Run */
		break;
	default:
		break;
	}

	/* the current pos */
	refreshCurrentPosByIcon(hdc,curPos);

	getDevStatus(&proRunStatus,motorStatus.devStatus);  /* get machine status */
	refreshUserProgramRunStatus(hwnd,hdc,pStatus,1,0);
	refreshUserProgramRunStatus(hwnd,hdc,pStatus,4,0);

	return 0;
}

static int reloadProgramFile(HWND hwnd,
			      userProgramEditData *pProEdit,
			      struct fromProListToPro_t *pProCreate)
{
	int ret;
	int id = pProCreate->proHeader.id;
	char numBuf[4];

	pProEdit->userEditStatus = 0;   /* default edit state */
	pProEdit->currentCutsNum = 0;
	pProEdit->inputbuf[0] = '\0';
	pProEdit->justEdit.action = 0;  /* clear just flag */

	sprintf(numBuf,"%s %2d  %s",strProgramDesc, id, strProgramStep);
	SetDlgItemText(hwnd,IDC_PROGRAMNUM,numBuf); /* program number */

	/*update program name */
	SetDlgItemText(hwnd, IDC_PROGRAMNAME, pProCreate->proHeader.introduction);

	if(pProCreate->isFileNew){              /* file is empty -> new file */
		pProEdit->nInputCutsNum = 1;    /* only 0.0 is displayed */
		pProEdit->currentCutsNum = 0;
		pProEdit->isProNameNull = 1;    /* program name is NULL */
		/* clear all data in this file */
		memset(pProEdit->userProCuts, 0, sizeof(pProEdit->userProCuts));

	} else {
		/* load program data */
		ret = loadUserProgramFromFile(pProCreate->filePathname,&(pProCreate->proHeader),pProEdit);
		pProEdit->nInputCutsNum = ret - 2;
	}

	if(strlen(pProCreate->proHeader.introduction) == 0){
		pProEdit->isProNameNull = 1;
	} else{
		pProEdit->isProNameNull = 0;
		SetDlgItemText(hwnd,IDC_PROGRAMNAME,pProCreate->proHeader.introduction);
	}

	/* since we get the data, so init the data list */
	InitProCutsList(hwnd,pProEdit,pProCreate);

	return 0;
}

/*  Init Users list.
 *    Display "User 1: ..."
 *    HIDE these controls because default is MD_EDIT mode.
 *
 *  Called when enter this window.
 *
 *  hwnd: the main HWND of this dialog.
 */
static int initUsersList(HWND hwnd)
{
	int i;
	char strbuf[20];
	HWND hwndTemp;
	int total_user = usernumber;

	userModeStatus.status = USER_EDIT_SELECT;  /* reset set user mode status */

	current_highlight_user = currentid;

	for(i = 0; i < MAX_USER_NUMBER; i++){
		sprintf(strbuf, "%s %d:", strMachineMode[MD_USER], i+1);
		SetDlgItemText(hwnd, IDC_USER1_DESC + i, strbuf);

		/* Hide these window */
		hwndTemp = GetDlgItem(hwnd, IDC_USER1_DESC + i);
		ShowWindow(hwndTemp, SW_HIDE);
	}

	for(i = 0; i < MAX_USER_NUMBER; i++){
		hwndTemp = GetDlgItem(hwnd, IDC_USER1_NAME + i);
		if(i < total_user){    /* Only show valid user */
			if(i == 0) {
				sprintf(strbuf, "Super 1 (* mm)");
			} else if(i == 1) {
				sprintf(strbuf, "Super 2 (* in)");
			} else {
				sprintf(strbuf, "%s", userinformation[i].name);
			}
			SetDlgItemText(hwnd,IDC_USER1_NAME + i, strbuf);
		}

		/* Some controls are read-only, so they are grey */
		SetWindowBkColor(hwndTemp, PIXEL_lightwhite);
		/* Hide these window */
		ShowWindow(hwndTemp, SW_HIDE);
	}

	return total_user;
}

/* Update the users list. Call this when enter User Mode.
 * Also, in MD_USER, when UP and DOWN keys are pressed, call
 * this function to update the list.(Set the HIGHLIGHT item)
 *
 *   hwnd:    the main hwnd
 *   curuser: the current user number (currentid)
 *   hide_all:Hide/Show these controls
 *   action:  up or down
 */
static int updateUsersList(HWND hwnd, int curuser, int action)
{
	int i;
	HWND hwndTemp;
	int total_user = usernumber;
	int new_sel, old_sel;

	new_sel = old_sel = curuser;

	switch(action){
	case USERLIST_HIDE_ALL:
		/* Hide this window when change mode */
		for(i = 0; i < total_user; i++){
			hwndTemp = GetDlgItem(hwnd, IDC_USER1_DESC + i);
			ShowWindow(hwndTemp, SW_HIDE);

			hwndTemp = GetDlgItem(hwnd, IDC_USER1_NAME + i);
			ShowWindow(hwndTemp, SW_HIDE);
		}
		return total_user;

	case USERLIST_SEL_UP: /* ADD: UP: Move to the previous user  */
		if(userModeStatus.status != USER_EDIT_SELECT)
			return curuser;

		if(new_sel > 0 ){
			new_sel--;
		}

		break;

	case USERLIST_SEL_DOWN: /* SUB: DOWN: Move to the next user */
		if(userModeStatus.status != USER_EDIT_SELECT)
			return curuser;

		if(new_sel < total_user - 1 ){
			new_sel++;
		}
		break;

	case USERLIST_DELETE:
		/* we just delete a user, so hide it
		 * i=0 and 1 are super users, can not be deleted. skip.
		 */
		for(i = 2; i < MAX_USER_NUMBER; i++){
			hwndTemp = GetDlgItem(hwnd, IDC_USER1_NAME + i);
			SetWindowText(hwndTemp, userinformation[i].name);

			if(i >= total_user){
				ShowWindow(hwndTemp, SW_HIDE);
				hwndTemp = GetDlgItem(hwnd, IDC_USER1_DESC + i);
				ShowWindow(hwndTemp, SW_HIDE);
			}
		}
		break;
	default:
		break;
	}

	/* handle highlight issue */
	for(i = 0; i < total_user; i++){

#if 0
		printf("i=%d: oldsel=%d, newsel=%d total=%d\n",i, old_sel, new_sel, total_user);
#endif
		/* Show these window */
		hwndTemp = GetDlgItem(hwnd, IDC_USER1_DESC + i);
		ShowWindow(hwndTemp, SW_SHOW);

		hwndTemp = GetDlgItem(hwnd, IDC_USER1_NAME + i);
		ShowWindow(hwndTemp, SW_SHOW);

		if(i == new_sel){
			/* highlight new select item */
			SetWindowBkColor(hwndTemp, PIXEL_yellow);
			InvalidateRect(hwndTemp, NULL, TRUE);
			SetFocusChild(hwndTemp);
			/* printf("SetFocusChild %d \n", i); */

		} else {
			SetWindowBkColor(hwndTemp, PIXEL_lightwhite);
			InvalidateRect(hwndTemp, NULL, TRUE);
		}
	}

	current_highlight_user = new_sel;
	return 0;
}

static int initUserProEditDlg(HWND hwnd,
			      userProgramEditData *pProEdit,
			      struct fromProListToPro_t *pProCreate)
{
	int ret;
	int id = pProCreate->proHeader.id;
	char numBuf[4];

	pProEdit->userEditStatus = 0;   /* default edit state */
	pProEdit->currentCutsNum = 0;
	pProEdit->inputbuf[0] = '\0';
	pProEdit->justEdit.action = 0;      /* clear just flag */

	proRunStatus.runStatus = MD_EDIT;   /* edit mode */
	proRunStatus.lastRunStatus = MD_USER;/* any other than MD_EDIT */
	proRunStatus.runStop = 0;           /* machine is stopped */
	proRunStatus.pusher = 0;            /* pusher: no action */
	proRunStatus.secondFunc = 0;        /* clear shift function */
	proRunStatus.whichLimit = 0;        /* limit is set to front limit */
	proRunStatus.machineStop = 1;       /* machine is stopped */

	proRunStatus.curDestPos = 0;
	proRunStatus.lastCutPos = 0;
	proRunStatus.forCutLen = 0;
	proRunStatus.mannualFlag = 0;

	sprintf(numBuf,"%s %2d  %s",strProgramDesc, id, strProgramStep);
	SetDlgItemText(hwnd,IDC_PROGRAMNUM,numBuf); /* program number */

	SetDlgItemText(hwnd,IDC_CURPOSDESC, strCurrentPosDesc); /* current pos */

	updateProgramDataStep(hwnd, pProEdit);

	SetDlgItemText(hwnd,IDC_INPUTDESC, strInputDataDesc); /* input data */

	updateCurrentModeStatus(hwnd, &proRunStatus);


	if(pProCreate->isFileNew){              /* file is empty -> new file */
		pProEdit->nInputCutsNum = 1;    /* only 0.0 is displayed */
		pProEdit->currentCutsNum = 0;
		pProEdit->isProNameNull = 1;    /* program name is NULL */
		/* clear all data in this file */
		memset(pProEdit->userProCuts, 0, sizeof(pProEdit->userProCuts));

	} else {
		/* load program data */
		ret = loadUserProgramFromFile(pProCreate->filePathname,&(pProCreate->proHeader),pProEdit);
		pProEdit->nInputCutsNum = ret - 2;
	}
	/* An empty file means NO DATA, but it could have NAME*/
	if(strlen(pProCreate->proHeader.introduction) == 0){
		pProEdit->isProNameNull = 1;
	} else{
		pProEdit->isProNameNull = 0;
		SetDlgItemText(hwnd,IDC_PROGRAMNAME,pProCreate->proHeader.introduction);
	}

	/* since we get the data, so init the data list */
	InitProCutsList(hwnd,pProEdit,pProCreate);

	/* users list */
	initUsersList(hwnd);

	/* mode change lock */
	init_mdc_lock(&mode_change_lock);

	return 0;
}

static char lastListItem[40];
static int mannualEditFlag = 0;   /* the edit status flag in mannual mode */
static int autoCutChangeFlag = 0;


/*
 * callback functions
 */
static void editInputCutDataCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd;
	char cutDataBuf[10];
	int  cutDataLen=0;
	int ret;
	userProgramEditData *pProEdit = &programEdit;
	int status = pProEdit ->userEditStatus;
	mainHwnd = GetMainWindowHandle(hwnd);

	switch(nc){
	case EN_SETFOCUS:
		/* got focus, change IME to number input */
		SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);
		if(isIMEOpenInMotor == 1){
			ShowWindow(myIMEWindow,SW_HIDE);
			isIMEOpenInMotor = 0;
		}

		break;
	case EN_CHANGE: /* number key is pressed */
		cutDataLen = GetWindowTextLength(hwnd);
		if(cutDataLen > 7){  /* overflow */
			SetFocusChild(hwnd);
			SetWindowText(hwnd,"");
		}
		break;
	case EN_ENTER:
		if(proRunStatus.runStatus)
			break;

		/* First check the data validation, the do the follows:
		 *   update validate data to userProCuts[currentCutsNum]
		 *   currentCutsNum++ and nInputCutsNum++
		 *   update data to listbox and move to next line
		 */
		cutDataLen = GetWindowTextLength(hwnd);
		GetWindowText(hwnd,cutDataBuf,cutDataLen);    /* get input string */

		ret=isInputCutsDataValid(cutDataBuf,cutDataLen);
		if(ret >= 0 ){
			switch(status){
			case 0: /* edit mode */
			case 7: /* training mode edit */
				printf("editOneCutData %d \n",ret);
				editOneCutData(mainHwnd,pProEdit,ret);
				break;
			case 1: /* label edit */
				//userProgramLableEdit(mainHwnd,pProEdit,ret);
				break;
			case 2: /* average edit */
				//userProgramHalfEdit(mainHwnd,pProEdit,ret);
				break;
			case 3: /* modify data */
				//modifyOneCutData(mainHwnd,pProEdit,ret);
				break;
			case 4: /* insert data */
				//insertOneCutData(mainHwnd,pProEdit,ret);
				break;
			case 6: /* just data */
				justAllCutDatas(mainHwnd,pProEdit,ret);
				break;
			case 8: /* add */
				compareAddDecInput(mainHwnd,pProEdit,ret,0);
				break;
			case 9: /* sub */
				compareAddDecInput(mainHwnd,pProEdit,ret,1);
				break;
			}
		} else {        /* invalidate data */
			MessageBox(mainHwnd,userpro_help[*curSysLang].msg_cutdata_overflow,
				   userpro_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		}
		memset(cutDataBuf,0,sizeof(cutDataBuf));
		SetWindowText(hwnd,cutDataBuf);

		break;
	}
}

static void proCutListCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	int cursel;
	HWND mainhwnd = GetMainWindowHandle(hwnd);

	switch(nc){
	case LBN_SELCHANGE:
		cursel = SendMessage(hwnd, LB_GETCURSEL, 0, 0L);
		programEdit.currentCutsNum = cursel;

		updateProgramDataStep(mainhwnd, &programEdit);

		break;
	}
}

static void profileListCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	int cursel;
	int orig_id = programCreate.proHeader.id;
	HWND mainhwnd = GetMainWindowHandle(hwnd);
	struct programe_link_t *pPro;

	switch(nc){
	case LBN_SETFOCUS:
		if(isIMEOpenInMotor == 1){		// Close IME
			ShowWindow(myIMEWindow, SW_HIDE);
			isIMEOpenInMotor = 0;
		}
		break;

	case LBN_ENTER:
		cursel = SendMessage(hwnd, LB_GETCURSEL, 0, 0L);
		pPro = getProfileListItem(cursel);
#if 1
		printf("Select File: sel=%d id=%d orig=%d\n",cursel, pPro->prog.id, orig_id);
#endif
		memcpy(&programCreate.proHeader,&(pPro->prog), sizeof(struct userProFileHeader_t));
		programCreate.isFileNew = pPro->prog.isnull;

		getFilePathName(programCreate.filePathname, pPro->prog.id);

		/* change currentuser->recent_programe flag */
		currentuser->recent_programe = pPro->prog.id;

		/* load program file and enter MD_EDIT mode;
		 *
		 */
		if(orig_id != pPro->prog.id) {
			reloadProgramFile(mainhwnd, &programEdit, &programCreate);
		}

		/* Change Mode to MD_EDIT */
		SendMessage(mainhwnd, MSG_KEYDOWN, SCANCODE_EDIT, 0L);

		break;
	}
}

/* update two structure:
 *    programFileAttr:
 *    programFileAttrData:
 *
 */
static int getFileAttrData(HWND hwndFileList, int cursel)
{
	struct programe_link_t *pPro;
	int cur_id = programCreate.proHeader.id;
	int ret;

	pPro = getProfileListItem(cursel);

#if 1
	printf("Select File: sel=%d id=%d curID=%d\n",cursel, pPro->prog.id, cur_id);
#endif

	if(cur_id == pPro->prog.id) {
		memcpy(&programFileAttr, &programCreate, sizeof(struct fromProListToPro_t));
		memcpy(&programFileAttrData, &programEdit, sizeof(userProgramEditData));
	} else {
		/* Not the same file */
		/* First read file header */
		memcpy(&programFileAttr.proHeader,&(pPro->prog), sizeof(struct userProFileHeader_t));
		programFileAttr.isFileNew = pPro->prog.isnull;
		getFilePathName(programFileAttr.filePathname, pPro->prog.id);

		/* Then get the file data */

		memset(&programFileAttrData, 0, sizeof(userProgramEditData));

		/* load the file */
		if(programFileAttr.isFileNew){              /* file is empty -> new file */
			programFileAttrData.nInputCutsNum = 1;    /* only 0.0 is displayed */
			programFileAttrData.currentCutsNum = 0;
			programFileAttrData.isProNameNull = 1;    /* program name is NULL */
			/* clear all data in this file */
			memset(programFileAttrData.userProCuts, 0, sizeof(programFileAttrData.userProCuts));
		} else {
			/* load program data */
			ret = loadUserProgramFromFile(programFileAttr.filePathname,
						      &(programFileAttr.proHeader), &programFileAttrData);
			programFileAttrData.nInputCutsNum = ret - 2;
		}
	}

	return pPro->prog.id;

}

/*  Only update the file NAME to the program link list.
 *  called after the showProgramFileAttr window is over.
 */
static int setFileAttrData(HWND hwndFileList, int cursel)
{
	struct programe_link_t *pPro;

	pPro = getProfileListItem(cursel);

	if(programFileAttr.proHeader.id == pPro->prog.id) {
		memcpy(&(pPro->prog), &programFileAttr.proHeader, sizeof(struct userProFileHeader_t));
	}

	return pPro->prog.id;
}


static void userListEditboxCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainhwnd = GetMainWindowHandle(hwnd);

	switch(nc){
	case EN_ENTER:
#if 0
		printf("+++ current select user %d\n",current_highlight_user);
#endif
		actionUserModeEnterKeyPressed(mainhwnd, &userModeStatus);
		break;
	default:
		/* Do nothing */
		break;
	}
}


static int InitUserProDialogBoxProc (HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND  hWnd;
	int currentSel;
	int curPos;
	int i, ret;
	char cmdRecvBuf[13];
	char timeBuff[30];
	int frontlimit;
	int unit = currentuser->unit;

	HWND  hwndProName = GetDlgItem(hDlg,IDC_PROGRAMNAME);
	HWND  hwndInputCutData = GetDlgItem(hDlg,IDC_INPUTCUTDATA);
	HWND  hwndProCutList = GetDlgItem(hDlg,IDL_PROGRAMCUTSLIST);
	HWND  hwndProCutListHead = GetDlgItem(hDlg,IDC_PROGRAMCUTSLISTHEAD);
	HWND  hwndCurrentPos = GetDlgItem(hDlg,IDC_CURPOS);
	HWND  hwndCutCounter = GetDlgItem(hDlg,IDC_CUTCOUNTER);
	HWND  hwndClock = GetDlgItem(hDlg,IDC_TIME);
	HWND  hwndFileList = GetDlgItem(hDlg,IDC_PROFILE_LIST);
	HWND  hwndFileListHead = GetDlgItem(hDlg,IDC_PROFILE_HEAD);

	HWND  hwndButtonEnter, hwndButtonClear, hwndButtonRun;
	HWND hwndtemp;

	switch (message) {
	case MSG_INITDIALOG:
		SetNotificationCallback(hwndInputCutData, editInputCutDataCallback);
		/* SetNotificationCallback(hwndProName, editProgramNameCallback); */
		SetNotificationCallback(hwndFileList, profileListCallback);
		SetNotificationCallback(hwndProCutList, proCutListCallback);

		for(i = 0; i < MAX_USER_NUMBER; i++){
			hwndtemp = GetDlgItem(hDlg, IDC_USER1_NAME + i);
			SetNotificationCallback(hwndtemp, userListEditboxCallback);
		}

		if(unit){
			SetDlgItemText(hDlg,IDC_PROGRAMCUTSLISTHEAD,
				       userpro_help[*curSysLang].msg_cutslist_header_inch);
		} else {
			SetDlgItemText(hDlg,IDC_PROGRAMCUTSLISTHEAD,
				       userpro_help[*curSysLang].msg_cutslist_header);
		}

		/* set window fonts */
		SetWindowFont(hwndInputCutData,userFont24);
		SetWindowFont(hwndProCutList,userFont24);
		/* SetWindowFont(hwndCurrentPos,userFont24); */
		/* SetWindowFont(hwndCutCounter,userFont24); */

		/* set font color */
		SetWindowElementColorEx(hwndInputCutData,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 0, 0)); //Red
		SetWindowElementColorEx(hwndCurrentPos,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 255, 0)); //Yellow
		SetWindowElementColorEx(hwndClock,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 255, 255)); //White
		SetWindowElementColorEx(hwndCutCounter,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 0, 0)); //Red

		userProExitFlag = STAY_HERE;

		SetTimer(hDlg,IDC_TIMER, 100); /* timer set to 100ms */

		/* init the window, display data */
		initUserProEditDlg(hDlg,&programEdit,&programCreate);
		curPos = motorStatus.currentCutPos;

		/* display the current pos */
		refreshCurrentPos(hwndCurrentPos,proRunStatus.forCutLen);

		motorStatus.cutCounter = loadCutCounter();
		refreshCutCounter(hDlg);
		/* ShowWindow(hwndCutCounter,SW_HIDE); */

		/* update and save the file used date and used times */
		UpdateFileHeader(&programCreate,&programEdit,1);
		saveUserProgramToFile(programCreate.filePathname,&(programCreate.proHeader),&programEdit);

		/* init listbox icon structure */
		myicons.dwIcons[0] = (DWORD)&flagicon[0];
		myicons.dwIcons[1] = (DWORD)&flagicon[1];
		myicons.dwIcons[2] = (DWORD)&flagicon[2];
		myicons.dwIcons[3] = (DWORD)&flagicon[3];
		myicons.dwIcons[4] = (DWORD)&flagicon[4];
		myicons.dwIcons[5] = (DWORD)&flagicon[1];
		myicons.dwIcons[6] = (DWORD)&flagicon[2];
		myicons.dwIcons[7] = (DWORD)&flagicon[3];
		SendMessage(hwndProCutList,LB_SETICONDATA, 0, (DWORD)&myicons);

		/* Create button controls
		 * The Fn buttons in all modes are share the same button.
		 * But when mode changed, the BITMAP will be changed by SET_IMAGE message
		 */
		hwndButtonEditFn[0] =
			CreateWindow (CTRL_BUTTON,
				      " F1 ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				      IDC_BUTTON_EDIT_F1,
				      592, 122, 42, 42, hDlg, (DWORD)(&btBmpEditFn[0]));

		hwndButtonEditFn[1] =
			CreateWindow (CTRL_BUTTON,
				      " F2 ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				      IDC_BUTTON_EDIT_F2,
				      592, 168, 42, 42, hDlg, (DWORD)(&btBmpEditFn[1]));
		hwndButtonEditFn[2] =
			CreateWindow (CTRL_BUTTON,
				      " F3 ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				      IDC_BUTTON_EDIT_F3,
				      592, 214, 42, 42, hDlg, (DWORD)(&btBmpEditFn[2]));
		hwndButtonEditFn[3] =
			CreateWindow (CTRL_BUTTON,
				      " F4 ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				      IDC_BUTTON_EDIT_F4,
				      592, 260, 42, 42, hDlg, (DWORD)(&btBmpEditFn[3]));
		hwndButtonEditFn[4] =
			CreateWindow (CTRL_BUTTON,
				      " F5 ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				      IDC_BUTTON_EDIT_F5,
				      592, 306, 42, 42, hDlg, (DWORD)(&btBmpEditFn[4]));
		hwndButtonEditFn[5] =
			CreateWindow (CTRL_BUTTON,
				      " F6 ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				      IDC_BUTTON_EDIT_F6,
				      592, 352, 42, 42, hDlg, (DWORD)(&btBmpEditFn[5]));

		/* left side buttons */
		hwndButtonCommon[0] =
			CreateWindow (CTRL_BUTTON,
				      " User ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				      IDC_BUTTON_USERS,
				      06, 122, 42, 42, hDlg, (DWORD)(&btBmpCommonLeft[0]));
		hwndButtonCommon[1] =
			CreateWindow (CTRL_BUTTON,
				      " ProLst ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				      IDC_BUTTON_PROLIST,
				      06, 168, 42, 42, hDlg, (DWORD)(&btBmpCommonLeft[1]));
		hwndButtonCommon[2] =
			CreateWindow (CTRL_BUTTON,
				      " Edit ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				      IDC_BUTTON_MD_EDIT,
				      06, 214, 42, 42, hDlg, (DWORD)(&btBmpCommonLeft[2]));
		hwndButtonCommon[3] =
			CreateWindow (CTRL_BUTTON,
				      " Auto ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				      IDC_BUTTON_MD_AUTO,
				      06, 260, 42, 42, hDlg, (DWORD)(&btBmpCommonLeft[3]));
		hwndButtonCommon[4] =
			CreateWindow (CTRL_BUTTON,
				      " Manual ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				      IDC_BUTTON_MD_MANUAL,
				      06, 306, 42, 42, hDlg, (DWORD)(&btBmpCommonLeft[4]));
		hwndButtonCommon[5] =
			CreateWindow (CTRL_BUTTON,
				      " Training ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				      IDC_BUTTON_MD_TRAINING,
				      06, 352, 42, 42, hDlg, (DWORD)(&btBmpCommonLeft[5]));

		/* Enter, Clear, Run buttons */
		hwndButtonEnter =
			CreateWindow (CTRL_BUTTON,
				      " enter ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				      IDC_BUTTON_ENTER,
				      485, 406, 32, 32, hDlg, (DWORD)(&btBmpEnter));
		hwndButtonClear =
			CreateWindow (CTRL_BUTTON,
				      " clear ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				      IDC_BUTTON_CLEAR,
				      521, 406, 32, 32, hDlg, (DWORD)(&btBmpClear));
		hwndButtonRun =
			CreateWindow (CTRL_BUTTON,
				      " run ",
				      WS_CHILD | BS_PUSHBUTTON | BS_BITMAP | BS_NOTIFY | WS_VISIBLE | BS_TOUCH_STYLE,
				      IDC_BUTTON_RUN,
				      557, 406, 32, 32, hDlg, (DWORD)(&btBmpRun));

		/* Hide Program file listbox */
		ShowWindow(hwndFileListHead, SW_HIDE);
		ShowWindow(hwndFileList, SW_HIDE);

#if 0
		ShowWindow(hwndProCutList, SW_HIDE);
		ShowWindow(hwndProCutListHead, SW_HIDE);
#endif
		/* set focus to data input edit box */
		SetFocusChild(hwndInputCutData);

		return 1;

	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}
		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initUserproEditDlgBkgnd(hDlg,hdc,&proRunStatus);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}

	case MSG_TIMER:
		convertSysTime(timeBuff);
		SetDlgItemText(hDlg, IDC_TIME, timeBuff);
		return 0;

	case MSG_CHAR:
		ret = LOWORD(wParam);
		if((ret > 0x2b) &&(ret < 0x3e) ){ /* it is number key or "." */
			if(mannualEditFlag == 1){ /* clear the editbox everytime it changed */
				SetWindowText(hwndInputCutData,"");
				mannualEditFlag = 0;
			}
		}
		break;

	case MSG_KEYDOWN:
		hdc=BeginPaint(hDlg);
		switch(LOWORD(wParam)){
		case SCANCODE_F1:
			/* Functions:
			 *   Edit Mode:   label edit
			 *   Auto Mode:   Clear cut counter
			 *   Manual Mode: Machine Maintainence
			 *   MD_PROLIST:  show Program file Attributes
			 *   MD_USER:     Create a new user
			 */
			if(proRunStatus.runStop > 0){    /* motor is running, edit is NOT allowed */
				break;
			}

			if(proRunStatus.runStatus == MD_EDIT){        /* edit mode */

				hWnd=GetFocusChild(hDlg);       /* only do when focus is right */
				if(hWnd != hwndInputCutData){
					printf("cur focus error \n");
					break;
				}

				if(programEdit.userEditStatus == 0){
					programEdit.userEditStatus = 1;  /* edit method is label */
					currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
					programEdit.currentCutsNum = currentSel;
					enterLableEdit(hDlg);
					//printf("enterLableEdit exit...%d \n",programEdit.lableEdit.lableStatus);
					if(programEdit.lableEdit.lableStatus == 7){
						/* Edit OK, update to listbox */
						UpdateProListWithFileData(hDlg,&programEdit);
					}
					programEdit.lableEdit.lableStatus = 0;
					ResetUserProgramEdit(hDlg,&programEdit,0);
					SetFocusChild(hwndInputCutData);
				}
				break;
			}

			if(proRunStatus.runStatus == MD_MANUAL) {


				break;
			}

			if(proRunStatus.runStatus == MD_AUTO) {
				/* AutoRun mode, clear the cut counter */
				resetCutCounter(hDlg);
				break;
			}

			if(proRunStatus.runStatus == MD_PROLIST) {
				currentSel = SendMessage(hwndFileList, LB_GETCURSEL, 0, 0L);

				getFileAttrData(hwndFileList, currentSel);
				showProgramFileAttr(hDlg);
				setFileAttrData(hwndFileList, currentSel);

				/* we should update the list, although this is heavy */
				refreshProgramListByDefault(hDlg, currentSel);

				/* update file name in this window */
				SetDlgItemText(hDlg, IDC_PROGRAMNAME, programCreate.proHeader.introduction);

				/* set focus back to list */
				SetFocusChild(hwndFileList);
				break;
			}

			if(proRunStatus.runStatus == MD_USER) {
				prepareToCreateUser(hDlg, &userModeStatus);
				break;
			}

			break;

		case SCANCODE_F2:
			/* Functions:
			 *   Edit Mode:   average edit
			 *   Auto Mode:
			 *   Manual Mode: Motor Config
			 *   MD_PROLIST:  UP
			 *   MD_USER:     Delete a user
			 */
			if(proRunStatus.runStop > 0){
				break;
			}

			if(proRunStatus.runStatus == MD_EDIT){
				hWnd=GetFocusChild(hDlg);
				if(hWnd != hwndInputCutData){
					break;
				}

				if(programEdit.userEditStatus == 0){
					programEdit.userEditStatus = 2; /* edit method is Average */
					currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
					programEdit.currentCutsNum = currentSel;
					enterAverageEdit(hDlg);
					if(programEdit.halfEdit.halfStatus == 3){
						UpdateProListWithFileData(hDlg,&programEdit);
					}
					programEdit.halfEdit.halfStatus = 0;
					ResetUserProgramEdit(hDlg,&programEdit,0);
					SetFocusChild(hwndInputCutData);
				}
				break;

			}

			if (proRunStatus.runStatus == MD_MANUAL){
				enterMachineConfig(hDlg);
				curPos = motorStatus.currentCutPos;
				refreshCurrentPosArea(hDlg,hdc,curPos,1);
				break;
			}

			if (proRunStatus.runStatus == MD_AUTO){
				/* AutoRun/Manual mode, clear the cut counter */
				motorStatus.cutCounter = 0;
				refreshCutCounter(hDlg);
				break;
			}

			if (proRunStatus.runStatus == MD_PROLIST){
				/* UP */
				SendMessage(hwndFileList, MSG_KEYDOWN, SCANCODE_CURSORBLOCKUP,0);
				SetFocusChild(hwndFileList);   /* The MSG_KEYDOWN will change the focus,
								* so set it back.
								*/
			}

			if(proRunStatus.runStatus == MD_USER) { /* delete selected user */
				DeleteOneUser(hDlg, current_highlight_user, &userModeStatus);
				break;
			}

			break;

		case SCANCODE_F3:
			/* Functions:
			 *   Edit Mode:   modify AIR flag
			 *   Auto Mode:   base setup
			 *   Manual Mode: base setup
			 *   MD_PROLIST:  DOWN
			 *   MD_USER:     AaBbCc IME
			 */
			if(proRunStatus.runStop > 0){
				break;
			}

			/* change air flag */
			if((proRunStatus.runStatus == MD_EDIT) && (programEdit.userEditStatus == 0)){
				hWnd=GetFocusChild(hDlg);
				if(hWnd != hwndInputCutData)
					break;

				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				programEdit.currentCutsNum = currentSel;
				curPos = programEdit.userProCuts[currentSel];
				changeAirFlagOfData(hDlg,&programEdit);
				break;
			}

			if((proRunStatus.runStatus == MD_MANUAL) || (proRunStatus.runStatus == MD_AUTO)){

				setCutPos(hDlg);
				break;

			}

			if (proRunStatus.runStatus == MD_PROLIST){
				/* DOWN */
				SendMessage(hwndFileList, MSG_KEYDOWN, SCANCODE_CURSORBLOCKDOWN,0);
				SetFocusChild(hwndFileList);
				break;
			}

			if (proRunStatus.runStatus == MD_USER){
				/* IME: ABC */
				showIMEWindow(1, IME_ABC);
				break;
			}

			break;

		case SCANCODE_F4:
			/* Functions:
			 *   Edit Mode:   modify PUSHER flag
			 *   Auto Mode:
			 *   Manual Mode: clear counter
			 *   MD_PROLIST:  Array list by date
			 *   MD_USER:     123 IME
			 */
			if(proRunStatus.runStop > 0)
				break;

			if((proRunStatus.runStatus == MD_EDIT) && (programEdit.userEditStatus == 0)){
				hWnd=GetFocusChild(hDlg);
				if(hWnd != hwndInputCutData)
					break;

				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				programEdit.currentCutsNum = currentSel;
				changePusherFlagOfData(hDlg,&programEdit);
				break;
			}


			if(proRunStatus.runStatus == MD_MANUAL){
				resetCutCounter(hDlg);
				break;
			}

			if(proRunStatus.runStatus == MD_PROLIST){
				listProgramFileByDate(hDlg, 0);
				break;
			}

			if (proRunStatus.runStatus == MD_USER){
				/* IME: 123 */
				printf("User Mode: change to IME NUMBER mode\n");
				showIMEWindow(1, IME_NUMBER);
				break;
			}

			break;

		case SCANCODE_F5:
			/* Functions:
			 *   Edit Mode:   modify AUTOCUT flag
			 *   Auto Mode:   RFU
			 *   Manual Mode: Sensor Status
			 *   MD_PROLIST:  Array list by Num
			 *   MD_USER:     pinyin IME
			 */
			if(proRunStatus.runStop > 0)
				break;

			if((proRunStatus.runStatus == MD_EDIT) && (programEdit.userEditStatus == 0)){
				hWnd=GetFocusChild(hDlg);
				if(hWnd != hwndInputCutData)
					break;

				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				programEdit.currentCutsNum = currentSel;
				curPos = programEdit.userProCuts[currentSel];
				if(curPos & AUTOCUT_FLAG){
					delAutoCutFlagFromData(hDlg,&programEdit);
				} else {
					addAutoCutFlagToData(hDlg,&programEdit);
				}
				break;
			}

			if(proRunStatus.runStatus == MD_PROLIST){
				listProgramFileByNum(hDlg, 0);
				break;
			}

			if (proRunStatus.runStatus == MD_USER){
				/* IME: 123 */
				showIMEWindow(1, IME_PINYIN);
				break;
			}

			if (proRunStatus.runStatus == MD_MANUAL){
				/* === TODO: enter configure window */
				/* EnterConfigureUser(hDlg); */
				/* curPos = motorStatus.currentCutPos; */
				/* refreshCurrentPosArea(hDlg,hdc,curPos,1); */

				break;
			}

			break;

		case SCANCODE_F6:
			/* Functions:
			 *   Edit Mode:   computer
			 *   Auto Mode:   RFU
			 *   Manual Mode: Computer
			 *   MD_PROLIST:  Back to MD_EDIT
			 *   MD_USER:     Back to MD_EDIT
			 */
			if(proRunStatus.runStop > 0){
				break;
			}

			if((proRunStatus.runStatus == MD_EDIT) || (proRunStatus.runStatus == MD_MANUAL)) {
				hWnd=GetFocusChild(hDlg);
				if(hWnd != hwndInputCutData)
					break;

				EnterComputer(hDlg);
				char *pBuf = computerData.inputBuf;
				int resultLen = strlen(pBuf);
				/* dispaly the result to the input box */
				SetWindowText(hwndInputCutData,pBuf);
				/* set the caret to the last so that user can input */
				SendMessage(hwndInputCutData,EM_SETCARETPOS,0,resultLen);
				break;
			}

			if(proRunStatus.runStatus == MD_PROLIST){
				SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_EDIT, 0L);
			}
			break;

		case SCANCODE_ADD:     /* data just: Add */
			if(proRunStatus.runStop > 0){
				break;
			}
			if((proRunStatus.secondFunc == 1)&&(proRunStatus.runStatus == 0)
			   &&(programEdit.userEditStatus == 0)){
				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				programEdit.currentCutsNum = currentSel;
				curPos = programEdit.userProCuts[currentSel];
				if(curPos & AUTOCUT_FLAG){
					delAutoCutFlagFromData(hDlg,&programEdit);
				} else {
					addAutoCutFlagToData(hDlg,&programEdit);
				}
				break;
			}

			if((proRunStatus.runStatus == 0)&&(programEdit.userEditStatus == 0)){
				/* Add the input data to the last data, the sum is new */
				programEdit.userEditStatus = 8;
			}else if((proRunStatus.runStatus == 0)&&(programEdit.userEditStatus == 8)){
				// Exit this mode
				programEdit.userEditStatus = 0;
				RefreshBoxArea (hdc,&userProgramEditBkgnd,88,395,40,40,88,395); // clear "+" flag
			}
			if((proRunStatus.runStatus == 2)&&(proRunStatus.runStop == 0)){
				if(proRunStatus.mannualFlag == 0){
					proRunStatus.mannualFlag = 1;
				}else if(proRunStatus.mannualFlag == 1){
					proRunStatus.mannualFlag = 0;
					ResetUserProgramEdit(hDlg,&programEdit,0); //Clear Add flag
				}
				break;
			}
			break;

		case SCANCODE_SUB:      /* sub */
			if(proRunStatus.runStop > 0){           // Running
				break;
			}
			if((proRunStatus.secondFunc == 1)&&(proRunStatus.runStatus == 0)
			   &&(programEdit.userEditStatus == 0)){
				/* change all the data's autocut flag */
				autoCutChangeFlag ++;
				if(autoCutChangeFlag%2){
					addAutoCutFlagToAllDatas(hDlg,&programEdit);
				}else{
					delAutoCutFlagFromAllDatas(hDlg,&programEdit);
				}
				break;
			}

			if((proRunStatus.runStatus == 0)&&(programEdit.userEditStatus == 0)){
				programEdit.userEditStatus = 9;
			}else if((proRunStatus.runStatus == 0)&&(programEdit.userEditStatus == 9)){
				// Exit this mode
				programEdit.userEditStatus = 0;
				RefreshBoxArea (hdc,&userProgramEditBkgnd,88,395,40,40,88,395); // clear "-" flag
			}

			if((proRunStatus.runStatus == 2)&&(proRunStatus.runStop == 0)){
				if(proRunStatus.mannualFlag == 0){
					proRunStatus.mannualFlag = 2;
				}else if(proRunStatus.mannualFlag == 2){
					proRunStatus.mannualFlag = 0;
					ResetUserProgramEdit(hDlg,&programEdit,0); //Clear Add flag
				}
				break;
			}
			break;

		case SCANCODE_MODIFY:  /* modify the current data */
			/* Functions:
			 *   Edit Mode:   Modify current data
			 *   Auto Mode:   RFU
			 *   Manual Mode: RFU
			 *   MD_USER:     Change user name
			 * Usage:
			 *   Input the data and press MODIFY key, the current selected data
			 *   in listbox will be changed
			 */
			if(proRunStatus.runStop > 0)
				break;

			if((proRunStatus.runStatus == MD_EDIT) && (programEdit.userEditStatus == 0)){
				hWnd=GetFocusChild(hDlg);
				if(hWnd != hwndInputCutData)
					break;

				programEdit.userEditStatus = 3;  /* set edit mothod to modify */
				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				programEdit.currentCutsNum = currentSel;

				modifyOneCutData(hDlg,&programEdit);
				ResetUserProgramEdit(hDlg,&programEdit,0);
				SetFocusChild(hwndInputCutData);
				break;
			}

			if(proRunStatus.runStatus == MD_USER) {
				prepareToModifyUserName(hDlg, current_highlight_user, &userModeStatus);
				break;
			}

			break;

		case SCANCODE_INSERTDATA:
			/* Functions:
			 *   Edit Mode:   Modify current data.
			 *                IME switch
			 *   Auto Mode:   RFU
			 *   Manual Mode: RFU
			 * Usage:
			 *   Input the data then press insert key, the data will be inserted
			 *   to the before current item.
			 */
			if(proRunStatus.runStatus > 0)
				break;

			hWnd=GetFocusChild(hDlg);
			if(hWnd != hwndInputCutData){
				if(hWnd == hwndProName){
					/* Now used as IME change KEY */
					break;
				}
				SetFocusChild(hwndInputCutData);
				break;
			}

			if((proRunStatus.runStatus == MD_EDIT) && (programEdit.userEditStatus == 0)){
				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				programEdit.currentCutsNum = currentSel;
				programEdit.userEditStatus = 4; /* set edit method to insert */

				/* insert inputed data to current position */
				insertOneCutData(hDlg,&programEdit);
				ResetUserProgramEdit(hDlg,&programEdit,0);
				SetFocusChild(hwndInputCutData);

				/*
				 * SCANCODE_INSERTDATA is also used to change the IME type,
				 * so back to number input mode
				 */
				showIMEWindow(0, IME_NUMBER);
			}

			break;

		case SCANCODE_HELP:  /* get help */

		case SCANCODE_DELETE:
			if(proRunStatus.runStop > 0)
				break;

			if(proRunStatus.runStatus == MD_EDIT){

				if( proRunStatus.secondFunc == 1){       /* shift key active */
					releaseSecondFunction(hDlg);
					currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
					programEdit.currentCutsNum = currentSel;
					//deleteAllCutDatas(hDlg,&programEdit);
					deleteBehindCutDatas(hDlg,&programEdit);

				} else {
					programEdit.userEditStatus = 5;
					currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
					int totalCount = SendMessage(hwndProCutList,LB_GETCOUNT,0,0);
					programEdit.currentCutsNum = currentSel;
					if(currentSel < (totalCount-1)){
						deleteOneCutData(hDlg,&programEdit);
					} else if(currentSel == (totalCount-1)){ /* The last 0.0 can NOT be deleted */
						SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
						programEdit.userEditStatus = 0;
					}
				}
				break;
			}

			if(proRunStatus.runStatus == MD_PROLIST){
				currentSel = SendMessage(hwndFileList, LB_GETCURSEL, 0, 0);
				deleteOneProFile(hDlg, currentSel);
				break;
			}
			break;

		case SCANCODE_PUSHER:
			/* Functions:
			 *   Edit Mode:   modify PUSHER flag (shift = 0)
			 *                Enter User Mode(shift = 1) -> MD_USER
			 *   Auto Mode:   RFU
			 *   Manual Mode: RFU
			 *   MD_PROLIST:  RFU
			 *   MD_USER:     RFU
			 */
			if(proRunStatus.runStop > 0)
				break;

			if (proRunStatus.secondFunc == 0){
				if((proRunStatus.runStatus == MD_EDIT) && (programEdit.userEditStatus == 0)){
					hWnd = GetFocusChild(hDlg);
					if(hWnd != hwndInputCutData)
						break;

					currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
					programEdit.currentCutsNum = currentSel;
					changePusherFlagOfData(hDlg,&programEdit);
				}
				break;
			} else {/* shift key valid */
				/* USER Mode can ONLY be enter from EDIT or PROFILE mode */
				if(is_mdc_locked(&mode_change_lock))
					break;

				if(proRunStatus.runStatus == MD_USER){
					/* Already in MD_USER, do nothing */
					releaseSecondFunction(hDlg);
					break;
				}
				if((proRunStatus.runStatus == MD_EDIT) || (proRunStatus.runStatus == MD_PROLIST)) {
					ShowWindow(hwndProCutList, SW_HIDE);
					ShowWindow(hwndProCutListHead, SW_HIDE);

					ShowWindow(hwndFileList, SW_HIDE);
					ShowWindow(hwndFileListHead, SW_HIDE);

					current_highlight_user = currentid;  /* update this filed */

					updateUsersList(hDlg, current_highlight_user, USERLIST_DEFAULT);
					proRunStatus.lastRunStatus = proRunStatus.runStatus;
					proRunStatus.runStatus = MD_USER;

					resetMotorRunStatus(&proRunStatus);
					InvalidateRect(hDlg,NULL,TRUE);
					refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);

					showUserModeFnButtons();
					showModeButtons(MD_USER);

					updateCurrentModeStatus(hDlg, &proRunStatus);
				}

				releaseSecondFunction(hDlg);
				break;
			}
			break;

		case SCANCODE_MYEQUAL:  //  shift + equal -> Add Y flag
			if(proRunStatus.runStop > 0){   // Disable when RUNNING
				break;
			}
			if(proRunStatus.secondFunc != 1){
				break;
			}
			if((proRunStatus.runStatus == MD_EDIT) && (programEdit.userEditStatus == 0)){
				hWnd=GetFocusChild(hDlg);       // Only when focus in edit box
				if(hWnd != hwndInputCutData){
					break;
				}
				currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
				programEdit.currentCutsNum = currentSel;
				curPos = programEdit.userProCuts[currentSel];
				changePressSheetFlagOfData(hDlg,&programEdit);
			}
			releaseSecondFunction(hDlg);
			break;


		case SCANCODE_PAGEUP:
			if((proRunStatus.runStatus==MD_AUTO) || (proRunStatus.runStatus==MD_AUTOCUT) ||
			   (proRunStatus.runStatus==MD_PROLIST) || (isIMEOpenInMotor==1)){
				break;
			}
			hWnd=GetFocusChild(hDlg);
			if(hWnd == hwndInputCutData){
				SetFocusChild(hwndProCutList);   /* Give focus back to editbox in KEYUP msg */
			}
			break;

		case SCANCODE_PAGEDOWN:
			if((proRunStatus.runStatus==MD_AUTO) || (proRunStatus.runStatus==MD_AUTOCUT) ||
			   (proRunStatus.runStatus==MD_PROLIST) || (isIMEOpenInMotor==1)){
				break;
			}
			hWnd=GetFocusChild(hDlg);
			if(hWnd == hwndInputCutData){
				SetFocusChild(hwndProCutList);   /* Give focus back to editbox in KEYUP msg */
			}
			break;

		case SCANCODE_CURSORBLOCKUP:
			if((proRunStatus.runStatus==MD_AUTO) || (proRunStatus.runStatus==MD_AUTOCUT)){
				break;
			} else if(proRunStatus.runStatus == MD_PROLIST){
				/* the program file list mode, set focus to list */
				SetFocusChild(hwndFileList);
			} else if(proRunStatus.runStatus == MD_USER){
				updateUsersList(hDlg, current_highlight_user, USERLIST_SEL_UP);
				break;
			}

			hWnd=GetFocusChild(hDlg);
			if(hWnd == hwndInputCutData){
				SetFocusChild(hwndProCutList);
			}
			break;

		case SCANCODE_CURSORBLOCKDOWN:
			if((proRunStatus.runStatus==MD_AUTO) || (proRunStatus.runStatus==MD_AUTOCUT)){
				break;
			} else if(proRunStatus.runStatus == MD_PROLIST){
				/* the program file list mode, set focus to list */
				SetFocusChild(hwndFileList);

			} else if(proRunStatus.runStatus == MD_USER){
				updateUsersList(hDlg, current_highlight_user, USERLIST_SEL_DOWN);
				break;
			}

			hWnd=GetFocusChild(hDlg);
			if(hWnd == hwndInputCutData){
				SetFocusChild(hwndProCutList);
			}
			break;

		case SCANCODE_FORWARD:
			frontlimit = getValidLimit();
			if(motorStatus.currentCutPos > frontlimit){
				motorRunStep(MOTOR_FORWARD,0);
			}
			break;

		case SCANCODE_BACKWARD:
			if(motorStatus.currentCutPos < currentuser->backlimit){
				motorRunStep(MOTOR_BACKWARD,0);
			}
			break;

		case SCANCODE_FORWARD_HS:        /* Highspeed Backward */
			frontlimit = getValidLimit();
			if(motorStatus.currentCutPos > frontlimit){
				motorRunStep(MOTOR_FORWARD_HS,0);
			}
			break;

		case SCANCODE_BACKWARD_HS:      /* Highspeed Backward */
			if(motorStatus.currentCutPos < currentuser->backlimit){
				motorRunStep(MOTOR_BACKWARD_HS,0);
			}
			break;

		case SCANCODE_FUNCTION:  /* left shift */
			if(proRunStatus.secondFunc == 0){
				proRunStatus.secondFunc = 1;
			} else {
				proRunStatus.secondFunc = 0;
			}
			refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,3,0);
			break;


		case SCANCODE_MODE_CHANGE:
			/* === TODO === */
			/*
			 * Change Modes in roll:
			 *    shift=0:  MD_EDIT -> MD_AUTO -> MD_MANUAL -> MD_EDIT
			 *    shift=1:  MD_MANUAL -> MD_AUTO -> MD_EDIT -> MD_MANUAL
			 */
			break;

		case SCANCODE_PROG:
			/* Functions:
			 *   Edit Mode:   enter program list window in any mode
			 *   Auto Mode:   RFU
			 *   Manual Mode: RFU
			 */
			if(proRunStatus.runStop > 0)
				break;

			if(is_mdc_locked(&mode_change_lock))
				break;

			/* enter profile mode */
			if(proRunStatus.runStatus == MD_PROLIST){
				/* already in program file list mode */
				SetFocusChild(hwndFileList);    /* Just make sure */
				break;
			} else {
#if 0
				printf("Enter MD_PROLIST mode.\n");
#endif
				ShowWindow(hwndProCutList, SW_HIDE);
				ShowWindow(hwndProCutListHead, SW_HIDE);

				ShowWindow(hwndFileList, SW_SHOW);
				ShowWindow(hwndFileListHead, SW_SHOW);

				/* Hide User List Controls */
				updateUsersList(hDlg, current_highlight_user, USERLIST_HIDE_ALL);

				proRunStatus.lastRunStatus = proRunStatus.runStatus;
				proRunStatus.runStatus = MD_PROLIST;

				resetMotorRunStatus(&proRunStatus);
				/* InvalidateRect(hDlg,NULL,TRUE); */
				refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);

				showFileModeFnButtons();
				showModeButtons(MD_PROLIST);

				updateCurrentModeStatus(hDlg, &proRunStatus);


				if(!isFileListLoaded){
					isFileListLoaded = 1;
					initProgramList(hDlg, programCreate.proHeader.id - 1);
				}

				/* == TODO == */
				/* Set this focus here will make it enter to MD_EDIT from
				 * MD_USER directly
				 */
				SetFocusChild(hwndFileList);
			}

			break;

		case SCANCODE_EDIT: /* Edit Mode */
			/* Functions:
			 *   shift = 0:  enter Edit mode
			 *   shift = 1:  enter/exit Training mode
			 * Usage:
			 *   shift = 0: press edit key to enter Edit mode;
			 *   shift = 1: if current is Training mode, then exit and back
			 *              to Edit mode; If it is not, then enter.
			 */
			if(proRunStatus.runStop > 0)
				break;

#if 0
			printf("SCANCODE_EDIT pressed. Current Mode = %d.\n", proRunStatus.runStatus);
#endif

			if(is_mdc_locked(&mode_change_lock))
				break;

			if(isSecondFunctionValid()){
				/* enter/exit training mode */

				if(proRunStatus.runStatus == MD_EDIT){
					/* Enter from Edit mode */
					if(programEdit.userEditStatus == 0){

						resetMotorRunStatus(&proRunStatus);
						programEdit.userEditStatus = 7;

						/* regresh mode, make a trick here
						 * Trainig mode is NOT a really mode, it is a minor mode
						 * of Edit mode.
						 */
						proRunStatus.lastRunStatus = proRunStatus.runStatus;
						proRunStatus.runStatus = MD_TRAINING;
						updateCurrentModeStatus(hDlg, &proRunStatus);
						proRunStatus.runStatus = MD_EDIT;

					}else if(programEdit.userEditStatus == 7){  /* Exit Traing Mode */
						resetMotorRunStatus(&proRunStatus);
						programEdit.userEditStatus = 0;     /* back to normal edit */

						/* regresh mode  */
						updateCurrentModeStatus(hDlg, &proRunStatus);
					}

				} else if((proRunStatus.runStatus == MD_MANUAL) || (proRunStatus.runStatus == MD_AUTO)){
					/* Enter from Auto/Manual Mode
					 * So first we enter to Edit Mode
					 */

					resetMotorRunStatus(&proRunStatus);
					programEdit.userEditStatus = 7;

					proRunStatus.lastRunStatus = proRunStatus.runStatus;
					proRunStatus.runStatus = MD_EDIT;
					/* add the last "0.0" data item */
					addLastListItem(hwndProCutList,&programEdit,lastListItem);
					programEdit.currentCutsNum = 0;
					SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
					resetMotorRunStatus(&proRunStatus);
					InvalidateRect(hDlg,NULL,TRUE);
					/* update status  */
					refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);
					/* regresh mode  */
					proRunStatus.runStatus = MD_TRAINING;
					updateCurrentModeStatus(hDlg, &proRunStatus);
					proRunStatus.runStatus = MD_EDIT;

					/* show Fn buttons */
					showEditModeFnButtons();
				}

				/* release shift key */
				releaseSecondFunction(hDlg);
				break;
			}

			if (proRunStatus.runStatus == MD_EDIT){
				/* already in edit mode */

				/* But it also could be training mode */
				if(programEdit.userEditStatus == 7){  /* Exit Traing Mode */
					resetMotorRunStatus(&proRunStatus);
					programEdit.userEditStatus = 0;     /* back to normal edit */

					/* regresh mode  */
					updateCurrentModeStatus(hDlg, &proRunStatus);
				}

				/* Edit mode, shift=0 , do nothing */
				break;

			} else if((proRunStatus.runStatus == MD_MANUAL) || (proRunStatus.runStatus == MD_AUTO)){

				proRunStatus.lastRunStatus = proRunStatus.runStatus;
				proRunStatus.runStatus = MD_EDIT;
				/* add the last "0.0" data item */
				addLastListItem(hwndProCutList,&programEdit,lastListItem);
				programEdit.currentCutsNum = 0;
				SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
				resetMotorRunStatus(&proRunStatus);
				InvalidateRect(hDlg,NULL,TRUE);
				/* update status  */
				refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);
				/* regresh mode  */
				updateCurrentModeStatus(hDlg, &proRunStatus);
				/* show Fn buttons */
				showEditModeFnButtons();
				break;

			} else if((proRunStatus.runStatus == MD_PROLIST) || (proRunStatus.runStatus == MD_USER)){
				/* Usually we enter MD_EDIT from MD_PROLIST by pressing ENTER key,
				 * if use the EDIT key to enter MD_EDIT, it means just use the orignal
				 * program. So just show the ListBox and the Input Box.
				 */
				proRunStatus.lastRunStatus = proRunStatus.runStatus;
				proRunStatus.runStatus = MD_EDIT;

				ShowWindow(hwndFileList, SW_HIDE);
				ShowWindow(hwndFileListHead, SW_HIDE);

				ShowWindow(hwndProCutList, SW_SHOW);
				ShowWindow(hwndProCutListHead, SW_SHOW);

				updateUsersList(hDlg, current_highlight_user, USERLIST_HIDE_ALL);

				SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
				resetMotorRunStatus(&proRunStatus);
				InvalidateRect(hDlg,NULL,TRUE);
				/* update status  */
				refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);
				/* regresh mode  */
				updateCurrentModeStatus(hDlg, &proRunStatus);
				/* show Fn buttons */
				showEditModeFnButtons();
				showModeButtons(MD_EDIT);
			}
			break;

		case SCANCODE_MANUAL:  /* maunal mode */
			/* Functions:
			 *   Enter Manual Mode.
			 *
			 * Note:
			 *   We can ONLY enter this mode from MD_EDIT or MD_AUTO.
			 *   In MD_PROLIST and MD_USER, this key should be disabled.
			 */
			if(proRunStatus.runStop > 0)
				break;

			if(is_mdc_locked(&mode_change_lock))
				break;

			if(proRunStatus.runStatus == MD_MANUAL){
				/* already in manual mode */
				break;
			}

			if(proRunStatus.runStatus >= MD_PROLIST){
				/* MD_PROLIST or MD_USER */
				break;
			}

			if (proRunStatus.runStatus == MD_EDIT){
				proRunStatus.lastRunStatus = proRunStatus.runStatus;
				proRunStatus.runStatus = MD_MANUAL;
				exitTrainingMode(&programEdit);
				deleteLastListItem(hwndProCutList,&programEdit,lastListItem);
				programEdit.currentCutsNum = 0;
				SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
				resetMotorRunStatus(&proRunStatus);
				InvalidateRect(hDlg,NULL,TRUE);
				refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);
			}

			if (proRunStatus.runStatus == MD_AUTO){
				proRunStatus.lastRunStatus = proRunStatus.runStatus;
				proRunStatus.runStatus = MD_MANUAL;
				programEdit.currentCutsNum = 0;
				SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
				resetMotorRunStatus(&proRunStatus);
				InvalidateRect(hDlg,NULL,TRUE);
				refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);
			}

			/* regresh mode  */
			updateCurrentModeStatus(hDlg, &proRunStatus);
			/* show Fn buttons */
			showManualModeFnButtons();

			break;

		case SCANCODE_AUTORUN:  /* autorun mode */
			if(proRunStatus.runStop > 0) break;

			if(is_mdc_locked(&mode_change_lock))
				break;

			if(proRunStatus.runStatus == MD_AUTO){
				break;
			}

			if(proRunStatus.runStatus >= MD_PROLIST){
				/* MD_PROLIST or MD_USER */
				break;
			}

			if (proRunStatus.runStatus == MD_EDIT){
				proRunStatus.lastRunStatus = proRunStatus.runStatus;
				proRunStatus.runStatus = MD_AUTO;

				exitTrainingMode(&programEdit);
				deleteLastListItem(hwndProCutList,&programEdit,lastListItem);
				programEdit.currentCutsNum = 0;
				SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
				resetMotorRunStatus(&proRunStatus);
				InvalidateRect(hDlg,NULL,TRUE);
				refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);
			}
			if (proRunStatus.runStatus == MD_MANUAL){
				proRunStatus.lastRunStatus = proRunStatus.runStatus;
				proRunStatus.runStatus = MD_AUTO;
				programEdit.currentCutsNum = 0;
				SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
				resetMotorRunStatus(&proRunStatus);
				InvalidateRect(hDlg,NULL,TRUE);
				refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);
			}
			/* regresh mode  */
			updateCurrentModeStatus(hDlg, &proRunStatus);
			/* show Fn buttons */
			showAutoModeFnButtons();

			break;

		case SCANCODE_RUN:  /* run this program */
			/* handle in KEYUP message */
			break;

		case SCANCODE_ENTER:
			/* We take action in call back functions */
			break;

		case SCANCODE_CANCEL: /* clear */
			if((proRunStatus.runStatus == MD_AUTO) || (proRunStatus.runStatus == MD_AUTOCUT) ||
			   (proRunStatus.runStatus == MD_PROLIST) || (proRunStatus.runStatus == MD_USER)){
				break;
			}

			hWnd=GetFocusChild(hDlg);
			if(hWnd == hwndInputCutData){
				SetFocusChild(hwndInputCutData);
				SetWindowText(hWnd,"");
			}

			break;


#if 0
		case SCANCODE_HELP:  /* get help */

#if 0                   /* debug */
			currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
			programEdit.currentCutsNum = currentSel;

			showTrackBar(hDlg, &proRunStatus, &programEdit, 1);
			break;
#endif
			hWnd=GetFocusChild(hDlg);
			if(hWnd == hwndProName){
				MessageBox(hDlg,userpro_help[*curSysLang].help_userpro_name,
					   userpro_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			}else if(hWnd == hwndInputCutData){
				MessageBox(hDlg,userpro_help[*curSysLang].help_userpro_cuts,
					   userpro_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
			}

			break;
#endif
		}
		EndPaint(hDlg,hdc);
		break;

	case MSG_KEYUP:
		hdc=BeginPaint(hDlg);
		switch(LOWORD(wParam)){
		case SCANCODE_F6:      /* save data to file */

			break;

			/* === TODO: not used now */
			if(!isFunctionButtonValid(&proRunStatus)){
				break;
			}

			hWnd=GetFocusChild(hDlg);    /* focus is in edit box, not else */
			if(hWnd != hwndInputCutData){
				break;
			}
			printf("Save:%s\n",programCreate.filePathname);
			/* first update file header */
			UpdateFileHeader(&programCreate,&programEdit,0);
			saveUserProgramToFile(programCreate.filePathname,&(programCreate.proHeader),&programEdit);

			/* save lastuser and last program */
			saveUserConfigFile(0);
			/* refresh lastuser as current user */
			startupParams.lastuser = currentid;
			saveInitConfigFile(&startupParams);

			MessageBox(hDlg,userpro_help[*curSysLang].msg_save_pro_ok,
				   userpro_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
			SetFocusChild(hwndInputCutData);
			break;

		case SCANCODE_CURSORBLOCKUP:   /* UP arrow */
			if((proRunStatus.runStatus==MD_AUTO) || (proRunStatus.runStatus==MD_AUTOCUT) ||
			   (proRunStatus.runStatus==MD_PROLIST) || (proRunStatus.runStatus==MD_USER)){
				/* disabled in Autorun mode */
				break;
			}

			/* set focus back to edit box after listbox item move */
			SetFocusChild(hwndInputCutData);
			break;

		case SCANCODE_CURSORBLOCKDOWN: /* DOWN arrow */
			if((proRunStatus.runStatus==MD_AUTO) || (proRunStatus.runStatus==MD_AUTOCUT) ||
			   (proRunStatus.runStatus==MD_PROLIST) || (proRunStatus.runStatus==MD_USER)){
				break;
			}

			SetFocusChild(hwndInputCutData);
			break;

		case SCANCODE_PAGEUP:
			if((proRunStatus.runStatus==MD_AUTO) || (proRunStatus.runStatus==MD_AUTOCUT) ||
			   (proRunStatus.runStatus==MD_PROLIST) || (proRunStatus.runStatus==MD_USER) ||
			   (isIMEOpenInMotor==1)){
				break;
			}

			SetFocusChild(hwndInputCutData);
			break;

		case SCANCODE_PAGEDOWN :
			if((proRunStatus.runStatus==MD_AUTO) || (proRunStatus.runStatus==MD_AUTOCUT) ||
			   (proRunStatus.runStatus==MD_PROLIST) || (proRunStatus.runStatus==MD_USER) ||
			   (isIMEOpenInMotor==1)){
				break;
			}

			SetFocusChild(hwndInputCutData);
			break;

		case SCANCODE_RUN:    /* run this program */

			if(proRunStatus.machineStop == 0){      // Running
				break;
			}

			/* In training mode, this key is enabled */
			if((proRunStatus.runStatus == 0) && (programEdit.userEditStatus != 7)){
				break;
			}

			/* printf("Run status =%d %d \n",proRunStatus.runStatus,programEdit.nInputCutsNum); */
			if(proRunStatus.runStatus == MD_AUTO){
				if(programEdit.nInputCutsNum == 1){
					/* No program data to run */
					break;
				}else{
					/* save this program automatically */
					UpdateFileHeader(&programCreate,&programEdit,0);
					saveUserProgramToFile(programCreate.filePathname,
							      &(programCreate.proHeader),&programEdit);
				}
			}

			if((proRunStatus.runStatus == MD_MANUAL) &&(proRunStatus.runStop == 0)){
				proRunStatus.runStop = 1;   /* set machine is running */
			}

			if(proRunStatus.runStatus == MD_MANUAL){    /* handle the edit flag in manual mode */
				mannualEditFlag = 1;
				if(proRunStatus.mannualFlag){  /* add or sub */
					ResetUserProgramEdit(hDlg,&programEdit,0);
				}
			}

			currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
			SendMessage(hwndProCutList,LB_SETCURSEL,currentSel,0);
			programEdit.currentCutsNum = currentSel;
			proRunStatus.destCutNum = currentSel;
			refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,1,0);

			proRunStatus.cutActTimes = 0;
			proRunStatus.pusherActTimes = 0;
			proRunStatus.cutPusherActTimes = 0;
			proRunStatus.isOneCutDone = 1;     /* emulate the last cut is done */
			proRunStatus.machineStop = 0;
			//printf("motorRunProcess %d \n",currentSel);
			motorRunProcess(hDlg,&programEdit,&proRunStatus);  /* begin to run this program */
			break;

		}

		EndPaint(hDlg,hdc);
		break;

	case MSG_RECV:  /* got data from machine */

		recvCommand(cmdRecvBuf); /* First copy to user space */

		/* printf("MSG_RECV: %s\n",cmdRecvBuf); */

		// For test
		//strcpy(cmdRecvBuf,testbuffer);
		//ret = isCmdValid(cmdRecvBuf);  /* do checksum */
		//if(ret){
		memset(&cmdData,0,sizeof(cmdData));
		getCmdFrame(&cmdData,cmdRecvBuf);   /* get data */
		if(cmdData.flag == 'G'){            /* data frame */
			hdc=BeginPaint(hDlg);
			if(unit){
				cmdData.data = mm_to_inch(cmdData.data);
			}
			if( motorStatus.currentCutPos != cmdData.data){
				/* only refresh data if it is changed */
				motorStatus.currentCutPos = cmdData.data;
				refreshCurrentPosArea(hDlg,hdc,cmdData.data,0);
				/* printf("Refresh forCutLen...\n"); */
				refreshForCutLenArea(hwndCurrentPos,&proRunStatus,cmdData.data);
			}

			/* get machine status */
			if(motorStatus.devStatus != cmdData.status){
				motorStatus.devStatus = cmdData.status;  /* update status */
				ret = getDevStatus(&proRunStatus,cmdData.status);
				refreshCurrentPos(hwndCurrentPos,proRunStatus.forCutLen);
				/* regresh pusher and cut status */
				refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,4,0);
				/* refresh run/stop status */
				refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,1,0);
				if(ret){
					motorStatus.cutCounter++;
					refreshCutCounter(hDlg);
					if((proRunStatus.runStatus == MD_EDIT)&&(programEdit.userEditStatus == 7)){
						/* in trainig mode, record this data */
						editOneCutData(hDlg,&programEdit,motorStatus.currentCutPos);
						resetMotorRunStatus(&proRunStatus);
					}
					if(proRunStatus.runStatus == MD_MANUAL){
						/* in maual mode, reset for next run */
						resetMotorRunStatus(&proRunStatus);
					}
				}

			}
			EndPaint(hDlg,hdc);

			/* in Autorun mode, go to next data */
			if(proRunStatus.runStatus == MD_AUTO){
				motorRunAuto(hDlg,&programEdit,&proRunStatus);
			}
		}
		//}
		break;

	case MSG_COMMAND:
		switch(wParam){
		/*
		 * Right side F1 - F6
		 */
		case IDC_BUTTON_EDIT_F1:
			printf("IDC_BUTTON_EDIT_F1 push\n");
			SetFocusChild(hwndInputCutData);
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F1, 0L);
			break;
		case IDC_BUTTON_EDIT_F2:
			printf("IDC_BUTTON_EDIT_F2 push\n");
			SetFocusChild(hwndInputCutData);
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F2, 0L);
			break;
		case IDC_BUTTON_EDIT_F3:
			printf("IDC_BUTTON_EDIT_F3 push\n");
			SetFocusChild(hwndInputCutData);
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F3, 0L);
			break;
		case IDC_BUTTON_EDIT_F4:
			printf("IDC_BUTTON_EDIT_F4 push\n");
			if(proRunStatus.runStatus == MD_EDIT){
				SetFocusChild(hwndInputCutData);
				SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F4, 0L);

			}

			if (proRunStatus.runStatus == MD_USER){
				/* IME: 123 */
				printf("User Mode: change to IME NUMBER mode\n");
				showIMEWindow(1, IME_NUMBER);
				break;
			}

			break;
		case IDC_BUTTON_EDIT_F5:
			printf("IDC_BUTTON_EDIT_F5 push\n");
			SetFocusChild(hwndInputCutData);
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F5, 0L);
			break;
		case IDC_BUTTON_EDIT_F6:
			printf("IDC_BUTTON_EDIT_F6 push\n");
			SetFocusChild(hwndInputCutData);
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_F6, 0L);
			break;

		/*
		 * Left side common button
		 */
		case IDC_BUTTON_USERS:
			printf("IDC_BUTTON_USERS push\n");

			/* set shift key enabled */
			proRunStatus.secondFunc = 1;
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_PUSHER, 0L);

			break;

		case IDC_BUTTON_PROLIST:
			SetFocusChild(hwndInputCutData);
			printf("IDC_BUTTON_PROLIST push\n");
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_PROG, 0L);
			break;
		case IDC_BUTTON_MD_EDIT:
			SetFocusChild(hwndInputCutData);
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_EDIT, 0L);
			break;
		case IDC_BUTTON_MD_AUTO:
			SetFocusChild(hwndInputCutData);
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_AUTORUN, 0L);
			break;
		case IDC_BUTTON_MD_MANUAL:
			SetFocusChild(hwndInputCutData);
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_MANUAL, 0L);
			break;
		case IDC_BUTTON_MD_TRAINING:
			/* Enter/Exit training mode
			 *   shift + edit keys
			 *   if current is NOT trainig mode, enter;
			 *   if current is training mode, exit to Edit mode;
			 */
			printf("IDC_BUTTON_TRAINING push shift=%d \n", proRunStatus.secondFunc);
			SetFocusChild(hwndInputCutData);

			/* set shift key enabled */
			proRunStatus.secondFunc = 1;
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_EDIT, 0L);
			break;

		/*
		 * Enter, clear and run
		 */
		case IDC_BUTTON_ENTER:
#if 1
			hwndtemp = GetDlgItem(hDlg, IDC_USER1_NAME);
			SetWindowBkColor(hwndtemp, PIXEL_blue);
			InvalidateRect(hwndtemp, NULL, TRUE);
			break;
#endif
			SetFocusChild(hwndInputCutData);
			printf("IDC_BUTTON_ENTER push\n");
			/* broadcast KEYDOWN message to all window/controls */
			SendMessage(HWND_DESKTOP, MSG_KEYDOWN, SCANCODE_ENTER, 0L);
			break;

		case IDC_BUTTON_CLEAR:
#if 1
			hwndtemp = GetDlgItem(hDlg, IDC_USER1_NAME);
			SetWindowBkColor(hwndtemp, PIXEL_red);
			InvalidateRect(hwndtemp, NULL, TRUE);
			break;
#endif

			SetFocusChild(hwndInputCutData);
			printf("IDC_BUTTON_CLEAR push\n");
			SendMessage(hDlg, MSG_KEYDOWN, SCANCODE_CANCEL, 0L);
			break;

		case IDC_BUTTON_RUN:
			SetFocusChild(hwndInputCutData);
			printf("IDC_BUTTON_CLEAR push\n");
			SendMessage(hDlg, MSG_KEYUP, SCANCODE_RUN, 0L);
			break;

		case IDCANCEL:
			KillTimer(hDlg,IDC_TIMER);

			/* destroy all controls in this window */
			DestroyAllControls (hDlg);

			EndDialog (hDlg, wParam);
			break;
		}
		break;

	}


	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void InitUserProDialogBox (HWND hWnd)
{
	UserProgromDlgInitProgress.controls = CtrlInitProgress;

	DialogBoxIndirectParam (&UserProgromDlgInitProgress, hWnd, InitUserProDialogBoxProc, 0L);
}

int EnterUserProgram(void)
{

	/* load bmp files */
	if(loadUserProBmpFiles())
		return(1);

	InitUserProDialogBox (HWND_DESKTOP); /* Should not return except receive CLOSE MSG */

	printf("exit UserProgram()...UnloadBitmap...\n");
	unloadUserProBmpFiles();

	/* if(userProExitFlag == TO_PROLIST_WINDOW){ */
	/* 	/\* goto program list window *\/ */
	/* }else if(userProExitFlag == TO_LOGIN_WINDOW){ */
	/* 	/\* goto login window *\/ */
	/* 	EnterRegisterDialog(); */
	/* } */
	return 0;
}
