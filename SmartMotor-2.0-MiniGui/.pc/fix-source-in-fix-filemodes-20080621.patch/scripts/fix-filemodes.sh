#!/bin/sh

# Exit immediately on errors.
set -e


# For SS related works.
setup=$(find . -maxdepth 1 -name "build-*")
if [ -n "$setup" ]; then
    chmod ug+x build-*
fi

# For git to properly work with .pc/ files.
chmod -R ug+rw .pc/

# This script itself
chmod ug+x scripts/fix-filemodes.sh


# Looks at the current index and checks to see if merges or updates
# are needed by checking stat() information.
# This is needed for errornous cg-commit log listing all files
# modified by this file.
#
# git-update-index returns non-zero (indicating some files are changed).
# Ignore it by `|| true', so that the entire script exits with zero.
git-update-index --refresh > /dev/null || true
