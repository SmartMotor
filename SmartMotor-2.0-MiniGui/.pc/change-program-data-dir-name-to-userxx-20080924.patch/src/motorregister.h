/*
 *  motorregister.h
 *
 *  Copyright 2008 @ xianweizeng@gmail.com
 *
 */
#ifndef _MOTORREGISTER_H_
#define _MOTORREGISTER_H_

#define MAX_USER_NUMBER	8

struct userinfor_t{		/* User profile */
	char name[20];
	char password[10];
	int recent_programe;
	int language;		/* 0:CN 1:EN */
	int unit;		/* 0:mm 1:mil ( 1/1000 inch)*/
	int frontlimit;
	int middlelimit;
	int backlimit;
	int forwardstep;
	int backwardstep;
	int speed;	       /* Run Speed 8 - 12 m/min */
};

/* global variables defined in motorregister.c */
extern struct userinfor_t userinformation[MAX_USER_NUMBER];
extern struct userinfor_t *currentuser;
extern int  currentid;
extern int  usernumber;


int EnterRegisterDialog(void);
int loadUserConfigFile(void);
int saveUserConfigFile(int flags);

#endif /* _MOTORREGISTER_H_ */
