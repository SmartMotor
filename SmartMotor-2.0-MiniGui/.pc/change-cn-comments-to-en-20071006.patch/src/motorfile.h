/*   common.h
 *   2005-11-12
 */

#ifndef _MOTORFILE_H_
#define _MOTORFILE_H_


struct programe_t{		//读文件的结构体	
    int fileflags;
    int isnull;
    int id;			//列表框的id,其实是文件名称
    char introduction[20];
    char recent_date[12];
    int times;
    int first;
}programe;

struct programe_link_t{	//存储文件的结构体链表
   struct programe_t prog;
   struct programe_link_t *next;
};



int EnterProListDialog(void);

#endif
