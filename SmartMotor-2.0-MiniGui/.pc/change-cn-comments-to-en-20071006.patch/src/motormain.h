/*   common.h
 *   2005-11-12
 */


#ifndef _MOTORMAIN_H_
#define _MOTORMAIN_H_

int loadInitConfigFile(int *pFlash,int *pBkgnd);
int saveInitConfigFile(int *pFlash,int *pBkgnd);

#endif
