/* 
** $Id: motormain.c,v 1.0 2006/09/24 05:02:15 zengxianwei Exp $
**
** Copyright Reserved
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <minigui/common.h>
#include <minigui/minigui.h>
#include <minigui/gdi.h>
#include <minigui/window.h>
#include <minigui/control.h>
#include <minigui/mgext.h>

#include "motormain.h"
#include "motorregister.h"
#include "motorfile.h"
#include "motor.h"
#include "bkgndEdit.h"
#include  "help.h"	

#include "userprogram.h"

/* define globle variables */
HWND myIMEWindow;
int isIMEOpenInMotor;	/* 0: disappear 1: display */

/* startup paramters */
struct startup_params_t startupParams;

/* motor status */
struct motorStatus_t motorStatus;

/* system font */
PLOGFONT systemDefaultFont,userFont24,userFont16;

/* system language */
int *curSysLang;


static int animationFlag;		// Is flash on?

struct welcome_help_t{
	unsigned char * msg_welcome;
};

static struct welcome_help_t welcome_help[SYSLANG_NO] = 
{
{msg_welcome_cn},
{msg_welcome_en}
};


/*   Load config file
 */
int loadInitConfigFile(struct startup_params_t *pStartup_params)
{
    FILE *fp;
    int isFlashOn = 0;
    int isBkgnd = 0;
    int lastuser = 0;
    fp = fopen("./user/init.cfg","r");
    if(fp == NULL){
	printf("Open init file error!\n");
	return -1;
    }

    while(!feof(fp)){
	    fscanf(fp,"%d %d %d\n",&isFlashOn, &isBkgnd, &lastuser);
    }

    pStartup_params->isFlashOn = isFlashOn;
    pStartup_params->whichBkgnd = isBkgnd;
    pStartup_params->lastuser = lastuser;

    fclose(fp);

    return(0);
    
}

int saveInitConfigFile (struct startup_params_t *pStartup_params)
{
    FILE *fp;

    fp=fopen("./user/init.cfg","w");
    if(fp == NULL) {
	printf("Open init file error!\n");
	return(-1);	
    } 

    fprintf(fp,"%d %d %d\n",pStartup_params->isFlashOn,
	                    pStartup_params->whichBkgnd,
	                    pStartup_params->lastuser);
    fclose(fp);

    return(0);

}

static struct comCmdData_t cmdData;

static int sendUserLimits(int flag)
{
    int data = 0, tmpdata;
    int unit = currentuser->unit;
    char cmdSendBuf[13];

    memset(cmdSendBuf,0,12);
    cmdData.flag = 'L';
    switch(flag){
	case 0:
	    data = currentuser->frontlimit;
	    break;
	case 1:
	    data = currentuser->middlelimit;
	    break;
	case 2:
	    data = currentuser->backlimit;
	    break;
    }
    tmpdata = data;
    if(unit){
  	data = inch_to_mm(tmpdata);
    }

    cmdData.data = data;
    cmdData.status = 0x30 + flag;

    setCmdFrame(&cmdData,cmdSendBuf);
    sendCommand(cmdSendBuf);
    printf("Send Limit %d %d\n",flag,data);
    return(data);
}

static void sendUserAllLimits(void)
{
    int delay = 1000;
    int flag = 0;
    for(flag=0;flag<3;flag++){
	delay = 1000;
	sendUserLimits(flag);
	while(delay--);
    }
}


static int AnimationWinProc(HWND hWnd, int message, WPARAM wParam, LPARAM lParam)
{
    char cmdbuf[12];
    unsigned char *pWelcomeText = welcome_help[*curSysLang].msg_welcome;
    HDC hdc;
    switch (message) {
    case MSG_CREATE:
    {

	if(!animationFlag){
	    hdc = BeginPaint (hWnd);
	    BITMAP welcomeBkgnd;
            if (LoadBitmap (HDC_SCREEN, &welcomeBkgnd, "./res/welcome.jpg"))
           	return 1;
	    FillBoxWithBitmap (hdc, 0, 0, 0, 0, &welcomeBkgnd);
	    UnloadBitmap (&welcomeBkgnd);
            
	    break;
	}

	hdc = BeginPaint (hWnd);
        TextOut (hdc, 210, 220, pWelcomeText);
	EndPaint (hWnd, hdc);
        ANIMATION* anim = CreateAnimationFromGIF89aFile (HDC_SCREEN, "./res/welcome.gif");
        if (anim == NULL)
            return 1;

        SetWindowAdditionalData (hWnd, (DWORD) anim);
        CreateWindow (CTRL_ANIMATION, 
                          "", 
                          WS_VISIBLE | ANS_AUTOLOOP, 
                          800, 					// ID
                          0, 0, 640, 480, hWnd, (DWORD)anim);
        SendMessage (GetDlgItem (hWnd, 800), ANM_STARTPLAY, 0, 0);


        break;
    }

    case MSG_KEYDOWN:		// anykey press,pause
	switch(LOWORD(wParam)){
	    case SCANCODE_RUN:
		memset(cmdbuf,0,12);
		cmdData.flag = 'R';
		cmdData.status = 0x30;
		cmdData.data = 80000;
		setCmdFrame(&cmdData,cmdbuf);	// En-frame
		sendCommand(cmdbuf);		// Sent it out

#if defined CONFIG_DEBUG_ON_PC || defined CONFIG_DEBUG_ON_TARGET
		if(animationFlag){
		   SendMessage (GetDlgItem (hWnd, 800), ANM_STOPPLAY, 0, 0);
		}
#endif
		break;
	}
	break;

#if defined CONFIG_DEBUG_ON_PC || defined CONFIG_DEBUG_ON_TARGET
    case MSG_KEYUP:		// anykey press,go out
	switch(LOWORD(wParam)){
	    case SCANCODE_RUN:
		SendMessage(hWnd,MSG_CLOSE,0,0);
		break;
	}   
        break; 
#endif

    case MSG_RECV:		// Reveive frame from LowerMachine
	recvCommand(cmdbuf);
        //int ret = isCmdValid(cmdbuf);	// Now, there is no Check
	//if(ret){
	    memset(&cmdData,0,sizeof(cmdData));
	    getCmdFrame(&cmdData,cmdbuf);	// Decode, get data
	    if(cmdData.flag == 'R'){		
	    	motorStatus.currentCutPos = cmdData.data;	// Refresh Data
		motorStatus.devStatus = cmdData.status;	// Refresh Status
		if(animationFlag){
	    	   SendMessage (GetDlgItem (hWnd, 800), ANM_STOPPLAY, 0, 0);
		}
	    	SendMessage(hWnd,MSG_CLOSE,0,0);
	    }
	//}
	break;

    case MSG_DESTROY:
	if(animationFlag){
           DestroyAnimation ((ANIMATION*)GetWindowAdditionalData (hWnd), TRUE);
           DestroyAllControls (hWnd);
	}
        return 0;

    case MSG_CLOSE:
        DestroyMainWindow (hWnd);
        PostQuitMessage (hWnd);
        return 0;
    }

    return DefaultMainWinProc(hWnd, message, wParam, lParam); 
}


int MiniGUIMain (int argc, const char* argv[])
{
    MSG Msg;
    HWND hMainWnd;
    MAINWINCREATE CreateInfo;

    isIMEOpenInMotor = 0;
    motorStatus.currentCutPos = 0;
    motorStatus.newCutPos = 0;
    motorStatus.devStatus = 0;
    motorStatus.flags = 0;   
    motorStatus.cutCounter = 0;     

#ifdef _IME_GB2312
    myIMEWindow=GBIMEWindow (HWND_DESKTOP);	// IME window handler
    if(myIMEWindow){
	    MoveWindow(myIMEWindow,170,110,365,40,0);
	    ShowWindow(myIMEWindow,SW_HIDE);	// Hide IME window
	    isIMEOpenInMotor = 0;
    }
#endif

	// Create font
    userFont24 = CreateLogFont (NULL, "song", "GB2312", 
                        FONT_WEIGHT_BOOK, FONT_SLANT_ROMAN, FONT_SETWIDTH_NORMAL,
                        FONT_SPACING_CHARCELL, FONT_UNDERLINE_NONE,
		 	FONT_STRUCKOUT_NONE, 24, 0);
    userFont16 = CreateLogFont (NULL, "song", "GB2312", 
                        FONT_WEIGHT_BOOK, FONT_SLANT_ROMAN, FONT_SETWIDTH_NORMAL,
                        FONT_SPACING_CHARCELL, FONT_UNDERLINE_NONE,
			FONT_STRUCKOUT_NONE, 16, 0);
    //systemDefaultFont = GetSystemFont(SYSLOGFONT_DEFAULT);

#ifndef CONFIG_DEBUG_ON_PC
    initSerialCommunication();		// Init Serial Communication Port
#endif

    loadInitConfigFile(&startupParams);

    //printf("animationFlag = %d \n",animationFlag);
    loadUserConfigFile();
    printf("Unit Flag = %d \n",userinformation[0].unit);
    curSysLang = &userinformation[0].language;

#ifdef _LITE_VERSION
    SetDesktopRect(0, 0, 1024, 768);
#endif

    if (!InitMiniGUIExt()) {
        return 1;
    }

    CreateInfo.dwStyle = WS_VISIBLE;
    CreateInfo.dwExStyle = WS_EX_NONE;
    CreateInfo.spCaption = "" ;
    CreateInfo.hMenu = 0;
    CreateInfo.hCursor = 0;
    CreateInfo.hIcon = 0;
    CreateInfo.MainWindowProc = AnimationWinProc;
    CreateInfo.lx = 0; 
    CreateInfo.ty = 0;
    CreateInfo.rx = 640;
    CreateInfo.by = 480;
    CreateInfo.iBkColor = PIXEL_lightwhite;
    CreateInfo.dwAddData = 0;
    CreateInfo.dwReserved = 0;
    CreateInfo.hHosting = HWND_DESKTOP;

    hMainWnd = CreateMainWindow (&CreateInfo);
    if (hMainWnd == HWND_INVALID)
        return -1;

    ShowWindow (hMainWnd, SW_SHOWNORMAL);

    while (GetMessage(&Msg, hMainWnd)) {
        TranslateMessage(&Msg);
        DispatchMessage(&Msg);
    }

    MainWindowThreadCleanup (hMainWnd);
    MiniGUIExtCleanUp();


    /*Enter Login Window*/
/*     EnterRegisterDialog(); */

    /* Enter user window directly */
    /* before entering user window, at least these data structure should
     * be loaded:
     *   - userinformation[] and the currentuser pointer is set correctly
     *   - the user limits should be send to machine
     *   - programCreate should be filled and parsed to user window
     */
    loadUserConfigFile();                 /* fill userinformation[] */
    currentid = startupParams.lastuser;   /* get lastuser id */
    currentuser = &userinformation[currentid];

    sendUserAllLimits();     /* send user limits to machine */

    printf("current user %d current program %d \n",currentid,
	   currentuser->recent_programe);

    /* load program file */
    loadProgramFile(currentuser->recent_programe, &programCreate);

    EnterUserProgram();

    return 0;
} 

#ifndef _LITE_VERSION 
#include <minigui/dti.c>
#endif
