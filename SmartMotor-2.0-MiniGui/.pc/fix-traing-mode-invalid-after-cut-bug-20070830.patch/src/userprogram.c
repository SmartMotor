/* 
** $Id: userprogram.c, zxw$
**
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <minigui/common.h>
#include <minigui/minigui.h>
#include <minigui/gdi.h>
#include <minigui/window.h>
#include <minigui/control.h>

#include "userprogram.h"
#include "bkgndEdit.h"
#include "motor.h"
#include "configure.h"
#include "computer.h"
#include "motorregister.h"
#include "help.h"

#define BMP_INPUT_X		2640	// X坐标
#define BMP_AUTO_X		2720	// X坐标
#define BMP_AUTOCUT_X		2800
#define BMP_MANNUAL_X		3040
#define BMP_RUN_X		2880
#define BMP_STOP_X		2960
#define BMP_PUSHER_X		3040
#define BMP_SHIFT_X		3120
#define BMP_CUTUP_X		2320
#define BMP_CUTDOWN_X		2400
#define BMP_PUSHUP_X		2480
#define BMP_PUSHDOWN_X		2560


#define AUTO_X		200	//  在背景上X坐标
#define HALFAUTO_X	200
#define MANNUAL_X	200
#define INPUT_X		200	// X坐标
#define RUN_X		280
#define STOP_X		280
#define PUSHER_X		360
#define SHIFT_X		360
#define CUTUP_X		40
#define CUTDOWN_X	40
#define PUSHUP_X		120
#define PUSHDOWN_X	120

#define ENTER_X		260	
#define ENTER_Y		435	
#define CANCEL_X		320	
#define CANCEL_Y		435

#define CURPOS_X		460
#define CURPOS_Y		400

/*   数据状态标志
 */
#define PUSHER_FLAG	(0x01000000)	// 推纸器标志,高8位为状态标志
#define AUTOCUT_FLAG	(0x02000000)	// 自动切刀标志,高8位为状态标志
#define AIR_FLAG		(0x04000000)	// 气垫标志,高8位为状态标志

/*   机器状态标志
 */
#define CUT_STATUS_MASK	0x0001		// bit 0
#define PUSHER_STATUS_MASK	0x0002		// bit 1
#define WHICH_LIMIT_MASK	0x0004		// bit 2
#define MACHINE_STOP_MASK	0x0008		// bit 3

/*   机器运行宏定义
 */
#define MOTOR_FORWARD		0
#define MOTOR_BACKWARD	1


BITMAP userProgramEditBkgnd;	// 背景图片,定义为全局

static BITMAP userProgramEditButton;	// 标签,等长
static BITMAP buttonsFunctions;	// F1 -- F8
static BITMAP buttonsOkEsc;		// 确定,取消
static BITMAP cutIconBkgnd;		// 切纸器图标
static BITMAP largeNumsIcon;		// 0-9
static BITMAP lableEditBkgnd;

userProgramEditData programEdit;	// 程序编辑全局变量
fromProListToProData programCreate;	// 程序创建全局变量
static userProgramRunStatus proRunStatus;	// 程序运行状态
static int userProExitFlag = 0;
static struct comCmdData_t cmdData;

/*   F功能键的属性(F1-F6)
 */
static struct functionButton_t funcButtons[6] = 
{
  {460,50,80,40,0,960,0,80},		// 标签
  {460,95,80,40,1,960,2,80},	// 等分
  {460,140,80,40,2,960,4,80},	// 微调
  {460,185,80,40,3,960,6,80},	// 计算器
  {460,230,80,40,4,960,7,80},	// 机器设置
  {460,275,80,40,5,960,8,80}		// 保存程序
};

struct userpro_help_t{
	unsigned char * caption_help;
	unsigned char * caption_waring;
	unsigned char * msg_cutdata_overflow;
	unsigned char * msg_save_pro_confirm;
	unsigned char * msg_save_pro_ok;
	unsigned char * msg_delcutdata_confirm;
	unsigned char * msg_delalldata_confirm;
	unsigned char * msg_proname_change;
	unsigned char * msg_just_confirm;
	unsigned char * msg_cutslist_header;
	unsigned char * msg_cutslist_header_inch;
	unsigned char * help_userpro_name;
	unsigned char * help_userpro_cuts;
	unsigned char * help_t9;

};

static struct userpro_help_t userpro_help[SYSLANG_NO] = 
{
    {	caption_help_cn,
	caption_waring_cn,
	msg_cutdata_overflow_cn,
	msg_save_pro_confirm_cn,
	msg_save_pro_ok_cn,
	msg_delcutdata_confirm_cn,
	msg_delalldata_confirm_cn,
	msg_proname_change_cn,
	msg_just_confirm_cn,
	msg_cutslist_header_cn,
	msg_cutslist_header_inch_cn,
	help_userpro_name_cn,
	help_userpro_cuts_cn,
	help_t9_cn
   },

   {	caption_help_en,
	caption_waring_en,
	msg_cutdata_overflow_en,
	msg_save_pro_confirm_en,
	msg_save_pro_ok_en,
	msg_delcutdata_confirm_en,
	msg_delalldata_confirm_en,
	msg_proname_change_en,
	msg_just_confirm_en,
	msg_cutslist_header_en,
	msg_cutslist_header_inch_en,
	help_userpro_name_en,
	help_userpro_cuts_en,
	help_t9_en
   }
};

static DLGTEMPLATE UserProgromDlgInitProgress =
{
    WS_BORDER, 
    WS_EX_NONE,
    0, 0, 640, 480, 
    "VAM-CNC init...",
    0, 0,
    8, NULL,
    0
};

static CTRLDATA CtrlInitProgress [] =
{ 
	{
        CTRL_STATIC,
        WS_VISIBLE | SS_CENTER,
        460, 5, 180, 30,
        IDC_TIME,
        NULL,
        0,
	WS_EX_TRANSPARENT
    	},
    {
        CTRL_STATIC,
        WS_VISIBLE |ES_READONLY,		// 序号不能修改
        95 ,55, 80, 30, 
        IDC_PROGRAMNUM, 
        NULL,
        0,
	WS_EX_TRANSPARENT
    },
    {
        CTRL_SLEDIT,
        WS_VISIBLE | SS_SIMPLE|WS_BORDER,
        250, 50, 200, 30, 
        IDC_PROGRAMNAME,
        "",
        0
    },
    {
        CTRL_STATIC,
        WS_VISIBLE | SS_SIMPLE |WS_BORDER| ES_READONLY,		// 序号不能修改
        8 , 90, 350,20, 
        IDC_PROGRAMCUTSLISTHEAD, 
        NULL,
        0
    },
    {	CTRL_LISTBOX,
	WS_VISIBLE|WS_BORDER|WS_VSCROLL | LBS_NOTIFY,
        8, 110, 350, 260,
        IDL_PROGRAMCUTSLIST,
        NULL,
        0
    },
    {
        CTRL_SLEDIT,
        WS_VISIBLE |WS_BORDER,
        110, 440, 130, 32, 
        IDC_INPUTCUTDATA,
        NULL,
        0
    }, 
    {
        CTRL_SLEDIT,
        WS_VISIBLE | WS_BORDER | ES_READONLY,
        520, 334, 110, 32, 
        IDC_CURPOS,
        NULL,
        0,
	WS_EX_TRANSPARENT
    },    
    {
        CTRL_SLEDIT,
        WS_VISIBLE | WS_BORDER | ES_READONLY,
        520, 284, 110, 32, 
        IDC_CUTCOUNTER,
        NULL,
        0,
	WS_EX_TRANSPARENT
    }     

};

/******************标签编辑对话框模板****************************/
static DLGTEMPLATE lableEditDlgBox =	
{
    WS_BORDER ,
    WS_EX_NONE,
    8, 90, 350, 280,
    "lableEdit",
    0, 0,
    3, NULL,
    0
};

static CTRLDATA lableEditCtrlsInit[] =
{   
    {
        CTRL_SLEDIT,
        WS_VISIBLE | SS_SIMPLE|WS_BORDER,
        130, 40, 120, 30, 
        IDC_LABLETOTALLEN,
        "",
        0
    },
    {
        CTRL_SLEDIT,
        WS_VISIBLE | SS_SIMPLE|WS_BORDER,
        130, 100, 120, 30, 
        IDC_LABLELEN,
        "",
        0
    },
    {
        CTRL_SLEDIT,
        WS_VISIBLE | SS_SIMPLE|WS_BORDER,
        130, 160, 120, 30, 
        IDC_UNUSEDLEN,
        "",
        0
    }	
};

/******************等分编辑对话框模板****************************/
static DLGTEMPLATE averageEditDlgBox =	
{
    WS_BORDER ,
    WS_EX_NONE,
    8, 90, 350, 280,
    "averageEdit",
    0, 0,
    2, NULL,
    0
};

static CTRLDATA averageEditCtrlsInit[] =
{   
    {
        CTRL_SLEDIT,
        WS_VISIBLE | SS_SIMPLE|WS_BORDER,
        130, 40, 120, 30, 
        IDC_HALFTOTALLEN,
        "",
        0
    },
    {
        CTRL_SLEDIT,
        WS_VISIBLE | SS_SIMPLE|WS_BORDER,
        130, 100, 120, 30, 
        IDC_HALFNUMBER,
        "",
        0
    }
};


static void ResetUserProgramEdit(HWND hwnd,userProgramEditData *pProEdit,int func);
static void refreshUserProgramRunStatus(HWND hwnd,HDC hdc,userProgramRunStatus *pStatus,int type,int flag);
static void releaseSecondFunction(HWND hwnd);

/**********************************************/
// 一些全局函数,在其他文件中需要用到,暂时放到这个文件中,以后
// 需要归类

/*  刷新切刀当前位置,数字显示
 *  hwnd : 编辑框句柄
 */
void refreshCurrentPos(HWND hwnd,int data)
{
    float curPos;
    char curPosBuf[10];
    curPos = (float)data/100.0;	
    sprintf(curPosBuf,"%7.2f",curPos);	
    SetWindowText(hwnd,curPosBuf);	// 设置当前位置
   
}
/*  使用数字图标刷新当前尺寸
 *  hwnd: 主界面句柄
 */
void refreshCurrentPosByIcon(HDC hdc,int data)
{	
    float curPos;
    char curPosBuf[10];
    int len,i,numX;
    curPos = (float)data/100.0;	
    sprintf(curPosBuf,"%7.2f",curPos);	
    len = strlen(curPosBuf);
    //printf("pos = %s len = %d \n",curPosBuf,len);
    for(i=0;i<len;i++){
	if(curPosBuf[i] >= '0' && (curPosBuf[i] <= '9')){
	    numX = ((int)(curPosBuf[i] - 0x30) * 20);
	} else if(curPosBuf[i] == '.'){
	    numX = 200;	// 小数点
	} else {
	    numX = 220; // 空格
	}
	
	RefreshBoxArea (hdc,&largeNumsIcon,CURPOS_X+i*20,CURPOS_Y,20,32,numX,0);
    }
   
}
/*  刷新切刀当前位置,数字显示和滑块显示区域
 *  data: 当前位置
 *  range: 是否更新滑块范围
 */
static void refreshCurrentPosArea(HWND hDlg,HDC hdc,int data,int range)
{

    HWND hwndTrackBar = GetDlgItem(hDlg,IDC_TRACKBAR);
    int posMin;
    int posMax;
    if(range){	 // 设置滑块范围
	posMin = currentuser->frontlimit / 100;
	posMax = currentuser->backlimit / 100;
	SendMessage(hwndTrackBar,TBM_SETRANGE,posMin,posMax);
    }
    refreshCurrentPosByIcon(hdc,data);
    SendMessage(hwndTrackBar,TBM_SETPOS,data/100,0);	// 滑块
}

/*  // 刷新刀前尺寸
 */
static void refreshForCutLenArea(HWND hwnd,userProgramRunStatus *pStatus,int curPos)
{
    int len ; 
    int lastPos = pStatus->lastCutPos;
    if(curPos < lastPos){
	len = lastPos - curPos;
	pStatus->forCutLen = len;
    } else {
	pStatus->forCutLen = 0;
    }
    refreshCurrentPos(hwnd,pStatus->forCutLen);	
    //printf("refreshCurPos:last=%d cur=%d len=%d\n",lastPos,curPos,pStatus->forCutLen);
}

static void refreshCutCounter(HWND hwnd)
{
    char dispbuf[8];
    HWND hwndCutCnt = GetDlgItem(hwnd,IDC_CUTCOUNTER);
    memset(dispbuf,0,8);
    sprintf(dispbuf,"%6d",motorStatus.cutCounter);
    SetWindowText(hwndCutCnt,dispbuf);

}


/*   得到当前有效极限值
 */
static int getValidLimit(void)
{
     int frontLimit;
     int whichLimit = proRunStatus.whichLimit;	// 选择有效极限
     if(!whichLimit){
      	frontLimit= currentuser->frontlimit;
     } else {
      	frontLimit= currentuser->middlelimit;	
     }  
     return(frontLimit);
}

static void releaseSecondFunction(HWND hwnd)
{
    HDC hdc;
    if(proRunStatus.secondFunc == 1){	// 第二功能
	proRunStatus.secondFunc = 0;
	hdc = BeginPaint(hwnd);
	refreshUserProgramRunStatus(hwnd,hdc,&proRunStatus,3,0);
	EndPaint(hwnd,hdc);
    }

}

/* 根据输入的总长,标签长度和废边长度计算标签输入结果,
 * 并将结果保存到pProEdit结构的userProCuts数组中;
 * 参数:  	pProEdit
 * 返回值:	有效刀数
 */
static int CountLableData(userProgramEditData *pProEdit)
{
     int frontLimit = getValidLimit();
     int totalLen = pProEdit->lableEdit.totalLen;
     int lableLen = pProEdit->lableEdit.lableLen;
     int unusedLen = pProEdit->lableEdit.unusedLen;

     int currentCut = pProEdit->currentCutsNum;
     int totalCuts = pProEdit->nInputCutsNum;

     //printf("CountLableData...totalCuts=%d curCut=%d\n",totalCuts,currentCut);
     int cut = totalLen;
     if((currentCut<totalCuts)&&(currentCut<MAX_PROGRAM_CUTS)){
     	pProEdit->userProCuts[currentCut++] = cut;
     }else {
	return totalCuts;
     }
     while(cut >= frontLimit){	//前极限
	cut = cut-lableLen;
	if((cut >= frontLimit) && (currentCut<MAX_PROGRAM_CUTS)){
	    pProEdit->userProCuts[currentCut++] = cut;
	} else {
	    break;
 	}
	cut = cut-unusedLen;
	if((cut >= frontLimit) && (currentCut<MAX_PROGRAM_CUTS)){
	    pProEdit->userProCuts[currentCut++] = cut;
	} else {
	    break;
	}
     } 

     if(currentCut >= totalCuts){	// 更新总刀数,当前刀数不变
	totalCuts = currentCut+1;
	pProEdit->userProCuts[totalCuts-1] = 0;// 最后一刀置为0
	pProEdit->userProCuts[totalCuts] = 0;	// 最后一刀置为0
     }
     
     pProEdit->nInputCutsNum = totalCuts;   

     return totalCuts;
	
}

/* 根据输入的总长,等分计算标签输入结果,
 * 并将结果保存到pProEdit结构的userProCuts数组中;
 * 参数:  	pProEdit
 * 返回值:	有效刀数
 */

static int CountHalfData(userProgramEditData *pProEdit)
{
     int frontLimit = getValidLimit();
     int totalLen = pProEdit->halfEdit.totalLen;
     int num = pProEdit->halfEdit.num;
     int currentCut = pProEdit->currentCutsNum;
     int totalCuts = pProEdit->nInputCutsNum;
     int cutBase = totalLen / num ;	// 每次递减的尺寸
     int cut = totalLen;

     if((currentCut<totalCuts)&&(currentCut<MAX_PROGRAM_CUTS)){
     	pProEdit->userProCuts[currentCut++] = cut;
     } else {
	return totalCuts;
     }
     while(cut >= frontLimit){	//前极限
	cut = cut-cutBase;
	if((cut >= frontLimit) && (currentCut<MAX_PROGRAM_CUTS)){
	    pProEdit->userProCuts[currentCut++] = cut;
	} else {
	    break;
 	}
     } 
     //printf("currentcut = %d \n",currentCut);

     if(currentCut >= totalCuts){	// 更新总刀数,当前刀数不变
	totalCuts = currentCut+1;
     //printf("totalCuts = %d \n",totalCuts);
	pProEdit->userProCuts[totalCuts-1] = 0;// 最后一刀置为0
	pProEdit->userProCuts[totalCuts] = 0;	// 最后一刀置为0
     }
     pProEdit->nInputCutsNum = totalCuts;   
     return totalCuts;
	
}
/**********************************************/
// 文件操作
/*    保存程序数据到文件
 *    参数: pathname : 文件路径及文件名
 *   	    fileHeader: 从上层得到的文件头
 *	    pProEdit:  输入或者修改好的程序数据
 *    返回值:
 *	    成功 1 否则 -1
 *		
 */
static int 
saveUserProgramToFile(char *pathname, userProFileHeader *fileHeader,userProgramEditData *pProEdit)
{	
    //printf("file name=%s\n",programCreate.filePathname);	
    int nInputCuts = pProEdit->nInputCutsNum;
    FILE *fp;  
    unsigned char pathtmp[16] = "./default.pro";
    if(strlen(pathname) == 0){		// 文件名为空
	pathname = pathtmp;
    }

    fp = fopen(pathname,"wb+");	// read ,write ,binary
    if(fp == NULL){
	perror("File open error.\n");
	return(-1);
    }
    fwrite(fileHeader,sizeof(struct userProFileHeader_t),1,fp); // 写文件头,包含第一刀 
    fwrite(&(pProEdit->userProCuts[1]),sizeof(int)*nInputCuts,1,fp); // 写剩下的其他数据
  
    fclose(fp);
    
    return 1;
}

/*    从文件中读取程序数据
 *    参数: pathname : 文件路径及文件名
 *   	    fileHeader: 从上层得到的文件头
 *	    pProEdit:  输入或者修改好的程序数据
 *    返回值:
 *	    成功 1 否则 -1
 *		
 */
static int 
loadUserProgramFromFile(char *pathname, userProFileHeader *fileHeader,userProgramEditData *pProEdit)
{
    FILE *fp;  
    int ret;
    int index=1;
    //printf("open filename=%s\n",programCreate.filePathname);
    fp = fopen(pathname,"r");	// read ,write ,binary
   
    if(fp == NULL){
	perror("File open erreo.\n");
	return(-1);
    }

    ret = fread(fileHeader,sizeof(struct userProFileHeader_t),1,fp); 
    //printf("%x %d %d \n",fileHeader->fileflags,fileHeader->usedTimes,fileHeader->first);
    while(!(feof(fp))){
	ret = fread(&(pProEdit->userProCuts[index]),4,1,fp);
	//printf("ret=%d \n",pProEdit->userProCuts[index]);
	index++;
	
    }
    pProEdit->userProCuts[0] = fileHeader->first; 
     
    return(index);
}

static void UpdateFileHeader(fromProListToProData *pProCreate,userProgramEditData *pProEdit,int times)
{
    char date[12];
    struct rtc_tm tm;
    getSysRtcTime(&tm);	
    sprintf(date,"%02d-%02d-%02d",tm.tm_year-2000,tm.tm_mon+1,tm.tm_mday); 
    memset(pProCreate->proHeader.latestDate,0,12); 
    strncpy(pProCreate->proHeader.latestDate,date,strlen(date));
    pProCreate->proHeader.isnull = 0;
    pProCreate->proHeader.first = pProEdit->userProCuts[0];
    if(times){
        pProCreate->proHeader.usedTimes = pProCreate->proHeader.usedTimes +1;
    }


}

/*   更新列表,
 *   hwnd:列表的父窗口,pProEdit: 程序输入结构体指正
 *   功能:  更新pProEdit->userProCuts[currentCut]指定的数据到列表
 *	   
 *   参数说明:	type为0: 修改pProEdit->currentCutsNum指定的地方的数据
 *		type为1时追加到文件末尾
 */
static int UpdateProListWithData(HWND hwnd,userProgramEditData *pProEdit,int type)
{
    int data,flag;
    float fdata;
    char dataflag[10];
    int currentCut = pProEdit->currentCutsNum;
    int unit = currentuser->unit;
    HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);
    char dispbuf[40];			//显示缓冲
    memset(dispbuf,0,40);
    memset(dataflag,0,10);
    if(currentCut<MAX_PROGRAM_CUTS){
	data = pProEdit->userProCuts[currentCut] & 0x00ffffff ;  //取出数据,高8位为状态
        fdata = (float)(data / 100.0);
	if(unit){
            sprintf(dispbuf," %03d      %06.2f   ",currentCut+1,fdata);	
	} else {
            sprintf(dispbuf," %03d      %07.2f   ",currentCut+1,fdata);
	}
	flag = pProEdit->userProCuts[currentCut] & AIR_FLAG;
	if(!flag){		// flag = 0, ON
    	   strncpy(dataflag,"  N ",4);  
	}else{
    	   strncpy(dataflag,"  F ",4); 
	}

	strcat(dispbuf,dataflag);
	if(type == 0){
	    // 先删除当前条目
	    SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_DELETESTRING,currentCut,0);
	    // 增加条目
    	    SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_INSERTSTRING,currentCut,
				(LPARAM)dispbuf);	
	} else { 	
	    SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_ADDSTRING,0,(LPARAM)dispbuf);	
        }
    	SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);	 // 高亮输入项

    } else {
	pProEdit->currentCutsNum = 0;
    } 
    
    return 0;
}

/*   更新列表
 *   hwnd:列表的父窗口,pProEdit: 程序输入结构体指正
 *   功能: 从新刷新列表,数据为pProEdit中的数组,刷新后高亮项为
 *	   pProEdit->currentCutsNum 指定的位置
 */
static int UpdateProListWithFileData(HWND hwnd,userProgramEditData *pProEdit)
{
    int data;
    float fdata;
    int flagt=0,flagq=0; 
    char dataflag[10];
    int index = 0;
    int totalCuts = pProEdit->nInputCutsNum;   
    HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);
    char dispbuf[40];			//显示缓冲
    int unit = currentuser->unit;
    memset(dispbuf,0,40);

    SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_RESETCONTENT,0,0);// 清空列表
    for(index=0;index<totalCuts;index++){
	memset(dataflag,0,10);
	data = pProEdit->userProCuts[index] & 0x00ffffff;   //取出数据,高8位为状态
	fdata = (float)(data /100.0);
        flagt = pProEdit->userProCuts[index] & AIR_FLAG;
	if(!flagt){		// flag = 0, ON
    	   strncpy(dataflag,"  N ",4);  
	}else{
    	   strncpy(dataflag,"  F ",4); 
	}
        flagt = pProEdit->userProCuts[index] & PUSHER_FLAG;
	if(flagt){
    	   strncpy(&dataflag[4],"  T ",4);  
	}
	flagq = pProEdit->userProCuts[index] & AUTOCUT_FLAG;
	if(flagq){
    	   strncpy(&dataflag[4],"  Q ",4);  
	} 
	if(unit){
            sprintf(dispbuf," %03d      %06.2f   ",index+1,fdata);	
	} else {
            sprintf(dispbuf," %03d      %07.2f   ",index+1,fdata);
	}

	strcat(dispbuf,dataflag);
	// 更新条目
    	SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_ADDSTRING,0,(LPARAM)dispbuf);	
    }    
    //pProEdit->currentCutsNum = 0;
    SendMessage(hwndProCutList,LB_SETCURSEL,pProEdit->currentCutsNum,0);	 // 高亮输入项
    return 0;
}

/*  进入自动模式下不显示最后一刀0,并复制到缓冲区,以便切换到输入模式下显示
 */
static int delectLastListItem(HWND hwndList,userProgramEditData *pProEdit,char *buffer)
{

    int totalCuts = pProEdit->nInputCutsNum;	// 最后一刀
    int index;
    index = SendMessage(hwndList,LB_GETCOUNT,0,0);
    if(index == totalCuts){
	SendMessage(hwndList,LB_GETTEXT,index-1,(LPARAM)buffer);
	//printf("index = %d total=%d:%s \n",index,totalCuts,buffer);
    	SendMessage(hwndList,LB_DELETESTRING,index-1,0);
    }

    return(index);
}
static int addLastListItem(HWND hwndList,userProgramEditData *pProEdit,char *buffer)
{
    int totalCuts = pProEdit->nInputCutsNum;	// 最后一刀
    int index;
    index = SendMessage(hwndList,LB_GETCOUNT,0,0); 
    //printf("add:index = %d total = %d :%s \n",index,totalCuts,buffer);
    if(index == totalCuts -1){
    	SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)buffer);	// 使用ADDSTRING,自动添加到最后
    }      
    return(index);
}
/*  检查输入的尺寸是否合法,合法的话进行转换,将字符串转成响应的数字
 *  pBuf : 输入的字符串
 *  返回值: 输入的字符串长度
 */
int isInputCutsDataValid(char *pBuf,int len)
{

    int data,i,j;
    char strBuf[10];

    if(len > 8){
	return(-1);
    }
    
    //printf("input: %s  %d --> ",pBuf,len);
    i = 0;
    j = -1;	// j = -1,no point
    while(i<len){
	if(pBuf[i] == '.'){
	    j = i;	// Find the first point position 
	    break;
	}
	if(((pBuf[i]<'0') || (pBuf[i] > '9')) && (pBuf[i] != '.')){
	    return(-1);
	}
	i++;
    }
    memset(strBuf,0,10);

    if(j>=0){
	strncpy(strBuf,pBuf,j);	// 整数
	strncat(strBuf,"00",2);	// * 100
	if((pBuf[j+1] >= '0') && (pBuf[j+1] <= '9')){
	    strBuf[j] = pBuf[j+1];
	    if((pBuf[j+2] >= '0') && (pBuf[j+2] <= '9'))
	    	strBuf[j+1] = pBuf[j+2];
	}	

    }else{  // j < 0, no point
	strncpy(strBuf,pBuf,len);
	strncat(strBuf,"00",2);	// * 100
    }

    data = atoi(strBuf);
    //printf("strbuf = %s data = %d \n",strBuf,data);
    return(data);

}

/************************************************/
/**************标签编辑界面************************/
static void initLableEditBkgnd(HDC hdc)
{
     FillBoxWithBitmap (hdc, 0, 0, 350, 280, &lableEditBkgnd); 	// 背景   
     RefreshBoxArea (hdc,&userProgramEditButton,30,30,80,40,1760,0);	// 输入总长
     RefreshBoxArea (hdc,&userProgramEditButton,30,90,80,40,1840,0);	// 输入标签
     RefreshBoxArea (hdc,&userProgramEditButton,30,150,80,40,1920,0);	// 输入废边
     RefreshBoxArea (hdc,&userProgramEditButton,10,220,80,40,0,0);	// F1
     RefreshBoxArea (hdc,&userProgramEditButton,90,220,80,40,3680,0);	// 完成
     RefreshBoxArea (hdc,&userProgramEditButton,180,220,80,40,160,0);	// F2
     RefreshBoxArea (hdc,&userProgramEditButton,260,220,80,40,1040,0);	// 退出标签

}
static int getLableEditData(HWND *hwndEdit,userProgramEditData *pProEdit)
{
    int index = 0;
    char cutDataBuf[10];
    int  cutDataLen=0;
    int ret;
    pProEdit->lableEdit.lableStatus = 0;
    pProEdit->lableEdit.totalLen = 0;
    pProEdit->lableEdit.lableLen = 0;
    pProEdit->lableEdit.unusedLen = 0;
    for(index=0;index<3;index++){
	memset(cutDataBuf,0,10);
	cutDataLen = GetWindowTextLength(hwndEdit[index]);	// 获得输入的字符
	GetWindowText(hwndEdit[index],cutDataBuf,cutDataLen);
	ret=isInputCutsDataValid(cutDataBuf,cutDataLen); 	// 判断输入是否有效
 	if(ret > 0 ){
	    switch(index){
		case 0:
		     pProEdit->lableEdit.totalLen = ret;
		     pProEdit->lableEdit.lableStatus |= 0x1;	//bit 0		
		     break;
		case 1:
		     pProEdit->lableEdit.lableLen = ret;
		     pProEdit->lableEdit.lableStatus |= 0x2;	
		     break;
		case 2:
		     pProEdit->lableEdit.unusedLen = ret;
		     pProEdit->lableEdit.lableStatus |= 0x4;	
		     break;
	    }
	}else{
	    SetWindowText(hwndEdit[index],"");		// 清空输入区
	    SetFocusChild(hwndEdit[index]);
            //printf("getlabledata status=%d \n",pProEdit->lableEdit.lableStatus);
	    return(-1);
	}				
    }
    //pProEdit->lableEdit.lableStatus = 7;
    //printf("getlabledata status=%d \n",pProEdit->lableEdit.lableStatus);

    return(0);

}

static void lableTotalLenEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{ 
     HWND mainHwnd,nextHwnd;
     int dataLen,ret;
     char dataBuf[10];
     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     nextHwnd = GetDlgItem(mainHwnd,IDC_LABLELEN);

     switch(nc){
	case EN_CHANGE:		// 有数字键按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    if(dataLen > 7){		// 输入超出范围,clear
		SetFocusChild(hwnd);
		SetWindowText(hwnd,"");	// 清空输入区 
    	    }
	    break;
	case EN_ENTER:		// Enter key 按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    GetWindowText(hwnd,dataBuf,dataLen);
	    ret=isInputCutsDataValid(dataBuf,dataLen); 	// 判断输入是否有效
 	    if(ret > 0 ){				// 输入有效,返回转换后的数据
		programEdit.lableEdit.totalLen = ret;
		programEdit.lableEdit.lableStatus |= 0x1;	//bit 0	
		if(programEdit.lableEdit.lableStatus == 7){	// 输入完成
		    CountLableData(&programEdit);		//计算结果
		    SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0); //exit
		}else{
    		    SetFocusChild(nextHwnd);	
		}
	    }else {
		SetWindowText(hwnd,"");	// 清除
	    }
	    break;
     }
}

static void lableLenEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{ 
     HWND mainHwnd,nextHwnd;
     int dataLen,ret;
     char dataBuf[10];
     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     nextHwnd = GetDlgItem(mainHwnd,IDC_UNUSEDLEN);

     switch(nc){
	case EN_CHANGE:		// 有数字键按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    if(dataLen > 7){		// 输入超出范围,clear
		SetFocusChild(hwnd);
		SetWindowText(hwnd,"");	// 清空输入区 
    	    }
	    break;
	case EN_ENTER:		// Enter key 按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    GetWindowText(hwnd,dataBuf,dataLen);
	    ret=isInputCutsDataValid(dataBuf,dataLen); 	// 判断输入是否有效
 	    if(ret > 0 ){				// 输入有效,返回转换后的数据
		programEdit.lableEdit.lableLen = ret;
		programEdit.lableEdit.lableStatus |= 0x2;	//bit 1	
    		if(programEdit.lableEdit.lableStatus == 7){	// 输入完成
		    CountLableData(&programEdit);		//计算结果
		    SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0); //exit
		}else{
    		    SetFocusChild(nextHwnd);	
		}
	    }else {
		SetWindowText(hwnd,"");	// 清除
	    }
	    break;
     }
}
static void lableUnusedLenEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{ 
     HWND mainHwnd,nextHwnd;
     int dataLen,ret;
     char dataBuf[10];
     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     nextHwnd = GetDlgItem(mainHwnd,IDC_LABLETOTALLEN);

     switch(nc){
	case EN_CHANGE:		// 有数字键按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    if(dataLen > 7){		// 输入超出范围,clear
		SetFocusChild(hwnd);
		SetWindowText(hwnd,"");	// 清空输入区 
    	    }
	    break;
	case EN_ENTER:		// Enter key 按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    GetWindowText(hwnd,dataBuf,dataLen);
	    ret=isInputCutsDataValid(dataBuf,dataLen); 	// 判断输入是否有效
 	    if(ret > 0 ){				// 输入有效,返回转换后的数据
		programEdit.lableEdit.unusedLen = ret;
		programEdit.lableEdit.lableStatus |= 0x4;	//bit 2	
		if(programEdit.lableEdit.lableStatus == 7){	// 输入完成
		    CountLableData(&programEdit);		//计算结果
		    SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0); //exit
		}else{
    		    SetFocusChild(nextHwnd);	
		}
	    }else {
		SetWindowText(hwnd,"");	// 清除
	    }
	    break;
     }
}

static int enterLableEditDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)		
{	
     HDC hdc;
     HWND hwndEdit[3],hwndTemp;;
     hwndEdit[0] = GetDlgItem(hDlg,IDC_LABLETOTALLEN);
     hwndEdit[1] = GetDlgItem(hDlg,IDC_LABLELEN);
     hwndEdit[2] = GetDlgItem(hDlg,IDC_UNUSEDLEN);

     switch (message) 
	{
    	case MSG_INITDIALOG:
	    SetNotificationCallback(hwndEdit[0], lableTotalLenEditCallback);
	    SetNotificationCallback(hwndEdit[1], lableLenEditCallback);
	    SetNotificationCallback(hwndEdit[2], lableUnusedLenEditCallback);
            programEdit.lableEdit.lableStatus = 0;
            programEdit.lableEdit.totalLen = 0;
            programEdit.lableEdit.lableLen = 0;
            programEdit.lableEdit.unusedLen = 0;

	    return(1);

    	case MSG_ERASEBKGND:
        {  
	    hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            		}       
                    
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
            }
	           
	    initLableEditBkgnd(hdc); 	 //初始化背景	
	    SetFocusChild(hwndEdit[0]);	

            if (fGetDC)
                ReleaseDC (hdc);
	    return 0;	
	}        
    	case MSG_KEYDOWN:
	    switch(LOWORD(wParam)){
		case SCANCODE_F1:	//   完成标签
		    getLableEditData(hwndEdit,&programEdit);
		    if(programEdit.lableEdit.lableStatus == 7){
			CountLableData(&programEdit);		//计算结果
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0); //exit
		    }
		    break;
		case SCANCODE_F2:	//   取消标签
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
		case SCANCODE_CURSORBLOCKDOWN:	//   向下
		    hwndTemp = GetFocusChild(hDlg);
 		    if(hwndTemp == hwndEdit[0]){
			SetFocusChild(hwndEdit[1]);
		    } else {
			SetFocusChild(hwndEdit[2]);
		    }
		    break;
		case SCANCODE_CURSORBLOCKUP:	//   向上
		    hwndTemp = GetFocusChild(hDlg);
 		    if(hwndTemp == hwndEdit[2]){
			SetFocusChild(hwndEdit[1]);
		    } else {
			SetFocusChild(hwndEdit[0]);
		    }
		    break;
		case SCANCODE_ENTER:	//   确定
		    //SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;		
		case SCANCODE_CANCEL:	//   取消
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
	    }			
	    break;
    	case MSG_COMMAND:
	    switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
	    	EndDialog(hDlg,IDOK);
		break;
	    }
	    break;
	}
     return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void enterLableEditDialogBox (HWND hwnd)
{ 
   lableEditDlgBox.controls = lableEditCtrlsInit;
   DialogBoxIndirectParam (&lableEditDlgBox, hwnd,enterLableEditDialogBoxProc, 0L);
} 
static int enterLableEdit (HWND hwnd)
{
    //printf("enterLableEdit().\n");
    enterLableEditDialogBox(hwnd);		// Should not return except receive CLOSE MSG
    return 0;
 
}

/************************************************/
/**************等分编辑界面************************/
static void initAverageEditBkgnd(HDC hdc)
{
     FillBoxWithBitmap (hdc, 0, 0, 350, 280, &lableEditBkgnd); 	// 背景   
     RefreshBoxArea (hdc,&userProgramEditButton,30,30,80,40,1760,0);	// 输入总长
     RefreshBoxArea (hdc,&userProgramEditButton,30,90,80,40,2000,0);	// 输入等分
     RefreshBoxArea (hdc,&userProgramEditButton,10,220,80,40,0,0);	// F1
     RefreshBoxArea (hdc,&userProgramEditButton,90,220,80,40,3680,0);	// 完成
     RefreshBoxArea (hdc,&userProgramEditButton,180,220,80,40,160,0);	// F2
     RefreshBoxArea (hdc,&userProgramEditButton,260,220,80,40,1200,0);	// 退出等分

}
static int getAverageEditData(HWND *hwndEdit,userProgramEditData *pProEdit)
{
    int index = 0;
    char cutDataBuf[10];
    int  cutDataLen=0;
    int ret;
    pProEdit->halfEdit.halfStatus = 0;
    pProEdit->halfEdit.totalLen = 0;
    pProEdit->halfEdit.num = 0;
    for(index=0;index<2;index++){
	memset(cutDataBuf,0,10);
	cutDataLen = GetWindowTextLength(hwndEdit[index]);	// 获得输入的字符
	GetWindowText(hwndEdit[index],cutDataBuf,cutDataLen);
	ret=isInputCutsDataValid(cutDataBuf,cutDataLen); 	// 判断输入是否有效
 	if(ret > 0 ){
	    switch(index){
		case 0:
		     pProEdit->halfEdit.totalLen = ret;
		     pProEdit->halfEdit.halfStatus |= 0x1;	//bit 0		
		     break;
		case 1:
		     pProEdit->halfEdit.num = ret/100;
		     pProEdit->halfEdit.halfStatus |= 0x2;	
		     break;
	    }
	}else{
	    SetWindowText(hwndEdit[index],"");		// 清空输入区
	    SetFocusChild(hwndEdit[index]);
            //printf("getlabledata status=%d \n",pProEdit->lableEdit.lableStatus);
	    return(-1);
	}				
    }
    //pProEdit->lableEdit.lableStatus = 3;
    //printf("getlabledata status=%d \n",pProEdit->lableEdit.lableStatus);

    return(0);

}

static void averageTotalLenEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{ 
     HWND mainHwnd,nextHwnd;
     int dataLen,ret;
     char dataBuf[10];
     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     nextHwnd = GetDlgItem(mainHwnd,IDC_HALFNUMBER);

     switch(nc){
	case EN_CHANGE:		// 有数字键按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    if(dataLen > 7){		// 输入超出范围,clear
		SetFocusChild(hwnd);
		SetWindowText(hwnd,"");	// 清空输入区 
    	    }
	    break;
	case EN_ENTER:		// Enter key 按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    GetWindowText(hwnd,dataBuf,dataLen);
	    ret=isInputCutsDataValid(dataBuf,dataLen); 	// 判断输入是否有效
 	    if(ret > 0 ){				// 输入有效,返回转换后的数据
		programEdit.halfEdit.totalLen = ret;
		programEdit.halfEdit.halfStatus |= 0x1;	//bit 0	
		if(programEdit.halfEdit.halfStatus == 3){	// 输入完成
		    CountHalfData(&programEdit);		//计算结果
		    SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0); //exit
		}else{
    		    SetFocusChild(nextHwnd);	
		}
	    }else {
		SetWindowText(hwnd,"");	// 清除
	    }
	    break;
     }
}

static void averageNumEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{ 
     HWND mainHwnd,nextHwnd;
     int dataLen,ret;
     char dataBuf[10];
     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     nextHwnd = GetDlgItem(mainHwnd,IDC_HALFTOTALLEN);

     switch(nc){
	case EN_CHANGE:		// 有数字键按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    if(dataLen > 5){		// 输入超出范围,clear
		SetFocusChild(hwnd);
		SetWindowText(hwnd,"");	// 清空输入区 
    	    }
	    break;
	case EN_ENTER:		// Enter key 按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    GetWindowText(hwnd,dataBuf,dataLen);
	    ret=isInputCutsDataValid(dataBuf,dataLen); 	// 判断输入是否有效
 	    if((ret > 0) && (ret %100 == 0) ){	// 等分数必须为整数
		programEdit.halfEdit.num = ret/100;
		programEdit.halfEdit.halfStatus |= 0x2;	//bit 1	
    		if(programEdit.halfEdit.halfStatus == 3){	// 输入完成
		    CountHalfData(&programEdit);		//计算结果
		    SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0); //exit
		}else{
    		    SetFocusChild(nextHwnd);	
		}
	    }else {
		SetWindowText(hwnd,"");	// 清除
	    }
	    break;
     }
}


static int enterAverageEditDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)		
{	
     HDC hdc;
     HWND hwndEdit[2],hwndTemp;;
     hwndEdit[0] = GetDlgItem(hDlg,IDC_HALFTOTALLEN);
     hwndEdit[1] = GetDlgItem(hDlg,IDC_HALFNUMBER);

     switch (message) 
	{
    	case MSG_INITDIALOG:
	    SetNotificationCallback(hwndEdit[0], averageTotalLenEditCallback);
	    SetNotificationCallback(hwndEdit[1], averageNumEditCallback);
            programEdit.halfEdit.halfStatus = 0;
            programEdit.halfEdit.totalLen = 0;
            programEdit.halfEdit.num = 0;

	    return(1);

    	case MSG_ERASEBKGND:
        {  
	    hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            		}       
                    
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
            }
	           
	    initAverageEditBkgnd(hdc); 	 //初始化背景	
	    SetFocusChild(hwndEdit[0]);	

            if (fGetDC)
                ReleaseDC (hdc);
	    return 0;	
	}        
    	case MSG_KEYDOWN:
	    switch(LOWORD(wParam)){
		case SCANCODE_F1:	//   完成等分
		    getAverageEditData(hwndEdit,&programEdit);
		    if(programEdit.halfEdit.halfStatus == 3){
			CountHalfData(&programEdit);		//计算结果
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0); //exit
		    }
		    break;
		case SCANCODE_F2:	//   取消等分
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
		case SCANCODE_CURSORBLOCKDOWN:	//   向下
		    SetFocusChild(hwndEdit[1]);
		    break;
		case SCANCODE_CURSORBLOCKUP:	//   向上
		    SetFocusChild(hwndEdit[0]);
		    break;	
		case SCANCODE_CANCEL:	//   取消
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
	    }			
	    break;
    	case MSG_COMMAND:
	    switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
	    	EndDialog(hDlg,IDOK);
		break;
	    }
	    break;
	}
     return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void enterAverageEditDialogBox (HWND hwnd)
{ 
   averageEditDlgBox.controls = averageEditCtrlsInit;
   DialogBoxIndirectParam (&averageEditDlgBox, hwnd,enterAverageEditDialogBoxProc, 0L);
} 
static int enterAverageEdit (HWND hwnd)
{
    //printf("enterLableEdit().\n");
    enterAverageEditDialogBox(hwnd);		// Should not return except receive CLOSE MSG
    return 0;
 
}



/*  正常编辑一刀数据,追加到文件末尾 
 *  分两步: 1 修改当前0.0数据;
 *	   2  追加一个0.0数据;
 */
static int editOneCutData(HWND hwnd,userProgramEditData *pProEdit,int data)
{
   int frontLimit = getValidLimit();
   int currentSel = pProEdit->currentCutsNum;
   int totalCuts = pProEdit->nInputCutsNum ;	// 新增一刀数据
   //printf("total=%d current=%d \n",totalCuts,currentSel);
   if(currentSel != totalCuts-1){
	currentSel = totalCuts-1;	// 只能在最后一刀中编辑输入
   }
   
   // 判断数据是否有效
   if((data < frontLimit) || (data > currentuser->backlimit)){	// 数据溢出
	MessageBox(hwnd,userpro_help[*curSysLang].msg_cutdata_overflow,
		  userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
	return totalCuts;
   }
   // 有效则更新结构体数据
   pProEdit->currentCutsNum = currentSel;
   pProEdit->userProCuts[currentSel] = data;
   UpdateProListWithData(hwnd,pProEdit,0);	// 修改,type=0	

   // 显示下一刀0.0数据
   currentSel = currentSel + 1;
   totalCuts = totalCuts +1;
   pProEdit->userProCuts[currentSel] = 0;
   // 更新结构体
   pProEdit->nInputCutsNum = totalCuts;
   pProEdit->currentCutsNum = currentSel;
   // 更新列表
   UpdateProListWithData(hwnd,pProEdit,1);	// 追加,type=1
   
   return totalCuts;

}

/*    比较输入
 *    flag = 0 : 加   1 : 减
 */
static int compareAddDecInput(HWND hwnd,userProgramEditData *pProEdit,int ret,int flag)
{
   int data;
   int frontLimit = getValidLimit();
   int currentSel = pProEdit->currentCutsNum;
   int totalCuts = pProEdit->nInputCutsNum ;	// 新增一刀数据
   //printf("total=%d current=%d \n",totalCuts,currentSel);
   if(currentSel != totalCuts-1){
	currentSel = totalCuts-1;	// 只能在最后一刀中编辑输入
   }
   if(!flag){
   	data = pProEdit->userProCuts[totalCuts - 2] + ret;
   }else{
	data = pProEdit->userProCuts[totalCuts - 2] - ret;
   }

   // 判断数据是否有效
   if((data < frontLimit) || (data > currentuser->backlimit)){	// 数据溢出
	ResetUserProgramEdit(hwnd,pProEdit,0);
	MessageBox(hwnd,userpro_help[*curSysLang].msg_cutdata_overflow,
		  userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
	return totalCuts;
   }
   // 有效则更新结构体数据
   pProEdit->currentCutsNum = currentSel;
   pProEdit->userProCuts[currentSel] = data;
   UpdateProListWithData(hwnd,pProEdit,0);	// 修改,type=0	

   // 显示下一刀0.0数据
   currentSel = currentSel + 1;
   totalCuts = totalCuts +1;
   pProEdit->userProCuts[currentSel] = 0;
   // 更新结构体
   pProEdit->nInputCutsNum = totalCuts;
   pProEdit->currentCutsNum = currentSel;
   // 更新列表
   UpdateProListWithData(hwnd,pProEdit,1);	// 追加,type=1

   ResetUserProgramEdit(hwnd,pProEdit,0);
   return totalCuts;


}

/*   修改已有数据
 */

static int modifyOneCutData(HWND hwnd,userProgramEditData *pProEdit,int data)
{
   int frontLimit = getValidLimit();
   int currentSel = pProEdit->currentCutsNum;
   int totalCuts = pProEdit->nInputCutsNum;
   int tmp;
   // 判断数据是否有效
   if((data < frontLimit) || (data > currentuser->backlimit)){	// 数据溢出
	MessageBox(hwnd,userpro_help[*curSysLang].msg_cutdata_overflow,
		  userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
	return totalCuts;
   }
   
   tmp = pProEdit->userProCuts[currentSel] & 0xff000000;	// 不改变状态标志
   tmp |= data;
   pProEdit->userProCuts[currentSel] = tmp;
   // 更新列表
   UpdateProListWithData(hwnd,pProEdit,0);
   // 插入后退出插入功能 
   ResetUserProgramEdit(hwnd,pProEdit,0);

   return totalCuts;

}

static int modifyOneCutDataNew(HWND hDlg,userProgramEditData *pProEdit)
{
   HWND hwndInputEdit = GetDlgItem(hDlg,IDC_INPUTCUTDATA);
   int frontLimit = getValidLimit();
   int currentSel = pProEdit->currentCutsNum;
   int totalCuts = pProEdit->nInputCutsNum;
   int tmp;
   char cutDataBuf[10];
   int  cutDataLen=0;
   int data;

   if(currentSel == totalCuts -1){ // ,最后一刀为0的数据
        SetWindowText(hwndInputEdit,"");
	return(-1);
   }
   // 读出数据
   cutDataLen = GetWindowTextLength(hwndInputEdit);	// 获得输入的字符
   memset(cutDataBuf,0,10);
   GetWindowText(hwndInputEdit,cutDataBuf,cutDataLen);
	    //memset(programEdit.inputbuf,0,10);
	    //strncpy(programEdit.inputbuf,cutDataBuf,cutDataLen);
	    //printf("%s len=%d\n",cutDataBuf,cutDataLen);
   data=isInputCutsDataValid(cutDataBuf,cutDataLen); 	// 判断输入是否有效
   if(data < 0){
	SetWindowText(hwndInputEdit,"");
	return -1;
   }

   // 判断数据是否有效
   if((data < frontLimit) || (data > currentuser->backlimit)){	// 数据溢出
	MessageBox(hDlg,userpro_help[*curSysLang].msg_cutdata_overflow,
		  userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
        SetWindowText(hwndInputEdit,"");
	return totalCuts;
   }
   
   tmp = pProEdit->userProCuts[currentSel] & 0xff000000;	// 不改变状态标志
   tmp |= data;
   pProEdit->userProCuts[currentSel] = tmp;
   // 更新列表
   UpdateProListWithData(hDlg,pProEdit,0);
   // 插入后退出插入功能 
   ResetUserProgramEdit(hDlg,pProEdit,0);
   SetWindowText(hwndInputEdit,"");

   return totalCuts;

}
/*   准备插入一刀数据,在当前位置对应的userProCuts数组中增加一刀
 *   参数: hwnd 列表句柄
 *        pProEdit
 *   返回值: 插入后的总刀数    
 */
static int PrepareToInsertOneCutData(HWND hwnd,userProgramEditData *pProEdit)
{
   int currentSel = pProEdit->currentCutsNum;
//   int totalCuts = pProEdit->nInputCutsNum+1;	// 插入后总刀数
   int index = 0;
   char dispbuf[40];
   float fdata = 0.0;
   sprintf(dispbuf," %03d        %07.2f   mm    ",index,fdata);
   SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_INSERTSTRING,currentSel,
				(LPARAM)dispbuf);	
   SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_SETCURSEL,currentSel,0);	   
/*
   // 循环将数据后移一刀
   for(index=totalCuts-1;index>currentSel;index--){
	pProEdit->userProCuts[index] = pProEdit->userProCuts[index-1];
   }
	
   pProEdit->userProCuts[currentSel] = 0;
   pProEdit->nInputCutsNum=totalCuts;
  
   // 刷新列表
   UpdateProListWithFileData(hwnd,pProEdit);
*/
   return currentSel;

}
/*   插入数据
 */
static int insertOneCutData(HWND hwnd,userProgramEditData *pProEdit,int data)
{
   int frontLimit = getValidLimit();
   int currentSel = pProEdit->currentCutsNum;
   int totalCuts = pProEdit->nInputCutsNum;
   int index;
   // 判断数据是否有效
   if((data < frontLimit) || (data > currentuser->backlimit)){	// 数据溢出
	MessageBox(hwnd,userpro_help[*curSysLang].msg_cutdata_overflow,
		  userpro_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);

	SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_DELETESTRING,currentSel,0);
	SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_SETCURSEL,currentSel,0);	
	
	ResetUserProgramEdit(hwnd,pProEdit,0);	// Clear Flag
	InvalidateRect(hwnd,NULL,TRUE);	// 引发窗口重绘
	return totalCuts;
   }
   totalCuts = totalCuts + 1;
   for(index=totalCuts-1;index>currentSel;index--){
	pProEdit->userProCuts[index] = pProEdit->userProCuts[index-1];
   }
	
   //pProEdit->userProCuts[currentSel] = 0;
   pProEdit->nInputCutsNum=totalCuts;

   pProEdit->userProCuts[currentSel] = data;
   // 更新列表
   UpdateProListWithFileData(hwnd,pProEdit);
   // 插入后退出插入功能 
   ResetUserProgramEdit(hwnd,pProEdit,0);

   return totalCuts;

}

static int insertOneCutDataNew(HWND hDlg,userProgramEditData *pProEdit)
{
   HWND hwndInputEdit = GetDlgItem(hDlg,IDC_INPUTCUTDATA);
   int frontLimit = getValidLimit();
   int currentSel = pProEdit->currentCutsNum;
   int totalCuts = pProEdit->nInputCutsNum;
   int tmp;
   char cutDataBuf[10];
   int  cutDataLen=0;
   int data;
   int index;

   if(currentSel == totalCuts -1){ // ,最后一刀为0的数据
        SetWindowText(hwndInputEdit,"");
	return(-1);
   }
   // 读出数据
   cutDataLen = GetWindowTextLength(hwndInputEdit);	// 获得输入的字符
   memset(cutDataBuf,0,10);
   GetWindowText(hwndInputEdit,cutDataBuf,cutDataLen);
	    //memset(programEdit.inputbuf,0,10);
	    //strncpy(programEdit.inputbuf,cutDataBuf,cutDataLen);
	    //printf("%s len=%d\n",cutDataBuf,cutDataLen);
   data=isInputCutsDataValid(cutDataBuf,cutDataLen); 	// 判断输入是否有效
   if(data < 0){
	SetWindowText(hwndInputEdit,"");
	return -1;
   }

   // 判断数据是否有效
   if((data < frontLimit) || (data > currentuser->backlimit)){	// 数据溢出
	MessageBox(hDlg,userpro_help[*curSysLang].msg_cutdata_overflow,
		  userpro_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);

	//SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_DELETESTRING,currentSel,0);
	//SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_SETCURSEL,currentSel,0);	
	SetWindowText(hwndInputEdit,"");
	//ResetUserProgramEdit(hwnd,pProEdit,0);	// Clear Flag
	//InvalidateRect(hwnd,NULL,TRUE);	// 引发窗口重绘
	return totalCuts;
   }
   totalCuts = totalCuts + 1;
   for(index=totalCuts-1;index>currentSel;index--){
	pProEdit->userProCuts[index] = pProEdit->userProCuts[index-1];
   }
	
   //pProEdit->userProCuts[currentSel] = 0;
   pProEdit->nInputCutsNum=totalCuts;

   pProEdit->userProCuts[currentSel] = data;
   // 更新列表
   UpdateProListWithFileData(hDlg,pProEdit);
   // 插入后退出插入功能 
   //ResetUserProgramEdit(hDlg,pProEdit,0);
   SetWindowText(hwndInputEdit,"");

   return totalCuts;

}

static int deleteOneCutData(HWND hwnd,userProgramEditData *pProEdit)
{
   int currentSel = pProEdit->currentCutsNum;
   int totalCuts = pProEdit->nInputCutsNum-1;	// 删除后总刀数
   int index;
   
   if(totalCuts == 0){		// 溢出处理
	pProEdit->nInputCutsNum = 1;
	pProEdit->userProCuts[totalCuts] = 0;
	pProEdit->userEditStatus = 0;
	UpdateProListWithFileData(hwnd,pProEdit);
	
	return(1);
   }
   int ret=MessageBox(hwnd,userpro_help[*curSysLang].msg_delcutdata_confirm,
		  userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
   switch(ret){
	case IDOK:		// 确认删除
		// 循环将数据后移一刀
	    for(index=currentSel;index<totalCuts;index++){
	    	pProEdit->userProCuts[index] = pProEdit->userProCuts[index+1];
  	    }
   	    pProEdit->userProCuts[totalCuts] = 0;	// 清除最后一刀数据  
   	    pProEdit->nInputCutsNum=totalCuts;
   		// 刷新列表
   	    UpdateProListWithFileData(hwnd,pProEdit);	    
	    break;
	}

   pProEdit->userEditStatus = 0;
   return totalCuts;

}

/*	 Delete all cut data behind current selection
 */
static int deleteBehindCutDatas(HWND hwnd,userProgramEditData *pProEdit)
{
   int currentSel = pProEdit->currentCutsNum;
   int totalCuts = pProEdit->nInputCutsNum;	// 删除前总刀数
   int ret=MessageBox(hwnd,userpro_help[*curSysLang].msg_delalldata_confirm,
		  userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
   switch(ret){
	case IDOK:		// 确认删除
	    memset(&(pProEdit->userProCuts[currentSel]),0,totalCuts-currentSel);	//清空数据
   	    pProEdit->nInputCutsNum=currentSel+1;		// 显示0.0
	    //pProEdit->currentCutsNum = 0;
   		// 刷新列表
   	    UpdateProListWithFileData(hwnd,pProEdit);	    
	    break;

	case IDCANCEL:		  
	    break;
	}

   pProEdit->userEditStatus = 0;
   return 1;

}

/*	 删除所有 数据
 */
static int deleteAllCutDatas(HWND hwnd,userProgramEditData *pProEdit)
{

   int ret=MessageBox(hwnd,userpro_help[*curSysLang].msg_delalldata_confirm,
		  userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
   switch(ret){
	case IDOK:		// 确认删除
		// 循环将数据后移一刀
	    memset(pProEdit->userProCuts,0,sizeof(pProEdit->userProCuts));	//清空数据
   	    pProEdit->nInputCutsNum=1;		// 显示0.0
	    pProEdit->currentCutsNum = 0;
   		// 刷新列表
   	    UpdateProListWithFileData(hwnd,pProEdit);	    
	    break;

	case IDCANCEL:		  
	    break;
	}

   pProEdit->userEditStatus = 0;
   return 1;

}

static int deleteOneData(int currentSel,int totalCuts,userProgramEditData *pProEdit)
{
   int index;
   
   for(index=currentSel;index<totalCuts;index++){
	pProEdit->userProCuts[index] = pProEdit->userProCuts[index+1];
   }
   pProEdit->userProCuts[totalCuts-1] = 0;	// 清除最后一刀数据  
   return totalCuts;

}
/*	 微调所有数据(如何保证数据的有效性?)
 */
static int justAllCutDatas(HWND hwnd,userProgramEditData *pProEdit,int data)
{
   int frontLimit = getValidLimit();
   int totalCuts = pProEdit->nInputCutsNum;	// 总刀数
   int index,action;   
   int tmp,overNum = 0;
   int ret,saveFlag = 1;

   action = pProEdit->justEdit.action;
   
   switch(action){
	case 0:		// reset
	    funcButtons[2].textpos = 4;	// 微调
   	    ResetUserProgramEdit(hwnd,pProEdit,3);
	    return(0);
	case 1:		//  加
	    for(index=0;index<totalCuts-1;index++){	// 先扫描一遍,看看是否超出极限
		tmp = pProEdit->userProCuts[index] + data;
		if((tmp < frontLimit) || (tmp > currentuser->backlimit)){
		    overNum++;
		}
	    }
	    if(overNum > 0){	// 有数据异常
		ret = MessageBox(hwnd,userpro_help[*curSysLang].msg_just_confirm,
		   	userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
		if(ret == IDCANCEL){
		    saveFlag = 0;
		}
	    }
 	    if(saveFlag){		// 计算存储
		index = 0;
		while(index<totalCuts-1){	
		   pProEdit->userProCuts[index] = pProEdit->userProCuts[index] + data;
		   tmp = pProEdit->userProCuts[index];
		   if((tmp < frontLimit) || (tmp > currentuser->backlimit)){
		    	    deleteOneData(index,totalCuts,pProEdit);
			    totalCuts--;
		   } else {
			index++;
		   }
		}
	    }
	    break;
	case 2:		// 减
	    for(index=0;index<totalCuts-1;index++){	// 先扫描一遍,看看是否超出极限
		tmp = pProEdit->userProCuts[index] - data;
		if((tmp < frontLimit) || (tmp > currentuser->backlimit)){
		    overNum++;
		}
	    }
	    if(overNum > 0){	// 有数据异常
		ret = MessageBox(hwnd,userpro_help[*curSysLang].msg_just_confirm,
		   	userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
		if(ret == IDCANCEL){
		    saveFlag = 0;
		}
	    }
 	    if(saveFlag){		// 计算存储
		index = 0;
		while(index<totalCuts-1){	
		   pProEdit->userProCuts[index] = pProEdit->userProCuts[index] - data;
		   tmp = pProEdit->userProCuts[index];
		   if((tmp < frontLimit) || (tmp > currentuser->backlimit)){
		    	    deleteOneData(index,totalCuts,pProEdit);
			    totalCuts--;
		   } else {
			index++;
		   }
		}
	    }
	    break;
   }

   pProEdit->nInputCutsNum = totalCuts;
   programEdit.justEdit.action = 0;
   funcButtons[2].textpos = 4;		// 微调

   InvalidateRect(hwnd,NULL,TRUE);	// 引发窗口重绘
   ResetUserProgramEdit(hwnd,pProEdit,3);	// 复位
   UpdateProListWithFileData(hwnd,pProEdit);	// 刷新列表	    

   return(totalCuts);
}

static void addPusherFlagToData(HWND hwnd,userProgramEditData *pProEdit)
{

   int currentSel = pProEdit->currentCutsNum;
   if((pProEdit->userProCuts[currentSel] & AUTOCUT_FLAG) == 0){	// 自动切刀标志和推纸标志只能二选一
	pProEdit->userProCuts[currentSel] = pProEdit->userProCuts[currentSel] | PUSHER_FLAG;
   	UpdateProListWithFileData(hwnd,pProEdit);	// 刷新列表
        //UpdateProListWithData(hwnd,pProEdit,0);
   }
   
} 

static void delPusherFlagFromData(HWND hwnd,userProgramEditData *pProEdit)
{
   int currentSel = pProEdit->currentCutsNum;
   pProEdit->userProCuts[currentSel] &=  ~PUSHER_FLAG;
   UpdateProListWithFileData(hwnd,pProEdit);	// 刷新列表
   //UpdateProListWithData(hwnd,pProEdit,0);      
}


static void changeAirFlagOfData(HWND hwnd,userProgramEditData *pProEdit)
{
   int currentSel = pProEdit->currentCutsNum;

   pProEdit->userProCuts[currentSel] ^=  AIR_FLAG;	// Reverse AIR_FLAG
   UpdateProListWithData(hwnd,pProEdit,0);
   
}


/*  增加自动切刀标志,须满足两个条件:
 *  1. 该数据比前一刀数据小
 *  2. 第一刀数据不能加 
 */
static void addAutoCutFlagToData(HWND hwnd,userProgramEditData *pProEdit)
{
   int currentSel = pProEdit->currentCutsNum;
   int foredata;
   int curdata = pProEdit->userProCuts[currentSel];
   if(curdata & PUSHER_FLAG){		// 已有推纸标志,不能增加自切标志
	releaseSecondFunction(hwnd);		// 刷新第二功能标志
	return;
   }   

   if(currentSel == 0){
        releaseSecondFunction(hwnd);		// 刷新第二功能标志
	return;
   }
   // Now currentSel >= 1
   foredata = pProEdit->userProCuts[currentSel-1] & 0x00ffffff;	// 取出数据比较
   curdata = pProEdit->userProCuts[currentSel] & 0x00ffffff;
   if(curdata < foredata){
   	pProEdit->userProCuts[currentSel] = pProEdit->userProCuts[currentSel] | AUTOCUT_FLAG;
	UpdateProListWithFileData(hwnd,pProEdit);	// 刷新列表
   }
   releaseSecondFunction(hwnd);		// 刷新第二功能标志

} 

static void delAutoCutFlagFromData(HWND hwnd,userProgramEditData *pProEdit)
{
   int currentSel = pProEdit->currentCutsNum;
   pProEdit->userProCuts[currentSel] &=  ~AUTOCUT_FLAG;
   releaseSecondFunction(hwnd);		// 刷新第二功能标志
   UpdateProListWithFileData(hwnd,pProEdit);	// 刷新列表
      
}

static void addAutoCutFlagToAllDatas(HWND hwnd,userProgramEditData *pProEdit)
{
   int totalCuts = pProEdit->nInputCutsNum;	// 总刀数
   int index;
   int foredata,curdata;
   if(totalCuts == 1){
        releaseSecondFunction(hwnd);		// 刷新第二功能标志
	return;
   }
   // Now totalCuts > 1
   for(index=1;index<totalCuts-1;index++){	// 第一刀数据不能加,最后一刀一直为0,不用加
	foredata = pProEdit->userProCuts[index-1] & 0x00ffffff;	// 取出数据比较
	curdata = pProEdit->userProCuts[index] & 0x00ffffff;
	if((curdata < foredata)&&((pProEdit->userProCuts[index] & PUSHER_FLAG) == 0)){
   	   pProEdit->userProCuts[index] = pProEdit->userProCuts[index] | AUTOCUT_FLAG;
  	}
   }
  
   releaseSecondFunction(hwnd);		// 刷新第二功能标志
   UpdateProListWithFileData(hwnd,pProEdit);	// 刷新列表
   
} 

/* 删除所由自动切刀标志 */
static void delAutoCutFlagFromAllDatas(HWND hwnd,userProgramEditData *pProEdit)
{
   int totalCuts = pProEdit->nInputCutsNum;	// 总刀数
   int index;

   // Now totalCuts > 1
   for(index=0;index<totalCuts;index++){		// 不论有无都同样处理
	pProEdit->userProCuts[index] &=  ~AUTOCUT_FLAG;
   }
   
   releaseSecondFunction(hwnd);		// 刷新第二功能标志
   UpdateProListWithFileData(hwnd,pProEdit);	// 刷新列表
      
}

/*************************************************************/
/*    主要完成界面处理,func 表示是否刷新功能键对应的文字
 */
static void ResetUserProgramEdit(HWND hwnd,userProgramEditData *pProEdit,int func)
{
    HDC hdc;
    pProEdit->userEditStatus = 0;
    hdc=BeginPaint(hwnd);
    RefreshBoxArea (hdc,&userProgramEditButton,8,435,80,40,1680,0);	// 输入尺寸  
    RefreshBoxArea (hdc,&userProgramEditBkgnd,8,395,120,40,8,395);	// 清除提示栏, 长度为80+40(微调)

    if(func){
    	RefreshFunctionButton(hdc,&userProgramEditButton,0,&funcButtons[func-1],1);
    }
    EndPaint(hwnd,hdc);

}

/***********************************************************/
/****************        Local Functions        ********************/

/*	程序名输入编辑框回调函数
 */
static void editProgramNameCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
     HWND mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     HWND hwndInputCutData = GetDlgItem(mainHwnd,IDC_INPUTCUTDATA);
     char proNameBuf[40];
     int  proNameLen=0;
     
     switch(nc){
	case EN_SETFOCUS:	// 编辑框得到焦点
	    // 切换输入法到 中文输入状态
	    SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)1);
	    if(isIMEOpenInMotor == 0){
		ShowWindow(myIMEWindow,SW_SHOW);
		isIMEOpenInMotor =1;
	    }
	    break;

	case EN_ENTER:		// Enter key 按下,程序名输入完毕
	    programEdit.isProNameNull = 0;		// 置标志位 isProNameNull
	    proNameLen = GetWindowTextLength(hwnd);	 	// 获得输入的字符,并保存到文件头结构体中
	    if(proNameLen == 0){
		strcpy(proNameBuf,"default");
	    }else{
	        GetWindowText(hwnd,proNameBuf,proNameLen);
	    }
	    // 首先将原来的清空
	    memset(programCreate.proHeader.introduction,0,sizeof(programCreate.proHeader.introduction));
	    strncpy(programCreate.proHeader.introduction,proNameBuf,proNameLen);	// 复制到文件头结构体中

            UpdateFileHeader(&programCreate,&programEdit,0);	// 更新文件头
	    // 清除第二功能标志

	    releaseSecondFunction(mainHwnd);
	    SetFocusChild(hwndInputCutData);
	    break;
     }
}

/*	程序数据输入编辑框回调函数
 */
static void editInputCutDataCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
     HWND mainHwnd;
     char cutDataBuf[10];
     int  cutDataLen=0;
     int ret;
     userProgramEditData *pProEdit = &programEdit;
     int status = pProEdit ->userEditStatus;	// 程序编辑方法	
     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄

     switch(nc){
	case EN_SETFOCUS:	// 编辑框得到焦点
	    // 切换输入法到 数字输入状态
	    SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);
	    if(isIMEOpenInMotor == 1){
	        ShowWindow(myIMEWindow,SW_HIDE);
		isIMEOpenInMotor = 0;
	    }

	    break;
	case EN_CHANGE:		// 有数字键按下
	    cutDataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    if(cutDataLen > 7){		// 输入超出范围,clear
		SetFocusChild(hwnd);
		SetWindowText(hwnd,"");	// 清空输入区 
    	    }
	    break;
	case EN_ENTER:			
	    if(proRunStatus.runStatus){		// 非编辑状态,直接返回
		break;
	    }
	    // 先检查输入数据的有效性
            // 将有效数据写入userProCuts[currentCutsNum],currentCutsNum递增,nInputCutsNum递增
	    // 更新列表框数据,当前输入下移到下一刀位置
            cutDataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    GetWindowText(hwnd,cutDataBuf,cutDataLen);
	    //memset(programEdit.inputbuf,0,10);
	    //strncpy(programEdit.inputbuf,cutDataBuf,cutDataLen);
	    //printf("%s len=%d\n",cutDataBuf,cutDataLen);
	    ret=isInputCutsDataValid(cutDataBuf,cutDataLen); 	// 判断输入是否有效
 	    if(ret >= 0 ){					// 输入有效,返回转换后的数据
		switch(status){
		    case 0:	// 正常编辑
		    case 7:	// 示教		
			editOneCutData(mainHwnd,pProEdit,ret);
			break;	
		    case 1:	// 标签编辑
			//userProgramLableEdit(mainHwnd,pProEdit,ret);
			break;
		    case 2:	// 等分编辑
			//userProgramHalfEdit(mainHwnd,pProEdit,ret);
			break;
		    case 3:	// 修改数据,修改完退出
			//modifyOneCutData(mainHwnd,pProEdit,ret);
			break;	
		    case 4:	// 插入数据
			//insertOneCutData(mainHwnd,pProEdit,ret);
			break;	
		    case 6:	// 微调数据
			justAllCutDatas(mainHwnd,pProEdit,ret);
			break;	
		    case 8:	// 加
			compareAddDecInput(mainHwnd,pProEdit,ret,0);
			break;	
		    case 9:	// 减
			compareAddDecInput(mainHwnd,pProEdit,ret,1);
			break;	
 	    	}
	    } else {	// 数据非法
		MessageBox(mainHwnd,userpro_help[*curSysLang].msg_cutdata_overflow,
		   	userpro_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
	    }
	    memset(cutDataBuf,0,sizeof(cutDataBuf));
  	    SetWindowText(hwnd,cutDataBuf);		// 清空输入区 

	    break;
     }
}

/*	程序输入列表框回调函数
 */
static void listProgramCutsCallback(HWND hwnd,int id,int nc,DWORD add_data)
{ 
     HWND mainHwnd;
     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     switch(nc){
	case LBN_SETFOCUS:	// 列表框得到焦点

	    break;
	case LBN_SELCHANGE:	// 列表框选项有变化,增加小箭头
	    //printf("selchange...\n");
	    break;
	case LBN_ENTER:		// Enter key 按下, 编辑该刀数据
            // 得到当前选中项	    
 	    // 设置焦点到输入框 
	    break;
     }
}

/*********************** 机器运行控制函数 ************************/
/*   机器步进,	dir = 0 前进
 *		dir = 1 后退
 *		step 步进量  单位0.01mm
 */
void motorRunStep(int dir,int step)
{
    int data = step;
    char cmdSendBuf[13];
    memset(cmdSendBuf,0,12);
    cmdData.flag = 'F';
    cmdData.data = data;
    switch(dir){
	case MOTOR_FORWARD:		// 前进
	    cmdData.status = 0x30;
	    break;
	case MOTOR_BACKWARD:		// 后退
	    cmdData.status = 0x31;
	    break;

    }
    setCmdFrame(&cmdData,cmdSendBuf);	// 组帧	step*0.01毫米
    sendCommand(cmdSendBuf);  		// Send
}

/*  处理状态帧,收到状态帧之后首先调用该函数处理
 *  功能: 刷新当前切刀和推纸器状态
 *  返回: 切刀动作是否完成 1:完成 0:没有
 */
static int getDevStatus(userProgramRunStatus *pStatus,int status)
{
     HDC hdc;
     int ret = 0;
     int isStatusChanged = 0;
     int cutOldStatus = pStatus->cutStatus;
     int pusherOldStatus = pStatus->pusherStatus;
     
     if(status & CUT_STATUS_MASK){
	pStatus->cutStatus = 1;	// 切刀切下
	pStatus->lastCutPos = motorStatus.currentCutPos;	// 更新上次切下位置
	pStatus->forCutLen = 0;	// 置位刀前尺寸
     } else {
	pStatus->cutStatus = 0;
     }
   
     if(status & PUSHER_STATUS_MASK){
	pStatus->pusherStatus = 1;
     } else {
	pStatus->pusherStatus = 0;
     }

     if(status & WHICH_LIMIT_MASK){
	pStatus->whichLimit = 1;	// 中极限有效
     } else {	
	pStatus->whichLimit = 0;	// 前极限有效
     }

     if(status & MACHINE_STOP_MASK){
	pStatus->machineStop = 0;	// 该bit为0标志停止,1表示移动中
	pStatus->runStop = 1;	// 运行
     } else {	
	pStatus->machineStop = 1;	
	pStatus->runStop = 0;	// 停止
     }

     if(cutOldStatus != pStatus->cutStatus){
	    pStatus->cutActTimes++;
	    isStatusChanged = 1;
     }
     if(pusherOldStatus != pStatus->pusherStatus){
	    pStatus->pusherActTimes++;
	    isStatusChanged = 1;
     }
     if(isStatusChanged){
	//printf("getStatus 1:%d \n",pStatus->cutPusherActTimes );
	if((pStatus->pusherStatus == 1) &&(pStatus->cutStatus == 1)
					&& (pStatus->cutPusherActTimes == 0)){
	    	pStatus->cutPusherActTimes = 1;	
	}
	//printf("getStatus 2 :%d \n",pStatus->cutPusherActTimes );
	if((pStatus->pusherStatus == 0) &&(pStatus->cutStatus == 0) 
					&& (pStatus->cutPusherActTimes == 1)){
    	    pStatus->cutPusherActTimes = 2;	
	    if(pStatus->cutActTimes >= 2){		// 切刀裁切动作完成,刀前尺寸清零
	    	//pStatus->forCutLen = 0;
	    	// 裁切动作完成
		ret = 1;
	    }
	}
	    //printf("getStatus 3 :%d \n",pStatus->cutPusherActTimes );
     }

     return(ret);
/*
     //printf("getDevStatus: whichLimit = %d \n",pStatus->whichLimit);
     //printf("getDevStatus: cut %d -> %d \n",cutOldStatus,pStatus->cutStatus);
     //printf("getDevStatus: pusher %d -> %d\n",pusherOldStatus,pStatus->pusherStatus);
     if(pStatus->runStatus > 0){	// 自动/手动运行时有效
	if(cutOldStatus != pStatus->cutStatus){
	    pStatus->cutActTimes++;
	    isStatusChanged = 1;
	}
	if(pusherOldStatus != pStatus->pusherStatus){
	    pStatus->pusherActTimes++;
	    isStatusChanged = 1;
	}
	if(isStatusChanged){
	    //printf("getStatus 1:%d \n",pStatus->cutPusherActTimes );
	    if((pStatus->pusherStatus == 1) &&(pStatus->cutStatus == 1)
					&& (pStatus->cutPusherActTimes == 0)){
	    	pStatus->cutPusherActTimes = 1;	
	    }
	    //printf("getStatus 2 :%d \n",pStatus->cutPusherActTimes );
	    if((pStatus->pusherStatus == 0) &&(pStatus->cutStatus == 0) 
					&& (pStatus->cutPusherActTimes == 1)){
	    	pStatus->cutPusherActTimes = 2;	
		//if(pStatus->cutActTimes >= 2){		// 切刀裁切动作完成,刀前尺寸清零
		//    pStatus->forCutLen = 0;
		//}
	    }
	    //printf("getStatus 3 :%d \n",pStatus->cutPusherActTimes );
	}
     }
*/


}


/*   重置运行状态,即将发送一条运行指令前调用
 */
static void resetMotorRunStatus(userProgramRunStatus *pStatus)
{
    pStatus->isOneCutDone = 0;	// 更新状态
    pStatus->cutActTimes = 0;
    pStatus->pusherActTimes = 0;
    pStatus->cutPusherActTimes = 0;
}
/*   发送一刀运行指令 
 *   返回 	0  该帧为正常帧
 *	  	1  推纸帧
 */ 
static int motorRunOneCut(userProgramEditData *pProEdit,userProgramRunStatus *pStatus)
{
    int ret = 0;
    char cmdSendBuf[13];
    int currentCut = pProEdit->currentCutsNum;
    int data = pProEdit->userProCuts[currentCut];	// 取出数据
    int runStatus = 0;	// So, 在自切模式下 runStatus = 1 否则 = 0
    int unit = currentuser->unit;
    int tmp = data & 0x00ffffff;

    if((tmp < currentuser->frontlimit) || (tmp > currentuser->backlimit)){
	return 0;
    }

    if(pStatus->runStatus == 3){
	runStatus = 1;
    }    
    if(data & PUSHER_FLAG){	// 数据包含推纸信息,发送推纸帧
	ret = 1;
	runStatus |= 0x02;		// Bit 1
    }
    //if((data & AUTOCUT_FLAG) && (pStatus->runStatus == 3)){// 只有在自动切刀模式才发出该标志
    if(data & AUTOCUT_FLAG){
	runStatus |= 0x04;		// Bit2
    }
    if(data & AIR_FLAG){	// 
	runStatus |= 0x08;		// Bit3
    }
    data = data & 0x00ffffff;	// 取出有效数据    
    if(unit){
	data = inch_to_mm(data);
    }
    cmdData.flag = 'G';
    cmdData.data = data;
    cmdData.status = 0x30 | runStatus;		// Bit 0-2 对应自动/手动 推纸 自动切刀
    if((runStatus & 0x04)){	// Should not be this,But the lower machine is Outand cann't be changed
	//cmdData.status = 0x36;
	cmdData.status |= 0x06;	// can not change AIR_FLAG
    }
    setCmdFrame(&cmdData,cmdSendBuf);	// 组帧
    sendCommand(cmdSendBuf);  		// Send
    //printf("motorRunOneCut %d %s\n",data,cmdSendBuf);
    return(ret);

}
/*   自动程序运行控制,检测切刀和推纸器动作变化作为主要控制标志,下位机发送的停止信号
 *   仅用于推纸刀的运行
 */
static int motorRunAuto(HWND hwnd,userProgramEditData *pProEdit,userProgramRunStatus *pStatus)
{
    HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);
    int runStatus = pStatus->runStatus;
    int currentCut = pProEdit->currentCutsNum;
    int totalCut = pProEdit->nInputCutsNum;
    int curCutPos = motorStatus.currentCutPos;	// 当前切刀位置
    int destCut = pStatus->destCutNum;
    int ret;
    int data = pProEdit->userProCuts[currentCut];	// 取出数据
    int curCutPushFlag = 0;

    if(data & PUSHER_FLAG){	// 数据包含推纸信息,发送推纸帧
	curCutPushFlag = 1;
    }
/*
    printf("Auto Run !currentCut = %d times = %d %d %d \n",currentCut,pStatus->cutActTimes,
    		pStatus->pusherActTimes,pStatus->cutPusherActTimes);
    printf("Auto Run !machineStop = %d curCutPushFlag = %d \n",pStatus->machineStop,curCutPushFlag);
    printf("DstCutPos = %d curCutPos = %d\n",pStatus->lastCutPos,curCutPos);
*/
    // 判断一次动作是否完成,完成则发送下一刀指令,否则直接返回   
    // 如果该刀为推纸刀,则不用等待状态信息,收到当前切刀位置与发送数据相等时运行到下一刀
//    if(((pStatus->cutActTimes == 2) && (pStatus->pusherActTimes >= 2)) || 
    if(((pStatus->cutPusherActTimes == 2) && (pStatus->cutActTimes >= 2))||	 // 防止运行前两者都在下方 
	((pStatus->machineStop == 1) && (curCutPushFlag == 1) && (curCutPos == pStatus->curDestPos))){
	pStatus->isOneCutDone = 1;
	// 处理刀前尺寸
	pStatus->lastCutPos = pStatus->curDestPos;	// 更新参考位置为上回目标位置
	pStatus->forCutLen = 0;			// clear

	//pProEdit->currentCutsNum++;			// 自动运行到下一刀
	//if(pProEdit->currentCutsNum == totalCut-1){	// 倒数第一刀
	//     pProEdit->currentCutsNum = 0;
	//}
	pStatus->destCutNum++;
	if(pStatus->destCutNum == totalCut - 1){	// 倒数第一刀
	     pStatus->destCutNum = 0;
	}

	currentCut = pProEdit->currentCutsNum;
	destCut = pStatus->destCutNum;
	//SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);	 // 高亮当前运行项
    }

    if(pStatus->isOneCutDone){	// 前一刀已经完成
	currentCut = pProEdit->currentCutsNum;
	destCut = pStatus->destCutNum;
	if(destCut == 0){	// 是最后一刀,则重头开始
	    pProEdit->currentCutsNum = 0;
	    SendMessage(hwndProCutList,LB_SETCURSEL,0,0);	 // 高亮当前运行项
	} 

	currentCut = pProEdit->currentCutsNum;
	//SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);	 // 高亮当前运行项
	if(destCut == (currentCut + 1) ){
	    SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);	// Reset Before the next	
	    SendMessage(hwnd,MSG_KEYDOWN,SCANCODE_CURSORBLOCKDOWN,0); //  Move to next
	    pProEdit->currentCutsNum++;
	}
	currentCut = pProEdit->currentCutsNum;
	pStatus->curDestPos = pProEdit->userProCuts[currentCut] & 0x00ffffff;	// 更新保存当前目的位置
	ret = motorRunOneCut(pProEdit,pStatus);	// 继续运行
	pStatus->machineStop = 0;		// 移动中
	resetMotorRunStatus(pStatus);		// 更新状态,times,isOneCutDone清零
    }
    //printf("Enter Auto Run !currentCut = %d \n",pProEdit->currentCutsNum);
    //currentCut = pProEdit->currentCutsNum;
    //SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);	 // 高亮当前运行项
    
    return(pProEdit->currentCutsNum);

}

/*   半自动程序运行控制
 */
static int motorRunHalfAuto(HWND hwnd,userProgramEditData *pProEdit,userProgramRunStatus *pStatus)
{
    int currentCut;
    int totalCut = pProEdit->nInputCutsNum;
    HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST); 
    
    currentCut = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);// 得到当前列表框选项
    if(currentCut >= totalCut-1){	// 是最后一刀,数据为0
	return(0);
    } 
    pProEdit->currentCutsNum = currentCut;
    motorRunOneCut(pProEdit,pStatus);	// 继续运行 
    resetMotorRunStatus(pStatus);		// 更新状态
    
    return(currentCut);

}
/*   手动程序运行控制
 */
static int motorRunMannual(HWND hwnd,userProgramEditData *pProEdit,userProgramRunStatus *pStatus)
{
    char dataBuf[10];
    char cmdSendBuf[13];
    int data,len;
    int unit = currentuser->unit;
    int frontLimit = getValidLimit();
    HWND  hwndInputCutData = GetDlgItem(hwnd,IDC_INPUTCUTDATA);

    len = GetWindowTextLength(hwndInputCutData);	// 获得输入的字符
    GetWindowText(hwndInputCutData,dataBuf,len);
    data = isInputCutsDataValid(dataBuf,len); 	// 判断输入是否有效
    
    switch(pStatus->mannualFlag){
	case 1:	// ADD
	     data = motorStatus.currentCutPos + data;
	     break;
	case 2:	// SUB
	     data = motorStatus.currentCutPos - data;
	     break;
	default:
	     break;
    }
    if(pStatus->mannualFlag){
    	pStatus->mannualFlag = 0;	// Clear
	// 将结果更新到输入框 
	SetWindowText(hwndInputCutData,"");	// clear
	refreshCurrentPos(hwndInputCutData,data);
    }

    //printf("data = %d \n",data);
    if((data >= frontLimit) && (data <= currentuser->backlimit)){	// 输入有效,在极限范围内
	// 更新刀前尺寸
	pStatus->curDestPos = data;
	pStatus->forCutLen = 0;		// What should i do here?

    	cmdData.flag = 'G';
   	cmdData.status = 0x33;
	if(unit){
	    data = inch_to_mm(data);
        }
    	cmdData.data = data;
    	setCmdFrame(&cmdData,cmdSendBuf);	// 组帧
        sendCommand(cmdSendBuf);  		// Send
    } else {	// 数据非法
	MessageBox(hwnd,userpro_help[*curSysLang].msg_cutdata_overflow,
		   	userpro_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
	SetWindowText(hwndInputCutData,"");	// clear   
	
    }

    return(data);

}

static void motorRunProcess(HWND hDlg,userProgramEditData *pProEdit,userProgramRunStatus *pStatus)
{
    int runStatus = pStatus->runStatus;

    switch(runStatus){
	case 1:		// 自动运行
	//case 3:
	    motorRunAuto(hDlg,pProEdit,pStatus);
	    break;
	case 0:		// 编辑模式下的示教编辑
	    //printf("Trainning Run...\n");
	    motorRunMannual(hDlg,pProEdit,pStatus);
	    break;
	case 2:		// 手动运行    
	    //printf("Enter Mannual Run...\n");
	    motorRunMannual(hDlg,pProEdit,pStatus);
	    break;
    }
}

/* 列表初始化函数,读出文件内容
 * 如果文件为空,则新建程序文件,初始化文件头
 */
static int InitProCutsList(HWND hwnd,userProgramEditData *pProEdit,fromProListToProData *pProCreate)
{
    UpdateProListWithFileData(hwnd,pProEdit);
    // 初始化列表头(序号,尺寸)
    //SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_ADDSTRING,0,(LPARAM)message_cutslist_header);
    
    return 0;
}

/*  判断在当前状态下按键(F1-F6)是否有效
 */
static BOOL isFunctionButtonValid(userProgramRunStatus *pStatus)
{
    if(pStatus->runStop > 0){	// 运行时都无效
	return FALSE;	
    }
    if(pStatus->runStatus > 0){
	return FALSE;
    }
   
    return TRUE;	// 停止,输入
}

static int loadUserProButtonIcon(void)
{
    int langtype = *curSysLang;
    switch(langtype){	
	case 1:	// 英文
   	    if (LoadBitmap (HDC_SCREEN, &userProgramEditBkgnd, "./res/userProgramRun1.jpg"))
         	return 1;  
   	    if (LoadBitmap (HDC_SCREEN, &userProgramEditButton, "./res/userProEdit_en.jpg"))
         	return 1;  
    	    if (LoadBitmap (HDC_SCREEN, &buttonsOkEsc, "./res/buttonsOkEsc_en.jpg"))
         	return 1;	
	    break;
	case 0:
	default:
   	    if (LoadBitmap (HDC_SCREEN, &userProgramEditBkgnd, "./res/userProgramRun0.jpg"))
         	return 1;  
   	    if (LoadBitmap (HDC_SCREEN, &userProgramEditButton, "./res/userProEdit.jpg"))
         	return 1;  
    	    if (LoadBitmap (HDC_SCREEN, &buttonsOkEsc, "./res/buttonsOkEsc.jpg"))
        	return 1;  
	    break;    
    }
    return(0);
 

}


/*   刷新状态图标,包括设备状态,自动/半自动/手动,运行/停止,退纸标志 
 *   flag 是否设置焦点标志
 */
static void refreshUserProgramRunStatus(HWND hwnd,HDC hdc,userProgramRunStatus *pStatus,int type,int flag)
{
     int currentCut = programEdit.currentCutsNum;
     HWND  hwndInputCutData = GetDlgItem(hwnd,IDC_INPUTCUTDATA);
     HWND  hwndProCutList = GetDlgItem(hwnd,IDL_PROGRAMCUTSLIST);
     HWND  hwndCutCnt = GetDlgItem(hwnd,IDC_CUTCOUNTER);
     int status;

     switch(type){
	case 0:	// 刷自动区域
	    status = pStatus->runStatus;
     	    switch(status){
		case 0:			// 输入
	    	    RefreshBoxArea (hdc,&userProgramEditButton,INPUT_X,0,80,40,BMP_INPUT_X,0);
		    ShowWindow(hwndInputCutData,SW_SHOW);
		    ShowWindow(hwndCutCnt,SW_HIDE);
		    if(flag){
		        SetFocusChild(hwndInputCutData);
			SetWindowText(hwndInputCutData,"");	//clear
		    }
	    	    break;
		case 1:			// 自动 
	    	    RefreshBoxArea (hdc,&userProgramEditButton,AUTO_X,0,80,40,BMP_AUTO_X,0);
		    ShowWindow(hwndInputCutData,SW_HIDE);
		    ShowWindow(hwndCutCnt,SW_SHOW);
		    if(flag){	 
		    	SetFocusChild(hwndProCutList);	
			SendMessage(hwndProCutList,LB_SETCURSEL,currentCut,0);// 高亮第一项
		    }
		    
	    	    break;
		case 2:			// 手动
	    	    RefreshBoxArea (hdc,&userProgramEditButton,
					MANNUAL_X,0,80,40,BMP_MANNUAL_X,0); 
		    ShowWindow(hwndInputCutData,SW_SHOW);
		    ShowWindow(hwndCutCnt,SW_SHOW);
		    if(flag){
		        SetFocusChild(hwndInputCutData);
			SetWindowText(hwndInputCutData,"");	//clear	
		    }
	    	    break;
     	     }
	     break;
        case 1:	// 刷运行/停止区域
     	    status = pStatus->runStop;	// 运行/停止 
            switch(status){
		case 0:			// 停止
	    	    RefreshBoxArea (hdc,&userProgramEditButton,STOP_X,0,80,40,BMP_STOP_X,0);	
	    	    break;
		case 1:
	    	    RefreshBoxArea (hdc,&userProgramEditButton,RUN_X,0,80,40,BMP_RUN_X,0);
	    	    break;
    	    }
            break;
	case 2:	// 刷推纸区域
	    if(pStatus->pusher){		// 退纸
		RefreshBoxArea (hdc,&userProgramEditButton,PUSHER_X,0,80,40,BMP_PUSHER_X,0);	
     	    } else {
		RefreshBoxArea (hdc,&userProgramEditBkgnd,PUSHER_X,0,80,40,PUSHER_X,0);	
     	    }
	    break;
	case 3:	// 刷Shift区域
	    if(pStatus->secondFunc){		// shift
		RefreshBoxArea (hdc,&userProgramEditButton,SHIFT_X,0,80,40,BMP_SHIFT_X,0);	
     	    } else {
		RefreshBoxArea (hdc,&userProgramEditBkgnd,SHIFT_X,0,80,40,SHIFT_X,0);	
     	    }
	    break;
	case 4:	// 刷新机器状态区域  刀上/刀下  推上/推下
	    //printf("refreshUserProgramRunStatus...%d %d\n",pStatus->cutStatus,pStatus->pusherStatus);
	    if(pStatus->cutStatus){		// 刀下
		RefreshBoxArea (hdc,&userProgramEditButton,CUTDOWN_X,0,80,40,BMP_CUTDOWN_X,0);	
     	    } else {			// 刀上
		RefreshBoxArea (hdc,&userProgramEditButton,CUTUP_X,0,80,40,BMP_CUTUP_X,0);	
     	    }

	    if(pStatus->pusherStatus){		// 推下
		RefreshBoxArea (hdc,&userProgramEditButton,
					    PUSHDOWN_X,0,80,40,BMP_PUSHDOWN_X,0);	
     	    } else {			// 推上
		RefreshBoxArea (hdc,&userProgramEditButton,PUSHUP_X,0,80,40,BMP_PUSHUP_X,0);	
     	    }
	    break;
     }
}
/*   刷新F1 - F6 
 */
static void refreshFunctionButtons(HDC hdc,const BITMAP *pBitmap)
{
     int index ;
     for(index=0;index<6;index++){
	RefreshFunctionButton (hdc,pBitmap,0,&funcButtons[index],1);
     } 
}

/*   对话框初始化函数
 */
static void initUserproEditDlgBkgnd(HWND hwnd,HDC hdc,userProgramRunStatus *pStatus)
{
     int status = pStatus->runStatus;
     int curPos = motorStatus.currentCutPos;
     FillBoxWithBitmap (hdc, 0, 0, 0, 0, &userProgramEditBkgnd); 	// 背景 
     FillBoxWithBitmap (hdc, 430, 380, 0, 0, &cutIconBkgnd); 		// 切纸器图标
     RefreshBoxArea (hdc,&userProgramEditButton,430,330,80,40,2160,0);	// 刀前尺寸
     
     if(status == 0){	// 输入
	RefreshBoxArea (hdc,&userProgramEditButton,8,435,80,40,1680,0);		// 输入尺寸
        RefreshButtonArea (hdc,&buttonsOkEsc,MSG_KEYUP,CANCEL_X,CANCEL_Y,40,40, 1);// 清除
	refreshFunctionButtons(hdc,&userProgramEditButton);
	RefreshButtonArea (hdc,&buttonsOkEsc,MSG_KEYUP,ENTER_X,ENTER_Y,40,40, 0);// Enter 
     }
     if(status == 1){	// 自动
     	RefreshBoxArea (hdc,&userProgramEditButton,430,279,80,40,3760,0);// 下刀计数
     	RefreshBoxArea (hdc,&userProgramEditButton,460,95,80,40,0,0);// F1
     	RefreshBoxArea (hdc,&userProgramEditButton,540,95,80,40,3840,0);// clear
     }
     if(status == 2){	// 手动
	RefreshBoxArea (hdc,&userProgramEditButton,8,435,80,40,1680,0);		// 输入尺寸
        RefreshButtonArea (hdc,&buttonsOkEsc,MSG_KEYUP,CANCEL_X,CANCEL_Y,40,40, 1);// 清除
	RefreshButtonArea (hdc,&buttonsOkEsc,MSG_KEYUP,ENTER_X,ENTER_Y,40,40, 3);// Run
     	RefreshBoxArea (hdc,&userProgramEditButton,430,279,80,40,3760,0);// 下刀计数
     	RefreshBoxArea (hdc,&userProgramEditButton,460,95,80,40,0,0);// F1
     	RefreshBoxArea (hdc,&userProgramEditButton,540,95,80,40,3840,0);// clear
     	RefreshBoxArea (hdc,&userProgramEditButton,460,140,80,40,4080,0);// F4
     	RefreshBoxArea (hdc,&userProgramEditButton,540,140,80,40,4000,0);// computer
     }  
     //if(flag){		
     	refreshCurrentPosByIcon(hdc,curPos);
	
     	refreshUserProgramRunStatus(hwnd,hdc,pStatus,0,0);	// 刷背景是不设置焦点
     	refreshUserProgramRunStatus(hwnd,hdc,pStatus,1,0);
     	getDevStatus(&proRunStatus,motorStatus.devStatus);	// 读出机器状态
     	refreshUserProgramRunStatus(hwnd,hdc,pStatus,4,0);    // 刷新状态 
    //}
}

static int initUserProEditDlg(HWND hwnd,userProgramEditData *pProEdit, fromProListToProData *pProCreate)
{
    int ret;
    int id = pProCreate->proHeader.id;
    char numBuf[4];
    pProEdit->userEditStatus = 0;		//正常编辑
    pProEdit->currentCutsNum = 0;
    pProEdit->inputbuf[0] = '\0';
    pProEdit->justEdit.action = 0;		// 微调标志清除
  
    proRunStatus.runStatus = 0;	// 输入模式
    proRunStatus.runStop = 0;	// 停止
    proRunStatus.pusher = 0;		// 推纸器无动作
    proRunStatus.secondFunc = 0;	// 无第二功能
    proRunStatus.whichLimit = 0;	// 前极限有效
    proRunStatus.machineStop = 1;	// 停止

    proRunStatus.curDestPos = 0;
    proRunStatus.lastCutPos = 0;
    proRunStatus.forCutLen = 0;
    proRunStatus.mannualFlag = 0;

    sprintf(numBuf," %2d",id);
    SetDlgItemText(hwnd,IDC_PROGRAMNUM,numBuf); // 序号

    if(pProCreate->isFileNew){		// 文件为空
	pProEdit->nInputCutsNum = 1;	// 显示0.0
	pProEdit->currentCutsNum = 0;	// 高亮该项
	pProEdit->isProNameNull = 1;			// 程序名为空
	memset(pProEdit->userProCuts,0,sizeof(pProEdit->userProCuts));	//清空数据
	
    } else {
	// 读出文件数据,保存到结构体中
	ret = loadUserProgramFromFile(pProCreate->filePathname,&(pProCreate->proHeader),pProEdit);
	pProEdit->nInputCutsNum = ret-2;
	if(strlen(pProCreate->proHeader.introduction) == 0){
		pProEdit->isProNameNull = 1;
	} else{
	   pProEdit->isProNameNull = 0;
	   SetDlgItemText(hwnd,IDC_PROGRAMNAME,pProCreate->proHeader.introduction); // 说明
	}
    }
    InitProCutsList(hwnd,pProEdit,pProCreate);	//初始化列表框

    return 0;
}

static char lastListItem[40];
static int mannualEditFlag = 0;	// 手动运行时编辑框标志;
static int autoCutChangeFlag = 0;

static int testCutFlag = 0,testPusherFlag = 0;
static char testbuffer[13];

static int InitUserProDialogBoxProc (HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
    HDC hdc;
    HWND  hWnd;
    int currentSel;
    int curPos;
    int ret;
    char cmdRecvBuf[13];
    char timeBuff[40];		// 时钟显示,当前位置显示
    char starttip[8],endtip[8];
    int frontlimit;
    int unit = currentuser->unit;

    HWND  hwndProNum = GetDlgItem(hDlg,IDC_PROGRAMNUM);
    HWND  hwndProName = GetDlgItem(hDlg,IDC_PROGRAMNAME);
    HWND  hwndInputCutData = GetDlgItem(hDlg,IDC_INPUTCUTDATA);
    HWND  hwndProCutList = GetDlgItem(hDlg,IDL_PROGRAMCUTSLIST);
    HWND  hwndCurrentPos = GetDlgItem(hDlg,IDC_CURPOS);
    HWND  hwndCutCounter = GetDlgItem(hDlg,IDC_CUTCOUNTER);
    HWND  hwndClock = GetDlgItem(hDlg,IDC_TIME);
    HWND  hwndTrackBar;

    switch (message) {
    case MSG_INITDIALOG:
	SetNotificationCallback(hwndInputCutData, editInputCutDataCallback);
	SetNotificationCallback(hwndProName, editProgramNameCallback);
	//SetNotificationCallback(hwndProCutList, listProgramCutsCallback);
	if(unit){
	    SetDlgItemText(hDlg,IDC_PROGRAMCUTSLISTHEAD,
				userpro_help[*curSysLang].msg_cutslist_header_inch);
	} else {
	    SetDlgItemText(hDlg,IDC_PROGRAMCUTSLISTHEAD,
				userpro_help[*curSysLang].msg_cutslist_header);
	}

	// 创建滑块
	hwndTrackBar = CreateWindow(CTRL_TRACKBAR,
				"",
				WS_VISIBLE | TBS_VERTICAL | TBS_TIP,
				IDC_TRACKBAR,
				380,90,48,385,hDlg,0);
 	// 设置字体
	//SetWindowFont(hwndClock,userFont24);
	SetWindowFont(hwndInputCutData,userFont24);
	SetWindowFont(hwndProCutList,userFont24);
	SetWindowFont(hwndCurrentPos,userFont24);
	SetWindowFont(hwndCutCounter,userFont24);
	//SetWindowFont(hwndProNum,userFont24);

	// 设置字体颜色
	SetWindowElementColorEx(hwndInputCutData,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 0, 0)); //Red	
	SetWindowElementColorEx(hwndCurrentPos,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 255, 0)); //Yellow	
	SetWindowElementColorEx(hwndClock,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 255, 255)); //White
	SetWindowElementColorEx(hwndCutCounter,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 0, 0)); //Red		

	userProExitFlag = 0;
	int posMin = currentuser->frontlimit / 100;
	int posMax = currentuser->backlimit / 100;
	// 设置滑块范围
	SendMessage(hwndTrackBar,TBM_SETRANGE,posMin,posMax);
	SendMessage(hwndTrackBar,TBM_SETTICKFREQ,100,0);	// 设置刻度间格
	//SendMessage(hwndTrackBar,TBM_SETPAGESIZE,100,0);	// 设置刻度间格
	sprintf(starttip,"0");
	sprintf(endtip,"%d",posMax);
	SendMessage(hwndTrackBar,TBM_SETTIP,(WPARAM)starttip,(LPARAM)endtip);	// 设置刻度间格

	SetTimer(hDlg,IDC_TIMER,50);	// 时钟显示,100ms
	// 初始化界面
	initUserProEditDlg(hDlg,&programEdit,&programCreate);
	curPos = motorStatus.currentCutPos;
	SendMessage(hwndTrackBar,TBM_SETPOS,curPos/100,0);	// 设置滑块位置

	refreshCurrentPos(hwndCurrentPos,proRunStatus.forCutLen);	// 刀前尺寸

   	refreshCutCounter(hDlg);
        ShowWindow(hwndCutCounter,SW_HIDE);
	// 保存程序文件,更新使用次数和时间
	UpdateFileHeader(&programCreate,&programEdit,1);	// 保存

	saveUserProgramToFile(programCreate.filePathname,&(programCreate.proHeader),&programEdit);

	SetFocusChild(hwndInputCutData);

        return 1;

     case MSG_ERASEBKGND:
        {   
	    hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            		}         
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
            }
            initUserproEditDlgBkgnd(hDlg,hdc,&proRunStatus); 

            if (fGetDC)
                ReleaseDC (hdc);
	return 0;	
	}

    case MSG_TIMER:	
	SetDlgItemText(hDlg,IDC_TIME,convertSysTime(timeBuff));
	//SetWindowText(hwndClock,convertSysTime(timeBuff));
	return 0;   

    case MSG_CHAR:
	ret = LOWORD(wParam);
	if((ret > 0x2b) &&(ret < 0x3e) ){	// 如果是数字键或者小数点安下
	   if(mannualEditFlag == 1){	// 每次内容改变时清空输入框
		SetWindowText(hwndInputCutData,"");	
	 	mannualEditFlag = 0;
	   }
    	}
	break;
    
    case MSG_KEYDOWN:
	hdc=BeginPaint(hDlg);
	switch(LOWORD(wParam)){
	    case SCANCODE_F1:	//标签输入
		if(proRunStatus.runStop > 0){	// Running
		    break;
    		}
		if(proRunStatus.runStatus == 0){	// Input
		    hWnd=GetFocusChild(hDlg);	//只有焦点在输入框时该按键才有效
		    if(hWnd != hwndInputCutData){
		     	break;
		    }
		    if(programEdit.userEditStatus == 0){	 
		    	programEdit.userEditStatus = 1;  // 设置编辑方法为标签 
		    	currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
	    	    	programEdit.currentCutsNum = currentSel;
		    	enterLableEdit(hDlg);
			//printf("enterLableEdit exit...%d \n",programEdit.lableEdit.lableStatus);
                    	if(programEdit.lableEdit.lableStatus == 7){
                    	    // 将计算结果刷新到列表
	            	    UpdateProListWithFileData(hDlg,&programEdit);
		        }
			programEdit.lableEdit.lableStatus == 0;
			ResetUserProgramEdit(hDlg,&programEdit,0);	// 复位输入,不刷新F1文字
		    	SetFocusChild(hwndInputCutData);
		    } 
		    break;
		}
                /*if(proRunStatus.runStatus == 0){	// Input
		    if(programEdit.userEditStatus == 0){	 
		    	funcButtons[0].textpos = 0;	// 退出标签	
		    	programEdit.userEditStatus = 1;  // 设置编辑方法为标签 
		    	currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
	    	    	programEdit.currentCutsNum = currentSel;
		    	enterLableEdit(hDlg);
			printf("enterLableEdit exit...%d \n",programEdit.lableEdit.lableStatus);
                    	if(programEdit.lableEdit.lableStatus == 7){
		       	    programEdit.lableEdit.lableStatus == 0;
	            	    ResetUserProgramEdit(hDlg,&programEdit,1);
                    	    // 将计算结果刷新到列表
	            	    UpdateProListWithFileData(hDlg,&programEdit);
		        }
		    } else if(programEdit.userEditStatus == 1){		// 退出标签
		        funcButtons[0].textpos = 0;	// 标签
		    	programEdit.userEditStatus = 0;
		    	ResetUserProgramEdit(hDlg,&programEdit,0);	// 复位输入,不刷新F1文字
		    	SetFocusChild(hwndInputCutData);
		    }
		    break;
		}*/
		if(proRunStatus.runStatus > 0){	// 自动,手动时为下刀计数清零
		    motorStatus.cutCounter = 0;
   		    refreshCutCounter(hDlg);
		}
		break;

	    case SCANCODE_F2:	//等分输入
		if(proRunStatus.runStop > 0){	// Running
		    break;
    		}
                if(proRunStatus.runStatus > 0)  break;

		if(proRunStatus.runStatus == 0){	// Input
		    hWnd=GetFocusChild(hDlg);	//只有焦点在输入框时该按键才有效
		    if(hWnd != hwndInputCutData){
		     	break;
		    }
		    if(programEdit.userEditStatus == 0){	 
		    	programEdit.userEditStatus = 2;  // 设置编辑方法为标签 
		    	currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
	    	    	programEdit.currentCutsNum = currentSel;
		    	enterAverageEdit(hDlg);
                        if(programEdit.halfEdit.halfStatus == 3){
                    	    // 将计算结果刷新到列表
	            	    UpdateProListWithFileData(hDlg,&programEdit);
		    	}
			programEdit.halfEdit.halfStatus = 0;
			ResetUserProgramEdit(hDlg,&programEdit,0);	// 复位输入,不刷新F1文字
		    	SetFocusChild(hwndInputCutData);
		    } 
		    break;
		}

	 	/*if(programEdit.userEditStatus == 0){
		    funcButtons[1].textpos = 2;	// 退出等分
		    programEdit.userEditStatus = 2;  // 设置编辑方法为等分
		    currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
	    	    programEdit.currentCutsNum = currentSel;
		    enterAverageEdit(hDlg);
                    if(programEdit.halfEdit.halfStatus == 3){
		    	programEdit.halfEdit.halfStatus == 0;
	            	ResetUserProgramEdit(hDlg,&programEdit,1);
                    	// 将计算结果刷新到列表
	            	UpdateProListWithFileData(hDlg,&programEdit);
		    }
		} else if(programEdit.userEditStatus == 2){
		    funcButtons[1].textpos = 2;	// 等分
		    programEdit.userEditStatus = 0;
		    ResetUserProgramEdit(hDlg,&programEdit,0);	// 复位输入,不刷新F2文字
		    SetFocusChild(hwndInputCutData);
		}*/
		break;	

            case SCANCODE_F3:	// 示教
		if(proRunStatus.runStop > 0){	// 运行时都无效
		    break;
    		}
		if(proRunStatus.runStatus > 0) break;
		hWnd=GetFocusChild(hDlg);	//只有焦点在输入框时该按键才有效
		if(hWnd != hwndInputCutData){
		     	break;
		}
		if(programEdit.runStatus == 0){
		    if(programEdit.userEditStatus == 0){
			programEdit.userEditStatus = 7;
			funcButtons[2].textpos = 5;	// 退出示教
			RefreshBoxArea (hdc,&userProgramEditButton,8,395,80,40,1280,0);  // 示教标志

		    }else if(programEdit.userEditStatus == 7){	// 退出示教
			programEdit.userEditStatus = 0;
			funcButtons[2].textpos = 4;	// 退出微调
			ResetUserProgramEdit(hDlg,&programEdit,0);	// 复位输入,不刷新F3文字
			
		    }
		}
		RefreshFunctionButton (hdc,&userProgramEditButton,1,&funcButtons[2],1);// 按键动作

		/*if(!isFunctionButtonValid(&proRunStatus)){		// 如果正在运行,按此键无效
		    break;	
		}
    		//printf("Just...%d\n", programEdit.userEditStatus);
		if(programEdit.userEditStatus == 0){
		    funcButtons[2].textpos = 5;	// 退出微调
		    programEdit.userEditStatus = 6;	// 状态为微调
		    RefreshBoxArea (hdc,&userProgramEditButton,8,395,80,40,1280,0);	// 微调标志
		} else {
		    funcButtons[2].textpos = 4;	// 退出微调
		    programEdit.userEditStatus = 0;
		    ResetUserProgramEdit(hDlg,&programEdit,0);	// 复位输入,不刷新F3文字
		    SetFocusChild(hwndInputCutData);
		}
		RefreshFunctionButton (hdc,&userProgramEditButton,1,&funcButtons[2],1);// 按键
		*/
		break;

            case SCANCODE_F4:	// 输入模式:气垫;手动模式:计算器
		if(proRunStatus.runStop > 0){	// 运行时都无效
		    break;
    		}
		if((proRunStatus.runStatus == 0) && (programEdit.userEditStatus == 0)){
		    hWnd=GetFocusChild(hDlg);	//只有焦点在输入框时该按键才有效
		    if(hWnd != hwndInputCutData){
		     	break;
		    }
		    currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
	    	    programEdit.currentCutsNum = currentSel;
		    curPos = programEdit.userProCuts[currentSel];	// 取出数据
		    changeAirFlagOfData(hDlg,&programEdit);		    
		    break;
		}
		if(proRunStatus.runStatus == 2){	// 手动
		    EnterComputer(hDlg);
		    char *pBuf = computerData.inputBuf;	// 将结果显示到数据输入框
		    int resultLen = strlen(pBuf);
		    SetWindowText(hwndInputCutData,pBuf);	
		    // 设置光标到最后
		    SendMessage(hwndInputCutData,EM_SETCARETPOS,0,resultLen);
		}
		break;

            case SCANCODE_F5:	// 进入设置界面 
		hWnd=GetFocusChild(hDlg);	//只有焦点在输入框时该按键才有效
		if(hWnd != hwndInputCutData){
		     	break;
		}
		if(!isFunctionButtonValid(&proRunStatus)){		// 如果正在运行,按此键无效
		    break;	
		}
		EnterConfigureUser(hDlg);	
		curPos = motorStatus.currentCutPos;
		refreshCurrentPosArea(hDlg,hdc,curPos,1);
		
		break;

	    case SCANCODE_F6:	//保存
		if(!isFunctionButtonValid(&proRunStatus)){		// 如果正在运行,按此键无效
		    break;	
		}
		RefreshFunctionButton (hdc,&userProgramEditButton,1,&funcButtons[5],0);        
		break;

	    case SCANCODE_ADD:		// 微调 加
		/*if((proRunStatus.runStatus == 0)&&(programEdit.userEditStatus == 6)){
		     programEdit.justEdit.action = 1;
		     RefreshBoxArea (hdc,&userProgramEditButton,88,395,40,40,3200,0);// 微调加
		     break;
		}
		if((proRunStatus.runStatus == 2)&&(proRunStatus.runStop == 0)){
		     if(proRunStatus.mannualFlag == 0){
			proRunStatus.mannualFlag = 1;
			RefreshBoxArea (hdc,&userProgramEditButton,88,395,40,40,3200,0);// 手动加
		     }else if(proRunStatus.mannualFlag == 1){
			proRunStatus.mannualFlag = 0;
			ResetUserProgramEdit(hDlg,&programEdit,0); //Clear Add flag
		     }
		     break;
		}
		 // 下面处理第二功能
		if(!isFunctionButtonValid(&proRunStatus)){	
		    break;	
		}
		if( proRunStatus.secondFunc == 1){
		    currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
	    	    programEdit.currentCutsNum = currentSel;
		    curPos = programEdit.userProCuts[currentSel];	// 取出数据
    		    if(curPos & AUTOCUT_FLAG){	// 数据包含自动切刀信息,则删除
		    	delAutoCutFlagFromData(hDlg,&programEdit);
    		    } else {
  		    	addAutoCutFlagToData(hDlg,&programEdit);
		    }
		}*/
		if(proRunStatus.runStop > 0){		// Running
		    break;
		}
		if((proRunStatus.secondFunc == 1)&&(proRunStatus.runStatus == 0)
					&&(programEdit.userEditStatus == 0)){
		    // 改变当前数据切刀标志
		    currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
	    	    programEdit.currentCutsNum = currentSel;
		    curPos = programEdit.userProCuts[currentSel];	// 取出数据
    		    if(curPos & AUTOCUT_FLAG){	// 数据包含自动切刀信息,则删除
		    	delAutoCutFlagFromData(hDlg,&programEdit);
    		    } else {
  		    	addAutoCutFlagToData(hDlg,&programEdit);
		    }
		    break;
		}

		if((proRunStatus.runStatus == 0)&&(programEdit.userEditStatus == 0)){
		     programEdit.userEditStatus = 8;
		     RefreshBoxArea (hdc,&userProgramEditButton,88,395,40,40,3200,0);// 微调加
		     // 最后一刀数据加上输入的数据,结果新增一刀
		}
		if((proRunStatus.runStatus == 2)&&(proRunStatus.runStop == 0)){
		     if(proRunStatus.mannualFlag == 0){
			proRunStatus.mannualFlag = 1;
			RefreshBoxArea (hdc,&userProgramEditButton,88,395,40,40,3200,0);// 手动加
		     }else if(proRunStatus.mannualFlag == 1){
			proRunStatus.mannualFlag = 0;
			ResetUserProgramEdit(hDlg,&programEdit,0); //Clear Add flag
		     }
		     break;
		}
		break;

	     case SCANCODE_SUB:		// 减
		 /*if((proRunStatus.runStatus == 0)&&(programEdit.userEditStatus == 6)){
		     programEdit.justEdit.action = 2;
		     RefreshBoxArea (hdc,&userProgramEditButton,88,395,40,40,3240,0);// 微调减
		     break;
		 }
		if((proRunStatus.runStatus == 2)&&(proRunStatus.runStop == 0)){
		     if(proRunStatus.mannualFlag == 0){
			proRunStatus.mannualFlag = 2;
			RefreshBoxArea (hdc,&userProgramEditButton,88,395,40,40,3240,0);// 手动减
		     }else if(proRunStatus.mannualFlag == 2){
			proRunStatus.mannualFlag = 0;
			ResetUserProgramEdit(hDlg,&programEdit,0); //Clear Add flag
		     }	     
		     break;
		}
		 // 下面处理第二功能
		if(!isFunctionButtonValid(&proRunStatus)){	
		    break;	
		}
		if( proRunStatus.secondFunc == 1){
		    addAutoCutFlagToAllDatas(hDlg,&programEdit);
		}*/

		if(proRunStatus.runStop > 0){		// Running
		    break;
		}
		if((proRunStatus.secondFunc == 1)&&(proRunStatus.runStatus == 0)
					&&(programEdit.userEditStatus == 0)){
		    // 改变所有数据切刀标志
		    autoCutChangeFlag ++;	// counter
		    if(autoCutChangeFlag%2){
			addAutoCutFlagToAllDatas(hDlg,&programEdit);
		    }else{
			delAutoCutFlagFromAllDatas(hDlg,&programEdit);
		    }
		    break;
		}

		if((proRunStatus.runStatus == 0)&&(programEdit.userEditStatus == 0)){
		     programEdit.userEditStatus = 9;
		     RefreshBoxArea (hdc,&userProgramEditButton,88,395,40,40,3240,0);// 微调加
		     // 最后一刀数据加上输入的数据,结果新增一刀
		}

		if((proRunStatus.runStatus == 2)&&(proRunStatus.runStop == 0)){
		     if(proRunStatus.mannualFlag == 0){
			proRunStatus.mannualFlag = 2;
			RefreshBoxArea (hdc,&userProgramEditButton,88,395,40,40,3240,0);// 手动减
		     }else if(proRunStatus.mannualFlag == 2){
			proRunStatus.mannualFlag = 0;
			ResetUserProgramEdit(hDlg,&programEdit,0); //Clear Add flag
		     }	     
		     break;
		}
 	 	break;


 	    case SCANCODE_MODIFY:	//编辑已有数据,F7
		if(!isFunctionButtonValid(&proRunStatus)){		// 如果正在运行,按此键无效
		    break;	
		}
		if(proRunStatus.runStatus > 0){
		    break;
 		}
		if(programEdit.userEditStatus == 0){	 	
		    //RefreshBoxArea (hdc,&userProgramEditButton,8,395,80,40,2080,0);	// 修改
		    programEdit.userEditStatus = 3;  // 设置编辑方法为修改
		    currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
	    	    programEdit.currentCutsNum = currentSel;
			
		    modifyOneCutDataNew(hDlg,&programEdit);	// Modify
 		    ResetUserProgramEdit(hDlg,&programEdit,0);	// 复位输入,不刷新F1文字
		    SetFocusChild(hwndInputCutData);
		} 
                /*if(programEdit.userEditStatus == 0){	 	
		    //RefreshBoxArea (hdc,&userProgramEditButton,8,395,80,40,2080,0);	// 修改
		    programEdit.userEditStatus = 3;  // 设置编辑方法为修改
		    currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
	    	    programEdit.currentCutsNum = currentSel;
		} else if(programEdit.userEditStatus = 3){	// 退出修改状态
		    programEdit.userEditStatus = 0;
		    ResetUserProgramEdit(hDlg,&programEdit,0);	// 复位输入,不刷新F1文字
		    SetFocusChild(hwndInputCutData);
		}*/	
		break;
	
 	    case SCANCODE_INSERTDATA:		//插入数据,Insert
		if(!isFunctionButtonValid(&proRunStatus)){		// 如果正在运行,按此键无效
		    break;	
		}
		if(proRunStatus.runStatus > 0){
		    break;
 		}
		hWnd=GetFocusChild(hDlg);		//只有焦点在输入框时该按键才有效
		if(hWnd != hwndInputCutData){
		     SetFocusChild(hwndInputCutData);
		     break;
		}
		// 得到当前列表框选项
		currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
	    	programEdit.currentCutsNum = currentSel;
		if(programEdit.userEditStatus == 0){
		    programEdit.userEditStatus = 4;  // 设置编辑方法为插入
		    // 该键同时作为输入法切换
		    SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);	
		    // 设定currentCut,等待插入
		    //PrepareToInsertOneCutData(hDlg,&programEdit);
		    insertOneCutDataNew(hDlg,&programEdit);
		    ResetUserProgramEdit(hDlg,&programEdit,0);	// 复位输入,不刷新F1文字
		    SetFocusChild(hwndInputCutData); 
		    SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);	
		}
		
		/*if(programEdit.userEditStatus == 0){
		    RefreshBoxArea (hdc,&userProgramEditButton,8,395,80,40,2240,0);	// 插入
		    programEdit.userEditStatus = 4;  // 设置编辑方法为插入

		    // 该键同时作为输入法切换
		    SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);	
		    // 设定currentCut,等待插入
		    PrepareToInsertOneCutData(hDlg,&programEdit);

		} else if(programEdit.userEditStatus == 4){
		     programEdit.userEditStatus = 0;
		     ResetUserProgramEdit(hDlg,&programEdit,0);	// Clear Flag
		     SendMessage(hwndProCutList,LB_DELETESTRING,currentSel,0);
		     SendMessage(hwndProCutList,LB_SETCURSEL,currentSel,0);

		     // 该键同时作为输入法切换
		     SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);	
		}
		*/
		break;
		
	     case SCANCODE_DELETE:	//删除已有数据
		if(!isFunctionButtonValid(&proRunStatus)){		// 如果正在运行,按此键无效
		    break;	
		}
		
		if( proRunStatus.secondFunc == 1){	// 第二功能按下,全部删除
		    releaseSecondFunction(hDlg); 
		    currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
		    programEdit.currentCutsNum = currentSel;
		    //deleteAllCutDatas(hDlg,&programEdit); 
		    deleteBehindCutDatas(hDlg,&programEdit); 
		    
		} else {
		    programEdit.userEditStatus = 5;  // 设置编辑方法为删除
		    // 得到当前列表框选项
		    currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
		    int totalCount = SendMessage(hwndProCutList,LB_GETCOUNT,0,0); // 列表条目数
	    	    programEdit.currentCutsNum = currentSel;
		    // 删除该数据
		    if(currentSel < (totalCount-1)){	// 正常删除
		    	deleteOneCutData(hDlg,&programEdit);
		    } else if(currentSel == (totalCount-1)){  //  试图删除最后一刀0.0
		    	SendMessage(hwndProCutList,LB_SETCURSEL,0,0);	 // 高亮输入项
			programEdit.userEditStatus = 0;
		    }
		}
		break;

	    case SCANCODE_PUSHER:	// 增加推纸标志  
		if(!isFunctionButtonValid(&proRunStatus)){		// 如果正在运行,按此键无效
		    break;	
		}
		// proRunStatus.pusher = 1;	// 推纸完后切换回 0  
	     	// refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,2,0);
		currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0);
	    	programEdit.currentCutsNum = currentSel;
		curPos = programEdit.userProCuts[currentSel];	// 取出数据
    		if(curPos & PUSHER_FLAG){	// 数据包含推纸信息,则删除推纸
		    //printf("delPusherFlagFromData.\n");
		    delPusherFlagFromData(hDlg,&programEdit);
    		} else {
		    //printf("addPusherFlagToData.\n");
  		    addPusherFlagToData(hDlg,&programEdit);
		}		

		break;

	    case SCANCODE_MANNUAL:	//  停止
		if(!isFunctionButtonValid(&proRunStatus)){		// 如果正在运行,按此键无效
		    break;	
		}
		// Do nothing
		break;

	    case SCANCODE_PAGEUP :	// 当当前焦点在输入框时可以移动列表框选项
		if((proRunStatus.runStatus==1)||(proRunStatus.runStatus==3)||(isIMEOpenInMotor==1)){	
					// 自动模式以及输入法打开时,按此键无效
		    break;	
		}
		hWnd=GetFocusChild(hDlg);		// 在KEYUP消息中给回焦点到输入框
		if(hWnd == hwndInputCutData){
		    SetFocusChild(hwndProCutList);
		}
		break;

	    case SCANCODE_PAGEDOWN :		// 自动模式,按此键无效
		if((proRunStatus.runStatus==1)||(proRunStatus.runStatus==3)||(isIMEOpenInMotor==1)){	
		    break;	
		}
		hWnd=GetFocusChild(hDlg);		// 得到当前焦点,移动后给回焦点
		if(hWnd == hwndInputCutData){
		    SetFocusChild(hwndProCutList);
		}
		break; 
	    case SCANCODE_CURSORBLOCKUP :	// 当当前焦点在输入框时可以移动列表框选项
		if((proRunStatus.runStatus == 1) || proRunStatus.runStatus == 3){	
								// 自动模式,按此键无效
		    break;	
		}
		hWnd=GetFocusChild(hDlg);		// 在KEYUP消息中给回焦点到输入框
		if(hWnd == hwndInputCutData){
		    SetFocusChild(hwndProCutList);
		}
		break;

	    case SCANCODE_CURSORBLOCKDOWN :		// 
		if((proRunStatus.runStatus == 1) || proRunStatus.runStatus == 3){	
								// 自动模式,按此键无效
		    break;	
		}
		hWnd=GetFocusChild(hDlg);		// 得到当前焦点,移动后给回焦点
		if(hWnd == hwndInputCutData){
		    SetFocusChild(hwndProCutList);
		}
		break; 

	    case SCANCODE_PROG:		// 程序键,返回程序界面
		//if(!isFunctionButtonValid(&proRunStatus)){		// 如果正在运行,按此键无效
		//    break;	
		//}
		if(proRunStatus.secondFunc == 1){	// 第二功能为输入程序名
		    SetFocusChild(hwndProName);
		    releaseSecondFunction(hDlg);
		    break;
		}
		// 退出我的程序,发送退出消息,等待退出后调用程序
		userProExitFlag = 0;
		SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);	
		break;

	    case SCANCODE_FORWARD:		//  切刀前进, 发送前进命令
		frontlimit = getValidLimit();
		if(motorStatus.currentCutPos > frontlimit){
		    motorRunStep(MOTOR_FORWARD,0);
		}
		break;

	    case SCANCODE_BACKWARD:	// 后退  
		if(motorStatus.currentCutPos < currentuser->backlimit){ 
		    motorRunStep(MOTOR_BACKWARD,0);	
		}
		break;

	    case SCANCODE_FUNCTION:	// 第二功能  lest-shift
	     	if(proRunStatus.secondFunc == 0){
		    proRunStatus.secondFunc = 1;	// 使用第二功能
		} else {
		    proRunStatus.secondFunc = 0;	
		}  
		refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,3,0);
		break;

	    case SCANCODE_AUTORUN:	// 输入模式 
		if(proRunStatus.runStop > 0) break;
		if(proRunStatus.runStatus == 0){
		    break;
		}else{
		    proRunStatus.runStatus = 0;
		    addLastListItem(hwndProCutList,&programEdit,lastListItem);
		    programEdit.currentCutsNum = 0;	//  高亮第一刀
		    SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
		    resetMotorRunStatus(&proRunStatus);
		    InvalidateRect(hDlg,NULL,TRUE);	// 引发窗口重绘
		    // 采取响应的动作 
	     	    refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);    
		}
		
		
		//if(proRunStatus.runStop){		// 如果正在运行,按此键无效
		//    break;			// 暂时屏蔽
		//}
		/*if(proRunStatus.runStatus < 3){		// 状态循环切换
		    proRunStatus.runStatus ++;	// 非输入模式,删除列表最后一项 0
	    	    delectLastListItem(hwndProCutList,&programEdit,lastListItem);
		} else {
		    proRunStatus.runStatus = 0;	// 添加最后一项0
		    addLastListItem(hwndProCutList,&programEdit,lastListItem);	
		} 
		
		programEdit.currentCutsNum = 0;	//  高亮第一刀
		SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
		resetMotorRunStatus(&proRunStatus);
		InvalidateRect(hDlg,NULL,TRUE);	// 引发窗口重绘
		// 采取响应的动作 
	     	refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);
		*/
		break;
	
            case SCANCODE_MUL:	// 手动
		if(proRunStatus.runStop > 0) break;
		if(proRunStatus.runStatus == 2){
		    break;
		}
		if (proRunStatus.runStatus == 0){
		    proRunStatus.runStatus = 2;
		    delectLastListItem(hwndProCutList,&programEdit,lastListItem);
		    programEdit.currentCutsNum = 0;	//  高亮第一刀
		    SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
		    resetMotorRunStatus(&proRunStatus);
		    InvalidateRect(hDlg,NULL,TRUE);	// 引发窗口重绘
		    // 采取响应的动作 
	     	    refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);    
		}
		if (proRunStatus.runStatus == 1){
		    proRunStatus.runStatus = 2;
		    programEdit.currentCutsNum = 0;	//  高亮第一刀
		    SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
		    resetMotorRunStatus(&proRunStatus);
		    InvalidateRect(hDlg,NULL,TRUE);	// 引发窗口重绘
		    // 采取响应的动作 
	     	    refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);    
		}
		break;
	
            case SCANCODE_DIV:		// 自动
		if(proRunStatus.runStop > 0) break;
		if(proRunStatus.runStatus == 1){
		    break;
		}
		if (proRunStatus.runStatus == 0){
		    proRunStatus.runStatus = 1;
		    delectLastListItem(hwndProCutList,&programEdit,lastListItem);
		    programEdit.currentCutsNum = 0;	//  高亮第一刀
		    SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
		    resetMotorRunStatus(&proRunStatus);
		    InvalidateRect(hDlg,NULL,TRUE);	// 引发窗口重绘
		    // 采取响应的动作 
	     	    refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);    
		}
		if (proRunStatus.runStatus == 2){
		    proRunStatus.runStatus = 1;
		    programEdit.currentCutsNum = 0;	//  高亮第一刀
		    SendMessage(hwndProCutList,LB_SETCURSEL,0,0);
		    resetMotorRunStatus(&proRunStatus);
		    InvalidateRect(hDlg,NULL,TRUE);	// 引发窗口重绘
		    // 采取响应的动作 
	     	    refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,0,1);    
		}
		break;

	    case SCANCODE_RUN:			// 运行 
		if(proRunStatus.runStatus == 0){	// 输入模式无效
		    break;
		} 
		if(proRunStatus.runStatus == 2){
		   RefreshButtonArea(hdc,&buttonsOkEsc,MSG_KEYDOWN,ENTER_X,ENTER_Y,40,40,3); 
		}
		break;

	   case SCANCODE_ENTER:	//确定
		if(proRunStatus.runStatus>0){
		   break;
		}
		RefreshButtonArea (hdc,&buttonsOkEsc,MSG_KEYDOWN,ENTER_X,ENTER_Y,48,48,0);
		break;

	   case SCANCODE_CANCEL:	//取消,END键
		if((proRunStatus.runStatus == 1) || (proRunStatus.runStatus == 3)){	
		    break;
		} 
		RefreshButtonArea (hdc,&buttonsOkEsc,MSG_KEYDOWN,CANCEL_X,CANCEL_Y,40,40, 1);
		break;

           case SCANCODE_HELP:	//帮助
		//显示帮助信息对话框
		hWnd=GetFocusChild(hDlg);	//焦点在不同的输入框时有不同的提示
		if(hWnd == hwndProName){
		     MessageBox(hDlg,userpro_help[*curSysLang].help_userpro_name,
		   	userpro_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		}else if(hWnd == hwndInputCutData){
		     MessageBox(hDlg,userpro_help[*curSysLang].help_userpro_cuts,
		   	userpro_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		}
		break;

	}
	EndPaint(hDlg,hdc);
        break;
    case MSG_KEYUP:
	hdc=BeginPaint(hDlg);
	switch(LOWORD(wParam)){
	    case SCANCODE_F1:	//标签输入
		if(!isFunctionButtonValid(&proRunStatus)){		// 如果正在运行,按此键无效
		    break;	
		}	 
		RefreshFunctionButton (hdc,&userProgramEditButton,0,&funcButtons[0],0);	// 按键动作
		break;

	    case SCANCODE_F2:	//等分输入
		if(!isFunctionButtonValid(&proRunStatus)){		// 如果正在运行,按此键无效
		    break;	
		}
		RefreshFunctionButton (hdc,&userProgramEditButton,0,&funcButtons[1],0);	// 按键动作
	
		break;	
	    case SCANCODE_F3:	//微调
		/*if(!isFunctionButtonValid(&proRunStatus)){		// 如果正在运行,按此键无效
		    break;	
		}*/	
		if(proRunStatus.runStatus > 0) break; 
		RefreshFunctionButton (hdc,&userProgramEditButton,0,&funcButtons[2],0);	// 按键动作
		
		break;

           case SCANCODE_F4:	//退出标签,等分
		/*if(!isFunctionButtonValid(&proRunStatus)){		// 如果正在运行,按此键无效
		    break;	
		}*/
		break;

	    case SCANCODE_F6:	//保存
		if(!isFunctionButtonValid(&proRunStatus)){		// 如果正在运行,按此键无效
		    break;	
		}
		hWnd=GetFocusChild(hDlg);	//只有焦点在输入框时该按键才有效
		if(hWnd != hwndInputCutData){
		     	break;
		}
		RefreshFunctionButton (hdc,&userProgramEditButton,0,&funcButtons[5],0);	// 按键动作
		printf("Save:%s\n",programCreate.filePathname);
		UpdateFileHeader(&programCreate,&programEdit,0);	// 保存
		saveUserProgramToFile(programCreate.filePathname,&(programCreate.proHeader),&programEdit);
		MessageBox(hDlg,userpro_help[*curSysLang].msg_save_pro_ok,
		  	userpro_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		SetFocusChild(hwndInputCutData);
		break;

	    case SCANCODE_CURSORBLOCKUP :	//
		if((proRunStatus.runStatus == 1) || proRunStatus.runStatus == 3){	
								// 自动模式,按此键无效
		    break;	
		}
		SetFocusChild(hwndInputCutData);	// 列表框上移之后光标给回输入框

		break;
	    case SCANCODE_CURSORBLOCKDOWN :	//
		if((proRunStatus.runStatus == 1) || proRunStatus.runStatus == 3){	
								// 自动模式,按此键无效
		    break;	
		}
		SetFocusChild(hwndInputCutData);	// 列表框下移之后光标给回输入框
		break; 
	    case SCANCODE_PAGEUP :	//
		if((proRunStatus.runStatus==1)||(proRunStatus.runStatus==3)||(isIMEOpenInMotor==1)){	
					// 自动模式,按此键无效
		    break;	
		}
		SetFocusChild(hwndInputCutData);	// 列表框上移之后光标给回输入框

		break;
	    case SCANCODE_PAGEDOWN :	
		if((proRunStatus.runStatus==1)||(proRunStatus.runStatus==3)||(isIMEOpenInMotor==1)){	
		    break;	
		}
		SetFocusChild(hwndInputCutData);	// 列表框下移之后光标给回输入框
		break; 
	    case SCANCODE_RUN:			// 运行 

		// 输入模式,或者马达正在运行时无效
		/*if((proRunStatus.runStatus == 0) || (proRunStatus.machineStop == 0)){	
		    break;
		} */ 
                // 2007-07-30
		if(proRunStatus.machineStop == 0){	// Running
		    break;
		}
		// 示教编辑模式下允许按运行键;
		if((proRunStatus.runStatus == 0) && (programEdit.userEditStatus != 7)){	
		    break;
		}
		/*if(proRunStatus.secondFunc == 1){
		    proRunStatus.runStop = 0;	// 停止,实际中不允许这样停止
		    refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,1,0);
		    releaseSecondFunction(hDlg);		// 刷新第二功能标志
		    if(proRunStatus.runStatus == 2){
		    	RefreshButtonArea(hdc,&buttonsOkEsc,MSG_KEYUP,ENTER_X,ENTER_Y,40,40,3);
		    }
		    break;
		}*/
		//printf("Run status =%d %d \n",proRunStatus.runStatus,programEdit.nInputCutsNum);
		if(proRunStatus.runStatus == 1){	
		    if(programEdit.nInputCutsNum == 1){	
			break;	// 此时没有输入数据
		    }else{	
			UpdateFileHeader(&programCreate,&programEdit,0);	// 保存
			saveUserProgramToFile(programCreate.filePathname,
					&(programCreate.proHeader),&programEdit);
		    }	
		} 

		
		if((proRunStatus.runStatus == 2) &&(proRunStatus.runStop == 0)){	// 界面处理
		    proRunStatus.runStop = 1;	// 运行
		} 
		if(proRunStatus.runStatus == 2){	// 处理手动运行时的编辑标志
		    mannualEditFlag = 1;
		    if(proRunStatus.mannualFlag){	// 加或减
			ResetUserProgramEdit(hDlg,&programEdit,0);	
		    }
		}  		    
		// InvalidateRect(hDlg,NULL,TRUE);	// 引发窗口重绘
		currentSel = SendMessage(hwndProCutList,LB_GETCURSEL,0,0); // Remark it here
		SendMessage(hwndProCutList,LB_SETCURSEL,currentSel,0);
	    	programEdit.currentCutsNum = currentSel;
		proRunStatus.destCutNum = currentSel;
	     	refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,1,0);
		if(proRunStatus.runStatus == 2){
		    RefreshButtonArea(hdc,&buttonsOkEsc,MSG_KEYUP,ENTER_X,ENTER_Y,40,40,3);
		}
		proRunStatus.cutActTimes = 0;
		proRunStatus.pusherActTimes = 0;
		proRunStatus.cutPusherActTimes = 0;		
		proRunStatus.isOneCutDone = 1;	// 模拟上一次已经完成
		proRunStatus.machineStop = 0;
		//printf("motorRunProcess %d \n",currentSel);
		motorRunProcess(hDlg,&programEdit,&proRunStatus);	// 运行程序
		break;

            case SCANCODE_ENTER:	//确定
		if(proRunStatus.runStatus>0){	// 非输入状态下无效
		   break;
		}
		RefreshButtonArea (hdc,&buttonsOkEsc,MSG_KEYUP,ENTER_X,ENTER_Y,40,40, 0);
		break;

	    case SCANCODE_CANCEL:		//取消,清空数据输入缓从区
		if((proRunStatus.runStatus == 1) || (proRunStatus.runStatus == 3)){	
		    break;
		} 
		RefreshButtonArea (hdc,&buttonsOkEsc,MSG_KEYUP,CANCEL_X,CANCEL_Y,40,40, 1);
		hWnd=GetFocusChild(hDlg);	//焦点在数据输入框时有效
		if(hWnd == hwndInputCutData){
		      SetFocusChild(hwndInputCutData);
		      SetWindowText(hWnd,"");	// clear   
		}
		break;
	}
	EndPaint(hDlg,hdc);
	break;

    case MSG_RECV:			// 接收到下位机发送的数据
	recvCommand(cmdRecvBuf);		// 复制到用户缓冲区
	// For test
	//strcpy(cmdRecvBuf,testbuffer);
	//ret = isCmdValid(cmdRecvBuf);	// 测试,不用校验
     	//if(ret){
	    	memset(&cmdData,0,sizeof(cmdData));
	    	getCmdFrame(&cmdData,cmdRecvBuf);	// 获得数据
	    	if(cmdData.flag == 'G'){			// 正常数据帧
		    hdc=BeginPaint(hDlg);	
		    if(unit){
  			cmdData.data = mm_to_inch(cmdData.data);
		    }	
		    if( motorStatus.currentCutPos != cmdData.data){	// 不同时才刷新
			motorStatus.currentCutPos = cmdData.data;	// 更新数据
			refreshCurrentPosArea(hDlg,hdc,cmdData.data,0);
			//printf("Refresh forCutLen...\n");
			refreshForCutLenArea(hwndCurrentPos,&proRunStatus,cmdData.data);	// 刀前尺寸
		    }
	    	    if(motorStatus.devStatus != cmdData.status){
			motorStatus.devStatus = cmdData.status;	// 更新状态,低四位有效 
			ret = getDevStatus(&proRunStatus,cmdData.status);	// 读出机器状态
			refreshCurrentPos(hwndCurrentPos,proRunStatus.forCutLen);	// 刀前尺寸
			// 刷新切刀和推纸器状态
		    	refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,4,0);
			// 刷新运行/停止
		    	refreshUserProgramRunStatus(hDlg,hdc,&proRunStatus,1,0);
			if(ret){
			    motorStatus.cutCounter++;
			    refreshCutCounter(hDlg);
			    if((proRunStatus.runStatus == 0)&&(programEdit.userEditStatus == 7)){
				// 示教
				editOneCutData(hDlg,&programEdit,motorStatus.currentCutPos);
				resetMotorRunStatus(&proRunStatus);
			    }	
			    if(proRunStatus.runStatus == 2){	// 手动 
				resetMotorRunStatus(&proRunStatus);
			    }
			}

		    }
		    EndPaint(hDlg,hdc);
		    // 处于自动运行状态,则继续运行
		    if(proRunStatus.runStatus == 1){	
			//printf("Autorun,go...\n");
			motorRunAuto(hDlg,&programEdit,&proRunStatus);
			//motorRunProcess(hDlg,&programEdit,&proRunStatus);	
		    }
		}
	//}
	break;

    case MSG_COMMAND:
	switch(wParam){
	    case IDCANCEL:
		KillTimer(hDlg,IDC_TIMER);
	    	EndDialog (hDlg, wParam);
		break;
	}
	break;
        
    }
   

    return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void InitUserProDialogBox (HWND hWnd)
{
    UserProgromDlgInitProgress.controls = CtrlInitProgress;
    
    DialogBoxIndirectParam (&UserProgromDlgInitProgress, hWnd, InitUserProDialogBoxProc, 0L);
}

int EnterUserProgram(void)
{	

    // Load background and other pics
    //if (LoadBitmap (HDC_SCREEN, &userProgramEditBkgnd, "./res/userProgramRun0.jpg"))
    //     return 1; 
    int flashFlag,bkgndFlag;
    loadInitConfigFile(&flashFlag,&bkgndFlag);	// 读入配置文件
    //loadUserBkgndScale(&userProgramEditBkgnd,bkgndFlag);	// 加载主背景

    if(loadUserProButtonIcon())	// 根据语言加载图标
	return(1);
    if (LoadBitmap (HDC_SCREEN, &cutIconBkgnd, "./res/cuticon.jpg"))
         return 1; 
    if (LoadBitmap (HDC_SCREEN, &largeNumsIcon, "./res/0_9.bmp"))
         return 1;   
     if (LoadBitmap (HDC_SCREEN, &lableEditBkgnd, "./res/configItemBkgnd.jpg"))//标签等分背景
         return 1; 

    InitUserProDialogBox (HWND_DESKTOP);		// Should not return except receive CLOSE MSG
    
    printf("EnterUserProgram()...UnloadBitmap...\n");
    UnloadBitmap (&userProgramEditBkgnd);	// unload background
    UnloadBitmap (&userProgramEditButton);	// unload background
    UnloadBitmap (&buttonsOkEsc);	// unload background
    UnloadBitmap (&cutIconBkgnd);	// unload background
    UnloadBitmap (&largeNumsIcon);	// unload background

    if(!userProExitFlag){	// 返回程序列表界面
    	EnterProListDialog();	// 在这里调用上层程序列表
    }
    return 0;
}

