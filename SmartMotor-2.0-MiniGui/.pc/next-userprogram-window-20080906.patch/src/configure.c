/*  
** $Id: configure.c, zxw$
**	修改机器配置
**
**
** 2005-12-15 Second Version,writen by Zeng Xianwei
** Rewrite most of code to open a new dialog for all of 
** the setup menu: SetCutPos,SetSysTime,SetSysDate,Limits
** and language
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <errno.h>
#include <time.h>

#include <unistd.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/select.h>

#include <minigui/common.h>
#include <minigui/minigui.h>
#include <minigui/gdi.h>
#include <minigui/window.h>
#include <minigui/control.h>

#include "motor.h"
#include "bkgndEdit.h"
#include "configure.h"
#include "motorregister.h"		// For 用户配置结构体
#include "userprogram.h"
#include "motormain.h"
#include "help.h"

static BITMAP configureBkgnd;
static BITMAP configureListBkgnd;
static BITMAP configurebuttonBkgnd;

static BITMAP *pSetUserBkgnds;	// tmp
static BITMAP setUserBkgnds;		// tmp
extern BITMAP userProgramEditBkgnd;	// 背景图片,定义为全局

static int flashFlag = 0;
static int bkgndFlag = 0;

#define CommonConfigItem	5
#define UserConfigItem	6	// 6 - 5 no speed	

#define ConfigureStartX	80
#define ConfigureStartY	90

#define CommonConfigStartX		80
#define CommonConfigStartY		120


#define UserBkgndScaleX 		130
#define UserBkgndScaleY		40
#define UserBkgndScaleW 		160
#define UserBkgndScaleH		120

#define UserBkgndNumber	2	// 2种选择
#define RunSpeedNumber	8	// 8种速度

static int currentConfig;	//  0 SetCutPos 1 SetFrontLimit ...

static struct comCmdData_t cmdData;

struct config_help_t{
	unsigned char * caption_help;
	unsigned char * caption_waring;
	unsigned char * caption_bkgnd;
	unsigned char * msg_cutdata_overflow;
	unsigned char * msg_step_overflow;
	unsigned char * msg_config_ok;
	unsigned char * help_config;
	unsigned char * help_date;
	unsigned char * help_time;
	unsigned char * help_lang;
	unsigned char * help_flash;
	unsigned char * help_cutpos;
	unsigned char * help_limit;
	unsigned char * help_step;
	unsigned char * help_bkgnd;
};

static struct config_help_t config_help[SYSLANG_NO] = 
{
    {	caption_help_cn,
	caption_waring_cn,
	caption_bkgnd_cn,
	msg_cutdata_overflow_cn,
	msg_step_overflow_cn,
	msg_config_ok_cn,
	help_config_cn,
	help_date_cn,
	help_time_cn,
	help_lang_cn,
	help_flash_cn,
	help_cutpos_cn,
	help_limit_cn,
	help_step_cn,
	help_bkgnd_cn
   },

   {	caption_help_en,
	caption_waring_en,
	caption_bkgnd_cn,
	msg_cutdata_overflow_en,
	msg_step_overflow_en,
	msg_config_ok_en,
	help_config_en,
	help_date_en,
	help_time_en,
	help_lang_en,
	help_flash_en,
	help_cutpos_en,
	help_limit_en,
	help_step_en,
	help_bkgnd_en
   }
};

static int userConfigList[UserConfigItem] = {0,1,2,3,4,7}; // 0,1,2,3,4,7
/*
 *	SetCutPos
 *	SetFrontLimit
 *	SetMiddleLimit
 *	SetBackLimit
 *	SetRunSpeed
 *	Back(exit)
 */
static int commonConfigList[CommonConfigItem] = {0,1,2,3,4};
/*
 *	SetSysDate
 *	SetSysTime
 *	SetInitFlash
 *	SetSysLanguage
 *	Back(exit)
 */

/* PCF8563 时间存取函数 */
#define RTC_DEVICE	"/dev/rtc"


/* RTC ioctl Macro cmds */
#define RTC_GETDATETIME	0
#define RTC_SETDATE	1
#define RTC_SETTIME	2
#define RTC_SETDATETIME	3
#define RTC_GETCTRL	4
#define RTC_SETCTRL	5

/* get system time from RTC chip */
int getSysRtcTime(struct rtc_tm *tm)
{
    int fd,ret;
    //printf("Open RTC devices.\n");
    fd=open(RTC_DEVICE,O_RDONLY);
    if(fd < 0){
     	perror("error:can not open rtc devicen");
     	return(-1);
    }
    ret = ioctl(fd,RTC_GETDATETIME,tm);
    if(ret < 0){
	perror("Get time error.\n");
	close(fd);
	return -1;
    }
  
    close(fd);
    return(0);
}


/* 时间显示转换函数 */
char * convertSysTime(char *dispBuf)
{
#ifdef CONFIG_DEBUG_ON_PC
    sprintf(dispBuf,"22:12:50 2006-06-15");
    return(dispBuf);
#endif
    struct rtc_tm tm;
    getSysRtcTime(&tm); 

    sprintf(dispBuf,"%02d:%02d:%02d %04d-%02d-%02d",tm.tm_hour,tm.tm_min,tm.tm_sec,
						tm.tm_year,tm.tm_mon+1,tm.tm_mday);
    return(dispBuf);
}

/*  时间设置函数 
 *  flag = 0 time   1 date
 */
static int setSysRtcDateTime(struct rtc_tm *tm,int flag)
{
    int fd,ret;
    //printf("Open RTC devices.\n");
    fd=open(RTC_DEVICE,O_RDWR);
    if(fd < 0){
     	perror("error:can not open rtc device: %s\n");
     	return(-1);
    }

    ret = ioctl(fd,flag,tm);
    if(ret < 0){
	perror("Set system datetime error.\n");
	close(fd);
	return -1;
    }    

    close(fd);
    return 0;
}



/**********************用户主设置对话框*******************************/
static DLGTEMPLATE configureDlgBox =
{
    WS_BORDER ,
    WS_EX_NONE,
    0, 0, 640, 480,
    "configure",
    0, 0,
    6, NULL,
    0
};

static CTRLDATA configureCtrlsInit[] =
{   
    {
        CTRL_STATIC,
        WS_VISIBLE | ES_READONLY ,
        455, 10, 175, 28,
        IDC_TIME,
        NULL,
        0,
	WS_EX_TRANSPARENT,
    },     
    {
        CTRL_SLEDIT,
        WS_VISIBLE | WS_BORDER | ES_READONLY,
        185, 90, 80, 32, 
        IDC_CURPOS,
        NULL,
        0
    },
    {
        CTRL_SLEDIT,
        WS_VISIBLE | WS_BORDER | ES_READONLY,
        185, 124, 80, 32, 
        IDC_LIMIT_F,
        NULL,
        0
    }, 
    {
        CTRL_SLEDIT,
        WS_VISIBLE | WS_BORDER | ES_READONLY,
        185, 158, 80, 32, 
        IDC_LIMIT_M,
        NULL,
        0
    },
    {
        CTRL_SLEDIT,
        WS_VISIBLE | WS_BORDER | ES_READONLY,
        185, 192, 80, 32, 
        IDC_LIMIT_B,
        NULL,
        0
    },
    {
        CTRL_SLEDIT,
        WS_VISIBLE | WS_BORDER | ES_READONLY,
        185, 226, 80, 32, 
        IDC_RUN_SPEED,
        NULL,
        0
    }

};
/**********************通用设置对话框*******************************/
static DLGTEMPLATE commonConfigureDlgBox =
{
    WS_BORDER ,
    WS_EX_NONE,
    0, 0, 640, 480,
    "commonSet",
    0, 0,
    1, NULL,
    0
};

static CTRLDATA commonConfigureCtrlsInit[] =
{   
    {
        CTRL_STATIC,
        WS_VISIBLE | ES_READONLY ,
        455, 10, 175, 28,
        IDC_TIME,
        NULL,
        0,
	WS_EX_TRANSPARENT,
    }	
};
/****************** 推纸器位置设定  ********************************/

// 位置设定对话框模板
static DLGTEMPLATE setCutPosDlgBox =	
{
    WS_BORDER ,
    WS_EX_NONE,
    200, 125, 300, 150,
    "setCutPos",
    0, 0,
    2, NULL,
    0
};

static CTRLDATA setCutPosCtrlsInit[] =
{   
    {
        CTRL_SLEDIT,
        WS_VISIBLE | SS_SIMPLE|WS_BORDER,
        120, 15, 150, 30, 
        IDC_CUT_POS,
        "",
        0
    },
    {
        CTRL_SLEDIT,
        WS_VISIBLE | SS_SIMPLE|WS_BORDER|ES_READONLY,
        120, 60, 150, 30, 
        IDC_CURRENT_POS,
        "",
        0,
	WS_EX_TRANSPARENT
    }	
};


/****************** 极限值设置  ********************************/
/*  极限设定共用一个模板
 */
// 位置和极限设定对话框模板
static DLGTEMPLATE setPosLimitDlgBox =	
{
    WS_BORDER ,
    WS_EX_NONE,
    200, 150, 300, 100,
    "setPosLimit",
    0, 0,
    1, NULL,
    0
};

static CTRLDATA setPosLimitCtrlsInit[] =
{   
    {
        CTRL_SLEDIT,
        WS_VISIBLE | SS_SIMPLE|WS_BORDER,
        120, 15, 150, 32, 
        IDC_POS_LIMIT,
        "",
        0
    }
	
};


/******************  系统日期设置  ********************************/
/*  日期时间设置共用一个模板 
 */
static DLGTEMPLATE setSysDateDlgBox =
{
    WS_BORDER ,
    WS_EX_NONE,
    200, 150, 300, 100,
    "setSysDate",
    0, 0,
    3, NULL,
    0
};

static CTRLDATA setSysDateCtrlsInit[] =
{   
    {
        CTRL_SLEDIT,
        WS_VISIBLE | SS_SIMPLE,
        50, 17, 50, 28, 
        IDC_YEAR,
        "",
        0,
	WS_EX_TRANSPARENT,
    },
    {
        CTRL_SLEDIT,
        WS_VISIBLE | SS_SIMPLE,
        130, 17, 26, 28, 
        IDC_MONTH,
        "",
        0,
	WS_EX_TRANSPARENT,
    },
    {
        CTRL_SLEDIT,
        WS_VISIBLE | SS_SIMPLE,
        180, 17, 36, 28, 
        IDC_DAY,
        "",
        0,
	WS_EX_TRANSPARENT,
    },
	
};
/******************  系统时间设置  ********************************/

static DLGTEMPLATE setSysTimeDlgBox =
{
    WS_BORDER ,
    WS_EX_NONE,
    200, 150, 300, 100,
    "setSysTime",
    0, 0,
    2, NULL,
    0
};

// 提示背景坐标(40,15)
static CTRLDATA setSysTimeCtrlsInit[] =
{   
    {
        CTRL_SLEDIT,
        WS_VISIBLE | SS_SIMPLE,
        100, 17, 30, 28, 
        IDC_HOUR,
        "",
        0,
	WS_EX_TRANSPARENT,
    },
    {
        CTRL_SLEDIT,
        WS_VISIBLE | SS_SIMPLE,
        145, 17, 30, 28, 
        IDC_MINUTE,
        "",
        0,
	WS_EX_TRANSPARENT,
    }	
};

/******************  系统语言设置  ********************************/
static DLGTEMPLATE setSysLangDlgBox =
{
    WS_BORDER ,
    WS_EX_NONE,
    200, 150, 300, 100,
    "setSysLang",
    0, 0,
    1, NULL,
    0
};

static CTRLDATA setSysLangCtrlsInit[] =
{   
 {
        CTRL_STATIC,
        WS_VISIBLE | ES_READONLY ,
        400, 200, 10, 10,
        298,
        NULL,
        0,
	WS_EX_TRANSPARENT,
    },
	
};

/******************  用户背景设置  ********************************/
static DLGTEMPLATE setUserBkgndDlgBox =
{
    WS_BORDER ,
    WS_EX_NONE,
    160, 120, 320, 240,
    "setUserBkgnd",
    0, 0,
    1, NULL,
    0
};

static CTRLDATA setUserBkgndCtrlsInit[] =
{   
    {	CTRL_LISTBOX,
	WS_VISIBLE|WS_BORDER|WS_VSCROLL | LBS_NOTIFY,
        30, 40, 80, 120,
        IDC_USERBKGND_LIST,
        NULL,
        0
    }	
};

/******************  运行速度设置  ********************************/
static DLGTEMPLATE setRunSpeedDlgBox =
{
    WS_BORDER ,
    WS_EX_NONE,
    160, 120, 300, 140,
    "setRunSpeed",
    0, 0,
    2, NULL,
    0
};

static CTRLDATA setRunSpeedCtrlsInit[] =
{   
    {	CTRL_SLEDIT,
        WS_VISIBLE | WS_BORDER | ES_READONLY,
        130, 10, 140, 28,
        IDC_CURRENT_SPEED,
        NULL,
        0,
	WS_EX_TRANSPARENT
    },
    {	CTRL_LISTBOX,
	WS_VISIBLE|WS_BORDER|WS_VSCROLL | LBS_NOTIFY,
        130, 43, 140, 85,
        IDC_RUN_SPEED_LIST,
        NULL,
        0
    }	
};

/***********************************************************/
/**  推纸器位置设置 **/

/*   初始化背景,推纸器位置设置的背景绘制
 */
static void initSetCutPosBkgnd(HDC hdc)
{
     int sel = currentConfig;
     int x=10,y=15;
     FillBoxWithBitmap (hdc, 0, 0, 300, 150, &configureListBkgnd); 	// 背景   
     RefreshButtonArea (hdc,&configurebuttonBkgnd,MSG_KEYUP,x,y,100,32, sel);	// 切刀位置
     RefreshBoxArea (hdc,&configurebuttonBkgnd,x,60,100,32,1780,0);			// 当前位置
     RefreshBoxArea (hdc,&configurebuttonBkgnd,40,110,60,32,1880,0);		// 运行,1680
     RefreshBoxArea (hdc,&configurebuttonBkgnd,120,110,60,32,1720,0);		// 修正
     RefreshBoxArea (hdc,&configurebuttonBkgnd,200,110,60,32,1660,0);		// 退出
}

/*   修正当前位置值
 */
static void modifyCurrentCutPos(HWND mainWnd,HWND editWnd)
{
     int dataLen;
     char dataBuf[10];
     char cmdbuf[13];
     int unit = currentuser->unit;  
     int ret,data;
     dataLen = GetWindowTextLength(editWnd);	// 获得输入的字符
     GetWindowText(editWnd,dataBuf,dataLen);
     ret=isInputCutsDataValid(dataBuf,dataLen); 	// 判断输入是否有效
     if(ret >= 0 ){			// 输入有效,返回转换后的数据
	cmdData.flag = 'P';
	cmdData.status = 0x30;
	if(unit){
	    data = inch_to_mm(ret);
	    ret = data;
	}
	cmdData.data = ret;			
	setCmdFrame(&cmdData,cmdbuf);
	sendCommand(cmdbuf);		// 推纸器位置校正

     }else {
	MessageBox(mainWnd,config_help[*curSysLang].msg_cutdata_overflow,
		  config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
	SetWindowText(editWnd,"");	// 清除
     }
}

static int runToInputPos(HWND editHwnd)
{
     int dataLen;
     char dataBuf[10];
     char cmdbuf[13];
     int unit = currentuser->unit;
     int ret,data;
     dataLen = GetWindowTextLength(editHwnd);	// 获得输入的字符
     GetWindowText(editHwnd,dataBuf,dataLen);
     ret=isInputCutsDataValid(dataBuf,dataLen); 	// 判断输入是否有效
     if(ret >= 0 ){				// 输入有效,返回转换后的数据
	cmdData.flag = 'G';
	cmdData.status = 0x33;		// 与手动运行相同
	if(unit){
	    data = inch_to_mm(ret);
	    ret = data;
	}
	cmdData.data = ret;			
	setCmdFrame(&cmdData,cmdbuf);
	sendCommand(cmdbuf);
     }else {
	MessageBox(editHwnd,config_help[*curSysLang].msg_cutdata_overflow,
		  config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
	SetWindowText(editHwnd,"");	// 清除
     }
     return(ret);
}
/*
static void setCutPosEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{ 
     HWND mainHwnd;
     int dataLen;
     char dataBuf[10];
     char cmdbuf[13];
     int unit = currentuser->unit;
     int ret,data;

     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     switch(nc){
	case EN_ENTER:		// Enter key 按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    GetWindowText(hwnd,dataBuf,dataLen);
	    ret=isInputCutsDataValid(dataBuf,dataLen); 	// 判断输入是否有效
 	    if(ret >= 0 ){				// 输入有效,返回转换后的数据
		cmdData.flag = 'G';
		cmdData.status = 0x33;		// 与手动运行相同
		if(unit){
		    data = inch_to_mm(ret);
		    ret = data;
		}
		cmdData.data = ret;			
		setCmdFrame(&cmdData,cmdbuf);
		sendCommand(cmdbuf);
	    }else {
		MessageBox(hwnd,config_help[*curSysLang].msg_cutdata_overflow,
		  config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		SetWindowText(hwnd,"");	// 清除
	    }

	    break;
     }
}
*/
static int setCutPosDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)		
{	
     HDC hdc;
     char cmdbuf[12];
     int data,index;
     int curPos;
     int unit = currentuser->unit;
     HWND hwndCutPosEdit = GetDlgItem(hDlg,IDC_CUT_POS);
     HWND hwndCurrentPosEdit = GetDlgItem(hDlg,IDC_CURRENT_POS);

     switch (message) 
	{
    	case MSG_INITDIALOG:
	    //SetNotificationCallback(hwndCutPosEdit, setCutPosEditCallback);
	    SetFocusChild(hwndCutPosEdit);
	    curPos = motorStatus.currentCutPos;	
	    refreshCurrentPos(hwndCurrentPosEdit,curPos);
	    return(1);
    	case MSG_ERASEBKGND:
        {  
	    hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            		}       
                    
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
            }
	    //初始化背景	        
	    initSetCutPosBkgnd(hdc); 

            if (fGetDC)
                ReleaseDC (hdc);
	    return 0;	
	}        
    	case MSG_KEYDOWN:
	    switch(LOWORD(wParam)){
		case SCANCODE_FORWARD:		//  切刀前进, 发送前进命令
		    memset(cmdbuf,0,12);
		    cmdData.flag = 'F';
		    cmdData.status = 0x30;		// 状态字节含有前进/后退标志
		    cmdData.data = 0;			
		    setCmdFrame(&cmdData,cmdbuf);
		    sendCommand(cmdbuf);
		    break;
		case SCANCODE_BACKWARD:	//  切刀后退,发送后退命令
		    memset(cmdbuf,0,12);
		    cmdData.flag = 'F';
		    cmdData.status = 0x31;
		    cmdData.data = 0;			
		    setCmdFrame(&cmdData,cmdbuf);
		    sendCommand(cmdbuf);		    
		    break;
		case SCANCODE_FORWARD_HS:
		    memset(cmdbuf,0,12);
		    cmdData.flag = 'F';
		    cmdData.status = 0x32;		// 状态字节含有前进/后退标志
		    cmdData.data = 0;
		    setCmdFrame(&cmdData,cmdbuf);
		    sendCommand(cmdbuf);
		    break;
		case SCANCODE_BACKWARD_HS:	//  切刀后退,发送后退命令
		    memset(cmdbuf,0,12);
		    cmdData.flag = 'F';
		    cmdData.status = 0x33;
		    cmdData.data = 0;
		    setCmdFrame(&cmdData,cmdbuf);
		    sendCommand(cmdbuf);
		    break;

		case SCANCODE_MODIFY:		//  修改位置(校正)
		    modifyCurrentCutPos(hDlg,hwndCutPosEdit);
		    break;

		case SCANCODE_ENTER:		//  确定键,修改位置(校正)+退出
		    modifyCurrentCutPos(hDlg,hwndCutPosEdit);
		    for(index=0;index<50;index++)	// 等待接收下位机返回修正信息
		    	usleep(10000);
		    SendMessage(hDlg,MSG_RECV,0,0);	// 手动接收信息
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;

		case SCANCODE_DELETE:		//  删除键,清除
		    SetFocusChild(hwndCutPosEdit);
  		    SetWindowText(hwndCutPosEdit,"");	
		    break;

		case SCANCODE_RUN:		//  运行,跟Enter一样的功能
		    runToInputPos(hwndCutPosEdit);
		    
		    break;
		case SCANCODE_HELP:		//   帮助
		    MessageBox(hDlg,config_help[*curSysLang].help_cutpos,
		  	config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		    break;
		case SCANCODE_CANCEL:		//   取消
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
	    }			
	    break;

    	case MSG_RECV:		// 收到位置信息,更新当前位置显示区
	    recvCommand(cmdbuf);		// 复制到用户缓冲区
	    //ret = isCmdValid(cmdbuf);	// 测试,不用校验
     	    //if(ret){
	    	memset(&cmdData,0,sizeof(cmdData));
	    	getCmdFrame(&cmdData,cmdbuf);	// 获得数据
	    	if(cmdData.flag == 'G'){			// 正常数据帧
		    data = cmdData.data;
		    if(unit){
			data = mm_to_inch(cmdData.data);
  		    }
	    	    motorStatus.currentCutPos = data;	// 更新数据
		    motorStatus.devStatus = cmdData.status;		// 更新状态,低四位有效 
		    refreshCurrentPos(hwndCurrentPosEdit,data);	// 设置当前位置
		}
	    	if(cmdData.flag == 'P'){			// 调整帧
		    data = cmdData.data;
		    if(unit){
			data = mm_to_inch(cmdData.data);
  		    }
	    	    motorStatus.currentCutPos = data;			// 更新数据
		    motorStatus.devStatus = cmdData.status;		// 更新状态,低四位有效 
		    refreshCurrentPos(hwndCurrentPosEdit,data);	// 设置当前位置
		}
	    //}
	    break;

    	case MSG_COMMAND:
	    switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
	    	EndDialog(hDlg,IDOK);
		break;
	    }
	    break;

	
	}
     return DefaultDialogProc (hDlg, message, wParam, lParam);
}


static void setCutPosDialogBox (HWND hwnd)
{ 
   setCutPosDlgBox.controls = setCutPosCtrlsInit;
   DialogBoxIndirectParam (&setCutPosDlgBox, hwnd,setCutPosDialogBoxProc, 0L);
} 
static int setCutPos (HWND hwnd)
{
    //printf("setCutPos().\n");
    setCutPosDialogBox(hwnd);		// Should not return except receive CLOSE MSG
    return 0;
 
}

/*********************************************************/
/*   初始化背景,极限设置的背景绘制
 */
static void initSetPosLimitBkgnd(HDC hdc)
{
     int sel = currentConfig;
     FillBoxWithBitmap (hdc, 0, 0, 0, 0, &configureListBkgnd); 	// 背景   
     RefreshButtonArea (hdc,&configurebuttonBkgnd,MSG_KEYUP,10,15,100,32, sel);// 输入提示
     RefreshBoxArea (hdc,&configurebuttonBkgnd,60,60,60,32,1600,0);		// 确定
     RefreshBoxArea (hdc,&configurebuttonBkgnd,150,60,60,32,1660,0);	// 退出
}


static void setPosLimitEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{ 
     HWND mainHwnd;
     int dataLen,data;
     char dataBuf[10];
     int status = currentConfig;
     int unit = currentuser->unit;
     char cmdSendBuf[13];
     memset(cmdSendBuf,0,12);
     cmdData.flag = 'L';

     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     switch(nc){
	case EN_SETFOCUS:	// 列表框得到焦点
	    // Do nothing
	    break;
	case EN_ENTER:		// Enter key 按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    GetWindowText(hwnd,dataBuf,dataLen);
	    int ret=isInputCutsDataValid(dataBuf,dataLen); 	// 判断输入是否有效
 	    if(ret >= 0 ){					// 输入有效,返回转换后的数据
		switch(status){
		    case 1:	// 前极限
			currentuser->frontlimit = ret;
			saveUserConfigFile(0);	// 保存
			cmdData.status = 0x30;
			break;
		    case 2:	// 中极限
			currentuser->middlelimit = ret;
			saveUserConfigFile(0);	// 保存
			cmdData.status = 0x31;
			break;	
		    case 3:	// 后极限
			currentuser->backlimit = ret;
			saveUserConfigFile(0);	// 保存
			cmdData.status = 0x32;
			break;	
 	    	}

		if(unit){	
		    data = inch_to_mm(ret);
		} else {
	 	    data = ret;
		}
		cmdData.data = data;
		setCmdFrame(&cmdData,cmdSendBuf);		// 组帧	step*0.01毫米
    		sendCommand(cmdSendBuf);  		// Send
		MessageBox(hwnd,config_help[*curSysLang].msg_config_ok,
		  config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0);	// 退出
	    }else {
		MessageBox(hwnd,config_help[*curSysLang].msg_cutdata_overflow,
		  config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		SetWindowText(hwnd,"");	// 清除
	    }
	    break;
     }
}

static int setPosLimitDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)		
{	
     HDC hdc;
     HWND hwndCutPosEdit = GetDlgItem(hDlg,IDC_POS_LIMIT);
     
     switch (message) 
	{
    	case MSG_INITDIALOG:
	    SetNotificationCallback(hwndCutPosEdit, setPosLimitEditCallback);
	    SetFocusChild(hwndCutPosEdit);	
	    return(1);
    	case MSG_ERASEBKGND:
        {  
	    hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            		}       
                    
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
            }
	    //初始化背景	        
	    initSetPosLimitBkgnd(hdc); 

            if (fGetDC)
                ReleaseDC (hdc);
	    return 0;	
	}        
    	case MSG_KEYDOWN:
	    switch(LOWORD(wParam)){
		case SCANCODE_CANCEL:	//   取消
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
            case SCANCODE_HELP:	//帮助
		MessageBox(hDlg,config_help[*curSysLang].help_limit,
		  	config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		break;
	    }			
	    break;
    	case MSG_COMMAND:
	    switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
	    	EndDialog(hDlg,IDOK);
		break;
	    }
	    break;

	
	}
     return DefaultDialogProc (hDlg, message, wParam, lParam);
}


static void setPosLimitDialogBox (HWND hwnd)
{ 
   setPosLimitDlgBox.controls = setPosLimitCtrlsInit;
   DialogBoxIndirectParam (&setPosLimitDlgBox, hwnd,setPosLimitDialogBoxProc, 0L);
} 
static int setPosLimit (HWND hwnd)
{
    //printf("setPosLimit().\n");
    setPosLimitDialogBox(hwnd);		// Should not return except receive CLOSE MSG
    return 0;
 
}


/*********************************************************/
/*   初始化背景,步进量设置
 */
static int currentStepMode;	// 当前步进模式 0 连续 1 步进
static int isStepDataEditWindowShow;
static int getCurrentStepMode(int stepSetStatus)
{
    int mode = 1;
    int curStep = 0;
    switch(stepSetStatus){
	case 4:
	    curStep = currentuser->forwardstep;
	    break;
   	case 5:
	    curStep = currentuser->backwardstep;
	    break;
    }
    if(curStep == 0){	// 连续
	mode = 0;
    } 
    return(mode);
}
static void initSetRunStepBkgnd(HDC hdc,HWND hWndStepEdit,int stepMode)
{
     int startx=1940;
     FillBoxWithBitmap (hdc, 0, 0, 0, 0, &configureListBkgnd); 	// 背景   
     RefreshBoxArea (hdc,&configurebuttonBkgnd,130,60,60,32,1600,0);	// 确定
     RefreshBoxArea (hdc,&configurebuttonBkgnd,210,60,60,32,1660,0);	// 退出

     if(stepMode){		// 步进模式
	RefreshBoxArea (hdc,&configurebuttonBkgnd,10,60,100,32,startx,0);	// 连续
	RefreshBoxArea (hdc,&configurebuttonBkgnd,10,15,100,32,startx+300,0);	// 选择步进
     } else {		// 连续模式
        if(isStepDataEditWindowShow){
	    ShowWindow(hWndStepEdit,SW_HIDE);
	    isStepDataEditWindowShow = 0;
 	} 
	RefreshBoxArea (hdc,&configurebuttonBkgnd,10,60,100,32,startx+100,0);	// 连续
	RefreshBoxArea (hdc,&configurebuttonBkgnd,10,15,100,32,startx+200,0);	// 选择步进
     }

}

static void refreshSetRunStepBkgnd(HDC hdc,HWND hWndStepEdit,int stepMode)
{
     int startx=1940;
     if(stepMode){		// 步进模式
	RefreshBoxArea (hdc,&configurebuttonBkgnd,10,60,100,32,startx,0);	// 连续
	RefreshBoxArea (hdc,&configurebuttonBkgnd,10,15,100,32,startx+300,0);	// 选择步进
	if(isStepDataEditWindowShow == 0){
	    ShowWindow(hWndStepEdit,SW_SHOW);
	    isStepDataEditWindowShow = 1;
	    SetFocusChild(hWndStepEdit);	
 	} 
     } else {		// 连续模式
        if(isStepDataEditWindowShow){
	    ShowWindow(hWndStepEdit,SW_HIDE);
	    isStepDataEditWindowShow = 0;
 	} 
	RefreshBoxArea (hdc,&configurebuttonBkgnd,10,60,100,32,startx+100,0);	// 连续
	RefreshBoxArea (hdc,&configurebuttonBkgnd,10,15,100,32,startx+200,0);	// 选择步进
     }
}

static void setRunStepEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{ 
     HWND mainHwnd;
     int dataLen;
     char dataBuf[10];
     int status = currentConfig;

     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     switch(nc){
	case EN_ENTER:		// Enter key 按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    GetWindowText(hwnd,dataBuf,dataLen);
	    int ret=isInputCutsDataValid(dataBuf,dataLen); 	// 判断输入是否有效
 	    if((ret >= 0) &&(dataLen>0)){	// 输入有效,返回转换后的数据
					//dataLen = 0说明用户没有输入直接输入确定
		switch(status){
		    case 4:	// 前进步进量
			if((ret>500) || ((ret%100) != 0)){
			   MessageBox(hwnd,config_help[*curSysLang].msg_step_overflow,
		  		config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
			   SetWindowText(hwnd,"");	// 清除
			   return;
			}
			currentuser->forwardstep = ret ;	// 1mm的整数倍
			saveUserConfigFile(0);	// 保存
			break;	
		    case 5:	// 后退步进量
			if((ret%100) != 0){	// 后退步进量没有大小限制
			   MessageBox(hwnd,config_help[*curSysLang].msg_step_overflow,
		  		config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
			   SetWindowText(hwnd,"");	// 清除
			   return;			// 返回
			}
			currentuser->backwardstep = ret;
			saveUserConfigFile(0);	// 保存
			break;	

 	    	}
		MessageBox(hwnd,config_help[*curSysLang].msg_config_ok,
		  	config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0);	// 退出
	    }else {
		MessageBox(hwnd,config_help[*curSysLang].msg_step_overflow,
		  	config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		SetWindowText(hwnd,"");	// 清除
	    }
	    break;
     }
}

static int setRunStepDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)		
{	
     HDC hdc;
     HWND hwndCutPosEdit = GetDlgItem(hDlg,IDC_POS_LIMIT);
     
     switch (message) 
	{
    	case MSG_INITDIALOG:
	    SetNotificationCallback(hwndCutPosEdit, setRunStepEditCallback);
	    SetFocusChild(hwndCutPosEdit);	
            currentStepMode = getCurrentStepMode(currentConfig);
	    isStepDataEditWindowShow = 1;
	    return(1);
    	case MSG_ERASEBKGND:
        {  
	    hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            		}       
                    
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
            }
	    //初始化背景	        
	    initSetRunStepBkgnd(hdc,hwndCutPosEdit,currentStepMode); 

            if (fGetDC)
                ReleaseDC (hdc);
	    return 0;	
	}        
    	case MSG_KEYDOWN:
	    switch(LOWORD(wParam)){
		case SCANCODE_CURSORBLOCKUP:		// 向上选择
		    hdc=BeginPaint(hDlg);
		    if(currentStepMode==0){
		   	currentStepMode = 1;
			refreshSetRunStepBkgnd(hdc,hwndCutPosEdit,currentStepMode);
 		    }
		    EndPaint(hDlg,hdc);
		    break;
		case SCANCODE_CURSORBLOCKDOWN:	// 向下选择
		    hdc=BeginPaint(hDlg);
		    if(currentStepMode==1){
		   	currentStepMode = 0;
			refreshSetRunStepBkgnd(hdc,hwndCutPosEdit,currentStepMode);
 		    }
		    EndPaint(hDlg,hdc);
		    break;
		case SCANCODE_ENTER:	//   取消
		    if(currentStepMode==1){
			break;
		    }
		    switch(currentConfig){	// 用户确认连续模式
		    	case 4:	// 步进量
			    currentuser->forwardstep = 0 ;	
			    saveUserConfigFile(0);	// 保存
			    break;	
		    	case 5:	// 步进量
			    currentuser->backwardstep = 0;
			    saveUserConfigFile(0);	// 保存
			    break;	
 	    	    }
		    MessageBox(hDlg,config_help[*curSysLang].msg_config_ok,
		  	config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
		case SCANCODE_CANCEL:	//   取消
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
            	case SCANCODE_HELP:	//帮助
		    MessageBox(hDlg,config_help[*curSysLang].help_step,
		  	config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		break;
	    }			
	    break;
    	case MSG_COMMAND:
	    switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
	    	EndDialog(hDlg,IDOK);
		break;
	    }
	    break;

	
	}
     return DefaultDialogProc (hDlg, message, wParam, lParam);
}


static void setRunStepDialogBox (HWND hwnd)
{ 
   setPosLimitDlgBox.controls = setPosLimitCtrlsInit;
   DialogBoxIndirectParam (&setPosLimitDlgBox, hwnd,setRunStepDialogBoxProc, 0L);
} 
static int setRunStep (HWND hwnd)	// 设置步进量
{
    //printf("setRunStep().\n");
    setRunStepDialogBox(hwnd);		// Should not return except receive CLOSE MSG
    return 0;
 
}


/***********************************************************/
/**  系统日期设置 **/
static struct rtc_tm tmSet;
static int dateInputStatus = 0; //日期输入状态,必须全部输入完成后按回车才有效

/*  检查待设置的日期时间是否有效
 */
static int isDateTimeValid(struct rtc_tm *tm,int flag)
{
    switch(flag){
	case RTC_SETDATE:	// 检查日期
	    
	    break;
	case RTC_SETTIME:	// 检查时间

	    break;
    }

    return(0);
}

static void initSetSysDateBkgnd(HDC hdc)
{
     int bx = 1800;

     FillBoxWithBitmap (hdc, 0, 0, 0, 0, &configureListBkgnd); 	// 背景   
     RefreshBoxArea (hdc,&configurebuttonBkgnd,40,15,200,32,bx,0);	// 提示
     RefreshBoxArea (hdc,&configurebuttonBkgnd,60,60,60,32,2200,0);		// 确定
     RefreshBoxArea (hdc,&configurebuttonBkgnd,150,60,60,32,2260,0);		// 退出
}

/*  数据合理性验证函数
 */
static int isInputDateValid(char *pBuf,int flag)
{
    int data = atoi(pBuf);
    switch(flag){
	case 0:	// 年
	    if((data < 2000)|| (data>2099)){
		return(0);
	    }
	    break;
	case 1:	// 月
	    if((data < 1)|| (data>12)){
		return(0);
	    }
	    break;
	case 2:	// 日,这里不进行闰年,大小月的判断
	    if((data < 1)|| (data>31)){
		return(0);
	    }
	    break;
    }

    return(data);

}

static int getDateEditData(HWND *hwndEdit)
{
    int index,ret;
    char cutDataBuf[10];
    int  cutDataLen=0;

    if(dateInputStatus == 7){
  	return 0;
    }

    //tmSet.tm_year = 0;
    //tmSet.tm_mon = 0;
    //tmSet.tm_mday = 0;
    dateInputStatus = 0;

    for(index=0;index<3;index++){
	memset(cutDataBuf,0,10);
	cutDataLen = GetWindowTextLength(hwndEdit[index]);	// 获得输入的字符
	GetWindowText(hwndEdit[index],cutDataBuf,cutDataLen);
	ret=isInputDateValid(cutDataBuf,index); 	// 判断输入是否有效
 	if(ret > 0 ){
	    switch(index){
		case 0:
		     tmSet.tm_year = ret - 1900;
		     dateInputStatus |= 0x1;	//bit 0		
		     break;
		case 1:
		     tmSet.tm_mon = ret - 1;
		     dateInputStatus |= 0x2;	
		     break;
		case 2:
		     tmSet.tm_mday = ret;
		     dateInputStatus |= 0x4;	
		     break;
	    }
	}else{
	    SetWindowText(hwndEdit[index],"");		// 清空输入区
	    SetFocusChild(hwndEdit[index]);
            //printf("getlabledata status=%d \n",pProEdit->lableEdit.lableStatus);
	    return(-1);
	}

}
     return 0;


}
static void setSysYearEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{ 
     HWND mainHwnd;
     HWND monthHwnd;
     int year;
     int dataLen;
     char dataBuf[6];

     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     monthHwnd = GetDlgItem(mainHwnd,IDC_MONTH);

     switch(nc){
	case EN_CHANGE:		// 有数字键按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    if(dataLen==4){		// 年 输入完成
		GetWindowText(hwnd,dataBuf,dataLen);
		year = isInputDateValid(dataBuf,0);
		if(!year){	// 输入非法
		    SetWindowText(hwnd,"");	// clear  
		} else {			// 输入有效
		    dateInputStatus |= 0x1;
		    //printf("year = %d \n",year);
		    tmSet.tm_year = year - 1900;
		    SetFocusChild(monthHwnd);
		}
	    }else if(dataLen > 4){
		SetWindowText(hwnd,"");	// clear
 	    }
		
	    break;
     }
}
static void setSysMonthEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{ 
     HWND mainHwnd;
     HWND dayHwnd;
     int month;
     int dataLen;
     char dataBuf[6];

     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     dayHwnd = GetDlgItem(mainHwnd,IDC_DAY);

     switch(nc){
	case EN_CHANGE:		// 有数字键按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    if(dataLen == 2){		// 年 输入完成
		GetWindowText(hwnd,dataBuf,dataLen);
		month = isInputDateValid(dataBuf,1);
		if(!month){	// 输入非法
		    SetWindowText(hwnd,"");	// clear  
		} else {			// 输入有效
		    dateInputStatus |= 0x2;
		    //printf("month = %d \n",month);
		    tmSet.tm_mon = month - 1;
		    SetFocusChild(dayHwnd);
		}
	    }else if(dataLen > 2){
		SetWindowText(hwnd,"");	// clear
 	    }
		
	    break;
     }
}
static void setSysDayEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{ 
     HWND mainHwnd;
     int day;
     int dataLen;
     char dataBuf[6];

     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     switch(nc){
	case EN_CHANGE:		// 有数字键按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    if(dataLen == 2){		// 日 输入完成
		GetWindowText(hwnd,dataBuf,dataLen);
		day = isInputDateValid(dataBuf,2);
		if(!day){	// 输入非法
		    SetWindowText(hwnd,"");	// clear  
		} else {			// 输入有效
		    dateInputStatus |= 0x4;
		    //printf("day = %d \n",day);
		    tmSet.tm_mday = day;
		}
	    } else if(dataLen > 2){		// 输入溢出,删除最后一个
		SetWindowText(hwnd,"");	// clear
	    }
		
	    break;
     }
}


static int setSysDateDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)		
{	
     HDC hdc;
     HWND hwndTmp,hwndEdit[3];
     hwndEdit[0] = GetDlgItem(hDlg,IDC_YEAR);
     hwndEdit[1] = GetDlgItem(hDlg,IDC_MONTH);
     hwndEdit[2] = GetDlgItem(hDlg,IDC_DAY);

     struct rtc_tm tm;
     char dispBuf[6];

     switch (message) 
	{
    	case MSG_INITDIALOG:
	    SetNotificationCallback(hwndEdit[0], setSysYearEditCallback);
	    SetNotificationCallback(hwndEdit[1], setSysMonthEditCallback);
	    SetNotificationCallback(hwndEdit[2], setSysDayEditCallback);

#ifdef CONFIG_DEBUG_ON_PC
	    tm.tm_year = 2006;
	    tm.tm_mon = 9;
	    tm.tm_mday = 1;
#else
	    getSysRtcTime(&tm); 
#endif
	    sprintf(dispBuf,"%04d",tm.tm_year);
	    SetWindowText(hwndEdit[0],dispBuf);
	    sprintf(dispBuf,"%02d",tm.tm_mon+1);
	    SetWindowText(hwndEdit[1],dispBuf);
	    sprintf(dispBuf,"%02d",tm.tm_mday);
	    SetWindowText(hwndEdit[2],dispBuf);

	    SetFocusChild(hwndEdit[0]);
	    dateInputStatus = 0;	
	    return(1);
    	case MSG_ERASEBKGND:
        {  
	    hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            		}       
                    
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
            }
	    //初始化背景	        
	    initSetSysDateBkgnd(hdc); 

            if (fGetDC)
                ReleaseDC (hdc);
	    return 0;	
	}        
    	case MSG_KEYDOWN:
	    switch(LOWORD(wParam)){
		case SCANCODE_CURSORBLOCKUP:	//   上
		    hwndTmp=GetFocusChild(hDlg);
  		    if(hwndTmp == hwndEdit[0]){
			break;
		    }
  		    if(hwndTmp == hwndEdit[2]){
			SetFocusChild(hwndEdit[1]);
		    }
  		    if(hwndTmp == hwndEdit[1]){
			SetFocusChild(hwndEdit[0]);
		    }
		    break;
		case SCANCODE_CURSORBLOCKDOWN:	//   下
		    hwndTmp=GetFocusChild(hDlg);
  		    if(hwndTmp == hwndEdit[2]){
			break;
		    }
  		    if(hwndTmp == hwndEdit[0]){
			SetFocusChild(hwndEdit[1]);
		    }
  		    if(hwndTmp == hwndEdit[1]){
			SetFocusChild(hwndEdit[2]);
		    }
		    break;
		case SCANCODE_ENTER:	//  设置
		    getDateEditData(hwndEdit);
		    //printf("YMD1:%d %d %d \n",tmSet.tm_year,tmSet.tm_mon,tmSet.tm_mday);
		    if(dateInputStatus != 7){	// 没有输入
			break;
  		    }
		    //printf("Input Done. SetSysDate.\n");			// 保存
		    if(!isDateTimeValid(&tmSet,RTC_SETDATE)){
			//printf("Input data OK. setSysRtcDateTime.\n");	
			//printf("YMD2:%d %d %d \n",tmSet.tm_year,tmSet.tm_mon,tmSet.tm_mday);
			int ret = setSysRtcDateTime(&tmSet,RTC_SETDATE);
			if(!ret){
			    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);	// 退出
			}    
		    }
		    break;
		case SCANCODE_CANCEL:	//   取消
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
            	case SCANCODE_HELP:	//帮助
		    MessageBox(hDlg,config_help[*curSysLang].help_date,
		  	config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		    break;
	    }			
	    break;
    	case MSG_COMMAND:
	    switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
	    	EndDialog(hDlg,IDOK);
		break;
	    }
	    break;

	
	}
     return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setSysDateDialogBox (HWND hwnd)
{ 
   setSysDateDlgBox.controls = setSysDateCtrlsInit;
   DialogBoxIndirectParam (&setSysDateDlgBox, hwnd,setSysDateDialogBoxProc, 0L);
} 
static int setSysDate (HWND hwnd)
{
    //printf("setSysDate().\n");
    setSysDateDialogBox(hwnd);		// Should not return except receive CLOSE MSG
    return 0;
 
}

/***********************************************************/
/**  系统时间设置 **/
static int timeInputStatus = 0;
static void initSetSysTimeBkgnd(HDC hdc)
{
     int bx = 2000;

     FillBoxWithBitmap (hdc, 0, 0, 0, 0, &configureListBkgnd); 	// 背景   
     RefreshBoxArea (hdc,&configurebuttonBkgnd,40,15,200,32,bx,0);	// 提示
     RefreshBoxArea (hdc,&configurebuttonBkgnd,60,60,60,32,2200,0);	// 确定
     RefreshBoxArea (hdc,&configurebuttonBkgnd,150,60,60,32,2260,0);	// 退出
}

/*  数据合理性验证函数
 */
static int isInputTimeValid(char *pBuf,int flag)
{
    int data = atoi(pBuf);
    switch(flag){
	case 0:	// 小时
	    if((data < 0)|| (data>23)){
		return(-1);
	    }
	    break;
	case 1:	// 分
	    if((data < 0)|| (data>59)){
		return(-1);
	    }
	    break;
    }

    return(data);

}
static void setSysHourEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{ 
     HWND mainHwnd;
     HWND minuteHwnd;
     int hour;
     int dataLen;
     char dataBuf[6];

     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     minuteHwnd = GetDlgItem(mainHwnd,IDC_MINUTE);
     switch(nc){
	case EN_CHANGE:		// 有数字键按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    if(dataLen== 2){		// 小时输入完成
		GetWindowText(hwnd,dataBuf,dataLen);
		hour = isInputTimeValid(dataBuf,0);
		if(hour < 0){			// 输入非法
		    SetWindowText(hwnd,"");	// clear  
		} else {			// 输入有效
		    //printf("hour = %d \n",hour);
		    timeInputStatus |= 0x01;
		    tmSet.tm_hour = hour;
		    SetFocusChild(minuteHwnd);
		}
	    } else if(dataLen > 2){
		SetWindowText(hwnd,"");	// clear  
	    }
	    break;
     }
}
static void setSysMinuteEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{ 
     HWND mainHwnd;
     int minute;
     int dataLen;
     char dataBuf[6];

     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     switch(nc){
	case EN_CHANGE:		// 有数字键按下
	    dataLen = GetWindowTextLength(hwnd);	// 获得输入的字符
	    if(dataLen== 2){		// 小时输入完成
		GetWindowText(hwnd,dataBuf,dataLen);
		minute = isInputTimeValid(dataBuf,1);
		if(minute < 0){			// 输入非法
		    SetWindowText(hwnd,"");	// clear  
		} else {			// 输入有效
		    timeInputStatus |= 0x02;
		    tmSet.tm_min = minute;
		    tmSet.tm_sec = 0;
		    //printf("min = %d \n",minute);
		}
	    }else if(dataLen > 2){		// 输入溢出,删除最后一个
		SetWindowText(hwnd,"");	// clear  
	    }
	    break;
     }
}

static int getTimeEditData(HWND hDlg)
{
     char dataBuf[10];
     int  dataLen=0;
     int ret;
     HWND hwndHourEdit = GetDlgItem(hDlg,IDC_HOUR);
     HWND hwndMinEdit = GetDlgItem(hDlg,IDC_MINUTE);
     timeInputStatus = 0;
     tmSet.tm_hour = 0;
     tmSet.tm_min = 0;
     tmSet.tm_sec = 0;
     dataLen = GetWindowTextLength(hwndHourEdit);	// 获得输入的字符
	    if(dataLen== 2){		// 输入完成
		GetWindowText(hwndHourEdit,dataBuf,dataLen);
		ret = isInputTimeValid(dataBuf,1);
		if(ret < 0){			// 输入非法
		    SetWindowText(hwndHourEdit,"");	// clear  
		} else {			// 输入有效
		    timeInputStatus |= 0x01;
		    tmSet.tm_hour = ret;
		}
	    }else if(dataLen > 2){		// 输入溢出,删除最后一个
		SetWindowText(hwndHourEdit,"");	// clear  
	    }

     dataLen = GetWindowTextLength(hwndMinEdit);	// 获得输入的字符
	    if(dataLen== 2){		// 输入完成
		GetWindowText(hwndMinEdit,dataBuf,dataLen);
		ret = isInputTimeValid(dataBuf,1);
		if(ret < 0){			// 输入非法
		    SetWindowText(hwndMinEdit,"");	// clear  
		} else {			// 输入有效
		    timeInputStatus |= 0x02;
		    tmSet.tm_min = ret;
		}
	    }else if(dataLen > 2){		// 输入溢出,删除最后一个
		SetWindowText(hwndMinEdit,"");	// clear  
	    }

      return 0;
}
static int setSysTimeDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)		
{	
     HDC hdc;
     HWND hwndHourEdit = GetDlgItem(hDlg,IDC_HOUR);
     HWND hwndMinEdit = GetDlgItem(hDlg,IDC_MINUTE);
     struct rtc_tm tm;
     char dispBuf[6];

     switch (message) 
	{
    	case MSG_INITDIALOG:
	    SetNotificationCallback(hwndHourEdit, setSysHourEditCallback);
	    SetNotificationCallback(hwndMinEdit, setSysMinuteEditCallback);

#ifdef CONFIG_DEBUG_ON_PC
	    tm.tm_hour = 23;
	    tm.tm_min = 30;
#else
	    getSysRtcTime(&tm); 
#endif
	    sprintf(dispBuf,"%02d",tm.tm_hour);
	    SetWindowText(hwndHourEdit,dispBuf);
	    sprintf(dispBuf,"%02d",tm.tm_min);
	    SetWindowText(hwndMinEdit,dispBuf);

	    SetFocusChild(hwndHourEdit);	
	    timeInputStatus = 0;
	    return(1);
    	case MSG_ERASEBKGND:
        {  
	    hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            		}       
                    
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
            }
	    //初始化背景	        
	    initSetSysTimeBkgnd(hdc); 

            if (fGetDC)
                ReleaseDC (hdc);
	    return 0;	
	}        
    	case MSG_KEYDOWN:
	    switch(LOWORD(wParam)){
		case SCANCODE_CURSORBLOCKUP:	//   上
		    SetFocusChild(hwndHourEdit);
		    break;
		case SCANCODE_CURSORBLOCKDOWN:	//   下
		    SetFocusChild(hwndMinEdit);
		    break;
		case SCANCODE_ENTER:	//  设置
		    getTimeEditData(hDlg);
		    if(timeInputStatus != 3 ){	// 没有输入
			break;
  		    }
		    if(!isDateTimeValid(&tmSet,RTC_SETTIME)){	
			int ret = setSysRtcDateTime(&tmSet,RTC_SETTIME);
			if(!ret){
			    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);	// 退出
			}    
		    }
		    break;
		case SCANCODE_CANCEL:	//   取消
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
            	case SCANCODE_HELP:	//帮助
		    MessageBox(hDlg,config_help[*curSysLang].help_time,
		  	config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		    break;
	    }			
	    break;
    	case MSG_COMMAND:
	    switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
	    	EndDialog(hDlg,IDOK);
		break;
	    }
	    break;

	
	}
     return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setSysTimeDialogBox (HWND hwnd)
{ 
   setSysTimeDlgBox.controls = setSysTimeCtrlsInit;
   DialogBoxIndirectParam (&setSysTimeDlgBox, hwnd,setSysTimeDialogBoxProc, 0L);
} 
static int setSysTime (HWND hwnd)
{
    //printf("setSysDate().\n");
    setSysTimeDialogBox(hwnd);		// Should not return except receive CLOSE MSG
    return 0;
 
}

/***********************************************************/
/**  系统语言设置 **/

static void refreshSetSysLangBkgnd(HDC hdc,int lan)
{
     int x=30,y=15;

     if(!lan){	// 选择中文
     	RefreshButtonArea (hdc,&configurebuttonBkgnd,MSG_KEYDOWN,x,y,100,32, 7);	// 中文
     	RefreshButtonArea (hdc,&configurebuttonBkgnd,MSG_KEYUP,x+110,y,100,32, 8);	// 英文
     } else {
     	RefreshButtonArea (hdc,&configurebuttonBkgnd,MSG_KEYUP,x,y,100,32, 7);	// 中文
     	RefreshButtonArea (hdc,&configurebuttonBkgnd,MSG_KEYDOWN,x+110,y,100,32, 8);	// 英文
     }

}

/*   初始化背景
 */
static void initSetSysLangBkgnd(HDC hdc)
{
     int sysLan = userinformation[0].language;

     FillBoxWithBitmap (hdc, 0, 0, 0, 0, &configureListBkgnd); 	// 背景   
     refreshSetSysLangBkgnd(hdc,sysLan);
     RefreshBoxArea (hdc,&configurebuttonBkgnd,60,60,60,32,2200,0);		// 确定
     RefreshBoxArea (hdc,&configurebuttonBkgnd,150,60,60,32,2260,0);	// 退出
}


static int setSysLangDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)		
{	
     HDC hdc;
     int sysLan = userinformation[0].language;
     
     switch (message) 
	{
    	case MSG_INITDIALOG:
	    //SetNotificationCallback(hwndBackLimitEdit, setBackLimitEditCallback);
	    //SetFocusChild(hwndBackLimitEdit);	
	    return(1);
    	case MSG_ERASEBKGND:
        {  
	    hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            		}       
                    
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
            }
	    //初始化背景	        
	    initSetSysLangBkgnd(hdc); 

            if (fGetDC)
                ReleaseDC (hdc);
	    return 0;	
	}        
    	case MSG_KEYDOWN:
	    switch(LOWORD(wParam)){
		case SCANCODE_CURSORBLOCKUP:		//  选择
		case SCANCODE_CURSORBLOCKLEFT:	
		    hdc=BeginPaint(hDlg);
		    sysLan = 0;				// 中文
		    userinformation[0].language = sysLan;
		    refreshSetSysLangBkgnd(hdc,sysLan);
		    EndPaint(hDlg,hdc);
		    break;
		case SCANCODE_CURSORBLOCKDOWN:	//  选择
		case SCANCODE_CURSORBLOCKRIGHT:	
		    hdc=BeginPaint(hDlg);
		    sysLan = 1;				// 英语
		    userinformation[0].language = sysLan;
		    refreshSetSysLangBkgnd(hdc,sysLan);
		    EndPaint(hDlg,hdc);
		    break;
		case SCANCODE_ENTER:	//  设置
		    if(currentConfig == 3){
			saveUserConfigFile(0);	// 保存
		 	SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    }
		    		    
		    break;
		case SCANCODE_CANCEL:	//   取消
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
            	case SCANCODE_HELP:	//帮助
		    MessageBox(hDlg,config_help[*curSysLang].help_lang,
		  	config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		    break;
	    }			
	    break;
    	case MSG_COMMAND:
	    switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
	    	EndDialog(hDlg,IDOK);
		break;
	    }
	    break;

	
	}
     return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setSysLangDialogBox (HWND hwnd)
{ 
   setSysLangDlgBox.controls = setSysLangCtrlsInit;
   DialogBoxIndirectParam (&setSysLangDlgBox, hwnd,setSysLangDialogBoxProc, 0L);
} 
static int setSysLang (HWND hwnd)
{
    //printf("setSysLanguage().\n");
    setSysLangDialogBox(hwnd);		// Should not return except receive CLOSE MSG
    return 0;
 
}


/***********************************************************/
/**  系统开机动画设置 **/

static void refreshSetInitFlashBkgnd(HDC hdc,int flag)
{
     int x=30,y=15;

     if(flag){	// 使用动画
     	RefreshButtonArea (hdc,&configurebuttonBkgnd,MSG_KEYDOWN,x,y,100,32, 5);	// 使用
     	RefreshButtonArea (hdc,&configurebuttonBkgnd,MSG_KEYUP,x+110,y,100,32, 6);	// 不使用
     } else {
     	RefreshButtonArea (hdc,&configurebuttonBkgnd,MSG_KEYUP,x,y,100,32, 5);	
     	RefreshButtonArea (hdc,&configurebuttonBkgnd,MSG_KEYDOWN,x+110,y,100,32, 6);	
     }

}


/*   初始化背景
 */
static void initSetInitFlashBkgnd(HDC hdc)
{
     FillBoxWithBitmap (hdc, 0, 0, 0, 0, &configureListBkgnd); 	// 背景   
     refreshSetInitFlashBkgnd(hdc,flashFlag);
     RefreshBoxArea (hdc,&configurebuttonBkgnd,60,60,60,32,2200,0);			// 确定
     RefreshBoxArea (hdc,&configurebuttonBkgnd,150,60,60,32,2260,0);		// 退出
}

static int setInitFlashDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)		
{	
     HDC hdc;
     
     switch (message) 
	{
    	case MSG_INITDIALOG:
	     flashFlag = startupParams.isFlashOn;

	    //SetNotificationCallback(hwndBackLimitEdit, setBackLimitEditCallback);
	    //SetFocusChild(hwndBackLimitEdit);	
	    return(1);
    	case MSG_ERASEBKGND:
        {  
	    hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            		}       
                    
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
            }
	    //初始化背景	        
	    initSetInitFlashBkgnd(hdc); 

            if (fGetDC)
                ReleaseDC (hdc);
	    return 0;	
	}        
    	case MSG_KEYDOWN:
	    switch(LOWORD(wParam)){
		case SCANCODE_CURSORBLOCKUP:		//  选择
		case SCANCODE_CURSORBLOCKLEFT:
		    hdc=BeginPaint(hDlg);
		    flashFlag = 1;	
		    refreshSetInitFlashBkgnd(hdc,flashFlag);
		    EndPaint(hDlg,hdc);
		    break;
		case SCANCODE_CURSORBLOCKDOWN:	//  选择
		case SCANCODE_CURSORBLOCKRIGHT:
		    hdc=BeginPaint(hDlg);
		    flashFlag = 0;	
		    refreshSetInitFlashBkgnd(hdc,flashFlag);
		    EndPaint(hDlg,hdc);
		    break;
		case SCANCODE_ENTER:	//  设置
		    if(currentConfig == 2){
   		        startupParams.isFlashOn = flashFlag;
			saveInitConfigFile(&startupParams);
		 	SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    }
		    		    
		    break;
		case SCANCODE_CANCEL:	//   取消
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
            	case SCANCODE_HELP:	//帮助
		    MessageBox(hDlg,config_help[*curSysLang].help_flash,
		  	config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		    break;
	    }			
	    break;
    	case MSG_COMMAND:
	    switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
	    	EndDialog(hDlg,IDOK);
		break;
	    }
	    break;

	
	}
     return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setInitFlashDialogBox (HWND hwnd)
{ 
   setSysLangDlgBox.controls = setSysLangCtrlsInit;
   DialogBoxIndirectParam (&setSysLangDlgBox, hwnd,setInitFlashDialogBoxProc, 0L);
} 
static int setInitFlash (HWND hwnd)
{
    //printf("setInitFlash().\n");
    setInitFlashDialogBox(hwnd);		// Should not return except receive CLOSE MSG
    return 0;
 
}

/***********************************************************/
/**  系统开机动画设置 **/

static int isScaleBmpLoaded = 0;
/*   初始化背景
 */
static void initSetUserBkgndBkgnd(HDC hdc)
{
     
     FillBoxWithBitmap (hdc, 0, 0, 320, 240, &configureListBkgnd); 	// 背景 
     FillBoxWithBitmap (hdc, UserBkgndScaleX, UserBkgndScaleY, 
			 UserBkgndScaleW,UserBkgndScaleH, pSetUserBkgnds); // 当前背景  
     RefreshBoxArea (hdc,&configurebuttonBkgnd,60,180,60,32,1600,0);		// 确定
     RefreshBoxArea (hdc,&configurebuttonBkgnd,150,180,60,32,1660,0);	// 退出
}

static void initUserBkgndList(HWND hwndList,int curSel)
{
     int index;
     char dispBuf[16];
     char tmp[4];
     SendMessage(hwndList,LB_RESETCONTENT,0,0);	// 清空列表
     for(index=0;index<UserBkgndNumber;index++){
	strcpy(dispBuf,config_help[*curSysLang].caption_bkgnd);
	sprintf(tmp,"%d ",index);
	strcat(dispBuf,tmp);
	SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)dispBuf);
     }
     SendMessage(hwndList,LB_SETCURSEL,curSel,0);	 // 高亮当前
}

int loadUserBkgndScale(BITMAP *pBkgnd,int curSel)
{
     switch(curSel){
	case 0:
	    LoadBitmap (HDC_SCREEN, pBkgnd, "./res/userProgramRun0.jpg");
	    break;
	case 1:
	    LoadBitmap (HDC_SCREEN, pBkgnd, "./res/userProgramRun1.jpg");
	    break;
        default:
	    LoadBitmap (HDC_SCREEN, pBkgnd, "./res/userProgramRun0.jpg");
	    break;
     }
     isScaleBmpLoaded = 1;
     return 0;
}
static void userBkgndListCallback(HWND hwnd,int id,int nc,DWORD add_data)
{ 
     HWND mainHwnd;
     HDC hdc;
     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     switch(nc){
	case LBN_SELCHANGE:	// 列表框选项有变化,增加小箭头
	    bkgndFlag = SendMessage(hwnd,LB_GETCURSEL,0,0);
	    //printf("sel change...%d \n",bkgndFlag);
	    if(isScaleBmpLoaded){
		 UnloadBitmap (&setUserBkgnds);	// unload background
            }
	    loadUserBkgndScale(&setUserBkgnds,bkgndFlag);
	    hdc = BeginPaint(mainHwnd);
	    FillBoxWithBitmap (hdc, UserBkgndScaleX, UserBkgndScaleY, 
			 UserBkgndScaleW,UserBkgndScaleH, &setUserBkgnds); // 当前背景 
	    EndPaint(mainHwnd,hdc); 
	    break;
	case LBN_ENTER:		// Enter key 按下
            
	    break;
     }
}


static int setUserBkgndDialogBoxProc(HWND hDlg, int message, 
				WPARAM wParam, LPARAM lParam)		
{	
     HDC hdc;
     HWND hwndBkgndList = GetDlgItem(hDlg,IDC_USERBKGND_LIST);
     switch (message) 
	{
    	case MSG_INITDIALOG:
	    bkgndFlag = startupParams.whichBkgnd;
	    SetNotificationCallback(hwndBkgndList, userBkgndListCallback);
	    initUserBkgndList(hwndBkgndList,bkgndFlag);	
	    pSetUserBkgnds = &userProgramEditBkgnd;
	    
	    SetFocusChild(hwndBkgndList);
	    return(1);
    	case MSG_ERASEBKGND:
        {  
	    hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            		}       
                    
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
            }
	    //初始化背景	        
	    initSetUserBkgndBkgnd(hdc); 

            if (fGetDC)
                ReleaseDC (hdc);
	    return 0;	
	}        
    	case MSG_KEYDOWN:
	    switch(LOWORD(wParam)){

		case SCANCODE_ENTER:	//  设置
		    if(currentConfig == 6){
			loadUserBkgndScale(&userProgramEditBkgnd,bkgndFlag);
			startupParams.whichBkgnd = bkgndFlag;
			saveInitConfigFile(&startupParams);
			if(isScaleBmpLoaded){
		 	    UnloadBitmap (&setUserBkgnds);	// unload background
            	        }
		    	SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    }	    
		    break;
		case SCANCODE_CANCEL:	//   取消
		    if(isScaleBmpLoaded){
		 	UnloadBitmap (&setUserBkgnds);	// unload background
            	    }
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
            	case SCANCODE_HELP:	//帮助
		    MessageBox(hDlg,config_help[*curSysLang].help_bkgnd,
		  	config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		    break;
	    }			
	    break;
    	case MSG_COMMAND:
	    switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
	    	EndDialog(hDlg,IDOK);
		break;
	    }
	    break;

	
	}
     return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setUserBkgndDialogBox (HWND hwnd)
{ 
   setUserBkgndDlgBox.controls = setUserBkgndCtrlsInit;
   DialogBoxIndirectParam (&setUserBkgndDlgBox, hwnd,setUserBkgndDialogBoxProc, 0L);
} 
static int setUserBkgnd (HWND hwnd)
{
 
    //printf("setUserBkgnd().\n");
    setUserBkgndDialogBox(hwnd);		// Should not return except receive CLOSE MSG
    UnloadBitmap (&setUserBkgnds);	// unload background
    return 0;
 
}

/*************设置运行速度********************************/
static void initSetRunSpeedBkgnd(HDC hdc)
{
     
     int sel = currentConfig;
     FillBoxWithBitmap (hdc, 0, 0, 300,140, &configureListBkgnd); 	// 背景   
     RefreshButtonArea (hdc,&configurebuttonBkgnd,MSG_KEYUP,10,10,100,32, sel);// 输入提示
     RefreshBoxArea (hdc,&configurebuttonBkgnd,30,55,60,32,1600,0);		// 确定
     RefreshBoxArea (hdc,&configurebuttonBkgnd,30,95,60,32,1660,0);	// 退出
}

static void initRunSpeedList(HWND hwndList,HWND hwndCurSpeed,int curSel)
{
     int index;
     char dispBuf[16];
     sprintf(dispBuf,"%4d   M/Min",curSel);
     SetWindowText(hwndCurSpeed,dispBuf);	// refresh current speed

     SendMessage(hwndList,LB_RESETCONTENT,0,0);	// 清空列表
     for(index=0;index<RunSpeedNumber;index++){
	sprintf(dispBuf,"%4d   M/Min",8+index);
	SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)dispBuf);
     }
     SendMessage(hwndList,LB_SETCURSEL,(curSel-8),0);	 // 高亮当前
}


static void RunSpeedListCallback(HWND hwnd,int id,int nc,DWORD add_data)
{ 
     HWND mainHwnd;
     int curSel;
     mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
     char cmdSendBuf[13];
     memset(cmdSendBuf,0,12);
     cmdData.flag = 'S';
     cmdData.status = 0x30;
     switch(nc){
	case LBN_ENTER:		// Enter key 按下
	    curSel = SendMessage(hwnd,LB_GETCURSEL,0,0);
            currentuser->speed = curSel + 8;
	    saveUserConfigFile(0);	// 保存
	    cmdData.data = currentuser->speed;
	    setCmdFrame(&cmdData,cmdSendBuf);		// 组帧	
    	    sendCommand(cmdSendBuf);  		// Send
	    MessageBox(hwnd,config_help[*curSysLang].msg_config_ok,
		  config_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);
	    SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0);	// 退出
	    break;
     }
}


static int setRunSpeedDialogBoxProc(HWND hDlg, int message, 
				WPARAM wParam, LPARAM lParam)		
{	
     HDC hdc;
     HWND hwndSpeedList = GetDlgItem(hDlg,IDC_RUN_SPEED_LIST);
     HWND hwndCurSpeed = GetDlgItem(hDlg,IDC_CURRENT_SPEED);
     switch (message) 
	{
    	case MSG_INITDIALOG:
	    SetNotificationCallback(hwndSpeedList, RunSpeedListCallback);
	    initRunSpeedList(hwndSpeedList,hwndCurSpeed,currentuser->speed);	
	
 	    
	    
	    SetFocusChild(hwndSpeedList);
	    return(1);
    	case MSG_ERASEBKGND:
        {  
	    hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            		}       
                    
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
            }
	    //初始化背景	        
	    initSetRunSpeedBkgnd(hdc); 

            if (fGetDC)
                ReleaseDC (hdc);
	    return 0;	
	}        
    	case MSG_KEYDOWN:
	    switch(LOWORD(wParam)){
		case SCANCODE_CANCEL:	//   取消
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
            	case SCANCODE_HELP:	//帮助
		    MessageBox(hDlg,config_help[*curSysLang].help_bkgnd,
		  	config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		    break;
	    }			
	    break;
    	case MSG_COMMAND:
	    switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
	    	EndDialog(hDlg,IDOK);
		break;
	    }
	    break;

	
	}
     return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void setRunSpeedDialogBox (HWND hwnd)
{ 
   setRunSpeedDlgBox.controls = setRunSpeedCtrlsInit;
   DialogBoxIndirectParam (&setRunSpeedDlgBox, hwnd,setRunSpeedDialogBoxProc, 0L);
} 
static int setRunSpeed (HWND hwnd)
{
 
    //printf("setRunSpeed().\n");
    setRunSpeedDialogBox(hwnd);		// Should not return except receive CLOSE MSG
    //UnloadBitmap (&setUserBkgnds);	// unload background
    return 0;
 
}
/*************************************************************************/
/*   进入对应的配置叶面
 */
static int loadConfigUserIcon(void)
{
    int langtype = *curSysLang;
    switch(langtype){	
	case 1:	// 英文
     	    if (LoadBitmap (HDC_SCREEN, &configurebuttonBkgnd, "./res/configuser_en.jpg"))// 配置选项
         	return 1; 
	    break;
	case 0:
	default:
     	    if (LoadBitmap (HDC_SCREEN, &configurebuttonBkgnd, "./res/configuser.jpg"))// 配置选项
         	return 1; 
	    break;    
    }
    return(0);
}
static void refreshConfigItem(HWND hDlg,int flag)
{
      HWND  hwndItem;
      int data;

      switch(flag){
	case 0:
	    data = motorStatus.currentCutPos;
	    hwndItem = GetDlgItem(hDlg,IDC_CURPOS);
	    break;
	case 1:
	    data = currentuser->frontlimit;
	    hwndItem = GetDlgItem(hDlg,IDC_LIMIT_F);	
	    break;
	case 2:
	    data = currentuser->middlelimit;
	    hwndItem = GetDlgItem(hDlg,IDC_LIMIT_M);
	    break;
	case 3:
	    data = currentuser->backlimit;
	    hwndItem = GetDlgItem(hDlg,IDC_LIMIT_B);
	    break;	
	case 4:
	    data = currentuser->speed * 100;		
	    hwndItem = GetDlgItem(hDlg,IDC_RUN_SPEED);
	    break;
        default:
	    return;
	
      }
      refreshCurrentPos(hwndItem,data);

}

static void EnterUserConfigDialog(HWND hwnd,int sel)
{
     switch(sel){
	case 0:	// 推纸器位置设置
	    setCutPos(hwnd);
     	    refreshConfigItem(hwnd,sel);
	    break;
	case 1:	// 前极限设置
	case 2:	// 中极限设置
	case 3:	// 后极限设置
	    setPosLimit(hwnd);	// 相同的处理
     	    refreshConfigItem(hwnd,sel);
	    break;
	case 4:  // 选择运行速度
	    setRunSpeed(hwnd);
     	    refreshConfigItem(hwnd,sel);
	    break;

	case 5:	// 退出
	    SendMessage(hwnd,MSG_COMMAND,IDCANCEL,0);
	    break;
     }
	
}

/*   刷新设置选项列表
 */
static int RefreshUserConfigListBkgnd(HDC hdc,int total,int sel)
{
     int i;
     int xBase = ConfigureStartX;
     int yBase = ConfigureStartY;
     int x,y,index;
     int flag;
     for(i=0;i<total;i++){
	   flag = ((i == sel)? MSG_KEYDOWN : MSG_KEYUP);
	   index = userConfigList[i];	// 得到位置信息
	   x = xBase;
           y = yBase + i*34; 
	   RefreshButtonArea (hdc,&configurebuttonBkgnd,flag,x,y,100,32, index);
	}

     return sel;
}

/*   初始化背景及设置选项
 */
static void initConfigBkgnd(HDC hdc)
{
     FillBoxWithBitmap (hdc, 0, 0, 0, 0, &configureBkgnd); 	// 背景 
     RefreshUserConfigListBkgnd(hdc,UserConfigItem,currentConfig);  

}

//*********************************************************//
static int configureDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)		
{	
      HDC hdc;
      HWND  hwndClock = GetDlgItem(hDlg,IDC_TIME);
      HWND  hwndCurrentPosEdit = GetDlgItem(hDlg,IDC_CURPOS);
      HWND  hwndFrontLimit = GetDlgItem(hDlg,IDC_LIMIT_F);
      HWND  hwndMiddlelimit = GetDlgItem(hDlg,IDC_LIMIT_M);
      HWND  hwndBackLimit = GetDlgItem(hDlg,IDC_LIMIT_B);
      HWND  hwndRunSpeed = GetDlgItem(hDlg,IDC_RUN_SPEED);
      char timeBuff[24];		// 时钟显示,当前位置显示
      int curPos;
      switch (message) 
	{
    	case MSG_INITDIALOG:	
	    currentConfig = 0;
	    SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);
	    if(isIMEOpenInMotor == 1){
	        ShowWindow(myIMEWindow,SW_HIDE);
		isIMEOpenInMotor = 0;
	    }
	    curPos = motorStatus.currentCutPos;
	    refreshCurrentPos(hwndCurrentPosEdit,curPos);	// 设置当前位置
	    refreshCurrentPos(hwndFrontLimit,currentuser->frontlimit);		// 设置前极限
	    refreshCurrentPos(hwndMiddlelimit,currentuser->middlelimit);	// 设置中极限
	    refreshCurrentPos(hwndBackLimit,currentuser->backlimit);		// 设置后极限
	    refreshCurrentPos(hwndRunSpeed,currentuser->speed * 100);		// 设置后极限

	    SetWindowElementColorEx(hwndClock,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 255, 255)); //White
	    SetTimer(hDlg,IDC_TIMER,50);	// 时钟显示
	    SetFocusChild(hwndClock);
	    return(1);
    	case MSG_TIMER:	
	    SetWindowText(hwndClock,convertSysTime(timeBuff));
	    return 0;          
    	case MSG_ERASEBKGND:
        {  
	    hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            		}       
                    
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
            }
	    //初始化背景	        
	    initConfigBkgnd(hdc);   
            if (fGetDC)
                ReleaseDC (hdc);
	    return 0;	
	}
    	case MSG_KEYDOWN:
	    switch(LOWORD(wParam)){
		case SCANCODE_ENTER:
		    EnterUserConfigDialog(hDlg,currentConfig);
		    
		    break;

		case SCANCODE_CURSORBLOCKUP:		// 向上选择
		    hdc=BeginPaint(hDlg);
		    // 单向选择
		    if(currentConfig>0){
		    	currentConfig--;
		    }else{
		   	currentConfig = 0;
 		    }
		    RefreshUserConfigListBkgnd(hdc,UserConfigItem,currentConfig);
		    EndPaint(hDlg,hdc);
		    break;
		case SCANCODE_CURSORBLOCKDOWN:	// 向下选择
		    hdc=BeginPaint(hDlg);
		    // 单向选择
		   if(currentConfig<UserConfigItem-1){
		    	currentConfig++;
		    }else{
		   	currentConfig = UserConfigItem-1;
 		    }
		    RefreshUserConfigListBkgnd(hdc,UserConfigItem,currentConfig);
		    EndPaint(hDlg,hdc);
		    break;
		case SCANCODE_CANCEL:	//   取消
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
            	case SCANCODE_HELP:	//帮助
		    MessageBox(hDlg,config_help[*curSysLang].help_config,
		  	config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		    break;
	    }			
	    break;

    	case MSG_COMMAND:
	    switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
		    KillTimer(hDlg,IDC_TIMER);
	    	    EndDialog(hDlg,IDOK);
		    break;
	    }
	    break;

	}
     return DefaultDialogProc (hDlg, message, wParam, lParam);
}



static void configureDialogBox (HWND hwnd)
{ 
   configureDlgBox.controls = configureCtrlsInit;
   DialogBoxIndirectParam (&configureDlgBox, hwnd,configureDialogBoxProc, 0L);
} 

int EnterConfigureUser (HWND hwnd)
{
     if (LoadBitmap (HDC_SCREEN, &configureBkgnd, "./res/configBkgnd.jpg"))	// 主背景
         return 1;
     if(loadConfigUserIcon())
	 return(1);
     if (LoadBitmap (HDC_SCREEN, &configureListBkgnd, "./res/configItemBkgnd.jpg"))// 配置界面背景
         return 1; 
    printf("Enter System Configure...\n");
    configureDialogBox(hwnd);		// Should not return except receive CLOSE MSG
    
    //printf("Unload configure res...\n");
    UnloadBitmap (&configureBkgnd);	// unload background
    UnloadBitmap (&configureListBkgnd);	// unload background
    UnloadBitmap (&configurebuttonBkgnd);	// unload background
    return 0;
 
}
/*************************************************************/
/*   初始化背景及设置选项
 */

static int loadConfigCommonIcon(void)
{
    int langtype = *curSysLang;
    switch(langtype){	
	case 1:	// 英文
     	    if (LoadBitmap (HDC_SCREEN, &configurebuttonBkgnd, "./res/configcommon_en.jpg"))
         	return 1; 
	    break;
	case 0:
	default:
     	    if (LoadBitmap (HDC_SCREEN, &configurebuttonBkgnd, "./res/configcommon.jpg"))
         	return 1; 
	    break;    
    }
    return(0);
}

static int EnterCommonConfigDialog(HWND hwnd,int sel)
{
     switch(sel){
	case 0:	// 日期设置
	    setSysDate(hwnd);	
	    break;
	case 1:	// 时间设置
	    setSysTime(hwnd);	
	    break;
	case 2:	// 开机动画
	    setInitFlash(hwnd);
	    break;
	case 3:	// 语言设置
	    printf("setSysLang...\n");
	    setSysLang(hwnd);
	    loadConfigCommonIcon();		// 重新载入
	    return(1);
	    break;
	case 4:	// 退出
	    SendMessage(hwnd,MSG_COMMAND,IDCANCEL,0);	
	    break;
     }
     return 0;	
}

static int RefreshCommonConfigListBkgnd(HDC hdc,int total,int sel)
{
     int i;
     int xBase = CommonConfigStartX;
     int yBase = CommonConfigStartY;
     int x,y,index;
     int flag;
     for(i=0;i<total;i++){
	   flag = ((i == sel)? MSG_KEYDOWN : MSG_KEYUP);
	   index = commonConfigList[i];	// 得到位置信息
	   x = xBase;
           y = yBase + i*34; 
	   RefreshButtonArea (hdc,&configurebuttonBkgnd,flag,x,y,100,32, index);
	}

     return sel;
}

static void initCommonConfigBkgnd(HDC hdc)
{
     FillBoxWithBitmap (hdc, 0, 0, 0, 0, &configureBkgnd); 	// 背景 
     RefreshCommonConfigListBkgnd(hdc,CommonConfigItem,currentConfig);  
}


static int commonConfigureDialogBoxProc(HWND hDlg, int message, WPARAM wParam,
							 LPARAM lParam)		
{    	
      HDC hdc;
      HWND  hwndClock = GetDlgItem(hDlg,IDC_TIME);
      char timeBuff[24];		// 时钟显示,当前位置显示
      int ret;
      switch (message) 
	{
    	case MSG_INITDIALOG:	
	    currentConfig = 0;
	    SendMessage(myIMEWindow,MSG_IME_SETSTATUS, 0, (LPARAM)0);
	    if(isIMEOpenInMotor == 1){
	        ShowWindow(myIMEWindow,SW_HIDE);
		isIMEOpenInMotor = 0;
	    }
	    SetWindowElementColorEx(hwndClock,FGC_CONTROL_NORMAL,
					RGB2Pixel(HDC_SCREEN, 255, 255, 255)); //White
	    SetTimer(hDlg,IDC_TIMER,50);	// 时钟显示
	    SetFocusChild(hwndClock);
	    return(1);
    	case MSG_TIMER:	
	    SetWindowText(hwndClock,convertSysTime(timeBuff));
	    return 0;          
    	case MSG_ERASEBKGND:
        {  
	    hdc = (HDC)wParam;
            const RECT* clip = (const RECT*) lParam;
            BOOL fGetDC = FALSE;
            RECT rcTemp; 
            if (hdc == 0) {
                hdc = GetClientDC (hDlg);
                fGetDC = TRUE;
            		}       
                    
            if (clip) {
                rcTemp = *clip;
                ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
                ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
                IncludeClipRect (hdc, &rcTemp);
            }

	    //初始化背景	        
	    initCommonConfigBkgnd(hdc);   
  
            if (fGetDC)
                ReleaseDC (hdc);
	    return 0;	
	}
    	case MSG_KEYDOWN:
	    switch(LOWORD(wParam)){
		case SCANCODE_ENTER:
		    ret = EnterCommonConfigDialog(hDlg,currentConfig);
		    if(ret){
			InvalidateRect(hDlg,NULL,TRUE);	// 引发窗口重绘	
 		    }
		    break;

		case SCANCODE_CURSORBLOCKUP:		// 向上选择
		    hdc=BeginPaint(hDlg);
		    // 单向选择
		    if(currentConfig>0){
		    	currentConfig--;
		    }else{
		   	currentConfig = 0;
 		    }
		    RefreshCommonConfigListBkgnd(hdc,CommonConfigItem,currentConfig);
		    EndPaint(hDlg,hdc);
		    break;
		case SCANCODE_CURSORBLOCKDOWN:	// 向下选择
		    hdc=BeginPaint(hDlg);
		    // 单向选择
		   if(currentConfig<CommonConfigItem-1){
		    	currentConfig++;
		    }else{
		   	currentConfig = CommonConfigItem-1;
 		    }
		    RefreshCommonConfigListBkgnd(hdc,CommonConfigItem,currentConfig);
		    EndPaint(hDlg,hdc);
		    break;
		case SCANCODE_CANCEL:	//   取消
		    SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
		    break;
            	case SCANCODE_HELP:	//帮助
		    MessageBox(hDlg,config_help[*curSysLang].help_config,
		  	config_help[*curSysLang].caption_help,MB_OK|MB_ICONINFORMATION);
		    break;
	    }			
	    break;

    	case MSG_COMMAND:
	    switch(wParam){
	    	case IDOK:
	    	case IDCANCEL:
		    KillTimer(hDlg,IDC_TIMER);
	    	    EndDialog(hDlg,IDOK);
		    break;
	    }
	    break;

	}
     return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void commonConfigureDialogBox (HWND hwnd)
{ 
   commonConfigureDlgBox.controls = configureCtrlsInit;
   DialogBoxIndirectParam (&commonConfigureDlgBox, hwnd,commonConfigureDialogBoxProc, 0L);
} 

int EnterConfigureCommon (HWND hwnd)
{
     if (LoadBitmap (HDC_SCREEN, &configureBkgnd, "./res/configBkgnd.jpg"))	// 主背景
         return 1;
     if(loadConfigCommonIcon())	// 配置选项
	return(1);
     if (LoadBitmap (HDC_SCREEN, &configureListBkgnd, "./res/configItemBkgnd.jpg"))// 配置界面背景
         return 1; 

    printf("Enter System Configure...\n");
    commonConfigureDialogBox(hwnd);		// Should not return except receive CLOSE MSG
    
    //printf("Unload configure res...\n");
    UnloadBitmap (&configureBkgnd);	// unload background
    UnloadBitmap (&configureListBkgnd);	// unload background
    UnloadBitmap (&configurebuttonBkgnd);	// unload background
    return 0;
 
}
