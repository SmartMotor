/*
 * src/program_edit.c
 *
 * The work window implementation
 *
 * Copyright 2007-2008 @ xianweizeng@gmail.com
 *
 * Author:
 *   Zeng Xianwei
 *
 *  File Coding system: gb2312
 *    set-buffer-file-coding-system
 *
 * ChangeLog:
 *   2008-09-21 split from userprogram.c
 *
 */


/********************************************************************/
static DLGTEMPLATE lableEditDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	8, 90, 350, 280,
	"lableEdit",
	0, 0,
	3, NULL,
	0
};

static CTRLDATA lableEditCtrlsInit[] =
{
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE|WS_BORDER,
		130, 40, 120, 30,
		IDC_LABLETOTALLEN,
		"",
		0
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE|WS_BORDER,
		130, 100, 120, 30,
		IDC_LABLELEN,
		"",
		0
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE|WS_BORDER,
		130, 160, 120, 30,
		IDC_UNUSEDLEN,
		"",
		0
	}
};

/********************************************************************/
static DLGTEMPLATE averageEditDlgBox =
{
	WS_BORDER ,
	WS_EX_NONE,
	8, 90, 350, 280,
	"averageEdit",
	0, 0,
	2, NULL,
	0
};

static CTRLDATA averageEditCtrlsInit[] =
{
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE|WS_BORDER,
		130, 40, 120, 30,
		IDC_HALFTOTALLEN,
		"",
		0
	},
	{
		CTRL_SLEDIT,
		WS_VISIBLE | SS_SIMPLE|WS_BORDER,
		130, 100, 120, 30,
		IDC_HALFNUMBER,
		"",
		0
	}
};


/*******************************************************************************************/



/* 根据输入的总长,标签长度和废边长度计算标签输入结果,
 * 并将结果保存到pProEdit结构的userProCuts数组中;
 * 参数:        pProEdit
 * 返回值:      有效刀数
 */
static int CountLableData(userProgramEditData *pProEdit)
{
	int frontLimit = getValidLimit();
	int totalLen = pProEdit->lableEdit.totalLen;
	int lableLen = pProEdit->lableEdit.lableLen;
	int unusedLen = pProEdit->lableEdit.unusedLen;

	int currentCut = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum;

	//printf("CountLableData...totalCuts=%d curCut=%d\n",totalCuts,currentCut);
	int cut = totalLen;
	if((currentCut<totalCuts)&&(currentCut<MAX_PROGRAM_CUTS)){
		pProEdit->userProCuts[currentCut++] = cut;
	}else {
		return totalCuts;
	}
	while(cut >= frontLimit){       //前极限
		cut = cut-lableLen;
		if((cut >= frontLimit) && (currentCut<MAX_PROGRAM_CUTS)){
			pProEdit->userProCuts[currentCut++] = cut;
		} else {
			break;
		}
		cut = cut-unusedLen;
		if((cut >= frontLimit) && (currentCut<MAX_PROGRAM_CUTS)){
			pProEdit->userProCuts[currentCut++] = cut;
		} else {
			break;
		}
	}

	if(currentCut >= totalCuts){    // 更新总刀数,当前刀数不变
		totalCuts = currentCut+1;
		pProEdit->userProCuts[totalCuts-1] = 0;// 最后一刀置为0
		pProEdit->userProCuts[totalCuts] = 0;   // 最后一刀置为0
	}

	pProEdit->nInputCutsNum = totalCuts;

	return totalCuts;

}

/* 根据输入的总长,等分计算标签输入结果,
 * 并将结果保存到pProEdit结构的userProCuts数组中;
 * 参数:        pProEdit
 * 返回值:      有效刀数
 */

static int CountHalfData(userProgramEditData *pProEdit)
{
	int frontLimit = getValidLimit();
	int totalLen = pProEdit->halfEdit.totalLen;
	int num = pProEdit->halfEdit.num;
	int currentCut = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum;
	int cutBase = totalLen / num ;  // 每次递减的尺寸
	int cut = totalLen;

	if((currentCut<totalCuts)&&(currentCut<MAX_PROGRAM_CUTS)){
		pProEdit->userProCuts[currentCut++] = cut;
	} else {
		return totalCuts;
	}
	while(cut >= frontLimit){       //前极限
		cut = cut-cutBase;
		if((cut >= frontLimit) && (currentCut<MAX_PROGRAM_CUTS)){
			pProEdit->userProCuts[currentCut++] = cut;
		} else {
			break;
		}
	}
	//printf("currentcut = %d \n",currentCut);

	if(currentCut >= totalCuts){    // 更新总刀数,当前刀数不变
		totalCuts = currentCut+1;
		//printf("totalCuts = %d \n",totalCuts);
		pProEdit->userProCuts[totalCuts-1] = 0;// 最后一刀置为0
		pProEdit->userProCuts[totalCuts] = 0;   // 最后一刀置为0
	}
	pProEdit->nInputCutsNum = totalCuts;
	return totalCuts;

}


/*
 * Label edit window
 */
static void initLableEditBkgnd(HDC hdc)
{
	FillBoxWithBitmap (hdc, 0, 0, 350, 280, &lableEditBkgnd);       // 背景
	RefreshBoxArea (hdc,&userProgramEditButton,30,30,80,40,1760,0); // 输入总长
	RefreshBoxArea (hdc,&userProgramEditButton,30,90,80,40,1840,0); // 输入标签
	RefreshBoxArea (hdc,&userProgramEditButton,30,150,80,40,1920,0);        // 输入废边
	RefreshBoxArea (hdc,&userProgramEditButton,10,220,80,40,0,0);   // F1
	RefreshBoxArea (hdc,&userProgramEditButton,90,220,80,40,3680,0);        // 完成
	RefreshBoxArea (hdc,&userProgramEditButton,180,220,80,40,160,0);        // F2
	RefreshBoxArea (hdc,&userProgramEditButton,260,220,80,40,1040,0);       // 退出标签

}
static int getLableEditData(HWND *hwndEdit,userProgramEditData *pProEdit)
{
	int index = 0;
	char cutDataBuf[10];
	int  cutDataLen=0;
	int ret;
	pProEdit->lableEdit.lableStatus = 0;
	pProEdit->lableEdit.totalLen = 0;
	pProEdit->lableEdit.lableLen = 0;
	pProEdit->lableEdit.unusedLen = 0;
	for(index=0;index<3;index++){
		memset(cutDataBuf,0,10);
		cutDataLen = GetWindowTextLength(hwndEdit[index]);      // 获得输入的字符
		GetWindowText(hwndEdit[index],cutDataBuf,cutDataLen);
		ret=isInputCutsDataValid(cutDataBuf,cutDataLen);        // 判断输入是否有效
		if(ret > 0 ){
			switch(index){
			case 0:
				pProEdit->lableEdit.totalLen = ret;
				pProEdit->lableEdit.lableStatus |= 0x1; //bit 0
				break;
			case 1:
				pProEdit->lableEdit.lableLen = ret;
				pProEdit->lableEdit.lableStatus |= 0x2;
				break;
			case 2:
				pProEdit->lableEdit.unusedLen = ret;
				pProEdit->lableEdit.lableStatus |= 0x4;
				break;
			}
		}else{
			SetWindowText(hwndEdit[index],"");              // 清空输入区
			SetFocusChild(hwndEdit[index]);
			//printf("getlabledata status=%d \n",pProEdit->lableEdit.lableStatus);
			return(-1);
		}
	}
	//pProEdit->lableEdit.lableStatus = 7;
	//printf("getlabledata status=%d \n",pProEdit->lableEdit.lableStatus);

	return(0);

}

static void lableTotalLenEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd,nextHwnd;
	int dataLen,ret;
	char dataBuf[10];
	mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
	nextHwnd = GetDlgItem(mainHwnd,IDC_LABLELEN);

	switch(nc){
	case EN_CHANGE:         // 有数字键按下
		dataLen = GetWindowTextLength(hwnd);    // 获得输入的字符
		if(dataLen > 7){                // 输入超出范围,clear
			SetFocusChild(hwnd);
			SetWindowText(hwnd,""); // 清空输入区
		}
		break;
	case EN_ENTER:          // Enter key 按下
		dataLen = GetWindowTextLength(hwnd);    // 获得输入的字符
		GetWindowText(hwnd,dataBuf,dataLen);
		ret=isInputCutsDataValid(dataBuf,dataLen);      // 判断输入是否有效
		if(ret > 0 ){                           // 输入有效,返回转换后的数据
			programEdit.lableEdit.totalLen = ret;
			programEdit.lableEdit.lableStatus |= 0x1;       //bit 0
			if(programEdit.lableEdit.lableStatus == 7){     // 输入完成
				CountLableData(&programEdit);           //计算结果
				SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0); //exit
			}else{
				SetFocusChild(nextHwnd);
			}
		}else {
			SetWindowText(hwnd,""); // 清除
		}
		break;
	}
}

static void lableLenEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd,nextHwnd;
	int dataLen,ret;
	char dataBuf[10];
	mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
	nextHwnd = GetDlgItem(mainHwnd,IDC_UNUSEDLEN);

	switch(nc){
	case EN_CHANGE:         // 有数字键按下
		dataLen = GetWindowTextLength(hwnd);    // 获得输入的字符
		if(dataLen > 7){                // 输入超出范围,clear
			SetFocusChild(hwnd);
			SetWindowText(hwnd,""); // 清空输入区
		}
		break;
	case EN_ENTER:          // Enter key 按下
		dataLen = GetWindowTextLength(hwnd);    // 获得输入的字符
		GetWindowText(hwnd,dataBuf,dataLen);
		ret=isInputCutsDataValid(dataBuf,dataLen);      // 判断输入是否有效
		if(ret > 0 ){                           // 输入有效,返回转换后的数据
			programEdit.lableEdit.lableLen = ret;
			programEdit.lableEdit.lableStatus |= 0x2;       //bit 1
			if(programEdit.lableEdit.lableStatus == 7){     // 输入完成
				CountLableData(&programEdit);           //计算结果
				SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0); //exit
			}else{
				SetFocusChild(nextHwnd);
			}
		}else {
			SetWindowText(hwnd,""); // 清除
		}
		break;
	}
}
static void lableUnusedLenEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd,nextHwnd;
	int dataLen,ret;
	char dataBuf[10];
	mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
	nextHwnd = GetDlgItem(mainHwnd,IDC_LABLETOTALLEN);

	switch(nc){
	case EN_CHANGE:         // 有数字键按下
		dataLen = GetWindowTextLength(hwnd);    // 获得输入的字符
		if(dataLen > 7){                // 输入超出范围,clear
			SetFocusChild(hwnd);
			SetWindowText(hwnd,""); // 清空输入区
		}
		break;
	case EN_ENTER:          // Enter key 按下
		dataLen = GetWindowTextLength(hwnd);    // 获得输入的字符
		GetWindowText(hwnd,dataBuf,dataLen);
		ret=isInputCutsDataValid(dataBuf,dataLen);      // 判断输入是否有效
		if(ret > 0 ){                           // 输入有效,返回转换后的数据
			programEdit.lableEdit.unusedLen = ret;
			programEdit.lableEdit.lableStatus |= 0x4;       //bit 2
			if(programEdit.lableEdit.lableStatus == 7){     // 输入完成
				CountLableData(&programEdit);           //计算结果
				SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0); //exit
			}else{
				SetFocusChild(nextHwnd);
			}
		}else {
			SetWindowText(hwnd,""); // 清除
		}
		break;
	}
}

static int enterLableEditDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndEdit[3],hwndTemp;;
	hwndEdit[0] = GetDlgItem(hDlg,IDC_LABLETOTALLEN);
	hwndEdit[1] = GetDlgItem(hDlg,IDC_LABLELEN);
	hwndEdit[2] = GetDlgItem(hDlg,IDC_UNUSEDLEN);

	switch (message)
	{
	case MSG_INITDIALOG:
		SetNotificationCallback(hwndEdit[0], lableTotalLenEditCallback);
		SetNotificationCallback(hwndEdit[1], lableLenEditCallback);
		SetNotificationCallback(hwndEdit[2], lableUnusedLenEditCallback);
		programEdit.lableEdit.lableStatus = 0;
		programEdit.lableEdit.totalLen = 0;
		programEdit.lableEdit.lableLen = 0;
		programEdit.lableEdit.unusedLen = 0;

		return(1);

	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initLableEditBkgnd(hdc);         //初始化背景
		SetFocusChild(hwndEdit[0]);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_F1:       //   完成标签
			getLableEditData(hwndEdit,&programEdit);
			if(programEdit.lableEdit.lableStatus == 7){
				CountLableData(&programEdit);           //计算结果
				SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0); //exit
			}
			break;
		case SCANCODE_F2:       //   取消标签
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_CURSORBLOCKDOWN:  //   向下
			hwndTemp = GetFocusChild(hDlg);
			if(hwndTemp == hwndEdit[0]){
				SetFocusChild(hwndEdit[1]);
			} else {
				SetFocusChild(hwndEdit[2]);
			}
			break;
		case SCANCODE_CURSORBLOCKUP:    //   向上
			hwndTemp = GetFocusChild(hDlg);
			if(hwndTemp == hwndEdit[2]){
				SetFocusChild(hwndEdit[1]);
			} else {
				SetFocusChild(hwndEdit[0]);
			}
			break;
		case SCANCODE_ENTER:    //   确定
			//SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_CANCEL:   //   取消
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		}
		break;

	case MSG_COMMAND:
		switch(wParam){
		case IDOK:
		case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;
	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void enterLableEditDialogBox (HWND hwnd)
{
	lableEditDlgBox.controls = lableEditCtrlsInit;
	DialogBoxIndirectParam (&lableEditDlgBox, hwnd,enterLableEditDialogBoxProc, 0L);
}
static int enterLableEdit (HWND hwnd)
{
	//printf("enterLableEdit().\n");
	enterLableEditDialogBox(hwnd);          // Should not return except receive CLOSE MSG
	return 0;

}

/*******************************************************************************************/
/*
 * Average edit window
 */
static void initAverageEditBkgnd(HDC hdc)
{
	FillBoxWithBitmap (hdc, 0, 0, 350, 280, &lableEditBkgnd);       // 背景
	RefreshBoxArea (hdc,&userProgramEditButton,30,30,80,40,1760,0); // 输入总长
	RefreshBoxArea (hdc,&userProgramEditButton,30,90,80,40,2000,0); // 输入等分
	RefreshBoxArea (hdc,&userProgramEditButton,10,220,80,40,0,0);   // F1
	RefreshBoxArea (hdc,&userProgramEditButton,90,220,80,40,3680,0);        // 完成
	RefreshBoxArea (hdc,&userProgramEditButton,180,220,80,40,160,0);        // F2
	RefreshBoxArea (hdc,&userProgramEditButton,260,220,80,40,1200,0);       // 退出等分

}
static int getAverageEditData(HWND *hwndEdit,userProgramEditData *pProEdit)
{
	int index = 0;
	char cutDataBuf[10];
	int  cutDataLen=0;
	int ret;
	pProEdit->halfEdit.halfStatus = 0;
	pProEdit->halfEdit.totalLen = 0;
	pProEdit->halfEdit.num = 0;
	for(index=0;index<2;index++){
		memset(cutDataBuf,0,10);
		cutDataLen = GetWindowTextLength(hwndEdit[index]);      // 获得输入的字符
		GetWindowText(hwndEdit[index],cutDataBuf,cutDataLen);
		ret=isInputCutsDataValid(cutDataBuf,cutDataLen);        // 判断输入是否有效
		if(ret > 0 ){
			switch(index){
			case 0:
				pProEdit->halfEdit.totalLen = ret;
				pProEdit->halfEdit.halfStatus |= 0x1;   //bit 0
				break;
			case 1:
				pProEdit->halfEdit.num = ret/100;
				pProEdit->halfEdit.halfStatus |= 0x2;
				break;
			}
		}else{
			SetWindowText(hwndEdit[index],"");              // 清空输入区
			SetFocusChild(hwndEdit[index]);
			//printf("getlabledata status=%d \n",pProEdit->lableEdit.lableStatus);
			return(-1);
		}
	}
	//pProEdit->lableEdit.lableStatus = 3;
	//printf("getlabledata status=%d \n",pProEdit->lableEdit.lableStatus);

	return(0);

}

static void averageTotalLenEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd,nextHwnd;
	int dataLen,ret;
	char dataBuf[10];
	mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
	nextHwnd = GetDlgItem(mainHwnd,IDC_HALFNUMBER);

	switch(nc){
	case EN_CHANGE:         // 有数字键按下
		dataLen = GetWindowTextLength(hwnd);    // 获得输入的字符
		if(dataLen > 7){                // 输入超出范围,clear
			SetFocusChild(hwnd);
			SetWindowText(hwnd,""); // 清空输入区
		}
		break;
	case EN_ENTER:          // Enter key 按下
		dataLen = GetWindowTextLength(hwnd);    // 获得输入的字符
		GetWindowText(hwnd,dataBuf,dataLen);
		ret=isInputCutsDataValid(dataBuf,dataLen);      // 判断输入是否有效
		if(ret > 0 ){                           // 输入有效,返回转换后的数据
			programEdit.halfEdit.totalLen = ret;
			programEdit.halfEdit.halfStatus |= 0x1; //bit 0
			if(programEdit.halfEdit.halfStatus == 3){       // 输入完成
				CountHalfData(&programEdit);            //计算结果
				SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0); //exit
			}else{
				SetFocusChild(nextHwnd);
			}
		}else {
			SetWindowText(hwnd,""); // 清除
		}
		break;
	}
}

static void averageNumEditCallback(HWND hwnd,int id,int nc,DWORD add_data)
{
	HWND mainHwnd,nextHwnd;
	int dataLen,ret;
	char dataBuf[10];
	mainHwnd = GetMainWindowHandle(hwnd); //得到父窗口句柄
	nextHwnd = GetDlgItem(mainHwnd,IDC_HALFTOTALLEN);

	switch(nc){
	case EN_CHANGE:         // 有数字键按下
		dataLen = GetWindowTextLength(hwnd);    // 获得输入的字符
		if(dataLen > 5){                // 输入超出范围,clear
			SetFocusChild(hwnd);
			SetWindowText(hwnd,""); // 清空输入区
		}
		break;
	case EN_ENTER:          // Enter key 按下
		dataLen = GetWindowTextLength(hwnd);    // 获得输入的字符
		GetWindowText(hwnd,dataBuf,dataLen);
		ret=isInputCutsDataValid(dataBuf,dataLen);      // 判断输入是否有效
		if((ret > 0) && (ret %100 == 0) ){      // 等分数必须为整数
			programEdit.halfEdit.num = ret/100;
			programEdit.halfEdit.halfStatus |= 0x2; //bit 1
			if(programEdit.halfEdit.halfStatus == 3){       // 输入完成
				CountHalfData(&programEdit);            //计算结果
				SendMessage(mainHwnd,MSG_COMMAND,IDCANCEL,0); //exit
			}else{
				SetFocusChild(nextHwnd);
			}
		}else {
			SetWindowText(hwnd,""); // 清除
		}
		break;
	}
}


static int enterAverageEditDialogBoxProc(HWND hDlg, int message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HWND hwndEdit[2];
	hwndEdit[0] = GetDlgItem(hDlg,IDC_HALFTOTALLEN);
	hwndEdit[1] = GetDlgItem(hDlg,IDC_HALFNUMBER);

	switch (message)
	{
	case MSG_INITDIALOG:
		SetNotificationCallback(hwndEdit[0], averageTotalLenEditCallback);
		SetNotificationCallback(hwndEdit[1], averageNumEditCallback);
		programEdit.halfEdit.halfStatus = 0;
		programEdit.halfEdit.totalLen = 0;
		programEdit.halfEdit.num = 0;

		return(1);

	case MSG_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		const RECT* clip = (const RECT*) lParam;
		BOOL fGetDC = FALSE;
		RECT rcTemp;
		if (hdc == 0) {
			hdc = GetClientDC (hDlg);
			fGetDC = TRUE;
		}

		if (clip) {
			rcTemp = *clip;
			ScreenToClient (hDlg, &rcTemp.left, &rcTemp.top);
			ScreenToClient (hDlg, &rcTemp.right, &rcTemp.bottom);
			IncludeClipRect (hdc, &rcTemp);
		}

		initAverageEditBkgnd(hdc);       //初始化背景
		SetFocusChild(hwndEdit[0]);

		if (fGetDC)
			ReleaseDC (hdc);
		return 0;
	}
	case MSG_KEYDOWN:
		switch(LOWORD(wParam)){
		case SCANCODE_F1:       //   完成等分
			getAverageEditData(hwndEdit,&programEdit);
			if(programEdit.halfEdit.halfStatus == 3){
				CountHalfData(&programEdit);            //计算结果
				SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0); //exit
			}
			break;
		case SCANCODE_F2:       //   取消等分
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		case SCANCODE_CURSORBLOCKDOWN:  //   向下
			SetFocusChild(hwndEdit[1]);
			break;
		case SCANCODE_CURSORBLOCKUP:    //   向上
			SetFocusChild(hwndEdit[0]);
			break;
		case SCANCODE_CANCEL:   //   取消
			SendMessage(hDlg,MSG_COMMAND,IDCANCEL,0);
			break;
		}
		break;
	case MSG_COMMAND:
		switch(wParam){
		case IDOK:
		case IDCANCEL:
			EndDialog(hDlg,IDOK);
			break;
		}
		break;
	}
	return DefaultDialogProc (hDlg, message, wParam, lParam);
}

static void enterAverageEditDialogBox (HWND hwnd)
{
	averageEditDlgBox.controls = averageEditCtrlsInit;
	DialogBoxIndirectParam (&averageEditDlgBox, hwnd,enterAverageEditDialogBoxProc, 0L);
}
static int enterAverageEdit (HWND hwnd)
{
	//printf("enterLableEdit().\n");
	enterAverageEditDialogBox(hwnd);                // Should not return except receive CLOSE MSG
	return 0;

}

/*******************************************************************************************/
/* data edit functions
 *
 */


/*  正常编辑一刀数据,追加到文件末尾
 *  分两步: 1 修改当前0.0数据;
 *         2  追加一个0.0数据;
 */
static int editOneCutData(HWND hwnd,userProgramEditData *pProEdit,int data)
{
	int frontLimit = getValidLimit();
	int currentSel = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum ;       // 新增一刀数据

	printf("total=%d current=%d \n",totalCuts,currentSel);

	if(currentSel != totalCuts-1){
		currentSel = totalCuts-1;       // 只能在最后一刀中编辑输入
	}

	// 判断数据是否有效
	if((data < frontLimit) || (data > currentuser->backlimit)){     // 数据溢出
		MessageBox(hwnd,userpro_help[*curSysLang].msg_cutdata_overflow,
			   userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
		return totalCuts;
	}

	printf("total=%d current=%d \n",totalCuts,currentSel);


	// 有效则更新结构体数据
	pProEdit->currentCutsNum = currentSel;
	pProEdit->userProCuts[currentSel] = data;
	UpdateProListWithData(hwnd,pProEdit,0); // 修改,type=0

	// 显示下一刀0.0数据
	currentSel = currentSel + 1;
	totalCuts = totalCuts +1;
	pProEdit->userProCuts[currentSel] = 0;
	// 更新结构体
	pProEdit->nInputCutsNum = totalCuts;
	pProEdit->currentCutsNum = currentSel;
	// 更新列表
	UpdateProListWithData(hwnd,pProEdit,1); // 追加,type=1

	/* UpdateProListWithFileData(hwnd, pProEdit); */

	dumpProgramData(pProEdit);

	return totalCuts;

}

/*    比较输入
 *    flag = 0 : 加   1 : 减
 */
static int compareAddDecInput(HWND hwnd,userProgramEditData *pProEdit,int ret,int flag)
{
	int data;
	int frontLimit = getValidLimit();
	int currentSel = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum ;       // 新增一刀数据
	//printf("total=%d current=%d \n",totalCuts,currentSel);
	if(currentSel != totalCuts-1){
		currentSel = totalCuts-1;       // 只能在最后一刀中编辑输入
	}
	if(!flag){
		data = pProEdit->userProCuts[totalCuts - 2] + ret;
	}else{
		data = pProEdit->userProCuts[totalCuts - 2] - ret;
	}

	// 判断数据是否有效
	if((data < frontLimit) || (data > currentuser->backlimit)){     // 数据溢出
		ResetUserProgramEdit(hwnd,pProEdit,0);
		MessageBox(hwnd,userpro_help[*curSysLang].msg_cutdata_overflow,
			   userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
		return totalCuts;
	}
	// 有效则更新结构体数据
	pProEdit->currentCutsNum = currentSel;
	pProEdit->userProCuts[currentSel] = data;
	UpdateProListWithData(hwnd,pProEdit,0); // 修改,type=0

	// 显示下一刀0.0数据
	currentSel = currentSel + 1;
	totalCuts = totalCuts +1;
	pProEdit->userProCuts[currentSel] = 0;
	// 更新结构体
	pProEdit->nInputCutsNum = totalCuts;
	pProEdit->currentCutsNum = currentSel;
	// 更新列表
	UpdateProListWithData(hwnd,pProEdit,1); // 追加,type=1

	ResetUserProgramEdit(hwnd,pProEdit,0);
	return totalCuts;


}


/* modify the current data
 *   read the current input data and modify to current selected data
 *   in listbox
 */
static int modifyOneCutData(HWND hDlg,userProgramEditData *pProEdit)
{
	HWND hwndInputEdit = GetDlgItem(hDlg,IDC_INPUTCUTDATA);
	int frontLimit = getValidLimit();
	int currentSel = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum;
	int tmp;
	char cutDataBuf[10];
	int  cutDataLen=0;
	int data;

	if(currentSel == totalCuts -1){ // ,最后一刀为0的数据
		SetWindowText(hwndInputEdit,"");
		return(-1);
	}
	// 读出数据
	cutDataLen = GetWindowTextLength(hwndInputEdit);        // 获得输入的字符
	memset(cutDataBuf,0,10);
	GetWindowText(hwndInputEdit,cutDataBuf,cutDataLen);
	//memset(programEdit.inputbuf,0,10);
	//strncpy(programEdit.inputbuf,cutDataBuf,cutDataLen);
	//printf("%s len=%d\n",cutDataBuf,cutDataLen);
	data=isInputCutsDataValid(cutDataBuf,cutDataLen);       // 判断输入是否有效
	if(data < 0){
		SetWindowText(hwndInputEdit,"");
		return -1;
	}

	// 判断数据是否有效
	if((data < frontLimit) || (data > currentuser->backlimit)){     // 数据溢出
		MessageBox(hDlg,userpro_help[*curSysLang].msg_cutdata_overflow,
			   userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
		SetWindowText(hwndInputEdit,"");
		return totalCuts;
	}

	tmp = pProEdit->userProCuts[currentSel] & 0xff000000;   // 不改变状态标志
	tmp |= data;
	pProEdit->userProCuts[currentSel] = tmp;
	// 更新列表
	UpdateProListWithData(hDlg,pProEdit,0);
	// 插入后退出插入功能
	ResetUserProgramEdit(hDlg,pProEdit,0);
	SetWindowText(hwndInputEdit,"");

	return totalCuts;

}

/*  insert one data
 *
 */
static int insertOneCutData(HWND hDlg,userProgramEditData *pProEdit)
{
	HWND hwndInputEdit = GetDlgItem(hDlg,IDC_INPUTCUTDATA);
	int frontLimit = getValidLimit();
	int currentSel = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum;
	char cutDataBuf[10];
	int  cutDataLen=0;
	int data;
	int index;

	if(currentSel == totalCuts -1){  /* the last one */
		SetWindowText(hwndInputEdit,"");
		return(-1);
	}
	// 读出数据
	cutDataLen = GetWindowTextLength(hwndInputEdit);        // 获得输入的字符
	memset(cutDataBuf,0,10);
	GetWindowText(hwndInputEdit,cutDataBuf,cutDataLen);
	//memset(programEdit.inputbuf,0,10);
	//strncpy(programEdit.inputbuf,cutDataBuf,cutDataLen);
	//printf("%s len=%d\n",cutDataBuf,cutDataLen);
	data=isInputCutsDataValid(cutDataBuf,cutDataLen);       // 判断输入是否有效
	if(data < 0){
		SetWindowText(hwndInputEdit,"");
		return -1;
	}

	// 判断数据是否有效
	if((data < frontLimit) || (data > currentuser->backlimit)){     // 数据溢出
		MessageBox(hDlg,userpro_help[*curSysLang].msg_cutdata_overflow,
			   userpro_help[*curSysLang].caption_waring,MB_OK|MB_ICONINFORMATION);

		//SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_DELETESTRING,currentSel,0);
		//SendDlgItemMessage(hwnd,IDL_PROGRAMCUTSLIST,LB_SETCURSEL,currentSel,0);
		SetWindowText(hwndInputEdit,"");
		//ResetUserProgramEdit(hwnd,pProEdit,0);        // Clear Flag
		//InvalidateRect(hwnd,NULL,TRUE);       // 引发窗口重绘
		return totalCuts;
	}
	totalCuts = totalCuts + 1;
	for(index=totalCuts-1;index>currentSel;index--){
		pProEdit->userProCuts[index] = pProEdit->userProCuts[index-1];
	}

	//pProEdit->userProCuts[currentSel] = 0;
	pProEdit->nInputCutsNum=totalCuts;

	pProEdit->userProCuts[currentSel] = data;
	// 更新列表
	UpdateProListWithFileData(hDlg,pProEdit);
	// 插入后退出插入功能
	//ResetUserProgramEdit(hDlg,pProEdit,0);
	SetWindowText(hwndInputEdit,"");

	return totalCuts;

}

static int deleteOneCutData(HWND hwnd,userProgramEditData *pProEdit)
{
	int currentSel = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum-1;      // 删除后总刀数
	int index;

	if(totalCuts == 0){             // 溢出处理
		pProEdit->nInputCutsNum = 1;
		pProEdit->userProCuts[totalCuts] = 0;
		pProEdit->userEditStatus = 0;
		UpdateProListWithFileData(hwnd,pProEdit);

		return(1);
	}
	int ret=MessageBox(hwnd,userpro_help[*curSysLang].msg_delcutdata_confirm,
			   userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
	switch(ret){
	case IDOK:              // 确认删除
				// 循环将数据后移一刀
		for(index=currentSel;index<totalCuts;index++){
			pProEdit->userProCuts[index] = pProEdit->userProCuts[index+1];
		}
		pProEdit->userProCuts[totalCuts] = 0;   // 清除最后一刀数据
		pProEdit->nInputCutsNum=totalCuts;
		// 刷新列表
		UpdateProListWithFileData(hwnd,pProEdit);
		break;
	}

	pProEdit->userEditStatus = 0;
	return totalCuts;

}

/*       Delete all cut data behind current selection
 */
static int deleteBehindCutDatas(HWND hwnd,userProgramEditData *pProEdit)
{
	int currentSel = pProEdit->currentCutsNum;
	int totalCuts = pProEdit->nInputCutsNum;        // 删除前总刀数
	int ret=MessageBox(hwnd,userpro_help[*curSysLang].msg_delalldata_confirm,
			   userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
	switch(ret){
	case IDOK:              // 确认删除
		memset(&(pProEdit->userProCuts[currentSel]),0,totalCuts-currentSel);    //清空数据
		pProEdit->nInputCutsNum=currentSel+1;           // 显示0.0
		//pProEdit->currentCutsNum = 0;
		// 刷新列表
		UpdateProListWithFileData(hwnd,pProEdit);
		break;

	case IDCANCEL:
		break;
	}

	pProEdit->userEditStatus = 0;
	return 1;

}

/*       删除所有 数据
 */
static int deleteAllCutDatas(HWND hwnd,userProgramEditData *pProEdit)
{

	int ret=MessageBox(hwnd,userpro_help[*curSysLang].msg_delalldata_confirm,
			   userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
	switch(ret){
	case IDOK:              // 确认删除
				// 循环将数据后移一刀
		memset(pProEdit->userProCuts,0,sizeof(pProEdit->userProCuts));  //清空数据
		pProEdit->nInputCutsNum=1;              // 显示0.0
		pProEdit->currentCutsNum = 0;
		// 刷新列表
		UpdateProListWithFileData(hwnd,pProEdit);
		break;

	case IDCANCEL:
		break;
	}

	pProEdit->userEditStatus = 0;
	return 1;

}

static int deleteOneData(int currentSel,int totalCuts,userProgramEditData *pProEdit)
{
	int index;

	for(index=currentSel;index<totalCuts;index++){
		pProEdit->userProCuts[index] = pProEdit->userProCuts[index+1];
	}
	pProEdit->userProCuts[totalCuts-1] = 0; // 清除最后一刀数据
	return totalCuts;

}
/*       微调所有数据(如何保证数据的有效性?)
 */
static int justAllCutDatas(HWND hwnd,userProgramEditData *pProEdit,int data)
{
	int frontLimit = getValidLimit();
	int totalCuts = pProEdit->nInputCutsNum;        // 总刀数
	int index,action;
	int tmp,overNum = 0;
	int ret,saveFlag = 1;

	action = pProEdit->justEdit.action;

	switch(action){
	case 0:         // reset
		ResetUserProgramEdit(hwnd,pProEdit,3);
		return(0);
	case 1:         //  加
		for(index=0;index<totalCuts-1;index++){ // 先扫描一遍,看看是否超出极限
			tmp = pProEdit->userProCuts[index] + data;
			if((tmp < frontLimit) || (tmp > currentuser->backlimit)){
				overNum++;
			}
		}
		if(overNum > 0){        // 有数据异常
			ret = MessageBox(hwnd,userpro_help[*curSysLang].msg_just_confirm,
					 userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
			if(ret == IDCANCEL){
				saveFlag = 0;
			}
		}
		if(saveFlag){           // 计算存储
			index = 0;
			while(index<totalCuts-1){
				pProEdit->userProCuts[index] = pProEdit->userProCuts[index] + data;
				tmp = pProEdit->userProCuts[index];
				if((tmp < frontLimit) || (tmp > currentuser->backlimit)){
					deleteOneData(index,totalCuts,pProEdit);
					totalCuts--;
				} else {
					index++;
				}
			}
		}
		break;
	case 2:         // 减
		for(index=0;index<totalCuts-1;index++){ // 先扫描一遍,看看是否超出极限
			tmp = pProEdit->userProCuts[index] - data;
			if((tmp < frontLimit) || (tmp > currentuser->backlimit)){
				overNum++;
			}
		}
		if(overNum > 0){        // 有数据异常
			ret = MessageBox(hwnd,userpro_help[*curSysLang].msg_just_confirm,
					 userpro_help[*curSysLang].caption_waring,MB_OKCANCEL|MB_ICONINFORMATION);
			if(ret == IDCANCEL){
				saveFlag = 0;
			}
		}
		if(saveFlag){           // 计算存储
			index = 0;
			while(index<totalCuts-1){
				pProEdit->userProCuts[index] = pProEdit->userProCuts[index] - data;
				tmp = pProEdit->userProCuts[index];
				if((tmp < frontLimit) || (tmp > currentuser->backlimit)){
					deleteOneData(index,totalCuts,pProEdit);
					totalCuts--;
				} else {
					index++;
				}
			}
		}
		break;
	}

	pProEdit->nInputCutsNum = totalCuts;
	programEdit.justEdit.action = 0;

	InvalidateRect(hwnd,NULL,TRUE); // 引发窗口重绘
	ResetUserProgramEdit(hwnd,pProEdit,3);  // 复位
	UpdateProListWithFileData(hwnd,pProEdit);       // 刷新列表

	return(totalCuts);
}

static void changePusherFlagOfData(HWND hwnd,userProgramEditData *pProEdit)
{
	int currentSel = pProEdit->currentCutsNum;

	pProEdit->userProCuts[currentSel] ^=  PUSHER_FLAG;      // Reverse PRESS_FLAG
	pProEdit->userProCuts[currentSel] &=  ~AUTOCUT_FLAG;    // Clear AUTOCUT_FLAG
	pProEdit->userProCuts[currentSel] &=  ~PRESS_FLAG;      // Clear PUSHER_FLAG

	UpdateProListItemFlags(hwnd, pProEdit);
}


static void changeAirFlagOfData(HWND hwnd,userProgramEditData *pProEdit)
{
	int currentSel = pProEdit->currentCutsNum;

	pProEdit->userProCuts[currentSel] ^=  AIR_FLAG; // Reverse AIR_FLAG
	UpdateProListItemFlags(hwnd, pProEdit);
}

static void changePressSheetFlagOfData(HWND hwnd,userProgramEditData *pProEdit)
{
	int currentSel = pProEdit->currentCutsNum;

	pProEdit->userProCuts[currentSel] ^=  PRESS_FLAG;       // Reverse PRESS_FLAG
	pProEdit->userProCuts[currentSel] &=  ~AUTOCUT_FLAG;    // Clear AUTOCUT_FLAG
	pProEdit->userProCuts[currentSel] &=  ~PUSHER_FLAG;     // Clear PUSHER_FLAG

	UpdateProListItemFlags(hwnd, pProEdit);

}


/*  增加自动切刀标志,须满足两个条件:
 *  1. 该数据比前一刀数据小
 *  2. 第一刀数据不能加
 */
static void addAutoCutFlagToData(HWND hwnd,userProgramEditData *pProEdit)
{
	int currentSel = pProEdit->currentCutsNum;
	int foredata;
	int curdata = pProEdit->userProCuts[currentSel];

	if(currentSel == 0){
		releaseSecondFunction(hwnd);            // 刷新第二功能标志
		return;
	}
	// Now currentSel >= 1
	foredata = pProEdit->userProCuts[currentSel-1] & 0x00ffffff;    // 取出数据比较
	curdata = pProEdit->userProCuts[currentSel] & 0x00ffffff;
	if(curdata < foredata){
		pProEdit->userProCuts[currentSel] |=  AUTOCUT_FLAG;
		pProEdit->userProCuts[currentSel] &=  ~PRESS_FLAG;      // Clear PRESS_FLAG
		pProEdit->userProCuts[currentSel] &=  ~PUSHER_FLAG;     // Clear PUSHER_FLAG
		UpdateProListItemFlags(hwnd,pProEdit);                  /* refresh flag */

	}
	releaseSecondFunction(hwnd);            // 刷新第二功能标志

}

static void delAutoCutFlagFromData(HWND hwnd,userProgramEditData *pProEdit)
{
	int currentSel = pProEdit->currentCutsNum;
	pProEdit->userProCuts[currentSel] &=  ~AUTOCUT_FLAG;
	releaseSecondFunction(hwnd);            // 刷新第二功能标志
	UpdateProListItemFlags(hwnd,pProEdit);                  /* refresh flag */

}

static void addAutoCutFlagToAllDatas(HWND hwnd,userProgramEditData *pProEdit)
{
	int totalCuts = pProEdit->nInputCutsNum;        // 总刀数
	int index;
	int foredata,curdata;
	if(totalCuts == 1){
		releaseSecondFunction(hwnd);            // 刷新第二功能标志
		return;
	}
	// Now totalCuts > 1
	for(index=1;index<totalCuts-1;index++){ // 第一刀数据不能加,最后一刀一直为0,不用加
		foredata = pProEdit->userProCuts[index-1] & 0x00ffffff; // 取出数据比较
		curdata = pProEdit->userProCuts[index] & 0x00ffffff;
		if(curdata < foredata){
			pProEdit->userProCuts[index] |=  AUTOCUT_FLAG;
			pProEdit->userProCuts[index] &=  ~PRESS_FLAG;   // Clear PRESS_FLAG
			pProEdit->userProCuts[index] &=  ~PUSHER_FLAG;  // Clear PUSHER_FLAG
		}
	}

	releaseSecondFunction(hwnd);            // 刷新第二功能标志

	/* refresh all item flag */
	UpdateProListAllItemFlags(hwnd, pProEdit);
}

/* 删除所由自动切刀标志 */
static void delAutoCutFlagFromAllDatas(HWND hwnd,userProgramEditData *pProEdit)
{
	int totalCuts = pProEdit->nInputCutsNum;        // 总刀数
	int index;

	// Now totalCuts > 1
	for(index=0;index<totalCuts;index++){           // 不论有无都同样处理
		pProEdit->userProCuts[index] &=  ~AUTOCUT_FLAG;
	}

	releaseSecondFunction(hwnd);            // 刷新第二功能标志

	/* refresh all item flag */
	UpdateProListAllItemFlags(hwnd, pProEdit);

}
