/*
 *  src/userprogram.h
 *
 *  Copyright 2008 @ xianweizeng@gmail.com
 *
 */


#ifndef _SRC_USERPROGRAM_H_
#define _SRC_USERPROGRAM_H_

#define MAX_PROGRAM_CUTS	999	/* Max 999 cut datas in one program */
#define LIMIT_FRONT		3000	/* default front limit: 30.00 */

/* lable edit structure */
struct userProgramEditTypeLable_t{
	int totalLen;		/* total length */
	int lableLen;		/* lable length */
	int unusedLen;		/* unused length */
        int lableStatus;	/* edit status: 0: Not yet input */
                                /*              1: Total length input done */
                                /*              2: Lable length input done */
                                /*              3: Unused length input done */
};

/* Average edit structure */
struct userProgramEditTypeHalf_t{
	int totalLen;		/* total length */
	int num;		/* divided numbers */
	int halfStatus;		/* edit status: 0: Not yet input */
				/*              1: Total length input done */
	                        /*              2: num input done */
};


/* just edit */
struct userProgramEditTypeJust_t{
	int action;		/* operation type: 0 exit 1 add 2 sub */
	int justLen;		/* length to just */
};

/* Main data structure used in program edit window */
typedef struct userProgramEditData_t {
	int  isProNameNull;		// 程序名是否为空,即使不输入程序名,也必须按回车确认
	
					// 进入该画面,焦点放到程序名输入,此时按回车即可
	int  userEditStatus;			// 0  正常编辑,一刀一刀输入
					// 1  标签
					// 2  等分
					// 3  修改数据
					// 4  插入
					// 5  删除
					// 6  微调 
					// 7  示教
					// 8  加
					// 9  减
	struct userProgramEditTypeLable_t lableEdit;
	struct userProgramEditTypeHalf_t  halfEdit;
	struct userProgramEditTypeJust_t  justEdit;
	
	int editFlag;			// 编辑标志, 2006-09-09增加
	int userProCuts[MAX_PROGRAM_CUTS];
	int currentCutsNum;			// 当前正在编辑的刀数,此为工作指针 0 为第一刀
	int nInputCutsNum;			// 已输入的刀数 >= 1,包含最后一刀0.0
					// 约束条件: currentCutsNum <= nInputCutsNum	
	unsigned char inputbuf[10];			// 输入缓从区

}userProgramEditData;


/*   定义程序运行状态结构体
 */
typedef struct userProgramRunStatus_t{
	int runStatus;	// 自动运行标志 0 输入模式 1 自动 2 手动 --3 自动切刀(自刀)
	int runStop;	// 运行/停止标志 0 停止 1 运行
 	int pusher;	// 推纸器标志
	int secondFunc;	// 第二功能标志(应该放到编辑结构体中)
	int destCutNum;	// 自动模式下切刀要运行到的刀数;
	int whichLimit;	// 哪个极限有效 0 前极限 1 后极限

	int cutStatus;	// 切刀当前状态  0 刀上   1 刀下
	int cutActTimes;	// 动作次数,一个循环2次动作
	int pusherStatus;	// 推纸器当前状态	 0 上  1 下
	int pusherActTimes;

	int cutPusherActTimes;	// 用于判断一刀是否完成

	int machineStop;	// 下位机发送的机器停止状态标志, 0 运行 1 停止
  	int isOneCutDone;	// 一次动作是否完成,用于判断是否进行下一次裁切  0 No  1 Yes
			// 判断是否完成的算法为: 
	int curDestPos;	// 当前运行的目标位置
	int lastCutPos;	// 上一刀切刀切下位置
	int forCutLen;	// 刀前尺寸

	int mannualFlag;	// 手动运行加减标志, 0 正常手动运行 1 当前尺寸加 2 当前尺寸减

	
}userProgramRunStatus;


extern userProgramEditData programEdit;		// 程序编辑全局变量

void refreshCurrentPos(HWND hwnd,int data);
int isInputCutsDataValid(char *pBuf,int len);
int EnterUserProgram(void);
#endif
