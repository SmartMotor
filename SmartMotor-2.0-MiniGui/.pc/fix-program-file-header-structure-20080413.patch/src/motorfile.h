/*
 *  motorfile.h
 *
 *  Copyright 2008 @ xianweizeng@gmail.com
 *
 */
#ifndef _MOTORFILE_H_
#define _MOTORFILE_H_

/* porgram file descriptor */
struct programe_t{
	int fileflags;          /* fileflags */
	int isnull;             /* this file contain NO data */
	int id;			/* id, file number */
	char introduction[20];  /* file description */
	char recent_date[12];   /* last modified/used date */
	int times;              /* used times */
	int first;              /* fisrt data */
};

/* defiend in motorregister.c */
extern struct programe_t programe;

/* file link table structure */
struct programe_link_t{
	struct programe_t prog;       /* porgram file descriptor */
	struct programe_link_t *next;
};

int loadProgramFile(int id, struct fromProListToPro_t *pFileData);
int EnterProListDialog(void);

#endif /* _MOTORFILE_H_ */
