/*
 * SpeedMeter.cpp
 *
 *  Created on: Apr 20, 2010
 *      Author: souken-i
 */

#include "SpeedMeter.h"
#include "Global.h"

SpeedMeter::SpeedMeter(QWidget *parent)
	:ManoMeter(parent)
{
	setMaximum(2000);
	setMinimum(0);
	setNominal(400);
	setCritical(1600);
	QString unit(tr("[RPM]"));
	setSuffix(unit);

	setValue(gMotor->getSpeed());
	resize(120, 120);

	// FIXME: should we do it here or in the MainFrame?
	connect(gMotor, SIGNAL(speedChanged(int)), this, SLOT(updateSpeed(int)));
}

SpeedMeter::~SpeedMeter() {
	// TODO Auto-generated destructor stub
}

void SpeedMeter::updateSpeed(int speed)
{
	setValue(speed);
}
