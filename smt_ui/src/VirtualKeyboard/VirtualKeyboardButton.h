/***************************************************************************
 *   Copyright (C) 2008 by Giovanni Romano                                 *
 *   Giovanni.Romano.76@gmail.com                                          *
 *                                                                         *
 *   This program may be distributed under the terms of the Q Public       *
 *   License as defined by Trolltech AS of Norway and appearing in the     *
 *   file LICENSE.QPL included in the packaging of this file.              *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 ***************************************************************************/

#ifndef _QKEYPUSHBUTTON_H
#define _QKEYPUSHBUTTON_H

#include <QtGui>
#include "PushButton.h"

#define DEFAULT_STYLE_BUTTON       "font-size:12px; border: 1px solid #8f8f91;border-radius:5px;"
#define DEFAULT_BACKGROUND_BUTTON	"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #f6f7fa, stop: 1 #dadbde);"
#define CHANGED_BACKGROUND_BUTTON	"background-color:qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 lightblue, stop: 1 lightblue);"
#define STYLE_FOCUS_TEXT           "border: 1px solid red"

#define KEY_WIDTH              48
#define KEY_HEIGHT             48

/**
 * A key on VirtualKeyboard.
 */
class VirtualKeyboardButton : public PushButton {
	Q_OBJECT

public:
	/**
	 * Create a key.
	 * A key has 4 elements: keyID ,unicode value, face, icon, defined by the first 4 parameters.
	 *  @keyId: One value of Qt::Key. The only representation of the key.
	 *  @c:     The unicode value of the key. Used in QWSServer::sendKeyEvent(). If not specified, it will be calculated from @keyId.
	 *  @face:  The string printed above the key. If not specified and no icon is specified, the same as @c.
	 *  @icon:  The icon set on the button.
	 *  @parent: The parent widget.
	 */
	VirtualKeyboardButton(int keyID, int c = -1, QString face = "", QString iconPath = "", QWidget *parent = 0);
	int keyId() { return _keyId; }
	void setKeyId (int id) { _keyId = id; }
	int character() { return _character; }
	void setCharacter(int c) { _character = c; }
	QString &face() { return _face; }
	void setFace(const QString &f) {
		_face = f;
		setText(_face);
	}

private:
	int			_keyId;
	int 			_character; /*< unicode value */
	QString 		_face;
};

#endif // _QKEYPUSHBUTTON_H
