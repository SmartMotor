/***************************************************************************
 *   Copyright (C) 2008 by Giovanni Romano                                 *
 *   Giovanni.Romano.76@gmail.com                                          *
 *                                                                         *
 *   This program may be distributed under the terms of the Q Public       *
 *   License as defined by Trolltech AS of Norway and appearing in the     *
 *   file LICENSE.QPL included in the packaging of this file.              *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 ***************************************************************************/


#include <QtGui>

#ifndef _WIDGETKEYBOARD_H

#define _WIDGETKEYBOARD_H

#include "VirtualKeyboardButton.h"
#include <RoundRectWidget.h>

/**
 * A virtual keyboard.
 * It has 2 modes:
 *   Full mode      : Show the whole keyboard.
 *   Numbew pad mode: Show only number pad.
 */
class VirtualKeyboard : public RoundRectWidget {
	Q_OBJECT

public:
	VirtualKeyboard(QWidget *p);
	~VirtualKeyboard();

	/**
	 * Set keyboard's mode.
	 * If keyboard currently is visible, this will take effect immediately.
	 * @mode: Mode to be set. If @mode is not a available mode, this function dose nothing.
	 */
	void setMode(const int mode);
	int getMode() { return _mode; }

	enum Modes {
		MODE_FULL,		/*< Full mode */
		MODE_NUMPAD,	/*< Number pad mode */
		MODE_NR,
	};

public slots:
    /**
     * Toggle visibility of this virtual keyboard.
     */
	void toggleVisibility(void);

	/**
	 * Public slot. Response to focus change event.
	 *   @old: Widget on which focus once was.
	 *   @now: Widget on which focus currently is.
	 */
//	void focusChanged(QWidget *, QWidget *);

signals:
	void vKeyboardHide();

protected:
	void			closeEvent (QCloseEvent * event);

private:
	int alphabetPadIndex;
	int symbolPadIndex;
	int _mode;
	QStackedLayout *leftStackedLayout;
	QHBoxLayout *globalLayout;
	QSignalMapper *signalMapper;
	QList<VirtualKeyboardButton *> allButtons;
	bool shiftPressed;
	QWidget *mainPad;
	QWidget *numPad;

	/**
	 * Create the right part with only numbers and arrow keys.
	 */
	QWidget *createNumberPad();

	/**
	 * Create the main page with Alphabets.
	 */
	QWidget *createAlphabetPad();

	/**
	 * Create other main page with symbols.
	 */
	QWidget *createSymbolPad();

	/**
	 * Adjust keyboard's position.
	 *   @now: Widget on which focus currently is.
	 */
//	void adjustPosition(QWidget *now);
	void createKeyboard();

private slots:
	void on_btn_clicked(int btn);
};

#endif // _WIDGETKEYBOARD_H
