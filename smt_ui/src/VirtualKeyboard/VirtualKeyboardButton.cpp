/***************************************************************************
 *   Copyright (C) 2008 by Giovanni Romano                                 *
 *   Giovanni.Romano.76@gmail.com                                          *
 *                                                                         *
 *   This program may be distributed under the terms of the Q Public       *
 *   License as defined by Trolltech AS of Norway and appearing in the     *
 *   file LICENSE.QPL included in the packaging of this file.              *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 ***************************************************************************/


#include "VirtualKeyboardButton.h"
#include "VirtualKeyboard.h"


VirtualKeyboardButton::VirtualKeyboardButton(int keyID, int c, QString face, QString iconPath, QWidget *parent)
	: PushButton(parent)
{
		QString  defaulStyle = QString(DEFAULT_STYLE_BUTTON) + QString(DEFAULT_BACKGROUND_BUTTON);

		setKeyId(keyID);

		if (c == -1) {
			if (Qt::Key_0 <= keyID && keyID <= Qt::Key_9) {
				c = keyID - Qt::Key_0 + '0';
			} else if (Qt::Key_A <= keyID && keyID <= Qt::Key_Z) {
				c = keyID - Qt::Key_A + 'A';
			}
		}

		if (face == "" && iconPath == "") {
			if (c == '&')
				face = "&&";
			else
				face = c;
		}

		setCharacter(c);
		setFace(face);

		if (iconPath != "") {
			setIcon(QPixmap(iconPath));
		}

		setMinimumSize(KEY_WIDTH, KEY_HEIGHT);
		setMaximumSize(KEY_WIDTH, KEY_HEIGHT);

		setStyleSheet(defaulStyle);
}
