/***************************************************************************
 *   Copyright (C) 2008 by Giovanni Romano                                 *
 *   Giovanni.Romano.76@gmail.com                                          *
 *                                                                         *
 *   This program may be distributed under the terms of the Q Public       *
 *   License as defined by Trolltech AS of Norway and appearing in the     *
 *   file LICENSE.QPL included in the packaging of this file.              *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 ***************************************************************************/


#include <QWSServer>
#include "VirtualKeyboard.h"
#include "SmtKb.h"

#define Y_MARGIN     30

VirtualKeyboard::VirtualKeyboard(QWidget *p) : RoundRectWidget(p)
{
	createKeyboard();

	_mode = MODE_NUMPAD;
	shiftPressed = 0;

	signalMapper = new QSignalMapper(this);
	allButtons = findChildren<VirtualKeyboardButton *>();

	for (int i=0; i<allButtons.count(); i++) {
		connect(allButtons.at(i), SIGNAL(clicked()), signalMapper, SLOT(map()));
		signalMapper->setMapping(allButtons.at(i), i);
	}
	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(on_btn_clicked(int)));

	setWindowFlags(windowFlags() | Qt::WindowStaysOnTopHint);
	setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #7C7B9D, stop: 1 #454457);");

	// by default, can't move
	setMovable(false);
}

VirtualKeyboard::~VirtualKeyboard()
{
	delete signalMapper;
}

void VirtualKeyboard::setMode(int mode)
{
	if (mode == MODE_FULL) {
		mainPad->setVisible(true);
		numPad->setVisible(false);
	} else if (mode == MODE_NUMPAD) {
		numPad->setVisible(true);
		mainPad->setVisible(false);
	} else
		return;

	_mode = mode;
}

void VirtualKeyboard::on_btn_clicked(int btn)
{
	VirtualKeyboardButton *button = allButtons.at(btn);
	int keyId = button->keyId();
	int unicode = 0;

	if (keyId == Qt::Key_Shift) { // Shift handling
		shiftPressed = !shiftPressed;
//		qDebug() << shiftPressed;
		if (shiftPressed) {
			button->setStyleSheet(QString(DEFAULT_STYLE_BUTTON) + QString(CHANGED_BACKGROUND_BUTTON));
		} else {
			button->setStyleSheet(QString(DEFAULT_STYLE_BUTTON) + QString(DEFAULT_BACKGROUND_BUTTON));
		}
	} else if (keyId == Qt::Key_Select) { // Toggle Alphabet/Symbol layout.
		if (leftStackedLayout->currentIndex() == alphabetPadIndex) {
			leftStackedLayout->setCurrentIndex(symbolPadIndex);
		} else if (leftStackedLayout->currentIndex() == symbolPadIndex) {
			leftStackedLayout->setCurrentIndex(alphabetPadIndex);
		}
	} else if (keyId == Qt::Key_Sleep) { // "Done" handling
		hide();
	} else if (keyId >= Qt::Key_A && keyId <= Qt::Key_Z) {
		if (shiftPressed) {
			unicode = 'A' + keyId - Qt::Key_A;
		} else {
			unicode = 'a' + keyId - Qt::Key_A;
		}
	} else {
		unicode = button->character();
	}
	QWSServer::sendKeyEvent(unicode, keyId, Qt::NoModifier, QEvent::KeyPress, false);
//	qDebug() << keyId;
}

void VirtualKeyboard::closeEvent(QCloseEvent * event)
{
	event->ignore();
}

QWidget *VirtualKeyboard::createNumberPad(void)
{
	numPad = new QWidget;
	QGridLayout *tmpLayout = new QGridLayout;
	VirtualKeyboardButton	*tmp;

	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_7),1, 1);
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_8),1, 2);
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_9),1, 3);

	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_4),2, 1);
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_5),2, 2);
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_6),2, 3);

	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_1),3, 1);
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_2),3, 2);
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_3),3, 3);

	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_0),4, 1);
//	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Up, 0, "", "images/keyboard-up.png"), 4, 2);
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Period, '.', "."), 4, 2);
	tmpLayout->addWidget(new VirtualKeyboardButton(SmtKb::Key_Clear, 0, tr("Clear")), 4, 3);

//	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Left, 0, "", "images/keyboard-left.png"),5, 1);
//	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Down, 0, "", "images/keyboard-down.png"),5, 2);
//	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Right, 0, "", "images/keyboard-right.png"),5, 3);

	tmp = new VirtualKeyboardButton(Qt::Key_Sleep, 0, tr("X"));
	tmp->setMaximumHeight((int)(tmp->maximumHeight() * 3));
	tmp->setMinimumHeight((int)(tmp->minimumHeight() * 1));
	tmp->setStyleSheet("font-size:15px; border: 1px solid #8f8f91;border-radius:5px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #FF2E2E, stop: 1 #CA2424); color: white");
	tmpLayout->addWidget(tmp, 1, 4, 1, 1);

	tmp = new VirtualKeyboardButton(Qt::Key_Backspace, 0, "", "images/keyboard-backspace.png");
	tmpLayout->addWidget(tmp, 2, 4, 1, 1);

	tmp = new VirtualKeyboardButton(SmtKb::Key_Enter, 0, tr("Enter"));
	tmp->setMaximumHeight((int)(tmp->maximumHeight() * 5));
	tmp->setMinimumHeight((int)(tmp->minimumHeight() * 2));
	tmpLayout->addWidget(tmp, 3, 4, 2, 1);

	tmpLayout->setContentsMargins(2, 2, 2, 2);

	numPad->setLayout(tmpLayout);
	return numPad;
}

QWidget *VirtualKeyboard::createAlphabetPad(void)
{
	QWidget *ret = new QWidget;
	VirtualKeyboardButton	*tmp = NULL;
	QVBoxLayout	*tmpVLayout = new QVBoxLayout;
	QHBoxLayout	*tmpLayout = new QHBoxLayout;
	QString         tmpStyle = QString::null;

	/*
	 *  Number keypad
	 */
	QGridLayout *numLayout = new QGridLayout;
	for (int i = 1; i <= 9; i++) {
		numLayout->addWidget(new VirtualKeyboardButton(Qt::Key_0 + i), (i-1)/3, (i-1)%3);
	}
	numLayout->addWidget(new VirtualKeyboardButton(Qt::Key_0), 3, 0);
	numLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Up, 0, "", "images/keyboard-up.png"), 3, 1);
	numLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Down, 0, "", "images/keyboard-down.png"), 3, 2);

	/*
	 *  Line begin with 'Q'.
	 */
	tmpLayout = new QHBoxLayout;
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Q));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_W));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_E));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_R));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_T));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Y));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_U));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_I));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_O));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_P));

	tmpLayout->addStretch();
	tmpVLayout->insertLayout(0, tmpLayout);

	/*
	 *  Line begin with "A".
	 */
	tmpLayout = new QHBoxLayout;
	tmpLayout->addStretch();

	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_A));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_S));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_D));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_F));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_G));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_H));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_J));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_K));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_L));

	tmpLayout->addStretch();
	tmpVLayout->insertLayout(1, tmpLayout);

	/*
	 *  Line begin with "Z".
	 */
	tmpLayout = new QHBoxLayout;
	tmp = new VirtualKeyboardButton(Qt::Key_Shift, 0, "Caps", "images/keyboard-caps.png");
	tmp->setMaximumWidth((int)(tmp->maximumWidth() * 1.5));
	tmp->setMinimumWidth((int)(tmp->minimumWidth() * 1.5));
	tmpLayout->addWidget(tmp);

	tmpLayout->addStretch();

	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Z));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_X));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_C));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_V));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_B));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_N));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_M));

	tmpLayout->addStretch();

	tmp = new VirtualKeyboardButton(Qt::Key_Backspace, 0, "", "images/keyboard-backspace.png");
	tmp->setMaximumWidth((int)(tmp->maximumWidth() * 1.5));
	tmp->setMinimumWidth((int)(tmp->minimumWidth() * 1.5));
	tmpLayout->addWidget(tmp);

	tmpVLayout->insertLayout(2, tmpLayout);

	/*
	 *  Space bar line.
	 */
	tmpLayout = new QHBoxLayout;

	//tmpLayout->addSpacing(KEY_WIDTH * 2);
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Select, 0, tr("#+="))); // The key ID is fake.
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Henkan, 0, "", "images/keyboard-globe.png"));
	tmp = new VirtualKeyboardButton(Qt::Key_Space, ' ', tr("Space"));
	tmp->setMaximumWidth((int)(tmp->maximumWidth() * 4));
	tmp->setMinimumWidth((int)(tmp->minimumWidth() * 7.5));
	tmpLayout->addWidget(tmp);

	tmpLayout->addStretch();
	tmp = new VirtualKeyboardButton(Qt::Key_Return, 0, "Enter", "");
	tmp->setMaximumWidth(tmp->maximumWidth() * 2);
	tmp->setMinimumWidth(tmp->minimumWidth() * 2);
	tmpLayout->addWidget(tmp);

	tmpVLayout->insertLayout(3, tmpLayout);
	tmpVLayout->setContentsMargins(2, 2, 2, 2);

	QHBoxLayout *layout = new QHBoxLayout;
	layout->addLayout(tmpVLayout);
	layout->addLayout(numLayout);
	ret->setLayout(layout);
	return ret;
}

QWidget *VirtualKeyboard::createSymbolPad(void)
{
	QWidget *ret = new QWidget();
	VirtualKeyboardButton	*tmp = NULL;	
	QVBoxLayout	*tmpVLayout = new QVBoxLayout;
	QHBoxLayout	*tmpLayout = new QHBoxLayout;
	QString         tmpStyle = QString::null;

	/*
	 *  Number keypad
	 */
	QGridLayout *numLayout = new QGridLayout;
	for (int i = 1; i <= 9; i++) {
		numLayout->addWidget(new VirtualKeyboardButton(Qt::Key_0 + i), (i-1)/3, (i-1)%3);
	}
	numLayout->addWidget(new VirtualKeyboardButton(Qt::Key_0), 3, 0);
	numLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Up, 0, "", "images/keyboard-up.png"), 3, 1);
	numLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Down, 0, "", "images/keyboard-down.png"), 3, 2);

	/*
	 *  Line begin with 'Q'.
	 */
	tmpLayout = new QHBoxLayout;
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_BracketLeft, '['));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_BracketRight, ']'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_BraceLeft, '{'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_BraceRight, '}'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_NumberSign, '#'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Percent, '%'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_AsciiCircum, '^'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Asterisk, '*'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Plus, '+'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Equal, '='));

	tmpLayout->addStretch();
	tmpVLayout->insertLayout(0, tmpLayout);

	/*
	 *  Line begin with "A".
	 */
	tmpLayout = new QHBoxLayout;
	tmpLayout->addStretch();

	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Minus, '-'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Slash, '/'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Colon, ':'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Semicolon, ';'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_ParenLeft, '('));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_ParenRight, ')'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Dollar, '$'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Ampersand, '&'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_At, '@'));

	tmpLayout->addStretch();
	tmpVLayout->insertLayout(1, tmpLayout);

	/*
	 *  Line begin with "Z".
	 */
	tmpLayout = new QHBoxLayout;
	tmp = new VirtualKeyboardButton(Qt::Key_Shift, 0, tr("Shift"));
	tmp->setMaximumWidth((int)(tmp->maximumWidth() * 1.5));
	tmp->setMinimumWidth((int)(tmp->minimumWidth() * 1.5));
	tmp->setIcon(QPixmap(":/images/editcopy.png"));
	tmpLayout->addWidget(tmp);

	tmpLayout->addStretch();

	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Period, '.'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Comma, ','));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Question, '?'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Greater, '>'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Less, '<'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_QuoteDbl, '\"'));
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_AsciiTilde, '~'));

	tmpLayout->addStretch();

	tmp = new VirtualKeyboardButton(Qt::Key_Backspace, 0, "", "images/keyboard-backspace.png");
	tmp->setMaximumWidth((int)(tmp->maximumWidth() * 1.5));
	tmp->setMinimumWidth((int)(tmp->minimumWidth() * 1.5));
	tmpLayout->addWidget(tmp);

	tmpVLayout->insertLayout(2, tmpLayout);

	/*
	 *  Space bar line.
	 */
	tmpLayout = new QHBoxLayout;

	//tmpLayout->addSpacing(KEY_WIDTH * 2);
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Select, 0, tr("ABC"))); // The key ID is fake.
	tmpLayout->addWidget(new VirtualKeyboardButton(Qt::Key_Henkan, 0, "", "images/keyboard-globe.png"));
	tmp = new VirtualKeyboardButton(Qt::Key_Space, ' ', tr("Space"));
	tmp->setMaximumWidth((int)(tmp->maximumWidth() * 7.5));
	tmp->setMinimumWidth((int)(tmp->minimumWidth() * 7.5));
	tmpLayout->addWidget(tmp);

	tmpLayout->addStretch();
	tmp = new VirtualKeyboardButton(Qt::Key_Return, 0, tr("Enter"));
	tmp->setMaximumWidth(tmp->maximumWidth() * 2);
	tmp->setMinimumWidth(tmp->minimumWidth() * 2);
	tmpLayout->addWidget(tmp);

	tmpVLayout->insertLayout(3, tmpLayout);
	tmpVLayout->setContentsMargins(2, 2, 2, 2);

	QHBoxLayout *layout = new QHBoxLayout;
	layout->addLayout(tmpVLayout);
	layout->addLayout(numLayout);

	ret->setLayout(layout);
	return ret;
}

void VirtualKeyboard::createKeyboard(void)
{

	leftStackedLayout = new QStackedLayout;
	alphabetPadIndex = leftStackedLayout->addWidget(createAlphabetPad());
	symbolPadIndex = leftStackedLayout->addWidget(createSymbolPad());

	mainPad = new QWidget;
	mainPad->setLayout(leftStackedLayout);

	globalLayout = new QHBoxLayout;
	globalLayout->addWidget(mainPad);

	QWidget *numPad = createNumberPad();
	globalLayout->addWidget(numPad);

	this->setLayout(globalLayout);
	this->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

#if 0
void VirtualKeyboard::adjustPosition(QWidget *now)
{
	int _x, _y;
	/**
	 * TODO: I don't think now will be NULL, but if do not check, run calculator will cause segv.
	 */
	if (now == NULL)
		return;

	QPoint global = now->mapToGlobal(QPoint(0,0));

	/*
	 * Align keyboard and widget's X-center.
	 * There is a Y_MARGIN between keyboard's bottom and widget's top.
	 */
	_x = global.x() + now->width()/2 - width()/2;
	_y = global.y() - height() - Y_MARGIN;

	/* Limit window position in screen */
	if (_x < 0)
		_x = 0;
	if (_y < 0)
		_y = 0;

	int max_x = QApplication::desktop()->geometry().right() - width();
	int max_y = QApplication::desktop()->geometry().bottom() - height();

	if (_x > max_x )
		_x = max_x;
	if (_y > max_y)
		_y = max_y;

	move(_x, _y);
//	qDebug() << global.x() << global.y();
//	qDebug() << _x << _y;
}

void VirtualKeyboard::focusChanged(QWidget */*old*/, QWidget */*now*/)
{
	//adjustPosition(now);
	//qDebug() << "focus changed";
}
#endif

void VirtualKeyboard::toggleVisibility()
{
	if (isVisible()) {
		hide();
		emit vKeyboardHide();
	} else {
		show();
#if 0 // disable this feature
		adjustPosition(QApplication::focusWidget());
#endif
	}
}
