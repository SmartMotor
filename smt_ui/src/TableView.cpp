/*
 * TreeView.cpp
 *
 *  Created on: 2010/03/28
 *      Author: crazysjf
 */

#include "include/TableView.h"
#include <Debug.h>

TableView::TableView(QWidget *parent) : QTableView(parent)
{
	horizontalHeader()->setStretchLastSection(true);
	setSelectionBehavior(QAbstractItemView::SelectRows);
	setSelectionMode(QAbstractItemView::SingleSelection);
}

TableView::~TableView()
{
}

void TableView::adaptColumns (const QModelIndex & topleft, const QModelIndex& bottomRight)
{
        int firstColumn= topleft.column();
        int lastColumn = bottomRight.column();

        do {
               resizeColumnToContents(firstColumn);
               firstColumn++;
        } while (firstColumn < lastColumn);
}

///
// When selection changed, this function is called.
//
void TableView::selectionChanged ( const QItemSelection & selected, const QItemSelection &deselected)
{
	QTableView::selectionChanged(selected, deselected);
	emit selectionChangedSignal(selected, deselected);
}

void TableView::moveCursorUp()
{
	QModelIndex index = moveCursor(MoveUp, Qt::NoModifier);
	setCurrentIndex(index);
}

void TableView::moveCursorDown()
{
	QModelIndex index = moveCursor(MoveDown, Qt::NoModifier);
	setCurrentIndex(index);
}

void TableView::moveCursorPageUp()
{
	QModelIndex index = moveCursor(MovePageUp, Qt::NoModifier);
	setCurrentIndex(index);
}

void TableView::moveCursorPageDown()
{
	QModelIndex index = moveCursor(MovePageDown, Qt::NoModifier);
	setCurrentIndex(index);
}
