/*
 * AverageEditor.cpp
 *
 *  Created on: Jun 4, 2010
 *      Author: souken-i
 */

#include <QtGui>
#include "AverageEditor.h"
#include "Global.h"

extern struct averageEditData_t averageEditData;

AverageEditor::AverageEditor(QWidget *parent)
	: EditDialog(parent)
{
	int i;

	// clear global structure
	averageEditData.mask = 0;  // no data is valid
	averageEditData.mode = AVERAGE_NUMBER_MODE;  // default is number mode

	QRegExp rx("\\d{1,4}\\.{0,1}\\d{0,2}");
	QValidator *validator = new QRegExpValidator(rx, this);

	for (i = 0; i < AVERAGE_EDIT_ITEM_NR; i++) {
		descLabels[i] = new QLabel;
		inputEdits[i] = new LineEditVKeyboard;
		inputEdits[i]->setValidator(validator);
	}

	numberButton = new PushButton(QIcon("images/average-number-mode.png"), tr("Number mode"), this);
	valueButton  = new PushButton(QIcon("images/average-value-mode.png"), tr("Value mode"), this);

	connect(getOKButton(), SIGNAL(clicked()), this, SLOT(handleInputData()));
	connect(getCancelButton(), SIGNAL(clicked()), this, SLOT(close()));
	connect(numberButton, SIGNAL(clicked()), this, SLOT(setAverageNumberMode()));
	connect(valueButton, SIGNAL(clicked()), this, SLOT(setAverageValueMode()));

	numberButton->setCheckable(true);
	numberButton->setFocusPolicy(Qt::NoFocus);
	valueButton->setCheckable(true);
	valueButton->setFocusPolicy(Qt::NoFocus);

	// Layout
	QGridLayout *inputLayout = new QGridLayout;
	for (i = 0; i < AVERAGE_EDIT_ITEM_NR; i++) {
		inputLayout->addWidget(descLabels[i], i, 0);
		inputLayout->addWidget(inputEdits[i], i, 1);
	}

	QHBoxLayout *modeButtonLayout = new QHBoxLayout;
	modeButtonLayout->addWidget(numberButton);
	modeButtonLayout->addWidget(valueButton);

	QVBoxLayout *leftLayout = new QVBoxLayout;
	leftLayout->addLayout(inputLayout);
	leftLayout->addLayout(modeButtonLayout);
	insertDrawingArea(leftLayout);

	inputEdits[TOTAL_LENGTH]->setFocus();

	setGuideIcon(QPixmap("images/guide-averageedit.png"));

	updateModeButtons();
	retranslateUi();
}

AverageEditor::~AverageEditor()
{

}

void AverageEditor::handleInputData()
{
	QString	input;
	int data;

	input = inputEdits[TOTAL_LENGTH]->text();
	data = MData::toInt(input);
	if (gMotor->isPostionValid(data)) {
		averageEditData.totalLength = data;
		averageEditData.mask = AED_TOTAL_LENGTH_VALID;
	} else {
		inputEdits[TOTAL_LENGTH]->clear();
		inputEdits[TOTAL_LENGTH]->setFocus();
		return;
	}

	input = inputEdits[AVERAGE_DATA]->text();
	if (averageEditData.mode == AVERAGE_NUMBER_MODE) {
		// Average number
		data = input.toInt();
	} else {
		// Average value
		data = MData::toInt(input);
	}

	if ( data > 0 ) {
		averageEditData.averageNumber = data;
		averageEditData.mask |= AED_AVERAGE_NUMBER_VALID;
	} else {
		inputEdits[AVERAGE_DATA]->clear();
		inputEdits[AVERAGE_DATA]->setFocus();
		return;
	}

	if (averageEditData.mask == AED_ALL_VALID) {
		close();
	}
}

void AverageEditor::setAverageNumberMode()
{
	setAverageMode(AVERAGE_NUMBER_MODE);
}

void AverageEditor::setAverageValueMode()
{
	setAverageMode(AVERAGE_VALUE_MODE);
}

void AverageEditor::setAverageMode(int mode)
{
	if (mode == AVERAGE_NUMBER_MODE) {
		setTitle(tr("Average Number Edit"));
		descLabels[AVERAGE_DATA ]->setText(tr("Average Number"));
	} else {
		setTitle(tr("Average Value Edit"));
		descLabels[AVERAGE_DATA ]->setText(tr("Average Length"));
	}

	averageEditData.mode = mode;
	updateModeButtons();
}

void AverageEditor::updateModeButtons()
{
	bool number_mode = (averageEditData.mode == AVERAGE_NUMBER_MODE);
	numberButton->setChecked(number_mode);
	valueButton->setChecked(!number_mode);
}

void AverageEditor::retranslateUi()
{
	setGuideText(tr("Press button to toggle average mode"));

	setAverageMode(averageEditData.mode);
	descLabels[TOTAL_LENGTH ]->setText(tr("Total Length"));
}

/**
 * After input data, then press Enter, if the data is valid,
 * move focus to next edit box
 */
void  AverageEditor::keyPressEvent(QKeyEvent *event)
{
#if 0
	qDebug() << "Key Pressed in Input Box: " << hex << event->key();
#endif
	switch(event->key()) {
	case SmtKb::Key_F1:
	case SmtKb::Key_Enter:
	case Qt::Key_Enter:
		handleInputData();
		break;
	case SmtKb::Key_F2:
		close();
		break;
	case SmtKb::Key_Clear:
		// clear current input box
		for (int i = 0; i < AVERAGE_EDIT_ITEM_NR; i++) {
			if (inputEdits[i]->hasFocus()) {
				inputEdits[i]->clear();
				break;
			}
		}
		break;
	default:
		QWidget::keyPressEvent(event);
	}
}
