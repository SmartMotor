/*
 * FlagEditor.cpp
 *
 *  Created on: 2010/04/01
 *      Author: crazysjf
 */

#include "include/FlagEditor.h"

#define PAINTING_SCALE_FACTOR     20
#define FLAG_IMAGE_MARGIN         3

QRect FlagEditor::paintingRect = QRect();

// This must be the same order as flags
QString FlagEditor::flagImageFiles[FLAG_NR] = {
		// group 0
		"images/flag-pusher.png",
		"images/flag-cut.png",
		"images/flag-press.png",
		// group 1
		"images/flag-air.png",
		"images/flag-air.png",   // unused
		"images/flag-air.png",	 // unused
		// group 2
		"images/flag-align.png",
		"images/flag-align.png", // unused
		"images/flag-align.png", // unused
		// group 3
		"images/flag-turn-around.png",
		"images/flag-turn-right.png",
		"images/flag-turn-left.png"
};

QImage FlagEditor::scaledFlagimages[FLAG_NR] = {
	QImage(),
	QImage(),
	QImage(),
	QImage()
};

FlagEditor::FlagEditor(QWidget *parent)
	: QWidget(parent)
{

}

FlagEditor::FlagEditor(int flags, QWidget *parent)
	: QWidget(parent)
{
	_flags = flags;
}

FlagEditor::~FlagEditor( )
{

}

void FlagEditor::paint(QPainter *painter, const QRect &rect, const QPalette &, int flags)
{
	int i, j, k;
	// Update paintRect and re-generate flag images using updated rect
	if (rect != paintingRect) {
		paintingRect = rect;

		QSize size;
		size.setHeight(rect.height() - 2 * FLAG_IMAGE_MARGIN);
		size.setWidth(size.height());

		for (i=0; i<FLAG_NR; i++) {
			QImage flagImage(flagImageFiles[i]);
			scaledFlagimages[i] = flagImage.scaled(size, Qt::IgnoreAspectRatio);
		}
	}

	QRegion clipRegion = painter->clipRegion();
	bool hasClipping = painter->hasClipping();
	painter->setClipRect(rect);
	QPoint topLeft = rect.topLeft();
	for (i = 0; i < FLAG_GROUP_NR; i++) {
		if (flags & (FLAG_GROUP_MASK << (i * FLAG_GROUP_BITS))) {
			j = i * FLAG_GROUP_BITS;
			// Only one flag inside this group can be set, if multiple bits are set
			// use the first bit
			for (k = 0; k < FLAG_GROUP_BITS; k++, j++) {
				if (flags & (1 << j))
					break;
			}

			QPoint p = topLeft +
					QPoint(FLAG_IMAGE_MARGIN + (scaledFlagimages[j].size().width() + FLAG_IMAGE_MARGIN) * i,
							FLAG_IMAGE_MARGIN);
			painter->drawImage(p, scaledFlagimages[j]);
		}
	}
	painter->setClipRegion(clipRegion);
	painter->setClipping(hasClipping);
}

void FlagEditor::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    paint(&painter, rect(), this->palette(), _flags);
}

QSize FlagEditor::sizeHint() const
{
	return PAINTING_SCALE_FACTOR * QSize(FLAG_GROUP_NR, 1);
}
