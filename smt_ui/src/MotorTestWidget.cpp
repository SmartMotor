/*
 * MotorTestWidget.cpp
 *
 *  This is for debug on host machine only
 *
 *  Created on: Jun 8, 2010
 *      Author: souken-i
 */

#include <QtGui>
#include "MotorTestWidget.h"
#include "Global.h"

#ifdef  MOTOR_TEST_WIDGET_ENABLE

MotorTestWidget::MotorTestWidget(QWidget *parent)
	: RoundRectWidget(parent)
{
	_cutStatus = Motor::CutUp;
	_pusherStatus = Motor::PusherUp;
	_currentPos = gMotor->pos();

	cutButton = new PushButton(tr("Move Cut Down"), this);
	connect(cutButton, SIGNAL(clicked()), this, SLOT(toggleCutStatus()));

	pusherButton = new PushButton(tr("Move Pusher Up"), this);
	connect(pusherButton, SIGNAL(clicked()), this, SLOT(togglePusherStatus()));

	QLabel *desc = new QLabel(tr("Push button to toggle cut or pusher status"));

	posButton = new PushButton(tr("Start Current Position Test"), this);
	connect(posButton, SIGNAL(clicked()), this, SLOT(currentPosTest()));

	QHBoxLayout *buttonLayout = new QHBoxLayout;
	buttonLayout->addWidget(cutButton);
	buttonLayout->addWidget(pusherButton);

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addWidget(desc);
	layout->addLayout(buttonLayout);
	layout->addWidget(posButton);
	setLayout(layout);

	_direction = 0; // add
	timer = new QTimer;
	timer->setInterval(500);
	timer->setSingleShot(false);
	connect(timer, SIGNAL(timeout()), this, SLOT(setCurrentPos()));

	retranslateUi();
}

MotorTestWidget::~MotorTestWidget()
{
	delete timer;
}

void MotorTestWidget::retranslateUi()
{
	updateButtons();
}

void MotorTestWidget::updateButtons()
{
	if (_cutStatus == Motor::CutDown) {
		cutButton->setText(tr("Move Cut Up"));
	} else {
		cutButton->setText(tr("Move Cut Down"));
	}

	if (_pusherStatus == Motor::PusherDown) {
		pusherButton->setText(tr("Move Pusher Up"));
	} else {
		pusherButton->setText(tr("Move Pusher Down"));
	}
}

void MotorTestWidget::toggleCutStatus()
{
	if (_cutStatus == Motor::CutDown) {
		_cutStatus = Motor::CutUp;
	} else {
		_cutStatus = Motor::CutDown;
	}

	updateButtons();

	/**
	 * forceStatusChange() accepts the parameters as RAW STATUS value.
	 */
	int rawCutStatus = (_cutStatus == Motor::CutDown)? 0 : 1;
	int rawPusherStatus = (_pusherStatus == Motor::PusherDown)? 1 : 0;
	gMotor->mcp->forceStatusChange(rawCutStatus, rawPusherStatus);
}

void MotorTestWidget::togglePusherStatus()
{
	if (_pusherStatus == Motor::PusherDown) {
		_pusherStatus = Motor::PusherUp;
	} else {
		_pusherStatus = Motor::PusherDown;
	}

	updateButtons();

	/**
	 * forceStatusChange() accepts the parameters as RAW STATUS value.
	 */
	int rawCutStatus = (_cutStatus == Motor::CutDown)? 0 : 1;
	int rawPusherStatus = (_pusherStatus == Motor::PusherDown)? 1 : 0;

	gMotor->mcp->forceStatusChange(rawCutStatus, rawPusherStatus);
}

void MotorTestWidget::currentPosTest()
{
	if (timer->isActive()) {
		timer->stop();
		posButton->setText(tr("Start Current Position Test"));
	} else {
		timer->start();
		posButton->setText(tr("Stop Current Position Test"));
	}
}

void MotorTestWidget::setCurrentPos()
{
	int maxPos = gMotor->getMaxLimit();
	int minPos = gMotor->getFrontLimit();

	if (!_direction) {
		_currentPos += 5000; // 50.00 mm
		if (_currentPos > maxPos) {
			_currentPos = maxPos;
			_direction = 1;
		}
	} else {
		_currentPos -= 5000; // 50.00 mm
		if (_currentPos < minPos) {
			_currentPos = minPos;
			_direction = 0;
		}
	}
	gMotor->mcp->forcePositionChange(_currentPos);

}

#endif /* MOTOR_TEST_WIDGET_ENABLE */
