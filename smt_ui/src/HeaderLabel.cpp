/*
 * HeaderLabel.cpp
 *
 *  Created on: 2010/03/06
 *      Author: crazysjf
 */
#include <QtGui>
#include <QDateTime>
#include <QTimer>

#include "HeaderLabel.h"
#include "Debug.h"

HeaderLabel::HeaderLabel()
{
#if 0
	// Using mainframe background image
	//	set background
//	QString styleSheet = "background-color: green;";
	QString styleSheet = "background-color: #5a4210;";
	setStyleSheet(styleSheet);
#endif

	logoPixmapLabel = new QLabel;
	QPixmap logoPixmap = QPixmap("images/logo-32.png");
	QPixmap scaledLogo = logoPixmap.scaled(LogoWidth, LogoHeight, Qt::KeepAspectRatio);
	logoPixmapLabel->setPixmap(scaledLogo);

	programDescription = new QLabel;

	QHBoxLayout *layout = new QHBoxLayout;
	layout->addWidget(logoPixmapLabel);
	layout->addWidget(programDescription);
	layout->addStretch();
	timeLabel = new TimeLabel;
	layout->addWidget(timeLabel);
	layout->addSpacing(10);
	layout->setContentsMargins(LEFT_MARGIN, TOP_MARGIN, RIGHT_MARGIN, BOTTOM_MARGIN);
	setLayout(layout);

	setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

	QString desc = QString("Program Description");
	updateProgramDescription(desc);
}

HeaderLabel::~HeaderLabel()
{
}

QSize HeaderLabel::sizeHint() const
{
	QSize size = QLabel::sizeHint();
	size.rheight() = HeaderHeight;

	return size;
}

void HeaderLabel::updateProgramDescription(const QString &desc)
{
	int length;
	QString str = desc;

	// Change newline to space because we only have one line
	str.replace(QChar('\n'), QChar(' '));

	length = str.size();
	if (length > MaxShowDescriptionLength) {
		QString head = str.mid(0, ShowHeaderLength);
		QString tail = str.mid(length - ShowTailLength, ShowTailLength);
		programDescription->setText(head + tr(" ... ") + tail);
	} else {
		programDescription->setText(str);
	}
}
