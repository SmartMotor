/*
 * MData.cpp
 *
 *  Created on: Mar 28, 2010
 *      Author: souken-i
 */
#include "MData.h"

MData::MData() {
	// TODO Auto-generated constructor stub

}

MData::~MData() {
	// TODO Auto-generated destructor stub
}

int MData::toInternal(int value)
{
	return value * 100;
}

int MData::toShow(int value)
{
	return value / 100;
}
/*
 *  convert int value to an user readable string
 *  eg: value = 12345 ->  "123.45"
 *      value = 12300 ->  "123"
 */
QString MData::toString(int value)
{

	QString mm = QString::number(toShow(value));
	int rem = value % 100;
	if (rem) {
		QString remStr;
		remStr.sprintf(".%02d", rem);
		return mm + remStr;
	} else {
		return mm + ".00";
	}
}

/*
 * Convert string to internal data
 * eg: "123"     -> 12300
 *     "123.45"  -> 12345
 *     "123.456" -> 12345
 */
int MData::toInt(QString &text)
{
	if (text.contains(QChar('.'))) {
		// TODO: Improve me
		int pos = text.indexOf(QChar('.'));
		int size = text.size();
		int mm = 0;
		if (pos >= 1) {
			QString high = text.mid(0, pos);
			mm = toInternal(high.toInt());
		}

		if (size - pos <= 2) {
			text.append("00");
		}

		// Cut 2 chars behind .
		QString low = text.mid(pos+1, 2);
		mm += low.toInt();
		return mm;

	} else {
		return MData::toInternal(text.toInt());
	}
}
