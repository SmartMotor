/*
 * Motor.h
 *
 *  Created on: Mar 29, 2010
 *      Author: souken-i
 */

#ifndef MOTOR_H_
#define MOTOR_H_

#include "MotorParameters.h"
#include "MotorControlProtocol.h"
#include "MotorCommand.h"
#include "MotorLog.h"
#include "Debug.h"

class Motor : public MotorParameters
{
    Q_OBJECT

public:
	Motor();
	virtual ~Motor();

	enum MotorDirection {
		DirectionForward  = 0,
		DirectionBackward = 1
	};

	enum ActiveLimitTypes {
		FrontLimitActive  = 0,
		MiddleLimitActive = 1
	};

	enum CutStates {
		CutUp   = 0,  // Cut is released
		CutDown = 1,
		CutUpToDown = 2,
		CutDownToUp = 3,
	};

	enum PusherStates {
		PusherUp   = 0,
		PusherDown = 1,
		PusherUpToDown = 2,
		PusherDownToUp = 3,
	};


	enum AlarmTypes {
		AlarmNone             = 0,
		AlarmOverCurrent      = 1,  // "OC"
		AlarmOverLoad         = 2,  // "OL"
		AlarmLoseControl      = 3,	// "LS"
		AlarmCommunicateError = 4,  // "RS"
	};

	// TODO This should be defined in program class
	enum DataFlagMask {
		AutoCutEnable  = 0x1,  // Relay 1
		CutEnable      = 0x2,  // Relay 2
		AutoPushEnable = 0x4,  // Relay 3
		AirEnable      = 0x8,  // Relay 4
	};

	enum MotorCurrentLevel {
		CurrentLevelGreen  = 0,
		CurrentLevelYellow = 1,
		CurrentLevelRed    = 2,
	};

	MotorControlProtocol *mcp;

	void postInitialize();

	bool isMotorPresent();

	// Basic commands
	void start();
	void stop();
	void runToPos(int pos);
	void runForward();
	void runBackward();
	void setPitch(PitchTypes value);
	void adjustCurrentPosition(int pos);
	void setPusherSpeed(int speed);
	void setZeroPos();
	void setCurPos(int pos);
	void setRelay(int data);
	void resendLastCommand();

	void disableFrontLimit();
	void disableMiddleLimit();
	void disableBackLimit();
	void disablePallet();
	void enableAllLimits();

	void startCodec();
	void stopCodec();

	int  getCutCount();
	void setCutCount(int count);

	// Motor Current Value
	int getMotorCurrent();

	// Speed
	int getSpeed();

	// Cut commands
	void runOneCut(int pos, int flag);

	void enableLimit(bool state);
	bool isLimitEnabled();

	int  getActiveLimit();

	// Check whether a given position is within current limits
	bool isPostionValid(int pos);
	static int currentOverloadLevel(int value);

signals:
	void oneCutLoopFinished();
	void cutStatusChanged(int cut);       // Cut status changed
	void pusherStatusChanged(int pusher); // Pusher status changed
	void currentPosChanged(int pos);      // Current cut position changed
	void alarmStatusChanged(int code);    // Happened or disappeared
	void frontLimitExceeded(bool status);
	void middleLimitExceeded(bool status);
	void backLimitExceeded(bool status);
	void currentChanged(int current);     // unit is Amp
	void speedChanged(int speed);         // unit is RPM
	void cutCountChanged(int count);

public slots:
	void handleStatusEvent(int status, int mask);
	void handlePositionEvent(int pos);
	void handleSpeedEvent(int speed);
	void motorTest();

private:
	// Run-time status variables
	int currentSpeed;   // Pusher speed
	int pitch;
	bool _enableLimit;   // If false, all limits will not be used
	int motorCurrent;   // unit: Amp
	MotorDirection direction;
	bool frontLimitExceed;
	bool middleLimitExceed;
	bool backLimitExceed;
	ActiveLimitTypes whichLimit;   // 0: front Limit 1: middle Limit
	CutStates cutState;
	PusherStates pusherState;
	int alarm;
	int currentLevel;
	int _cutCount;

	// Run-time control variables
	int pusherActionState;
	int cutActionState;
	int currentDataFlag;
	int currentDestination;
	bool isOneLoopDone;
	void updatePusherStatus();
	void updateCutStatus();
	bool isPusherOnly();

	// Motor Alarm Log
	MotorLog *motorLog;
};

#endif /* MOTOR_H_ */
