/*
 * Bubtton.h
 *
 *  Created on: Mar 21, 2010
 *      Author: Zeng Xianwei
 */
#ifndef BUTTON_H
#define BUTTON_H

#include "ToolButton.h"

class Button : public ToolButton
{
    Q_OBJECT

public:
    Button(QWidget *parent = 0);
    Button(const QString &text, QWidget *parent = 0);

    QSize sizeHint() const;
};

#endif
