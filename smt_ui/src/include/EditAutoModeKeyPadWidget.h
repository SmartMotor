/*
 * EditAutoModeKeyPadWidget.h
 *
 *  A keypad widget for Edit Mode and AutoRun Mode
 *
 *  Created on: Jun 19, 2010
 *      Author: souken-i
 */

#ifndef EDITAUTOMODEKEYPADWIDGET_H_
#define EDITAUTOMODEKEYPADWIDGET_H_

#include <QWidget>
#include <QString>
#include <QSignalMapper>

#include "Button.h"

class EditAutoModeKeyPadWidget : public QWidget
{
	Q_OBJECT

public:
	EditAutoModeKeyPadWidget(QWidget *parent = 0);
	virtual ~EditAutoModeKeyPadWidget();

	/* Change the Key Pad Type for Edit Mode or AutoRun Mode:
	 *   Only the last two buttons are different
	 */
	void setKeyPadType(int type);
    QSize sizeHint() const;

	enum {
		KP_Type_Edit = 0,
		KP_Type_AutoRun,
	};

	enum {
		KP_Rows    = 4,
		KP_Columns = 4,

		KP_MatrixButtonsNR = 16,    // 4x4
		KP_ButtonsNR       = 19,    // 4x4 + close, zoom-in, zoom-out
		KP_IconsNR         = 19,    // 4x4 + close, zoom-in, zoom-out + ++,--
	};

	enum {
		Button_Index_Add      = 12,     // These two buttons are mode-dependent
		Button_Index_Sub      = 13,
		Button_Index_Mutiply  = 14,     // These two buttons are mode-dependent
		Button_Index_Divide   = 15,

		Button_Index_Close    = 16,     // close button index
		Button_Index_ZoomIn   = 17,     // zoom-in button index
		Button_Index_ZoomOut  = 18,     // zoom-out button index
	};

	static QString iconFiles[KP_IconsNR];

signals:
		void clicked(int id);

protected:
	void mousePressEvent ( QMouseEvent * event );
	void resizeEvent ( QResizeEvent * event );

private slots:
	void hideButtons();

private:
	Button * createButton(int id , const QIcon &icon);
	void updateKeyPadType(int type);

	/**
	 * Convert internal button ID to global key code Mapping
	 */
	int getKeyPadCode(int button_id, int type = KP_Type_Edit);
	static int keyPadMappings[KP_ButtonsNR];

	int _keyPadType;
    QSignalMapper *signalMapper;
	Button *keyPadButton[KP_ButtonsNR];
};

#endif /* EDITAUTOMODEKEYPADWIDGET_H_ */
