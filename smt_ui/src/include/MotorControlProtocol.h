/*
 * MotorControlProtocol.h
 *
 *  Created on: Mar 29, 2010
 *      Author: souken-i
 */

#ifndef MOTORCONTROLPROTOCOL_H_
#define MOTORCONTROLPROTOCOL_H_

#include "SerialPortFrame.h"
#include "MotorCommand.h"
#include "Debug.h"

class MotorControlProtocol : public QObject
{
    Q_OBJECT

public:
	MotorControlProtocol(const char *deviceName);
	virtual ~MotorControlProtocol();

    // TODO FrameDataLengh is also defined in SerialPortFrame.h
    enum {
    	RxFrameTypeIndex  = 0,  // Byte 0

    	// T Frame
    	RxXIndex          = 1,  // byte 1
    	RxYIndex          = 2,  // byte 2
    	RxZIndex          = 3,  // byte 3
    	RxPositionIndex   = 4,  // byte 4 ~ 9
    	RxPositionLength  = 6,
    	RxAlarmCodeIndex  = 2,  // byte 2 ~ 3
    	RxAlarmCodeLength = 2,

    	// F Frame
    	RxPitchIndex      = 1,  // A
    	RxZeroIndex       = 2,  // B
    	RxCurrentIndex    = 3,
    	RxCurrentLength   = 2,  // byte 3 ~ 4
    	RxDirectionIndex  = 5,
    	RxSpeedIndex      = 6,
    	RxSpeedLength     = 4,  // byte 6 ~ 9
    };

    // This is defined in SerialPortFrame.h
#if 0
    enum {
    	TxFrameDataLength  = 9,
    	RxFrameDataLength  = 10,
    };
#endif

    // Motor Status Frame Types
    enum {
    	MS_TStatus       = 0,
    	MS_FStatus       = 1,
    	MS_StatusNumbers = 2
    };

    enum {
    	Dir_Forward  = 0,
    	Dir_Backward = 1,
    };

    // TODO FIXME This is also used in Motor class
    enum RawStatusBitOffset {
    	// T-X
    	// TODO: data meaning
    	TStatusXBit           = 0,
    	BackLimitEnableBit    = 0,   // 1 enable, 0 disable
	    MiddleLimitEnableBit  = 1,
    	FrontLimitEnableBit   = 2,

    	// T-Y
    	// TODO: data meaning
    	TStatusYBit           = 3,
    	BackLimitStatusBit    = 3,   // 1: ??  0: ??
	    MiddleLimitStatusBit  = 4,
    	FrontLimitStatusBit   = 5,

    	// T-Z
    	// TODO: data meaning
    	TStatusZBit           = 6,
    	PusherStatusBit       = 6,
    	CutStatusBit          = 7,
    	LimitSelectBit        = 8,

    	AlarmBit              = 9,   // bit 9, 10, 11
    	PitchValueBit         = 12,  // bit 12, 13, 14
    	ZerorizeBit           = 15,  // 1: zero done; 0: zero not done
    	StopModeBit           = 16,
    	DirectionBit          = 17,  // 1: +,forward  0: -,backward
    	CurrentValueBit       = 18,  // bit 18 ~ 22
    };

    enum RawStatusMask {
    	// X
    	BackLimitEnableMask    = (1 << BackLimitEnableBit),
  	    MiddleLimitEnableMask  = (1 << MiddleLimitEnableBit),
  	    FrontLimitEnableMask   = (1 << FrontLimitEnableBit),
  	    // Y
    	FrontLimitStatusMask   = (1 << FrontLimitStatusBit),
    	MiddleLimitStatusMask  = (1 << MiddleLimitStatusBit),
    	BackLimitStatusMask    = (1 << BackLimitStatusBit),
    	// Z
    	LimitSelectMask        = (1 << LimitSelectBit),
    	CutStatusMask          = (1 << CutStatusBit),
    	PusherStatusMask       = (1 << PusherStatusBit),

    	AlarmMask              = (7 << AlarmBit),
    	PitchValueMask         = (7 << PitchValueBit),
    	ZerorizeMask           = (1 << ZerorizeBit),
    	StopModeMask           = (1 << StopModeBit),
    	DirectionMask          = (1 << DirectionBit),
    	CurrentValueMask       = (0x1f << CurrentValueBit),

    	// Frame Mask
    	TStatusXMask           = (7 << TStatusXBit),
    	TStatusYMask           = (7 << TStatusYBit),
    	TStatusZMask           = (7 << TStatusZBit),
    	TStatusAlarmMask       = (7 << AlarmBit),
    };

    int sendCommand(MotorCommand & command);
    int sendCommand(QByteArray &commandString);
    int resendLastCommand();
    int getRawStatus();

    int  getCheckSumErrorCount();
    void setCheckSumErrorCount(int value);

    bool isMotorPresent();

    // Functions to check rawStatus
    static bool isFrontLimitMaskChanged(int mask);
    static bool isMiddleLimitMaskChanged(int mask);
    static bool isBackLimitMaskChanged(int mask);

    static bool isFrontLimitStatusChanged(int mask);
    static bool isMiddleLimitStatusChanged(int mask);
    static bool isBackLimitStatusChanged(int mask);

    static bool isCutStatusChanged(int mask);
    static bool isPusherStatusChanged(int mask);
    static bool isLimitSelectionChanged(int mask);

    static bool isAlarmStatusChanged(int mask);
    static bool isPitchChanged(int mask);
    static bool isZerorizeStatusChanged(int mask);
    static bool isStopModeChanged(int mask);
    static bool isDirectionChanged(int mask);
    static bool isCurrentValueChanged(int mask);

    // Functions to get value from rawStatus
    // All these functions return RAW data from motor, MCP doesn't
    // know the meanings of these data
    static int getFrontLimitMask(int status);
    static int getMiddleLimitMask(int status);
    static int getBackLimitMask(int status);

    static int getFrontLimitStatus(int status);
    static int getMiddleLimitStatus(int status);
    static int getBackLimitStatus(int status);

    static int getCutStatus(int status);
    static int getPusherStatus(int status);
    static int getWhichLimit(int status);

    static int getAlarmCode(int status);

    static int getPitch(int status);
    static int getZerorize(int status);
    static int getStopMode(int status);

    static int getDirection(int status);
    static int getCurrent(int status);

    // convert functions
	static char toChar(int number);
    static int  toInt(bool *ok, char ch);

    // Test functions
#ifdef MOTOR_TEST_WIDGET_ENABLE
    void forceStatusChange(int cutStatus, int pusherStatus);
    void forcePositionChange(int pos);
#endif

signals:
	void statusEvents(int status, int mask);
	void positionEvent(int pos);
	void speedEvent(int speed);

private slots:
	void handleRawSerialFrame();
	void handleFrameCheckSumError();

private:
	int  rawStatus;
	int  preRawStatus;
	int  currentPos;
	int  currentSpeed;
	char MotorStatusHeader[MS_StatusNumbers];

	SerialPortFrame *spf;
	void initializeSerialPortFrame(const char *deviceName);

	MotorCommand lastCommand;

	// Parse Received Frame
	void parseTStatusFrame(QByteArray &frame);
	void parseFStatusFrame(QByteArray &frame);

	int checkSumErrorCount;
};

#endif /* MOTORCONTROLPROTOCOL_H_ */
