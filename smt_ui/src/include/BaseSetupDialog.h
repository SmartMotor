/*
 * BaseSetupDialog.h
 *
 *  Set/Adjust Motor Base
 *
 *  Created on: Jun 19, 2010
 *      Author: souken-i
 */
#ifndef BASESETUPDIALOG_H
#define BASESETUPDIALOG_H

#include <QtGui/QWidget>
#include <QLabel>
#include "EditDialog.h"
#include "LineEditVKeyboard.h"

class BaseSetupDialog : public EditDialog
{
    Q_OBJECT

public:
    BaseSetupDialog(QWidget *parent = 0);
    ~BaseSetupDialog();

    void retranslateUi();

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
	void doBaseSetup();

private:
    QLabel *baseLabel;
    LineEditVKeyboard *baseEdit;
};

#endif // BASESETUPDIALOG_H
