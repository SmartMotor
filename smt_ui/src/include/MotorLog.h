/*
 * MotorLog.h
 *
 * We save the log when alarm happens.
 *
 * There are several informations needed to be saved:
 *   - Alarm code
 *   - Date and Time
 *   - Motor Status (Auto-Run Mode, Manual Mode or ...)
 *   - Cut position
 *
 *  Created on: Apr 18, 2010
 *      Author: souken-i
 */

#ifndef MOTORLOG_H_
#define MOTORLOG_H_

#include <QString>

class MotorLog
{
public:
	MotorLog();
	virtual ~MotorLog();

	enum {
		MaxLogItems = 15,
	};

	void setLogFileName(QString &fileName);
	QString & getLogFileName();
	void appendAlarmLog(int alarmCode, int position);

private:
	QString _logFileName;
	QString _logData[MaxLogItems];
	int _savedLogNumber;
	int _nextItem;

	void setNewLogLine(QString &log, int alarmCode, int position);
	void loadLog();
	void saveLog();
};

#endif /* MOTORLOG_H_ */
