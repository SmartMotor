/*
 * TableView.h
 *
 *  Created on: 2010/03/07
 *      Author: crazysjf
 */

#ifndef PROGRAM_EDITOR_H_
#define PROGRAM_EDITOR_H_
#include <QtGui>
#include <QLabel>
#include <QTableView>
//#include <QStandardItemModel>
#include <TableModel.h>
#include <QScrollBar>
#include <QGridLayout>
#include <Program.h>
#include "TableView.h"

class ProgramEditor : public QWidget
{
	Q_OBJECT

public:
	ProgramEditor();
	QSize sizeHint() const;

	// Make sure to reference column numbers by these macros.
	enum {
		POSITION_COLUMN,
		FLAGS_COLUMN,
		COLUMN_NR
	};

	enum {
		POSITION_COLUMN_WIDTH = 200,
		FLAGS_COLUMN_WIDTH    = 200,
	};

	enum {
		RELATIVE_ADD = 0,
		RELATIVE_SUB,
		RELATIVE_MUL,
		RELATIVE_DIV,
	};

	void retranslateUi();

	/**
	 * Open and load a program file
	 *   @fileName: Full path of the file name
	 *   @retval: non-0 on success, 0 on failure.
	 */
	int openProgramFile(QString &fileName);
	int saveProgramFile();
	Program & getCurrentProgram();

	/**
	 * select the first or the last row
	 */
	void topIndex();
	void lastIndex();

	/**
	 * Get the total data rows
	 *   @retval: row count
	 */
	int  getTotalDataNumber();

	/**
	 * Return TableView widget pointer
	 */
	QWidget *tableView();
    void forceKeyPressEvent(QKeyEvent *event);

signals:
	void dataIndexChanged(int total, int step);

public slots:
	void zoomInTableFont();
	void zoomOutTableFont();

	void insertData(int pos, int flags = 0);
	void modifyData(int pos, int flags = 0);
	void appendData(int pos, int flags = 0);
	void adjustPositionAll(int delta);
	void deleteData(void);
	void deleteAllData(void);
	void labelInsert(int totalLen, int labelLen, int unusedLen);
	void averageInsert(int totalLen, int divider, int mode = 0);

	/**
	 * Relative edit data and append data to last
	 * @param: delta: input data
	 * @param: operation_mode: add/sub/multiply/divide
	 */
	void relativeAppend(int delta, int operation_mode);

	void toggleFlag(int flagIndex);
	void clearFlag(int flagIndex);

	/**
	 * Clear all flags of current selected data
	 */
	void clearFlags(void);

	void getCurrentData(int *pos, int *flag);
	void getNextData(int *pos, int *flag, bool *last);

private slots:
	void handleSelectionChanged ( const QItemSelection & selected, const QItemSelection & deselected );

private:
	void insertDataRow(int pos, int flags, int row);
	Program currentProgram;
	TableView view;
	TableModel model;

	void setupItems(QStandardItem *position, const int p,
			QStandardItem *flags, const int f);
	void setTableFontSize(int step);
};

#endif /* TABLEVIEW_H_ */
