/*
 * AverageEditor.h
 *
 *  Created on: Jun 4, 2010
 *      Author: souken-i
 */

#ifndef AVERAGEEDITOR_H_
#define AVERAGEEDITOR_H_

#include <QtGui/QDialog>
#include <QLabel>
#include <QLineEdit>

#include "EditDialog.h"
#include "PushButton.h"
#include "LineEditVKeyboard.h"

class AverageEditor : public EditDialog
{
    Q_OBJECT

public:
	AverageEditor(QWidget *parent = 0);
	virtual ~AverageEditor();

    void retranslateUi();

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
	/**
	 * Slot for F1_DONE
	 *
	 * Check the input data, if all data is
	 * valid, exit this dialog; otherwise clear the invalid data
	 * and set focus to it
	 *
	 * The input data is put into global variable labelEditData
	 * structure.
	 */
	void handleInputData();

	/**
	 * Set Average Number Mode
	 */
	void setAverageNumberMode();

	/**
	 * Set Average Value Mode
	 */
	void setAverageValueMode();

private:
	void setAverageMode(int mode);
	void updateModeButtons();

    enum {
    	TOTAL_LENGTH  = 0,
    	AVERAGE_DATA,
    	AVERAGE_EDIT_ITEM_NR,
    };

    QLabel *descLabels[AVERAGE_EDIT_ITEM_NR];
    LineEditVKeyboard *inputEdits[AVERAGE_EDIT_ITEM_NR];

    PushButton *numberButton, *valueButton;
};

#endif /* AVERAGEEDITOR_H_ */
