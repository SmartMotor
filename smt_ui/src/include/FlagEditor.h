/*
 * FlagEditor.h
 *
 *  Created on: 2010/04/01
 *      Author: crazysjf
 */

#ifndef FLAGEDITOR_H_
#define FLAGEDITOR_H_

#include <QtGui>
#include "Program.h"

class FlagEditor : public QWidget
{
	Q_OBJECT

public:
	FlagEditor(QWidget *parent = 0 );
	FlagEditor(int flags, QWidget *parent = 0);
	~FlagEditor();

	// flag id definitions
	// groud_id = flag_id / 3
	// FLAG_NR must be less or equal than POSITION_SHIFT
	enum Flags {
		// Group 0
		FLAG_PUSHER = 0,
		FLAG_CUT,
		FLAG_PRESS,

		// Group 1
		FLAG_AIR,
		FLAG_UNUSED1,
		FLAG_UNUSED2,

		// Group 2
		FLAG_ALIGN,
		FLAG_UNUSED3,
		FLAG_UNUSED4,

		// Group 3
		FLAG_TURNAROUND,
		FLAG_TURNRIGHT,
		FLAG_TURNLEFT,

		FLAG_NR
	};

	enum FlagGroup {
		FLAG_GROUP_NR   = 4,
		FLAG_GROUP_BITS = 3,    // Every group has 3 exclusive flags
		FLAG_GROUP_MASK = 0x7,  // Every group has 3 exclusive flags
	};

	static void paint(QPainter *painter, const QRect &rect, const QPalette &palette, int flags);
	QSize sizeHint() const;

	int flags(void) { return _flags; }
	void setFlags(int flags) { _flags = flags; }
	static QRect paintingRect;
	static QString flagImageFiles[FLAG_NR];
	static QImage scaledFlagimages[FLAG_NR];

protected:
    void paintEvent(QPaintEvent *event);
private:
    int _flags;
};




#endif /* FLAGEDITOR_H_ */
