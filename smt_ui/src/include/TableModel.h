/*
 * TableModel.h
 *
 *  Created on: 2010/11/08
 *      Author: kinhi
 *      由于QStandardItemModel的插入速度太慢，需要自己实现一个模型。该模型使用QList存储内部数据，而且支持批量插入。
 *
 *      问题：
 *        1) Q_OBJECT子类不能是模板类，目前的数据结构导致列数固定，不能调节。
 */

#ifndef TABLEMODEL_H_
#define TABLEMODEL_H_
#include <QtGui>
#include <QAbstractTableModel>
#include <QVariant>
#include <QList>
#include <QStringList>
#include <QDebug>

const static int COLUMN_NUM = 2; //该模型的列数

class Row {
public:
	int columns[COLUMN_NUM];
	friend	QDebug operator<<(QDebug dbg, const Row &data);
};

class TableModel : public QAbstractTableModel {
	Q_OBJECT

public:
	TableModel() {}
	virtual ~TableModel() {};

	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	int columnCount (const QModelIndex &parent = QModelIndex()) const;
	QVariant data ( const QModelIndex & index, int role = Qt::DisplayRole ) const;
	Qt::ItemFlags flags(const QModelIndex &index) const;

	//
	// 该函数直接把值设为整形数，而不是通过字符串去转。
	// @value中应该保存一个整型数。
	//
	bool setData(const QModelIndex &index, const QVariant &value, int role);

	//
	// 显示表头。水平和垂直要分开处理。
	//   垂直的表头显示行号，从1开始。
	//
	QVariant headerData(int section, Qt::Orientation orientation, int role) const;

	void setHorizontalHeaderLabels(const QStringList &labels)
	{
		headerLabels = labels;
	}

	//
	// 插入行
	//   PS: 这个函数好像没什么用。如果用如下方式插入一行：
	//       model.insertRow(row);
	//       model.setData(row...);
	//   View上的内容竟然不会自动改变。
	//
	bool insertRow (int row, const QModelIndex & parent = QModelIndex());

	//
	// 插入一行数据。
	//   数据的各列在@rowData中。@rowData中超过模型中的列数的部分将被舍弃，
	//
	// 插入单行数据用此函数比较方便。
	//
	bool insertRow (int row, const QList<int> &rowData);

	//
	// 插入多个空行。空行中的数据需要用setData()去编辑。
	//
	bool insertRows(int position, int rowNum);

	//
	// 从position处插入多行
	//   虽然说是插入，但实际上是追加，因为QList不支持merge操作。如果要实现任意位置的插入，建议使用std::list。
	//
	bool insertRows(int position, const QList<Row> rows);

	bool removeRows(int position, int rows);

private:
	const static int _columnNum = COLUMN_NUM;
	QList<Row> _data;
	QStringList headerLabels;
};

#endif /* TABLEMODEL_H_ */
