/*
 * MotorParameters.h
 *
 *  Created on: Mar 21, 2010
 *      Author: souken-i
 */

#ifndef MOTORPARAMETERS_H_
#define MOTORPARAMETERS_H_

#include <QtGui>

class MotorParameters : public QObject
{
public:
    MotorParameters();
    MotorParameters(const QString &filename);
    virtual ~MotorParameters();

    enum PitchTypes {
    	Pitch_10MM_2Rate = 0,  // command data = 001000
    	Pitch_12MM_2Rate = 1,  // command data = 001100
    	Pitch_10MM_3Rate = 2,  // command data = 001200
    	Pitch_12MM_3Rate = 3,  // command data = 001300
    	Pitch_10MM_1Rate = 4,  // command data = 001400
    	Pitch_12MM_1Rate = 5,  // command data = 001500
    	MaxPitchTypes,
    };

    enum ScrewTypes {
    	Normal_Screw = 0,  // 10 MM
    	Ball_Screw, 	   // 12 MM
    	MaxScrewTypes,
    };

    enum ScrewReductionRatio {
    	ScrewRation_1To1    = 0,
    	ScrewRation_1To2,
    	ScrewRation_1To3,
    	MaxScrewRations,
    };

    int  getFrontLimit();
    void setFrontLimit(int value);
    int  getMiddleLimit();
    void setMiddleLimit(int value);
    int  getBackLimit();
    void setBackLimit(int value);
    int  getMaxLimit();
    int  getPusherSpeed();
    void setPusherSpeed(int speed);
    int  getMinPusherSpeed();
    int  getMaxPusherSpeed();
    int  getRunSpeed();
    void setRunSpeed(int speed);
    int  getMinRunSpeed();
    int  getMaxRunSpeed();
    int  getInitialPos();
    void setInitialPos(int position);
    int  getPitch();
    void setPitch(int pitch);
    void setConfigFile(const QString &filename);
    bool loadConfigFile(const QString &fileName);
    bool load();
    bool save();
    bool saveToSystem();
    int  pos();
    void setPos(int position);
    int  getScrewType();
    void setScrewType(int type);
    int  getScrewRatio();
    void setScrewRatio(int ratio);
    int  getAirStopDistance();
    void setAirStopDistance(int distance);
    int  getStepSize();
    void setStepSize(int size);

    int pitchTypeToValue(PitchTypes type);
    int speedMPerSecondToRPM(int meterPerSecond);

private:
    enum {
    	ItemInitialPos      = 0,
    	ItemFrontLimit      = 1,
    	ItemMiddleLimit     = 2,
    	ItemBackLimit       = 3,
    	ItemMaxLimit        = 4,
    	ItemPusherSpeed     = 5,   /* unit: m/s */
    	ItemMinPusherSpeed  = 6,
    	ItemMaxPusherSpeed  = 7,
    	ItemRunSpeed        = 8,   /* Jog Run Speed */
    	ItemMinRunSpeed     = 9,
    	ItemMaxRunSpeed     = 10,
    	ItemPitch           = 11,  /* Encoded from type and ratio */
    	ItemAirStopDistabce,       /* 0 - 20 mm */
    	ItemStepSize,              /* 1 - 20 mm */
    	MotorParameterItems,
    };
    int motorParasArray[MotorParameterItems];
    QString configFile;/* User can have his own config file */
    QString sysConfigFile;  /* system wide config file */

    // Run-time parameters, don't need to save to config file
    int _currentPos;
    int _screwType;
    int _screwRatio;

    bool loadFile(const QString &fileName);
    bool saveFile(const QString &fileName);
    void setDefaultParameters();
    void setParametersFromConfigFile(int *dataArray);
    void getCurrentParameters(int *dataArray);
};

#endif /* MOTORPARAMETERS_H_ */

/* Local Variables:  */
/* c-basic-offset: 4 */
/* End:              */
