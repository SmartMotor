/*
 * FnButtonsWidget.h
 *
 *  Emit SmtKb::Key_F1 ~ SmtKb::Key_F6 signals when button is clicked
 *
 *  Created on: Jun 28, 2010
 *      Author: souken-i
 */

#ifndef FNBUTTONSWIDGET_H_
#define FNBUTTONSWIDGET_H_

#include <QWidget>
#include <QStackedWidget>
#include <QIcon>
#include <QSignalMapper>

#include "Button.h"

class FnButtonsWidget : public QStackedWidget
{
	Q_OBJECT

public:
	FnButtonsWidget(QWidget *parent = 0);
	virtual ~FnButtonsWidget();

signals:
	void clicked(int key_code);

private:
	Button * createButton(int id , const QIcon &icon);
	QWidget * createFnButtonsWidget(int nr, const QString *iconFiles, Button **fnButtons);

	enum {
		EMD_F1_LabelEdit   = 0,
		EMD_F2_AverageEdit,
		EMD_F3_FileSelect,
		EMD_F4_PusherFlag,
		EMD_F5_AutoCutFlag,
		EMD_F6_Calculator,
		EditModeFnButtonNumber,
	};

	enum {
		AMD_F1_ClearCounter = 0,
		AMD_F2_BaseSetup,
		AMD_F3_FileSelect,
		AutoRunModeFnButtonNumber,
	};

	enum {
		MMD_F1_Setting      = 0,
		MMD_F2_Maintenance,
		MMD_F3_BaseSetup,
		MMD_F4_ClearCounter,
		MMD_F5_SensorStatus,
		MMD_F6_Calculator,
		ManualRunModeFnButtonNumber,
	};

	static QString editFnIconFiles[EditModeFnButtonNumber];
	static QString autoFnIconFiles[AutoRunModeFnButtonNumber];
	static QString manualFnIconFiles[ManualRunModeFnButtonNumber];

	QSignalMapper *signalMapper;

	Button *editFnButtons[EditModeFnButtonNumber];
	Button *autoFnButtons[AutoRunModeFnButtonNumber];
	Button *manualFnButtons[ManualRunModeFnButtonNumber];

	QWidget *editFnButtonsPage;
	QWidget *autoFnButtonsPage;
	QWidget *manualFnButtonsPage;
};

#endif /* FNBUTTONSWIDGET_H_ */
