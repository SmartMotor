/*
 * TreeView.h
 *
 *  Created on: 2010/03/28
 *      Author: crazysjf
 */

#ifndef TREEVIEW_H_
#define TREEVIEW_H_
#include <QtGui>

class TableView : public QTableView {
	Q_OBJECT
public:
	TableView(QWidget *parent = 0);
	virtual ~TableView();

	/**
	 * Even when this table view has no focus, we can move its cursor up/down
	 */
	void moveCursorUp();
	void moveCursorDown();
	void moveCursorPageUp();
	void moveCursorPageDown();

public slots:
	void adaptColumns (const QModelIndex & topleft, const QModelIndex &bottomRight);

signals:
	void selectionChangedSignal(const QItemSelection &, const QItemSelection &);

protected:
	void selectionChanged ( const QItemSelection & selected, const QItemSelection & deselected );
};

#endif /* TREEVIEW_H_ */
