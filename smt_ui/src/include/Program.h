/*
 * Program.h
 *
 *  Created on: 2010/03/21
 *      Author: crazysjf
 */

#ifndef PROGRAM_H_
#define PROGRAM_H_
#include <QVector>
#include <QString>
#include <Debug.h>
#include <QFile>

/**
 * Class program: The memory presentation of a program file
 *
 *  To load a program file
 *	   Program *p = new Program(<file name>);
 *     p -> loadFile();
 *     ...
 *     delete p;
 *
 *  To save to program file
 *     p -> save();
 *
 *  To crate a program file
 *     Program *p = new Program(<file name>);
 *     p->init();
 *     ...
 *     p -> save();
 *     delete p;
 *
 *  To update load date history
 *     Program *p = new Program(<file name>);
 *     p->updateLoadDateHistory();
 *     delete p;
 *
 *  ===================
 *  Program File Format
 *  ===================
 *
 *  | _header           |  _description    | _cuts          |
 *  | <fixed size>      | <variable size>  | <variable size |
 *
 *  _header: see struct _header.
 *    cutOffset in _header is the offset of the first cut from the beginning of the file.
 *    loadDateHistory[] keeps last file load dates. When user load this file, this should be updated.
 *
 *      NOTE: Not every time Program::loadFile() is called should this function be called.
 *            Program::loadFile() is called in many situations, but we only care about user's actual load action.
 *
 *  _description
 *    Length of _description is variable.
 *
 *  _cuts Format
 *  |<-------------- _cuts ------------->|
 *  | _cuts[0] | _cuts[1] | ....         |
 *  |   4B     |    4B    | ....         |
 *
 *  _cuts[i] Format
 *  |31                      12|11         0|
 *  |       position           |   flags    |
 *
 *  position: 20 bits. Unit: 0.01mm
 *  flags:    12 flags
 */

const static int MAGIC_LENGTH = 4;
const static int FILE_LOAD_DATE_HISTORY_LENTH = 3; /*< Number of file load date that is stored. */

struct UserProFileHeader {
	char magic[MAGIC_LENGTH];	// Should always be "SMT\0"
	int fileflags;				// file flags
	int isnull;					// this file contain NO data
	int id;						// id, file number
	int useCount;				// used times
	uint loadDateHistory[FILE_LOAD_DATE_HISTORY_LENTH]; // Last load dates. [0] is the newest. Bigger index is older.
	int cutOffset;				// Offset of the first cut
};

// Data of 1 cut
class Cut {
	const static int POSITION_SHIFT = 12;
	const static int FLAG_MASK = ((1 << POSITION_SHIFT) - 1);
	const static int POSITION_MASK = ~FLAG_MASK;
public:
	Cut() : position(0), flags(0) {
	};

	// build a cut from encodedData
	Cut(const int encodedData) {
		position = (encodedData & POSITION_MASK) >> POSITION_SHIFT;
		flags = encodedData & FLAG_MASK;
	}

	// build a cut from position and flags
	Cut(const int _position, const int _flags) : position(_position), flags(_flags) {
	}

	~Cut() {};

	// Return the encoded data
	int encode(void) {

		int ret = (position << POSITION_SHIFT) | (flags & FLAG_MASK);
		return ret;
 	}

	int position;
	int flags;
};


class Program : public QObject {
	Q_OBJECT
public:
	Program(void);
	Program(const QString &fileName);

	virtual ~Program();

	void init(void);

	/**
	 * Load a program from file.
	 * The file name is set in constructor.
	 * @retval: non-0 on success. 0 on failure.
	 */
	int load(void);

	/**
	 * Save the program to the file.
	 * @retval: non-0 on success. 0 on failure.
	 */
	int save(void);

	/**
	 * Update this program's load date history, and write it to file.
	 */
	void updateLoadDateHistory();
	void setFileName(const QString &name);
	UserProFileHeader &header(void) { return _header; }
	QVector<Cut> &cuts(void) { return _cuts; }
	QString &description(void) {return _description; }
	void setDescription(const QString &s) {_description = s; }
	int cutCount() { return _cuts.count(); };
	int increaseUseCount(void) { return ++_header.useCount; }

private:
	QString fileName;  				// Full path file name
	UserProFileHeader _header;
	QString _description;			// File description. NO need to be NULL-terminated.
	QVector<Cut> _cuts;
};


#endif /* PROGRAM_H_ */
