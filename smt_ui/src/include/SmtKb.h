/*
 * SmtKb.h
 *   SMT Keyboard Code Table
 *
 *   See SmartMotor/Hardware/Doc/smt255-keyboard-mapping.txt
 *
 *  Created on: Apr 24, 2010
 *      Author: souken-i
 */

#ifndef SMTKB_H_
#define SMTKB_H_

#include <Qt>

class SmtKb {
public:
	SmtKb();
	virtual ~SmtKb();

	enum Key {
		Key_F1     = Qt::Key_F1,
		Key_F2     = Qt::Key_F2,
		Key_F3     = Qt::Key_F3,
		Key_F4     = Qt::Key_F4,
		Key_F5     = Qt::Key_F5,
		Key_F6     = Qt::Key_F6,

		Key_0      = Qt::Key_0,
		Key_1      = Qt::Key_1,
		Key_2      = Qt::Key_2,
		Key_3      = Qt::Key_3,
		Key_4      = Qt::Key_4,
		Key_5      = Qt::Key_5,
		Key_6      = Qt::Key_6,
		Key_7      = Qt::Key_7,
		Key_8      = Qt::Key_8,
		Key_9      = Qt::Key_9,

		Key_Period    = Qt::Key_Period,
		Key_Plus      = Qt::Key_Plus,
		Key_Minus     = Qt::Key_Minus,
		Key_Div       = Qt::Key_Slash,
		Key_Multiply  = Qt::Key_Asterisk,
		Key_Equal     = Qt::Key_Equal,

		Key_Backspace = Qt::Key_Backspace,
		Key_Insert    = Qt::Key_Insert,
		Key_Modify    = Qt::Key_F7,
		Key_Enter     = Qt::Key_Return,

		Key_Up        = Qt::Key_Up,
		Key_Down      = Qt::Key_Down,
		Key_Left      = Qt::Key_Left,
		Key_Right     = Qt::Key_Right,
		Key_PageUp    = Qt::Key_PageUp,
		Key_PageDown  = Qt::Key_PageDown,

		Key_Delete    = Qt::Key_Delete,
		Key_Exit      = Qt::Key_End,

		Key_Program   = Qt::Key_F8,  // Program File

		Key_Forward   = Qt::Key_F9,
		Key_Backward  = Qt::Key_F10,
		Key_Run       = Qt::Key_F11,
		Key_Help      = Qt::Key_F12,
		Key_Tab       = Qt::Key_Tab,

		Key_Clear     = Qt::Key_F17,
		Key_Stop      = Qt::Key_F18,

		Key_Pusher    = Qt::Key_F16,
		Key_Edit      = Qt::Key_F13,
		Key_Manual    = Qt::Key_F14,
		Key_Auto      = Qt::Key_F15,
		Key_AutoCut   = Qt::Key_F22,
		Key_Traning   = Qt::Key_F21,

		Key_FastForward   = Qt::Key_F19,
		Key_FastBackward  = Qt::Key_F20,
	};

	/**
	 *	Soft KeyPad Code Mapping
	 */
	enum KeyPadMapping {
		KP_DeleteOneCut          = 0x01,
		KP_DeleteFlags,
		KP_Insert,
		KP_Modify,

		KP_ToggleTurnLeftFlag,
		KP_ToggleTurnRightFlag,
		KP_ToggleTurnAroundFlag,
		KP_DeleteAll,

		KP_TogglePressFlag,
		KP_TogglePusherFlag,
		KP_ToggleAirFlag,
		KP_ToggleAutoCutFlag,

		// Append: Edit and Auto Mode
		KP_RelativeAdd,
		KP_RelativeSub,
		KP_RelativeMultiply,  // These two are for Edit Mode
		KP_RelativeDivide,

		KP_AdjustAddAll,    // ++
		KP_AdjustSubAll,    // --

		// These won't change data in program file, Manual Mode
		KP_RelativeInputAdd,
		KP_RelativeInputSub,
		KP_RelativeInputMultiply,
		KP_RelativeInputDivide,

		KP_Hide,
		KP_ZoomIn,
		KP_ZoomOut,
	};
};

#endif /* SMTKB_H_ */
