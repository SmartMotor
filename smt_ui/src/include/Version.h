/*
 * Version.h
 *
 *  Created on: 2010/06/08
 *      Author: Zeng Xianwei
 */

#ifndef VERSION_H_
#define VERSION_H_

/**
 * Version contains three levels:
 *   - Major version
 *   - Patch version
 *   - Sub version
 *
 * The basic rules to update version are:
 *   - Update Major version for large release
 *   - Update Patch version for important new functions
 *   - Update Sub version for minor bug fix
 *
 * The version starts from 0.9.1
 */
#define SMT_VERSION      "0.9.5"

#endif /* VERSION_H_ */
