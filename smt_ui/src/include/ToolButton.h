/*
 * ToolButton.h
 *
 *  Created on: Sep 12, 2010
 *      Author: souken-i
 */

#ifndef TOOLBUTTON_H_
#define TOOLBUTTON_H_

#include <QToolButton>

class ToolButton : public QToolButton
{
public:
	ToolButton(QWidget *parent = 0);
	virtual ~ToolButton();

protected:
	void mousePressEvent ( QMouseEvent * event );
	void mouseReleaseEvent ( QMouseEvent * event );
};

#endif /* TOOLBUTTON_H_ */
