/*
 * StartScreen.h
 *
 *  Created on: Apr 27, 2010
 *      Author: souken-i
 */

#ifndef STARTSCREEN_H_
#define STARTSCREEN_H_

#include <QDialog>
#include <QPixmap>
#include <QLabel>

#include "ToolButton.h"

class StartScreen : public QDialog
{
	Q_OBJECT

public:
	StartScreen(QWidget *parent = 0);
	virtual ~StartScreen();

	void retranslateUi();

protected:
	void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent *event);

public slots:
	void start();
	void checkMotorPresent();

private slots:
	void touchscreenCalibration();
	void showHelp();
	void keyboardCheck();

private:
	ToolButton *runButton;
	QPixmap     pixmap;
	QLabel      *startLabel;
	QLabel      *helpLabel;
	int         _subDialogEnable;
};

#endif /* STARTSCREEN_H_ */
