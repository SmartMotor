/*
 * MotorTestWidget.h
 *
 *  Created on: Jun 8, 2010
 *      Author: souken-i
 */

#ifndef MOTORTESTWIDGET_H_
#define MOTORTESTWIDGET_H_

#include <QWidget>
#include <QLabel>
#include <QTimer>

#include "RoundRectWidget.h"
#include "PushButton.h"
#include "Debug.h"

#ifdef MOTOR_TEST_WIDGET_ENABLE
class MotorTestWidget : public RoundRectWidget
{
	Q_OBJECT

public:
	MotorTestWidget(QWidget *parent = 0);
	virtual ~MotorTestWidget();

	void retranslateUi();

public slots:
	void toggleCutStatus();
	void togglePusherStatus();
	void currentPosTest();

private slots:
	void setCurrentPos();

private:
	PushButton *cutButton;
	PushButton *pusherButton;
	PushButton *posButton;

	int _cutStatus;
	int _pusherStatus;
	int _currentPos;
	int _direction;

	QTimer *timer;

	void updateButtons();
};

#endif /* MOTOR_TEST_WIDGET_ENABLE */

#endif /* MOTORTESTWIDGET_H_ */
