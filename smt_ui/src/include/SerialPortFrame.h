/*
 * SerialPortFrame.h
 *
 *  Created on: Mar 24, 2010
 *      Author: souken-i
 */

#ifndef SERIALPORTFRAME_H_
#define SERIALPORTFRAME_H_

#include <QMutex>
#include <QThread>

#include <termios.h>


class SerialPortFrame : public QThread
{
    Q_OBJECT

public:
    SerialPortFrame();
    virtual ~SerialPortFrame();

    void run();
    void stopWorkThread();
    int open(const char *portname);
    bool isOpen();

    QByteArray & readFrame();
    int writeFrame(QByteArray &buffer);

    bool isFrameHeader(char ch);
    bool isFrameTail(char ch);
    bool isReceivedFrameValid(QByteArray & buffer);
    void handleReceivedFrame(QByteArray & buffer);

    /*
     *  Serial Frame Format
     *    0       1 ~ 9            10   11
     *  +----+-------------------+----+----+
     *  | %  |     Data          | Sum|  B |
     *  +----+-------------------+----+----+
     *
     *  SerialPortFrame class doesn't care about the data
     *  except for computing the checksum
     */
    enum {
    	FrameHeaderIndex   = 0,   	// Byte 0

    	// Tx
    	TxFrameDataLength    = 9,		// Byte 1 ~ 9
    	TxFrameCheckSumIndex = 10,	// Byte 10
    	TxFrameTailIndex     = 11,	// Byte 11

    	// Rx
    	RxFrameDataLength    = 10,		// Byte 1 ~ 10
    	RxFrameCheckSumIndex = 11,	    // Byte 11

    	FrameLength        = 12
    };

    static char computeChecksum(QByteArray & buffer, int length);

#if 0
    // TODO Clear me, it is for test ONLY
public slots:
    void sendFrameTest();
#endif

signals:
    void readyRead();
    void checksumError();

private:
    int  fd;
    int  portStatus;
    char _frameHeader;
    char _frameTail;
    bool threadShouldStop;
    struct termios old_termios;
    struct termios serialOptions;
    QMutex *mutex;
    QByteArray recvBuffer;
};

#endif /* SERIALPORTFRAME_H_ */

/* Local Variables:  */
/* c-basic-offset: 4 */
/* End:              */
