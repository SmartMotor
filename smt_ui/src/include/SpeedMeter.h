/*
 * SpeedMeter.h
 *
 *  Created on: Apr 20, 2010
 *      Author: souken-i
 */

#ifndef SPEEDMETER_H_
#define SPEEDMETER_H_

#include <QObject>
#include "manometer.h"

class SpeedMeter : public ManoMeter
{
	Q_OBJECT

public:
	SpeedMeter(QWidget *parent = 0);
	virtual ~SpeedMeter();

public slots:
	void updateSpeed(int speed);
};

#endif /* SPEEDMETER_H_ */
