/*
 * mainFrame.h
 *
 *  Created on: 2010/02/03
 *      Author: crazysjf
 */

#ifndef MAINFRAME_H_
#define MAINFRAME_H_

#include <QVBoxLayout>
#include <QPainter>
#include <QLabel>
#include <QListView>
#include <QTableView>
#include <QStandardItemModel>
#include <QHeaderView>
#include <QScrollBar>
#include <QRect>

#include "HeaderLabel.h"
#include "PaperPusherBar.h"
#include "ProgramEditor.h"
#include "Button.h"
#include "OpenFileDialog.h"
#include "Calculator.h"
#include "CurrentMeter.h"
#include "SpeedMeter.h"
#include "MachineSettingDialog.h"
#include "LabelEditor.h"
#include "AverageEditor.h"
#include "LineEditVKeyboard.h"
#include "SensorStatusWidget.h"
#include "BaseSetupDialog.h"
#include "EditAutoModeKeyPadWidget.h"
#include "ManualModeKeyPadWidget.h"
#include "FnButtonsWidget.h"

class MainFrame : public QWidget
{
	Q_OBJECT
public:
	MainFrame();
	~MainFrame();

	enum {
		// 5 major modes which have quick button on left side of screen
		ModeProgramEdit     = 0,
		ModeManualRun       = 1,
		ModeAutoRun         = 2,
		ModeTraining        = 3,
		ModeAutoCutRun      = 4,

		MaxMajorModeNumber  = 5,

		ModeProgramSelect   = 5,

		MaxDisplayModeNumber= 6,  // Above 6 modes changes display layout

		MinorModeDefault,         // Default minor mode
	};

	// Stacked widget page index
	enum {
		UnusedPageIndex          = 255,
		UnchangedPageIndex       = 254, // Keep the same as previous index
	};

	enum TopPageLayout {
		CommonTopPageIndex     = 0,
		FileSelectTopPageIndex = 1,
	};

	enum MainWindowPages {
		DataListPageIndex    = 0,
		MotorStatusPageIndex = 1,
		BaseSetupPageIndex   = 2,
		MaxMainPages         = 3
	};

	enum FnButtonPages {
		EditButtonPageIndex  = 0,
		AutoRunButtonPageIndex,
		ManualRunButtonPageIndex,
		MaxButtonPages
	};

	enum InputLinePages {
		EditInputPageIndex      = 0,
		RunInputPageIndex,
		MaxInputLinePages,
	};

	enum KeyPadPages {
		EditKeyPadPageIndex = 0,
		MetersPageIndex,
		MaxKeyPadPages,
	};

	enum {
		AutoCutStatusIcon = 0,
		SpeakerStatusIcon = 1,
		CutStatusIcon     = 2,
		PusherStatusIcon  = 3,
		MaxStatusIcons    = 4,
	};

	enum {
		SpeakerOff = 0,
		SpeakerOn,
	};

	void retranslateUi();

public slots:
	void machineSetting();
	void runCalculator();
	void runLabelEditor();
	void runAverageEditor();
	void setProgramEditMode();
	void setProgramSelectMode();
	void setAutoRunMode();
	void setManualRunMode();
	void setAutoCutRunMode();
	void setTrainingMode();
	void loadProgramFile();

	// Data Edit SLOT
	void insertData();
	void modifyData();
	void adjustDataAdd();
	void adjustDataSub();
	void deleteData();
	void deleteAllData();
	void save();

	void relativeAdd();
	void relativeSub();
	void relativeMultiply();
	void relativeDivide();

	void clearInput();  // Clear button
	void enterInput();  // OK/Enter button

	// Motor Control Related Slots
	// If one cut loop is finished, run to next
	void runToNext();
	void run();
	void stop();
	void runToPos(int pos);
	void autoRunStart();
	void manualRunStart();
	void runOneCut(int pos, int flag);

	// Toggle flag slots
	void toggleAirFlag();
	void togglePusherFlag();
	void toggleCutFlag();
	void toggleAlignFlag();
	void togglePressFlag();
	void toggleTurnAroundFlag();
	void toggleTurnRightFlag();
	void toggleTurnLeftFlag();

	// Status
	void updateCutStatus(int status);
	void updatePusherStatus(int status);

	void clearCutCount();

	// For debug
	void dummySlot();      // does nothing, need implementation

protected:
    void keyPressEvent(QKeyEvent *event);
    bool eventFilter(QObject *target, QEvent *event);
	void paintEvent(QPaintEvent *event);

private slots:
	void toggleSensorStatusImage();
	void runBaseSetup();
	void keyPadPressedEvent(int key_code);
	void fnButtonPressedEvent(int key_code);
	void handleVirtualKeyBoardShowEvent();

private:
	void loadLastOpenFile();
	void updateFileInfo(QString &fileName);
	void updateFontSizeColor();
	void loadStyleSheet(const QString & fileName);

	void openProgramFile(QString &fileName);

	QLabel *statusBar();
	void updateStatusBar();
	void updateAutoCutStatus();
	void updateSpeakerStatus(int status);

	/**
	 * update mode buttons when mode changed
	 */
	void updateModeButtons();

	// Widget Pages
	void createModeButtons();
	void createEditModeFnButtonsPage();
	void createAutoRunModeFnButtonsPage();
	void createMaunalRunModeFnButtonsPage();
	void createInputLineWidget();
	void createStatusBar();
	void createCommonTopPage();
	void createMetersPage();

	// Stacked Widgets
	void createTopPagesWidget();
	void createMainPagesWidget();
	void createKeypadPagesWidget();
	void createFnButtonPagesWidget();

	Button *createButton(const QIcon &icon, const QObject *receiver, const char *member, const QString &text = 0);

	void setMode(int mode);
	void setDisplayMode(int mode);
	void setStackedWidgetIndex(QStackedWidget *widget, int index);
	void setInputWidgetIndex(int index);

	/**
	 * Get input data from input edit box
	 */
	int  getInputData();

	void relativeInputAdd();
	void relativeInputSub();
	void relativeInputMultiply();
	void relativeInputDivide();

	// Keyboard Event Handlers
	void handleKeyPressF1();
	void handleKeyPressF2();
	void handleKeyPressF3();
	void handleKeyPressF4();
	void handleKeyPressF5();
	void handleKeyPressF6();

	// Stacked Widget Pointers
	QStackedWidget *topPagesWidget;
	QStackedWidget *mainPagesWidget;
	QStackedWidget *keypadPagesWidget;
	FnButtonsWidget *fnButtonPagesWidget;

	// Child Dialog
	Calculator *calculator;
	LabelEditor *labelEditorDialog;

	// Widget definitions
	HeaderLabel *headerLabel;
	PaperPusherBar *paperPusherBar;
	Button *modeButton[MaxMajorModeNumber];
	SensorStatusWidget *sensorStatusWidget;

	QWidget *commonTopPage;
	OpenFileDialog *fileSelectPage;
	ProgramEditor  *programEditor;

	EditAutoModeKeyPadWidget *editAutoModeKeyPadWidget;
	ManualModeKeyPadWidget *manualModeKeyPadWidget;

	QLabel *statusIconLabel[MaxStatusIcons];
	QLabel *statusBarLabel;

	QWidget  *metersWidget;
	CurrentMeter *currentMeter;
	SpeedMeter   *speedMeter;

	MachineSettingDialog *machineSettingDialog;

	int _majorMode;
	int _minorMode;
	int _previousMajorMode;
	int _currentDestination;

	// Input Line
	QWidget *inputWidget;
	QLabel  *currentModeLabel;
	QLabel  *inputGuideLabel;
	LineEditVKeyboard *inputBox;
	Button *clearButton;
	Button *okButton;
	Button *runButton;
	Button *stopButton;

	QWidget *inputButtonPage;  /* OK button */
	QWidget *runButtonPage;    /* Run/Stop button */
	QStackedWidget *inputButtonPagesWidget;

	QPixmap bkgndPixmap;
};

#endif /* MAINFRAME_H_ */
