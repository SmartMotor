/*
 * Debug.h
 *
 *  Created on: 2010/03/28
 *      Author: crazysjf
 */

#ifndef DEBUG_H_
#define DEBUG_H_

/*
 * Debug Macro
 */
//#define START_DEBUG

/**
 * Enable this macro to enable test widget which can simulate
 * cut up/down and pusher up/down signals
 */
//#define MOTOR_TEST_WIDGET_ENABLE

#endif /* DEBUG_H_ */
