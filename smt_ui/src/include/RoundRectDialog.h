#ifndef EDITORDIALOGTEMPLATE_H
#define EDITORDIALOGTEMPLATE_H

#include <QtGui/QDialog>
#include "Button.h"

/**
 * A round rectangle dialog. See also RoundRectWidget.
 * TODO:
 *   The code making it a round corner dialog is duplicated with RoundRectWidget class.
 *   There are at lease 3 places where round corner widget is desired to keep a consistent style:
 *     1. Virtual keyboard and IM window.
 *     2. This pop-up dialog.
 *     3. Warning dialog.
 *   I tried 2 ways to reuse the code in RoundRectWidget, but failed.
 *     1. Multiple inheritance from QDialog and RoundRectWidget. It is difficult to figure out which function should
 *        be used from which class, because of the complex diamond inheritance.
 *     2. Re-implement a QDialog's event loop in RoundRectWidget, and use RoundRectWidget in all situations. Too complex too.
 *
 *   Last, the most stupidest one is used: write the round corner code 3 times in 3 diffent places.
 */
class RoundRectDialog : public QDialog
{
    Q_OBJECT

public:
    RoundRectDialog(QWidget *parent = 0, float r = 0.02);
    ~RoundRectDialog();
	float radiusPercent() { return _radiusPercent; }
	void setRadiusPercent(float r) { _radiusPercent = r; }
	void setMovable(bool movable);

protected:
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);

private:
    QPoint dragPosition;
    float _radiusPercent;
    bool _movable;
};

#endif // EDITORDIALOGTEMPLATE_H
