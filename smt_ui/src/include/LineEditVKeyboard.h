/*
 * LineEditVKeyboard.h
 *
 *  Created on: 2010/06/08
 *      Author: kinhi
 */

#ifndef LINEEDITVKEYBOARD_H_
#define LINEEDITVKEYBOARD_H_
#include <QtGui>

/**
 * A line edit.
 * All behaviors are the same with QLineEdit,
 * except that mousePressEvent will toggle virtual keyboard.
 */
class LineEditVKeyboard : public QLineEdit {
	Q_OBJECT

public:
	LineEditVKeyboard(QWidget * parent = 0);
	virtual ~LineEditVKeyboard();
	void setKeyboardEnable(bool enable) { _keyboardEnable = enable; }

signals:
	void vKeyboardShowEvent(void);

protected:
	virtual void mousePressEvent ( QMouseEvent * event );
	void focusOutEvent ( QFocusEvent * event );

private:
	bool _keyboardEnable;
};

#endif /* LINEEDITVKEYBOARD_H_ */
