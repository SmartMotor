/*
 * FsModel.h
 *
 *  Created on: 2010/04/10
 *      Author: crazysjf
 */

#ifndef FSMODEL_H_
#define FSMODEL_H_

#include <QtGui>
#include <QStandardItemModel>

class FsModel : public QStandardItemModel {
	Q_OBJECT
public:
	FsModel(QObject *parent = 0);
	virtual ~FsModel();
	enum {
		FILE_NAME_COLUMN,
		LAST_MODIFIED_COLUMN,
		USE_COUNT_COLUMN,
		CUT_COUNT_COLUMN,
		FIRST_POSITION_COLUMN,
		FIRST_FLAGS_COLUMN,
		DESCRIPTION_COLUMN
	};
	void setRootPath ( const QString & newPath );
	QDir rootDirectory();
	QString fileName ( const QModelIndex & index ) const;
	QString filePath ( const QModelIndex & index ) const;
	bool remove ( const QModelIndex & index ) const;
	int findRow(const QString &fileName);

#if 0
	void refresh();
#endif
	static QString baseName(const QString &path);
private:
	QFileSystemWatcher *fsWatcher;
	QString rootPath;
	void setRowItems(const int row, QString &fileName);
private slots:
	void fileChanged(const QString &);
	void directoryChanged(const QString &);
};

#endif /* FSMODEL_H_ */
