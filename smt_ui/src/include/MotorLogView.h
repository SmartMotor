/*
 * MotorLogView.h
 *
 *  Created on: Apr 18, 2010
 *      Author: souken-i
 */

#ifndef MOTORLOGVIEW_H_
#define MOTORLOGVIEW_H_

#include <QWidget>
#include <QTableView>
#include <QStandardItemModel>

class MotorLogView : public QTableView
{
public:
	MotorLogView(QWidget * parent = 0);
	virtual ~MotorLogView();

	void openLogFile(QString &fileName);
	void setAlarmName(QString &name, int alarmCode);
	void setAlarmIcon(QIcon &icon, int alarmCode);

private:
	QStandardItemModel *model;
};

#endif /* MOTORLOGVIEW_H_ */
