/*
 * OpenFileDialog.h
 *
 *  Created on: 2010/03/22
 *      Author: crazysjf
 */

#ifndef OPENFILEDIALOG_H_
#define OPENFILEDIALOG_H_

#include <QtGui>

#include "Program.h"
#include "TableView.h"
#include "Button.h"
#include "FsModel.h"
#include "TextEditVKeyboard.h"

class OpenFileDialog : public QWidget
{
	Q_OBJECT
public:
	OpenFileDialog();
	virtual ~OpenFileDialog();

#if 0
	static QString getOpenFile(QString const &dir, bool *OK);
#endif

	QString &getCurrentFileName();

	void retranslateUi();

public slots:
	void viewSelectionChanged(const QItemSelection & selected, const QItemSelection &deselected);

protected:
    void keyPressEvent(QKeyEvent *event);
    void showEvent(QShowEvent *event);

private slots:
	void createProgramFile();
	void deleteProgramFile();
	void openProgramFile();
	void saveDescription();
	void sortByName();
	void sortByDate();
	void sortByUseCount();
	void modifyDescription();
	void modelItemChanged(QStandardItem *item);
	void adjustVKeyboardPosition();

signals:
	void programFileChanged();

private:
	FsModel *model;
	TableView *view;
	QString openFile;  // Current file

	QLabel    *descriptionLabel;
	TextEditVKeyboard *descriptionText;
	QLabel    *loadDateHistoryLabel;
	QTextEdit *loadDateHistoryText;

	enum {
		F1_Select      = 0,
		F2_Create      = 1,
		F3_Delete      = 2,
		F4_SortByName  = 3,
		F5_SortByUseCount = 4,
		F6_SaveDescription = 5,
		FnButtonNumber    = 6
	};

	QWidget *middleWidget;
	Button *fnButton[FnButtonNumber];

	/**
	 * Get an automatically-generated available file name.
	 * This is used to create a new file.
	 * The file name generating policy is determined by this function.
	 * Return file name does not contain full path.
	 */
	QString getAvailableFileName(void);
	void createButtons();
	Button *createButton(const QIcon &icon, const char *member);

	/**
	 * Update loadDateHistoryText with information from @p.
	 */
	void updateLoadDateHistory(Program *p);
	void clearLoadDateHistory() { loadDateHistoryText->clear(); }

	void setFileIndex(QString &fileName);
	void setCurrentOpenFileIndex();

};

#endif /* OPENFILEDIALOG_H_ */
