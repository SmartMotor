/*
 * SensorStatusWidget.h
 *
 * Show sensor status image or start image
 *
 *  Created on: Jun 18, 2010
 *      Author: souken-i
 */

#ifndef SENSORSTATUSWIDGET_H_
#define SENSORSTATUSWIDGET_H_

#include <QWidget>
#include <QLabel>

class SensorStatusWidget : public QWidget
{
	Q_OBJECT

public:
	SensorStatusWidget(QWidget *parent = 0);
	virtual ~SensorStatusWidget();

public slots:
	/**
	 * Show sensor status image or start image
	 */
	void toggleImage();

	/**
	 * update cut or pusher status
	 */
	void cutStatusEvent(int status);
	void pusherStatusEvent(int status);

protected:
	void resizeEvent( QResizeEvent * event );

private:
	void changeImage(int imageID);

	QLabel *imageLabel;

	enum {
		IMAGE_SENSORS_BOTH_OFF = 0, // 00
		IMAGE_SENSORS_CUT_ON,       // 01
		IMAGE_SENSORS_PUSHER_ON,    // 10
		IMAGE_SENSORS_BOTH_ON,      // 11
		IMAGE_START,
		IMAGE_NR,
	};
	static QString imageFiles[IMAGE_NR];
	int _current_image;
};

#endif /* SENSORSTATUSWIDGET_H_ */
