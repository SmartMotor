/*
 * FlagDelegate.h
 *
 *  Created on: 2010/04/01
 *      Author: crazysjf
 */

#ifndef FLAGDELEGATE_H_
#define FLAGDELEGATE_H_

#include <QStyledItemDelegate>
#include <QtGui>

#include "FlagEditor.h"

class ProgramEditorDelegate  : public QStyledItemDelegate {
	Q_OBJECT
public:
	ProgramEditorDelegate(int positionColumn, int flagsColumn, QWidget *parent = 0) :
		QStyledItemDelegate(parent), 	_positionColumn(positionColumn), _flagsColumn(flagsColumn) {};

	virtual ~ProgramEditorDelegate();

	void paint(QPainter *painter, const QStyleOptionViewItem &option,	const QModelIndex &index) const;
	QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
	QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
	void setEditorData(QWidget *editor, const QModelIndex &index) const;
	void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

private:
	int _positionColumn;
	int _flagsColumn;

private slots:
	void commitAndCloseEditor();
};

#endif /* FLAGDELEGATE_H_ */
