/*
 * HeaderLabel.h
 *
 *  Created on: 2010/03/06
 *      Author: crazysjf
 */

#ifndef HEADERLABEL_H_
#define HEADERLABEL_H_

#include <QLabel>
#include <QTimer>
#include <QString>
#include <TimeLabel.h>

//
// The label on the top most, with
//   1 icon on the left
//   some text to right of the icon
//   a time label on the right
//
class HeaderLabel : public QLabel
{
	Q_OBJECT

public:
	HeaderLabel();
	~HeaderLabel();

	QSize sizeHint() const;

public slots:
	void updateProgramDescription(const QString &desc);

private:
	QLabel *logoPixmapLabel;
	QLabel *programDescription;
	TimeLabel *timeLabel;

	enum {
		ShowHeaderLength = 32,
		ShowTailLength   = 10,
		MaxShowDescriptionLength = 42,
	};

	enum {
		HeaderHeight = 32,
		LogoHeight   = 28,
		LogoWidth    = 28,
	};

	enum {
		LEFT_MARGIN   = 2,
		RIGHT_MARGIN  = 2,
		TOP_MARGIN    = 2,
		BOTTOM_MARGIN = 2,
	};
};

#endif /* HEADERLABEL_H_ */
