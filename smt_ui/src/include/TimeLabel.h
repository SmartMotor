/*
 * TimeLabel.cpp
 *
 *  Created on: 2010/03/06
 *      Author: crazysjf
 */

#ifndef TIMELABEL_CPP_
#define TIMELABEL_CPP_

#include <QLabel>
#include <QTimer>
#include <QCalendarWidget>
#include "ClockCalendarWidget.h"

/**
 *  A label showing current time.
 *    Click on it will show a calendar.
 */

class TimeLabel : public QLabel
{
	Q_OBJECT
public:
	TimeLabel();
	~TimeLabel();

protected:
	virtual void mousePressEvent( QMouseEvent * ev );

private slots:
	void updateDateTime();

private:
	QTimer *timer;
	ClockCalendarWidget *clockCalendar;
	void toggleCalendar();
};

#endif /* TIMELABEL_CPP_ */
