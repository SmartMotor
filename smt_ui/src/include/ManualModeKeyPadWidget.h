/*
 * ManualModeKeyPadWidget.h
 *
 *  Created on: Jun 19, 2010
 *      Author: souken-i
 */

#ifndef MANUALMODEKEYPADWIDGET_H_
#define MANUALMODEKEYPADWIDGET_H_

#include <QWidget>
#include <QSignalMapper>
#include "Button.h"

class ManualModeKeyPadWidget : public QWidget
{
	Q_OBJECT

public:
	ManualModeKeyPadWidget(QWidget *parent = 0);
	virtual ~ManualModeKeyPadWidget();

	enum {
		KP_Rows    = 1,
		KP_Columns = 4,

		KP_MatrixButtonsNR = 4,    // 1x4
		KP_ButtonsNR       = 5,    // 1x4 + close
		KP_IconsNR         = 5,    // 1x4 + close
	};

	enum {
		Button_Index_Close = 4,    // close button index
	};

	static QString iconFiles[KP_IconsNR];

signals:
	void clicked(int id);

protected:
	void mousePressEvent ( QMouseEvent * event );
	void resizeEvent ( QResizeEvent * event );

private slots:
	void hideButtons();

private:
	Button * createButton(int id , const QIcon &icon);

	/**
	 * Convert internal button ID to global key code Mapping
	 */
	int getKeyPadCode(int button_id);
	static int keyPadMappings[KP_ButtonsNR];

    QSignalMapper *signalMapper;
	Button *keyPadButton[KP_ButtonsNR];
};

#endif /* MANUALMODEKEYPADWIDGET_H_ */
