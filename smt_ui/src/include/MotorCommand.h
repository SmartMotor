/*
 * MotorCommand.h
 *
 *  Created on: Mar 30, 2010
 *      Author: souken-i
 */

#ifndef MOTORCOMMAND_H_
#define MOTORCOMMAND_H_

#include <qbytearray.h>

class MotorCommand {
public:
	MotorCommand();
	MotorCommand(int type, int address, int data);
	virtual ~MotorCommand();

    struct CommandFrameData {
        int flag;     /* Frame Type */
        int address;  /* Frame Address */
        int data;     /* Frame Data */
    };

    // Motor Command Types
    enum CommandTypes {
    	MC_SetPitch       = 0,
    	MC_SetZeroPos,
    	MC_SetCurPos,
    	MC_RunToPos,
    	MC_JogRun,
    	MC_SetStopMode,
    	MC_SetRelay,
    	MC_SetPusherSpeed,
    	MC_SetParameters,
    	MC_SetLimitMask,
    	MC_CheckCodec,

    	MC_CommandNumbers,
    };

    // Motor Command Address List
    // Note: Current we are using toChar() to convert address
    //       To string, so it MUST be hex number.
    enum CommandAddrList {
    	MCADDR_SetPitch       	= 0x30,
    	MCADDR_SetZeroPos     	= 0x30,
    	MCADDR_SetCurPos      	= 0x30,
    	MCADDR_RunToPos       	= 0x30,
    	MCADDR_JogRunStop     	= 0x30,
    	MCADDR_JogRunForward    = 0x31,
    	MCADDR_JogRunBackward   = 0x32,
    	MCADDR_SetStopModePos   = 0x31,
    	MCADDR_SetStopModeSpeed = 0x32,
    	MCADDR_SetRelay         = 0x30,
    	MCADDR_SetPusherSpeed   = 0x30,
    	MCADDR_DecPosLoopGain              = 0x10,
    	MCADDR_IncPosLoopGain              = 0x11,
    	MCADDR_DecPosLoopDifferentiation   = 0x20,
    	MCADDR_IncPosLoopDifferentiation   = 0x21,
    	MCADDR_DecPosLoopIntegration       = 0x30,
    	MCADDR_IncPosLoopIntegration       = 0x31,
    	MCADDR_DecSpeedLoopGain     	   = 0x40,
    	MCADDR_IncSpeedLoopGain     	   = 0x41,
    	MCADDR_DecSpeedLoopDifferentiation = 0x50,
    	MCADDR_IncSpeedLoopDifferentiation = 0x51,
    	MCADDR_DecSpeedLoopIntegration     = 0x60,
    	MCADDR_IncSpeedLoopIntegration     = 0x61,
    	MCADDR_SetSpeedLoopStopValue  	   = 0x70,

    	MCADDR_StopCodec          = 0x30,
    	MCADDR_StartCodec         = 0x31,

    	MCADDR_EnableLimits       = 0x30,
    	MCADDR_DisableFrontLimit  = 0x31,
    	MCADDR_DisableMiddleLimit = 0x32,
    	MCADDR_DisableBackLimit   = 0x33,
    	MCADDR_DisablePallet        = 0x34,
    };

    // Special Command Parameters
    enum JogRunTypes {
    	JogRunStop     = MCADDR_JogRunStop,
    	JogRunForward  = MCADDR_JogRunForward,
    	JogRunBackward = MCADDR_JogRunBackward,
    };

    enum StopModes {
    	StopModesPosition = MCADDR_SetStopModePos,
    	StopModesSpeed    = MCADDR_SetStopModeSpeed,
    };

    enum RelayBitMask {
    	Relay1Mask   = 0x1,
    	Relay2Mask   = 0x2,
    	Relay3Mask   = 0x4,
    	Relay4Mask   = 0x8,
    	RelayMask    = 0xf
    };

    enum PosLoopParameterTypes {
    	DecPosLoopGain            = MCADDR_DecPosLoopGain,
    	IncPosLoopGain            = MCADDR_IncPosLoopGain,
    	DecPosLoopDifferentiation = MCADDR_DecPosLoopDifferentiation,
    	IncPosLoopDifferentiation = MCADDR_IncPosLoopDifferentiation,
    	DecPosLoopIntegration     = MCADDR_DecPosLoopIntegration,
    	IncPosLoopIntegration     = MCADDR_IncPosLoopIntegration,
    };

    enum SpeedLoopParametersTypes {
    	DecSpeedLoopGain            = MCADDR_DecSpeedLoopGain,
    	IncSpeedLoopGain 			= MCADDR_IncSpeedLoopGain,
    	DecSpeedLoopDifferentiation = MCADDR_DecSpeedLoopDifferentiation,
    	IncSpeedLoopDifferentiation = MCADDR_IncSpeedLoopDifferentiation,
    	DecSpeedLoopIntegration 	= MCADDR_DecSpeedLoopIntegration,
    	IncSpeedLoopIntegration 	= MCADDR_IncSpeedLoopIntegration,
    	SetSpeedLoopStopValue       = MCADDR_SetSpeedLoopStopValue,
    };

    enum LimitMaskTypes {
    	EnableAllLimits    = MCADDR_EnableLimits,
    	DisableFrontLimit  = MCADDR_DisableFrontLimit,
    	DisableMiddleLimit = MCADDR_DisableMiddleLimit,
    	DisableBackLimit   = MCADDR_DisableBackLimit,
    	DisablePallet      = MCADDR_DisablePallet,
    };

    enum CheckCodecActions {
    	StopCodec  = MCADDR_StopCodec,
    	StartCodec = MCADDR_StartCodec,
    };

    static void toCommand(QByteArray & buffer, int type, int address, int data);
    static void toCommand(QByteArray & buffer, CommandFrameData *data);
    static void initializeCommandHeaders();

    QByteArray & frame();

    // Command Helper Functions
    void setCommand(int type, int address, int data);
    void commandSetPitch(int data);
    void commandSetZeroPos(int pos);
    void commandSetCurPos(int pos);
    void commandRunToPos(int pos);
    void commandJogRun(JogRunTypes type, int speed);
    void commandSetStopMode(StopModes mode);
    void commandSetRelay(int relayStatus);
    void commandSetPusherSpeed(int rpm);
    void commandSetPosLoopParameters(PosLoopParameterTypes type, int value);
    void commandSetSpeedLoopParameters(SpeedLoopParametersTypes type, int value);

    void commandSetLimitMask(LimitMaskTypes type);
    void commandCheckCodec(CheckCodecActions action);

    static char toChar(int number);
    static int toInt(bool *ok, char ch);


private:
    QByteArray _frame;
    static char CommandHeaders[MC_CommandNumbers+1]; // Include \0
};

#endif /* MOTORCOMMAND_H_ */
