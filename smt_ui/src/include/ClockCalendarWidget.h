/*
 * ClockCalendarWidget.h
 *
 *  Show a WallClock on the left of on CalendarWidget
 *
 *  Created on: Jun 7, 2010
 *      Author: souken-i
 */

#ifndef CLOCKCALENDARWIDGET_H_
#define CLOCKCALENDARWIDGET_H_

#include <QWidget>
#include <QCalendarWidget>
#include "RoundRectWidget.h"
#include "wallclock.h"

class ClockCalendarWidget : public RoundRectWidget /* QWidget */
{
	Q_OBJECT

public:
	ClockCalendarWidget(QWidget *parent = 0);
	virtual ~ClockCalendarWidget();

protected:
	/**
	 * When widget is shown, set calendarWidget to current date
	 */
    void showEvent(QShowEvent *event);

private:
	QCalendarWidget *calendarWidget;
	WallClock *clock;
};

#endif /* CLOCKCALENDARWIDGET_H_ */
