/*
 * EditDialog.h
 *
 *  Created on: 2010/06/06
 *      Author: crazysjf
 */

#ifndef EDITDIALOG_H_
#define EDITDIALOG_H_

#include <QtGui>
#include <RoundRectDialog.h>
#include "PushButton.h"

/**
 * Base class for LabelEditDialog and AverageEditDialog.
 * Contains:
 *   A Title with bigger bold font.
 *   A warning area showing waning message.
 *   A "OK" button a "Cancel" button.
 *
 * The layout is:
 *   Top         : Title and warning area.
 *   Bottom left : Drawing area.
 *   Bottom right: OK and cancel buttons.
 *
 *   Content in drawing area can be inserted here by insertDrawingArea().
 *   Child class can call this function to insert self-defined contents.
 */
class EditDialog : public RoundRectDialog {
	Q_OBJECT

public:
	EditDialog(QWidget *parent = 0);
	virtual ~EditDialog();
	PushButton *createButton(const QIcon &icon, const char *member);
	void retranslateUi();
	PushButton *getOKButton() { return OKButton; }
	PushButton *getCancelButton() { return cancelButton; }

	void setTitle(const QString &title);
	void setGuideText(const QString &msg);
	void setGuideIcon(const QPixmap &pm);

private:
	QHBoxLayout *bottomLayout;
	QLabel *titleLabel;
	QLabel *guideLabel;
	QLabel *guideIconLabel;
	PushButton *OKButton, *cancelButton;

	void loadStyleSheet(const QString & fileName);

protected:
	/**
	 * Insert a layout to drawing area (bottom left).
	 * @layout: The layout to be inserted.
	 */
	void insertDrawingArea(QLayout *layout);
};

#endif /* EDITDIALOG_H_ */
