/*
 * PushButton.h
 *
 *  Created on: Sep 12, 2010
 *      Author: souken-i
 */

#ifndef PUSHBUTTON_H_
#define PUSHBUTTON_H_

#include <QPushButton>

class PushButton : public QPushButton {
public:
	PushButton ( QWidget * parent = 0 );
	PushButton ( const QString & text, QWidget * parent = 0 );
	PushButton ( const QIcon & icon, const QString & text, QWidget * parent = 0 );
	virtual ~PushButton();

protected:
	void mousePressEvent ( QMouseEvent * event );
	void mouseReleaseEvent ( QMouseEvent * event );
};

#endif /* PUSHBUTTON_H_ */
