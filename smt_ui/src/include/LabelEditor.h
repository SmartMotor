#ifndef LABELEDITOR_H
#define LABELEDITOR_H

#include <QtGui/QDialog>
#include <QLabel>
#include <QLineEdit>
#include "EditDialog.h"
#include "Button.h"
#include "LineEditVKeyboard.h"

class LabelEditor : public EditDialog
{
    Q_OBJECT

public:
    LabelEditor(QWidget *parent = 0);
    ~LabelEditor();

    void retranslateUi();

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
	/**
	 * Slot for F1_DONE
	 *
	 * Check the input data, if all data is
	 * valid, exit this dialog; otherwise clear the invalid data
	 * and set focus to it
	 *
	 * The input data is put into global variable labelEditData
	 * structure.
	 */
	void handleInputData();

private:
    enum {
    	TOTAL_LENGTH  = 0,
    	LABEL_LENGTH,
    	UNUSED_LENGTH,
    	LABEL_EDIT_ITEM_NR,
    };

    QLabel    *descLabels[LABEL_EDIT_ITEM_NR];
    LineEditVKeyboard *inputEdits[LABEL_EDIT_ITEM_NR];
};

#endif // LABELEDITOR_H
