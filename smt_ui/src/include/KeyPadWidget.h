/*
 * KeyPadWidget.h
 *
 * 4x4 key pad widget with close button
 *
 * This is used in MainFrame class. And it has different layout/button number in
 * different modes:
 *
 *   Edit Mode:    4x4
 *   AutoRun Mode: 4x4, but icons, functions of part of the buttons are different
 *   Manual Mode:  1x4
 *
 *  So this widget should provide APIs to:
 *    - set icon of each button
 *    - each row can be shown/hidden
 *
 *  Created on: Jun 14, 2010
 *      Author: souken-i
 */

#ifndef KEYPADWIDGET_H_
#define KEYPADWIDGET_H_

#include <QWidget>
#include <QSignalMapper>
#include "Button.h"

class KeyPadWidget : public QWidget
{
	Q_OBJECT

public:
	KeyPadWidget(int row, int column, QWidget *parent = 0);
	virtual ~KeyPadWidget();

	/**
	 * set icon to the specified button
	 */
	void setIcon(int button_id, const QIcon &icon);

	/**
	 *	Show/Hide buttons in this row
	 */
	void setRowVisable(bool visible, int row);

signals:
	void clicked(int id);

private:
	Button * createButton(int id /* , const QIcon &icon */);
    QSignalMapper *signalMapper;

    int _keyPadRows;         // rows, close button excluded
    int _keyPadColumns;      // columns, close button excluded
    int _keyPadButtonNumber; // rows * columns + 1 (close button)
	Button **keyPadButton;
};

#endif /* KEYPADWIDGET_H_ */
