/*
 * Calculator.h
 *
 *  Created on: Mar 21, 2010
 *      Author: Zeng Xianwei
 */
#ifndef CALCULATOR_H
#define CALCULATOR_H


#include <QLineEdit>
#include <QLabel>

#include "RoundRectDialog.h"
#include "Button.h"

/**
 * A simple calculator.
 * This calculator and be connected to a QLineEdit widget by setLineEdit().
 * If the connection is setup, the calculator's result will be set to the QLineEdit
 * widget when the calculator is closed.
 */
class Calculator : public RoundRectDialog
{
    Q_OBJECT

public:
    Calculator(QWidget *parent = 0);

    QString getCalculatorResult();
    /**
     * Move itself to the center of screen.
     * This should be called after show() is called at lease once,
     * for the correct size of itself can not be obtained before show()
     * is called, the size is used in calculation of the center position.
     */
    void moveToCenter();
	/**
	 * Set _lineEdit to @lineEidt.
	 * If _lineEdit is set, when calculator window is closed, the result will be shown on _lineEdit.
	 */
	void setLineEdit(QLineEdit *lineEdit);

public slots:
	void clearAll();
	void close();
private slots:
    void digitClicked();
    void unaryOperatorClicked();
    void additiveOperatorClicked();
    void multiplicativeOperatorClicked();
    void equalClicked();
    void pointClicked();
    void changeSignClicked();
    void backspaceClicked();

protected:
    void keyPressEvent(QKeyEvent *event);
    bool eventFilter(QObject *target, QEvent *event);
    //void resizeEvent(QResizeEvent* event);
    void showEvent ( QShowEvent * event );

private:
	void retranslateUi();
    Button *createButton(const QString &text, const char *member);
    void abortOperation();
    bool calculate(double rightOperand, const QString &pendingOperator);

    double sumSoFar;
    double factorSoFar;
    QString pendingAdditiveOperator;
    QString pendingMultiplicativeOperator;
    bool waitingForOperand;

    QLineEdit *display;  // results
//    QLineEdit *formula;

    enum { NumDigitButtons = 10 };
    Button *digitButtons[NumDigitButtons];
    Button *plusButton;     // +
    Button *minusButton;    // -
    Button *timesButton;    // *
    Button *divisionButton; // /

    QLineEdit *_lineEdit; /*< See setLineEdit(). */
    QLabel *guideLabel;
};

#endif
