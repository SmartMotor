/*
 * Motor.h
 *
 *  Created on: Mar 29, 2010
 *      Author: souken-i
 */

#ifndef GLOBAL_H_
#define GLOBAL_H_

#include "MData.h"
#include "Motor.h"
#include "System.h"
#include "SmtKb.h"
#include "VirtualKeyboard.h"
#include "Beeper.h"

#define DEFAUT_MAIN_W  800
#define DEFAUT_MAIN_H  600

enum {
	English = 0,
	Chinese,
	MaxLanguages,
};

enum {
	Alarm_OverCurrent = 1,
	Alarm_OverLoad,
	Alarm_LoseControl,
	Alarm_CommunicationError,
	MaxAlarmCodes
};

/**
 * Label edit data structure
 */
struct labelEditData_t {
	int totalLength;
	int labelLength;
	int unusedLength;
	int mask;             // bit 0 ~ 2
};

enum {
	LED_TOTAL_LENGTH_VALID  = 0x01,
	LED_LABEL_LENGTH_VALID  = 0x02,
	LED_UNUSED_LENGTH_VALID = 0x04,
	LED_ALL_VALID           = 0x07
};

enum {
	AED_TOTAL_LENGTH_VALID   = 0x01,
	AED_AVERAGE_NUMBER_VALID = 0x02,
	AED_ALL_VALID            = 0x03
};

enum {
	AVERAGE_NUMBER_MODE = 0,
	AVERAGE_VALUE_MODE,
};

struct averageEditData_t {
	int totalLength;
	int averageNumber;
	int mode;
	int mask;
};

/**
 * Configurations
 */
#define MAX_PROGRAM_FILES_NUMBER    100    // How many files can have
#define MAX_DATA_NUMBER_PER_FILE    10000

/**
 * Font Size Limitations
 */
#define SYSTEM_FONT_SIZE_MIN            12
#define SYSTEM_FONT_SIZE_MAX            24
#define SYSTEM_FONT_SIZE_DEFAULT        16

#define DATA_LIST_FONT_SIZE_MIN         20
#define DATA_LIST_FONT_SIZE_MAX         28
#define DATA_LIST_FONT_SIZE_DEFAULT     24

#define POSITION_FONT_SIZE_MIN          24
#define POSITION_FONT_SIZE_MAX          40
#define POSITION_FONT_SIZE_DEFAULT      32

#define DISTANCE_FONT_SIZE_MIN          16
#define DISTANCE_FONT_SIZE_MAX          32
#define DISTANCE_FONT_SIZE_DEFAULT      16

/*
 * Global objects
 */
extern System *gSysConfig;     /*< System configurations */
extern Motor  *gMotor;         /* Motor */
extern VirtualKeyboard *gVKeyboard; /*< Global virtual keyboard. */
extern Beeper *gBeeper; /*< Global beeper. */

#endif /* GLOBAL_H_ */
