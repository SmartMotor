/*
 * PaperPusherBar.h
 *
 *  Created on: 2010/03/07
 *      Author: crazysjf
 */

#ifndef PAPERPUSHERBAR_H_
#define PAPERPUSHERBAR_H_

#include <QLabel>
#include <QWidget>
#include "PaperPusher.h"
#include "Debug.h"
#include "MotorTestWidget.h"

class PaperPusherBar : public QWidget
{
	Q_OBJECT

public:
	PaperPusherBar(QWidget *parent = 0);
	//void paintEvent(QPaintEvent *event);
	QSize sizeHint() const;

	void retranslateUi();
	void updateFontSizeColor();

public slots:
	void updateProgramID(int id);
	void updateProgramName(QString &name);
	void updateProgramStep(int total, int step);
	void updateCutCount(int count);
	void updateCutDistance(int distance);

#ifdef MOTOR_TEST_WIDGET_ENABLE
protected:
	void mousePressEvent( QMouseEvent * ev );
#endif

private:
	PaperPusher *paperPusher;
	QLabel *programLabel;
	QLabel *programIDLabel;
	QLabel *stepLabel;
	QLabel *stepIDLabel;

	QLabel *distanceLabel;
	QLabel *distanceIconLabel;
	QLabel *cutCountLabel;
	QLabel *cutCountIconLabel;

#ifdef MOTOR_TEST_WIDGET_ENABLE
	MotorTestWidget *motorTestWidget;
#endif
};

#endif /* PAPERPUSHERBAR_H_ */
