/*
 * PaperPusher.h
 *
 *  Created on: 2010/03/06
 *      Author: crazysjf
 */

#ifndef PAPERPUSHER_H_
#define PAPERPUSHER_H_

#include <QWidget>
#include <QLabel>

//
// The class implementing the paper pusher control.
//
class PaperPusher : public QWidget
{
	Q_OBJECT

public:
	PaperPusher(QWidget *parent = 0);
	~PaperPusher();

	void updateFontSizeColor();

public slots:
	void updateCurrentPostion(int pos);

private:
	enum {
		MaxPixmapHeight = 80,
		MaxPixmapWidth  = 256,
		MaxTextHeight   = 40,
	};

	QLabel *pusherPixmapLabel;
	QLabel *textLabel;
};

#endif /* PAPERPUSHER_H_ */
