/*
 * TextEditVKeyboard.h
 *
 *  Created on: 2010/06/08
 *      Author: kinhi
 */

#ifndef TEXTEDITVKEYBOARD_H_
#define TEXTEDITVKEYBOARD_H_

#include <QtGui>

/**
 * A text edit.
 * All behaviors are the same with QTextEdit,
 * except that mousePressEvent will toggle virtual keyboard.
 */
class TextEditVKeyboard : public QTextEdit {
	Q_OBJECT

public:
	TextEditVKeyboard(QWidget * parent = 0);
	virtual ~TextEditVKeyboard();

signals:
	void vKeyboardShowEvent(void);

protected:
	virtual void mousePressEvent ( QMouseEvent * event );
};

#endif /* TEXTEDITVKEYBOARD_H_ */
