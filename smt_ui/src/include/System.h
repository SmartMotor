/*
 * System.h
 *
 *  Created on: 2010/03/21
 *      Author: crazysjf
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include <QString>
#include <QColor>
#include <QTranslator>
#include <QRect>


/**
 * System global setting is put here.
 */
class System {
public:
	System();
	virtual ~System();

	QTranslator appTranslator;

	void loadAppTranslator();
	void installAppTranslator();

	static void setCurrentUser(const QString &userName);
	static QString &currentUser(void);

	static void setDataRootDir(const QString &dir);
	static QString &dataRootDir(void);

	static QString currentUserDir(void) {
		return _dataRootDir + "/" +  _currentUser + "/";
	}

	static QString currentProgramDir(void) {
		return currentUserDir() + "programs/";
	}

	int getSysFontSize();
	int getDataListFontSize();
	//int getOpenFileDialogFontSize();
	//void setOpenFileDialogFontSize(int size);
	void setSysFontSize(int size);
	void setDataListFontSize(int size);
	bool load();
	bool save();
	void setSysConfigFile(QString &fileName);
	bool loadSysConfigFile(QString &fileName);
	bool isFontSizeValid(int size);

	int getPositionFontSize();
	int getDistanceFontSize();
	void setPositionFontSize(int size);
	void setDistanceFontSize(int size);

	int  getLanguage();
	void setLanguage(int language);

	int  getScreensaverTime();
	void setScreensaverTime(int seconds);

	QColor & getPositionFontColor();
	QColor & getPositionBackgroundColor();
	QColor & getDistanceFontColor();
	QColor & getDistanceBackgroundColor();
	void setPositionFontColor(QColor &color);
	void setPositionBackgroundColor(QColor &color);
	void setDistanceFontColor(QColor &color);
	void setDistanceBackgroundColor(QColor &color);

	QString & getLastOpenFile();
	bool saveLastOpenFile(QString & fileName);
	bool loadLastOpenFile();

private:
	static QString _currentUser;
	static QString _dataRootDir;
	QString _systemConfigFile;
	QString _lastOpenFile;           // File Name

	// Int configurations
	enum {
		ItemSysFontSize        = 0,
		ItemDataListFontSize,
		ItemMinFontSize,
		ItemMaxFontSize,
		//OpenFileDialogFontSize,
		ItemPositionFontSize,
		ItemDistanceFontSize,
		ItemScreenSaverTime,
		ItemLanguage,
		MaxSysConfigItems,
	};

	enum {
		// Configurations below are not int values
		ItemPostionFontColor = 0,
		ItemPostionBackgroundColor,
		ItemDistanceFontColor,
		ItemDistanceBackgroundColor,
		MaxSystemColors,
	};

	int _systemConfigs[MaxSysConfigItems];

	QColor _systemColors[MaxSystemColors];

	/**
	 * System wide pop-up dialog geometry
	 */
	QRect _popupDialogRect;
};

#endif /* SYSTEM_H_ */
