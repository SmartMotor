/*
 * RoundRectWidget.h
 *
 *  Created on: 2010/04/27
 *      Author: kinhi
 */

#ifndef ROUNDRECTWIDGET_H_
#define ROUNDRECTWIDGET_H_

#include <QtGui>

/**
 * A frameless, round corner widget.
 * The radius of the round corner can be get/set by radiusPercent() and setRadiusPercent().
 */
class RoundRectWidget : public QWidget {
public:
	RoundRectWidget(QWidget *parent = 0, float r = 0.02);
	virtual ~RoundRectWidget();

	float radiusPercent() { return _radiusPercent; }
	void setRadiusPercent(float r) { _radiusPercent = r; }
	void setMovable(bool movable);

protected:
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);

private:
    QPoint dragPosition;
    float _radiusPercent;
    bool _movable;
};

#endif /* ROUNDRECTWIDGET_H_ */
