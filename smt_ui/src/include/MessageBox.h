/*
 * MessageBox.h
 *
 *  Created on: Jun 5, 2010
 *      Author: souken-i
 */

#ifndef MESSAGEBOX_H_
#define MESSAGEBOX_H_

#include <QMessageBox>

class MessageBox : public QMessageBox
{
	Q_OBJECT

public:
	MessageBox(QWidget *parent = 0);
	virtual ~MessageBox();
};

#endif /* MESSAGEBOX_H_ */
