/*
 * Beeper.h
 *
 *  Created on: Sep 12, 2010
 *      Author: souken-i
 */

#ifndef BEEPER_H_
#define BEEPER_H_

class Beeper {
public:
	Beeper();
	virtual ~Beeper();

	void setBeeperOn(void);
	void setBeeperOff(void);

private:
	int beeper_fd;
};

#endif /* BEEPER_H_ */
