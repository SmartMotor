/*
 * KeyboardCheck.h
 *
 *  Created on: Aug 25, 2010
 *      Author: souken-i
 */

#ifndef KEYBOARDCHECK_H_
#define KEYBOARDCHECK_H_

#include <QDialog>
#include <QLabel>

class KeyboardCheck : public QDialog
{
public:
	KeyboardCheck(QWidget *parent = 0);
	virtual ~KeyboardCheck();

	void retranslateUi();

protected:
    void keyPressEvent(QKeyEvent *event);

private:
//    void checkKbdConnection();

	QLabel *display;
	QLabel *title;
	QLabel *guide;
	int _exitCount;
};

#endif /* KEYBOARDCHECK_H_ */
