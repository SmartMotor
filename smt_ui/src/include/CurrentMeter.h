/*
 * CurrentMeter.h
 *
 *  Created on: Apr 20, 2010
 *      Author: souken-i
 */

#ifndef CURRENTMETER_H_
#define CURRENTMETER_H_

#include <QObject>
#include "manometer.h"

class CurrentMeter : public ManoMeter
{
	Q_OBJECT

public:
	CurrentMeter(QWidget *parent = 0);
	virtual ~CurrentMeter();

public slots:
	void updateCurrent(int current);
};

#endif /* CURRENTMETER_H_ */
