/*
 * MData.h
 *
 *  Created on: Mar 28, 2010
 *      Author: souken-i
 */

#ifndef MDATA_H_
#define MDATA_H_

#include <qstring.h>

class MData {
public:
	MData();
	virtual ~MData();

	static int toInternal(int value);   /* 30 mm -> 3000 (0.01mm) */
	static int toShow(int value);       /* 3000  -> 30 mm    */
	static QString toString(int value); /* 1234  -> 12.34 mm */
	static int toInt(QString &text);
};

#endif /* MDATA_H_ */
