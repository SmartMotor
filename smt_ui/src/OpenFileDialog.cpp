/*
 * OpenFileDialog.cpp
 *
 *  Created on: 2010/03/22
 *      Author: crazysjf
 */
#include <QtGui>

#include "OpenFileDialog.h"
#include "Global.h"
#include "ProgramEditorDelegate.h"

OpenFileDialog::OpenFileDialog()
{
	model = new FsModel;
	model->setRootPath(System::currentProgramDir());

	view = new TableView;
	view->setModel(model);

	view->setSortingEnabled(true);
	ProgramEditorDelegate *flagDelegate = new ProgramEditorDelegate(FsModel::FIRST_POSITION_COLUMN, FsModel::FIRST_FLAGS_COLUMN);
	view->setItemDelegate(flagDelegate);
	view->verticalHeader()->hide();

	view->horizontalHeader()->setResizeMode(QHeaderView::Fixed);
	view->horizontalHeader()->setStretchLastSection(true);

	view->resizeColumnsToContents();

	connect(view, SIGNAL(selectionChangedSignal(const QItemSelection &, const QItemSelection &)),
			this, SLOT(viewSelectionChanged(const QItemSelection &, const QItemSelection &)));
	connect(model, SIGNAL(itemChanged(QStandardItem *)),
			this, SLOT(modelItemChanged(QStandardItem *)));

	descriptionLabel = new QLabel;
	descriptionText  = new TextEditVKeyboard;
	connect(descriptionText, SIGNAL(vKeyboardShowEvent()),
			this, SLOT(adjustVKeyboardPosition()));

	loadDateHistoryLabel = new QLabel;
	loadDateHistoryText = new QTextEdit;
	loadDateHistoryText->setReadOnly(true);

	createButtons();

	middleWidget = new QWidget;
	QVBoxLayout *middleLayout = new QVBoxLayout;

	// Set middleWidget's background.
	middleWidget->setPalette(QApplication::palette());
	middleWidget->setAutoFillBackground(true);

	middleLayout->addWidget(view);
	middleLayout->setStretchFactor(view, 3);
	QHBoxLayout *tmpHLayout = new QHBoxLayout;

	QVBoxLayout *tmpVLayout = new QVBoxLayout;
	tmpVLayout->addWidget(descriptionLabel);
	tmpVLayout->addWidget(descriptionText);
	tmpHLayout->addLayout(tmpVLayout);
	tmpHLayout->setStretchFactor(tmpVLayout, 2);

	tmpVLayout = new QVBoxLayout;
	tmpVLayout->addWidget(loadDateHistoryLabel);
	tmpVLayout->addWidget(loadDateHistoryText);
	tmpHLayout->addLayout(tmpVLayout);
	tmpHLayout->setStretchFactor(tmpVLayout, 1);

	middleLayout->addLayout(tmpHLayout);
	middleLayout->setStretchFactor(tmpHLayout, 1); // Description occupies Max 2-3 lines

	middleLayout->setContentsMargins(2, 2, 2, 2);

	middleWidget->setLayout(middleLayout);

	QVBoxLayout *buttonsLayout = new QVBoxLayout;
	for (int i = 0; i < FnButtonNumber; i++) {
		buttonsLayout->addWidget(fnButton[i]);
	}
	buttonsLayout->setContentsMargins(2, 2, 2, 2);

	QGridLayout *layout = new QGridLayout;
	layout->addWidget(middleWidget, 0, 0, 1, 8);
	layout->addLayout(buttonsLayout, 0, 8, 1, 1);
	layout->setColumnStretch(0, 1);
	layout->setContentsMargins(0, 0, 0, 0);
	setLayout(layout);

	if (openFile.isEmpty()) {
		openFile = gSysConfig->getLastOpenFile();
	}

	retranslateUi();
}

OpenFileDialog::~OpenFileDialog()
{
	// TODO Auto-generated destructor stub
}

Button *OpenFileDialog::createButton(const QIcon &icon, const char *member)
{
	Button *button = new Button;
	button->setIcon(icon);
    connect(button, SIGNAL(clicked()), this, member);
    button->setProperty("custom", true);
    return button;
}

void OpenFileDialog::createButtons()
{
	fnButton[F1_Select]
	         = createButton(QIcon("images/select-64.png"), SLOT(openProgramFile()));
	fnButton[F2_Create]
	         = createButton(QIcon("images/add-64.png"), SLOT(createProgramFile()));
	fnButton[F3_Delete]
	         = createButton(QIcon("images/delete-64.png"), SLOT(deleteProgramFile()));
	fnButton[F4_SortByName]
	         = createButton(QIcon("images/up-arrow-64.png"), SLOT(sortByName()));
	fnButton[F5_SortByUseCount]
	         = createButton(QIcon("images/up-arrow-64.png"), SLOT(sortByUseCount()));
	fnButton[F6_SaveDescription]
	         = createButton(QIcon("images/description-save.png"), SLOT(saveDescription()));
}

///
// Get an automatically-generated available file name.
// This is used to create a new file.
// The file name generating policy is determined by this function.
// Return file name does not contain full path.
//
QString OpenFileDialog::getAvailableFileName(void)
{
//	const static QString fPrefix = "data";
	const static QString fSurfix = ".pro";
	char buffer[4];

	QDir currentDir = model->rootDirectory();

	int i = 1;
//	QString fileName = fPrefix + QString("%1").arg(i) + fSurfix;

	::snprintf(buffer, 4, "%03d", i);
	QString fileName = QString(buffer) + fSurfix;

	while (currentDir.exists(fileName) && (i < MAX_PROGRAM_FILES_NUMBER)) {
		i ++;
//		fileName = fPrefix + QString("%1").arg(i) + fSurfix;

		::snprintf(buffer, 4, "%03d", i);
		fileName = QString(buffer) + fSurfix;
	}

	return fileName;
}

void OpenFileDialog::createProgramFile()
{
	QString fileName = model->rootDirectory().path() + "/" + getAvailableFileName();
	Program prog(fileName);
	prog.init();
	prog.save();
}

void OpenFileDialog::deleteProgramFile()
{
	QModelIndex currentIndex = view->currentIndex();
	if (currentIndex.row() >= 0) {
	    QMessageBox::StandardButton reply;
	    QString msg = tr("Are you sure to delete program file: ");
	    msg += model->index(currentIndex.row(), FsModel::FILE_NAME_COLUMN).data().toString();

	    reply = QMessageBox::question(this, tr("Program file delete"),
	    		msg ,
	    		QMessageBox::Yes | QMessageBox::No,
	    		QMessageBox::Yes);
	    if (reply == QMessageBox::No)
	        return;

		model->remove(currentIndex);
	}
}

void OpenFileDialog::openProgramFile()
{
	QModelIndex currentIndex = view->currentIndex();

	if (currentIndex.row() < 0) {
		// no selection
		return;
	}

	openFile = model->filePath(currentIndex);

	/* Update file load date history */
	Program *p = new Program(openFile);
	p->updateLoadDateHistory();
	delete p;
	emit programFileChanged();

}

void OpenFileDialog::sortByName()
{
	QHeaderView *header = view->horizontalHeader();
	header->setSortIndicator(FsModel::FILE_NAME_COLUMN,
			header->sortIndicatorOrder() == Qt::AscendingOrder ? Qt::DescendingOrder : Qt::AscendingOrder);
}

void OpenFileDialog::sortByDate()
{
	// TODO: Add function
}

void OpenFileDialog::sortByUseCount()
{
	QHeaderView *header = view->horizontalHeader();
	header->setSortIndicator(FsModel::USE_COUNT_COLUMN,
			header->sortIndicatorOrder() == Qt::AscendingOrder ? Qt::DescendingOrder : Qt::AscendingOrder);
}


QString & OpenFileDialog::getCurrentFileName()
{
	return openFile;
}

#if 0
//
// Show a dialog to allow user to select open file.
//  @dir: Directory in which file will be open
//  @OK:  If open button is clicked, *OK will be set to true. else false.
//  @retval:
//        If *OK is true, the full path of the selected file.
//        If *OK is false, no meaning.
//
QString OpenFileDialog::getOpenFile(QString const & dir, bool *OK)
{

	OpenFileDialog dialog;
	dialog.currentDir = dir;

	// Add watch to dir
	dialog.model.setRootPath(dir);

	dialog.view->setRootIndex(dialog.model.index(dir));

	// TODO: seems have no effect;
//	for (int column = 0; column < dialog.model.columnCount(); ++column)
//		dialog.view.resizeColumnToContents(column);

	// Enlarge the window
	dialog.resize(dialog.childrenRect().size());

	dialog.exec();

	if (dialog.clickedButton & OPEN_BUTTON) {
		*OK = true;
	} else {
		*OK = false;
	}

	return dialog.openFile;
}
#endif

void OpenFileDialog::saveDescription()
{
	QModelIndex currentIndex = view->currentIndex();
	int currentRow = currentIndex.row();
	if ( currentRow< 0)
		return;

	Program p(model->filePath(currentIndex));
	p.load();
	p.setDescription(descriptionText->toPlainText());
	p.save();

}

/* TODO
 *   This function is called when button clicked() signal is emitted.
 *   It should do:
 *     - Set focus to text editbox
 *     - show soft keyboard
 */
void OpenFileDialog::modifyDescription()
{
	descriptionText->setFocus();

}

// TODO
// When adding a new file, the model->itemChanged() signal is emitted
// for several times, thus this function is called from several times.
// We have to pay unnecessary cost.
void OpenFileDialog::modelItemChanged(QStandardItem *item)
{
	view->resizeColumnsToContents();

	view->horizontalHeader()->setStretchLastSection(true);

	// set index to this row
	QModelIndex index = item->index();
	view->setCurrentIndex(index);
}

void OpenFileDialog::viewSelectionChanged(const QItemSelection & selected, const QItemSelection &)
{
	if (!selected.indexes().isEmpty()) {
		Program p(model->filePath(selected.indexes()[0]));
		p.load();
		descriptionText->setPlainText(p.description());
		updateLoadDateHistory(&p);
	} else {
		descriptionText->clear();
		clearLoadDateHistory();
	}
}

void OpenFileDialog::keyPressEvent(QKeyEvent *event)
{
	qDebug() << "OpenFileDialog keyPressEvent " << event->key() << endl;

	if ( isVisible() ) {
		switch(event->key()) {
		case SmtKb::Key_Enter:
		case SmtKb::Key_F1:
			openProgramFile();
			break;
		case SmtKb::Key_F2:
			createProgramFile();
			break;
		case SmtKb::Key_F3:
			deleteProgramFile();
			break;
		case SmtKb::Key_F4:
			sortByName();
			break;
		case SmtKb::Key_F5:
			sortByUseCount();
			break;
		case SmtKb::Key_F6:
			saveDescription();
			break;
		default:
			QWidget::keyPressEvent(event);
		}
	}
}

void OpenFileDialog::setFileIndex(QString &fileName)
{
	QModelIndex index = model->index(model->findRow(fileName), 0);

	view->setCurrentIndex(index);
}

void OpenFileDialog::setCurrentOpenFileIndex()
{
	setFileIndex(openFile);
}

void OpenFileDialog::showEvent(QShowEvent *event)
{
	setCurrentOpenFileIndex();

	return QWidget::showEvent(event);
}

void OpenFileDialog::updateLoadDateHistory(Program *p)
{
	int i;
	QString s;
	for (i = 0; i < FILE_LOAD_DATE_HISTORY_LENTH; i++) {
		QDateTime d;
		uint time_t = p->header().loadDateHistory[i];
		if ( time_t != 0) {
			d.setTime_t(time_t);
			s += d.toString("yy/MM/dd hh:mm");
			s += "\n";
		}
	}

	loadDateHistoryText->setText(s);
}

void OpenFileDialog::adjustVKeyboardPosition()
{
	gVKeyboard->move(0, 160);
}

void OpenFileDialog::retranslateUi()
{
	descriptionLabel->setText(tr("Description:"));
	loadDateHistoryLabel->setText(tr("Load history:"));
}


