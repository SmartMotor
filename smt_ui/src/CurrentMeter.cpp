/*
 * CurrentMeter.cpp
 *
 *  Created on: Apr 20, 2010
 *      Author: souken-i
 */

#include "CurrentMeter.h"
#include "Global.h"

CurrentMeter::CurrentMeter(QWidget *parent)
	: ManoMeter(parent)
{
	setMaximum(30);
	setMinimum(0);
	setNominal(10);
	setCritical(15);
	QString unit(tr("[Amp]"));
	setSuffix(unit);

	setValue(gMotor->getMotorCurrent());
	resize(120, 120);

	// FIXME: should we do it here or in the MainFrame?
	/* update value when current is changed */
	connect(gMotor, SIGNAL(currentChanged(int)), this, SLOT(updateCurrent(int)));
}

CurrentMeter::~CurrentMeter()
{
}

void CurrentMeter::updateCurrent(int current)
{
	setValue(current);
}
