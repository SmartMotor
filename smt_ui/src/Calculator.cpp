/*
 * Calculator.cpp
 *
 *  Created on: Mar 21, 2010
 *      Author: Zeng Xianwei
 */
#include <QtGui>

#include <math.h>

#include "Button.h"
#include "Calculator.h"
#include "Global.h"

Calculator::Calculator(QWidget *parent) : RoundRectDialog(parent)
{
    sumSoFar = 0.0;
    factorSoFar = 0.0;
    waitingForOperand = true;

    display = new QLineEdit("0");
    display->setReadOnly(true);
    display->setAlignment(Qt::AlignRight);
    display->setMaxLength(15);

    QFont font = display->font();
    font.setPointSize(font.pointSize() + 16);
    display->setFont(font);

    display->installEventFilter(this);

    for (int i = 0; i < NumDigitButtons; ++i) {
    	digitButtons[i] = createButton(QString::number(i), SLOT(digitClicked()));
    }

    Button *pointButton = createButton(tr("."), SLOT(pointClicked()));
    Button *changeSignButton = createButton(tr("+/-"), SLOT(changeSignClicked()));

    Button *backspaceButton = createButton(tr("Backspace"), SLOT(backspaceClicked()));
    Button *clearAllButton = createButton(tr("Clear"), SLOT(clearAll()));

    divisionButton = createButton(tr("/"), SLOT(multiplicativeOperatorClicked()));
    timesButton = createButton(tr("*"), SLOT(multiplicativeOperatorClicked()));
    minusButton = createButton(tr("-"), SLOT(additiveOperatorClicked()));
    plusButton = createButton(tr("+"), SLOT(additiveOperatorClicked()));

#if 0
    Button *squareRootButton = createButton(tr("Sqrt"), SLOT(unaryOperatorClicked()));
    Button *powerButton = createButton(tr("x\262"), SLOT(unaryOperatorClicked()));
    Button *reciprocalButton = createButton(tr("1/x"), SLOT(unaryOperatorClicked()));
#endif

    Button *equalButton = createButton(tr("="), SLOT(equalClicked()));
    Button *exitButton = createButton(tr("Exit"), SLOT(close()));

    guideLabel = new QLabel;
    guideLabel->setAlignment(Qt::AlignCenter);

    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->setSizeConstraint(QLayout::SetFixedSize);

    mainLayout->addWidget(display, 0, 0, 1, 5);
    mainLayout->addWidget(backspaceButton, 1, 0, 1, 2);
    mainLayout->addWidget(clearAllButton, 1, 2, 1, 3);

    for (int i = 1; i < NumDigitButtons; ++i) {
        int row = ((9 - i) / 3) + 2;
        int column = ((i - 1) % 3) + 0;
        mainLayout->addWidget(digitButtons[i], row, column);
    }

    mainLayout->addWidget(digitButtons[0], 5, 0);
    mainLayout->addWidget(pointButton, 5, 1);
    mainLayout->addWidget(changeSignButton, 5, 2);

    mainLayout->addWidget(divisionButton, 2, 3);
    mainLayout->addWidget(timesButton, 3, 3);
    mainLayout->addWidget(minusButton, 4, 3);
    mainLayout->addWidget(plusButton, 5, 3);

#if 0
    mainLayout->addWidget(squareRootButton, 2, 4);
    mainLayout->addWidget(powerButton, 3, 4);
    mainLayout->addWidget(reciprocalButton, 4, 4);
#endif
    mainLayout->addWidget(equalButton, 4, 4, 2, 1);
    mainLayout->addWidget(exitButton, 2, 4, 2, 1);

    mainLayout->addWidget(guideLabel, 6, 0, 1, 5);

    setLayout(mainLayout);

#if 1
    /*
     * This flag can disable title bar, the setGeometry()
     * is also necessary
     */
    setWindowFlags(Qt::CustomizeWindowHint);
#endif

	setMovable(false); // Disable move by drag

    retranslateUi();
//    setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #E2EDED, stop: 1 #A7AFAF);");

}

void Calculator::digitClicked()
{
    Button *clickedButton = qobject_cast<Button *>(sender());
    int digitValue = clickedButton->text().toInt();
    if (display->text() == "0" && digitValue == 0.0)
        return;

    if (waitingForOperand) {
        display->clear();
		waitingForOperand = false;
    }
    display->setText(display->text() + QString::number(digitValue));
}

void Calculator::unaryOperatorClicked()
{
    Button *clickedButton = qobject_cast<Button *>(sender());
    QString clickedOperator = clickedButton->text();
    double operand = display->text().toDouble();
    double result = 0.0;

    if (clickedOperator == tr("Sqrt")) {
        if (operand < 0.0) {
            abortOperation();
            return;
        }
        result = sqrt(operand);
    } else if (clickedOperator == tr("x\262")) {
        result = pow(operand, 2.0);
    } else if (clickedOperator == tr("1/x")) {
        if (operand == 0.0) {
	    abortOperation();
	    return;
        }
        result = 1.0 / operand;
    }
    display->setText(QString::number(result));
    waitingForOperand = true;
}

void Calculator::additiveOperatorClicked()
{
    Button *clickedButton = qobject_cast<Button *>(sender());
    QString clickedOperator = clickedButton->text();
    double operand = display->text().toDouble();

    if (!pendingMultiplicativeOperator.isEmpty()) {
        if (!calculate(operand, pendingMultiplicativeOperator)) {
            abortOperation();
	    return;
        }
        display->setText(QString::number(factorSoFar));
        operand = factorSoFar;
        factorSoFar = 0.0;
        pendingMultiplicativeOperator.clear();
    }

    if (!pendingAdditiveOperator.isEmpty()) {
        if (!calculate(operand, pendingAdditiveOperator)) {
            abortOperation();
	    return;
        }
        display->setText(QString::number(sumSoFar));
    } else {
        sumSoFar = operand;
    }

    pendingAdditiveOperator = clickedOperator;
    waitingForOperand = true;
}

void Calculator::multiplicativeOperatorClicked()
{
    Button *clickedButton = qobject_cast<Button *>(sender());
    QString clickedOperator = clickedButton->text();
    double operand = display->text().toDouble();

    if (!pendingMultiplicativeOperator.isEmpty()) {
        if (!calculate(operand, pendingMultiplicativeOperator)) {
            abortOperation();
	    return;
        }
        display->setText(QString::number(factorSoFar));
    } else {
        factorSoFar = operand;
    }

    pendingMultiplicativeOperator = clickedOperator;
    waitingForOperand = true;
}

void Calculator::equalClicked()
{
    double operand = display->text().toDouble();

    if (!pendingMultiplicativeOperator.isEmpty()) {
        if (!calculate(operand, pendingMultiplicativeOperator)) {
            abortOperation();
	    return;
        }
        operand = factorSoFar;
        factorSoFar = 0.0;
        pendingMultiplicativeOperator.clear();
    }
    if (!pendingAdditiveOperator.isEmpty()) {
        if (!calculate(operand, pendingAdditiveOperator)) {
            abortOperation();
	    return;
        }
        pendingAdditiveOperator.clear();
    } else {
        sumSoFar = operand;
    }

    display->setText(QString::number(sumSoFar));
    sumSoFar = 0.0;
    waitingForOperand = true;
}

void Calculator::pointClicked()
{
    if (waitingForOperand)
        display->setText("0");
    if (!display->text().contains("."))
        display->setText(display->text() + tr("."));
    waitingForOperand = false;
}

void Calculator::changeSignClicked()
{
    QString text = display->text();
    double value = text.toDouble();

    if (value > 0.0) {
        text.prepend(tr("-"));
    } else if (value < 0.0) {
        text.remove(0, 1);
    }
    display->setText(text);
}

void Calculator::backspaceClicked()
{
    if (waitingForOperand)
        return;

    QString text = display->text();
    text.chop(1);
    if (text.isEmpty()) {
        text = "0";
        waitingForOperand = true;
    }
    display->setText(text);
}

void Calculator::clearAll()
{
    sumSoFar = 0.0;
    factorSoFar = 0.0;
    pendingAdditiveOperator.clear();
    pendingMultiplicativeOperator.clear();
    display->setText("0");
    waitingForOperand = true;
}

void Calculator::close()
{
	if (_lineEdit != NULL) {

		QString result = display->text();
		int data = MData::toInt(result);

		if (gMotor->isPostionValid(data)) {
			_lineEdit->setText(display->text());
		} else {
			_lineEdit->setText("0");
		}

		_lineEdit->selectAll();
	}
	RoundRectDialog::close();
}

void Calculator::setLineEdit(QLineEdit *lineEdit)
{
		_lineEdit= lineEdit;
}

void Calculator::moveToCenter()
{
	QWidget *d = qApp->desktop();

	int _x = d->width() / 2 - width() / 2;
	int _y = d->height() /2 - height() / 2;

	move(_x, _y);
}

Button *Calculator::createButton(const QString &text, const char *member)
{
    Button *button = new Button(text);
    connect(button, SIGNAL(clicked()), this, member);
    button->setProperty("custom", true);
    button->setMinimumHeight(48);
//    button->setStyleSheet("border: 1px solid #8f8f91;border-radius:3px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #f6f7fa, stop: 1 #dadbde);");
    return button;
}

void Calculator::abortOperation()
{
    clearAll();
    display->setText(tr("0"));
}

QString Calculator::getCalculatorResult()
{
	return display->text();
}

bool Calculator::calculate(double rightOperand, const QString &pendingOperator)
{
    if (pendingOperator == tr("+")) {
        sumSoFar += rightOperand;
    } else if (pendingOperator == tr("-")) {
        sumSoFar -= rightOperand;
    } else if (pendingOperator == tr("*")) {
        factorSoFar *= rightOperand;
    } else if (pendingOperator == tr("/")) {
	if (rightOperand == 0.0)
	    return false;
	factorSoFar /= rightOperand;
    }
    return true;
}


void Calculator::keyPressEvent(QKeyEvent *event)
{
//	qDebug() << "Key: " << event->key();

	switch(event->key()) {
	case SmtKb::Key_0:
	case SmtKb::Key_1:
	case SmtKb::Key_2:
	case SmtKb::Key_3:
	case SmtKb::Key_4:
	case SmtKb::Key_5:
	case SmtKb::Key_6:
	case SmtKb::Key_7:
	case SmtKb::Key_8:
	case SmtKb::Key_9:
		digitButtons[event->key() - SmtKb::Key_0]->click();
		break;
	case SmtKb::Key_Period:
		pointClicked();
		break;
	case SmtKb::Key_Equal:
		equalClicked();
		break;
	case SmtKb::Key_Plus:
		plusButton->click();
		break;
	case SmtKb::Key_Minus:
		minusButton->click();
		break;
	case SmtKb::Key_Multiply:
		timesButton->click();
		break;
	case SmtKb::Key_Div:
		divisionButton->click();
		break;
	case SmtKb::Key_F6:         // exit
		close();
		break;
	case SmtKb::Key_Clear:      // Clear
		clearAll();
		break;
	case SmtKb::Key_Help:
		break;
	default:
		RoundRectDialog::keyPressEvent(event);
	}
}

bool Calculator::eventFilter(QObject *target, QEvent *event)
{
	if (target == display) {
		if (event->type() == QEvent::KeyPress) {
			QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
			if (keyEvent->key() == SmtKb::Key_Backspace) {
				backspaceClicked();
				return true;
			}
			if (keyEvent->key() == SmtKb::Key_Exit) {
				this->close();
				return true;
			}
		}
	}
	return QObject::eventFilter(target, event);
}

void Calculator::showEvent ( QShowEvent * event )
{
	clearAll();

	QDialog::showEvent(event);
}

void Calculator::retranslateUi()
{
    setWindowTitle(tr("Calculator"));
    guideLabel->setText(tr("Press F6 to Exit."));
}
