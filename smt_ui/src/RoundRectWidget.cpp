/*
 * RoundRectWidget.cpp
 *
 *  Created on: 2010/04/27
 *      Author: kinhi
 */

#include "include/RoundRectWidget.h"

RoundRectWidget::RoundRectWidget(QWidget *parent, float r) : QWidget(parent, Qt::FramelessWindowHint | Qt::Tool) {
	// TODO Auto-generated constructor stub
	_radiusPercent = r;
	_movable = true; // movable by default
}

RoundRectWidget::~RoundRectWidget() {
	// TODO Auto-generated destructor stub
}

void RoundRectWidget::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        dragPosition = event->globalPos() - frameGeometry().topLeft();
        event->accept();
    }
}

void RoundRectWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (!_movable)
		return;

    if (event->buttons() & Qt::LeftButton) {
        move(event->globalPos() - dragPosition);
        event->accept();
    }
}

void RoundRectWidget::paintEvent(QPaintEvent * /* event */)
{
	QPainter painter(this);
	painter.setPen(QPen(Qt::darkGray, 2));

	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.drawRoundRect(0, 0, width(), height(),
			(int)(height() * _radiusPercent), (int)(width() * _radiusPercent));

	//QWidget::paintEvent(event);
}

void RoundRectWidget::resizeEvent(QResizeEvent * event)
{
	QBitmap bitmap(width(), height());
	bitmap.clear();
	QPainter painter(&bitmap);
	painter.setRenderHint(QPainter::Antialiasing, false);
	painter.setPen(QPen(Qt::black, 1));
	painter.setBrush(Qt::black);
	painter.drawRoundRect(0, 0, width(), height(),
			(int)(height() * _radiusPercent), (int)(width() * _radiusPercent));
	setMask(bitmap);
	QWidget::resizeEvent(event);
}

void RoundRectWidget::setMovable(bool movable)
{
	_movable = movable;
}
