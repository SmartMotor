/*
 * Program.cpp
 *
 *  Created on: 2010/03/21
 *      Author: crazysjf
 */

#include "include/Program.h"
#include <QFile>
#include <QDataStream>
#include <QMessageBox>
#include <QtGui>
#include <QVector>

//#define PROGRAM_DEBUG

static const char *TEXT_CODEC = "UTF-8";

Program::Program() {

}

//
// Though the file won't be load in the constructor.
// To load a file, call load() later.
//   @fileName: full path file name
//
Program::Program(const QString &fileName) {
	this->fileName = fileName;
}

Program::~Program() {
	// TODO Auto-generated destructor stub
}

//
// Init the _header.
// Why this is separated out from constructor is that,
//   when load a file, init is not needed, but when create a new file, init is needed.
//
void Program::init(void)
{
	memset(&_header, 0, sizeof(_header));
	_header.magic[0] = 'S';
	_header.magic[1] = 'M';
	_header.magic[2] = 'T';
	_header.magic[3] = '\0';
	_header.cutOffset = sizeof(_header);
}


int Program::load(void)
{
	QFile file(fileName);
	if (!file.exists())
		return 0;
	if (!file.open(QIODevice::ReadWrite)) {
		// TODO: error handling
		QMessageBox::critical(0, "Error",  QString("Can not open ") + fileName);
		return 0;
	}

	// Read in header
	file.read(reinterpret_cast<char *>(&_header), sizeof(_header));

	int descritionSize = _header.cutOffset - sizeof(_header);
	if (descritionSize != 0) {
		char *p = new char[descritionSize + 1];
		int len = file.read(p, descritionSize);
		if (len != descritionSize) {
			QMessageBox::warning(0, QString(tr("Loading")), QString(tr("Description load failed.")));
			return 0;
		}

		p[descritionSize] = '\0';

		QByteArray byteArray(p);

		QTextCodec *textCodec = QTextCodec::codecForName(TEXT_CODEC);
		if (textCodec == 0)
			QMessageBox::warning(0, tr("Loading"), tr("Text codec load failed."));

		QTextStream inStream(byteArray);
		inStream.setCodec(textCodec);

		_description = inStream.readAll();
		delete[] p;
	}

	_cuts.clear();

	int encodedData;
	while ( 0 != file.read(reinterpret_cast<char *>(&encodedData), sizeof(encodedData))) {
		_cuts.append(Cut(encodedData));
	}

	//updateLoadDateHistory(&file);
	file.close();

	return 1;
}

//
// Use this to change the file name.
// @fileName: full path file name
//
void Program::setFileName(const QString &fileName)
{
	this->fileName = fileName;
}


int Program::save(void)
{
	QFile file(fileName);
	if (!file.open(QIODevice::WriteOnly)) {
		QMessageBox::warning(0, QString("Saving"), QString("Can not open ") + fileName);
		return 0;
	}

	// Process _description.
	QTextCodec *codec = QTextCodec::codecForName(TEXT_CODEC);
	QByteArray b = codec->fromUnicode(_description);

	// Write _header. Update cutOffset first.
	_header.cutOffset = sizeof(_header) + b.size();
	int writeLen;
	int writtenLen;

	writeLen = sizeof(_header);
	writtenLen = file.write(reinterpret_cast<const char *>(&_header), sizeof(_header));

	// Write _description
	writeLen += b.size();
	writtenLen += file.write(b.constData(), b.size());

	// Write _cuts.
	int encodedData;
	for (int i = 0; i < _cuts.size(); ++i) {
#ifdef PROGRAM_DEBUG
		qDebug() << "writing cuts: " << i;
#endif
		encodedData = _cuts[i].encode();
		writeLen += sizeof(encodedData);
		writtenLen += file.write(reinterpret_cast<char *>(&encodedData), sizeof(encodedData));
	}

	file.close();

	if (writtenLen != writeLen) {
		QMessageBox::warning(0, tr("Saving"), "File " + fileName + " write error.");
		return 0;
	} else {
		return 1;
	}
}

void Program::updateLoadDateHistory()
{
	int i;
	QFile file(fileName);

	if (!file.open(QIODevice::ReadWrite)) {
		QMessageBox::warning(0, tr("Program"), QString("Can not open ") + fileName);
		return;
	}

	// Read in header
	file.read(reinterpret_cast<char *>(&_header), sizeof(_header));

	QVector<uint> v(FILE_LOAD_DATE_HISTORY_LENTH + 1);

	for (i=0; i<FILE_LOAD_DATE_HISTORY_LENTH; i++) {
		v[i] = _header.loadDateHistory[i];
	}

	v.prepend(QDateTime::currentDateTime().toTime_t());

	for (i=0; i<FILE_LOAD_DATE_HISTORY_LENTH; i++) {
		_header.loadDateHistory[i] = v[i];
	}

	// Write _header.
	file.seek(0);
	file.write(reinterpret_cast<const char *>(&_header), sizeof(_header));

	file.close();
}
