/*
 * TableView.cpp
 *
 *  Created on: 2010/03/07
 *      Author: crazysjf
 */

#include <ProgramEditor.h>
#include <QStandardItem>
#include <QFile>
#include <QHeaderView>
#include <QDataStream>
#include <System.h>
#include <QMessageBox>
#include <QString>
#include <QFont>

#include "OpenFileDialog.h"
#include "ProgramEditorDelegate.h"
#include "ProgramEditorDelegate.h"
#include "Debug.h"
#include "Global.h"

//#define PROGRAMEDITOR_DEBUG

#define WIDGET_HEIGHT_FACTOR    7
#define WIDGET_HEIGHT_DEVIDOR   12

ProgramEditor::ProgramEditor()
{
	view.setModel(&model);

	ProgramEditorDelegate *flagDelegate = new ProgramEditorDelegate(ProgramEditor::POSITION_COLUMN, ProgramEditor::FLAGS_COLUMN);
	view.setItemDelegate(flagDelegate);
	QFont viewFont = this->font();
	viewFont.setPointSize(gSysConfig->getDataListFontSize());
	view.setFont(viewFont);
	view.resizeRowsToContents();
	view.resizeColumnsToContents();
	view.horizontalHeader()->setStretchLastSection(true);
	view.setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	view.setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

	// Column size
	view.setColumnWidth(0, POSITION_COLUMN_WIDTH);
	view.setColumnWidth(1, FLAGS_COLUMN_WIDTH);

	// column size can NOT be changed
	view.horizontalHeader()->setResizeMode(QHeaderView::Fixed);

	// Set focus to table view if we get focus
	setFocusProxy(&view);

	connect(&view, SIGNAL(selectionChangedSignal(const QItemSelection &, const QItemSelection &)),
			this, SLOT(handleSelectionChanged(const QItemSelection &, const QItemSelection &)));

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addWidget(&view);
	setLayout(layout);

	// Set margins to 0 so that we can get the view geometry via programEditor.
	setContentsMargins(0, 0, 0, 0);

	setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);


	retranslateUi();
}

void ProgramEditor::retranslateUi()
{
	QStringList horizontalHeaderString;
	horizontalHeaderString << tr("Position") << tr("Flags");
	model.setHorizontalHeaderLabels(horizontalHeaderString);
}

QSize ProgramEditor::sizeHint() const
{
	//	Default height is main window's 7/12
	QSize size(DEFAUT_MAIN_W, DEFAUT_MAIN_H * WIDGET_HEIGHT_FACTOR / WIDGET_HEIGHT_DEVIDOR);

	return size;
}

int ProgramEditor::openProgramFile(QString &fileName)
{
	int retval;

	// Remove all rows in the model
	model.removeRows(0, model.rowCount());

	currentProgram.setFileName(fileName);
	retval = currentProgram.load();
	if (!retval)
		return 0;
	currentProgram.increaseUseCount();
	gSysConfig->saveLastOpenFile(fileName);

	QVector<Cut> cuts = currentProgram.cuts();

	QList<Row> rows;
	for (int row=0; row < cuts.count(); row++) {
		Row r;
		r.columns[POSITION_COLUMN] = cuts[row].position;
		r.columns[FLAGS_COLUMN]    = cuts[row].flags;
		rows << r;
	}
	model.insertRows(0, rows);

	// Set index to the first data
	topIndex();

#if 0
//	view.resizeRowsToContents();

	qDebug() << "V size " << view.verticalHeader()->sectionSize(0);
	qDebug() << "H size " << view.horizontalHeader()->sectionSize(0);
	qDebug() << "Default size " << view.verticalHeader()->defaultSectionSize();
#endif

	return 1;
}

///
// Save current open file.
//
int ProgramEditor::saveProgramFile()
{
	currentProgram.cuts().clear();

	for (int row=0; row < model.rowCount(); row++) {
		QModelIndex index = model.index(row, POSITION_COLUMN);
		int position = 	model.data(index).toInt();
		index = model.index(row, FLAGS_COLUMN);
		int flags = model.data(index).toInt();
#ifdef PROGRAMEDITOR_DEBUG
		qDebug() << "saving " << position << " " << flags;
#endif
		Cut c(position, flags);
		currentProgram.cuts().append(c);
	}
	currentProgram.save();

	return 1;
}

void ProgramEditor::zoomInTableFont()
{
	// TODO step
	int step = 3;
	setTableFontSize(step);
}

void ProgramEditor::zoomOutTableFont()
{
	// TODO step
	int step = -3;
	setTableFontSize(step);
}

/*
 * With default font size, it shows 8 lines
 *  - zoom in:  7 lines
 *  - zoom out: 9 lines
 */
void ProgramEditor::setTableFontSize(int step)
{
	QFont tableFont = view.font();
	int size = tableFont.pointSize();
	size += step;

//	if (gSysConfig->isFontSizeValid(size)) {
	if ((size >= DATA_LIST_FONT_SIZE_MIN) && (size <= DATA_LIST_FONT_SIZE_MAX)) {
#if 0 // Debug
	qDebug() << "Change font size from " << tableFont.pointSize() << " to " << size << endl;
#endif
		tableFont.setPointSize(size);
		view.setFont(tableFont);
		gSysConfig->setDataListFontSize(size);

		view.horizontalHeader()->setFont(tableFont);
		view.verticalHeader()->setFont(tableFont);

#if 1
		int sectionSize = view.verticalHeader()->defaultSectionSize();
		view.verticalHeader()->setDefaultSectionSize(sectionSize + step);
		view.verticalHeader()->resizeSections(QHeaderView::Fixed);
#else
		view.resizeRowsToContents();
#endif
		view.resizeColumnsToContents();
		view.horizontalHeader()->setStretchLastSection(true);

		// Column size
		view.setColumnWidth(0, POSITION_COLUMN_WIDTH);
		view.setColumnWidth(1, FLAGS_COLUMN_WIDTH);
	}

#if 0
	qDebug() << "V size " << view.verticalHeader()->sectionSize(0);
	qDebug() << "H size " << view.horizontalHeader()->sectionSize(0);
	qDebug() << "Default size " << view.verticalHeader()->defaultSectionSize();
#endif
}

///
// Insert data to row.
// This is pre-insert, and row begins from 0.
//
void ProgramEditor::insertDataRow(int pos, int flags, int row)
{
	if (!gMotor->isPostionValid(pos))
		return;

	if (row >= MAX_DATA_NUMBER_PER_FILE)
		return;

	QList<int> newRow;
	newRow << pos << flags;
	model.insertRow(row, newRow);

	QModelIndex index = model.index(row, POSITION_COLUMN);
	view.setCurrentIndex(index);
	view.resizeRowToContents(row);
}

///
// Pre-insert data to current index.
//
void ProgramEditor::insertData(int pos, int flags)
{
	int row = view.currentIndex().row();

	// Handle the situation when there is no selection.
	if (row < 0)
		row = 0;

	insertDataRow(pos, flags, row);

	saveProgramFile();
}

///
// Append one data to tail of the list
//
void ProgramEditor::appendData(int pos, int flags)
{
	int row = model.rowCount();
	insertDataRow(pos, flags, row);
}

///
// Modify data at current index.
//
void ProgramEditor::modifyData(int pos, int flags)
{
	if (!gMotor->isPostionValid(pos))
		return;

	int row = view.currentIndex().row();
	if (row < 0)
		return;

	QModelIndex index = model.index(row, 0);
	model.setData(index, pos, Qt::DisplayRole);
	index = model.index(row, 1);
	model.setData(index, flags, Qt::DisplayRole);


	saveProgramFile();
}

///
// Adjust all data's position by @delta.
//   @delta: Delta to original value. Positive, negative, 0 is all OK.
// Note: items whose data is out of range after adjust will be removed.
//
// 2010/07/26: Requirement
//   start: current index
//   end:   the first row whose value is larger then the former one
void ProgramEditor::adjustPositionAll(int delta)
{
#if 0
	int rowCount = model.rowCount();
	for (int row=rowCount - 1; row >= 0; row--) {
		QStandardItem *positionI = model.item(row, POSITION_COLUMN);

		int newPos = positionI->data(Qt::DisplayRole).toInt() + delta;
		if (gMotor->isPostionValid(newPos)) {
			positionI->setData(newPos, Qt::DisplayRole);
		} else {
			model.removeRow(row);
		}
	}

	saveProgramFile();
#else
	int row = view.currentIndex().row();
	int rowCount = model.rowCount();
	int nextIndex = row;
	QModelIndex index;

	do {
		index = model.index(row, POSITION_COLUMN);
		int curPos = model.data(index, Qt::DisplayRole).toInt();
		int newPos = curPos + delta;
		if (gMotor->isPostionValid(newPos)) {
			model.setData(index, newPos, Qt::DisplayRole);
		} else {
			model.removeRow(row);
			rowCount = model.rowCount(); // update total count
		}

		if (row < (rowCount - 1)) {
			// Not the last row
			index = model.index(row + 1, POSITION_COLUMN);
			int nextPos = model.data(index, Qt::DisplayRole).toInt();
			if (curPos < nextPos) {
				nextIndex = row + 1;
				break;
			}
		}

		row ++;
	} while (row < rowCount);

	index = model.index(nextIndex, 0);
	view.setCurrentIndex(index);

	saveProgramFile();
#endif
}

///
// Delete data at current index
//
void ProgramEditor::deleteData()
{
	int row = view.currentIndex().row();
	if (row < 0)
		return;

	model.removeRow(row);
	if (row > model.rowCount() - 1)
		row = model.rowCount() - 1;

	QModelIndex index = model.index(row, 0);
	view.setCurrentIndex(index);

	if (row == 0) {
		clearFlag(FlagEditor::FLAG_CUT);
	}

	// To fix the step display bug
	// Without this, the step shows the original one
	emit dataIndexChanged(model.rowCount(), row);

	saveProgramFile();
}

///
// Delete all data in model.
//
void ProgramEditor::deleteAllData()
{
	model.removeRows(0, model.rowCount());

	saveProgramFile();
}

///
// 标签编程 (插入多行).
//
//  |   labelLen   | unusedLen |   labelLen   | unusedLen | ... |
//  |<-------------------- totalLen --------------------------->|
//
// Position at each boundary will be calculated, and be inserted if is valid.
//
void ProgramEditor::labelInsert(int totalLen, int labelLen, int unusedLen)
{
	int defaultFlags = 0;
	int pos = totalLen, n = 0;
	int available_rows = MAX_DATA_NUMBER_PER_FILE - model.rowCount();

	if (available_rows < 0)
		return;

	if (totalLen < 0 || labelLen < 0 || unusedLen < 0) {
		QMessageBox::warning(this, tr("Insert"), tr("Invalid argument "));
		return;
	}

	QList<Row> rows;
	Row r;

	r.columns[FLAGS_COLUMN] = defaultFlags;
	if(gMotor->isPostionValid(pos)) {
		r.columns[POSITION_COLUMN] = pos;
		rows << r;
	}

	for ( ; (pos >= 0) && (n < available_rows); n += 2) {

		pos -= labelLen;
		if (!gMotor->isPostionValid(pos))
			continue;
		r.columns[POSITION_COLUMN] = pos;
		rows << r;

		pos -= unusedLen;
		if (!gMotor->isPostionValid(pos))
			continue;
		r.columns[POSITION_COLUMN] = pos;
		rows << r;
	}

	model.insertRows(model.rowCount(), rows);

	// FIXME: Why the font will be changed after insert?
	setTableFontSize(0);

	saveProgramFile();
}

///
// 等分编程 (插入多行).
// Row n's position pos[n] = totalLen - n * [totalLen / divider]
//
void ProgramEditor::averageInsert(int totalLen, int divider, int mode)
{
	int defaultFlags = 0;
	int pos, n = 0;
	int available_rows = MAX_DATA_NUMBER_PER_FILE - model.rowCount();

	if (available_rows < 0)
		return;

	if (totalLen < 0 || divider <= 0) {
		QMessageBox::warning(this, tr("Insert"), tr("Invalid argument "));
		return;
	}

	QList<Row> rows;

	if (mode == AVERAGE_NUMBER_MODE) {
		/* 等分数模式 */

		for (pos = totalLen - n * totalLen / divider;
				(pos >= 0) && (n < available_rows);
				n++, pos = totalLen - n * totalLen / divider) {
			Row r;

			if (!gMotor->isPostionValid(pos))
				continue;

			r.columns[POSITION_COLUMN] = pos;
			r.columns[FLAGS_COLUMN] = defaultFlags;
			rows << r;
		}
	} else {
		/*
		 * 等分值模式
		 * divider的值现在表示长度
		 */

		for (pos = totalLen - n * divider;
				(pos >= 0) && (n < available_rows);
				n++, pos = totalLen - n * divider) {
			Row r;

			if (!gMotor->isPostionValid(pos))
				continue;

			r.columns[POSITION_COLUMN] = pos;
			r.columns[FLAGS_COLUMN] = defaultFlags;
			rows << r;
		}
	}

	model.insertRows(model.rowCount(), rows);

	// FIXME: Why the font will be changed after insert?
	setTableFontSize(0);

	saveProgramFile();
}

///
// Relative edit data and append data to last
//   delta: input data, converted by MData::toInt()
//   operation_mode: add/sub/multiply/divide
// delta
void ProgramEditor::relativeAppend(int delta, int operation_mode)
{
	int newPos = 0;
	int row = model.rowCount();

	if (row <= 0) {
		return;
	}

	QModelIndex index = model.index(row - 1, POSITION_COLUMN);
	int lastPos = model.data(index, Qt::DisplayRole).toInt();

	switch(operation_mode) {
	case RELATIVE_ADD:
		newPos = lastPos + delta;
		break;
	case RELATIVE_SUB:
		newPos = lastPos - delta;
		break;
	case RELATIVE_MUL:
		newPos = (lastPos * delta) / 100;
		break;
	case RELATIVE_DIV:
		if (delta != 0) {
			newPos = lastPos * 100 / delta;
		} else {
			return;
		}
		break;
	default:
		break;
	}

	appendData(newPos);

	saveProgramFile();
}

///
// Toggle currently selected data's flag at @flagIndex
//  If @flagIndex is out of range, do nothing.
//
void ProgramEditor::toggleFlag(int flagIndex)
{
	if (flagIndex >= FlagEditor::FLAG_NR)
		return;

	int row = view.currentIndex().row();
	if (row < 0)
		return;

	// The first data can't have cut flag
	if ((row == 0) && (flagIndex == FlagEditor::FLAG_CUT))
		return;

	QModelIndex index = model.index(row, FLAGS_COLUMN);
	int flags = model.data(index, Qt::DisplayRole).toInt();
	int thisFlag = flags & (1 << flagIndex);

	// clear other flags in this group
	int groupShift = (flagIndex / FlagEditor::FLAG_GROUP_BITS) * FlagEditor::FLAG_GROUP_BITS;
	flags &= ~(FlagEditor::FLAG_GROUP_MASK << groupShift);

	// restore this flag
	flags |= thisFlag;

	// toggle flag
	flags  ^= (1 << flagIndex);
	model.setData(index, flags, Qt::DisplayRole);
}

void ProgramEditor::clearFlag(int flagIndex)
{
	int row = view.currentIndex().row();
	if (row < 0)
		return;

	QModelIndex index = model.index(row, FLAGS_COLUMN);
	int flags = index.data(Qt::DisplayRole).toInt();
	flags  &= ~(1 << flagIndex);
	model.setData(index, flags, Qt::DisplayRole);
}

void ProgramEditor::clearFlags(void)
{
	int row = view.currentIndex().row();
	if (row < 0)
		return;

	QModelIndex index = model.index(row, FLAGS_COLUMN);
	model.setData(index, 0, Qt::DisplayRole);

	saveProgramFile();
}

Program & ProgramEditor::getCurrentProgram()
{
	return currentProgram;
}


// Get current selected data
void ProgramEditor::getCurrentData(int *pos, int *flag)
{
	int row = view.currentIndex().row();
	if (row < 0)
		return;

	QModelIndex index = model.index(row, POSITION_COLUMN);
	*pos = model.data(index, Qt::DisplayRole).toInt();
	index = model.index(row, FLAGS_COLUMN);
	*flag = model.data(index, Qt::DisplayRole).toInt();
}

// Get next data and move index to next
// If current is the last one, move index to 0 (first), and
// set last to true, otherwise, last is false.
void ProgramEditor::getNextData(int *pos, int *flag, bool *last)
{
	int row = view.currentIndex().row();
	if (row < 0)
		return;

	// Next Row
	row = row + 1;
	if (row >= model.rowCount()) {
		// The last
		*last = true;

		// Index set to 0, the first one
		row = 0;
	} else {
		*last = false;
	}

	QModelIndex index = model.index(row, POSITION_COLUMN);
	*pos = model.data(index, Qt::DisplayRole).toInt();
	index = model.index(row, FLAGS_COLUMN);
	*flag = model.data(index, Qt::DisplayRole).toInt();

	view.selectRow(row);
}

void ProgramEditor::topIndex()
{
	if (model.rowCount() > 0) {
		view.selectRow(0);
	}
}

void ProgramEditor::lastIndex()
{
	int count = model.rowCount();
	if (count > 0) {
		view.selectRow(count - 1);
	}
}

int  ProgramEditor::getTotalDataNumber()
{
	return model.rowCount();
}

// Signal is passed like:
//   TableView -> ProgramEditor -> PaperPusherBar
void ProgramEditor::handleSelectionChanged( const QItemSelection & /* selected */, const QItemSelection & /* deselected */)
{
	emit dataIndexChanged(model.rowCount(), view.currentIndex().row());
}

QWidget *ProgramEditor::tableView()
{
	return (QWidget *)(&view);
}

void ProgramEditor::forceKeyPressEvent(QKeyEvent *event)
{
	int index = view.currentIndex().row();

	switch(event->key()) {
	case SmtKb::Key_Up:
		if (index == 0) {
			lastIndex();
		} else {
			view.moveCursorUp();
		}
		break;
	case SmtKb::Key_Down:
		if (index == (model.rowCount() - 1)) {
			topIndex();
		} else {
			view.moveCursorDown();
		}
		break;
	case SmtKb::Key_PageUp:
		view.moveCursorPageUp();
		break;
	case SmtKb::Key_PageDown:
		view.moveCursorPageDown();
		break;
	default:
		break;
	}

}

