/*
 * TimeLabel.cpp
 *
 *  Created on: 2010/03/06
 *      Author: crazysjf
 */

#include <QtGui>
#include <QDateTime>
#include <TimeLabel.h>

TimeLabel::TimeLabel()
{
	timer = new QTimer;
	timer->setInterval(500);
	timer->setSingleShot(false);
	connect(timer, SIGNAL(timeout()), this, SLOT(updateDateTime()));

	clockCalendar = new ClockCalendarWidget;

	updateDateTime();
	timer->start();
}

TimeLabel::~TimeLabel()
{
	delete timer;
}

void TimeLabel::mousePressEvent( QMouseEvent * /*ev*/ )
{
	toggleCalendar();
}

void TimeLabel::toggleCalendar()
{
	if (clockCalendar->isHidden()) {
		QPoint global = mapToGlobal(QPoint(0,0));
		/*
		 *  We need to call show() before move(), or clockCalendar->width() won't get correct value.
		 */
		clockCalendar->show();

		/*
		 * Put calendar right down of time table, and align the horizontal center of
		 * calendar and time table. And make sure calendar won't go out of screen.
		 */
		int x = global.x() + width() / 2 - clockCalendar->width() / 2;
		int maxX = QApplication::desktop()->geometry().right() - clockCalendar->width();

		if (x <= 0)
			x = 0;
		if (x > maxX)
			x = maxX;
		int y = global.y() + height();

		clockCalendar->move(x, y);
	} else {
		clockCalendar->hide();
	}
}

void TimeLabel::updateDateTime()
{
	QDateTime dateTime = QDateTime::currentDateTime();
	setText(dateTime.toString("yyyy.MM.dd hh:mm:ss"));
}
