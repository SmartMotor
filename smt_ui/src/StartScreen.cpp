/*
 * StartScreen.cpp
 *
 *  Created on: Apr 27, 2010
 *      Author: souken-i
 */
#include <QtGui>
#include <QTimer>
#include <QWSServer>

#include "StartScreen.h"
#include "Calibration.h"
#include "KeyboardCheck.h"
#include "Global.h"


StartScreen::StartScreen(QWidget *parent)
	: QDialog(parent)
{
	_subDialogEnable = 0;

    QRect desktop = QApplication::desktop()->geometry();
    desktop.moveTo(QPoint(0, 0));
    setGeometry(desktop);

    // TODO: load start.png -> start.jpg
    pixmap = QPixmap("images/start.png");

    runButton = new ToolButton;
    runButton->setIcon(QIcon("images/run-64.png"));
    runButton->setIconSize(QSize(128, 48));
    connect(runButton, SIGNAL(clicked()), this, SLOT(start()));

	startLabel = new QLabel;
//	startLabel->setAlignment(Qt::AlignCenter);
	startLabel->setAlignment(Qt::AlignLeft);
	startLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

	helpLabel = new QLabel;
	helpLabel->setAlignment(Qt::AlignLeft);
	helpLabel->setShown(false); // Hide by default

#if 0
	if ( !gMotor->isMotorPresent() ) {
		// Hide them first
		startLabel->setShown(false);
		runButton->setShown(false);

		QTimer timer;
		timer.setInterval(5);
		timer.setSingleShot(true);
        connect(&timer, SIGNAL(timeout()), this, SLOT(checkMotorPresent()));

        qDebug() << "Start timer";
		timer.start();
	}
#endif

	QVBoxLayout *leftLayout = new QVBoxLayout;
	leftLayout->addWidget(helpLabel);
	leftLayout->addWidget(startLabel);

    QHBoxLayout *bottomLayout = new QHBoxLayout;
    bottomLayout->addSpacing(30);
    bottomLayout->addLayout(leftLayout);
    bottomLayout->addWidget(runButton);
    bottomLayout->addSpacing(30);
    bottomLayout->setStretchFactor(startLabel, 1);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addStretch(1);
    layout->addLayout(bottomLayout);
    setLayout(layout);

    retranslateUi();
}

StartScreen::~StartScreen()
{
}

void StartScreen::start()
{
	// Send start command to motor
	gMotor->start();

	close();
}
void StartScreen::checkMotorPresent()
{
	qDebug() << "Timer out" << endl;

	if ( gMotor->isMotorPresent() ) {
		// present
		startLabel->setShown(true);
		runButton->setShown(true);
	} else {
		// TODO
		// error dialog
		qDebug() << "Can not found the motor" << endl;
		close();
	}
}

void StartScreen::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    p.fillRect(rect(), Qt::black);

    // Construct a rect r in the topleft corner with
    // the size of the pixmap
    QRect r(QPoint(0, 0), pixmap.size());

    // Let r's center be the center of the widget and draw the
    // pixmap there
    r.moveCenter(rect().center());

    p.drawPixmap(r, pixmap);

	QWidget::paintEvent(event);
}

void StartScreen::keyPressEvent(QKeyEvent *event)
{
//	qDebug() << "Key Pressed: " << hex << event->key();

	switch(event->key()) {
	case SmtKb::Key_Enter:
	case Qt::Key_Enter:
		break;
	case SmtKb::Key_F1:
		if (_subDialogEnable)
			keyboardCheck();
		break;
	case SmtKb::Key_F2:
		if (_subDialogEnable)
			touchscreenCalibration();
		break;
	case SmtKb::Key_Help:
		_subDialogEnable = 1;
		showHelp();
		break;
	case SmtKb::Key_Run:
		// FIXME
		close();
		break;
	case SmtKb::Key_Stop:
		break;
	default:
		QDialog::keyPressEvent(event);
	}
}

void StartScreen::touchscreenCalibration()
{
	if (QWSServer::mouseHandler()) {
		Calibration dlg;
		dlg.exec();
	} else {
		QMessageBox message;
		message.setText(tr("No touchscreen device found."));
		message.exec();
	}
}

void StartScreen::showHelp()
{
	helpLabel->setText(tr("F1: Keyboard Check; F2: Touch Screen Calibration"));
	helpLabel->setShown(true);
}

void StartScreen::keyboardCheck()
{
	KeyboardCheck dlg;
	dlg.exec();
}

void StartScreen::retranslateUi()
{
	startLabel->setText(tr("Press RUN button to start motor..."));
}
