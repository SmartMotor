/*
 * MachineSettingPages.h
 *
 *  Created on: Apr 15, 2010
 *      Author: Zeng Xianwei
 */

#ifndef MACHINESETTINGPAGES_H_
#define MACHINESETTINGPAGES_H_

#include <QWidget>
#include <QLabel>
#include <QSpinBox>
#include <QCalendarWidget>
#include <QCheckBox>
#include <QTimer>
#include <QStackedWidget>
#include <QComboBox>
#include "Button.h"
#include "TransButton.h"
#include "ConfigSelector.h"
#include "SystemSettingsPage.h"
#include "System.h"
#include "MotorLogView.h"
#include "SpinBox.h"

class MotorParametersPage : public QWidget
{
	Q_OBJECT

public:
	MotorParametersPage(QWidget *parent = 0);
	virtual ~MotorParametersPage();

	void retranslateUi();

protected:
    void keyPressEvent(QKeyEvent *event);
    void showEvent(QShowEvent *event);

private slots:
	void resetParameters();
	void saveParameters();
	void setLimitState(int state);
	void focusChangedEvent(QWidget *pre, QWidget *cur);

private:
	void updateHelpMessage();

	QCheckBox *limitsCheckBox;
    QLabel    *frontLimitLabel;
    SpinBox   *frontLimitEdit;
    QLabel    *midLimitLabel;
    SpinBox   *midLimitEdit;
    QLabel    *backLimitLabel;
    SpinBox   *backLimitEdit;
    QLabel    *motorSpeedLabel;   // Pusher speed
    SpinBox   *motorSpeedEdit;
    QLabel    *runSpeedLabel;     // Jog run speed
    SpinBox   *runSpeedEdit;
    QLabel    *airDistanceLabel;
    SpinBox   *airDistanceEdit;
    QLabel    *stepSizeLabel;
    SpinBox   *stepSizeEdit;

    QLabel    *helpMessageLabel;

    Button *resetButton;
    Button *saveButton;
};

/******************************************************************************/
class MaintenancePage : public QWidget
{
	Q_OBJECT

public:
	MaintenancePage(QWidget *parent = 0);
	virtual ~MaintenancePage();

	enum {
		MaintenanceIconNumber = 4,
	};
	void retranslateUi();

protected:
	void showEvent(QShowEvent *event);

private slots:
	void nextPicture();
	void previousPicture();

private:
	Button *createButton(const QIcon &icon, const char *member);
	void showIcon(int index);

	int _currentIcon;
	QLabel *maintenanceIcon;
	Button *previousButton;
	Button *nextButton;

	QLabel *logLabel;

	MotorLogView *logView;
};

/******************************************************************************/
class AdvancedMotorParametersPage : public QWidget
{
	Q_OBJECT

public:
	AdvancedMotorParametersPage(QWidget *parent = 0);
	virtual ~AdvancedMotorParametersPage();

	void retranslateUi();

protected:
	void showEvent(QShowEvent *event);

private slots:
	void showVersion();

private:
	TransButton *versionButton;
};

/******************************************************************************/
class DateTimePage : public QWidget
{
	Q_OBJECT

public:
	DateTimePage(QWidget *parent = 0);
	virtual ~DateTimePage();
	void retranslateUi();

protected:
    void keyPressEvent(QKeyEvent *event);
    void showEvent(QShowEvent *event);

private slots:
	void saveDateTime();
	void updateTime();

private:
    QCalendarWidget *sysDateCalendar;
    QLabel *currentSysTimeLabel;
	QLabel *currentSysTime;
	QLabel *sysTimeHourLabel;
	QLabel *sysTimeMinuteLabel;
	QLabel *sysTimeSecondLabel;
	SpinBox *sysTimeHourEdit;
	SpinBox *sysTimeMinuteEdit;
	SpinBox *sysTimeSecondEdit;

	QTimer timer;

    Button *saveButton;
};

/******************************************************************************/
class TouchScreenCalibrateWidget : public QWidget
{
public:
	TouchScreenCalibrateWidget(QWidget *parent = 0);
	virtual ~TouchScreenCalibrateWidget();

	void retranslateUi();
	void touchscreenCalibration();

protected:
	void mousePressEvent(QMouseEvent *event);

private:
	QLabel *guideLabel;
};

#endif /* MACHINESETTINGPAGES_H_ */
