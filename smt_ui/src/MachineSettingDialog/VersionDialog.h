/*
 * VersionDialog.h
 *
 *  Created on: Jun 8, 2010
 *      Author: souken-i
 */

#ifndef VERSIONDIALOG_H_
#define VERSIONDIALOG_H_

#include <QWidget>
#include <QLabel>
#include "TransButton.h"

#include "RoundRectDialog.h"

class VersionDialog : public RoundRectDialog
{
public:
	VersionDialog(QWidget *parent = 0);
	virtual ~VersionDialog();

	void retranslateUi();

protected:
	void mouseMoveEvent(QMouseEvent *event);

private:
	QLabel *versionLabel;
	QLabel *copyrightLabel;
	TransButton *okButton;
};

#endif /* VERSIONDIALOG_H_ */
