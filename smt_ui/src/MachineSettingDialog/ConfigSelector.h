/*
 * ConfigSelector.h
 *
 *  Created on: Apr 17, 2010
 *      Author: souken-i
 */

#ifndef CONFIGSELECTOR_H_
#define CONFIGSELECTOR_H_

#include <QWidget>
#include <QListView>
#include <QStandardItemModel>
#include <QIcon>

class ColorSelector : public QListView
{
	Q_OBJECT

public:
	ColorSelector(QWidget * parent = 0);
	virtual ~ColorSelector();

	QColor getColor();

signals:
	void selectionChangedSignal(QColor &color);

protected:
	void selectionChanged ( const QItemSelection & selected, const QItemSelection & deselected );

private:
	void setupColorTable();

	QStandardItemModel *model;
};

/*********************************************************/
class LanguageSelector : public QListView
{
	Q_OBJECT

public:
	LanguageSelector(QWidget * parent = 0);
	virtual ~LanguageSelector();

	int  getLanguage(QString *name, QIcon *icon);
	QString getLanguageName(int row);
	QIcon   getLanguageIcon(int row);
	int     getSelectedLanguageID();
	void setIndex(int row);

signals:
	void selectionChangedSignal(int id);

protected:
	void selectionChanged ( const QItemSelection & selected, const QItemSelection & deselected );

private:
	void setupLanguageTable();

	QStandardItemModel *model;
};

/*********************************************************/
class FontSizeSelector : public QListView
{
	Q_OBJECT

public:
	FontSizeSelector(QWidget * parent = 0);
	virtual ~FontSizeSelector();

	int getFontSize();
	void setupFontSizeTable(int min, int max, int selected_size = 0);

signals:
	void selectionChangedSignal(int size);

protected:
	void selectionChanged ( const QItemSelection & selected, const QItemSelection & deselected );

private:
	int _font_size_min, _font_size_max;

	QStandardItemModel *model;
};
#endif /* CONFIGSELECTOR_H_ */
