/*
 * machinesettingdialog.cpp
 *   Machine Setting Dialog Class
 *
 * Author:    Zeng Xianwei (xianweizeng@gmail.com)
 * ChangeLog: 2010/03/19     Initial version
 *
 */
#include <QtGui>
#include "MachineSettingDialog.h"
#include "MachineSettingPages.h"

#include "Global.h"

using namespace::std;

MachineSettingDialog::MachineSettingDialog(QWidget *parent)
    : QDialog(parent)
{
	titleLabel = new QLabel;
	titleLabel->setAlignment(Qt::AlignCenter);
	QFont font = this->font();
	font.setPointSize(28);
	font.setBold(true);
	titleLabel->setFont(font);

	QPalette palette;
	palette.setColor(QPalette::WindowText, QColor("red"));
	titleLabel->setPalette(palette);
	titleLabel->setAutoFillBackground(true);

    createPages();

    configPages = new QStackedWidget;
    configPages->addWidget(motorParameterSettingWidget);
    configPages->addWidget(maintenceWidget);
    configPages->addWidget(dateTimeWidget);
    configPages->addWidget(systemSettingWidget);
    configPages->addWidget(tsCalibrateWidget);
    configPages->addWidget(advancedSettingWidget);

	configItems = new QListWidget;
    configItems->setViewMode(QListView::IconMode);
    // FIXME: We change size from 64 to 48 to fit 800x600 screen
    configItems->setIconSize(QSize(48, 48));
    configItems->setMovement(QListView::Static);
    configItems->setMinimumWidth(96);
    configItems->setMaximumWidth(128);
    configItems->setSpacing(24);

    createIcons();
    configItems->setCurrentRow(0);

    // Disable its focus so that up/down arrow can be used to change focus
    configItems->setFocusPolicy(Qt::NoFocus);


    closeButton = new Button;
    closeButton->setIcon(QIcon("images/delete-64.png"));
    closeButton->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    closeButton->setIconSize(QSize(64, 64));
#if 0
    // We are going to HIDE the dialog, not close it.
    connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
#else
    connect(closeButton, SIGNAL(clicked()), this, SLOT(accept()));
#endif
    closeButton->setProperty("custom", true);

    QVBoxLayout *leftLayout = new QVBoxLayout;
    leftLayout->addWidget(configItems);
    leftLayout->addWidget(closeButton);
    leftLayout->addSpacing(5);

    QVBoxLayout *rightLayout = new QVBoxLayout;
    rightLayout->addWidget(titleLabel);
    rightLayout->addWidget(configPages);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addLayout(leftLayout);
    layout->addLayout(rightLayout);

    setLayout(layout);

    loadStyleSheet(QString(":/qss/setting.qss"));

//    qDebug() << "selection behavior " << configItems->selectionBehavior() << endl;

    this->showFullScreen();
	retranslateUi();
}

MachineSettingDialog::~MachineSettingDialog()
{

}

void MachineSettingDialog::createIcons()
{

	iconButton[MotorParameterSettings] = createIcon(QIcon("images/MS-F1-parameters.png"));
	iconButton[MaintenanceSettings   ] = createIcon(QIcon("images/MS-F2-maintenance.png"));
	iconButton[DateTimeSettings      ] = createIcon(QIcon("images/MS-F3-datetime.png"));
	iconButton[SystemSettings        ] = createIcon(QIcon("images/MS-F4-system.png"));
	iconButton[TouchScreenCalibration] = createIcon(QIcon("images/MS-F5-tscalibrate.png"));
	iconButton[AdvancedMotorSettings ] = createIcon(QIcon("images/MS-F6-advanced.png"));

    connect(configItems,
            SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)),
            this, SLOT(changePage(QListWidgetItem*,QListWidgetItem*)));
}

QListWidgetItem *MachineSettingDialog::createIcon(const QIcon &icon)
{
	QListWidgetItem *listItem = new QListWidgetItem(configItems);
	listItem->setIcon(icon);
	listItem->setTextAlignment(Qt::AlignCenter);
	listItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

	return listItem;
}

void MachineSettingDialog::createPages()
{
	motorParameterSettingWidget = new MotorParametersPage(this);
	maintenceWidget             = new MaintenancePage(this);
	dateTimeWidget              = new DateTimePage(this);
	systemSettingWidget         = new SystemSettingsPage(this);
	tsCalibrateWidget           = new TouchScreenCalibrateWidget(this);
	advancedSettingWidget       = new AdvancedMotorParametersPage(this);
}

void MachineSettingDialog::selectPage(int index)
{
    if (index < MaxSettingPages) {
    	configPages->setCurrentIndex(index);

    	// Also update title
    	updateTitle(index);
    }
}

void MachineSettingDialog::changePage(QListWidgetItem *current, QListWidgetItem *previous)
{
    if (!current)
        current = previous;

    int index = configItems->row(current);
    selectPage(index);
}

void MachineSettingDialog::updateTitle(int index)
{
	switch (index) {
	case MotorParameterSettings:
		titleLabel->setText(tr("Machine Parameter Settings"));
		break;
	case MaintenanceSettings:
		titleLabel->setText(tr("Machine Maintenance"));
		break;
	case DateTimeSettings:
		titleLabel->setText(tr("Date And Time Settings"));
		break;
	case SystemSettings:
		titleLabel->setText(tr("System Settings"));
		break;
	case TouchScreenCalibration:
		titleLabel->setText(tr("Touch Screen Calibration"));
		break;
	case AdvancedMotorSettings:
		titleLabel->setText(tr("Advanced Machine Settings"));
		break;
	default:
		titleLabel->setText(tr("Machine Settings"));
		break;
	}
}

void MachineSettingDialog::loadStyleSheet(const QString & fileName)
{
	if ( !fileName.isEmpty() ) {
		QFile file(fileName);
		file.open(QFile::ReadOnly);
		QString styleSheet = QLatin1String(file.readAll());

		this->setStyleSheet(styleSheet);

		file.close();
	}
}

void MachineSettingDialog::retranslateUi()
{
	updateTitle(0);

#if 1
	// FIXME: The icon size is small, so the test is not aligned
	iconButton[MotorParameterSettings]->setText(tr("F1"));
	iconButton[MaintenanceSettings   ]->setText(tr("F2"));
	iconButton[DateTimeSettings      ]->setText(tr("F3"));
	iconButton[SystemSettings        ]->setText(tr("F4"));
	iconButton[TouchScreenCalibration]->setText(tr("F5"));
	iconButton[AdvancedMotorSettings ]->setText(tr("F6"));
#endif
}

void MachineSettingDialog::keyPressEvent(QKeyEvent *event)
{
	int index;

//	qDebug() << "MachineSettingDialog Key Pressed: " << hex << event->key();

	switch(event->key()) {
	case SmtKb::Key_F1:
	case SmtKb::Key_F2:
	case SmtKb::Key_F3:
	case SmtKb::Key_F4:
	case SmtKb::Key_F5:
	case SmtKb::Key_F6:
		index = event->key() - SmtKb::Key_F1;
		if (index < MaxSettingPages) {
			configItems->setCurrentRow(index);
		}
		break;
	case SmtKb::Key_Enter:
		tsCalibrateWidget->touchscreenCalibration();
		break;
	case SmtKb::Key_Exit:
		close();
		break;
	default:
		QWidget::keyPressEvent(event);
	}
}
