/*
 * machinesettingdialog.h
 *   Machine Setting Dialog Class Definition
 *
 * Author:    Zeng Xianwei (xianweizeng@gmail.com)
 * ChangeLog: 2010/03/19     Initial version
 *
 */
#ifndef MACHINESETTINGDIALOG_H
#define MACHINESETTINGDIALOG_H

#include <QtGui/QDialog>
#include <QListWidget>
#include <QStackedWidget>
#include <QLabel>

#include "Button.h"
#include "MachineSettingPages.h"

/**
 * Machine setting dialog. Contains several child setting pages, such as:
 *   Machine parameters
 *   Machine maintenance
 *   Date & time
 *   System setting
 */
class MachineSettingDialog : public QDialog
{
    Q_OBJECT

public:
    MachineSettingDialog(QWidget *parent = 0);
    ~MachineSettingDialog();

    void retranslateUi();

public slots:
	void changePage(QListWidgetItem *current, QListWidgetItem *previous);

protected:
    void keyPressEvent(QKeyEvent *event);

private:
	void createIcons();
	QListWidgetItem *createIcon(const QIcon &icon);
	void createPages();
	void updateTitle(int index);
	void selectPage(int index);
	void loadStyleSheet(const QString & fileName);

	QLabel         *titleLabel;
	QStackedWidget *configPages;
	QListWidget    *configItems;
	Button         *closeButton;

	enum {
		MotorParameterSettings = 0,
		MaintenanceSettings,
		DateTimeSettings,
		SystemSettings,
		TouchScreenCalibration,
		AdvancedMotorSettings,    // version
		MaxSettingPages
	};

	QListWidgetItem *iconButton[MaxSettingPages];

	// Setting Pages
	MotorParametersPage *motorParameterSettingWidget;
	MaintenancePage     *maintenceWidget;
	DateTimePage        *dateTimeWidget;
	SystemSettingsPage  *systemSettingWidget;
	AdvancedMotorParametersPage *advancedSettingWidget;
	TouchScreenCalibrateWidget  *tsCalibrateWidget;
};

#endif // MACHINESETTINGDIALOG_H
