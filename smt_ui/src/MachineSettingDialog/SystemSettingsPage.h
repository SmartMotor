/*
 * SystemSettingsPage1.h
 *
 *  Created on: 2010/06/14
 *      Author: kinhi
 */

#ifndef SYSTEMSETTINGSPAGE1_H_
#define SYSTEMSETTINGSPAGE1_H_
#include <QtGui>
#include "TransButton.h"
#include "Button.h"
#include "ConfigSelector.h"

class SystemSettingsPage : public QWidget
{
	Q_OBJECT

public:
	SystemSettingsPage(QWidget *parent = 0);
	virtual ~SystemSettingsPage();

	void retranslateUi();

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
	void udpateSelectedLanguage(int id);
	void updateSelectedFontSize(int size);
	void updateSelectedColor(QColor &color);

	void resetSettings();
	void saveSettings();

	// called when settingButton clicked
	void selectSettingEvent();

	/**
	 * Temp code.
	 * Change GUI's theme.
	 * @themeName: Built in theme name.
	 */
	void changeTheme(const QString &themeName);
	void changeStylesheet(const QString &sheetName);

private:
	void createSettingButtons();
	void craeteSelectorPages();
	TransButton *createTransButton(int id, const char *member, const QString &text = 0);
	TransButton *createColorButton(int id, const char *member, QColor &color);
	TransButton *createFontSizeButton(int id, const char *member, int size);
	TransButton *createLanguageButton(int id, const char *member);

	void updateColorButton(TransButton *button, QColor &color);
	void updateSizeButton(TransButton *button, int size);
	void updateLanguageButton(int languageID);
	void updateCurrentSettings();

	// Update buttons' check states
	void updateButtonsCheckedState();

	int _selectedSetting;
	int _previousSelectedSetting;
	int _previousLanguage;

    enum {
    	Item_Language = 0,
    	Item_Theme,
    	Item_SystemFontSize,
    	Item_PositionFontSize,
    	Item_PositionFontColor,
    	Item_PositionBackgroundColor,
    	Item_DistanceFontSize,
    	Item_DistanceFontColor,
    	Item_DistanceBackgroundColor,
    	Item_ScreenSaverTime,
    	MaxSettingItems,
    };
    QLabel      *settingLabel[MaxSettingItems];
    TransButton *settingButton[MaxSettingItems];

    enum {
    	FontSizeListPageIndex = 0,
    	ColorListPageIndex,
    	LanguageListPageIndex,
    };

    QStackedWidget *selectorPages;
    FontSizeSelector *fontSizeList;
    LanguageSelector *languageList;
    ColorSelector    *colorList;
    QWidget          *fontSizeListWidget;
    QWidget          *languageListWidget;
    QWidget          *colorListWidget;

    QLabel *guideLabel;
    QLabel *helpMessageLabel;

    Button *resetButton;
    Button *saveButton;

    /*
     * TODO:
     *  Temp theme combobox. Should be cleaned up.
     */
    QLabel *themeLabel;
    QComboBox *themeComboBox;
    QLabel *styleSheetLabel;
    QComboBox *styleSheetComboBox;
};

#endif /* SYSTEMSETTINGSPAGE1_H_ */
