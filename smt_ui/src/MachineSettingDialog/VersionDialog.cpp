/*
 * VersionDialog.cpp
 *
 *  Created on: Jun 8, 2010
 *      Author: souken-i
 */

#include <QtGui>
#include "VersionDialog.h"
#include "Version.h"

VersionDialog::VersionDialog(QWidget *parent)
	: RoundRectDialog(parent)
{
	versionLabel = new QLabel;
	versionLabel->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);

	copyrightLabel = new QLabel;
	copyrightLabel->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);

	okButton = new TransButton;
	okButton->setIcon(QIcon("images/enter-48.png"));
	connect(okButton, SIGNAL(clicked()), this, SLOT(close()));

	QHBoxLayout *buttonLayout = new QHBoxLayout;
	buttonLayout->addStretch();
	buttonLayout->addWidget(okButton);

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addWidget(versionLabel);
	layout->addSpacing(10);
	layout->addWidget(copyrightLabel);
	layout->addLayout(buttonLayout);
	setLayout(layout);

	// Set focus to this button so that user can exit by pressing ENTER.
	okButton->setFocus();

	retranslateUi();
	versionLabel->setStyleSheet("background-color: gainsboro; background-image: url(images/logo-64.png); background-position: top right; background-repeat: no-repeat");
}

VersionDialog::~VersionDialog()
{
}

void VersionDialog::retranslateUi()
{
	okButton->setText(tr("OK"));
	versionLabel->setText(tr("<h2>Smart Motor</h2>"
			"<p>Version: " SMT_VERSION "</p>"));
	copyrightLabel->setText(tr("Copyright @ 2010 SmartMotor Inc. All Rights Reserved."));
}

void VersionDialog::mouseMoveEvent(QMouseEvent * /* event */)
{
	// Do nothing, this dialog can't be moved
}
