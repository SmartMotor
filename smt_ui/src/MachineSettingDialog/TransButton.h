/*
 * TransButton.h
 *
 * A Push Button with ID and no focus
 *
 *  Created on: Mar 28, 2010
 *      Author: souken-i
 */

#ifndef TRANSBUTTON_H
#define TRANSBUTTON_H

#include "PushButton.h"


class TransButton : public PushButton
{
	Q_OBJECT

public:
    TransButton(QWidget *parent = 0);
    ~TransButton();

    /**
     * return ID of this button
     */
    int  getID();

    /**
     * Set ID of this button
     */
    void setID(int id);

private:
    /**
     * ID of this button
     *
     * We could also use QSignalMapper to do this
     */
    int _id;
};

#endif // TRANSBUTTON_H
