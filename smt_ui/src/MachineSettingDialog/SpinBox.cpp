/*
 * SpinBox.cpp
 *
 *  Created on: Jul 21, 2010
 *      Author: souken-i
 */

#include <QtGui>
#include "SpinBox.h"
#include "SmtKb.h"

SpinBox::SpinBox(QWidget *parent)
	: QSpinBox(parent)
{
	buddyLabel = 0;
}

SpinBox::~SpinBox() {
	// does nothing
}

void SpinBox::keyPressEvent(QKeyEvent *event)
{
	switch(event->key()) {
	case SmtKb::Key_Left:
		stepDown();
		break;
	case SmtKb::Key_Right:
		stepUp();
		break;
	case SmtKb::Key_PageUp:
		stepBy(10);
		break;
	case SmtKb::Key_PageDown:
		stepBy(-10);
		break;
	default:
		QWidget::keyPressEvent(event); // Handler of QWiget but not QSpinBox
	}
}

void SpinBox::focusInEvent(QFocusEvent *event)
{
	setBuddyLabelHightLight();

	return QSpinBox::focusInEvent(event);
}

void SpinBox::focusOutEvent(QFocusEvent *event)
{
	clearBuddyLabelHightLight();

	return QSpinBox::focusOutEvent(event);
}

void SpinBox::setBuddyLabelHightLight()
{
	if (buddyLabel) {
		QPalette palette;
		palette.setColor(QPalette::WindowText, QColor("red"));
		buddyLabel->setPalette(palette);
	}
}

void SpinBox::clearBuddyLabelHightLight()
{
	if (buddyLabel) {
		QPalette palette;
		// FIXME: use original color, not absolute "black"
		palette.setColor(QPalette::WindowText, QColor("black"));
		buddyLabel->setPalette(palette);
	}
}

void SpinBox::setBuddy(QLabel *buddy)
{
	buddyLabel = buddy;
}
