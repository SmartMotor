/*
 * ConfigSelector.cpp
 *
 *  Created on: Apr 17, 2010
 *      Author: souken-i
 */

#include <QtGui>

#include "ConfigSelector.h"
#include "Global.h"

ColorSelector::ColorSelector(QWidget * parent)
	: QListView(parent)
{
	model = new QStandardItemModel;
	model->setColumnCount(1);

	setModel(model);
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setupColorTable();
}

ColorSelector::~ColorSelector()
{
}

void ColorSelector::selectionChanged (const QItemSelection &selected, const QItemSelection &deselected)
{
	QListView::selectionChanged(selected, deselected);

	QColor color = getColor();
	emit selectionChangedSignal(color);
}

QColor ColorSelector::getColor()
{
    return qVariantValue<QColor>(model->data(currentIndex(), Qt::DecorationRole));
}

void ColorSelector::setupColorTable()
{
    QStringList colorNames = QColor::colorNames();

    for (int i = 0; i < colorNames.size(); i++) {
        QColor color(colorNames[i]);
        model->insertRows(i, 1,  QModelIndex());
        model->setData(model->index(i, 0, QModelIndex()),
                        colorNames[i]);
        model->setData(model->index(i, 0, QModelIndex()),
                        color, Qt::DecorationRole);
        // ReadOnly
        model->item(i, 0)->setEditable(false);
    }
}

/*******************************************************************************/
LanguageSelector::LanguageSelector(QWidget * parent)
	: QListView(parent)
{
	model = new QStandardItemModel;
	model->setColumnCount(1);

	setModel(model);
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	setupLanguageTable();
}

LanguageSelector::~LanguageSelector()
{
}

void LanguageSelector::selectionChanged (const QItemSelection &selected, const QItemSelection &deselected)
{
	QListView::selectionChanged(selected, deselected);

	emit selectionChangedSignal(getSelectedLanguageID());
}

int LanguageSelector::getLanguage(QString *name, QIcon *icon)
{
	*name = qVariantValue<QString>(model->data(currentIndex()));
	*icon = qVariantValue<QIcon>(model->data(currentIndex(), Qt::DecorationRole));

	return currentIndex().row();
}
int LanguageSelector::getSelectedLanguageID()
{
	return currentIndex().row();
}

QString LanguageSelector::getLanguageName(int row)
{
	return qVariantValue<QString>(model->index(row, 0).data());
}

QIcon LanguageSelector::getLanguageIcon(int row)
{
	return qVariantValue<QIcon>(model->index(row, 0).data(Qt::DecorationRole));
}

void LanguageSelector::setupLanguageTable()
{
	QString fileName = System::dataRootDir() + QString("languages.cfg");
	QFile file(fileName);

	if (!file.exists()) {
		// Create a default languages.cfg file
		if (file.open(QFile::WriteOnly | QFile::Text)) {
			QTextStream out(&file);
			out.setCodec("UTF-8");

			out << "# Languages format: Name,icon" << endl;
			out << "English,images/usa.png" << endl;
			out << "Chinese,images/china.png" << endl;

			file.close();
		}
	}

	if (file.open(QFile::ReadOnly | QFile::Text)) {
		QTextStream in(&file);
		in.setCodec("UTF-8");

        model->removeRows(0, model->rowCount(QModelIndex()), QModelIndex());

        int row = 0;
    	while ( !in.atEnd() ) {
    		QString line = in.readLine();
    		if ( !line.isEmpty() && line.at(0) == '#' ) {  /* comment line */
    			continue;
    		}

    		QStringList fields = line.split(',', QString::KeepEmptyParts); /* separated by comma */

    		model->insertRows(row, 1,  QModelIndex());

            model->setData(model->index(row, 0, QModelIndex()),
            		fields.value(0));
            model->setData(model->index(row, 0, QModelIndex()),
            		QIcon(fields.value(1)), Qt::DecorationRole);

            model->item(row, 0)->setEditable(false);

            row++;
    	}

    	file.close();
	}
}

void LanguageSelector::setIndex(int row)
{
	QModelIndex index = model->index(row, 0);
	setCurrentIndex(index);
}


/*******************************************************************************/
FontSizeSelector::FontSizeSelector(QWidget * parent)
	: QListView(parent)
{
	model = new QStandardItemModel;
	model->setColumnCount(1);

	setModel(model);
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	setupFontSizeTable(SYSTEM_FONT_SIZE_MIN, SYSTEM_FONT_SIZE_MAX);
}

FontSizeSelector::~FontSizeSelector()
{
}

void FontSizeSelector::selectionChanged (const QItemSelection &selected, const QItemSelection &deselected)
{
	QListView::selectionChanged(selected, deselected);

	emit selectionChangedSignal(getFontSize());
}

int FontSizeSelector::getFontSize()
{
	return qVariantValue<int>(model->data(currentIndex()));
}

void FontSizeSelector::setupFontSizeTable(int min, int max, int selected_size)
{
	int step = 2;
    int row = 0;
    int selected_row = -1;

    if (!min || !max || (min > max))
    	return;

#if 0
    // need it?
    if ((min == _font_size_min) && (max == _font_size_max))
    	return;
#endif

    _font_size_min = min;
    _font_size_max = max;

    model->removeRows(0, model->rowCount(QModelIndex()), QModelIndex());

    for (int size = _font_size_min; size <= _font_size_max; size += step) {
    	model->insertRows(row, 1,  QModelIndex());
    	model->setData(model->index(row, 0, QModelIndex()), size);
    	model->setData(model->index(row, 0, QModelIndex()),
    			QIcon("images/font-32.png"), Qt::DecorationRole);
    	// ReadOnly
    	model->item(row, 0)->setEditable(false);

    	if (selected_size == size)
    		selected_row = row;

    	row++;
    }

    if (selected_row >= 0) {
#ifdef CONFIG_SELECTOR_DEBUG
    	cout << "Selected row " << selected_row << " Size " << selected_size << endl;
#endif
    	QModelIndex index = model->index(selected_row, 0);
    	setCurrentIndex(index);
    }
}


