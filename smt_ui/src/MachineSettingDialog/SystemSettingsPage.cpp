/*
 * SystemSettingsPage.cpp
 *
 *  Created on: 2010/06/14
 *      Author: kinhi
 */

#include <QWSServer>
#include "SystemSettingsPage.h"
#include "SmtKb.h"
#include "Global.h"
#include "System.h"

//#include "System.h"

/******************************************************************************/
SystemSettingsPage::SystemSettingsPage(QWidget *parent)
	: QWidget(parent)
{
	_selectedSetting = 0;
	_previousSelectedSetting = 0;
	_previousLanguage = gSysConfig->getLanguage();

	guideLabel = new QLabel;
	helpMessageLabel = new QLabel;

	// Note: Selectors MUST be created before buttons
	//       because we call selector to get some info
	craeteSelectorPages();

	createSettingButtons();

	// Signals
	connect(languageList, SIGNAL(selectionChangedSignal(int)), this, SLOT(udpateSelectedLanguage(int)));
	connect(fontSizeList, SIGNAL(selectionChangedSignal(int)), this, SLOT(updateSelectedFontSize(int)));
	connect(colorList, SIGNAL(selectionChangedSignal(QColor &)), this, SLOT(updateSelectedColor(QColor &)));

	resetButton = new Button;
	resetButton->setIcon(QIcon("images/reset.png"));
	resetButton->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	resetButton->setIconSize(QSize(64, 64));
	connect(resetButton, SIGNAL(clicked()), this, SLOT(resetSettings()));
	resetButton->setProperty("custom", true);

	saveButton  = new Button;
	saveButton->setIcon(QIcon("images/save.png"));
	saveButton->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	saveButton->setIconSize(QSize(64, 64));
	connect(saveButton, SIGNAL(clicked()), this, SLOT(saveSettings()));
	saveButton->setProperty("custom", true);

	QGridLayout *topLeftLayout = new QGridLayout;
	for (int i = 0; i < MaxSettingItems; i++) {
		topLeftLayout->addWidget(settingLabel[i],  i, 0);
		topLeftLayout->addWidget(settingButton[i], i, 1);
	}


	/*
	 * TODO:
	 *  Temp theme combobox. Should be cleaned up.
	 */
//	themeLabel = new QLabel(tr("Style"));
//	themeComboBox = new QComboBox;
//	themeComboBox->addItems(QStyleFactory::keys());
//
//	topLeftLayout->addWidget(themeLabel, MaxSettingItems, 0);
//	topLeftLayout->addWidget(themeComboBox, MaxSettingItems, 1);
//
//	connect(themeComboBox, SIGNAL(activated(const QString &)),
//			this, SLOT(changeTheme(const QString &)));

	styleSheetLabel = new QLabel;//
	styleSheetComboBox = new QComboBox;
	styleSheetComboBox->addItem("coffee");
	styleSheetComboBox->addItem("default");

	topLeftLayout->addWidget(styleSheetLabel, MaxSettingItems, 0);
	topLeftLayout->addWidget(styleSheetComboBox, MaxSettingItems, 1);

#if 0
	// FIXME: disable this feature now 2010-08-31
	connect(styleSheetComboBox, SIGNAL(activated(const QString &)),
			this, SLOT(changeStylesheet(const QString &)));
#endif

	QVBoxLayout *topRightLayout = new QVBoxLayout;
	topRightLayout->addWidget(guideLabel);
	topRightLayout->addWidget(selectorPages);
	topRightLayout->addWidget(helpMessageLabel);

	QHBoxLayout *topLayout = new QHBoxLayout;
	topRightLayout->addSpacing(16);
	topLayout->addLayout(topLeftLayout);
	topRightLayout->addSpacing(16);
	topLayout->addLayout(topRightLayout);
	topLayout->setStretchFactor(topLeftLayout, 1);

	QHBoxLayout *bottomLayout = new QHBoxLayout;
	bottomLayout->addStretch(1);
	bottomLayout->addWidget(resetButton);
	bottomLayout->addWidget(saveButton);

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addLayout(topLayout);
	layout->addLayout(bottomLayout);
	setLayout(layout);

	// Show the default item
	_selectedSetting = Item_PositionFontColor;
	selectorPages->setCurrentIndex(ColorListPageIndex);

	updateButtonsCheckedState();

	updateCurrentSettings();

	retranslateUi();
}

SystemSettingsPage::~SystemSettingsPage()
{
}

void SystemSettingsPage::changeTheme(const QString &themeName)
{
	QApplication::setStyle(QStyleFactory::create(themeName));
}

void SystemSettingsPage::changeStylesheet(const QString &sheetName)
{
	QFile file("qss/" + sheetName.toLower() + ".qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());

	qApp->setStyleSheet(styleSheet);
}

void SystemSettingsPage::createSettingButtons()
{
	int fontsize;
	QColor color("red");

	// Labels
	for (int i = 0; i < MaxSettingItems; i++) {
		settingLabel[i] = new QLabel;
	}

	// Buttons
	settingButton[Item_Language] = createLanguageButton(Item_Language, SLOT(selectSettingEvent()));
	settingButton[Item_Theme]    = createLanguageButton(Item_Theme, SLOT(selectSettingEvent()));

	fontsize = gSysConfig->getSysFontSize();
	settingButton[Item_SystemFontSize] = createFontSizeButton(Item_SystemFontSize, SLOT(selectSettingEvent()), fontsize);

	fontsize = gSysConfig->getPositionFontSize();
	settingButton[Item_PositionFontSize] = createFontSizeButton(Item_PositionFontSize, SLOT(selectSettingEvent()), fontsize);

	color = gSysConfig->getPositionFontColor();
	settingButton[Item_PositionFontColor] = createColorButton(Item_PositionFontColor, SLOT(selectSettingEvent()), color);

	color = gSysConfig->getPositionBackgroundColor();
	settingButton[Item_PositionBackgroundColor] = createColorButton(Item_PositionBackgroundColor, SLOT(selectSettingEvent()), color);

	fontsize = gSysConfig->getDistanceFontSize();
	settingButton[Item_DistanceFontSize] = createFontSizeButton(Item_DistanceFontSize, SLOT(selectSettingEvent()), fontsize);

	color = gSysConfig->getDistanceFontColor();
	settingButton[Item_DistanceFontColor] = createColorButton(Item_DistanceFontColor, SLOT(selectSettingEvent()), color);

	color = gSysConfig->getDistanceBackgroundColor();
	settingButton[Item_DistanceBackgroundColor] = createColorButton(Item_DistanceBackgroundColor, SLOT(selectSettingEvent()), color);

	settingButton[Item_ScreenSaverTime] = createLanguageButton(Item_ScreenSaverTime, SLOT(selectSettingEvent()));

	// TODO
	settingButton[Item_ScreenSaverTime]->setDisabled(true);
	settingButton[Item_Theme]->setDisabled(true);

}

void SystemSettingsPage::craeteSelectorPages()
{
	selectorPages = new QStackedWidget;

	// Font Size List Page
	fontSizeList = new FontSizeSelector;
	QVBoxLayout *fontSizeListLayout = new QVBoxLayout;
	fontSizeListLayout->addWidget(fontSizeList);
	fontSizeListWidget = new QWidget;
	fontSizeListWidget->setLayout(fontSizeListLayout);

	// Language List Page
	languageList = new LanguageSelector;
	QVBoxLayout *languageListLayout = new QVBoxLayout;
	languageListLayout->addWidget(languageList);
	languageListLayout->addStretch(1);
	languageListWidget = new QWidget;
	languageListWidget->setLayout(languageListLayout);

	// Color Selector Page
	colorList = new ColorSelector;
	QVBoxLayout *colorListLayout = new QVBoxLayout;
	colorListLayout->addWidget(colorList);
	colorListWidget = new QWidget;
	colorListWidget->setLayout(colorListLayout);

	// Stacked Pages
	selectorPages->addWidget(fontSizeListWidget);
	selectorPages->addWidget(colorListWidget);
	selectorPages->addWidget(languageListWidget);
}

TransButton *SystemSettingsPage::createTransButton(int id, const char *member, const QString &text)
{
	TransButton *button = new TransButton;
	if (text != 0) {
		button->setText(text);
	}
	button->setID(id);
	button->setMaximumWidth(128);
	button->setMinimumWidth(96);
	button->setCheckable(true);

    connect(button, SIGNAL(clicked()), this, member);
    return button;
}

TransButton *SystemSettingsPage::createColorButton(int id, const char *member, QColor &color)
{
	TransButton *button = createTransButton(id, member);
	button->setIconSize(QSize(64,24));
	updateColorButton(button, color);
	return button;
}

TransButton *SystemSettingsPage::createFontSizeButton(int id, const char *member, int size)
{
	TransButton *button = createTransButton(id, member);
	button->setIcon(QIcon("images/font-64.png"));
	updateSizeButton(button, size);

	return button;
}

void SystemSettingsPage::updateButtonsCheckedState()
{
	if (_previousSelectedSetting == _selectedSetting) {
		return;
	}

	if((_selectedSetting >= MaxSettingItems) || (_previousSelectedSetting >= MaxSettingItems)) {
		return;
	}

	settingButton[_previousSelectedSetting]->setChecked(false);
	settingButton[_selectedSetting]->setChecked(true);
}

TransButton *SystemSettingsPage::createLanguageButton(int id, const char *member)
{
	TransButton *button = createTransButton(id, member);

	return button;
}

void SystemSettingsPage::updateColorButton(TransButton *button, QColor &color)
{
	QPixmap pm(64, 24);
	pm.fill(color);
	QIcon icon(pm);

	button->setIcon(icon);
}

void SystemSettingsPage::updateSizeButton(TransButton *button, int size)
{
	button->setText(QString::number(size) + QString(" px"));
}

// Show current language on button
void SystemSettingsPage::updateLanguageButton(int languageID)
{
	QIcon icon = languageList->getLanguageIcon(languageID);
	settingButton[Item_Language]->setIcon(icon);

	QString name = languageList->getLanguageName(languageID);
	settingButton[Item_Language]->setText(name);
}

void SystemSettingsPage::updateCurrentSettings()
{
	updateLanguageButton(gSysConfig->getLanguage());

	// Fone Size
	updateSizeButton(settingButton[Item_SystemFontSize  ], gSysConfig->getSysFontSize());
	updateSizeButton(settingButton[Item_PositionFontSize], gSysConfig->getPositionFontSize());
	updateSizeButton(settingButton[Item_DistanceFontSize], gSysConfig->getDistanceFontSize());

	// Font Color
	updateColorButton(settingButton[Item_PositionFontColor], gSysConfig->getPositionFontColor());
	updateColorButton(settingButton[Item_DistanceFontColor], gSysConfig->getDistanceFontColor());

	// Background color
	updateColorButton(settingButton[Item_PositionBackgroundColor], gSysConfig->getPositionBackgroundColor());
	updateColorButton(settingButton[Item_DistanceBackgroundColor], gSysConfig->getDistanceBackgroundColor());
}

void SystemSettingsPage::selectSettingEvent()
{
	TransButton *clickedButton = qobject_cast<TransButton *>(sender());

	_previousSelectedSetting = _selectedSetting;
	_selectedSetting = clickedButton->getID();

	switch(_selectedSetting) {
	case Item_Language:
		selectorPages->setCurrentIndex(LanguageListPageIndex);
		languageList->setIndex(gSysConfig->getLanguage());
		languageList->setFocus();
		break;
	case Item_Theme:
		// TODO
		break;
	case Item_SystemFontSize:
		selectorPages->setCurrentIndex(FontSizeListPageIndex);
		fontSizeList->setupFontSizeTable(SYSTEM_FONT_SIZE_MIN, SYSTEM_FONT_SIZE_MAX,
				gSysConfig->getSysFontSize());
		fontSizeList->setFocus();
		break;
	case Item_PositionFontSize:
		selectorPages->setCurrentIndex(FontSizeListPageIndex);
		fontSizeList->setupFontSizeTable(POSITION_FONT_SIZE_MIN, POSITION_FONT_SIZE_MAX,
				gSysConfig->getPositionFontSize());
		fontSizeList->setFocus();
		break;
	case Item_DistanceFontSize:
		selectorPages->setCurrentIndex(FontSizeListPageIndex);
		fontSizeList->setupFontSizeTable(DISTANCE_FONT_SIZE_MIN, DISTANCE_FONT_SIZE_MAX,
				gSysConfig->getDistanceFontSize());
		fontSizeList->setFocus();
		break;
	case Item_PositionFontColor:
	case Item_PositionBackgroundColor:
	case Item_DistanceFontColor:
	case Item_DistanceBackgroundColor:
		selectorPages->setCurrentIndex(ColorListPageIndex);
		colorList->setFocus();
		break;
	case Item_ScreenSaverTime:
		// TODO
		break;
	default:
		break;
	}

	updateButtonsCheckedState();
}

void SystemSettingsPage::resetSettings()
{
	// Reload configurations from file
	gSysConfig->load();

	updateCurrentSettings();
}

void SystemSettingsPage::saveSettings()
{
	gSysConfig->save();

	// Update Language
	if (_previousLanguage != gSysConfig->getLanguage())
	{
		_previousLanguage = gSysConfig->getLanguage();
		gSysConfig->loadAppTranslator();
		retranslateUi();
	}
}

// SLOTS
void SystemSettingsPage::udpateSelectedLanguage(int id)
{
	gSysConfig->setLanguage(id);
	updateLanguageButton(id);
}

void SystemSettingsPage::updateSelectedFontSize(int size)
{
	TransButton *button = settingButton[_selectedSetting];
	updateSizeButton(button, size);

	// Update system configuration
	switch (_selectedSetting) {
	case Item_SystemFontSize:
		gSysConfig->setSysFontSize(size);
		break;
	case Item_PositionFontSize:
		gSysConfig->setPositionFontSize(size);
		break;
	case Item_DistanceFontSize:
		gSysConfig->setDistanceFontSize(size);
		break;
	default:
		break;
	}
}

void SystemSettingsPage::updateSelectedColor(QColor &color)
{
	TransButton *button = settingButton[_selectedSetting];
	updateColorButton(button, color);

	switch (_selectedSetting) {
	case Item_PositionFontColor:
		gSysConfig->setPositionFontColor(color);
		break;
	case Item_PositionBackgroundColor:
		gSysConfig->setPositionBackgroundColor(color);
		break;
	case Item_DistanceFontColor:
		gSysConfig->setDistanceFontColor(color);
		break;
	case Item_DistanceBackgroundColor:
		gSysConfig->setDistanceBackgroundColor(color);
		break;
	default:
		break;
	}
}


void SystemSettingsPage::retranslateUi()
{
	settingLabel[Item_Language]->setText(tr("System Language:"));
	settingLabel[Item_Theme   ]->setText(tr("System Theme:"));
	settingLabel[Item_SystemFontSize]->setText(tr("System Font Size:"));
	settingLabel[Item_PositionFontSize]->setText(tr("Position Font Size:"));
	settingLabel[Item_PositionFontColor]->setText(tr("Position Font Color:"));
	settingLabel[Item_PositionBackgroundColor]->setText(tr("Position Background Color:"));
	settingLabel[Item_DistanceFontSize]->setText(tr("Distance Font Size:"));
	settingLabel[Item_DistanceFontColor]->setText(tr("Distance Font Color:"));
	settingLabel[Item_DistanceBackgroundColor]->setText(tr("Distance Background Color:"));
	settingLabel[Item_ScreenSaverTime]->setText(tr("Screensaver Time:"));
	styleSheetLabel->setText(tr("Theme"));

	guideLabel->setText(tr("Please Select:"));
	helpMessageLabel->setText(tr("<h3><font color=red>Operation Guide:</font></h3><p><font color=blue>Press button and then select value from the list.</font></p><p><font color=blue>To reset settings, press RESET button.</font></p><p><font color=blue>To save settings, press SAVE button.</font></p>"));
}

void SystemSettingsPage::keyPressEvent(QKeyEvent *event)
{
	switch(event->key()) {
	case SmtKb::Key_Modify:
		saveSettings();
		break;
	case SmtKb::Key_Insert:
		resetSettings();
		break;
	default:
		QWidget::keyPressEvent(event);
	}
}
