/*
 * SpinBox.h
 *
 *  Customized QSpinBox:
 *    - StepUp/StepDown by left/right arrows
 *    - up/down arrows are ignored and filtered because its parent
 *      widget may use it these keys to change focus
 *
 *  Created on: Jul 21, 2010
 *      Author: souken-i
 */

#ifndef SPINBOX_H_
#define SPINBOX_H_

#include <QWidget>
#include <QSpinBox>
#include <QLabel>

class SpinBox : public QSpinBox
{
public:
	SpinBox(QWidget *parent = 0);
	virtual ~SpinBox();

	void setBuddy(QLabel *buddy);

protected:
    void keyPressEvent(QKeyEvent *event);
    void focusInEvent(QFocusEvent *event);
    void focusOutEvent(QFocusEvent *event);

private:
    QLabel *buddyLabel;

    void setBuddyLabelHightLight();
    void clearBuddyLabelHightLight();
};

#endif /* SPINBOX_H_ */
