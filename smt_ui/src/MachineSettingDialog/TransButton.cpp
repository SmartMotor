#include "TransButton.h"

TransButton::TransButton(QWidget *parent)
    : PushButton(parent)
{
	// Disable focus on this button
	setFocusPolicy(Qt::NoFocus);
	_id = 0;
}

TransButton::~TransButton()
{

}

int  TransButton::getID()
{
	return _id;
}

void TransButton::setID(int id)
{
	_id = id;
}
