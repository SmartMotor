/*
 * MachineSettingPages.cpp
 *
 *  Created on: Apr 15, 2010
 *      Author: souken-i
 */


/*
 * We have several setting pages:
 *   Motor Parameters Setting
 *   Maintenance: picture + log
 *   Date and Time setting
 *   font color and size
 *   System Setting: theme, language, screen saver
 */
#include <QtGui>
#include <QDateTime>
#include <QMessageBox>
#include <QTimer>
#include <QWSServer>
#include <QApplication>

#include <iostream>
#include <sys/time.h>  // For settimeofday()

#include "MachineSettingPages.h"
#include "Global.h"
#include "MotorLogView.h"
#include "wallclock.h"
#include "Calibration.h"
#include "VersionDialog.h"

/******************************************************************************/
/* Motor Parameters Setting */
MotorParametersPage::MotorParametersPage(QWidget *parent)
	: QWidget(parent)
{
	frontLimitLabel = new QLabel;
	frontLimitEdit  = new SpinBox;
	frontLimitEdit->setRange(0, MData::toShow(gMotor->getMiddleLimit()));
	frontLimitEdit->setBuddy(frontLimitLabel);

	midLimitLabel = new QLabel;
	midLimitEdit  = new SpinBox;
	midLimitEdit->setRange(MData::toShow(gMotor->getFrontLimit()),
						   MData::toShow(gMotor->getMaxLimit()));
	midLimitEdit->setBuddy(midLimitLabel);

	backLimitLabel = new QLabel;
	backLimitEdit  = new SpinBox;
	backLimitEdit->setRange(MData::toShow(gMotor->getMiddleLimit()),
							MData::toShow(gMotor->getMaxLimit()));
	backLimitEdit->setBuddy(backLimitLabel);

	limitsCheckBox = new QCheckBox;
	limitsCheckBox->setChecked(gMotor->isLimitEnabled());
	connect(limitsCheckBox, SIGNAL(stateChanged(int)), this, SLOT(setLimitState(int)));
	QFont font = this->font();
	font.setPointSize(20);
	font.setBold(true);
	limitsCheckBox->setFont(font);
	QPalette palette;
	palette.setColor(QPalette::WindowText, QColor("blue"));
	limitsCheckBox->setPalette(palette);
	limitsCheckBox->setAutoFillBackground(true);

	motorSpeedLabel = new QLabel;
	motorSpeedEdit  = new SpinBox;
	motorSpeedEdit->setRange(gMotor->getMinPusherSpeed(), gMotor->getMaxPusherSpeed());
	motorSpeedEdit->setBuddy(motorSpeedLabel);

	runSpeedLabel  = new QLabel;
	runSpeedEdit   = new SpinBox;
	runSpeedEdit->setRange(gMotor->getMinRunSpeed(), gMotor->getMaxRunSpeed());
	runSpeedEdit->setBuddy(runSpeedLabel);

	airDistanceLabel = new QLabel;
	airDistanceEdit  = new SpinBox;
	airDistanceEdit->setRange(0, 20);
	airDistanceEdit->setBuddy(airDistanceLabel);

	stepSizeLabel = new QLabel;
	stepSizeEdit  = new SpinBox;
	stepSizeEdit->setRange(1, 20);
	stepSizeEdit->setBuddy(stepSizeLabel);

	// Show current values
	resetParameters();

	helpMessageLabel = new QLabel;
	font.setPointSize(16);
	font.setBold(false);
	helpMessageLabel->setFont(font);

	resetButton = new Button;
	resetButton->setIcon(QIcon("images/reset.png"));
	resetButton->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	resetButton->setIconSize(QSize(64, 64));
	connect(resetButton, SIGNAL(clicked()), this, SLOT(resetParameters()));
	resetButton->setProperty("custom", true);

	saveButton  = new Button;
	saveButton->setIcon(QIcon("images/save.png"));
	saveButton->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	saveButton->setIconSize(QSize(64, 64));
	connect(saveButton, SIGNAL(clicked()), this, SLOT(saveParameters()));
	saveButton->setProperty("custom", true);

	QGridLayout *configLayout = new QGridLayout;
	configLayout->addWidget(limitsCheckBox,  0, 0, 1, 2);
	configLayout->addWidget(frontLimitLabel,  1, 0);
	configLayout->addWidget(frontLimitEdit,   1, 1);
	configLayout->addWidget(midLimitLabel,    2, 0);
	configLayout->addWidget(midLimitEdit,     2, 1);
	configLayout->addWidget(backLimitLabel,   3, 0);
	configLayout->addWidget(backLimitEdit,    3, 1);
	configLayout->addWidget(motorSpeedLabel,  4, 0);
	configLayout->addWidget(motorSpeedEdit,   4, 1);
	configLayout->addWidget(runSpeedLabel,    5, 0);
	configLayout->addWidget(runSpeedEdit,     5, 1);
	configLayout->addWidget(airDistanceLabel, 6, 0);
	configLayout->addWidget(airDistanceEdit,  6, 1);
	configLayout->addWidget(stepSizeLabel,    7, 0);
	configLayout->addWidget(stepSizeEdit,     7, 1);

	// Unit Labels
	palette.setColor(QPalette::WindowText, QColor("blue"));
	QLabel *unitLabels[7];
	for (int i = 0; i < 7; i++) {
		unitLabels[i] = new QLabel(tr("[mm]"));
		unitLabels[i]->setAlignment(Qt::AlignLeft);
		unitLabels[i]->setPalette(palette);
		configLayout->addWidget(unitLabels[i], i+1, 2);
	}
	unitLabels[3]->setText(tr("[Meter/Minute]"));
	unitLabels[4]->setText(tr("[RPM]"));

	QVBoxLayout *topLayout = new QVBoxLayout;
	topLayout->addLayout(configLayout);
	topLayout->addWidget(helpMessageLabel);

	QHBoxLayout *bottomLayout = new QHBoxLayout;
	bottomLayout->addStretch(1);
	bottomLayout->addWidget(resetButton);
	bottomLayout->addWidget(saveButton);

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addSpacing(32);
	layout->addLayout(topLayout);
	layout->addStretch(1);
	layout->addLayout(bottomLayout);
	setLayout(layout);

	/*
	 * FIXME
	 *
	 * I use the focusChanged() signal from qApp here, because I can't find
	 * a better way to update the help message according to current focus.
	 *
	 * The disadvantage to use focusChanged() is, even when this widget is hidden,
	 * it will also handle this signal, this wastes time.
	 */
	connect(qApp, SIGNAL(focusChanged(QWidget *, QWidget *)), this, SLOT(focusChangedEvent(QWidget *, QWidget *)));

	retranslateUi();
}

MotorParametersPage::~MotorParametersPage()
{
}

void MotorParametersPage::resetParameters()
{
	frontLimitEdit->setValue(MData::toShow(gMotor->getFrontLimit()));
	midLimitEdit->setValue  (MData::toShow(gMotor->getMiddleLimit()));
	backLimitEdit->setValue (MData::toShow(gMotor->getBackLimit()));
	motorSpeedEdit->setValue(gMotor->getPusherSpeed());
	runSpeedEdit->setValue(gMotor->getRunSpeed());
	airDistanceEdit->setValue(gMotor->getAirStopDistance());
	stepSizeEdit->setValue(gMotor->getStepSize());
}

void MotorParametersPage::saveParameters()
{
	gMotor->setFrontLimit(MData::toInternal(frontLimitEdit->value()));
	gMotor->setMiddleLimit(MData::toInternal(midLimitEdit->value()));
	gMotor->setBackLimit(MData::toInternal(backLimitEdit->value()));
	gMotor->setPusherSpeed(motorSpeedEdit->value());
	gMotor->setRunSpeed(runSpeedEdit->value());
	gMotor->setAirStopDistance(airDistanceEdit->value());
	gMotor->setStepSize(stepSizeEdit->value());

	gMotor->save();
}

void MotorParametersPage::setLimitState(int state)
{
	if (state == Qt::Unchecked) {
		gMotor->enableLimit(false);

		// Also disable limits setting
		frontLimitEdit->setDisabled(true);
		midLimitEdit->setDisabled(true);
		backLimitEdit->setDisabled(true);
	} else {
		gMotor->enableLimit(true);

		frontLimitEdit->setEnabled(true);
		midLimitEdit->setEnabled(true);
		backLimitEdit->setEnabled(true);
	}

	updateHelpMessage();
}

void MotorParametersPage::focusChangedEvent(QWidget * /* pre */, QWidget * /* cur */)
{
	if (!isHidden()) {
		updateHelpMessage();
	}
}

void MotorParametersPage::updateHelpMessage()
{
	QString title = QString(tr("<h2><font color=red>Help: </font></h2>"));

	QString msg1;
	if (limitsCheckBox->isChecked()) {
		msg1 = QString(tr("<p>Clear the \"Enable Limits\" box to disable limits.</p>"));
	} else {
		msg1 = QString(tr("<p>Check the \"Enable Limits\" box to enable limits.</p>"));
	}

	QString msg2 = QString(tr("<p>Font Limit Range: 0 ~ %1 mm</p>")).arg(QString::number(MData::toShow(gMotor->getMiddleLimit())));
	QString msg3 = QString(tr("<p>Middle Limit Range: %1 ~ %2 mm</p>")).arg(QString::number(MData::toShow(gMotor->getFrontLimit()))).arg(QString::number(MData::toShow(gMotor->getBackLimit())));
	QString msg4 = QString(tr("<p>Back Limit Range: %1 ~ %2 mm</p>")).arg(QString::number(MData::toShow(gMotor->getMiddleLimit()))).arg(QString::number(MData::toShow(gMotor->getMaxLimit())));
	QString msg5 = QString(tr("<p>Pusher Speed Range: %1 ~ %2 mm</p>")).arg(QString::number(gMotor->getMinPusherSpeed())).arg(QString::number(gMotor->getMaxPusherSpeed()));
	QString msg6 = QString(tr("<p>Jog Speed Range: %1 ~ %2 RPM</p>")).arg(QString::number(gMotor->getMinRunSpeed())).arg(QString::number(gMotor->getMinRunSpeed()));
	QString msg7 = QString(tr("<p>Air Stop Distance Range: 0 ~ 20 mm</p>"));
	QString msg8 = QString(tr("<p>Step Size Range: 1 ~ 20 mm</p>"));

	if (limitsCheckBox->hasFocus() || frontLimitEdit->hasFocus() || \
		midLimitEdit->hasFocus() || backLimitEdit->hasFocus()) {
		helpMessageLabel->setText(title + msg1 + msg2 + msg3 + msg4);
	} else {
		helpMessageLabel->setText(title + msg5 + msg6 + msg7 + msg8);
	}
}

void MotorParametersPage::retranslateUi()
{
	//resetButton->setText(tr("Reset"));
	//saveButton->setText(tr("Save"));

	limitsCheckBox->setText(tr("Enable Limits"));

	frontLimitLabel->setText(tr("Front Limit:"));
	midLimitLabel->setText(tr("Middle Limit:"));
	backLimitLabel->setText(tr("Back Limit:"));
	motorSpeedLabel->setText(tr("Pusher Speed:"));
	runSpeedLabel->setText(tr("Jog Speed:"));
	airDistanceLabel->setText(tr("Air Stop Distance:"));
	stepSizeLabel->setText(tr("Step Size:"));

	// Update help message according to current focus
	updateHelpMessage();
}

void MotorParametersPage::showEvent(QShowEvent *event)
{
	if (limitsCheckBox->isChecked()) {
		frontLimitEdit->setFocus();
	} else {
		motorSpeedEdit->setFocus();
	}

	return QWidget::showEvent(event);
}

void MotorParametersPage::keyPressEvent(QKeyEvent *event)
{
//	cout << "Key " << hex << event->key() << " Pressed" << endl;

	switch(event->key()) {
	case SmtKb::Key_Modify:
		saveParameters();
		break;
	case SmtKb::Key_Insert:
		resetParameters();
		break;
	case SmtKb::Key_Up:
		// Limit to these widgets
		if (!frontLimitEdit->hasFocus())
			focusNextPrevChild(false);
		break;
	case SmtKb::Key_Down:
		if (!stepSizeEdit->hasFocus())
			focusNextPrevChild(true);
		break;
	default:
		QWidget::keyPressEvent(event);
	}
}

/******************************************************************************/
static const char *maintenanceIconFile[MaintenancePage::MaintenanceIconNumber] = {
	"images/motor-1-128.png",
	"images/motor-2-128.png",
	"images/motor-3-128.png",
	"images/motor-4-128.png",
};

MaintenancePage::MaintenancePage(QWidget *parent)
	: QWidget(parent)
{
	_currentIcon = 0;

	logLabel = new QLabel;
	QFont font = this->font();
	font.setPointSize(gSysConfig->getSysFontSize() + 4);
	font.setBold(true);
	logLabel->setFont(font);

	QPalette palette;
	palette.setColor(QPalette::Window, QColor("yellow"));
	palette.setColor(QPalette::WindowText, QColor("blue"));
	logLabel->setPalette(palette);
	logLabel->setAutoFillBackground(true);

	logView = new MotorLogView;
	QString logFile = System::currentUserDir() + QString("motor.log");
	logView->openLogFile(logFile);

	maintenanceIcon = new QLabel;
	maintenanceIcon->setAlignment(Qt::AlignCenter);

	previousButton = createButton(QIcon("images/pageup-64.png"), SLOT(previousPicture()));
	nextButton = createButton(QIcon("images/pagedown-64.png"), SLOT(nextPicture()));

	QVBoxLayout *buttonLayout = new QVBoxLayout;
	buttonLayout->addWidget(previousButton);
	buttonLayout->addWidget(nextButton);

	QHBoxLayout *topLayout = new QHBoxLayout;
	topLayout->addWidget(maintenanceIcon);
	topLayout->addLayout(buttonLayout);
	topLayout->setStretchFactor(maintenanceIcon, 1);

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addSpacing(16);
	layout->addLayout(topLayout);
	layout->addWidget(logLabel);
	layout->addWidget(logView);

	setLayout(layout);

	showIcon(_currentIcon);
	retranslateUi();
}

MaintenancePage::~MaintenancePage()
{
}

Button *MaintenancePage::createButton(const QIcon &icon, const char *member)
{
	Button *button = new Button;

	button->setIcon(icon);
	button->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
	button->setIconSize(QSize(32, 32));
    connect(button, SIGNAL(clicked()), this, member);
    button->setProperty("custom", true);
    return button;
}

void MaintenancePage::showIcon(int index)
{
	if (index >= 0 && index < MaintenanceIconNumber) {
		QPixmap pm = QPixmap(maintenanceIconFile[index]);
		maintenanceIcon->setPixmap(pm);
	}

	if (index <= 0) {
		previousButton->setDisabled(true);
	} else {
		previousButton->setDisabled(false);
	}

	if (index >= (MaintenanceIconNumber - 1)) {
		nextButton->setDisabled(true);
	} else {
		nextButton->setDisabled(false);
	}
}

void MaintenancePage::nextPicture()
{
	if (_currentIcon < MaintenanceIconNumber - 1) {
		_currentIcon++;
		showIcon(_currentIcon);
	}
}

void MaintenancePage::previousPicture()
{
	if (_currentIcon > 0) {
		_currentIcon--;
		showIcon(_currentIcon);
	}
}

void MaintenancePage::retranslateUi()
{
	logLabel->setText(tr("Motor Alarm/Warning/Error Log:"));
}

void MaintenancePage::showEvent(QShowEvent *event)
{
	logView->setFocus();

	return QWidget::showEvent(event);
}

/******************************************************************************/
AdvancedMotorParametersPage::AdvancedMotorParametersPage(QWidget *parent)
	: QWidget(parent)
{
	versionButton = new TransButton;
	versionButton->setIcon(QIcon("images/save.png"));
	versionButton->setIconSize(QSize(64, 32));
	connect(versionButton, SIGNAL(clicked()), this, SLOT(showVersion()));

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addWidget(versionButton);

	setLayout(layout);

	retranslateUi();
}

AdvancedMotorParametersPage::~AdvancedMotorParametersPage()
{

}

void AdvancedMotorParametersPage::retranslateUi()
{
	versionButton->setText(tr("About"));
}

void AdvancedMotorParametersPage::showVersion()
{
	VersionDialog dlg;
	dlg.exec();
}

void AdvancedMotorParametersPage::showEvent(QShowEvent *event)
{
	versionButton->setFocus();

	return QWidget::showEvent(event);
}

/******************************************************************************/
/* Date and Time Setting Page */
DateTimePage::DateTimePage(QWidget *parent)
	: QWidget(parent)
{
	sysDateCalendar = new QCalendarWidget;
	sysDateCalendar->setMinimumDate(QDate(2010, 1, 1));
	sysDateCalendar->setMaximumDate(QDate(2050, 1, 1));
	sysDateCalendar->setGridVisible(true);
	sysDateCalendar->setVerticalHeaderFormat(QCalendarWidget::NoVerticalHeader);

	/* System Time */
	WallClock *clock = new WallClock;
	clock->resize(128, 128);

	currentSysTimeLabel = new QLabel;
	currentSysTime = new QLabel;
	currentSysTime->setAlignment(Qt::AlignCenter);
	updateTime();

	connect(&timer, SIGNAL(timeout()), this, SLOT(updateTime()));
	timer.setInterval(1000);
	timer.setSingleShot(false);

	QTime now = QTime::currentTime();
	sysTimeHourLabel   = new QLabel;
	sysTimeMinuteLabel = new QLabel;
	sysTimeSecondLabel = new QLabel;

	sysTimeHourEdit    = new SpinBox;
	sysTimeHourEdit->setRange(0, 23);
	sysTimeHourEdit->setValue(now.hour());
	sysTimeHourEdit->setBuddy(sysTimeHourLabel);

	sysTimeMinuteEdit  = new SpinBox;
	sysTimeMinuteEdit->setRange(0, 59);
	sysTimeMinuteEdit->setValue(now.minute());
	sysTimeMinuteEdit->setBuddy(sysTimeMinuteLabel);

	sysTimeSecondEdit  = new SpinBox;
	sysTimeSecondEdit->setRange(0, 59);
	sysTimeSecondEdit->setValue(now.second());
	sysTimeSecondEdit->setBuddy(sysTimeSecondLabel);

	saveButton  = new Button;
	saveButton->setIcon(QIcon("images/save.png"));
	saveButton->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	saveButton->setIconSize(QSize(64, 64));
	connect(saveButton, SIGNAL(clicked()), this, SLOT(saveDateTime()));
	saveButton->setProperty("custom", true);

	QGridLayout *timeLayout = new QGridLayout;
	timeLayout->addWidget(currentSysTimeLabel, 0, 0);
	timeLayout->addWidget(currentSysTime, 0, 1);
	timeLayout->addWidget(sysTimeHourLabel, 1, 0);
	timeLayout->addWidget(sysTimeHourEdit, 1, 1);
	timeLayout->addWidget(sysTimeMinuteLabel, 2, 0);
	timeLayout->addWidget(sysTimeMinuteEdit, 2, 1);
	timeLayout->addWidget(sysTimeSecondLabel, 3, 0);
	timeLayout->addWidget(sysTimeSecondEdit, 3, 1);

	QHBoxLayout *clockLayout = new QHBoxLayout;
	clockLayout->addWidget(clock);
	clockLayout->addLayout(timeLayout);

	QVBoxLayout *topLayout = new QVBoxLayout;
	topLayout->addWidget(sysDateCalendar);
	topLayout->addSpacing(10);
	topLayout->addLayout(clockLayout);

	QHBoxLayout *buttonLayout = new QHBoxLayout;
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(saveButton);

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addSpacing(32);
	layout->addLayout(topLayout);
//	layout->addStretch(1);
	layout->addSpacing(32);
	layout->addLayout(buttonLayout);
	setLayout(layout);

	timer.start();

	retranslateUi();
}

DateTimePage::~DateTimePage()
{
	timer.stop();
}

void DateTimePage::saveDateTime()
{
	struct tm *tm;
	struct timeval tv;

	tv.tv_sec = time(0);
	tm = localtime(&tv.tv_sec);

	QDate date = sysDateCalendar->selectedDate();

	tm->tm_sec  = sysTimeSecondEdit->value();
	tm->tm_min  = sysTimeMinuteEdit->value();
	tm->tm_hour = sysTimeHourEdit->value();
	tm->tm_mday = date.day();
	tm->tm_mon  = date.month() - 1;
	tm->tm_year = date.year() - 1900;
	tm->tm_wday = date.dayOfWeek();
	tm->tm_yday = date.dayOfYear();

	tv.tv_sec = mktime(tm);
	tv.tv_usec = 0;

#if 1
	int ret = settimeofday(&tv, NULL);
	if (ret < 0) {
		qDebug() << "settimeofday() failed: " << ret << endl;
	}

	// update to RTC
	::system("/devel/sbin/hwclock --systohc");
#endif

}

void DateTimePage::updateTime()
{
	QTime time = QTime::currentTime();
	currentSysTime->setText(time.toString("hh:mm:ss"));
}

void DateTimePage::retranslateUi()
{
	currentSysTimeLabel->setText(tr("Current Time:"));
	sysTimeHourLabel->setText(tr("Hour:"));
	sysTimeMinuteLabel->setText(tr("Minute:"));
	sysTimeSecondLabel->setText(tr("Second:"));
}

void DateTimePage::showEvent(QShowEvent *event)
{
	sysTimeHourEdit->setFocus();

	return QWidget::showEvent(event);
}

void DateTimePage::keyPressEvent(QKeyEvent *event)
{
	switch(event->key()) {
	case SmtKb::Key_Modify:
		saveDateTime();
		break;
	case SmtKb::Key_Up:
		// Limit to these widgets
		if (!sysTimeHourEdit->hasFocus() && !sysDateCalendar->hasFocus())
			focusNextPrevChild(false);
		break;
	case SmtKb::Key_Down:
		if (!sysTimeSecondEdit->hasFocus()  && !sysDateCalendar->hasFocus())
			focusNextPrevChild(true);
		break;
	default:
		QWidget::keyPressEvent(event);
	}
}

/******************************************************************************/
TouchScreenCalibrateWidget::TouchScreenCalibrateWidget(QWidget *parent)
	: QWidget(parent)
{
	guideLabel = new QLabel;
	guideLabel->setAlignment(Qt::AlignCenter);

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addStretch(1);
	layout->addWidget(guideLabel);
	layout->addStretch(1);

	setLayout(layout);

	retranslateUi();
}

TouchScreenCalibrateWidget::~TouchScreenCalibrateWidget()
{
}

void TouchScreenCalibrateWidget::retranslateUi()
{
	guideLabel->setText(tr("Press Enter key or touch the center place."));
}

void TouchScreenCalibrateWidget::touchscreenCalibration()
{
	if ( QWSServer::mouseHandler() ) {
		Calibration cal;
		cal.exec();
	} else {
		QMessageBox message;
		message.setText(tr("<p>No touchscreen device found.</p>"));
		message.exec();
	}
}

void TouchScreenCalibrateWidget::mousePressEvent(QMouseEvent * /* event */)
{
	touchscreenCalibration();
}
