#include <QtGui>
#include "RoundRectDialog.h"
#include "Global.h"

RoundRectDialog::RoundRectDialog(QWidget *parent, float r)
    : QDialog(parent, Qt::FramelessWindowHint)
{
	_radiusPercent = r;
	_movable = false; // Not movable by default
}

RoundRectDialog::~RoundRectDialog()
{

}

void RoundRectDialog::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        dragPosition = event->globalPos() - frameGeometry().topLeft();
        event->accept();
    }
}

void RoundRectDialog::mouseMoveEvent(QMouseEvent *event)
{
	if (!_movable)
		return;

    if (event->buttons() & Qt::LeftButton) {
        move(event->globalPos() - dragPosition);
        event->accept();
    }
}

void RoundRectDialog::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setPen(QPen(Qt::darkGray, 2));

	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.drawRoundRect(0, 0, width(), height(), height() * _radiusPercent, width() * _radiusPercent);

	QDialog::paintEvent(event);
}

void RoundRectDialog::resizeEvent(QResizeEvent * /* event */)
{
	QBitmap bitmap(width(), height());
	bitmap.clear();
	QPainter painter(&bitmap);
	painter.setRenderHint(QPainter::Antialiasing, false);
	painter.setPen(QPen(Qt::black, 1));
	painter.setBrush(Qt::black);
	painter.drawRoundRect(0, 0, width(), height(), height() * _radiusPercent, width() * _radiusPercent);
	setMask(bitmap);
}

void RoundRectDialog::setMovable(bool movable)
{
	_movable = movable;
}
