/*
 * Bubtton.cpp
 *
 *  Created on: Mar 21, 2010
 *      Author: Zeng Xianwei
 */
#include <QtGui> 

#include "Button.h"

Button::Button(QWidget *parent)
    : ToolButton(parent)
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
}

Button::Button(const QString &text, QWidget *parent)
    : ToolButton(parent)
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    setText(text);
}

QSize Button::sizeHint() const
{
    QSize size = ToolButton::sizeHint();
    size.rheight() += 2;
    size.rwidth() = qMax(size.width(), size.height());
    return size;
}
