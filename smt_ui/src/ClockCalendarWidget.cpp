/*
 * ClockCalendarWidget.cpp
 *
 *  Created on: Jun 7, 2010
 *      Author: souken-i
 */

#include <QtGui>
#include "ClockCalendarWidget.h"
#include "Button.h"
#include "Debug.h"

ClockCalendarWidget::ClockCalendarWidget(QWidget *parent)
	: RoundRectWidget(parent)
{
	calendarWidget = new QCalendarWidget;
	calendarWidget->setMinimumDate(QDate(2010, 1, 1));
	calendarWidget->setMaximumDate(QDate(2050, 1, 1));
	calendarWidget->setGridVisible(true);
	calendarWidget->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
	calendarWidget->setVerticalHeaderFormat(QCalendarWidget::NoVerticalHeader);

	clock = new WallClock;
	clock->resize(128, 128);
	clock->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

	Button *closeButton = new Button;
	closeButton->setIcon(QIcon("images/close.png"));
	closeButton->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	closeButton->setIconSize(QSize(24, 24));
	connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));

	QHBoxLayout *topLayout = new QHBoxLayout;
	topLayout->addStretch();
	topLayout->addWidget(closeButton);

	QVBoxLayout *rightLayout = new QVBoxLayout;
	rightLayout->addLayout(topLayout);
	rightLayout->addWidget(clock);

	QHBoxLayout *layout = new QHBoxLayout;
	layout->addWidget(calendarWidget);
	layout->addLayout(rightLayout);

	setLayout(layout);
}

ClockCalendarWidget::~ClockCalendarWidget()
{
	// Do nothing
}

void ClockCalendarWidget::showEvent(QShowEvent * /* event */)
{
	calendarWidget->setSelectedDate(QDate::currentDate());
}

