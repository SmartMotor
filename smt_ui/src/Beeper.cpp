/*
 * Beeper.cpp
 *
 *  Created on: Sep 12, 2010
 *      Author: souken-i
 */

#include <QtGui>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "Beeper.h"

Beeper::Beeper()
{
	beeper_fd = open("/proc/beeper", O_WRONLY);
	if (beeper_fd < 0) {
		qDebug() << "Open /proc/beeper failed: " << beeper_fd;
	}
}

Beeper::~Beeper()
{
	close(beeper_fd);
}

void Beeper::setBeeperOn(void)
{
	if (beeper_fd < 0)
		return;

	ssize_t ret = write(beeper_fd, "1", 1);
	if (ret < 0) {
		qDebug() << "Write /proc/beeper failed";
	}
}

void Beeper::setBeeperOff(void)
{
	if (beeper_fd < 0)
		return;

	ssize_t ret = write(beeper_fd, "0", 1);
	if (ret < 0) {
		qDebug() << "Write /proc/beeper failed";
	}
}
