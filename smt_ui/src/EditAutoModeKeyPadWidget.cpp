/*
 * EditAutoModeKeyPadWidget.cpp
 *
 *  Created on: Jun 19, 2010
 *      Author: souken-i
 */
#include <QtGui>
#include "EditAutoModeKeyPadWidget.h"
#include "FlagEditor.h"
#include "Global.h"

//#define EDITAUTOMODEKEYPADWIDGET_DEBUG

QString EditAutoModeKeyPadWidget::iconFiles[]  =
{
	"images/delete-one.png",  // Delete One Cut
	"images/delete-flag.png",  // Delete Flag
	"images/insert.png",     // Insert
	"images/modify.png",     // Modify

	"images/flag-turn-left.png",
	"images/flag-turn-right.png",
	"images/flag-turn-around.png",
	"images/delete-all.png",  // Delete All

	"images/flag-press.png",
	"images/flag-pusher.png",
	"images/flag-air.png",
	"images/flag-cut.png",

	"images/add.png",
	"images/sub.png",
//	"images/multiply.png",   // Multiply
//	"images/divide.png",     // Divide
	"images/adjust-add.png",  // ++
	"images/adjust-sub.png",  // --

	"images/close.png",
	"images/zoom-in.png",
	"images/zoom-out.png",


};

int EditAutoModeKeyPadWidget::keyPadMappings[EditAutoModeKeyPadWidget::KP_ButtonsNR] =
{
	SmtKb::KP_DeleteOneCut,
	SmtKb::KP_DeleteFlags,
	SmtKb::KP_Insert,
	SmtKb::KP_Modify,

	SmtKb::KP_ToggleTurnLeftFlag,
	SmtKb::KP_ToggleTurnRightFlag,
	SmtKb::KP_ToggleTurnAroundFlag,
	SmtKb::KP_DeleteAll,

	SmtKb::KP_TogglePressFlag,
	SmtKb::KP_TogglePusherFlag,
	SmtKb::KP_ToggleAirFlag,
	SmtKb::KP_ToggleAutoCutFlag,

	SmtKb::KP_RelativeAdd,
	SmtKb::KP_RelativeSub,
	SmtKb::KP_AdjustAddAll,
	SmtKb::KP_AdjustSubAll,

	SmtKb::KP_Hide,
	SmtKb::KP_ZoomIn,
	SmtKb::KP_ZoomOut,
};

EditAutoModeKeyPadWidget::EditAutoModeKeyPadWidget(QWidget *parent)
	: QWidget(parent)
{
	int i;

	_keyPadType = KP_Type_Edit;

	signalMapper = new QSignalMapper;

	QGridLayout *buttonsLayout = new QGridLayout;
	for (i = 0; i < KP_ButtonsNR; i++) {
		keyPadButton[i] = createButton(i, QIcon(iconFiles[i]));

		if (i < KP_MatrixButtonsNR) {
			// Not close button and zoom buttons
			buttonsLayout->addWidget(keyPadButton[i], i / KP_Columns, i % KP_Columns);
		}
	}
    connect(signalMapper, SIGNAL(mapped(int)), this, SIGNAL(clicked(int)));

    // TODO: enable this to hide key pad
//	connect(keyPadButton[Button_Index_Close], SIGNAL(clicked()), this, SLOT(hideButtons()));

//	setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	// Layout
	QHBoxLayout *zoomLayout = new QHBoxLayout;
	zoomLayout->addWidget(keyPadButton[Button_Index_ZoomIn]);
	zoomLayout->addWidget(keyPadButton[Button_Index_ZoomOut]);
	zoomLayout->addStretch(1);
	zoomLayout->addWidget(keyPadButton[Button_Index_Close]);

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addLayout(zoomLayout);
	layout->addStretch(1);
	layout->addLayout(buttonsLayout);
	setLayout(layout);

	updateKeyPadType(_keyPadType);
}

EditAutoModeKeyPadWidget::~EditAutoModeKeyPadWidget() {
	// TODO Auto-generated destructor stub
}

void EditAutoModeKeyPadWidget::setKeyPadType(int type)
{
	_keyPadType = type;

	updateKeyPadType(_keyPadType);
}

void EditAutoModeKeyPadWidget::updateKeyPadType(int type)
{
	int i;
#if 0
	for (i = Button_Index_Mutiply; i < KP_MatrixButtonsNR; i++) {
		if (type == KP_Type_Edit) {
			keyPadButton[i]->setIcon(QIcon(iconFiles[i]));
			signalMapper->setMapping(keyPadButton[i], getKeyPadCode(i, type));
		} else {
			keyPadButton[i]->setIcon(QIcon(iconFiles[i + 5]));
			signalMapper->setMapping(keyPadButton[i], getKeyPadCode(i, type));
		}
	}
#else
	for (i = Button_Index_Add; i < KP_MatrixButtonsNR; i++) {
		if (type == KP_Type_Edit) {
			keyPadButton[i]->hide();
		} else {
			keyPadButton[i]->show();
		}
	}
#endif
}

void EditAutoModeKeyPadWidget::mousePressEvent ( QMouseEvent * event )
{
	// Show buttons
	if (!keyPadButton[0]->isVisible()){
		for (int i = 0; i < KP_ButtonsNR; i++) {
			keyPadButton[i]->show();
		}
	}
	QWidget::mousePressEvent(event);
}

QSize EditAutoModeKeyPadWidget::sizeHint() const
{
	return QSize(208, 294);
}


void EditAutoModeKeyPadWidget::resizeEvent ( QResizeEvent * event )
{
#ifdef EDITAUTOMODEKEYPADWIDGET_DEBUG
	qDebug() << "resizeEvent: " << event->oldSize().width() << "x" << event->oldSize().height();
	qDebug() << "--> " << event->size().width() << "x" << event->size().height() << endl;
#endif

	QWidget::resizeEvent(event);
}

void EditAutoModeKeyPadWidget::hideButtons()
{
	if (keyPadButton[0]->isVisible()){
		for (int i = 0; i < KP_ButtonsNR; i++) {
			keyPadButton[i]->hide();
		}
	}
}

Button * EditAutoModeKeyPadWidget::createButton(int id , const QIcon &icon)
{
	Button *button = new Button;
	button->setIcon(icon);
    connect(button, SIGNAL(clicked()), signalMapper, SLOT(map()));
    button->setProperty("custom", true);
    signalMapper->setMapping(button, getKeyPadCode(id));
    return button;
}

int EditAutoModeKeyPadWidget::getKeyPadCode(int button_id, int type)
{
	int code = keyPadMappings[button_id];

#if 0
	if (type == KP_Type_AutoRun) {
		switch (button_id) {
		case Button_Index_Mutiply:
			code = SmtKb::KP_AdjustAddAll;
			break;
		case Button_Index_Divide:
			code = SmtKb::KP_AdjustSubAll;
			break;
		default:
			break;
		}
	}
#endif

	return code;
}
