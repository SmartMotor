/*
 * MotorControlProtocol.cpp
 *
 *  Created on: Mar 29, 2010
 *      Author: souken-i
 */

#include "MotorControlProtocol.h"
#include "MotorCommand.h"
#include "Global.h"

#include "Debug.h"

//#define MTCP_DEBUG

MotorControlProtocol::MotorControlProtocol(const char *deviceName)
{
	checkSumErrorCount = 0;
	preRawStatus = 0;
	rawStatus = 0;

	currentPos = 0xdeadbeef;

	MotorStatusHeader[MS_TStatus] = 'T';
	MotorStatusHeader[MS_FStatus] = 'F';

	MotorCommand::initializeCommandHeaders();

	// Create the SerialPortFrame Object
	initializeSerialPortFrame(deviceName);
}

MotorControlProtocol::~MotorControlProtocol()
{
	// TODO Auto-generated destructor stub
	delete spf;
}

void MotorControlProtocol::initializeSerialPortFrame(const char *deviceName)
{
	spf = new SerialPortFrame;
	spf->open(deviceName);
	connect(spf, SIGNAL(readyRead()), this, SLOT(handleRawSerialFrame()));
	connect(spf, SIGNAL(checksumError()), this, SLOT(handleFrameCheckSumError()));
}

void MotorControlProtocol::handleRawSerialFrame()
{
#ifdef MTCP_DEBUG
	qDebug() << "handleRawSerialFrame" << endl;
#endif

	// Read
	QByteArray frame = spf->readFrame();

	// Save last status
	preRawStatus = rawStatus;

	if (frame.at(RxFrameTypeIndex) == MotorStatusHeader[MS_TStatus]) {
		parseTStatusFrame(frame);
	}

	if (frame.at(RxFrameTypeIndex) == MotorStatusHeader[MS_FStatus]) {
		parseFStatusFrame(frame);
	}

	int event = rawStatus ^ preRawStatus;
	if (event) {
#ifdef MTCP_DEBUG
	qDebug() << "emit statusEvents: " << rawStatus << endl;
#endif
		emit statusEvents(rawStatus, event);
	}
}

void MotorControlProtocol::handleFrameCheckSumError()
{
	checkSumErrorCount++;
	// TODO Notice user
}

void MotorControlProtocol::parseTStatusFrame(QByteArray &frame)
{
	bool ok = false;

	char x = frame.at(RxXIndex);  // Byte 1
	char y = frame.at(RxYIndex);
	char z = frame.at(RxZIndex);

	if ((x >= '0') && (x <= '7')) {
		rawStatus &= ~TStatusXMask;  // Clear
		rawStatus |= ((x - '0') << TStatusXBit);
	}

	if ((y >= '0') && (y <= '7')) {
		rawStatus &= ~TStatusYMask;  // Clear
		rawStatus |= ((y - '0') << TStatusYBit);
	} else {
		/* Alarm */
		int alarmCode = 0;
		if (y == 'O' && z == 'C') {
			alarmCode = Alarm_OverCurrent;
		}
		if (y == 'O' && z == 'L') {
			alarmCode = Alarm_OverLoad;
		}
		if (y == 'L' && z == 'S') {
			alarmCode = Alarm_LoseControl;
		}
		if (y == 'R' && z == 'S') {
			alarmCode = Alarm_CommunicationError;
		}

		if (alarmCode > 0) {
			rawStatus &= ~TStatusAlarmMask;
			rawStatus |= (alarmCode << AlarmBit);
		}
	}

	if ((z >= '0') && (z <= '7')) {
		rawStatus &= ~TStatusZMask;  // Clear
		rawStatus |= ((z - '0') << TStatusZBit);
	}

	QByteArray data = frame.mid(RxPositionIndex, 6);
	int pos = data.toInt(&ok, 10);
	if (ok == true) {
		if (currentPos != pos) {
			currentPos = pos;
#ifdef MTCP_DEBUG
			qDebug() << "emit positionEvent: " << currentPos << endl;
#endif
			emit positionEvent(currentPos);
		}
	}
}

void MotorControlProtocol::parseFStatusFrame(QByteArray &frame)
{
	// TODO FIXME: The F status frame should also have checksum
	bool ok;

	char a = frame.at(RxPitchIndex);
	char b = frame.at(RxZeroIndex);

	if ((a >= '0') && (a <= '7')) {
		rawStatus &= ~PitchValueMask;  // Clear
		rawStatus |= ((a - '0') << PitchValueBit);
	}

	if (b == 'N') {
		rawStatus &= ~ZerorizeMask;
	} else if (b == 'Z') {
		rawStatus |= ZerorizeMask;
	}

	QByteArray currentArray = frame.mid(RxCurrentIndex, RxCurrentLength);
	int current = currentArray.toInt(&ok, 10);
	if (ok == true) {
		rawStatus &= ~CurrentValueMask;
		rawStatus |= (current << CurrentValueBit);
	}

	// Motor Direction
	char d = frame.at(RxDirectionIndex);
	if ((d == '+') || (d == '-')) {
		int dir;
		if (d == '+')
			dir = Dir_Forward;
		if (d == '-')
			dir = Dir_Backward;

		rawStatus &= ~DirectionMask;
		rawStatus |= (dir << DirectionBit);
	}

	// Motor Pusher Speed
	QByteArray speedArray = frame.mid(RxSpeedIndex, RxSpeedLength);
	int speed = speedArray.toInt(&ok, 10);
	if (ok == true) {
		if (currentSpeed!= speed) {
			currentSpeed = speed;
			emit speedEvent(currentSpeed);
		}
	}
}

int MotorControlProtocol::sendCommand(MotorCommand &command)
{
	lastCommand = command;

	return spf->writeFrame(command.frame());
}

int MotorControlProtocol::resendLastCommand()
{
	return spf->writeFrame(lastCommand.frame());
}

int MotorControlProtocol::sendCommand(QByteArray &commandString)
{
	return spf->writeFrame(commandString);
}

int MotorControlProtocol::getRawStatus()
{
	return rawStatus;
}

int MotorControlProtocol::getCheckSumErrorCount()
{
	return checkSumErrorCount;

}
void MotorControlProtocol::setCheckSumErrorCount(int value)
{
	checkSumErrorCount = value;
}

bool MotorControlProtocol::isMotorPresent()
{
	// If we got data from motor, the currentPos should be changed
	return (currentPos != 0xdeadbeef);
}

/*********************************************************************************/
bool MotorControlProtocol::isFrontLimitMaskChanged(int mask)
{
	return ((mask & FrontLimitEnableMask) != 0);
}

bool MotorControlProtocol::isMiddleLimitMaskChanged(int mask)
{
	return ((mask & MiddleLimitEnableMask) != 0);
}

bool MotorControlProtocol::isBackLimitMaskChanged(int mask)
{
	return ((mask & BackLimitEnableMask) != 0);
}

bool MotorControlProtocol::isFrontLimitStatusChanged(int mask)
{
	return ((mask & FrontLimitStatusMask) != 0);
}

bool MotorControlProtocol::isMiddleLimitStatusChanged(int mask)
{
	return ((mask & MiddleLimitStatusMask) != 0);
}

bool MotorControlProtocol::isBackLimitStatusChanged(int mask)
{
	return ((mask & BackLimitStatusMask) != 0);
}

bool MotorControlProtocol::isCutStatusChanged(int mask)
{
	return ((mask & CutStatusMask) != 0);
}

bool MotorControlProtocol::isPusherStatusChanged(int mask)
{
	return ((mask & PusherStatusMask) != 0);
}

bool MotorControlProtocol::isLimitSelectionChanged(int mask)
{
	return ((mask & LimitSelectMask) != 0);
}

bool MotorControlProtocol::isAlarmStatusChanged(int mask)
{
	return ((mask & AlarmMask) != 0);
}

bool MotorControlProtocol::isPitchChanged(int mask)
{
	return ((mask & PitchValueMask) != 0);
}

bool MotorControlProtocol::isZerorizeStatusChanged(int mask)
{
	return ((mask & ZerorizeMask) != 0);
}

bool MotorControlProtocol::isStopModeChanged(int mask)
{
	return ((mask & StopModeMask) != 0);
}

bool MotorControlProtocol::isDirectionChanged(int mask)
{
	return ((mask & DirectionMask) != 0);

}

bool MotorControlProtocol::isCurrentValueChanged(int mask)
{
	return ((mask & CurrentValueMask) != 0);
}

/*********************************************************************************/
int MotorControlProtocol::getFrontLimitMask(int status)
{
	return ((status & FrontLimitEnableMask) >> FrontLimitEnableBit);
}

int MotorControlProtocol::getMiddleLimitMask(int status)
{
	return ((status & MiddleLimitEnableMask) >> MiddleLimitEnableBit);
}

int MotorControlProtocol::getBackLimitMask(int status)
{
	return ((status & BackLimitEnableMask) >> BackLimitEnableBit);
}

int MotorControlProtocol::getFrontLimitStatus(int status)
{
	return ((status & FrontLimitStatusMask) >> FrontLimitStatusBit);
}

int MotorControlProtocol::getMiddleLimitStatus(int status)
{
	return ((status & MiddleLimitStatusMask) >> MiddleLimitStatusBit);
}

int MotorControlProtocol::getBackLimitStatus(int status)
{
	return ((status & BackLimitStatusMask) >> BackLimitStatusBit);
}

int MotorControlProtocol::getCutStatus(int status)
{
	return ((status & CutStatusMask) >> CutStatusBit);
}

int MotorControlProtocol::getPusherStatus(int status)
{
	return ((status & PusherStatusMask) >> PusherStatusBit);
}

int MotorControlProtocol::getWhichLimit(int status)
{
	return ((status & LimitSelectMask) >> LimitSelectBit);
}

int MotorControlProtocol::getAlarmCode(int status)
{
	return ((status & AlarmMask) >> AlarmBit);
}

int MotorControlProtocol::getPitch(int status)
{
	return ((status & PitchValueMask) >> PitchValueBit);
}

int MotorControlProtocol::getZerorize(int status)
{
	return ((status & ZerorizeMask) >> ZerorizeBit);
}

int MotorControlProtocol::getStopMode(int status)
{
	return ((status & StopModeMask) >> StopModeBit);
}

int MotorControlProtocol::getDirection(int status)
{
	return ((status & DirectionMask) >> DirectionBit);
}

int MotorControlProtocol::getCurrent(int status)
{
	return ((status & CurrentValueMask) >> CurrentValueBit);
}

#ifdef MOTOR_TEST_WIDGET_ENABLE
void MotorControlProtocol::forceStatusChange(int cutStatus, int pusherStatus)
{
	preRawStatus = rawStatus;

	rawStatus &= ~TStatusZMask;  // Clear
	rawStatus |= ((cutStatus << CutStatusBit) | (pusherStatus << PusherStatusBit));
	int event = rawStatus ^ preRawStatus;
	if (event) {
#ifdef MTCP_DEBUG
	qDebug() << "emit statusEvents: " << rawStatus << " " << event;
#endif
		emit statusEvents(rawStatus, event);
	}
}

void MotorControlProtocol::forcePositionChange(int pos)
{
	currentPos = pos;
	emit positionEvent(currentPos);
}
#endif


