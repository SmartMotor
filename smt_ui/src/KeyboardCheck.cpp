/*
 * KeyboardCheck.cpp
 *
 *  Created on: Aug 25, 2010
 *      Author: souken-i
 */

#include <QtGui>

// For Keyboard ioctl
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <linux/input.h>
#include <fcntl.h>
#include <unistd.h>

#include "SmtKb.h"
#include "KeyboardCheck.h"

#define KEYBOARD_DEVICE        "/dev/input/event0"
#define KEYBOARD_ONLINE_MASK   0x1000

KeyboardCheck::KeyboardCheck(QWidget *parent)
	: QDialog(parent)
{
    QRect desktop = QApplication::desktop()->geometry();
    desktop.moveTo(QPoint(0, 0));
    setGeometry(desktop);

    setFocusPolicy(Qt::StrongFocus);
    setFocus();
    setModal(true);

    _exitCount = 0;

    title = new QLabel;
    title->setAlignment(Qt::AlignCenter);
	QFont font = this->font();
	font.setPointSize(28);
	font.setBold(true);
	title->setFont(font);

	QPalette palette;
	palette.setColor(QPalette::WindowText, QColor("blue"));
	title->setPalette(palette);
	title->setAutoFillBackground(true);

	guide = new QLabel;
	guide->setAlignment(Qt::AlignCenter);

	display = new QLabel;
	display->setAlignment(Qt::AlignCenter);
	display->setFont(font);
	display->setPalette(palette);
	display->setAutoFillBackground(true);

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addSpacing(50);
	layout->addWidget(title);
	layout->addSpacing(20);
	layout->addWidget(guide);
	layout->addStretch(1);
	layout->addWidget(display);
	layout->addStretch(2);

	setLayout(layout);
    setStyleSheet("background-color: white;");

	retranslateUi();

#if 0
	checkKbdConnection();
#endif
}

KeyboardCheck::~KeyboardCheck()
{
}

void KeyboardCheck::retranslateUi()
{
	title->setText(tr("Keyboard Check"));
	guide->setText(tr("Press any key ... Double press Enter to exit."));

}

void KeyboardCheck::keyPressEvent(QKeyEvent *event)
{
	switch(event->key()) {
	case SmtKb::Key_Enter:
		display->setText(tr("Enter"));
		if (++_exitCount == 2) {
			close();
		}
		break;
	case SmtKb::Key_F1:
		display->setText("F1");	break;
	case SmtKb::Key_F2:
		display->setText("F2");	break;
	case SmtKb::Key_F3:
		display->setText("F3");	break;
	case SmtKb::Key_F4:
		display->setText("F4"); break;
	case SmtKb::Key_F5:
		display->setText("F5"); break;
	case SmtKb::Key_F6:
		display->setText("F6"); break;
	case SmtKb::Key_0:
		display->setText("0"); break;
	case SmtKb::Key_1:
		display->setText("1"); break;
	case SmtKb::Key_2:
		display->setText("2"); break;
	case SmtKb::Key_3:
		display->setText("3"); break;
	case SmtKb::Key_4:
		display->setText("4"); break;
	case SmtKb::Key_5:
		display->setText("5"); break;
	case SmtKb::Key_6:
		display->setText("6"); break;
	case SmtKb::Key_7:
		display->setText("7"); break;
	case SmtKb::Key_8:
		display->setText("8"); break;
	case SmtKb::Key_9:
		display->setText("9"); break;

	case SmtKb::Key_Period:
		display->setText("."); break;
	case SmtKb::Key_Plus:
		display->setText("+"); break;
	case SmtKb::Key_Minus:
		display->setText("-"); break;
	case SmtKb::Key_Multiply:
		display->setText("*"); break;
	case SmtKb::Key_Div:
		display->setText("/"); break;
	case SmtKb::Key_Equal:
		display->setText("="); break;

	case SmtKb::Key_Backspace:
		display->setText(tr("Backspace")); break;
	case SmtKb::Key_Insert:
		display->setText(tr("Insert")); break;
	case SmtKb::Key_Modify:
		display->setText(tr("Modify")); break;

	case SmtKb::Key_Up:
		display->setText(tr("Up")); break;
	case SmtKb::Key_Down:
		display->setText(tr("Down")); break;
	case SmtKb::Key_Left:
		display->setText(tr("Left")); break;
	case SmtKb::Key_Right:
		display->setText(tr("Right")); break;
	case SmtKb::Key_PageUp:
		display->setText(tr("Page Up")); break;
	case SmtKb::Key_PageDown:
		display->setText(tr("Page Down")); break;
	case SmtKb::Key_Delete:
		display->setText(tr("Delete")); break;
	case SmtKb::Key_Exit:
		display->setText(tr("Exit")); break;
	case SmtKb::Key_Program:
		display->setText(tr("Program")); break;
	case SmtKb::Key_Forward:
		display->setText(tr("Forward")); break;
	case SmtKb::Key_Backward:
		display->setText(tr("Backward")); break;
	case SmtKb::Key_Run:
		display->setText(tr("Run")); break;
	case SmtKb::Key_Help:
		display->setText(tr("Help")); break;
	case SmtKb::Key_Clear:
		display->setText(tr("Clear")); break;
	case SmtKb::Key_Stop:
		display->setText(tr("Stop")); break;
	case SmtKb::Key_Pusher:
		display->setText(tr("Pusher")); break;
	case SmtKb::Key_Edit:
		display->setText(tr("Edit")); break;
	case SmtKb::Key_Auto:
		display->setText(tr("Auto")); break;
	case SmtKb::Key_Manual:
		display->setText(tr("Manual")); break;
	case SmtKb::Key_AutoCut:
		display->setText(tr("AutoCut")); break;
	case SmtKb::Key_Traning:
		display->setText(tr("Training")); break;
	case SmtKb::Key_FastBackward:
		display->setText(tr("Fast Backward")); break;
	case SmtKb::Key_FastForward:
		display->setText(tr("Fast Forward")); break;

	default:
		display->setText(tr("Unknown Key"));
		break;
	}
}

#if 0
void KeyboardCheck::checkKbdConnection()
{
	int fd = 0;
	unsigned int version;
	unsigned short id[4];

	if ((fd = ::open(KEYBOARD_DEVICE, O_RDONLY)) < 0) {
		display->setText(tr("Open keyboard device error."));
		return;
	}

	if (::ioctl(fd, EVIOCGVERSION, &version)) {
		display->setText(tr("Open keyboard device error."));
		::close(fd);
		return;
	}

	::ioctl(fd, EVIOCGID, id);

	if (id[ID_PRODUCT] & KEYBOARD_ONLINE_MASK) {
		display->setText(tr("Keyboard is connected."));
	} else {
		display->setText(tr("Keyboard is not connected."));
	}

	::close(fd);
}
#endif
