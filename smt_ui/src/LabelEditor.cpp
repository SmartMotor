/*
 * LabelEditor.cpp
 *
 *  Label Editor Dialog
 *    - Called from MainFrame
 *    - Provide 3 line edits to get total length, label length and
 *      unused length;
 *    - When one data is done(input data and press Enter), set focus
 *      to the next line edit;
 *
 *
 *  Created on: May,31 2010
 *      Author: Zeng Xianwei
 */
#include <QtGui>
#include "LabelEditor.h"
#include "Global.h"
#include "Debug.h"

extern struct labelEditData_t labelEditData;

LabelEditor::LabelEditor(QWidget *parent)
	: EditDialog(parent)
{
	int i;

	// clear global structure
	labelEditData.mask = 0;  // no data is valid

	connect(getOKButton(), SIGNAL(clicked()), this, SLOT(handleInputData()));
	connect(getCancelButton(), SIGNAL(clicked()), this, SLOT(close()));

	QRegExp rx("\\d{1,4}\\.{0,1}\\d{0,2}");
	QValidator *validator = new QRegExpValidator(rx, this);

	for (i = 0; i < LABEL_EDIT_ITEM_NR; i++) {
		descLabels[i] = new QLabel;
		inputEdits[i] = new LineEditVKeyboard;
		inputEdits[i]->setValidator(validator);
	}

	// Layout
	QGridLayout *inputLayout = new QGridLayout;
	for (i = 0; i < LABEL_EDIT_ITEM_NR; i++) {
		inputLayout->addWidget(descLabels[i], i, 0);
		inputLayout->addWidget(inputEdits[i], i, 1);
	}

	insertDrawingArea(inputLayout);

	// Focus to the first input box
	inputEdits[TOTAL_LENGTH]->setFocus();

	setGuideIcon(QPixmap("images/guide-labeledit.png"));

	retranslateUi();
    //setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #E2EDED, stop: 1 #A7AFAF);");
}

LabelEditor::~LabelEditor()
{
}

void LabelEditor::handleInputData()
{
	QString	input;
	int data;

//	qDebug() << "handleInputData " << endl;

	input = inputEdits[TOTAL_LENGTH]->text();
	data = MData::toInt(input);
	if ( gMotor->isPostionValid(data) ) {
		labelEditData.totalLength = data;
		labelEditData.mask = LED_TOTAL_LENGTH_VALID;
	} else {
		inputEdits[TOTAL_LENGTH]->selectAll();
		inputEdits[TOTAL_LENGTH]->setFocus();
		return;
	}

	// Label length doesn't have limitation
	input = inputEdits[LABEL_LENGTH]->text();
	data = MData::toInt(input);
	if ( data > 0 ) {
		labelEditData.labelLength = data;
		labelEditData.mask |= LED_LABEL_LENGTH_VALID;
	} else {
		inputEdits[LABEL_LENGTH]->selectAll();
		inputEdits[LABEL_LENGTH]->setFocus();
		return;
	}

	// Unused length doesn't have limitation
	input = inputEdits[UNUSED_LENGTH]->text();
	data = MData::toInt(input);
	if ( data > 0 ) {
		labelEditData.unusedLength = data;
		labelEditData.mask |= LED_UNUSED_LENGTH_VALID;
	} else {
		inputEdits[UNUSED_LENGTH]->selectAll();
		inputEdits[UNUSED_LENGTH]->setFocus();
		return;
	}

	if (labelEditData.mask == LED_ALL_VALID) {
		close();
	}
}

void LabelEditor::retranslateUi()
{
	setTitle(tr("Label Edit"));
	setGuideText(tr("Input length and press ENTER"));
	descLabels[TOTAL_LENGTH ]->setText(tr("Total Length"));
	descLabels[LABEL_LENGTH ]->setText(tr("Label Length"));
	descLabels[UNUSED_LENGTH]->setText(tr("Unused Length"));
}

/**
 * After input data, then press Enter, if the data is valid,
 * move focus to next edit box
 */
void  LabelEditor::keyPressEvent(QKeyEvent *event)
{
//	qDebug() << "Key Pressed in Input Box: " << hex << event->key() << endl;

	switch(event->key()) {
	case SmtKb::Key_F1:
	case SmtKb::Key_Enter:
	case Qt::Key_Enter:
		handleInputData();
		break;
	case SmtKb::Key_F2:
		close();
		break;
	case SmtKb::Key_Clear:
		// clear current input box
		for (int i = 0; i < LABEL_EDIT_ITEM_NR; i++) {
			if (inputEdits[i]->hasFocus()) {
				inputEdits[i]->clear();
				break;
			}
		}
		break;
	default:
		QWidget::keyPressEvent(event);
	}
}

