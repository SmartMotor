#include <QApplication>
#include <QVBoxLayout>
#include <QPainter>
#include <QLabel>
#include <QTextCodec>
#include <QDir>

#include "MainFrame.h"
#include "System.h"
#include "Motor.h"
#include "IM/IM.h"
#include "Global.h"
#include "StartScreen.h"

// Global object
System *gSysConfig;
Motor  *gMotor;
VirtualKeyboard *gVKeyboard;
Beeper *gBeeper;

int main(int argc, char **argv)
{
	QApplication app(argc, argv, QApplication::GuiServer);

#ifdef START_DEBUG
	qDebug() << "Start smt_ui ... ";
#endif

	/* Set text codec. This should be done more flexibly. */
	QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
	QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));

	gSysConfig = new System;
	gMotor = new Motor;

	System::setCurrentUser(QString("user0"));
	System::setDataRootDir(app.applicationDirPath() + QString("/data/"));

	QString sysConfigFile = System::dataRootDir() + QString("system.cfg");
	gSysConfig->loadSysConfigFile(sysConfigFile);

	// FIXME: Where should be set system-wide font?
	QFont sysFont = qApp->font();
	sysFont.setPointSize(gSysConfig->getSysFontSize());
	qApp->setFont(sysFont);

	/* Hide cursor */
	/*
	 * We compile qt library with -no-feature-CURSOR, so this
	 * is not necessary
	 */
//	qApp->setOverrideCursor(QCursor(Qt::BlankCursor));

#if 1
	// Language
	gSysConfig->installAppTranslator();
	gSysConfig->loadAppTranslator();
#endif

	// Dir initialize
	QDir dir;
	dir = QDir::current();
	if (!dir.exists(System::dataRootDir()))
			dir.mkdir(System::dataRootDir());

	if (!dir.exists(System::currentUserDir()))
		dir.mkdir(System::currentUserDir());

	if (!dir.exists(System::currentProgramDir()))
		dir.mkdir(System::currentProgramDir());

	gMotor->loadConfigFile(System::currentUserDir() + "motor.cfg");

	// Initialize Motor Log and so on
	gMotor->postInitialize();

#if 0 /* For test */
	gSysConfig->save();

	gMotor->save();
	gMotor->saveToSystem();
#endif

	// Set window icon for all widgets
	qApp->setWindowIcon(QIcon("images/logo-32.png"));

	// beeper
	gBeeper = new Beeper;
	gBeeper->setBeeperOn(); // test

#if 1
	StartScreen startScreen;
	startScreen.show();
	startScreen.showFullScreen();
	startScreen.exec();
#endif

	MainFrame mainFrame;
#if 0
	// Use showFullScreen()
	mainFrame.resize(DEFAUT_MAIN_W, DEFAUT_MAIN_H);
#endif

	//
	// Install input method.
	//
	if (QApplication::GuiServer == app.type()) {
		IM::installInputMethod();
		//QObject::connect(&app, SIGNAL(focusChanged(QWidget *, QWidget *)),
		//		IM::getIMWindow(), SLOT(focusChanged(QWidget *, QWidget *)));
	}

	mainFrame.showFullScreen();

	return app.exec();
}
