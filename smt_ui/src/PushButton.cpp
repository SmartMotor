/*
 * PushButton.cpp
 *
 *  Created on: Sep 12, 2010
 *      Author: souken-i
 */

#include "PushButton.h"
#include "Global.h"

PushButton::PushButton(QWidget * parent)
	: QPushButton(parent)
{
}

PushButton::PushButton ( const QString & text, QWidget * parent )
	: QPushButton(text, parent)
{
}

PushButton::PushButton ( const QIcon & icon, const QString & text, QWidget * parent )
	: QPushButton(icon, text, parent)
{
	// do nothing
}

PushButton::~PushButton() {
	// TODO Auto-generated destructor stub
}

void PushButton::mousePressEvent ( QMouseEvent * event )
{
	gBeeper->setBeeperOn();

	QPushButton::mousePressEvent(event);
}

void PushButton::mouseReleaseEvent ( QMouseEvent * event )
{
	gBeeper->setBeeperOff();

	QPushButton::mouseReleaseEvent(event);
}
