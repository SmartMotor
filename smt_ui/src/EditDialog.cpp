/*
 * EditDialog.cpp
 *
 *  Created on: 2010/06/06
 *      Author: crazysjf
 */

#include "include/EditDialog.h"
#include <Debug.h>

EditDialog::EditDialog(QWidget *parent) : RoundRectDialog(parent)
{
	titleLabel = new QLabel;
	titleLabel->setAlignment(Qt::AlignCenter);

	QFont font = this->font();
	font.setPointSize(20);
	font.setBold(true);
	titleLabel->setFont(font);

	guideLabel = new QLabel;
	guideLabel->setAlignment(Qt::AlignCenter);

	guideIconLabel = new QLabel;
	guideIconLabel->setAlignment(Qt::AlignCenter);

	//QPalette palette;
	//palette.setColor(QPalette::WindowText, QColor("red"));
	//titleLabel->setPalette(palette);
	//titleLabel->setAutoFillBackground(true);

	/*
	 * TODO:
	 *   Why do I have to write 2 l's to get the "Cancel" text displayed correctly when padding is used in stylesheet?.
	 */
	OKButton     = new PushButton(QIcon("images/enter-48.png"), tr("F1: Done"), this);
	cancelButton = new PushButton(QIcon("images/delete-64.png"), tr("F2: Exit"), this);
	OKButton->setFocusPolicy(Qt::NoFocus);
	cancelButton->setFocusPolicy(Qt::NoFocus);

	// Layout
	QVBoxLayout *buttonLayout = new QVBoxLayout;
	buttonLayout->addStretch();
	buttonLayout->addWidget(OKButton);
	buttonLayout->addWidget(cancelButton);

	bottomLayout = new QHBoxLayout;
	bottomLayout->addLayout(buttonLayout);

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addWidget(titleLabel);
	layout->addSpacing(10);
	layout->addWidget(guideLabel);
	layout->addWidget(guideIconLabel);
	layout->addSpacing(10);
	layout->addLayout(bottomLayout);
	layout->addSpacing(10);
	setLayout(layout);

	retranslateUi();
    //setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #E2EDED, stop: 1 #A7AFAF);");
	loadStyleSheet(":/qss/editdialog.qss");
}

EditDialog::~EditDialog() {
	// TODO Auto-generated destructor stub
}

void EditDialog::setTitle(const QString &title)
{
	titleLabel->setText(title);
}

void EditDialog::setGuideText(const QString &msg)
{
	guideLabel->setText(msg);
}

void EditDialog::setGuideIcon(const QPixmap &pm)
{
	guideIconLabel->setPixmap(pm);
}

void EditDialog::retranslateUi()
{
}

void EditDialog::insertDrawingArea(QLayout *layout)
{
	if (layout != NULL)
		bottomLayout->insertLayout(0, layout);
}

void EditDialog::loadStyleSheet(const QString & fileName)
{
	if ( !fileName.isEmpty() ) {
		QFile file(fileName);
		file.open(QFile::ReadOnly);
		QString styleSheet = QLatin1String(file.readAll());

		this->setStyleSheet(styleSheet);

		file.close();
	}
}
