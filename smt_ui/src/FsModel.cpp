/*
 * FsModel.cpp
 *
 *  Created on: 2010/04/10
 *      Author: crazysjf
 */

#include "FsModel.h"
#include "Program.h"

//#define FSMODEL_DEBUG

//#define HAVE_DESCRIPTION_IN_LIST

FsModel::FsModel(QObject *parent)
	: QStandardItemModel(parent)
{
	QStringList horizontalHeaderString;
#ifdef HAVE_DESCRIPTION_IN_LIST
	horizontalHeaderString << tr("File") << tr("Last Modified") << tr("Use Count") << tr("Cut Count") << tr("First Position") << tr("First Flags") << tr("Description");
#else
	// No description in the list
	horizontalHeaderString << tr("File") << tr("Last Modified") << tr("Use Count") << tr("Cut Count") << tr("First Position") << tr("First Flags");
#endif
	setHorizontalHeaderLabels(horizontalHeaderString);

	fsWatcher = new QFileSystemWatcher(this);
	connect(fsWatcher, SIGNAL(fileChanged ( const QString &)),
			this, SLOT(fileChanged(const QString &)));
	connect(fsWatcher, SIGNAL(directoryChanged ( const QString &)),
			this, SLOT(directoryChanged(const QString &)));
}

FsModel::~FsModel() {
	// TODO Auto-generated destructor stub
}

///
// Set root directory of this model.
//
void FsModel::setRootPath(const QString &newPath)
{
	rootPath = newPath;
	fsWatcher->addPath(newPath);
	QDir dir(rootPath);
	QStringList entryList = dir.entryList();
	for (int i=0; i<entryList.count(); i++) {
		QString &fileName = entryList[i];
		if (fileName == ".." || fileName == ".")
			continue;
	}

	// refresh display
	directoryChanged(rootPath);
}

///
// Return root directory of this model.
//
QDir FsModel::rootDirectory()
{
	return QDir(rootPath);
}

///
// Return @path's base name.
//
//   eg. If @path == "/abc/def/ghi", the return "ghi".
//
QString FsModel::baseName(const QString &path)
{
	return path.right(path.size() - path.lastIndexOf("/") - 1);
}

///
// Find row number of the line corresponding to @fileName.
//
//   @ret: Row number on success. -1 on failure.
//
int FsModel::findRow(const QString &fileName)
{
	QFileInfo finfo(fileName);
//	QString fileID = finfo.baseName().remove("data");
	QString fileID = finfo.baseName();

	for (int row = 0; row < rowCount(); row++) {
		if (index(row, FILE_NAME_COLUMN).data(Qt::DisplayRole).toString() == fileID)
			return row;
	}

	return -1;
}

///
// Load @fileName, use its information to set the line at @row in model.
//
//   @row:      Row to be set.
//   @fileName: File name under root path.
//
//
void FsModel::setRowItems(const int row, QString &fileName)
{
	QString filePath = rootPath + "/" + fileName;
	Program p(filePath);
	p.load();

	QFileInfo info(filePath);

	int cutCount = p.cutCount();

	QStandardItem *fileNameI = new QStandardItem;
	QStandardItem *lastModifiedI = new QStandardItem;
	QStandardItem *useCountI = new QStandardItem;
	QStandardItem *cutCountI = new QStandardItem;
	QStandardItem *firstPositionI = new QStandardItem;
	QStandardItem *firstFlagsI = new QStandardItem;
#ifdef HAVE_DESCRIPTION_IN_LIST
	QStandardItem *descriptionI = new QStandardItem;
#endif


#if 0
	fileNameI->setData(fileName, Qt::DisplayRole);
#else
	QFileInfo finfo(fileName);
	QString basename = finfo.baseName();
//	fileNameI->setData(basename.remove("data"), Qt::DisplayRole);
	fileNameI->setData(basename, Qt::DisplayRole);
#endif
	QIcon icon("images/file-icon-32.png");
	fileNameI->setData(icon, Qt::DecorationRole);

//	lastModifiedI->setData(info.lastModified().toString(Qt::DefaultLocaleShortDate), Qt::DisplayRole);
	// Modify the date & time format to get a fix length
	lastModifiedI->setData(info.lastModified().toString("yy/MM/dd hh:mm"), Qt::DisplayRole);

	useCountI->setData(p.header().useCount, Qt::DisplayRole);

	cutCountI->setData(cutCount, Qt::DisplayRole);
	if(cutCount != 0) {
		firstPositionI->setData(p.cuts()[0].position, Qt::DisplayRole);
		firstFlagsI->setData(p.cuts()[0].flags, Qt::DisplayRole);
	}

#ifdef HAVE_DESCRIPTION_IN_LIST
	descriptionI->setData(p.description(), Qt::DisplayRole);
#endif

	fileNameI->setFlags(fileNameI->flags() & ~Qt::ItemIsEditable);
	lastModifiedI->setFlags(lastModifiedI->flags() & ~Qt::ItemIsEditable);
	useCountI->setFlags(useCountI->flags() & ~Qt::ItemIsEditable);
	cutCountI->setFlags(cutCountI->flags() & ~Qt::ItemIsEditable);
	firstPositionI->setFlags(firstPositionI->flags() & ~Qt::ItemIsEditable);
	firstFlagsI->setFlags(firstFlagsI->flags() & ~Qt::ItemIsEditable);
#ifdef HAVE_DESCRIPTION_IN_LIST
	descriptionI->setFlags(descriptionI->flags() & ~Qt::ItemIsEditable);
#endif

	setItem(row, FILE_NAME_COLUMN, fileNameI);
	setItem(row, LAST_MODIFIED_COLUMN, lastModifiedI);
	setItem(row, USE_COUNT_COLUMN, useCountI);
	setItem(row, CUT_COUNT_COLUMN, cutCountI);
	setItem(row, FIRST_POSITION_COLUMN, firstPositionI);
	setItem(row, FIRST_FLAGS_COLUMN, firstFlagsI);
#ifdef HAVE_DESCRIPTION_IN_LIST
	setItem(row, DESCRIPTION_COLUMN, descriptionI);
#endif
}

#if 0
///
// Refresh model using latest data.
//
void FsModel::refresh()
{
	int row = 0;

	removeRows(0, rowCount());
	QDir dir(rootPath);
	QStringList entryList = dir.entryList();
	for (int i=0; i<entryList.count(); i++) {
		QString &fileName = entryList[i];
		if (fileName == ".." || fileName == ".")
			continue;

		setRowItems(row, fileName);
		row++;
	}
}
#endif

///
// Return file name (not full path) of the file under @index.
//
QString FsModel::fileName(const QModelIndex & index) const
{

	QString fileName = item(index.row(), FILE_NAME_COLUMN)->data(Qt::DisplayRole).value<QString>();
#if 0
	return fileName;
#else
//	return "data" + fileName + ".pro";
	return fileName + ".pro";
#endif
}


///
// Return full path of the file under @index.
//
QString FsModel::filePath(const QModelIndex & index) const
{

//	qDebug() << index.row() << endl;
	QString fileName = item(index.row(), FILE_NAME_COLUMN)->data(Qt::DisplayRole).value<QString>();
#if 0
	return rootPath + "/" + fileName;
#else
//	return rootPath + "/data" + fileName + ".pro";
	return rootPath + "/" + fileName + ".pro";
#endif
}

///
// Remove the file under @index.
//
bool FsModel::remove(const QModelIndex & index) const
{
	QDir dir(rootPath);
//	qDebug() << "remove " << fileName(index).toAscii().data() << endl;
	return dir.remove(fileName(index));
}

///
// Handle the following cases in this slot.
//
//    file modification
//
void FsModel::fileChanged(const QString &path)
{
	QString _baseName = baseName(path);
#ifdef FSMODEL_DEBUG
	qDebug() << "file changed: " << path.toAscii().data() << " " << _baseName.toAscii().data()<< endl;
#endif

	if (rootDirectory().exists(_baseName)) {
		// Handle modification
		setRowItems(findRow(_baseName), _baseName);
	}
}

///
// Handle the following cases in this slot.
//
//    file addition
//    file deletion
//
void FsModel::directoryChanged(const QString & path)
{
#ifdef FSMODEL_DEBUG
	qDebug() << "directory changed: " << path.toAscii().data() << endl;
#endif

	static QStringList oldList;

	QStringList newList = rootDirectory().entryList();

	// Handle addition.
	for (int i=0; i<newList.count(); i++) {
		QString &fileName = newList[i];

		if (fileName == ".." || fileName == ".")
			continue;

		if (oldList.contains(fileName))
			continue;

#ifdef FSMODEL_DEBUG
		qDebug() << "adding: " << fileName.toAscii().data() << endl;
#endif

		setRowItems(rowCount(), fileName);
		fsWatcher->addPath(rootPath + fileName);
	}

	// Handle deletion.
	for (int i=0; i<oldList.count(); i++) {
		QString &fileName = oldList[i];

		if (fileName == ".." || fileName == ".")
			continue;

		if (newList.contains(fileName))
			continue;

#ifdef FSMODEL_DEBUG
		qDebug() << "deleteing: " << fileName.toAscii().data() << endl;
#endif

		removeRow(findRow(fileName));
		fsWatcher->removePath(rootPath + fileName);
	}

	oldList = newList;
}
