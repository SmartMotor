
#include <QtGui>
#include <QFileInfo>

#include "MainFrame.h"
#include "MachineSettingDialog.h"
#include "FlagEditor.h"
#include "MessageBox.h"

#include "Global.h"

//#define MAINFRAME_DEBUG

/**
 * Global variables
 */
struct labelEditData_t labelEditData;
struct averageEditData_t averageEditData;

/*
 * MainFrame Layout:
 *   MainFrame has two kinds of layouts:
 *   Common Layout and File Layout
 *
 * Common Layout:
 *
 *    +----------------------+
 *    |       Header         |
 *    +--+-----------+---+---+
 *    |  |           |   |   |
 *    |A |    B      |C  | D |
 *    |  +-----------+---+   |
 *    |  |      E        |   |
 *    +--+---------------+---+
 *    |       Status Bar     |
 *    +----------------------+
 *
 *    A is mode buttons, never changed.
 *    B,C,D,E have more than one stacked page.
 *
 * File Select Dialog Layout:
 *
 *    +----------------------+
 *    |       Header         |
 *    +--+---------------+---+
 *    |  |               |   |
 *    |A |       B       | D |
 *    |  |               |   |
 *    |  |               |   |
 *    +--+---------------+---+
 *    |       Status Bar     |
 *    +----------------------+
 *
 *    B is file list and file description edit box.
 *    D is function button
 *
 */
static const struct ModePageIndexTable {
	int topPage;
	int mainPage;                // B: Main List Page Index
	int keypadPage;              // C: KeyPad Page Index
	int fnButtonPage;            // D: Fn Button Page Index
	int inputPage;               // E: Input Line Page Index
} pageIndexTable[MainFrame::MaxDisplayModeNumber] = {
	// FIXME: Keep the order
	{
		// ModeProgramEdit
		MainFrame::CommonTopPageIndex,
		MainFrame::DataListPageIndex,
		MainFrame::EditKeyPadPageIndex,
		MainFrame::EditButtonPageIndex,
		MainFrame::EditInputPageIndex,
	},
	{
		// ModeManualRun
		MainFrame::CommonTopPageIndex,
		MainFrame::MotorStatusPageIndex,
		MainFrame::MetersPageIndex,
		MainFrame::ManualRunButtonPageIndex,
		MainFrame::RunInputPageIndex,
	},
	{
		// ModeAutoRun
		MainFrame::CommonTopPageIndex,
		MainFrame::DataListPageIndex,
		MainFrame::EditKeyPadPageIndex,
		MainFrame::AutoRunButtonPageIndex,
		MainFrame::RunInputPageIndex,
	},
	{
		// ModeTraining
		MainFrame::CommonTopPageIndex,
		MainFrame::DataListPageIndex,
		MainFrame::EditKeyPadPageIndex,
		MainFrame::EditButtonPageIndex,
		MainFrame::RunInputPageIndex,
	},
	{
		// ModeAutoCutRun
		MainFrame::CommonTopPageIndex,
		MainFrame::DataListPageIndex,
		MainFrame::EditKeyPadPageIndex,
		MainFrame::AutoRunButtonPageIndex,
		MainFrame::RunInputPageIndex,
	},
	{
		// ModeProgramSelect
		MainFrame::FileSelectTopPageIndex,
		MainFrame::UnusedPageIndex,
		MainFrame::UnusedPageIndex,
		MainFrame::UnusedPageIndex,
		MainFrame::UnusedPageIndex,
	},
};

MainFrame::MainFrame()
{
	_majorMode          = ModeProgramEdit;
	_previousMajorMode  = ModeProgramEdit;
	_minorMode          = _majorMode;

#if 0
	// set background
	QPalette palette;
	palette.setColor(QPalette::Window, QColor("#797979"));
	setPalette(palette);
#endif

	// Load background picture
	bkgndPixmap = QPixmap("images/main-bkgnd.png");

	headerLabel = new HeaderLabel;
	paperPusherBar = new PaperPusherBar;

	QVBoxLayout *topLayout = new QVBoxLayout;
	topLayout->addWidget(headerLabel);
	topLayout->addWidget(paperPusherBar);
	topLayout->setSpacing(0);

	// Mode buttons on left side
	createModeButtons();
	QVBoxLayout *modeButtonLayout = new QVBoxLayout;
	for (int i = 0; i < MaxMajorModeNumber; i++) {
		modeButtonLayout->addWidget(modeButton[i]);
	}
	modeButtonLayout->setSpacing(5);

	// Mode Pages
	createTopPagesWidget();

	QGridLayout *middleLayout = new QGridLayout;
	middleLayout->addLayout(modeButtonLayout, 0, 0, 1, 1);
	middleLayout->addWidget(topPagesWidget, 0, 1, 1, 9);

	createStatusBar();
	QHBoxLayout *statusBarLayout = new QHBoxLayout;
	statusBarLayout->addWidget(statusIconLabel[0]);
	statusBarLayout->addWidget(statusIconLabel[1]);
	statusBarLayout->addWidget(statusBarLabel);
	statusBarLayout->setStretchFactor(statusBarLabel, 1);
	statusBarLayout->addWidget(statusIconLabel[2]);
	statusBarLayout->addWidget(statusIconLabel[3]);

	QVBoxLayout *mainLayout = new QVBoxLayout;
	mainLayout->addLayout(topLayout);
	mainLayout->addLayout(middleLayout);
	mainLayout->addLayout(statusBarLayout);
	setLayout(mainLayout);

	// Load last open file
	loadLastOpenFile();

	// Initialize current index to 0
	programEditor->topIndex();

	inputBox->setFocus();
	updateModeButtons();

	// They will be constructed when needed
	calculator = 0;
	machineSettingDialog = 0;

	// Signals and slot
	connect(gMotor, SIGNAL(oneCutLoopFinished()), this, SLOT(runToNext()));

	gVKeyboard = new VirtualKeyboard(this);
//	connect(qApp, SIGNAL(focusChanged(QWidget *, QWidget *)),
//			gVKeyboard, SLOT(focusChanged(QWidget *, QWidget *)));

	loadStyleSheet(QString(":/qss/mainframe.qss"));


	// Initialize motor, for test
	gMotor->start();

	this->showFullScreen();

	retranslateUi();
}

MainFrame::~MainFrame()
{
}

/****************************************************************************/
/*   Mode Buttons on the left side */
void MainFrame::createModeButtons()
{
//	modeButton[ModeProgramSelect] = createButton(QIcon("images/files-64.png"), this, SLOT(setProgramSelectMode()));
	modeButton[ModeProgramEdit  ] = createButton(QIcon("images/md-edit.png"), this, SLOT(setProgramEditMode()));
	modeButton[ModeAutoRun      ] = createButton(QIcon("images/md-autorun.png"), this, SLOT(setAutoRunMode()));
	modeButton[ModeManualRun    ] = createButton(QIcon("images/md-manual.png"), this, SLOT(setManualRunMode()));
	modeButton[ModeAutoCutRun   ] = createButton(QIcon("images/md-autocut.png"), this, SLOT(setAutoCutRunMode()));
	modeButton[ModeTraining     ] = createButton(QIcon("images/md-training.png"), this, SLOT(setTrainingMode()));

	for (int i = 0; i < MaxMajorModeNumber; i++) {
		modeButton[i]->setCheckable(true);
	}
}

/****************************************************************************/
/*   Top Page Widget */
void MainFrame::createTopPagesWidget()
{
	topPagesWidget = new QStackedWidget;

	// Initialize commonTopPage
	createCommonTopPage();

	// File Select Top Page
	fileSelectPage = new OpenFileDialog;
	connect(fileSelectPage, SIGNAL(programFileChanged()),
			this, SLOT(loadProgramFile()));

	topPagesWidget = new QStackedWidget;
	topPagesWidget->addWidget(commonTopPage);
	topPagesWidget->addWidget(fileSelectPage);

	topPagesWidget->setCurrentIndex(CommonTopPageIndex);
	topPagesWidget->setContentsMargins(0, 0, 0, 0);
}

/*
 *  Common Top Page
 */
void MainFrame::createCommonTopPage()
{
	// CommonTopPage Pointer
	commonTopPage = new QWidget;

	// Main Pages: mainPagesWidget
	createMainPagesWidget();

	// KeyPad Pages
	createKeypadPagesWidget();

	// Input Line Pages
	createInputLineWidget();

	// Function Button Pages on Right Side
	createFnButtonPagesWidget();

	QGridLayout *editorKeysLayout = new QGridLayout;
	editorKeysLayout->addWidget(mainPagesWidget, 0, 0, 1, 3);
	editorKeysLayout->addWidget(keypadPagesWidget, 0, 3, 1, 2);
	editorKeysLayout->setContentsMargins(0, 0, 0, 0);

	QVBoxLayout *middleLayout = new QVBoxLayout;
	middleLayout->addLayout(editorKeysLayout);
	middleLayout->addWidget(inputWidget);
	middleLayout->setContentsMargins(0, 0, 0, 0);

	QHBoxLayout *layout = new QHBoxLayout;
	layout->addLayout(middleLayout);
	layout->addWidget(fnButtonPagesWidget);
	layout->setContentsMargins(0, 0, 0, 0);

	commonTopPage->setLayout(layout);
	commonTopPage->setContentsMargins(0, 0, 0, 0);
}

void MainFrame::createMainPagesWidget()
{
	mainPagesWidget = new QStackedWidget;

	// First Page of mainPagesWidget
	programEditor = new ProgramEditor;

	connect(programEditor, SIGNAL(dataIndexChanged(int, int)),
			paperPusherBar, SLOT(updateProgramStep(int, int)));

	// Second Page of mainPagesWidget
	sensorStatusWidget = new SensorStatusWidget;
	sensorStatusWidget->setContentsMargins(0, 0, 0, 0);

	mainPagesWidget->addWidget(programEditor);
	mainPagesWidget->addWidget(sensorStatusWidget);

	mainPagesWidget->setCurrentIndex(DataListPageIndex);
	mainPagesWidget->setContentsMargins(0, 0, 0, 0);
}

void MainFrame::createFnButtonPagesWidget()
{
	fnButtonPagesWidget = new FnButtonsWidget;
	fnButtonPagesWidget->setCurrentIndex(0);

	connect(fnButtonPagesWidget, SIGNAL(clicked(int)),
			this, SLOT(fnButtonPressedEvent(int)));
}

void MainFrame::createMetersPage()
{
	metersWidget = new QWidget;

//	currentMeter = new CurrentMeter;
	speedMeter   = new SpeedMeter;
	manualModeKeyPadWidget = new ManualModeKeyPadWidget;

	connect(manualModeKeyPadWidget, SIGNAL(clicked(int)),
			this, SLOT(keyPadPressedEvent(int)));

	QVBoxLayout *layout = new QVBoxLayout;
//	layout->addWidget(currentMeter);
	layout->addWidget(speedMeter);
	layout->addWidget(manualModeKeyPadWidget);

	metersWidget->setLayout(layout);
}


// NOTE:Must be called behind programEditor is initialized
void MainFrame::createKeypadPagesWidget()
{
	keypadPagesWidget = new QStackedWidget;

	/*
	 * First KeyPad Widget Page
	 */
	editAutoModeKeyPadWidget = new EditAutoModeKeyPadWidget;

	connect(editAutoModeKeyPadWidget, SIGNAL(clicked(int)),
			this, SLOT(keyPadPressedEvent(int)));

	/*
	 * Second Page
	 */
	createMetersPage();

	keypadPagesWidget->addWidget(editAutoModeKeyPadWidget);
	keypadPagesWidget->addWidget(metersWidget);
	keypadPagesWidget->setCurrentIndex(EditKeyPadPageIndex);
	keypadPagesWidget->setContentsMargins(0, 0, 0, 0);

}

void MainFrame::createInputLineWidget()
{
//	qDebug() << "createInputLineWidget" << endl;

	inputWidget = new QWidget;

	inputGuideLabel = new QLabel;
	inputGuideLabel->setMinimumWidth(148);
	inputGuideLabel->setAlignment(Qt::AlignCenter);
	inputGuideLabel->setAutoFillBackground(true);
	QPalette palette;
	palette.setColor(QPalette::Window, QColor("yellow"));
	inputGuideLabel->setPalette(palette);

	inputBox = new LineEditVKeyboard;
	inputBox->setMaxLength(7); // xxxx.xx
	QRegExp rx("\\d{1,4}\\.{0,1}\\d{0,2}");
	QValidator *validator = new QRegExpValidator(rx, this);
	inputBox->setValidator(validator);
	inputBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);

	QFont inputFont = this->font();
	inputFont.setPointSize(gSysConfig->getDataListFontSize());
	inputBox->setFont(inputFont);

	inputBox->installEventFilter(this);

	inputBox->setKeyboardEnable(true);
	/* use this signal to move keyboard to right place */
	connect(inputBox, SIGNAL(vKeyboardShowEvent()),
			this, SLOT(handleVirtualKeyBoardShowEvent()));

	clearButton = createButton(QIcon("images/clear.png"), this, SLOT(clearInput()));

	inputButtonPage = new QWidget;
	okButton = createButton(QIcon("images/enter-48.png"), this, SLOT(enterInput()));

	QHBoxLayout *okLayout = new QHBoxLayout;
	okLayout->addWidget(okButton);
	okLayout->setContentsMargins(0, 0, 0, 0);
	inputButtonPage->setLayout(okLayout);

	runButtonPage = new QWidget;
	runButton = createButton(QIcon("images/run-64.png"), this, SLOT(run()));
	stopButton = createButton(QIcon("images/stop-64.png"), this, SLOT(stop()));

	QHBoxLayout *runLayout = new QHBoxLayout;
	runLayout->addWidget(runButton);
	runLayout->addWidget(stopButton);
	runLayout->setContentsMargins(0, 0, 0, 0);
	runButtonPage->setLayout(runLayout);

	inputButtonPagesWidget = new QStackedWidget;
	inputButtonPagesWidget->addWidget(inputButtonPage);
	inputButtonPagesWidget->addWidget(runButtonPage);
	inputButtonPagesWidget->setCurrentIndex(0);
	inputButtonPagesWidget->setContentsMargins(0, 0, 0, 0);

	currentModeLabel = new QLabel;
	currentModeLabel->setMinimumWidth(128);
	currentModeLabel->setAlignment(Qt::AlignCenter);
	currentModeLabel->setAutoFillBackground(true);
	palette.setColor(QPalette::Window, QColor("red"));
	currentModeLabel->setPalette(palette);

	QHBoxLayout *inputLayout = new QHBoxLayout;

	inputLayout->addWidget(inputGuideLabel);
	inputLayout->addWidget(inputBox);
	inputLayout->addStretch(1);
	inputLayout->addWidget(clearButton);
	inputLayout->addWidget(inputButtonPagesWidget);
	inputLayout->addStretch(1);
	inputLayout->addWidget(currentModeLabel);
	inputLayout->addStretch(1);
	inputLayout->setContentsMargins(0, 0, 0, 0);

	QVBoxLayout *mainLayout = new QVBoxLayout;
	mainLayout->addStretch(1);
	mainLayout->addLayout(inputLayout);

	inputWidget->setLayout(mainLayout);
	inputWidget->setContentsMargins(0, 0, 0, 0);
}

void MainFrame::createStatusBar()
{

	for (int i = 0; i < MaxStatusIcons; i++) {
		statusIconLabel[i] = new QLabel;
	}

	updateAutoCutStatus();
	updateSpeakerStatus(SpeakerOff);
	updateCutStatus(Motor::CutUp);
	updatePusherStatus(Motor::PusherUp);

	connect(gMotor, SIGNAL(cutStatusChanged(int)), this, SLOT(updateCutStatus(int)));
	connect(gMotor, SIGNAL(pusherStatusChanged(int)), this, SLOT(updatePusherStatus(int)));

	statusBarLabel = new QLabel;
	statusBarLabel->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	statusBarLabel->setAutoFillBackground(true);
	QPalette palette;
	palette.setColor(QPalette::Window, QColor("gray"));
	statusBarLabel->setPalette(palette);
}

/********************************************************************************/
/*
 * Helper function to create button
 */
Button *MainFrame::createButton(const QIcon &icon, const QObject *receiver, const char *member, const QString &text)
{
	Button *button = new Button;
	button->setIcon(icon);
	if (text != 0)
		button->setText(text);
    connect(button, SIGNAL(clicked()), receiver, member);
    button->setProperty("custom", true);
    return button;
}

/********************************************************************************/
/*
 * UI Messages
 */
QLabel *MainFrame::statusBar()
{
	return statusBarLabel;
}

void MainFrame::updateStatusBar()
{
	QString msg;

	switch ( _minorMode ) {
	case ModeProgramSelect:
		statusBar()->setText(tr("Select a program file or edit program description."));
		currentModeLabel->setText(tr("Program Select"));
		break;
	case ModeProgramEdit:
		msg = QString(tr("Input data between %1 - %2 [mm]")).arg(MData::toShow(gMotor->getActiveLimit())).arg(MData::toShow(gMotor->getBackLimit()));
		statusBar()->setText(msg);

		currentModeLabel->setText(tr("Edit"));
		break;
	case ModeAutoRun:
		statusBar()->setText(tr("Press RUN button to run this program automatically."));
		currentModeLabel->setText(tr("Auto Run"));
		break;
	case ModeManualRun:
		statusBar()->setText(tr("Input cut position and press RUN button."));
		currentModeLabel->setText(tr("Manual Run"));
		break;
	case ModeAutoCutRun:
		statusBar()->setText(tr("Dangerous!!! Pay care to the cut."));
		currentModeLabel->setText(tr("Auto Cut Run"));
		break;
	case ModeTraining:
		statusBar()->setText(tr("Run one data to add it to program."));
		currentModeLabel->setText(tr("Training"));
		break;
	default:
		statusBar()->setText(tr("Use MODE buttons on the left size to change mode."));
		currentModeLabel->setText(tr("Edit"));
		break;
	}
}

void MainFrame::retranslateUi()
{
	inputGuideLabel->setText(tr("Input Data:"));
	clearButton->setText(tr("Clear"));
	okButton->setText(tr("OK"));
	runButton->setText(tr("Run"));

	updateStatusBar();

	// Also update child widget UI here
	paperPusherBar->retranslateUi();
	programEditor->retranslateUi();
}

/********************************************************************************/
/*
 * Mode Change Functions
 */
void MainFrame::updateModeButtons()
{
#ifdef MAINFRAME_DEBUG
	qDebug() << "updateModeButtons: mode = " << _majorMode << endl;
#endif
	for (int i = 0; i < MaxMajorModeNumber; i++) {
		modeButton[i]->setChecked(_majorMode == i);
	}
}

void MainFrame::setStackedWidgetIndex(QStackedWidget *widget, int index)
{
	if ((index != UnusedPageIndex) || (index != UnchangedPageIndex)) {
		widget->setCurrentIndex(index);
	}
}

void MainFrame::setInputWidgetIndex(int index)
{
	// FIXME: iputWidget is a QWidget object, but NOT QStackedWidget.
	if ((index != UnusedPageIndex) || (index != UnchangedPageIndex)) {
		inputButtonPagesWidget->setCurrentIndex(index);
	}

	// Reset Input Widget, clear input data buffer
	inputBox->clear();

	// data can also be edit in AutoRun mode
#if 0
	// Disable InputBox for AutoRun and AutoCut mode
	if ((_majorMode == ModeAutoRun) || (_majorMode == ModeAutoCutRun)) {
		inputBox->setDisabled(true);
		clearButton->setDisabled(true);
	} else {
		inputBox->setDisabled(false);
		clearButton->setDisabled(false);
	}
#endif
}


// Don't change the major mode
void MainFrame::setDisplayMode(int mode)
{
	if ((mode >= MaxDisplayModeNumber)) {
		return;
	}

	_minorMode = mode;

#ifdef MAINFRAME_DEBUG
	qDebug() << "Change display mode to " << mode << endl;
#endif

	setStackedWidgetIndex(topPagesWidget, pageIndexTable[mode].topPage);
	setStackedWidgetIndex(mainPagesWidget, pageIndexTable[mode].mainPage);
	setStackedWidgetIndex(keypadPagesWidget, pageIndexTable[mode].keypadPage);
	setStackedWidgetIndex(fnButtonPagesWidget, pageIndexTable[mode].fnButtonPage);
	setInputWidgetIndex(pageIndexTable[mode].inputPage);

	updateStatusBar();
}

void MainFrame::setMode(int mode)
{

	if (mode >= MaxMajorModeNumber) {
		return;
	}

	_previousMajorMode  = _majorMode;
	_majorMode = mode;

#ifdef MAINFRAME_DEBUG
	qDebug() << "Change major mode from " << _previousMajorMode << " to "  << mode << endl;
#endif

	// By default, _minorMode == _majorMode
	setDisplayMode(mode);

	// Update Auto Cut Icon
	updateAutoCutStatus();

	// update Key Pad Mode
	if ((mode == ModeProgramEdit) || (mode == ModeTraining)) {
		editAutoModeKeyPadWidget->setKeyPadType(EditAutoModeKeyPadWidget::KP_Type_Edit);
	}

	// update Key Pad Mode
	if ((mode == ModeAutoRun) || (mode == ModeAutoCutRun)) {
		editAutoModeKeyPadWidget->setKeyPadType(EditAutoModeKeyPadWidget::KP_Type_AutoRun);
	}
}

void MainFrame::setProgramEditMode()
{
	setMode(ModeProgramEdit);

	// Always move to the first data
	programEditor->topIndex();

	// Focus to input box
	inputBox->setFocus();

	// Update mode buttons
	updateModeButtons();
}

void MainFrame::setAutoRunMode()
{
	setMode(ModeAutoRun);

	// Set index to the first data
	programEditor->topIndex();
//	programEditor->setFocus();

	// Update mode buttons
	updateModeButtons();
}

void MainFrame::setAutoCutRunMode()
{
	MessageBox msg;
	msg.setText(tr("<h2><font color=red>AutoCut mode is Dangerous!</font></h2>"));
	msg.setInformativeText(tr("Do you really want to enter AutoCut mode?"));
	msg.setIcon(QMessageBox::Warning);
	msg.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
	msg.setDefaultButton(QMessageBox::Cancel);

	int ret = msg.exec();
	switch(ret) {
	case QMessageBox::Ok:
#ifdef MAINFRAME_DEBUG
		qDebug() << "Enter autocut mode" << endl;
#endif
		setMode(ModeAutoCutRun);

		// Set index to the first data
		programEditor->topIndex();
//		programEditor->setFocus();
		break;
	case QMessageBox::Cancel:
	default:
		// Do nothing
		break;
	}

	// Update mode buttons
	updateModeButtons();
}

void MainFrame::setProgramSelectMode()
{
	// Don't change the major mode
	setDisplayMode(ModeProgramSelect);
}

void MainFrame::setManualRunMode()
{
	setMode(ModeManualRun);

	// Focus to input box
	inputBox->setFocus();

	// Update mode buttons
	updateModeButtons();
}

void MainFrame::setTrainingMode()
{
	setMode(ModeTraining);

	// Focus to input box
	inputBox->setFocus();

	// Update mode buttons
	updateModeButtons();
}

/********************************************************************************/


void MainFrame::updateFileInfo(QString &fileName)
{
	QFileInfo info(fileName);
	// Note: baseName() returns dataxx
	//       completeBaseName() returns dataxx.pro
	QString basename = info.baseName();
	paperPusherBar->updateProgramName(basename.remove("data"));

	// TODO: file description
	QString desc = programEditor->getCurrentProgram().description();
	headerLabel->updateProgramDescription(desc);
}

void MainFrame::openProgramFile(QString &fileName)
{
	programEditor->openProgramFile(fileName);

	// Update File Infomation
	updateFileInfo(fileName);

	// step shows the wrong number if program file is empty
	if(programEditor->getTotalDataNumber() <= 0) {
		paperPusherBar->updateProgramStep(0, 0);
	}
}

void MainFrame::loadProgramFile()
{
	QString fileName = fileSelectPage->getCurrentFileName();

	openProgramFile(fileName);
	setProgramEditMode();
}

void MainFrame::loadLastOpenFile()
{
	QString lastFile = gSysConfig->getLastOpenFile();

	if ( !lastFile.isEmpty() && lastFile.contains("pro", Qt::CaseInsensitive)) {
#ifdef MAINFRAME_DEBUG
	qDebug() << "Load file: " << lastFile.toAscii().data() << endl;
#endif
		openProgramFile(lastFile);
	}
}

// TODO: This is a dummy function
void MainFrame::dummySlot()
{

}

int  MainFrame::getInputData()
{
	QString input = inputBox->text();

	// clear input data
	inputBox->clear();

	return MData::toInt(input);
}


// Edit Mode Slots
/*
 * Insert one data to current index
 */
void MainFrame::insertData()
{
	int pos = getInputData();
	programEditor->insertData(pos);
}

/*
 * Modify current data
 */
void MainFrame::modifyData()
{
	int pos = getInputData();
	programEditor->modifyData(pos);
}

/*
 * Adjust all data in current file
 */
void MainFrame::adjustDataAdd()
{
	int delta = getInputData();
	programEditor->adjustPositionAll(delta);
}

/*
 * Adjust all data in current file
 */
void MainFrame::adjustDataSub()
{
	int delta = getInputData();
	programEditor->adjustPositionAll(-delta);
}

void MainFrame::deleteData()
{
	programEditor->deleteData();
}

void MainFrame::deleteAllData()
{
	MessageBox msg;
	msg.setText(tr("Delete all data?"));
	msg.setIcon(QMessageBox::Warning);
	msg.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
	msg.setDefaultButton(QMessageBox::Cancel);

	int ret = msg.exec();
	switch(ret) {
	case QMessageBox::Ok:
		programEditor->deleteAllData();
		break;
	case QMessageBox::Cancel:
	default:
		// Do nothing
		break;
	}
}

void MainFrame::save()
{
	programEditor->saveProgramFile();
}

void MainFrame::relativeAdd()
{
	int delta = getInputData();
	programEditor->relativeAppend(delta, ProgramEditor::RELATIVE_ADD);
}

void MainFrame::relativeSub()
{
	int delta = getInputData();
	programEditor->relativeAppend(delta, ProgramEditor::RELATIVE_SUB);
}

void MainFrame::relativeMultiply()
{
	int delta = getInputData();
	programEditor->relativeAppend(delta, ProgramEditor::RELATIVE_MUL);
}

void MainFrame::relativeDivide()
{
	int delta = getInputData();
	programEditor->relativeAppend(delta, ProgramEditor::RELATIVE_DIV);
}

void MainFrame::relativeInputAdd()
{
	int data = gMotor->pos() + getInputData();
	inputBox->setText(MData::toString(data));
}

void MainFrame::relativeInputSub()
{
	int data = gMotor->pos() - getInputData();

	if (data < 0)
		data = 0;
	inputBox->setText(MData::toString(data));
}

void MainFrame::relativeInputMultiply()
{
	int delta = getInputData();
	int data = (gMotor->pos() * delta) / 100;  // delta is 100 times larger by getInputData()

	inputBox->setText(MData::toString(data));
}

void MainFrame::relativeInputDivide()
{
	int delta = getInputData();
	int data;

	if (delta)
		data = gMotor->pos() * 100 / delta; // delta is 100 times larger by getInputData()

	inputBox->setText(MData::toString(data));
}

/*
 * Handle Enter button clicked() signal
 */
void MainFrame::enterInput()
{
	int data = getInputData();

	if ( gMotor->isPostionValid(data) ) {
//		qDebug() << "handleInputLineDataReady: major " << _majorMode << " minor " << _minorMode << endl;
		if ( _majorMode == ModeProgramEdit ) {
			programEditor->appendData(data);

			save();
		}

		// Clear input box
		clearInput();

		// Refresh status bar, since it may show the out-of-range error
		updateStatusBar();
	} else {
		// Data is not valid
		statusBar()->setText(tr("<font color=red>Inputed data is out of range.</font>"));
	}
}

/**
 * Toggle flag slots
 */
void MainFrame::toggleAirFlag()
{
	programEditor->toggleFlag(FlagEditor::FLAG_AIR);
}

void MainFrame::togglePusherFlag()
{
	programEditor->toggleFlag(FlagEditor::FLAG_PUSHER);
}

void MainFrame::toggleCutFlag()
{
	programEditor->toggleFlag(FlagEditor::FLAG_CUT);
}

void MainFrame::toggleAlignFlag()
{
	programEditor->toggleFlag(FlagEditor::FLAG_ALIGN);
}

void MainFrame::togglePressFlag()
{
	programEditor->toggleFlag(FlagEditor::FLAG_PRESS);
}

void MainFrame::toggleTurnAroundFlag()
{
	programEditor->toggleFlag(FlagEditor::FLAG_TURNAROUND);
}

void MainFrame::toggleTurnRightFlag()
{
	programEditor->toggleFlag(FlagEditor::FLAG_TURNRIGHT);
}

void MainFrame::toggleTurnLeftFlag()
{
	programEditor->toggleFlag(FlagEditor::FLAG_TURNLEFT);
}

void MainFrame::updateFontSizeColor()
{
	paperPusherBar->updateFontSizeColor();
}

void MainFrame::toggleSensorStatusImage()
{
	sensorStatusWidget->toggleImage();
}


/************************************************************************/
// Motor Control Related Slots
void MainFrame::runToNext()
{
#ifdef MAINFRAME_DEBUG
	qDebug() << "One cut loop finished!" << endl;
#endif

	int pos, flag;
	bool last = false;

	if ((_majorMode == ModeAutoRun) || (_majorMode == ModeAutoCutRun)) {
		// Move To Next
		programEditor->getNextData(&pos, &flag, &last);

		if (last) {
			// We have finished running the whole program, so stop
			stop();
		} else {
			// Continue next data
			runOneCut(pos, flag);
		}
	}

	if (_majorMode == ModeManualRun) {
		// Nothing To Do
	}

	if (_majorMode == ModeTraining) {
#ifdef MAINFRAME_DEBUG
		qDebug() << "Training Mode: append data " << _currentDestination << endl;
#endif
		// Add this data to list
		programEditor->appendData(_currentDestination);

		save();
	}
}

void MainFrame::run()
{
	switch(_majorMode) {
	case ModeManualRun:
		manualRunStart();
		break;
	case ModeAutoRun:
	case ModeAutoCutRun:
		autoRunStart();
		break;
	default:
		break;
	}
}

void MainFrame::stop()
{
	gMotor->stop();
}

void MainFrame::runToPos(int pos)
{
	gMotor->runToPos(pos);
}

void MainFrame::runOneCut(int pos, int flag)
{
#ifdef MAINFRAME_DEBUG
	qDebug() << "Running: pos: " << pos << " flag: " << flag << endl;
#endif

	gMotor->runOneCut(pos, flag);
}
/*
 * For Auto Run, we does:
 *   - Get data/flag from current index
 *   - Call runOneCut()
 *   - Handle the next data in runToNext() if one loop is finished
 *   - Move index to next and run
 *   - If reaches the end, jump to the first data in the list
 */
void MainFrame::autoRunStart()
{
#ifdef MAINFRAME_DEBUG
	qDebug() << "autoRunStart: mode " << _majorMode << endl;
#endif

	if ((_majorMode != ModeAutoCutRun) && (_majorMode != ModeAutoRun)) {
		return;
	}

	// Get Data
	int pos, flag;
	programEditor->getCurrentData(&pos, &flag);

	// Run
	runOneCut(pos, flag);

	// Update current destination
	_currentDestination = pos;
}

void MainFrame::manualRunStart()
{
	int pos = getInputData();

	// Air Flag is default
	runOneCut(pos, FlagEditor::FLAG_AIR);

	// Update current destination
	_currentDestination = pos;
}

// Manual Run Mode Slots
void MainFrame::machineSetting()
{
#if 0
	MachineSettingDialog dlg(this);
	dlg.exec();
#endif

	if ( !machineSettingDialog ) {
		machineSettingDialog = new MachineSettingDialog;
	}

	machineSettingDialog->raise();
	machineSettingDialog->exec();


#if 1 // FIXME: For TEST, do it unconditionally
	retranslateUi();

	// Change Font size and color
	updateFontSizeColor();
#endif
}

void MainFrame::runCalculator()
{
#if 0
	if ( !calculator ) {
		calculator = new Calculator;
	}

	// Clear last time result?
	calculator->clearAll();

	calculator->show();
	calculator->moveToCenter();

	// Set result to input box
	//QString result = calculator->getCalculatorResult();
	//inputBox->setText(result);
	calculator->setLineEdit(inputBox);
#else
	if ( !calculator ) {
		calculator = new Calculator;
		calculator->setLineEdit(inputBox);
	}

	// FIXME: How to get the x,y automatically
	calculator->move(390, 120);
	calculator->exec();
#endif

}

void MainFrame::runLabelEditor()
{
#ifdef MAINFRAME_DEBUG
	qDebug() << "Enter LabelEditor" << endl;
#endif
	LabelEditor dlg(this);
	//EditDialog dlg(this);

	dlg.move(370,175);
	dlg.exec();

	if (labelEditData.mask == LED_ALL_VALID) {
		programEditor->labelInsert(labelEditData.totalLength,
								   labelEditData.labelLength,
								   labelEditData.unusedLength);
	}
}

void MainFrame::runAverageEditor()
{
#ifdef MAINFRAME_DEBUG
	qDebug() << "Enter AverageEditor" << endl;
#endif
	AverageEditor dlg(this);
	dlg.move(360,170);
	dlg.exec();

	if (averageEditData.mask == AED_ALL_VALID) {
		programEditor->averageInsert(averageEditData.totalLength,
					averageEditData.averageNumber, averageEditData.mode);
	}
}

void MainFrame::runBaseSetup()
{
	BaseSetupDialog dlg(this);
	dlg.exec();
}

void MainFrame::handleVirtualKeyBoardShowEvent()
{
	/* Move virtual keyboard to a good place */
	gVKeyboard->move(485, 195);
}

// AutoCut, Cut, Pusher and Speaker status slots
/*
 * Cut and Pusher status are received from Motor
 * AutoCut and Speaker status are from this program
 */
void MainFrame::updateCutStatus(int status)
{
	if (status == Motor::CutDown) {
		statusIconLabel[CutStatusIcon]->setPixmap(QPixmap("images/cut-down.bmp"));
	} else if (status == Motor::CutUp) {
		statusIconLabel[CutStatusIcon]->setPixmap(QPixmap("images/cut-up.bmp"));
	}
}

void MainFrame::updatePusherStatus(int status)
{
	if (status == Motor::PusherDown) {
		statusIconLabel[PusherStatusIcon]->setPixmap(QPixmap("images/pusher-down.bmp"));
	} else if (status == Motor::PusherUp) {
		statusIconLabel[PusherStatusIcon]->setPixmap(QPixmap("images/pusher-up.bmp"));
	}
}

void MainFrame::updateAutoCutStatus()
{
	bool isAutoCutMode = (_majorMode == ModeAutoCutRun);

	if (isAutoCutMode) {
		statusIconLabel[AutoCutStatusIcon]->setPixmap(QPixmap("images/cut-down.bmp"));
	} else {
		statusIconLabel[AutoCutStatusIcon]->setPixmap(QPixmap("images/cut-up.bmp"));
	}
}

void MainFrame::updateSpeakerStatus(int status)
{
	if (status == SpeakerOff) {
		statusIconLabel[SpeakerStatusIcon]->setPixmap(QPixmap("images/beeper-silent.bmp"));
	} else if (status == SpeakerOn) {
		statusIconLabel[SpeakerStatusIcon]->setPixmap(QPixmap("images/beeper-sound.bmp"));
	}
}

void MainFrame::clearCutCount()
{
	gMotor->setCutCount(0);
	paperPusherBar->updateCutCount(0);
}

void MainFrame::clearInput()
{
	inputBox->clear();
}

void MainFrame::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    p.fillRect(rect(), Qt::black);

    // Construct a rect r in the topleft corner with
    // the size of the pixmap
    QRect r(QPoint(0, 0), bkgndPixmap.size());

    // Let r's center be the center of the widget and draw the
    // pixmap there
    r.moveCenter(rect().center());

    p.drawPixmap(r, bkgndPixmap);

	QWidget::paintEvent(event);
}

/********************************************************************************/
/*
 * key pad Event handler
 */
void MainFrame::keyPadPressedEvent(int key_code)
{
#ifdef MAINFRAME_DEBUG
	qDebug() << "Button ID " << key_code << endl;
#endif

	switch (key_code) {
	case SmtKb::KP_DeleteOneCut:
		statusBar()->setText(tr("Delete current data"));
		deleteData();
		break;
	case SmtKb::KP_DeleteFlags:
		statusBar()->setText(tr("Delete all flags of current data"));
		programEditor->clearFlags();
		break;
	case SmtKb::KP_Insert:
		statusBar()->setText(tr("Insert one data to current index"));
		insertData();
		break;
	case SmtKb::KP_Modify:
		statusBar()->setText(tr("Modify current data"));
		modifyData();
		break;
	case SmtKb::KP_ToggleTurnLeftFlag:
		statusBar()->setText(tr("Toggle Turn Left flag"));
		toggleTurnLeftFlag();
		break;
	case SmtKb::KP_ToggleTurnRightFlag:
		statusBar()->setText(tr("Toggle Turn Right flag"));
		toggleTurnRightFlag();
		break;
	case SmtKb::KP_ToggleTurnAroundFlag:
		statusBar()->setText(tr("Toggle Turn Around flag"));
		toggleTurnAroundFlag();
		break;
	case SmtKb::KP_DeleteAll:
		statusBar()->setText(tr("Delete all data"));
		deleteAllData();
		break;
	case SmtKb::KP_TogglePressFlag:
		statusBar()->setText(tr("Toggle Press flag"));
		togglePressFlag();
		break;
	case SmtKb::KP_TogglePusherFlag:
		statusBar()->setText(tr("Toggle Pusher flag"));
		//		togglePusherFlag();
		toggleAlignFlag();
		break;
	case SmtKb::KP_ToggleAirFlag:
		statusBar()->setText(tr("Toggle Air flag"));
		toggleAirFlag();
		break;
	case SmtKb::KP_ToggleAutoCutFlag:
		statusBar()->setText(tr("Toggle Auto Cut flag"));
		// AutoCut Flag is cut flag
		toggleCutFlag();
		break;
	case SmtKb::KP_RelativeAdd:
		statusBar()->setText(tr("Relative add append data"));
		relativeAdd();
		break;
	case SmtKb::KP_RelativeSub:
		statusBar()->setText(tr("Relative sub append data"));
		relativeSub();
		break;
	case SmtKb::KP_RelativeMultiply:
		statusBar()->setText(tr("Relative multiply append data"));
		relativeMultiply();
		break;
	case SmtKb::KP_RelativeDivide:
		statusBar()->setText(tr("Relative divide append data"));
		relativeDivide();
		break;
	case SmtKb::KP_AdjustAddAll:
		statusBar()->setText(tr("Adjust add all data from current index"));
		adjustDataAdd();
		break;
	case SmtKb::KP_AdjustSubAll:
		statusBar()->setText(tr("Adjust sub all data from current index"));
		adjustDataSub();
		break;
	case SmtKb::KP_RelativeInputAdd:
		statusBar()->setText(tr("Relative add input data"));
		relativeInputAdd();
		break;
	case SmtKb::KP_RelativeInputSub:
		statusBar()->setText(tr("Relative sub input data"));
		relativeInputSub();
		break;
	case SmtKb::KP_RelativeInputMultiply:
		statusBar()->setText(tr("Relative multiply input data"));
		relativeInputMultiply();
		break;
	case SmtKb::KP_RelativeInputDivide:
		statusBar()->setText(tr("Relative divide input data"));
		relativeInputDivide();
		break;
	case SmtKb::KP_ZoomIn:
		statusBar()->setText(tr("Zoom in data list font"));
		programEditor->zoomInTableFont();
		break;
	case SmtKb::KP_ZoomOut:
		statusBar()->setText(tr("Zoom out data list font"));
		programEditor->zoomOutTableFont();
		break;
	case SmtKb::KP_Hide:
		qDebug() << "Hide keypad";
		break;
	default:
		break;
	}
}

void MainFrame::fnButtonPressedEvent(int key_code)
{
#ifdef MAINFRAME_DEBUG
	qDebug() << "Button ID " << key_code << endl;
#endif

	switch (key_code) {
	case SmtKb::Key_F1:
		handleKeyPressF1();
		break;
	case SmtKb::Key_F2:
		handleKeyPressF2();
		break;
	case SmtKb::Key_F3:
		handleKeyPressF3();
		break;
	case SmtKb::Key_F4:
		handleKeyPressF4();
		break;
	case SmtKb::Key_F5:
		handleKeyPressF5();
		break;
	case SmtKb::Key_F6:
		handleKeyPressF6();
		break;
	default:
		break;
	}
}


/********************************************************************************/
/*
 * Keyboard Event handler
 */
void MainFrame::handleKeyPressF1()
{
	switch (_majorMode) {
	case ModeProgramEdit:
	case ModeTraining:
		runLabelEditor();
		break;
	case ModeAutoRun:
	case ModeAutoCutRun:
		clearCutCount();
		break;
	case ModeManualRun:
		machineSetting();
		break;
	default:
		break;
	}
}

void MainFrame::handleKeyPressF2()
{
	switch (_majorMode) {
	case ModeProgramEdit:
	case ModeTraining:
		runAverageEditor();
		break;
	case ModeAutoRun:
	case ModeAutoCutRun:
		runBaseSetup();
		break;
	case ModeManualRun:
		// TODO
		break;
	default:
		break;
	}
}

void MainFrame::handleKeyPressF3()
{
	switch (_majorMode) {
	case ModeProgramEdit:
	case ModeTraining:
	case ModeAutoCutRun:
	case ModeAutoRun:
		setProgramSelectMode();
		break;
	case ModeManualRun:
		runBaseSetup();
		break;
	default:
		break;
	}
}

void MainFrame::handleKeyPressF4()
{
	switch (_majorMode) {
	case ModeProgramEdit:
	case ModeTraining:
		togglePusherFlag();
		break;
	case ModeManualRun:
		clearCutCount();
		break;
	default:
		break;
	}
}

void MainFrame::handleKeyPressF5()
{
	switch (_majorMode) {
	case ModeProgramEdit:
	case ModeTraining:
		toggleCutFlag();
		break;
	case ModeManualRun:
		sensorStatusWidget->toggleImage();
		break;
	default:
		break;
	}
}

void MainFrame::handleKeyPressF6()
{
	switch (_majorMode) {
	case ModeProgramEdit:
	case ModeTraining:
	case ModeManualRun:
		runCalculator();
		break;
	default:
		break;
	}
}

// Keyboard Events
void MainFrame::keyPressEvent(QKeyEvent *event)
{
#if 1 //def MAINFRAME_DEBUG
	qDebug() << "Key Pressed: " << hex << event->key();
#endif

	switch(event->key()) {
	case SmtKb::Key_Enter:
	case Qt::Key_Enter:
		enterInput();
		break;
	case SmtKb::Key_Clear:
		clearInput();
		break;
	case SmtKb::Key_F1:
		handleKeyPressF1();
		break;
	case SmtKb::Key_F2:
		handleKeyPressF2();
		break;
	case SmtKb::Key_F3:
		handleKeyPressF3();
		break;
	case SmtKb::Key_F4:
		handleKeyPressF4();
		break;
	case SmtKb::Key_F5:
		handleKeyPressF5();
		break;
	case SmtKb::Key_F6:
		handleKeyPressF6();
		break;
	case SmtKb::Key_Edit:
		setProgramEditMode();
		break;
	case SmtKb::Key_Auto:
		setAutoRunMode();
		break;
	case SmtKb::Key_AutoCut:
		setAutoCutRunMode();
		break;
	case SmtKb::Key_Manual:
		setManualRunMode();
		break;
	case SmtKb::Key_Traning:
		setTrainingMode();
	case SmtKb::Key_Run:
		run();
		break;
	case SmtKb::Key_Stop:
		stop();
		break;
	case SmtKb::Key_Forward:
		// TODO
		break;
	case SmtKb::Key_Backward:
		// TODO
		break;
	case SmtKb::Key_FastForward:
		// TODO
		break;
	case SmtKb::Key_FastBackward:
		// TODO
		break;
	default:
		QWidget::keyPressEvent(event);
	}
}

bool MainFrame::eventFilter(QObject *target, QEvent *event)
{
	/**
	 * In edit mode, focus is always in inputBox, and we want to move the cursor
	 * of program data table view even when it has no focus. So that we have to
	 * filter the key press event to programEditor.
	 */
	if (target == inputBox) {
		if (event->type() == QEvent::KeyPress) {
			QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);

			qDebug() << "eventFileter(): Key Pressed: " << hex << keyEvent->key();

			switch (keyEvent->key()) {
			case SmtKb::Key_Up:
			case SmtKb::Key_Down:
			case SmtKb::Key_PageUp:
			case SmtKb::Key_PageDown:
				programEditor->forceKeyPressEvent(keyEvent);
				return false;
			case SmtKb::Key_Delete:
				deleteData();
				return false;

			default:
				// Do nothing
				break;
			}
		}
	}
	return QObject::eventFilter(target, event);
}

void MainFrame::loadStyleSheet(const QString & fileName)
{
	if ( !fileName.isEmpty() ) {
		QFile file(fileName);
		file.open(QFile::ReadOnly);
		QString styleSheet = QLatin1String(file.readAll());

		this->setStyleSheet(styleSheet);

		file.close();
	}
}
