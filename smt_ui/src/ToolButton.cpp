/*
 * ToolButton.cpp
 *
 *  Created on: Sep 12, 2010
 *      Author: souken-i
 */

#include "ToolButton.h"
#include "Global.h"

ToolButton::ToolButton(QWidget *parent)
	: QToolButton(parent)
{
}

ToolButton::~ToolButton()
{
}

void ToolButton::mousePressEvent ( QMouseEvent * event )
{
	gBeeper->setBeeperOn();

	QToolButton::mousePressEvent(event);
}

void ToolButton::mouseReleaseEvent ( QMouseEvent * event )
{
	gBeeper->setBeeperOff();

	QToolButton::mouseReleaseEvent(event);
}
