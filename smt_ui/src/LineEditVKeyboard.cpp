/*
 * LineEditVKeyboard.cpp
 *
 *  Created on: 2010/06/08
 *      Author: kinhi
 */

#include "include/LineEditVKeyboard.h"
#include "Global.h"

LineEditVKeyboard::LineEditVKeyboard(QWidget * parent)
	: QLineEdit(parent)
{
	_keyboardEnable = false;
}

LineEditVKeyboard::~LineEditVKeyboard()
{
}

void LineEditVKeyboard::mousePressEvent ( QMouseEvent * event )
{
	if (_keyboardEnable) {
		/* Set keyboard to number pad mode */
		gVKeyboard->setMode(VirtualKeyboard::MODE_NUMPAD);
		gVKeyboard->toggleVisibility();
		if (gVKeyboard->isVisible()) {
//			qDebug() << "gVKeyboard visible";
			emit vKeyboardShowEvent();
		}
	}

	QLineEdit::mousePressEvent ( event );
}

void LineEditVKeyboard::focusOutEvent ( QFocusEvent * event )
{
	/* hide keyboard when lose focus */
	gVKeyboard->hide();

	QLineEdit::focusOutEvent(event);
}
