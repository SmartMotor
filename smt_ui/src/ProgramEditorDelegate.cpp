/*
 * FlagDelegate.cpp
 *
 *  Created on: 2010/04/01
 *      Author: crazysjf
 */

#include "ProgramEditorDelegate.h"
#include "ProgramEditor.h"
#include "MData.h"

ProgramEditorDelegate::~ProgramEditorDelegate() {
	// TODO Auto-generated destructor stub
}

void ProgramEditorDelegate::setEditorData(QWidget *editor, const QModelIndex & index) const
{
	QStyledItemDelegate::setEditorData(editor, index);
}



void ProgramEditorDelegate::paint(QPainter *painter, const QStyleOptionViewItem & option, const QModelIndex & index) const
{
	static QString RightPadding("  ");

	if (index.column() == _positionColumn){
		painter->drawText(option.rect, Qt::AlignRight, MData::toString(index.data().toInt()) + RightPadding);
	} else if (index.column() == _flagsColumn) {
		int f = index.data().toInt();
		FlagEditor::paint(painter, option.rect, option.palette, f);
	} else {
		QStyledItemDelegate::paint(painter, option, index);
	}

}


QWidget *ProgramEditorDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem & option, const QModelIndex & index) const
{
	return QStyledItemDelegate::createEditor(parent, option, index);

}



QSize ProgramEditorDelegate::sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index) const
{
	return QStyledItemDelegate::sizeHint(option, index);
}



void ProgramEditorDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex & index) const
{
	QStyledItemDelegate::setModelData(editor, model, index);

}

void ProgramEditorDelegate::commitAndCloseEditor()
{

}
