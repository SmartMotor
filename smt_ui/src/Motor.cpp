/*
 * Motor.cpp
 *
 *  Created on: Mar 29, 2010
 *      Author: souken-i
 */

#include "Motor.h"
#include "Global.h"

//#define MOTOR_DEBUG

Motor::Motor()
{
	_enableLimit = true;
	whichLimit   = FrontLimitActive;
	cutState     = CutUp;
	pusherState  = PusherUp;
	alarm        = 0;
	currentLevel = CurrentLevelGreen;
	_cutCount    = 0;
	motorCurrent = 10; // Assume
	currentSpeed = 400;

#ifdef START_DEBUG
	qDebug() << "Start motor control protocol ..." << endl;
#endif

	mcp = new MotorControlProtocol("/dev/ttyS0");
	connect(mcp, SIGNAL(statusEvents(int, int)), this, SLOT(handleStatusEvent(int, int)));
	connect(mcp, SIGNAL(positionEvent(int)), this, SLOT(handlePositionEvent(int)));
	connect(mcp, SIGNAL(speedEvent(int)), this, SLOT(handleSpeedEvent(int)));

#ifdef START_DEBUG
	qDebug() << "Start motor log" << endl;
#endif

	// Set Motor Log
	// FIXME: Motor is a global object and it is initialized before main(),
	// FIXME: So at this time, currentUserDir() is not available;
	// FIXME: Thus, we must call postInitialize() to do this again.
	motorLog = new MotorLog;

#ifdef START_DEBUG
	qDebug() << "Motor() done" << endl;
#endif
}

Motor::~Motor()
{
	// FIXME Should we delete mcp here or it is
	// FIXME deleted automatically?
	delete mcp;
}

//
void Motor::postInitialize()
{
	QString logFile = System::currentUserDir() + QString("motor.log");
	motorLog->setLogFileName(logFile);
}

bool Motor::isMotorPresent()
{
	return mcp->isMotorPresent();
}

void Motor::handlePositionEvent(int pos)
{
	// TODO: update pos
	// TODO: We could also connect the signal to GUI, and only
	// TODO: set the position here
	setPos(pos);
	emit currentPosChanged(pos);
}

void Motor::handleStatusEvent(int status, int mask)
{
#ifdef MOTOR_DEBUG
	qDebug() << "handleStatusEvent " << status << endl;
#endif

	// TODO
	// These parts are still not clear.
	if (MotorControlProtocol::isFrontLimitMaskChanged(mask)) {

	}

	if (MotorControlProtocol::isMiddleLimitMaskChanged(mask)) {

	}

	if (MotorControlProtocol::isBackLimitMaskChanged(mask)) {

	}

	if (MotorControlProtocol::isFrontLimitStatusChanged(mask)) {

	}

	if (MotorControlProtocol::isMiddleLimitStatusChanged(mask)) {

	}

	if (MotorControlProtocol::isBackLimitStatusChanged(mask)) {

	}


	if (MotorControlProtocol::isLimitSelectionChanged(mask)) {
		whichLimit = (MotorControlProtocol::getWhichLimit(status) ? MiddleLimitActive : FrontLimitActive);
	}

	// 1: cut up 0: cut down
	if (MotorControlProtocol::isCutStatusChanged(mask)) {
		cutState = (MotorControlProtocol::getCutStatus(status) ? CutUp : CutDown);
		emit cutStatusChanged(cutState);
		updateCutStatus();
	}

	// 1: pusher down 0: pusher up
	if (MotorControlProtocol::isPusherStatusChanged(mask)) {
		pusherState = (MotorControlProtocol::getPusherStatus(status) ? PusherDown : PusherUp);
		emit pusherStatusChanged(pusherState);
		updatePusherStatus();
		if (isOneLoopDone == true) {
			// NOTE this flag will be cleared until next loop

			// Clear this flag here. If not, we can test it
			isOneLoopDone = false;
			emit oneCutLoopFinished();
		}
	}

	if (MotorControlProtocol::isAlarmStatusChanged(mask)) {
		alarm = MotorControlProtocol::getAlarmCode(status);
		emit alarmStatusChanged(alarm);

#ifdef MOTOR_DEBUG
		qDebug() << "emit alarmStatusChanged " << alarm << " @ " << pos() << endl;
#endif
		if (alarm) {
			// alarm > 0: alarm happens
			// alarm = 0: alarm is cleared
			motorLog->appendAlarmLog(alarm, pos());
		}
	}

	if (MotorControlProtocol::isPitchChanged(mask)) {
		pitch = MotorControlProtocol::getPitch(status);
	}

	if (MotorControlProtocol::isDirectionChanged(mask)) {
		direction = MotorControlProtocol::getDirection(status) ? DirectionBackward : DirectionForward;
	}

	if (MotorControlProtocol::isCurrentValueChanged(mask)) {
		int current = MotorControlProtocol::getCurrent(status);
		if (current != motorCurrent) {
			motorCurrent = current;
#ifdef MOTOR_DEBUG
		qDebug() << "emit currentChanged: " << motorCurrent << endl;
#endif
			emit currentChanged(motorCurrent);
		}
	}
}

void Motor::handleSpeedEvent(int speed)
{
	if (currentSpeed != speed) {
		currentSpeed = speed;
		emit speedChanged(currentSpeed);
	}
}

void Motor::start()
{
	int pitch = getPitch();
	setPitch((PitchTypes)pitch);

	setPos(getInitialPos());

	// TODO: Should we do zerorize here
	setZeroPos();
}

void Motor::stop()
{
	MotorCommand cmd;
	cmd.commandJogRun(MotorCommand::JogRunStop, 0);

	mcp->sendCommand(cmd);
}

void Motor::setZeroPos()
{
	int pos = getInitialPos();

	MotorCommand cmd;
	cmd.commandSetZeroPos(pos);

	mcp->sendCommand(cmd);
}

void Motor::setCurPos(int pos)
{
	MotorCommand cmd;
	cmd.commandSetCurPos(pos);

	mcp->sendCommand(cmd);
}

void Motor::runToPos(int pos)
{
	currentDestination = pos;

	MotorCommand cmd;
	cmd.commandRunToPos(pos);

	mcp->sendCommand(cmd);
}

void Motor::runForward()
{
	int speed = getRunSpeed();
	MotorCommand cmd;
	cmd.commandJogRun(MotorCommand::JogRunForward, speed);

	mcp->sendCommand(cmd);
}

void Motor::runBackward()
{
	int speed = getRunSpeed();
	MotorCommand cmd;
	cmd.commandJogRun(MotorCommand::JogRunBackward, speed);

	mcp->sendCommand(cmd);
}

void Motor::setPitch(PitchTypes value)
{
	int data = pitchTypeToValue(value);
	MotorCommand cmd;
	cmd.commandSetPitch(data);

	mcp->sendCommand(cmd);
}

void Motor::adjustCurrentPosition(int pos)
{
	MotorCommand cmd;
	cmd.commandSetCurPos(pos);

	mcp->sendCommand(cmd);
}

void Motor::setPusherSpeed(int speed)
{
	int data = speedMPerSecondToRPM(speed);

	MotorCommand cmd;
	cmd.commandSetPusherSpeed(data);

	mcp->sendCommand(cmd);
}

void Motor::setRelay(int data)
{
	MotorCommand cmd;
	cmd.commandSetRelay(data & MotorCommand::RelayMask);

	mcp->sendCommand(cmd);
}

void Motor::disableFrontLimit()
{
	MotorCommand cmd;
	cmd.commandSetLimitMask(MotorCommand::DisableFrontLimit);

	mcp->sendCommand(cmd);
}

void Motor::disableMiddleLimit()
{
	MotorCommand cmd;
	cmd.commandSetLimitMask(MotorCommand::DisableMiddleLimit);

	mcp->sendCommand(cmd);
}

void Motor::disableBackLimit()
{
	MotorCommand cmd;
	cmd.commandSetLimitMask(MotorCommand::DisableBackLimit);

	mcp->sendCommand(cmd);
}

void Motor::disablePallet()
{
	MotorCommand cmd;
	cmd.commandSetLimitMask(MotorCommand::DisablePallet);

	mcp->sendCommand(cmd);
}

void Motor::enableAllLimits()
{
	MotorCommand cmd;
	cmd.commandSetLimitMask(MotorCommand::EnableAllLimits);

	mcp->sendCommand(cmd);
}

void Motor::startCodec()
{
	MotorCommand cmd;
	cmd.commandCheckCodec(MotorCommand::StartCodec);

	mcp->sendCommand(cmd);
}

void Motor::stopCodec()
{
	MotorCommand cmd;
	cmd.commandCheckCodec(MotorCommand::StopCodec);

	mcp->sendCommand(cmd);
}

void Motor::resendLastCommand()
{
	mcp->resendLastCommand();
}

void Motor::runOneCut(int pos, int flag)
{
	currentDestination = pos;
	currentDataFlag = flag;
	pusherActionState = PusherUp;
	cutActionState = CutUp;
	isOneLoopDone = false;

	// TODO FIXME run
}

void Motor::updatePusherStatus()
{
	// TODO Also check the destination and current position
	if ((pusherState == PusherDown) && (cutState == CutUp)) {
		/* Pusher down and Cut is up*/
		pusherActionState = PusherUpToDown;
	}

	if ((pusherState == PusherUp) && (pusherActionState == PusherUpToDown)) {
		pusherActionState = PusherDownToUp;
		bool cutDone = ((cutState == CutUp) && (cutActionState == CutDownToUp));
		if (cutDone || isPusherOnly()) {
			isOneLoopDone = true;
		}
	}
}

void Motor::updateCutStatus()
{
	// TODO Also check the destination and current position
	if ((cutState == CutDown) && (pusherState == PusherDown)) {
		cutActionState = CutUpToDown;
	}

	if ((cutState == CutUp) && (pusherState == PusherDown)) {
		if (cutActionState == CutUpToDown) {
			cutActionState = CutDownToUp;

			// Increase cut count
			_cutCount++;
			emit cutCountChanged(_cutCount);
		}
	}
}
bool Motor::isPusherOnly()
{
	return ((currentDataFlag & AutoPushEnable) == AutoPushEnable);
}

void Motor::enableLimit(bool state)
{
	_enableLimit = state;
}

bool Motor::isLimitEnabled()
{
	return _enableLimit;
}

int  Motor::getActiveLimit()
{
	if (whichLimit == FrontLimitActive) {
		return getFrontLimit();
	} else {
		return getMiddleLimit();
	}
}

bool Motor::isPostionValid(int pos)
{
	if (_enableLimit == false) {
		// No limitation
		return true;
	}

	int minLimit, maxLimit;
	if (whichLimit == FrontLimitActive) {
		minLimit = getFrontLimit();
	} else {
		minLimit = getMiddleLimit();
	}

	maxLimit = getBackLimit();

	if ((pos >= minLimit) && (pos <= maxLimit)) {
		return true;
	}

	return false;
}

int Motor::currentOverloadLevel(int value)
{
	if (value <= 10)
		return CurrentLevelGreen;
	else if (value >= 15)
		return CurrentLevelRed;
	else
		return CurrentLevelYellow;
}

int  Motor::getCutCount()
{
	return _cutCount;
}

void Motor::setCutCount(int count)
{
	_cutCount = count;
}

int Motor::getMotorCurrent()
{
	return motorCurrent;
}

int Motor::getSpeed()
{
	return currentSpeed;
}

void Motor::motorTest()
{
	setPitch(Pitch_10MM_2Rate);
	setZeroPos();
	runToPos(1000);
}

