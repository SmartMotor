/*
 * TableModel.cpp
 *
 *  Created on: 2010/11/08
 *      Author: kinhi
 *
 */

#include "TableModel.h"

QDebug operator<<(QDebug dbg, const Row &data)
{
	dbg.nospace() << "[";
	for (int i=0; i < COLUMN_NUM; i++)
		dbg.nospace() << data.columns[i]<< " ";
	dbg.nospace() << "]";
	return dbg.nospace();
}


int TableModel::rowCount(const QModelIndex &/*parent*/) const
{
	return _data.size();
}

int TableModel::columnCount (const QModelIndex &/*parent*/) const
{
	return _columnNum;
}

QVariant TableModel::data ( const QModelIndex & index, int role) const
{
	if (!index.isValid()) {
		return QVariant();
	}
	if (index.row() >= _data.size()) {
		return QVariant();
	}
	if (role == Qt::DisplayRole || role == Qt::EditRole) {
		return QString("%1").arg(_data[index.row()].columns[index.column()]);

	} else
		return QVariant();
}

Qt::ItemFlags TableModel::flags(const QModelIndex &index) const
{
     if (!index.isValid())
         return Qt::ItemIsEnabled;

     return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

bool TableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	int i = index.isValid();
	if (!i)
		qDebug() << "index is not valid";
     if (index.isValid() && (role == Qt::EditRole || role == Qt::DisplayRole)) {
         _data[index.row()].columns[index.column()] = value.toInt();
         emit dataChanged(index, index);
         qDebug() << "in setData()";
         qDebug() << _data;
         return true;
     }

     return false;
}

QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal) {
		if (section >= headerLabels.size())
			return QVariant();
		return headerLabels[section];
	}
	else
		return QString("%1").arg(section + 1);

}

bool TableModel::insertRow (int row, const QModelIndex &/*parent*/)
{
	if (row <= _data.size()) {
		Row newRow;
		newRow.columns[0] = 0;
		newRow.columns[1] = 0;
		_data.insert(row, newRow);
		qDebug() << "data:" << _data;
		return true;
	}

	return false;
}

bool TableModel::insertRow (int row, const QList<int> &rowData)
{
	Row newRow;

	beginInsertRows(QModelIndex(), row, row);

	for(int i=0; i < rowData.size() && i < COLUMN_NUM; i++) {
		newRow.columns[i] = rowData[i];
	}
	_data.insert(row, newRow);
	qDebug() << "data:" << _data;
	endInsertRows();

	return 1;
}

bool TableModel::insertRows(int position, int rowNum)
{
	beginInsertRows(QModelIndex(), position, position+rowNum-1);

	Row newRow;
	for (int row = 0; row < rowNum; ++row) {
		_data.insert(position, newRow);
	}

	endInsertRows();

	return true;
}

bool TableModel::insertRows(int position, const QList<Row> rows)
{
	beginInsertRows(QModelIndex(), position, position+rows.size() - 1);

	_data += rows;
	endInsertRows();

	return true;
}

bool TableModel::removeRows(int position, int rows)
{
	beginRemoveRows(QModelIndex(), position, position+rows-1);

	for (int row = 0; row < rows; ++row) {
		_data.removeAt(position);
	}

	endRemoveRows();
	return true;
}
