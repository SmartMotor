/*
 * MotorCommand.cpp
 *
 *  Created on: Mar 30, 2010
 *      Author: souken-i
 */

#include <stdio.h>  /* For snprintf() */

#include "MotorCommand.h"
#include "MotorControlProtocol.h"

char MotorCommand::CommandHeaders[MotorCommand::MC_CommandNumbers+1] = "ZRPGJMKSC";

MotorCommand::MotorCommand()
{
	_frame.resize(SerialPortFrame::TxFrameDataLength);
	_frame.fill(0);
}

MotorCommand::MotorCommand(int type, int address, int data)
{
	_frame.resize(SerialPortFrame::TxFrameDataLength);
	_frame.fill(0);

	toCommand(_frame, type, address, data);
}

MotorCommand::~MotorCommand()
{
}

/*
 * Command string is saved to QByteArray buffer;
 * The buffer must be allocated by caller.
 */
void MotorCommand::toCommand(QByteArray & buffer, int type, int address, int data)
{
	if (buffer.size() != SerialPortFrame::TxFrameDataLength) {
		buffer.resize(SerialPortFrame::TxFrameDataLength);
	}

	buffer.fill(0);
	buffer.data()[0] = CommandHeaders[type];
	buffer.data()[1] = toChar(address >> 4);
	buffer.data()[2] = toChar(address);
	::snprintf(&(buffer.data()[3]), 6, "%06d", data);

#if 0  // For debug
	::printf("CMD: type %c addr %d data %d -> %s \n", CommandHeaders[type],
				address, data, buffer.data());
#endif
}

void MotorCommand::toCommand(QByteArray & buffer, CommandFrameData *cfd)
{
	toCommand(buffer, cfd->flag, cfd->address, cfd->data);
}

QByteArray & MotorCommand::frame()
{
	return _frame;
}

void MotorCommand::setCommand(int type, int address, int data)
{
	toCommand(_frame, type, address, data);
}

void MotorCommand::commandSetPitch(int data)
{
	// TODO data should be handled
	setCommand(MC_SetPitch, MCADDR_SetPitch, data);
}

void MotorCommand::commandSetZeroPos(int pos)
{
	setCommand(MC_SetZeroPos, MCADDR_SetZeroPos, pos);
}

void MotorCommand::commandSetCurPos(int pos)
{
	setCommand(MC_SetCurPos, MCADDR_SetCurPos, pos);
}

void MotorCommand::commandRunToPos(int pos)
{
	setCommand(MC_RunToPos, MCADDR_RunToPos, pos);
}

void MotorCommand::commandJogRun(JogRunTypes type, int speed)
{
	// For JogRunStop, speed will be ignored
	setCommand(MC_JogRun, type, speed);
}

void MotorCommand::commandSetStopMode(StopModes mode)
{
	// The data is ignored by this command
	setCommand(MC_SetStopMode, mode, 0);
}

/*
 * Relay Status is in format:
 *   bit 0 : Relay 1 status
 *   bit 1 : Relay 2 status
 *   bit 2 : Relay 3 status
 *   bit 3 : Relay 4 status
 * 4 relays are supported
 */
void MotorCommand::commandSetRelay(int relayStatus)
{
	// TODO FIX ME: it is ugly
	int data = 0;

	if (relayStatus & Relay1Mask) {
		data += 1;
	}
	if (relayStatus & Relay2Mask) {
		data += 10;
	}
	if (relayStatus & Relay3Mask) {
		data += 100;
	}
	if (relayStatus & Relay4Mask) {
		data += 1000;
	}

	setCommand(MC_SetRelay, MCADDR_SetRelay, data);
}

void MotorCommand::commandSetPusherSpeed(int rpm)
{
	setCommand(MC_SetPusherSpeed, MCADDR_SetPusherSpeed, rpm);
}

void MotorCommand::commandSetPosLoopParameters(PosLoopParameterTypes type, int value)
{
	setCommand(MC_SetParameters, type, value);
}

void MotorCommand::commandSetSpeedLoopParameters(SpeedLoopParametersTypes type, int value)
{
	setCommand(MC_SetParameters, type, value);
}

void MotorCommand::commandSetLimitMask(LimitMaskTypes type)
{
	setCommand(MC_SetLimitMask, type, 0);
}

void MotorCommand::commandCheckCodec(CheckCodecActions action)
{
	setCommand(MC_CheckCodec, action, 0);
}

void MotorCommand::initializeCommandHeaders()
{
	CommandHeaders[MC_SetPitch      ] = 'Z';
	CommandHeaders[MC_SetZeroPos    ] = 'R';
	CommandHeaders[MC_SetCurPos     ] = 'P';
	CommandHeaders[MC_RunToPos      ] = 'G';
	CommandHeaders[MC_JogRun        ] = 'J';
	CommandHeaders[MC_SetStopMode   ] = 'M';
	CommandHeaders[MC_SetRelay      ] = 'K';
	CommandHeaders[MC_SetPusherSpeed] = 'S';
	CommandHeaders[MC_SetParameters ] = 'C';
	CommandHeaders[MC_CheckCodec    ] = 'Q';
	CommandHeaders[MC_SetLimitMask  ] = 'L';
}

/*
 * Convert number to char
 *   0  - 9  : '0' - '9'
 *   10 - 15 : 'A' - 'F'
 * Only the lower 4 bits are available.
 */
char MotorCommand::toChar(int number)
{
#if 0
	if ((number & 0x0f) <= 9)
		return (number & 0x0f) + 0x30;       /* 0 - 9 */
	else
		return (number & 0x0f) + 0x41 - 10;  /* A - F */
#else
	return QString::number(number & 0x0f, 16).toUpper().at(0).toAscii();
#endif
}

int MotorCommand::toInt(bool *ok, char ch)
{
#if 0
	int data = 0;

	if ((ch >= 0x30) && (ch <= 0x39)) {
		data = ch - 0x30;
		*ok = true;
	} else if ((ch >= 0x41) && (ch <= 0x46)) {
		data = ch - 0x41 + 10;
		*ok = true;
	} else
		*ok = false;

	return data;
#else
	QString src = QString(QChar(ch));
	return src.toInt(ok, 16);
#endif
}
