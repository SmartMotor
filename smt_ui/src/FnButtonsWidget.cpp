/*
 * FnButtonsWidget.cpp
 *
 *  Created on: Jun 28, 2010
 *      Author: souken-i
 */

#include <QtGui>

#include "FnButtonsWidget.h"
#include "FlagEditor.h"

QString FnButtonsWidget::editFnIconFiles[]  =
{
	"images/EMD-F1-labeledit.png",
	"images/EMD-F2-averageedit.png",
	"images/EMD-F3-files.png",
	"images/EMD-F4-flag-pusher.png",
	"images/EMD-F5-flag-cut.png",
	"images/EMD-F6-calc.png",
};

QString FnButtonsWidget::autoFnIconFiles[]  =
{
	"images/AMD-F1-clear.png",
	"images/AMD-F2-basesetup.png",
	"images/EMD-F3-files.png",      // Same Icon
};

QString FnButtonsWidget::manualFnIconFiles[]  =
{
	"images/MMD-F1-setting.png",
	"images/MMD-F2-maintenance.png",
	"images/MMD-F3-basesetup.png",
	"images/MMD-F4-clear.png",
	"images/MMD-F5-sensor.png",
	"images/EMD-F6-calc.png",       // Same Icon
};

FnButtonsWidget::FnButtonsWidget(QWidget *parent)
	: QStackedWidget(parent)
{
	signalMapper = new QSignalMapper;

	editFnButtonsPage = createFnButtonsWidget(EditModeFnButtonNumber,
			editFnIconFiles, editFnButtons);

	autoFnButtonsPage =	createFnButtonsWidget(AutoRunModeFnButtonNumber,
			autoFnIconFiles, autoFnButtons);

	manualFnButtonsPage = createFnButtonsWidget(ManualRunModeFnButtonNumber,
			manualFnIconFiles, manualFnButtons);

	connect(signalMapper, SIGNAL(mapped(int)), this, SIGNAL(clicked(int)));

	// The order is important
	addWidget(editFnButtonsPage);
	addWidget(autoFnButtonsPage);
	addWidget(manualFnButtonsPage);

	setCurrentIndex(0);
	setContentsMargins(0, 0, 0, 0);
}

FnButtonsWidget::~FnButtonsWidget()
{
}

Button * FnButtonsWidget::createButton(int id , const QIcon &icon)
{
	Button *button = new Button;
	button->setIcon(icon);
    connect(button, SIGNAL(clicked()), signalMapper, SLOT(map()));
    button->setProperty("custom", true);
    signalMapper->setMapping(button, (Qt::Key_F1 + id));
    return button;
}

QWidget * FnButtonsWidget::createFnButtonsWidget(int nr, const QString *iconFiles, Button **fnButtons)
{
	int i;
	QWidget * widget = new QWidget;

	QVBoxLayout *layout = new QVBoxLayout;
	for (i = 0; i < nr; i++) {
		fnButtons[i] = createButton(i, QIcon(iconFiles[i]));
		layout->addWidget(fnButtons[i]);
	}
	layout->setContentsMargins(0, 0, 0, 0);
	widget->setLayout(layout);
	widget->setContentsMargins(0, 0, 0, 0);

	return widget;
}
