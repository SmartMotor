/*
 * TextEditVKeyboard.cpp
 *
 *  Created on: 2010/06/08
 *      Author: kinhi
 */

#include "include/TextEditVKeyboard.h"
#include "Global.h"
#include "Debug.h"

TextEditVKeyboard::TextEditVKeyboard(QWidget * parent) : QTextEdit(parent) {
	// TODO Auto-generated constructor stub

}

TextEditVKeyboard::~TextEditVKeyboard() {
	// TODO Auto-generated destructor stub
}

void TextEditVKeyboard::mousePressEvent ( QMouseEvent * event )
{
	/* Set keyboard to full mode */
	gVKeyboard->setMode(VirtualKeyboard::MODE_FULL);
	gVKeyboard->toggleVisibility();
	if (gVKeyboard->isVisible()) {
		emit vKeyboardShowEvent();
	}
	QTextEdit::mousePressEvent ( event );
}
