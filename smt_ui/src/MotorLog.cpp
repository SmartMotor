/*
 * MotorLog.cpp
 *
 *  Created on: Apr 18, 2010
 *      Author: souken-i
 */

#include <QDateTime>
#include "MotorLog.h"
#include "Global.h"

MotorLog::MotorLog()
{
	_savedLogNumber = 0;
	_nextItem = 0;
}

MotorLog::~MotorLog()
{
}

void MotorLog::setLogFileName(QString &fileName)
{
	if (!fileName.isEmpty()) {
		_logFileName = fileName;

		loadLog();
	}
}

QString & MotorLog::getLogFileName()
{
	return _logFileName;
}

void MotorLog::loadLog()
{
	if (!_logFileName.isEmpty()) {
        QFile file(_logFileName);

        if (file.open(QFile::ReadOnly | QFile::Text)) {
    		QTextStream in(&file);
    		in.setCodec("UTF-8");

    		int index = 0;

        	while ( !in.atEnd() ) {
        		QString line = in.readLine();
        		if ( !line.isEmpty() && line.at(0) == '#' ) {  /* comment line */
        			continue;
        		}

        		if (!line.isEmpty() && line.contains(QString("LOG"), Qt::CaseSensitive)) {
        			_logData[index] = line;
        			index++;

        			if (index >= MaxLogItems) {
        				break;
        			}
        		}
        	}
			_savedLogNumber = index;
			_nextItem = index % MaxLogItems;
        }
	}
}

void MotorLog::appendAlarmLog(int alarmCode, int position)
{
	QString log;

#if 0
	qDebug() << "appendAlarmLog: " << alarmCode << " " << position << endl;
	qDebug() << "appendAlarmLog to: " << _logFileName.toAscii().data() << endl;
#endif

	setNewLogLine(log, alarmCode, position);

	if (_nextItem < MaxLogItems) {
		_logData[_nextItem++] = log;

		if(_nextItem == MaxLogItems) {
			_nextItem = 0;
		}

		if (_savedLogNumber < MaxLogItems) {
			_savedLogNumber ++;
		}
	} else {
		// Error, clear all
		_savedLogNumber = 0;
		_nextItem = 0;
	}

	saveLog();
}

void MotorLog::saveLog()
{
	int start, total;

	total = (_savedLogNumber >= MaxLogItems) ? MaxLogItems : _savedLogNumber;
	if (total >= MaxLogItems) {
		start = _nextItem;
	} else {
		start = 0;
	}

	if (!_logFileName.isEmpty()) {
		QFile file(_logFileName);

		if (file.open(QFile::WriteOnly | QFile::Text)) {
			QTextStream out(&file);
			out.setCodec("UTF-8");

			out << "# Version: 1.0" << endl;  /* format version */
			for (int i = 0; i < total; i++) {
				int index = (start + i) % MaxLogItems;
				if (!_logData[index].isEmpty()) {
					out << _logData[index] << endl;
				}
			}

			file.close();
		}
	}
}

void MotorLog::setNewLogLine(QString &log, int alarmCode, int position)
{

	QDateTime dateTime = QDateTime::currentDateTime();
	QString date = dateTime.toString("yyyy.MM.dd");
	QString time = dateTime.toString("hh:mm:ss");
	QString comma = QString(",");

	log = QString("LOG,") + QString::number(alarmCode) + comma + date + comma + time + comma +
		  QString("Running Mode") + comma + MData::toString(position);
}

