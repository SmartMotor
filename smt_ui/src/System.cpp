/*
 * System.cpp
 *
 *  Created on: 2010/03/21
 *      Author: crazysjf
 */

#include <QtGui>
#include <QString>
#include <QStringList>
#include <QFile>
#include <QTextStream>
#include <QApplication>

#include "System.h"
#include "Debug.h"
#include "Global.h"

QString System::_currentUser = "";
QString System::_dataRootDir = "";

System::System()
{
	_systemConfigs[ItemSysFontSize]      = SYSTEM_FONT_SIZE_DEFAULT;
	_systemConfigs[ItemDataListFontSize] = DATA_LIST_FONT_SIZE_DEFAULT;
	_systemConfigs[ItemMinFontSize]      = SYSTEM_FONT_SIZE_MIN;
	_systemConfigs[ItemMaxFontSize]      = SYSTEM_FONT_SIZE_MAX;
	//_systemConfigs[OpenFileDialogFontSize] = 10;

	_systemConfigs[ItemPositionFontSize] = POSITION_FONT_SIZE_DEFAULT;
	_systemConfigs[ItemDistanceFontSize] = DISTANCE_FONT_SIZE_DEFAULT;
	_systemConfigs[ItemScreenSaverTime]  = 600;  // Second

	_systemConfigs[ItemLanguage]         = English;

	// TODO
	_systemColors[ItemPostionFontColor]        = QColor("black");
	_systemColors[ItemPostionBackgroundColor]  = QColor("white");
	_systemColors[ItemDistanceFontColor]       = QColor("black");
	_systemColors[ItemDistanceBackgroundColor] = QColor("white");

	_popupDialogRect = QRect();

#ifdef START_DEBUG
	qDebug() << "System() done" << endl;
#endif
}

System::~System()
{
}

/*
 *   *** NOTE ***
 *   This function MUST be called AFTER the qApp initialization
 *   in main().
 */
void System::installAppTranslator()
{
	qApp->installTranslator(&appTranslator);
}

void System::loadAppTranslator()
{
	int language = getLanguage();
	QString qmFile;

	switch(language) {
	case Chinese:
		qmFile = QString("smt_cn.qm");
		break;
	case English:
	default:
		qmFile = QString("smt_en.qm");
		break;
	}

	appTranslator.load(qmFile, "translations");
}

void System::setCurrentUser(const QString &userName) {
	_currentUser = userName;
}

QString &System::currentUser(void) {
	return _currentUser;
}

void System::setDataRootDir(const QString &dir) {
	_dataRootDir = dir;
}

QString &System::dataRootDir(void) {
	return _dataRootDir;
}

int System::getLanguage()
{
	return _systemConfigs[ItemLanguage];
}

void System::setLanguage(int language)
{
	if (language < MaxLanguages) {
		_systemConfigs[ItemLanguage] = language;
	}
}

int System::getSysFontSize()
{
	return _systemConfigs[ItemSysFontSize];
}

void System::setSysFontSize(int size)
{
	if (isFontSizeValid(size)) {
		_systemConfigs[ItemSysFontSize] = size;
	}
}

int  System::getScreensaverTime()
{
	return _systemConfigs[ItemScreenSaverTime];
}

void System::setScreensaverTime(int seconds)
{
	_systemConfigs[ItemScreenSaverTime] = seconds;
}

int System::getDataListFontSize()
{
	return _systemConfigs[ItemDataListFontSize];
}

void System::setDataListFontSize(int size)
{
	if (isFontSizeValid(size)) {
		_systemConfigs[ItemDataListFontSize] = size;
	}
}

//int System::getOpenFileDialogFontSize()
//{
//	return _systemConfigs[OpenFileDialogFontSize];
//}
//
//void System::setOpenFileDialogFontSize(int size)
//{
//	if (isFontSizeValid(size)) {
//		_systemConfigs[OpenFileDialogFontSize] = size;
//	}
//}

int System::getPositionFontSize()
{
	return _systemConfigs[ItemPositionFontSize];
}

void System::setPositionFontSize(int size)
{
	if (isFontSizeValid(size)) {
		_systemConfigs[ItemPositionFontSize] = size;
	}
}

int System::getDistanceFontSize()
{
	return _systemConfigs[ItemDistanceFontSize];
}

void System::setDistanceFontSize(int size)
{
	if (isFontSizeValid(size)) {
		_systemConfigs[ItemDistanceFontSize] = size;
	}
}

QColor & System::getPositionFontColor()
{
	return _systemColors[ItemPostionFontColor];
}

void System::setPositionFontColor(QColor &color)
{
	_systemColors[ItemPostionFontColor] = color;
}

QColor & System::getPositionBackgroundColor()
{
	return _systemColors[ItemPostionBackgroundColor];
}

void System::setPositionBackgroundColor(QColor &color)
{
	_systemColors[ItemPostionBackgroundColor] = color;
}

QColor & System::getDistanceFontColor()
{
	return _systemColors[ItemDistanceFontColor];
}

void System::setDistanceFontColor(QColor &color)
{
	_systemColors[ItemDistanceFontColor] = color;
}

QColor & System::getDistanceBackgroundColor()
{
	return _systemColors[ItemDistanceBackgroundColor];
}

void System::setDistanceBackgroundColor(QColor &color)
{
	_systemColors[ItemDistanceBackgroundColor] = color;
}

bool System::isFontSizeValid(int size)
{
	return ((size >= _systemConfigs[ItemMinFontSize]) &&
			(size <= _systemConfigs[ItemMaxFontSize]));
}

void System::setSysConfigFile(QString & fileName)
{
	_systemConfigFile = fileName;
}

bool System::loadSysConfigFile(QString &fileName)
{
	if ( fileName.isEmpty() ) {
		// User specified an empty file
		return false;
	}

	_systemConfigFile = fileName;
	load();

	return 1;
}

bool System::load()
{
	QFile file(_systemConfigFile);

	if (_systemConfigFile.isEmpty()) {
		return false;
	}

	if ( !file.open(QIODevice::ReadOnly) ){
		return false;
	}

	QTextStream in(&file);
	in.setCodec("UTF-8");

	qDebug() << "== Loading system configuration file ==";

	while ( !in.atEnd() ) {
		QString line = in.readLine();
		if ( !line.isEmpty() && line.at(0) == '#' ) {  /* comment line */
#ifdef SYSTEM_DEBUG
			qDebug << "Found comment line";
#endif
			continue;
		}

		// Integral  values
		if ( !line.isEmpty() && line.at(0) == '@' && line.at(1) == 'I' ) {
			QStringList fields = line.split(' '); /* seperated by space */
			if ( fields.size() >= MaxSysConfigItems ) {
				// Ignore the first one
				fields.takeFirst();

				for (int i = 0; i < MaxSysConfigItems; i++) {
					_systemConfigs[i] = fields.takeFirst().toInt();
#ifdef SYSTEM_DEBUG
					::printf("== system %d: data %d \n", i, _systemConfigs[i]);
#endif
				}
			}
		}

		if ( !line.isEmpty() && line.at(0) == '@' && line.at(1) == 'C' ) {
#ifdef SYSTEM_DEBUG
			qDebug() << "Found color line";
#endif
			QStringList fields = line.split(' '); /* seperated by space */
			if ( fields.size() >= MaxSystemColors ) {
				// Ignore the first one
				fields.takeFirst();

				for (int i = 0; i < MaxSystemColors; i++) {
					_systemColors[i] = QColor(fields.takeFirst());
#ifdef SYSTEM_DEBUG
					::printf("== system color %d:  %s \n", i, _systemColors[i].name().toAscii().data());
#endif
				}
			}
		}
	}


	file.close();

	// Also load last open file
	loadLastOpenFile();

	return true;
}

bool System::save()
{
	QFile file(_systemConfigFile);

	if ( !file.open(QIODevice::WriteOnly) ){
		return false;
	}

	qDebug() << "== Saving system configuration file ==";

	QTextStream out(&file);
	out.setCodec("UTF-8");

	out << "# Version: 1.1" << endl;  /* format version */
	out << "# @I: int value; @C: colors" << endl;

	// Save int configs
	out << "@I" << " ";
	for (int i = 0; i < MaxSysConfigItems; i++) {
		out << _systemConfigs[i];
		if (i != MaxSysConfigItems) {
			out << " ";
		}
	}
	out << endl;

	// Save colors
	out << "@C" << " ";
	for (int i = 0; i < MaxSystemColors; i++) {
		out << _systemColors[i].name();
		if (i != MaxSystemColors) {
			out << " ";
		}
	}
	out << endl;


	file.close();
	return true;
}

QString & System::getLastOpenFile()
{
	return _lastOpenFile;
}

bool System::loadLastOpenFile()
{
	QString configFile = currentUserDir() + QString("lastfile.cfg");
	QFile file(configFile);

	if ( !file.open(QIODevice::ReadOnly) ){
		return false;
	}

	QTextStream in(&file);
	in.setCodec("UTF-8");

	while ( !in.atEnd() ) {
		QString line = in.readLine();
		// Must check isEmpty() before calling at(0)
		if ( !line.isEmpty() && line.at(0) == '#' ) {  /* comment line */
			continue;
		}

		if (!line.isEmpty()) {
			_lastOpenFile = line;
			break;
		}
	}

	file.close();
	return true;
}

bool System::saveLastOpenFile(QString & fileName)
{
	QString configFile = currentUserDir() + QString("lastfile.cfg");
	QFile file(configFile);

	_lastOpenFile = fileName;

	if ( !file.open(QIODevice::WriteOnly) ){
		return false;
	}

	QTextStream out(&file);
	out.setCodec("UTF-8");

	out << "# Version: 1.0" << endl;  /* format version */
	out << _lastOpenFile << endl;

	file.close();
	return true;
}

#if 0
// Load style sheet inside widget itself
void System::loadStyleSheet(QString & fileName)
{
	if ( !fileName.isEmpty() ) {
		QFile file(fileName);
		file.open(QFile::ReadOnly);
		QString styleSheet = QLatin1String(file.readAll());

		qApp->setStyleSheet(styleSheet);

		file.close();
	}
}
#endif
