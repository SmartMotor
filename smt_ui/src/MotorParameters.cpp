/*
 * MotorParameters.cpp
 *
 *  Created on: Mar 21, 2010
 *      Author: souken-i
 */
#include <QString>
#include <QStringList>
#include <QFile>
#include <QTextStream>

#include "MotorParameters.h"

MotorParameters::MotorParameters()
{
	setDefaultParameters();
	sysConfigFile = "./data/motor.cfg";
	_currentPos = getInitialPos();

	// FIXME
	_screwRatio = ScrewRation_1To2;
	_screwType  = Normal_Screw;
}

MotorParameters::MotorParameters(const QString &fileName)
{
	configFile = fileName;

	/* load specified config file */
	if ( !loadFile(configFile) ) {
		setDefaultParameters();
	}

	sysConfigFile = "./data/motor.cfg";
	_currentPos = getInitialPos();

	// FIXME
	_screwRatio = ScrewRation_1To2;
	_screwType  = Normal_Screw;
}

MotorParameters::~MotorParameters()
{
}

void MotorParameters::setDefaultParameters()
{
	motorParasArray[ItemInitialPos]     = 100000;  /* 1000 mm */
	motorParasArray[ItemFrontLimit]     = 3000;    /* 30   mm */
	motorParasArray[ItemMiddleLimit]    = 8000;    /* 80   mm */
	motorParasArray[ItemBackLimit]      = 150000;
	motorParasArray[ItemMaxLimit]       = 200000;
	motorParasArray[ItemPusherSpeed]    = 8;       /* m/s */
	motorParasArray[ItemMinPusherSpeed] = 8;       /* m/s */
	motorParasArray[ItemMaxPusherSpeed] = 15;      /* m/s */
	motorParasArray[ItemRunSpeed]       = 400;     /* RPM */
	motorParasArray[ItemMinRunSpeed]    = 400;     /* RPM */
	motorParasArray[ItemMaxRunSpeed]    = 1600;    /* RPM */
	motorParasArray[ItemPitch]          = 0;       /* type, see coding table */
	motorParasArray[ItemAirStopDistabce]= 10;       /* type, see coding table */
	motorParasArray[ItemStepSize]       = 10;       /* type, see coding table */
}


int MotorParameters::getFrontLimit()
{
	return motorParasArray[ItemFrontLimit];
}

void MotorParameters::setFrontLimit(int value)
{
	if ((value < 0) || (value > getMiddleLimit())) {
		return;
	}
	motorParasArray[ItemFrontLimit] = value;
}

int MotorParameters::getMiddleLimit()
{
	return motorParasArray[ItemMiddleLimit];
}

void MotorParameters::setMiddleLimit(int value)
{
	if ((value < getFrontLimit()) || (value > getBackLimit())) {
		return;
	}
	motorParasArray[ItemMiddleLimit] = value;
}

int MotorParameters::getBackLimit()
{
	return motorParasArray[ItemBackLimit];
}

void MotorParameters::setBackLimit(int value)
{
	if ((value < getMiddleLimit()) || (value > motorParasArray[ItemMaxLimit])) {
		return;
	}
	motorParasArray[ItemBackLimit] = value;
}


int MotorParameters::getPusherSpeed()
{
	return motorParasArray[ItemPusherSpeed];
}

void MotorParameters::setPusherSpeed(int speed)
{
	if ((speed >= motorParasArray[ItemMinPusherSpeed]) &&
		(speed <= motorParasArray[ItemMaxPusherSpeed])) {
		motorParasArray[ItemPusherSpeed] = speed;
	}
}

int MotorParameters::getMinPusherSpeed()
{
	return motorParasArray[ItemMinPusherSpeed];
}

int MotorParameters::getMaxPusherSpeed()
{
	return motorParasArray[ItemMaxPusherSpeed];
}

int MotorParameters::getRunSpeed()
{
	return motorParasArray[ItemRunSpeed];
}

void MotorParameters::setRunSpeed(int speed)
{
	if ((speed >= motorParasArray[ItemMinRunSpeed]) &&
		(speed <= motorParasArray[ItemMaxRunSpeed])) {
		motorParasArray[ItemRunSpeed] = speed;
	}
}

int MotorParameters::getMinRunSpeed()
{
	return motorParasArray[ItemMinRunSpeed];
}

int MotorParameters::getMaxRunSpeed()
{
	return motorParasArray[ItemMaxRunSpeed];
}

int MotorParameters::getInitialPos()
{
	return motorParasArray[ItemInitialPos];
}

void MotorParameters::setInitialPos(int position)
{
	if ((position >= getFrontLimit()) && (position <= getBackLimit())) {
		motorParasArray[ItemInitialPos] = position;
	}
}

int MotorParameters::getMaxLimit()
{
	return motorParasArray[ItemMaxLimit];
}

int MotorParameters::getPitch()
{
	return motorParasArray[ItemPitch];
}
void MotorParameters::setPitch(int pitch)
{
	motorParasArray[ItemPitch] = pitch;
}

int  MotorParameters::getScrewType()
{
	return _screwType;
}
void MotorParameters::setScrewType(int type)
{
	if (type < MaxScrewTypes) {
		_screwType = type;
	}
}
int  MotorParameters::getScrewRatio()
{
	return _screwRatio;

}
void MotorParameters::setScrewRatio(int ratio)
{
	if (ratio < MaxScrewRations) {
		_screwRatio = ratio;
	}
}

int  MotorParameters::getAirStopDistance()
{
	return motorParasArray[ItemAirStopDistabce];
}

void MotorParameters::setAirStopDistance(int distance)
{
	motorParasArray[ItemAirStopDistabce] = distance;
}

int  MotorParameters::getStepSize()
{
	return motorParasArray[ItemStepSize];
}

void MotorParameters::setStepSize(int size)
{
	motorParasArray[ItemStepSize] = size;
}

void MotorParameters::setConfigFile(const QString &fileName)
{
	if ( !fileName.isEmpty() ) {
		configFile = fileName;
	}
}

bool MotorParameters::loadConfigFile(const QString &fileName)
{
	if ( fileName.isEmpty() ) {
		return loadFile(sysConfigFile);
	}

	configFile = fileName;
	return loadFile(configFile);
}

/*
 * load configurations from file
 */
bool MotorParameters::load()
{
	if ( !configFile.isEmpty() ) {
		if ( loadFile(configFile) == false ) {
			/* Fall back to system configuration file */
			return loadFile(sysConfigFile);
		}
	}

	return true;
}

bool MotorParameters::save()
{
	return saveFile(configFile);
}


bool MotorParameters::saveToSystem()
{
	return saveFile(sysConfigFile);
}


bool MotorParameters::loadFile(const QString &fileName)
{
	QFile file(fileName);

	if ( !file.open(QIODevice::ReadOnly) ){
		return false;
	}

	QTextStream in(&file);
	in.setCodec("UTF-8");

	while ( !in.atEnd() ) {
		QString line = in.readLine();
		// Must check isEmpty() before calling at(0)
		if ( !line.isEmpty() && line.at(0) == '#' ) {  /* comment line */
#ifdef MOTOR_PARAMETERS_DEBUG
			_showMessage("Found comment line");
#endif
			continue;
		}

		QStringList fields = line.split(' '); /* seperated by space*/
#ifdef MOTOR_PARAMETERS_DEBUG
		qDebug() << "fields size " << fields.size() << endl;
#endif
		if ( fields.size() >= MotorParameterItems ) {
			int readData[MotorParameterItems];
			for (int i = 0; i < MotorParameterItems; i++) {
				readData[i] = fields.takeFirst().toInt();
#ifdef MOTOR_PARAMETERS_DEBUG
				::printf("== %d: data %d \n", i, readData[i]);
#endif
			}
			setParametersFromConfigFile(readData);
			break; /* loaded */
		}
	}

	file.close();

	return true;
}

bool MotorParameters::saveFile(const QString &fileName)
{
	QFile file(fileName);

	if ( !file.open(QIODevice::WriteOnly) ){
		return false;
	}

	QTextStream out(&file);
	out.setCodec("UTF-8");

	int dataArray[MotorParameterItems];
	getCurrentParameters(dataArray);

	out << "# Version: 1.0" << endl;  /* format version */
	for (int i = 0; i < MotorParameterItems; i++) {
		out << dataArray[i] << " ";
	}
	out << endl;

	file.close();

	return true;
}

void MotorParameters::setParametersFromConfigFile(int *dataArray)
{

	for (int i = 0; i < MotorParameterItems; i++) {
		motorParasArray[i] = dataArray[i];
	}
}

void MotorParameters::getCurrentParameters(int *dataArray)
{
	for (int i = 0; i < MotorParameterItems; i++) {
		dataArray[i] = motorParasArray[i];
	}
}


int MotorParameters::pos()
{
	return _currentPos;
}

void MotorParameters::setPos(int position)
{
	_currentPos = position;
}

int MotorParameters::pitchTypeToValue(PitchTypes type)
{
	return (1000 + type * 100);
}

int MotorParameters::speedMPerSecondToRPM(int meterPerSecond)
{
	/*
	 * 8  m/s -> 900   (Min)
	 * 9  m/s -> 1000
	 * ...
	 * 15 m/s -> 1600  (Max)
	 */
	return (900 + (meterPerSecond - 8) * 100);
}


/* Local Variables:  */
/* c-basic-offset: 4 */
/* End:              */
