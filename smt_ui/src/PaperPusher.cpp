#include <QtGui>
#include <QMessageBox>
#include "PaperPusher.h"

#include "Global.h"

PaperPusher::PaperPusher(QWidget *parent)
	: QWidget(parent)
{
	QPixmap paperPusherPixmap = QPixmap("images/pusher.bmp");
	QPixmap scaledPixMap = paperPusherPixmap.scaled(MaxPixmapWidth, MaxPixmapHeight, Qt::KeepAspectRatio);

	pusherPixmapLabel = new QLabel;
	pusherPixmapLabel->setPixmap(scaledPixMap);
	pusherPixmapLabel->setAlignment(Qt::AlignCenter);

	textLabel = new QLabel;
	textLabel->setAlignment(Qt::AlignCenter);
//	textLabel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
	textLabel->setFixedHeight(MaxTextHeight);

	updateFontSizeColor();

	connect(gMotor, SIGNAL(currentPosChanged(int)), this, SLOT(updateCurrentPostion(int)));

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addWidget(pusherPixmapLabel);
	layout->addWidget(textLabel);
	layout->setSpacing(4);
	setLayout(layout);

//	setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	setContentsMargins(0, 0, 0, 0);
	setFixedHeight(MaxPixmapHeight + MaxTextHeight);

	updateCurrentPostion(gMotor->pos());
}

PaperPusher::~PaperPusher()
{
}

void PaperPusher::updateCurrentPostion(int pos)
{
	QString text = QString(MData::toString(pos)) + QString(" MM");
	textLabel->setText(text);
}

void PaperPusher::updateFontSizeColor()
{
	QPalette palette;
	QColor backgroundColor = gSysConfig->getPositionBackgroundColor();
	QColor fontColor = gSysConfig->getPositionFontColor();
	palette.setColor(QPalette::Window, backgroundColor);
	palette.setColor(QPalette::WindowText, fontColor);
	textLabel->setPalette(palette);

	QFont font = textLabel->font();
	font.setPointSize(gSysConfig->getPositionFontSize());
	textLabel->setFont(font);
}


