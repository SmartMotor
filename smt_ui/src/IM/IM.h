/*
 * IM.h
 *
 *  Created on: 2010/04/22
 *      Author: kinhi
 */

#ifndef IM_H_
#define IM_H_
#include <QWSInputMethod>
#include <QSqlDatabase>
#include <QSqlQuery>
#include "IMWindow.h"

class IM : public QWSInputMethod {
    Q_OBJECT
public:
	IM();
	virtual ~IM();

    static void installInputMethod();
    static void uninstallInputMethod();
    static IM* instance();
    static IMWindow *getIMWindow() { return pIMWindow; }

    virtual bool filter(int unicode, int keycode, int modifiers,
                        bool isPress, bool autoRepeat);

public slots:
	void selectionButtonEvent(int id);

private:
    void toggleIME();

    void newCharacter(char);
	bool makeSelection(int);
    void showNextPage();
    void showPreviousPage();
    void showCurrentPage();
    void query();
    static IM *pIM;
    static IMWindow *pIMWindow;
    static QString pinyin;
    static QVector<QString> queryResult;
    static int currentDisplayPage;
    static QSqlDatabase db;
    int statusWaitingNewRound;
    //static QSqlQuery query;
};

#endif /* IM_H_ */
