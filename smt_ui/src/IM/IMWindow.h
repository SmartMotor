/*
 * IMWindow.h
 *
 *  Created on: 2010/04/22
 *      Author: kinhi
 */

#ifndef IMWINDOW_H_
#define IMWINDOW_H_

#include <QtGui>
#include <QSignalMapper>

#include "RoundRectWidget.h"
#include "PushButton.h"

#define CHARACTER_PER_PAGE   10

class IMWindow : public RoundRectWidget {
    Q_OBJECT
public:
	IMWindow(QWidget* parent = 0);
	virtual ~IMWindow();
	void setHanziList(const QVector<QString> displayList);
	void setPinyin(const QString &pinyin);
	QString *getSelection(int n);
	void clearWindow();


public slots:
//	void focusChanged(QWidget *, QWidget *);
	void toggleVisibility();
//	void cursorPostionChanged();

signals:
	void buttonClicked(int id);

protected:
    virtual void paintEvent(QPaintEvent*);

private:
    QVector<QString> _hanziList;
    QString _pinyin;
	PushButton *createButton(int id);

//    void layout();
//    void adjustPosition(QPoint &);
//    QTextEdit *currentInputWindow;

    int marginX; // Margin from text to window frame in X direction.
    int marginY; // Margin from text to window frame in Y direction.

    PushButton *selectionButton[10];
    QLabel      *pinyinLabel;
    QSignalMapper *signalMapper;

};

#endif /* IMWINDOW_H_ */
