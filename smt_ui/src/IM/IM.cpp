/*
 * IM.cpp
 *
 *  Created on: 2010/04/22
 *      Author: kinhi
 */

#include "IM.h"

IM* IM::pIM = NULL;
IMWindow *IM::pIMWindow = NULL;
QString IM::pinyin;
QVector<QString> IM::queryResult;
int IM::currentDisplayPage;
QSqlDatabase IM::db;
//QSqlQuery IM::query;

IM::IM() {
	// TODO Auto-generated constructor stub
	pIMWindow = new IMWindow();
	connect(pIMWindow, SIGNAL(buttonClicked(int)),
			this, SLOT(selectionButtonEvent(int)));

	statusWaitingNewRound = 1;
}

IM::~IM() {
	// TODO Auto-generated destructor stub
	delete pIMWindow;
}

/* Get a IM instance. If not exists, create one. */
IM *IM::instance()
{
	if (NULL == pIM)
	{
		pIM = new IM();
	}

	return pIM;
}

/* Install this input method to QWS. */
/* TODO: What happens if called multiple times? */
void IM::installInputMethod()
{
	IM* pim = instance();

	if (pim) {
		db = QSqlDatabase::addDatabase("QSQLITE");
		db.setDatabaseName("zhpy_table.db");

		if (!db.open()) {
			QMessageBox::critical(NULL, QObject::tr("Open database failed\n"), QObject::tr("Press Cancel to Exit"),QMessageBox::Cancel,QMessageBox::NoButton);
			return;
		}

		//query.prepare("SELECT * FROM zhcn WHERE py=:pinyin");
		pinyin.clear();

		QWSServer::setCurrentInputMethod(pim);
	}
}

/* Uninstall this input method from QWS */
void IM::uninstallInputMethod()
{
	if (pIM) {
		QWSServer::setCurrentInputMethod(NULL);
		delete pIM;
		pIM = NULL;
	}
}

/**
 * Select the n'th character and commit it.
 */
bool IM::makeSelection(int n)
{
	QString *s = pIMWindow->getSelection(n);
	if (s != NULL) {
		sendCommitString(*s);
		pinyin.clear();
		statusWaitingNewRound = 1;

		// Clear hanzi list
		pIMWindow->clearWindow();
	}

	// Always return true, don"t by-pass this key if pinyin is not NULL
	return true;
}

/**
 * Show current character page to IMWindow.
 */
void IM::showCurrentPage()
{
	QVector<QString> tmp;

	int i = currentDisplayPage * CHARACTER_PER_PAGE;
	int pageEnd = i + CHARACTER_PER_PAGE;
	while (i < queryResult.count() && i < pageEnd) {
		tmp.append(queryResult[i]);
		i++;
	}

	pIMWindow->setHanziList(tmp);
}

/**
 * Show next character page.
 * If out of range, do nothing.
 */
void IM::showNextPage()
{
	if ((currentDisplayPage + 1) * CHARACTER_PER_PAGE >= queryResult.count()) {
		return;
	}
	currentDisplayPage ++;
	showCurrentPage();
}

/**
 * Show previous character page.
 * If out of range, do nothing.
 */
void IM::showPreviousPage()
{
	if (currentDisplayPage - 1 < 0) {
		return;
	}
	currentDisplayPage --;
	showCurrentPage();

}

void IM::query()
{
	QSqlQuery query;
	query.prepare("SELECT * FROM zhcn WHERE py=:pinyin");
    query.bindValue(":pinyin",pinyin);

    if(query.exec()) {
//    	qDebug()<<QObject::tr("query success!\n");
    } else {
//    	qDebug()<<QObject::tr("query failed!\n");
    }

    QString tmp;
    queryResult.clear();
    while(query.next()) {
        tmp = query.value(2).toString();
        queryResult.append(tmp);
    }
}

bool IM::filter(int /*unicode*/, int keycode, int modifiers, bool isPress, bool /*autoRepeat*/)
{
	if (isPress &&
			(((Qt::AltModifier & modifiers) && (Qt::Key_Z == keycode)) ||
					Qt::Key_Henkan == keycode)) {
		toggleIME();
		return true;
	}

	if (pIMWindow->isVisible() && isPress) {
		if ((Qt::Key_A <= keycode) && (Qt::Key_Z >= keycode)) {
			if (statusWaitingNewRound == 1) {
				pinyin.clear();
				statusWaitingNewRound = 0;
			}
			char ch = (char)((Qt::ShiftModifier & modifiers) ?
					keycode : (keycode - Qt::Key_A + 'a'));
			pinyin += ch;
//			qDebug() << pinyin;
			pIMWindow->setPinyin(pinyin);

			query();
			currentDisplayPage = 0;
		    showCurrentPage();

		    return true;
		}
		if (Qt::Key_Backspace == keycode) {
			if (pinyin.length() == 0) {
				return false; // If no there is no pinyin left, by pass this key event.
			}
			pinyin.remove(pinyin.length() - 1, 1); // remove last character
//			qDebug() << pinyin;
			pIMWindow->setPinyin(pinyin);

			query();
			currentDisplayPage = 0;
		    showCurrentPage();

			return true;
		}

		if ((Qt::Key_0 <= keycode) && (Qt::Key_9 >= keycode)) {
			if (pinyin.length() == 0) {
				return false; // If no there is no pinyin left, by pass this key event.
			}
			return makeSelection(keycode - Qt::Key_0);
		}

		if (Qt::Key_Down == keycode) {
			showNextPage();
			return true;
		}

		if (Qt::Key_Up == keycode) {
			showPreviousPage();
			return true;
		}

		if (Qt::Key_Return == keycode) {
			query();
		    currentDisplayPage = 0;
		    showCurrentPage();
		    return true;
		}

		return false;
	}

	return false;
}

void IM::selectionButtonEvent(int id)
{
	makeSelection(id);
}

void IM::toggleIME()
{
	if (pIMWindow->isVisible()) {
		pIMWindow->hide();
		//mpdata->imedata.reset();
	} else {
		pIMWindow->show();
	}
}
