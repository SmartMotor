/*
 * IMWindow.cpp
 *
 *  Created on: 2010/04/22
 *      Author: kinhi
 */

#include "IMWindow.h"
#include "Global.h"

#define DEFAULT_STYLE_BUTTON       "border: 1px solid #8f8f91;border-radius:5px;"
#define DEFAULT_BACKGROUND_BUTTON	"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #f6f7fa, stop: 1 #dadbde);"
#define CHANGED_BACKGROUND_BUTTON	"background:lightblue;"
#define STYLE_FOCUS_TEXT           "border: 1px solid red"

IMWindow::IMWindow(QWidget* parent)
	: RoundRectWidget(parent, 0.07)
{
	marginX = 10;
	marginY = 5;

	QSize desktopSize = QApplication::desktop()->screenGeometry(0).size();

	int w = desktopSize.width();;
	int h = 32 + marginY * 2;
	resize(w, h);
	move(0, desktopSize.height() - h - marginY); // Move to bottom-center.

	setMovable(false); // disable drag

	// Label and buttons
	pinyinLabel = new QLabel;
	pinyinLabel->setAlignment(Qt::AlignCenter);
	pinyinLabel->setMinimumWidth(64);
	pinyinLabel->setMaximumWidth(64);

	QHBoxLayout *layout = new QHBoxLayout;
	layout->addWidget(pinyinLabel);

	signalMapper = new QSignalMapper;

	for (int i = 1; i <= CHARACTER_PER_PAGE; i++) {
		selectionButton[i-1] = createButton(i);
		layout->addWidget(selectionButton[i-1]);
	}
    connect(signalMapper, SIGNAL(mapped(int)), this, SIGNAL(buttonClicked(int)));

	setLayout(layout);
	layout->setContentsMargins(5,0,5,0);
	layout->setSpacing(2);

	// Hide ourself if virtual keyboard is hiden
	connect(gVKeyboard, SIGNAL(vKeyboardHide()), this, SLOT(hide()));


	setWindowFlags(windowFlags() | Qt::WindowStaysOnTopHint | Qt::Tool | Qt::CustomizeWindowHint);
//	setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop:  0 #f6f7fa, stop: 1 #dadbde);");
}

IMWindow::~IMWindow() {
	// TODO Auto-generated destructor stub
}

/**
 * Set the character list to be displayed on IMWindow.
 */
void IMWindow::setHanziList(QVector<QString> displayList)
{
	_hanziList.clear();
	_hanziList = displayList;

	int count = _hanziList.count();

	for (int i = 0; i< CHARACTER_PER_PAGE; i++) {
		if (i < count) {
			int index;
			if (i < 9)
				index = i + 1;
			else if (i == 9)
				index = 0;

			QString hanzi = QString("%1.%2 ").arg(index).arg(_hanziList[i]);
			selectionButton[i]->setText(hanzi);
		} else {
			selectionButton[i]->setText("");
		}
	}
}

void IMWindow::setPinyin(const QString &pinyin)
{
	pinyinLabel->setText(pinyin);
}

void IMWindow::clearWindow(void)
{
	_hanziList.clear();
	pinyinLabel->setText("");

	for (int i = 0; i < CHARACTER_PER_PAGE; i++)
		selectionButton[i]->setText("");
}

/**
 * Return the n'th string displayed on IMWindow.
 *   @n: Must in the range of displayed strings. Or else return NULL String.
 */
QString *IMWindow::getSelection(int n)
{
	if (n >= CHARACTER_PER_PAGE || n > _hanziList.count() || n < 0)
		return NULL;
	int index = (n > 0) ? (n - 1) : 9;

	// n == 0, then index = 9, but count() may less than 10
	if (index >= _hanziList.count())
		return NULL;

	return &_hanziList[index];
}

void IMWindow::paintEvent(QPaintEvent * e)
{
#if 0
	QPainter painter(this);
	QString displayString;

	displayString = _pinyin;
	if (displayString.length() != 0)
		displayString += ": ";
	for (int i=0; i<_hanziList.count(); i++) {
		int index;
		if (i < 9)
			index = i + 1;
		else if (i == 9)
			index = 0;
		displayString += QString("%1.%2 ").arg(index).arg(_hanziList[i]);
	}

	painter.drawText(rect().adjusted(marginX, marginY, -marginX, -marginY), Qt::AlignLeft | Qt::AlignVCenter, displayString);
#endif

	RoundRectWidget::paintEvent(e);
}

PushButton *IMWindow::createButton(int id)
{
	PushButton *button = new PushButton;
//	button->setFlat(true);
	button->setMinimumSize(32,32);
	button->setMaximumSize(64,32);

    connect(button, SIGNAL(clicked()), signalMapper, SLOT(map()));
    signalMapper->setMapping(button, (id == CHARACTER_PER_PAGE ? 0 : id));
    return button;
}

#if 0
/**
 * Adjust IMWindow's position.
 *   @cursorPoint: Cursor's abusolute point.
 */
void IMWindow::adjustPosition(QPoint &cursorPoint)
{
	static const int Y_MARGIN = 10;
	int _x, _y;

//	if (now->inherits("QTextEdit")) {
//		QRect cursorRect = dynamic_cast<QTextEdit *>(now)->cursorRect();
//		qDebug() << cursorRect;
//		QPoint global = now->mapToGlobal(QPoint(0,0));
//
//		_y = global.y() - height() - Y_MARGIN;
//
//		_x = global.x() + now->width()/2 - width()/2;
//		move(_x, _y);
//		qDebug() << global.x() << global.y();
//		qDebug() << _x << _y;
//	}
}

void IMWindow::cursorPostionChanged()
{
	QWidget *focusWidget = QApplication::focusWidget();
	if (focusWidget->inherits("QTextEdit")) {
		QTextEdit *nowQTextEdit = dynamic_cast<QTextEdit *>(focusWidget);
		if (nowQTextEdit == NULL)
			return;
		QPoint p = nowQTextEdit->mapFromGlobal(QPoint(nowQTextEdit->cursorRect().left(), nowQTextEdit->cursorRect().top()));
		qDebug() << p;
	}
}

/**
 * Public slot. Response to focus change event.
 *   @old: Widget on which focus once was.
 *   @now: Widget on which focus currently is.
 */
void IMWindow::focusChanged(QWidget */*old*/, QWidget *now)
{
	if (now->inherits("QTextEdit")) {
		QTextEdit *nowQTextEdit = dynamic_cast<QTextEdit *>(now);

		if (nowQTextEdit == NULL)
			return;
		connect(nowQTextEdit, SIGNAL(cursorPositionChanged()),
				this, SLOT(cursorPositionChanged()));
		currentInputWindow = nowQTextEdit;
	} else {

	}
	qDebug() << "focus changed";
}
#endif

/**
 * Toggle visibility of this virtual keyboard.
 */
void IMWindow::toggleVisibility()
{
	if (isVisible()) {
		hide();
	} else {
//		cursorPostionChanged();
		show();
	}
}
