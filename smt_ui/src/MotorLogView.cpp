/*
 * MotorLogView.cpp
 *
 *  Created on: Apr 18, 2010
 *      Author: souken-i
 */

#include <QtGui>
#include "MotorLogView.h"
#include "Global.h"

MotorLogView::MotorLogView(QWidget * parent)
	: QTableView(parent)
{
	model = new QStandardItemModel;
	model->setColumnCount(5);
	QStringList header;
	header << tr("Alarm Code") << tr("Date") << tr("Time") << tr("Motor Status") << tr("Cut Position");
	model->setHorizontalHeaderLabels(header);

	setModel(model);

	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setSelectionBehavior(QAbstractItemView::SelectRows);
	setSelectionMode(QAbstractItemView::SingleSelection);

	// No sorting
	setSortingEnabled(false);

	horizontalHeader()->setResizeMode(QHeaderView::Fixed);
	horizontalHeader()->setStretchLastSection(true);
}

MotorLogView::~MotorLogView()
{
}

void MotorLogView::openLogFile(QString &fileName)
{

    if (!fileName.isEmpty()) {
        QFile file(fileName);

        if (file.open(QFile::ReadOnly | QFile::Text)) {
    		QTextStream in(&file);
    		in.setCodec("UTF-8");

            model->removeRows(0, model->rowCount(QModelIndex()), QModelIndex());

            int row = 0;
            QString alarmName;
            QIcon   alarmIcon;

        	while ( !in.atEnd() ) {
        		QString line = in.readLine();
        		if ( !line.isEmpty() && line.at(0) == '#' ) {  /* comment line */
        			continue;
        		}

        		if ( !line.contains(QString("LOG"), Qt::CaseSensitive)) {
        			continue;
        		}

        		QStringList fields = line.split(',', QString::KeepEmptyParts); /* separated by comma */

        		model->insertRows(row, 1,  QModelIndex());

        		// fields.value(0) is "LOG"
        		int alarmCode = fields.value(1).toInt();
        		setAlarmName(alarmName, alarmCode);
        		setAlarmIcon(alarmIcon, alarmCode);

                model->setData(model->index(row, 0, QModelIndex()),
                		alarmName);
                model->setData(model->index(row, 0, QModelIndex()),
                		alarmIcon, Qt::DecorationRole);

                // Date
                model->setData(model->index(row, 1, QModelIndex()),
                		fields.value(2));
                // Time
                model->setData(model->index(row, 2, QModelIndex()),
                		fields.value(3));
                // Motor Status
                model->setData(model->index(row, 3, QModelIndex()),
                		fields.value(4));
                // Position
                model->setData(model->index(row, 4, QModelIndex()),
                		fields.value(5));

                model->item(row, 0)->setEditable(false);
                model->item(row, 1)->setEditable(false);
                model->item(row, 2)->setEditable(false);
                model->item(row, 3)->setEditable(false);
                model->item(row, 4)->setEditable(false);

                row++;
        	}

        	file.close();
    	}
    }

    // Resize columns
    resizeColumnsToContents();
}

void MotorLogView::setAlarmName(QString &name, int alarmCode)
{
	switch(alarmCode) {
	case Alarm_OverCurrent:
		name = QString(tr("Over Current"));
		break;
	case Alarm_OverLoad:
		name = QString(tr("Over Load"));
		break;
	case Alarm_LoseControl:
		name = QString(tr("Lose Control"));
		break;
	case Alarm_CommunicationError:
		name = QString(tr("Communication Error"));
		break;
	default:
		name = QString(tr("Unknown Error"));
		break;
	}
}

void MotorLogView::setAlarmIcon(QIcon &icon, int alarmCode)
{
	switch(alarmCode) {
	case Alarm_OverCurrent:
		icon = QIcon("images/flag0.png");
		break;
	case Alarm_OverLoad:
		icon = QIcon("images/flag1.png");
		break;
	case Alarm_LoseControl:
		icon = QIcon("images/flag2.png");
		break;
	case Alarm_CommunicationError:
		icon = QIcon("images/flag3.png");
		break;
	default:
		icon = QIcon("images/flag0.png");
		break;
	}
}
