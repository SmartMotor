/*
 * BaseSetupDialog.cpp
 *
 *  Set/Adjust Motor Base
 *
 *  Created on: Jun 19, 2010
 *      Author: souken-i
 */
#include <QtGui>
#include "BaseSetupDialog.h"
#include "Global.h"

BaseSetupDialog::BaseSetupDialog(QWidget *parent)
	: EditDialog(parent)
{

	baseLabel = new QLabel;

	QRegExp rx("\\d{1,4}\\.{0,1}\\d{0,2}");
	QValidator *validator = new QRegExpValidator(rx, this);

	baseEdit = new LineEditVKeyboard;
	baseEdit->setValidator(validator);

	// Layout
	QHBoxLayout *setupLayout = new QHBoxLayout;
	setupLayout->addWidget(baseLabel);
	setupLayout->addWidget(baseEdit);

	insertDrawingArea(setupLayout);

	connect(getOKButton(), SIGNAL(clicked()), this, SLOT(doBaseSetup()));
	connect(getCancelButton(), SIGNAL(clicked()), this, SLOT(close()));

	// Set Focus to input box
	baseEdit->setFocus();

	setGuideIcon(QPixmap("images/guide-basesetup.png"));

	retranslateUi();
}

BaseSetupDialog::~BaseSetupDialog()
{
}

void BaseSetupDialog::retranslateUi()
{
	setTitle(tr("Base Setup"));
	baseLabel->setText(tr("Motor Base:"));
	setGuideText(tr("Input the real position from pusher to cut and press Enter key."));
}

void BaseSetupDialog::doBaseSetup()
{
	QString input = baseEdit->text();
	int base =	MData::toInt(input);

	if (gMotor->isPostionValid(base)) {
		// Send new base position to Motor
		gMotor->setCurPos(base);

		// exit
		close();
	} else {
		baseEdit->clear();
	}
}

void  BaseSetupDialog::keyPressEvent(QKeyEvent *event)
{
	switch(event->key()) {
	case SmtKb::Key_Enter:
	case Qt::Key_Enter:
		doBaseSetup();
		break;
	default:
		QWidget::keyPressEvent(event);
	}
}
