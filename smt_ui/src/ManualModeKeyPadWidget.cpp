/*
 * ManualModeKeyPadWidget.cpp
 *
 *  Created on: Jun 19, 2010
 *      Author: souken-i
 */
#include <QtGui>
#include "ManualModeKeyPadWidget.h"
#include "SmtKb.h"

QString ManualModeKeyPadWidget::iconFiles[]  =
{
	"images/add.png",
	"images/sub.png",
	"images/multiply.png",
	"images/divide.png",
	"images/close.png",
};

int ManualModeKeyPadWidget::keyPadMappings[] =
{
	SmtKb::KP_RelativeInputAdd,
	SmtKb::KP_RelativeInputSub,
	SmtKb::KP_RelativeInputMultiply,
	SmtKb::KP_RelativeInputDivide,
	SmtKb::KP_Hide,
};

ManualModeKeyPadWidget::ManualModeKeyPadWidget(QWidget *parent)
	: QWidget(parent)
{
	int i;

	signalMapper = new QSignalMapper;

	QGridLayout *buttonsLayout = new QGridLayout;
	for (i = 0; i < KP_ButtonsNR; i++) {
		keyPadButton[i] = createButton(i, QIcon(iconFiles[i]));

		if (i < KP_MatrixButtonsNR) {
			// Not close button
			buttonsLayout->addWidget(keyPadButton[i], i / KP_Columns, i % KP_Columns);
		}
	}
    connect(signalMapper, SIGNAL(mapped(int)), this, SIGNAL(clicked(int)));

    // TODO: enable this to hide key pad
//	connect(keyPadButton[Button_Index_Close], SIGNAL(clicked()), this, SLOT(hideButtons()));

	// Layout
	QHBoxLayout *closeLayout = new QHBoxLayout;
	closeLayout->addStretch(1);
	closeLayout->addWidget(keyPadButton[Button_Index_Close]);

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addLayout(closeLayout);
	layout->addLayout(buttonsLayout);
	setLayout(layout);
}

ManualModeKeyPadWidget::~ManualModeKeyPadWidget()
{

}

void ManualModeKeyPadWidget::mousePressEvent ( QMouseEvent * event )
{
	// Show buttons
	if (!keyPadButton[0]->isVisible()){
		for (int i = 0; i < KP_ButtonsNR; i++) {
			keyPadButton[i]->show();
		}
	}
	QWidget::mousePressEvent(event);
}

void ManualModeKeyPadWidget::resizeEvent ( QResizeEvent * event )
{
	QWidget::resizeEvent(event);
}

void ManualModeKeyPadWidget::hideButtons()
{
	if (keyPadButton[0]->isVisible()){
		for (int i = 0; i < KP_ButtonsNR; i++) {
			keyPadButton[i]->hide();
		}
	}
}

Button * ManualModeKeyPadWidget::createButton(int id , const QIcon &icon)
{
	Button *button = new Button;
	button->setIcon(icon);
    connect(button, SIGNAL(clicked()), signalMapper, SLOT(map()));
    button->setProperty("custom", true);
    signalMapper->setMapping(button, getKeyPadCode(id));
    return button;
}

int ManualModeKeyPadWidget::getKeyPadCode(int button_id)
{
	return keyPadMappings[button_id];
}
