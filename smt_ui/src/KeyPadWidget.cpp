/*
 * KeyPadWidget.cpp
 *
 *  Created on: Jun 14, 2010
 *      Author: souken-i
 */

#include <QtGui>

#include "KeyPadWidget.h"

KeyPadWidget::KeyPadWidget(int row, int column, QWidget *parent)
	: QWidget(parent)
{
	int i;

	signalMapper = new QSignalMapper;

	_keyPadRows = row;
	_keyPadColumns = column;

	if (_keyPadRows <= 0) {
		_keyPadRows = 1;
	}

	if (_keyPadColumns <= 0) {
		_keyPadColumns = 1;
	}

	_keyPadButtonNumber = _keyPadRows * _keyPadColumns + 1;
	keyPadButton = new Button *[_keyPadButtonNumber];

	QGridLayout *buttonsLayout = new QGridLayout;
	for (i = 0; i < _keyPadButtonNumber; i++) {
		keyPadButton[i] = createButton(i);
		if (i != _keyPadButtonNumber - 1) {
			// Not close button
			buttonsLayout->addWidget(keyPadButton[i], i / _keyPadColumns, i % _keyPadColumns);
		}
	}
    connect(signalMapper, SIGNAL(mapped(int)), this, SIGNAL(clicked(int)));

    // close button
    keyPadButton[_keyPadButtonNumber-1]->setIcon(QIcon("images/close.png"));

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(keyPadButton[_keyPadButtonNumber-1]); // close button
    layout->addLayout(buttonsLayout);
    layout->setContentsMargins(0, 0, 0, 0);

    setLayout(layout);
}

KeyPadWidget::~KeyPadWidget()
{
	// nothing to do
}

Button * KeyPadWidget::createButton(int id /*, const QIcon &icon */)
{
	Button *button = new Button;
//	button->setIcon(icon);
    connect(button, SIGNAL(clicked()), signalMapper, SLOT(map()));
    button->setProperty("custom", true);
    signalMapper->setMapping(button, id);
    return button;
}
void KeyPadWidget::setIcon(int button_id, const QIcon &icon)
{
	if (button_id >= _keyPadButtonNumber) {
		return;
	}

	keyPadButton[button_id]->setIcon(icon);
}

void KeyPadWidget::setRowVisable(bool visible, int row)
{
	int i, start;

	if (row >= _keyPadRows) {
		return;
	}

	start = row * _keyPadColumns;
	for (i = 0; i < _keyPadColumns; i++) {
		keyPadButton[start + i]->setVisible(visible);
	}
}
