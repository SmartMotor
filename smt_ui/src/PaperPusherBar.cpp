/*
 * PaperPusherBar.cpp
 *
 *  Created on: 2010/03/07
 *      Author: crazysjf
 */

#include <QtGui>
#include <QPoint>
#include <QPainter>

#include "PaperPusherBar.h"
#include "Debug.h"
#include "Global.h"

#define WIDGET_HEIGHT_PROPORTION     6
#define POINTS_NUM                   6
#define TRIANGLE_WIDTH_PROPORTION    16
#define TRIANGLE_HEIGHT_PROPORTION   4

PaperPusherBar::PaperPusherBar(QWidget *parent)
	: QWidget(parent)
{
#ifdef MOTOR_TEST_WIDGET_ENABLE
	motorTestWidget = new MotorTestWidget;
#endif

	QFont font = this->font();
	font.setPointSize(gSysConfig->getSysFontSize());
	setFont(font);

	programLabel = new QLabel;
	programIDLabel = new QLabel;
	stepLabel = new QLabel;
	stepIDLabel = new QLabel;
	programIDLabel->setAlignment(Qt::AlignCenter);
	stepIDLabel->setAlignment(Qt::AlignCenter);

	QHBoxLayout *programIDLayout = new QHBoxLayout;
	programIDLayout->addWidget(programLabel);
	programIDLayout->addWidget(programIDLabel);

	QHBoxLayout *stepIDLayout = new QHBoxLayout;
	stepIDLayout->addWidget(stepLabel);
	stepIDLayout->addWidget(stepIDLabel);

	QVBoxLayout *idLayout = new QVBoxLayout;
	idLayout->addStretch();
	idLayout->addLayout(programIDLayout);
	idLayout->addLayout(stepIDLayout);
	idLayout->addStretch();

	paperPusher = new PaperPusher;

	distanceLabel = new QLabel;
	cutCountLabel = new QLabel;

	distanceLabel->setMinimumWidth(96);
	distanceLabel->setMaximumWidth(256);
	distanceLabel->setAlignment(Qt::AlignCenter);
	distanceLabel->setAutoFillBackground(true);
	updateFontSizeColor();

	cutCountLabel->setMinimumWidth(96);
	cutCountLabel->setMaximumWidth(256);
	cutCountLabel->setAlignment(Qt::AlignCenter);
	cutCountLabel->setAutoFillBackground(true);
	QPalette palette;
	palette.setColor(QPalette::Window, QColor("yellow"));
	palette.setColor(QPalette::WindowText, QColor("black"));
	cutCountLabel->setPalette(palette);

	// Update if count changed
	connect(gMotor, SIGNAL(cutCountChanged(int)), this, SLOT(updateCutCount(int)));

	distanceIconLabel  = new QLabel;
	cutCountIconLabel = new QLabel;
	QPixmap cutCountPixmap = QPixmap("images/count.bmp");
	cutCountIconLabel->setPixmap(cutCountPixmap);
	cutCountIconLabel->setMaximumWidth(80);

	QPixmap distancePixmap = QPixmap("images/distance.bmp");
	distanceIconLabel->setPixmap(distancePixmap);
	distanceIconLabel->setMaximumWidth(80);


	QHBoxLayout *rightTopLayout = new QHBoxLayout;
	rightTopLayout->addWidget(distanceLabel);
	rightTopLayout->addWidget(distanceIconLabel);

	QHBoxLayout *rightBottomLayout = new QHBoxLayout;
	rightBottomLayout->addWidget(cutCountLabel);
	rightBottomLayout->addWidget(cutCountIconLabel);

	QVBoxLayout *rightLayout = new QVBoxLayout;
	rightLayout->addStretch(1);
	rightLayout->addLayout(rightTopLayout);
	rightLayout->addLayout(rightBottomLayout);
	rightLayout->addStretch(1);

	QHBoxLayout *layout = new QHBoxLayout;
	layout->addSpacing(20);
	layout->addLayout(idLayout);
	layout->addSpacing(20);
	layout->addWidget(paperPusher);
	layout->addSpacing(20);
	layout->addLayout(rightLayout);
	setLayout(layout);
	setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
	setContentsMargins(0, 0, 0, 0);

	// TODO: read initial value from other object
	updateProgramStep(100, 1);
	updateCutCount(gMotor->getCutCount());
	updateCutDistance(100);

	retranslateUi();
}

#if 0
void PaperPusherBar::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	QSize mySize = this->size();

	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.setPen(QPen(Qt::gray, 2, Qt::SolidLine, Qt::RoundCap, Qt::MiterJoin));

	QLinearGradient gradient(mySize.width() / 2, mySize.height(), mySize.width() / 2, 0);
	gradient.setColorAt(0.0, Qt::gray);
	gradient.setColorAt(1.0, Qt::white);

	painter.setBrush(QBrush(gradient));

	//
	//	Drawing the damn 6-edged polygon
	//
	//  width/height of the 2 triangles in the left/right bottom corner
	//	width/height = window width/height / *_PROPORTION
	int trianleWidth  = mySize.width() / TRIANGLE_WIDTH_PROPORTION;
	int trianleHeight = mySize.height() / TRIANGLE_HEIGHT_PROPORTION;

	QPoint points[POINTS_NUM] = {
			QPoint(0, 0),
			QPoint(mySize.width() - 1,					0),
			QPoint(mySize.width() - 1,					mySize.height() - 1 - trianleHeight),
			QPoint(mySize.width() - 1 - trianleWidth,	mySize.height() - 1),
			QPoint(trianleWidth,						mySize.height() - 1),
			QPoint(0, 									mySize.height() - 1 - trianleHeight)
	};
	painter.drawPolygon(points, POINTS_NUM);
}
#endif

QSize PaperPusherBar::sizeHint() const
{
	//	Default height is main window's 1/6
	QSize size(DEFAUT_MAIN_W, DEFAUT_MAIN_H / WIDGET_HEIGHT_PROPORTION);

	return size;
}

void PaperPusherBar::updateProgramID(int id)
{
	programIDLabel->setText(QString::number(id));
}

void PaperPusherBar::updateProgramName(QString &name)
{
	programIDLabel->setText(name);
}

void PaperPusherBar::updateProgramStep(int total, int step)
{
	QString str;
	if (total > 0) {
		// step starts from 0(the first data)
		str = QString("%1 - %2").arg(total).arg(step+1);
		stepIDLabel->setText(str);
	} else {
		str = QString("0 - 0");
	}

	stepIDLabel->setText(str);
}

void PaperPusherBar::updateCutCount(int count)
{
	cutCountLabel->setText(QString::number(count));
}

void PaperPusherBar::updateCutDistance(int distance)
{
	distanceLabel->setText(MData::toString(distance));
}

void PaperPusherBar::updateFontSizeColor()
{
	QPalette palette;
	QColor backgroundColor = gSysConfig->getDistanceBackgroundColor();
	QColor fontColor = gSysConfig->getDistanceFontColor();
	palette.setColor(QPalette::Window, backgroundColor);
	palette.setColor(QPalette::WindowText, fontColor);
	distanceLabel->setPalette(palette);

	QFont font = distanceLabel->font();
	font.setPointSize(gSysConfig->getDistanceFontSize());
	distanceLabel->setFont(font);

	// Child Widget
	paperPusher->updateFontSizeColor();
}

void PaperPusherBar::retranslateUi()
{
	programLabel->setText(tr("Program"));
	stepLabel->setText(tr("Step"));
}

#ifdef MOTOR_TEST_WIDGET_ENABLE
void PaperPusherBar::mousePressEvent( QMouseEvent * /* ev */)
{
	if (motorTestWidget->isHidden()) {
		motorTestWidget->show();
	} else {
		motorTestWidget->hide();
	}
}
#endif
