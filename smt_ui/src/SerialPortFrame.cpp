/*
 * SerialPortFrame.cpp
 *
 *  Created on: Mar 24, 2010
 *      Author: souken-i
 */

#include <QDebug>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <sys/time.h>

#include "SerialPortFrame.h"


SerialPortFrame::SerialPortFrame()
{
	_frameHeader    = '%';
	_frameTail      = 'B';
	portStatus      = 0;
	threadShouldStop = false;

	mutex = new QMutex( QMutex::Recursive );
	recvBuffer.resize(RxFrameDataLength);
	recvBuffer.fill(0);
}

SerialPortFrame::~SerialPortFrame()
{
	stopWorkThread();

	if ( isOpen() ) {
		tcflush(fd, TCIOFLUSH);
		tcsetattr(fd, TCSAFLUSH | TCSANOW, &old_termios);
		::close(fd);
		portStatus = 0;
	}

	delete mutex;
}

/* Receive Child Thread Routine */
void SerialPortFrame::run()
{
	int len;

	while ( !threadShouldStop && isOpen()) {
		unsigned char header;
		len = ::read(fd, &header, 1);  /* block read */
		if ((len == 1) && isFrameHeader(header)) {
			QByteArray buffer(FrameLength-1, 0);
// TODO FIXME
// The sleep value is ugly
// We want to wait for at a whole frame is received
// For 9600 bps, total time is 11 * (8+1+1) / 9600 = 11.5 ms
// So we sleep 10 ms here
			usleep(10000);
			len = ::read(fd, buffer.data(), FrameLength-1);
			if (len == (FrameLength-1)) {
#ifdef SPF_DEBUG // XXX
				::printf("Get frame: %s\n", buffer.data());
#endif
				buffer.prepend(_frameHeader);
				handleReceivedFrame(buffer);
			}
#ifdef SPF_DEBUG // XXX
			else {
				::printf("Get imcomplete frame: %s len = %d\n",
						buffer.data(), len);
			}
#endif
		}
	}

	quit();
}

void SerialPortFrame::stopWorkThread()
{
	threadShouldStop = true;
}

bool SerialPortFrame::isOpen()
{
	return (portStatus == 1);
}

int SerialPortFrame::open(const char *portname)
{

	/*
	 * We use block read in the receive thread, so
	 * don't set the O_NODELAY flag
	 */
	fd = ::open (portname, O_RDWR | O_NOCTTY);
	if (fd != -1) {
		tcgetattr(fd,&old_termios);
		serialOptions = old_termios;         /* Make a copy */
		cfsetispeed(&serialOptions, B9600);
		cfsetospeed(&serialOptions, B9600);  /* 9600 */
		serialOptions.c_cflag &= ~PARENB;    /* 8N1   */
		serialOptions.c_cflag &= ~CSTOPB;
		serialOptions.c_cflag &= ~CSIZE;
		serialOptions.c_cflag |= CS8;
		serialOptions.c_cflag &= ~CRTSCTS;
		serialOptions.c_cflag |= (CLOCAL | CREAD);
		serialOptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); /* RAW input */

		tcsetattr(fd, TCSANOW, &serialOptions);
		portStatus = 1;

		::printf("Open device %s OK. fd %d\n", portname, fd);
	} else {
		qDebug() << "Can't open serial port device" << endl;
		return -ENODEV;
	}

	/* Start child thread */
	start();

	return 0;
}

QByteArray & SerialPortFrame::readFrame()
{
	QMutexLocker lock(mutex);
	return recvBuffer;
}

/*
 * Package the command frame:
 *   - append frame header and tail
 *   - compute the checksum and insert to frame
 * Send frame out
 */
int SerialPortFrame::writeFrame(QByteArray & buffer)
{
	QMutexLocker lock(mutex);
	int len = buffer.size();

	if (len != TxFrameDataLength) {
		// TODO Warning
	}

	// Header
	buffer.prepend(_frameHeader);
	buffer.resize(FrameLength);

	// Checksum
	char checkSum = computeChecksum(buffer, TxFrameCheckSumIndex);
	buffer[TxFrameCheckSumIndex] = checkSum;

	// Tail
	buffer[TxFrameTailIndex] = _frameTail;

	// Send
	return ::write(fd, buffer.data(), FrameLength);
}

// Compute frame checksum
//
char SerialPortFrame::computeChecksum(QByteArray & buffer, int length)
{
	char sum = 0;

	for (int i = 0; i < length; i++) {
		sum ^= buffer.at(i);
	}

	return sum;
}

bool SerialPortFrame::isFrameHeader(char ch)
{
	return (ch == _frameHeader);
}

bool SerialPortFrame::isFrameTail(char ch)
{
	return (ch == _frameTail);
}

bool SerialPortFrame::isReceivedFrameValid(QByteArray & buffer)
{
	if(buffer.isEmpty()) {
		qDebug() << "Receive buffer empty" << endl;
		return false;
	}

	if(buffer.size() != FrameLength) {
		qDebug() << "Receive buffer size error: "<< buffer.size() << endl;
		qDebug() << "Buffer: " << buffer.data() << endl;
		return false;
	}

#if 0
	// mtcp-v2.1 removes tail from T and F status frame

	// We must check the tail
	if (!isFrameTail(buffer.at(FrameTailIndex))) {
		return false;
	}
#endif

#if 1 // XXX Disable these check for test
	char checksum = computeChecksum(buffer, RxFrameCheckSumIndex);
	if (checksum != buffer.at(RxFrameCheckSumIndex)) {
		qDebug() << "Receved frame checksum error: " << buffer.data();
		qDebug() << "Should be " << checksum << " But it is " << buffer.at(RxFrameCheckSumIndex);
		return false;
	}
#endif

	return true;
}

// buffer is the whole frame: includes % header
void SerialPortFrame::handleReceivedFrame(QByteArray & buffer)
{
	QMutexLocker lock(mutex);

	if ( isReceivedFrameValid(buffer) ) {
		/*
		 * Only copy the frame data to the buffer
		 */
		// recvBuffer = buffer;
		::strncpy(recvBuffer.data(), &buffer.data()[1], RxFrameDataLength);

		/*
		 * Emit signal
		 */
		emit readyRead();
	} else {
		emit checksumError();
	}
}

#if 0
void SerialPortFrame::sendFrameTest()
{
	QByteArray test("HelloABCD");
	int len = writeFrame(test);

	::printf("Send out: %d bytes size %d\n", len, test.size());
}
#endif
/* Local Variables:  */
/* c-basic-offset: 4 */
/* End:              */
