/*
 * SensorStatusWidget.cpp
 *
 *  Created on: Jun 18, 2010
 *      Author: souken-i
 */

#include <QtGui>
#include "SensorStatusWidget.h"
#include "Debug.h"
#include "Global.h"

QString SensorStatusWidget::imageFiles[SensorStatusWidget::IMAGE_NR] = {
		"images/sensors-both-off.png",
		"images/sensors-cut-on.png",
		"images/sensors-pusher-on.png",
		"images/sensors-both-on.png",
		"images/start.png"
};

SensorStatusWidget::SensorStatusWidget(QWidget *parent)
	: QWidget (parent)
{
	_current_image = IMAGE_SENSORS_PUSHER_ON;

	imageLabel = new QLabel;
    imageLabel->setBackgroundRole(QPalette::Base);
    imageLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	imageLabel->setAlignment(Qt::AlignCenter);
	/*
	 * We use QImage.scaled() method, do set this attribute to true because
	 * setScaledContents() will not keep the original ratio.
	 */
	//imageLabel->setScaledContents(true);

	// signals and slots
	connect(gMotor, SIGNAL(cutStatusChanged(int)), this, SLOT(cutStatusEvent(int)));
	connect(gMotor, SIGNAL(pusherStatusChanged(int)), this, SLOT(pusherStatusEvent(int)));

	QHBoxLayout *layout = new QHBoxLayout;
	layout->addWidget(imageLabel);
	setLayout(layout);
}

SensorStatusWidget::~SensorStatusWidget()
{
	// Does nothing
}

void SensorStatusWidget::resizeEvent( QResizeEvent * event )
{
	/* scale image with original ratio */
	QImage image(imageFiles[_current_image]);
	QImage scaledImage = image.scaled(size(), Qt::KeepAspectRatio);

	imageLabel->setPixmap(QPixmap::fromImage(scaledImage));

	QWidget::resizeEvent(event);
}

void SensorStatusWidget::changeImage(int imageID)
{
	if (imageID < IMAGE_NR) {
		QImage image(imageFiles[imageID]);
		QImage scaledImage = image.scaled(size(), Qt::KeepAspectRatio);

		imageLabel->setPixmap(QPixmap::fromImage(scaledImage));

		_current_image = imageID;
	}
}

void SensorStatusWidget::toggleImage()
{
	if (_current_image != IMAGE_START) {
		_current_image = IMAGE_START;
	} else {
		// default
		_current_image = IMAGE_SENSORS_PUSHER_ON;
	}

	changeImage(_current_image);
}

void SensorStatusWidget::cutStatusEvent(int status)
{
	// If current is showing the start picture, skip
	if ((_current_image == IMAGE_START) || !isVisible())
		return;

	int imageID = _current_image & 0x02;  // bit 0 is for cut status

	// When cut is down, LED is on
	if (status == Motor::CutDown)
		imageID |= 0x1;

	changeImage(imageID);
}
void SensorStatusWidget::pusherStatusEvent(int status)
{
	// If current is showing the start picture, skip
	if ((_current_image == IMAGE_START) || !isVisible())
		return;

	int imageID = _current_image & 0x01; // bit 1 is for pusher status

	// When pusher is up, LED is on
	if (status == Motor::PusherUp)
		imageID |= 0x2;

	changeImage(imageID);
}
