/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "Calibration.h"

#include <QWSPointerCalibrationData>
#include <QPainter>
#include <QFile>
#include <QTimer>
#include <QApplication>
#include <QDesktopWidget>
#include <QMouseEvent>
#include <QScreen>
#include <QWSServer>

#include "SmtKb.h"

//! [0]
Calibration::Calibration()
{
    QRect desktop = QApplication::desktop()->geometry();
    desktop.moveTo(QPoint(0, 0));
    setGeometry(desktop);

    setFocusPolicy(Qt::StrongFocus);
    setFocus();
    setModal(true);
//! [0]

//! [1]
    int width = qt_screen->deviceWidth();
    int height = qt_screen->deviceHeight();

    int dx = 50;
    int dy = 50;

    QPoint *points = data.screenPoints;
    points[QWSPointerCalibrationData::TopLeft] = QPoint(dx, dy);
    points[QWSPointerCalibrationData::BottomLeft] = QPoint(dx, height - dy);
    points[QWSPointerCalibrationData::BottomRight] = QPoint(width - dx, height - dy);
    points[QWSPointerCalibrationData::TopRight] = QPoint(width - dx, dy);
    points[QWSPointerCalibrationData::Center] = QPoint(width / 2, height / 2);
//! [1]

//! [2]
    pressCount = 0;
}
//! [2]

//! [3]
Calibration::~Calibration()
{
}
//! [3]

//! [4]
int Calibration::exec()
{
    QWSServer::mouseHandler()->clearCalibration();
    grabMouse();
    activateWindow();
    int ret = QDialog::exec();
    releaseMouse();
    return ret;
}
//! [4]

//! [5]
void Calibration::paintEvent(QPaintEvent*)
{
    QPainter p(this);
    p.fillRect(rect(), Qt::white);

    QPoint point = data.screenPoints[pressCount];

    int width = qt_screen->deviceWidth();
    int height = qt_screen->deviceHeight();

    // Map to logical coordinates in case the screen is transformed
    QSize screenSize(width, height);
    point = qt_screen->mapFromDevice(point, screenSize);

#if 0
    p.fillRect(point.x() - 6, point.y() - 1, 13, 3, Qt::black);
    p.fillRect(point.x() - 1, point.y() - 6, 3, 13, Qt::black);
#else
    // Guide
    p.setPen(Qt::blue);
    QFont font = this->font();
    font.setBold(true);
    p.setFont(font);
    p.drawText(width/4, height/4, width/2, height/4, Qt::AlignCenter, tr("Touch cross bar to calibrate, press F2 to exit."));

    // Cross bar
    int x = point.x();
    int y = point.y();

    p.setPen(Qt::red);
    p.drawLine(x - 10, y, x - 2, y);
    p.drawLine(x + 2, y, x + 10, y);
    p.drawLine(x, y - 10, x, y - 2);
    p.drawLine(x, y + 2, x, y + 10);

	p.drawLine(x - 6, y - 9, x - 9, y - 9);
	p.drawLine(x - 9, y - 8, x - 9, y - 6);
	p.drawLine(x - 9, y + 6, x - 9, y + 9);
	p.drawLine(x - 8, y + 9, x - 6, y + 9);
	p.drawLine(x + 6, y + 9, x + 9, y + 9);
	p.drawLine(x + 9, y + 8, x + 9, y + 6);
	p.drawLine(x + 9, y - 6, x + 9, y - 9);
	p.drawLine(x + 8, y - 9, x + 6, y - 9);
#endif

}
//! [5]

//! [6]
void Calibration::mouseReleaseEvent(QMouseEvent *event)
{
    // Map from device coordinates in case the screen is transformed
    QSize screenSize(qt_screen->width(), qt_screen->height());
    QPoint p = qt_screen->mapToDevice(event->pos(), screenSize);

    data.devPoints[pressCount] = p;

    if (++pressCount < 5)
        repaint();
    else
        accept();
}
//! [6]

//! [7]
void Calibration::accept()
{
    Q_ASSERT(pressCount == 5);
    QWSServer::mouseHandler()->calibrate(&data);
    QDialog::accept();
}
//! [7]

void Calibration::keyPressEvent(QKeyEvent *event)
{
	switch(event->key()) {
	case SmtKb::Key_F2:
		close();
		break;
	default:
		break;
	}
}

