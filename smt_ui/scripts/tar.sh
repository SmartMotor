#! /bin/sh

# 
# Create a release tarball
# 
set -ex

TOPDIR=`pwd`

TAR=tar
DATE=`date +%Y%m%d`

OUTFILE=smt_ui-${DATE}.tar.gz

FILES="smt_ui images qss data translations zhpy_table.db"


cd $TOPDIR 

WORKDIR=.workdir/smt_ui

test -d $WORKDIR && rm -rf WORKDIR
mkdir -p $WORKDIR

test -d smt_ui && rm -rf smt_ui
for f in $FILES
do
    cp -rf $f ${WORKDIR}/
done

cd .workdir

$TAR zcf  ../$OUTFILE smt_ui

cd $TOPDIR
rm -rf $WORKDIR

exit 0