=========================================================
                      如何更新图标
=========================================================

目录：
  1. 文件组织结构
  2. 更新图标步骤
  3. 图标文件名对应关系表
  4. 修改记录


1. 文件组织结构
===============

程序压缩包解压之后生成 smt_ui 的目录。在该目录下，有如下文件和文件夹：

    smt_ui                  ---- 顶层目录
    |-- data                ---- 数据目录，存放数据和配置文件
    |-- images              ---- 图标文件目录
    |   |-- F1-setting.png  ---- 图标文件
    |   |-- ...
    |   |-- start.png
    |   |-- ...
    |   `-- zoom-out.png
    |-- qss                 ---- 程序数据
    |-- smt_ui              ---- 程序数据
    |-- translations        ---- 程序数据
    `-- zhpy_table.db       ---- 程序数据

所有图标均存放在 smt_ui/images/ 目录下。

2. 更新图标步骤
===============

    程序中使用固定文件名加载图标，如 F1-setting.png. 如果该图标文件不存
在，则不显示图标。因此，更新图标也仅仅是用新图标文件替换原有图标文件即可。 
注意文件名必须相同，而且文件名区分大小写 。


3. 图标文件名对应关系表
=======================

  界面              图标文件名          图标说明
  主界面            md-edit.png        编程模式按钮
  主界面            md-manual.png      手动模式按钮
  主界面            md-autorun.png     自动模式按钮
  主界面            md-training.png    示教模式按钮 
  主界面            md-autocut.png     自刀模式按钮
  主界面            clear.png          清除输入框数据
  主界面            enter.png          确定

  编程界面          F1-labeledit.png   标签编程
  编程界面          F2-averageedit.png 等分编程
  编程界面          F3-files.png       选择程序
  编程界面          F4-flag-pusher.png 推纸标志
  编程界面          F5-flag-cut.png    刀标志
  编程界面          F6-calc.png        计算器

  自动界面          F1-clear.png       清零
  自动界面	   F2-basesetup.png   基准设置
  自动界面	   F3-files.png       选择程序

  手动界面          F1-setting.png     机器设置
  手动界面  	   F2-maintenance.png 机器维护
  手动界面  	   F3-basesetup.png   基准设置
  手动界面  	   F4-clear.png       清零
  手动界面  	   F5-sensor.png      传感器状态
  手动界面  	   F6-calc.png        计算器

  自刀界面          F1-clear.png       清零   
  自刀界面          F2-basesetup.png   基准设置
  自刀界面          F3-files.png       选择程序

  示教界面          F1-labeledit.png   标签编程
  示教界面          F2-averageedit.png 等分编程
  示教界面          F3-files.png       选择程序
  示教界面          F4-flag-pusher.png 推纸标志
  示教界面          F5-flag-cut.png    刀标志
  示教界面          F6-calc.png        计算器
    

4. 修改记录
===========

2010/06/19  初版
