<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>AdvancedMotorParametersPage</name>
    <message>
        <source>About</source>
        <translation type="unfinished">版本信息</translation>
    </message>
</context>
<context>
    <name>AverageEditor</name>
    <message>
        <source>Number mode</source>
        <translation type="unfinished">等分数模式</translation>
    </message>
    <message>
        <source>Value mode</source>
        <translation type="unfinished">等分值模式</translation>
    </message>
    <message>
        <source>Average Number Edit</source>
        <translation type="unfinished">等分数编程</translation>
    </message>
    <message>
        <source>Average Number</source>
        <translation type="unfinished">等分数</translation>
    </message>
    <message>
        <source>Average Value Edit</source>
        <translation type="unfinished">等分值编程</translation>
    </message>
    <message>
        <source>Average Length</source>
        <translation type="unfinished">等分值</translation>
    </message>
    <message>
        <source>Total Length</source>
        <translation type="unfinished">总长</translation>
    </message>
    <message>
        <source>Press button to toggle average mode</source>
        <translation type="unfinished">按模式按钮切换等分模式</translation>
    </message>
</context>
<context>
    <name>BaseSetupDialog</name>
    <message>
        <source>Base Setup</source>
        <translation type="unfinished">基准设置</translation>
    </message>
    <message>
        <source>Motor Base:</source>
        <translation type="unfinished">基准:</translation>
    </message>
    <message>
        <source>Input the real position from pusher to cut and press Enter key.</source>
        <translation type="unfinished">输入推纸器到切刀的实际尺寸并按确定键.</translation>
    </message>
</context>
<context>
    <name>Calculator</name>
    <message>
        <source>.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>+/-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Backspace</source>
        <translation type="unfinished">退格</translation>
    </message>
    <message>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sqrt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>x²</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>1/x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>=</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="unfinished">退出</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Press F6 to Exit.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Calibration</name>
    <message>
        <source>Touch cross bar to calibrate, press F2 to exit.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CurrentMeter</name>
    <message>
        <source>[Amp]</source>
        <translation type="unfinished">[安培]</translation>
    </message>
</context>
<context>
    <name>DateTimePage</name>
    <message>
        <source>Current Time:</source>
        <translation type="unfinished">系统时间:</translation>
    </message>
    <message>
        <source>Hour:</source>
        <translation type="unfinished">小时:</translation>
    </message>
    <message>
        <source>Minute:</source>
        <translation type="unfinished">分钟:</translation>
    </message>
    <message>
        <source>Second:</source>
        <translation type="unfinished">秒:</translation>
    </message>
</context>
<context>
    <name>EditDialog</name>
    <message>
        <source>OK</source>
        <translation type="unfinished">输入</translation>
    </message>
    <message>
        <source>Cancell</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FsModel</name>
    <message>
        <source>File</source>
        <translation type="unfinished">程序</translation>
    </message>
    <message>
        <source>Last Modified</source>
        <translation type="unfinished">修改时间</translation>
    </message>
    <message>
        <source>Use Count</source>
        <translation type="unfinished">使用计数</translation>
    </message>
    <message>
        <source>Cut Count</source>
        <translation type="unfinished">总刀数</translation>
    </message>
    <message>
        <source>First Position</source>
        <translation type="unfinished">首刀位置</translation>
    </message>
    <message>
        <source>First Flags</source>
        <translation type="unfinished">首刀标志</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">程序描述</translation>
    </message>
</context>
<context>
    <name>HeaderLabel</name>
    <message>
        <source> ... </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeyboardCheck</name>
    <message>
        <source>Keyboard Check</source>
        <translation type="unfinished">键盘检测</translation>
    </message>
    <message>
        <source>Press any key ... Double press Enter to exit.</source>
        <translation type="unfinished">按任意键测试...连续按确认键两次退出.</translation>
    </message>
    <message>
        <source>Enter</source>
        <translation type="unfinished">确认</translation>
    </message>
    <message>
        <source>Backspace</source>
        <translation type="unfinished">退格</translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="unfinished">插入</translation>
    </message>
    <message>
        <source>Modify</source>
        <translation type="unfinished">修改</translation>
    </message>
    <message>
        <source>Up</source>
        <translation type="unfinished">上</translation>
    </message>
    <message>
        <source>Down</source>
        <translation type="unfinished">下</translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="unfinished">左</translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="unfinished">右</translation>
    </message>
    <message>
        <source>Page Up</source>
        <translation type="unfinished">上页</translation>
    </message>
    <message>
        <source>Page Down</source>
        <translation type="unfinished">下页</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished">删除</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="unfinished">退出</translation>
    </message>
    <message>
        <source>Program</source>
        <translation type="unfinished">程序</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="unfinished">前进</translation>
    </message>
    <message>
        <source>Backward</source>
        <translation type="unfinished">后退</translation>
    </message>
    <message>
        <source>Run</source>
        <translation type="unfinished">运行</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="unfinished">帮助</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="unfinished">清除</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="unfinished">停止</translation>
    </message>
    <message>
        <source>Pusher</source>
        <translation type="unfinished">推纸</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished">编辑</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="unfinished">自动</translation>
    </message>
    <message>
        <source>Manual</source>
        <translation type="unfinished">手动</translation>
    </message>
    <message>
        <source>AutoCut</source>
        <translation type="unfinished">自刀</translation>
    </message>
    <message>
        <source>Training</source>
        <translation type="unfinished">示教</translation>
    </message>
    <message>
        <source>Fast Backward</source>
        <translation type="unfinished">快速后退</translation>
    </message>
    <message>
        <source>Fast Forward</source>
        <translation type="unfinished">快速前进</translation>
    </message>
    <message>
        <source>Unknown Key</source>
        <translation type="unfinished">未知按键</translation>
    </message>
    <message>
        <source>Open keyboard device error.</source>
        <translation type="unfinished">键盘连接错误.</translation>
    </message>
    <message>
        <source>Keyboard is connected.</source>
        <translation type="unfinished">键盘已连接.</translation>
    </message>
    <message>
        <source>Keyboard is not connected.</source>
        <translation type="unfinished">未检测到键盘.</translation>
    </message>
</context>
<context>
    <name>LabelEditor</name>
    <message>
        <source>Label Edit</source>
        <translation type="unfinished">标签编程</translation>
    </message>
    <message>
        <source>Total Length</source>
        <translation type="unfinished">总长</translation>
    </message>
    <message>
        <source>Label Length</source>
        <translation type="unfinished">标签长度</translation>
    </message>
    <message>
        <source>Unused Length</source>
        <translation type="unfinished">废边长度</translation>
    </message>
    <message>
        <source>Input length and press ENTER</source>
        <translation type="unfinished">输入数据按回车键</translation>
    </message>
</context>
<context>
    <name>MachineSettingDialog</name>
    <message>
        <source>Machine Parameter Settings</source>
        <translation type="unfinished">机器参数设置</translation>
    </message>
    <message>
        <source>Machine Maintenance</source>
        <translation type="unfinished">机器保养</translation>
    </message>
    <message>
        <source>Date And Time Settings</source>
        <translation type="unfinished">时间日期设置</translation>
    </message>
    <message>
        <source>System Settings</source>
        <translation type="unfinished">系统设置</translation>
    </message>
    <message>
        <source>Advanced Machine Settings</source>
        <translation type="unfinished">高级机器参数设置</translation>
    </message>
    <message>
        <source>Machine Settings</source>
        <translation type="unfinished">机器设置</translation>
    </message>
    <message>
        <source>F1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>F2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>F3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>F5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Touch Screen Calibration</source>
        <translation type="unfinished">触摸屏校准</translation>
    </message>
    <message>
        <source>F6</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainFrame</name>
    <message>
        <source>Select a program file or edit program description.</source>
        <translation type="unfinished">选择程序文件，或者编辑程序说明.</translation>
    </message>
    <message>
        <source>Program Select</source>
        <translation type="unfinished">程序选择</translation>
    </message>
    <message>
        <source>Input data between %1 - %2 [mm]</source>
        <translation type="unfinished">输入数据范围 %1 - %2 [mm]</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished">编程</translation>
    </message>
    <message>
        <source>Press RUN button to run this program automatically.</source>
        <translation type="unfinished">按运行键自动运行程序.</translation>
    </message>
    <message>
        <source>Auto Run</source>
        <translation type="unfinished">自动运行</translation>
    </message>
    <message>
        <source>Input cut position and press RUN button.</source>
        <translation type="unfinished">输入数据，按运行键运行.</translation>
    </message>
    <message>
        <source>Manual Run</source>
        <translation type="unfinished">手动运行</translation>
    </message>
    <message>
        <source>Dangerous!!! Pay care to the cut.</source>
        <translation type="unfinished">危险!!! 按运行键自动运行程序.</translation>
    </message>
    <message>
        <source>Auto Cut Run</source>
        <translation type="unfinished">自动切刀</translation>
    </message>
    <message>
        <source>Run one data to add it to program.</source>
        <translation type="unfinished">运行数据并自动添加到程序中.</translation>
    </message>
    <message>
        <source>Training</source>
        <translation type="unfinished">示教</translation>
    </message>
    <message>
        <source>Use MODE buttons on the left size to change mode.</source>
        <translation type="unfinished">使用屏幕左边的模式键切换操作模式.</translation>
    </message>
    <message>
        <source>Input Data:</source>
        <translation type="unfinished">输入数据:</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="unfinished">清除</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished">输入</translation>
    </message>
    <message>
        <source>Run</source>
        <translation type="unfinished">运行</translation>
    </message>
    <message>
        <source>&lt;h2&gt;&lt;font color=red&gt;AutoCut mode is Dangerous!&lt;/font&gt;&lt;/h2&gt;</source>
        <translation type="unfinished">&lt;h2&gt;&lt;font color=red&gt;自刀模式高度危险！&lt;/font&gt;&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>Do you really want to enter AutoCut mode?</source>
        <translation type="unfinished">是否进入自刀模式?</translation>
    </message>
    <message>
        <source>Delete all data?</source>
        <translation type="unfinished">是否删除所有数据?</translation>
    </message>
    <message>
        <source>&lt;font color=red&gt;Inputed data is out of range.&lt;/font&gt;</source>
        <translation type="unfinished">&lt;font color=red&gt;输入数据超出范围。请重新输入&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Delete current data</source>
        <translation type="unfinished">删除当前数据</translation>
    </message>
    <message>
        <source>Delete all flags of current data</source>
        <translation type="unfinished">清除当前数据的所有标志</translation>
    </message>
    <message>
        <source>Insert one data to current index</source>
        <translation type="unfinished">插入数据</translation>
    </message>
    <message>
        <source>Modify current data</source>
        <translation type="unfinished">修改当前数据</translation>
    </message>
    <message>
        <source>Toggle Turn Left flag</source>
        <translation type="unfinished">增加或删除左掉头标志</translation>
    </message>
    <message>
        <source>Toggle Turn Right flag</source>
        <translation type="unfinished">增加或删除右掉头标志</translation>
    </message>
    <message>
        <source>Toggle Turn Around flag</source>
        <translation type="unfinished">增加或删除回掉头标志</translation>
    </message>
    <message>
        <source>Delete all data</source>
        <translation type="unfinished">删除所有数据</translation>
    </message>
    <message>
        <source>Toggle Press flag</source>
        <translation type="unfinished">增加或删除压纸标志</translation>
    </message>
    <message>
        <source>Toggle Pusher flag</source>
        <translation type="unfinished">增加或删除推纸标志</translation>
    </message>
    <message>
        <source>Toggle Air flag</source>
        <translation type="unfinished">增加或删除气标志</translation>
    </message>
    <message>
        <source>Toggle Auto Cut flag</source>
        <translation type="unfinished">增加或删除刀标志</translation>
    </message>
    <message>
        <source>Relative add append data</source>
        <translation type="unfinished">相对加，增加到最后一刀</translation>
    </message>
    <message>
        <source>Relative sub append data</source>
        <translation type="unfinished">相对减，增加到最后一刀</translation>
    </message>
    <message>
        <source>Relative multiply append data</source>
        <translation type="unfinished">相对乘，增加到最后一刀</translation>
    </message>
    <message>
        <source>Relative divide append data</source>
        <translation type="unfinished">相对除，增加到最后一刀</translation>
    </message>
    <message>
        <source>Adjust add all data from current index</source>
        <translation type="unfinished">批量增加</translation>
    </message>
    <message>
        <source>Adjust sub all data from current index</source>
        <translation type="unfinished">批量减少</translation>
    </message>
    <message>
        <source>Relative add input data</source>
        <translation type="unfinished">相对当前位置加</translation>
    </message>
    <message>
        <source>Relative sub input data</source>
        <translation type="unfinished">相对当前位置减</translation>
    </message>
    <message>
        <source>Relative multiply input data</source>
        <translation type="unfinished">相对当前位置乘</translation>
    </message>
    <message>
        <source>Relative divide input data</source>
        <translation type="unfinished">相对当前位置除</translation>
    </message>
    <message>
        <source>Zoom in data list font</source>
        <translation type="unfinished">放大列表框字体</translation>
    </message>
    <message>
        <source>Zoom out data list font</source>
        <translation type="unfinished">缩小列表框字体</translation>
    </message>
</context>
<context>
    <name>MaintenancePage</name>
    <message>
        <source>Motor Alarm/Warning/Error Log:</source>
        <translation type="unfinished">最近15次马达报警和错误记录:</translation>
    </message>
</context>
<context>
    <name>ManoMeter</name>
    <message>
        <source>Analog Barmeter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MotorLogView</name>
    <message>
        <source>Alarm Code</source>
        <translation type="unfinished">报警代码</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="unfinished">日期</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="unfinished">时间</translation>
    </message>
    <message>
        <source>Motor Status</source>
        <translation type="unfinished">马达状态</translation>
    </message>
    <message>
        <source>Cut Position</source>
        <translation type="unfinished">切刀位置</translation>
    </message>
    <message>
        <source>Over Current</source>
        <translation type="unfinished">过流</translation>
    </message>
    <message>
        <source>Over Load</source>
        <translation type="unfinished">过载</translation>
    </message>
    <message>
        <source>Lose Control</source>
        <translation type="unfinished">失控</translation>
    </message>
    <message>
        <source>Communication Error</source>
        <translation type="unfinished">通信错误</translation>
    </message>
    <message>
        <source>Unknown Error</source>
        <translation type="unfinished">未知错误</translation>
    </message>
</context>
<context>
    <name>MotorParametersPage</name>
    <message>
        <source>[mm]</source>
        <translation type="unfinished">[毫米]</translation>
    </message>
    <message>
        <source>[Meter/Minute]</source>
        <translation type="unfinished">[米/分钟]</translation>
    </message>
    <message>
        <source>[RPM]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;Font Limit Range: 0 ~ %1 mm&lt;/p&gt;</source>
        <translation type="unfinished">&lt;p&gt;前极限范围: 0 ~ %1 毫米&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Middle Limit Range: %1 ~ %2 mm&lt;/p&gt;</source>
        <translation type="unfinished">&lt;p&gt;中极限范围: %1 ~ %2 毫米&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Back Limit Range: %1 ~ %2 mm&lt;/p&gt;</source>
        <translation type="unfinished">&lt;p&gt;后极限范围: %1 ~ %2 毫米&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Pusher Speed Range: %1 ~ %2 mm&lt;/p&gt;</source>
        <translation type="unfinished">&lt;p&gt;推纸速度范围: %1 ~ %2 mm&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Jog Speed Range: %1 ~ %2 RPM&lt;/p&gt;</source>
        <translation type="unfinished">&lt;p&gt;运行速度范围: %1 ~ %2 RPM&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Air Stop Distance Range: 0 ~ 20 mm&lt;/p&gt;</source>
        <translation type="unfinished">&lt;p&gt;停气距离范围: 0 ~ 20 毫米&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Step Size Range: 1 ~ 20 mm&lt;/p&gt;</source>
        <translation type="unfinished">&lt;p&gt;步进距离范围: 1 ~ 20 毫米&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Enable Limits</source>
        <translation type="unfinished">前中后极限有效</translation>
    </message>
    <message>
        <source>Front Limit:</source>
        <translation type="unfinished">前极限:</translation>
    </message>
    <message>
        <source>Middle Limit:</source>
        <translation type="unfinished">中极限:</translation>
    </message>
    <message>
        <source>Back Limit:</source>
        <translation type="unfinished">后极限:</translation>
    </message>
    <message>
        <source>Pusher Speed:</source>
        <translation type="unfinished">推纸速度:</translation>
    </message>
    <message>
        <source>Jog Speed:</source>
        <translation type="unfinished">运行速度:</translation>
    </message>
    <message>
        <source>Air Stop Distance:</source>
        <translation type="unfinished">停气距离:</translation>
    </message>
    <message>
        <source>Step Size:</source>
        <translation type="unfinished">步进距离:</translation>
    </message>
    <message>
        <source>&lt;h2&gt;&lt;font color=red&gt;Help: &lt;/font&gt;&lt;/h2&gt;</source>
        <translation type="unfinished">&lt;h2&gt;&lt;font color=red&gt;提示: &lt;/font&gt;&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Clear the &quot;Enable Limits&quot; box to disable limits.&lt;/p&gt;</source>
        <translation type="unfinished">&lt;p&gt;点击 &quot;使能极限&quot; 选择框关闭前中后极限.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Check the &quot;Enable Limits&quot; box to enable limits.&lt;/p&gt;</source>
        <translation type="unfinished">&lt;p&gt;点击 &quot;使能极限&quot; 选择框打开前中后极限.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>MotorTestWidget</name>
    <message>
        <source>Move Cut Down</source>
        <translation type="unfinished">下刀</translation>
    </message>
    <message>
        <source>Move Pusher Up</source>
        <translation type="unfinished">松开</translation>
    </message>
    <message>
        <source>Push button to toggle cut or pusher status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move Cut Up</source>
        <translation type="unfinished">松刀</translation>
    </message>
    <message>
        <source>Move Pusher Down</source>
        <translation type="unfinished">压纸</translation>
    </message>
    <message>
        <source>Start Current Position Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop Current Position Test</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenFileDialog</name>
    <message>
        <source>Are you sure to delete program file: </source>
        <translation type="unfinished">确认删除程序文件:</translation>
    </message>
    <message>
        <source>Program file delete</source>
        <translation type="unfinished">程序文件删除成功</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation type="unfinished">程序描述:</translation>
    </message>
    <message>
        <source>Load history:</source>
        <translation type="unfinished">使用记录:</translation>
    </message>
</context>
<context>
    <name>PaperPusherBar</name>
    <message>
        <source>Program</source>
        <translation type="unfinished">程序</translation>
    </message>
    <message>
        <source>Step</source>
        <translation type="unfinished">步骤</translation>
    </message>
</context>
<context>
    <name>Program</name>
    <message>
        <source>Description load failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text codec load failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Loading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Program</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgramEditor</name>
    <message>
        <source>Position</source>
        <translation type="unfinished">切刀位置</translation>
    </message>
    <message>
        <source>Flags</source>
        <translation type="unfinished">标志</translation>
    </message>
    <message>
        <source>Invalid argument </source>
        <translation type="unfinished">参数错误</translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>æ æ³æå¼æ°æ®åº
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>éåºæCancelé®</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>query success!
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>query failed!
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SpeedMeter</name>
    <message>
        <source>[RPM]</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StartScreen</name>
    <message>
        <source>Press RUN button to start motor...</source>
        <translation type="unfinished">按运行键，马达初始化 ...</translation>
    </message>
    <message>
        <source>No touchscreen device found.</source>
        <translation type="unfinished">没有找到触摸屏设备.</translation>
    </message>
    <message>
        <source>F1: Keyboard Check; F2: Touch Screen Calibration</source>
        <translation type="unfinished">F1: 键盘检测 ; F2: 触摸屏校准</translation>
    </message>
</context>
<context>
    <name>SystemSettingsPage</name>
    <message>
        <source>System Language:</source>
        <translation type="unfinished">系统语言:</translation>
    </message>
    <message>
        <source>System Theme:</source>
        <translation type="unfinished">系统主题:</translation>
    </message>
    <message>
        <source>System Font Size:</source>
        <translation type="unfinished">系统字体大小:</translation>
    </message>
    <message>
        <source>Position Font Size:</source>
        <translation type="unfinished">马达位置字体大小:</translation>
    </message>
    <message>
        <source>Position Font Color:</source>
        <translation type="unfinished">马达位置字体颜色:</translation>
    </message>
    <message>
        <source>Position Background Color:</source>
        <translation type="unfinished">马达位置背景颜色:</translation>
    </message>
    <message>
        <source>Distance Font Size:</source>
        <translation type="unfinished">刀前尺寸字体大小:</translation>
    </message>
    <message>
        <source>Distance Font Color:</source>
        <translation type="unfinished">刀前尺寸字体颜色:</translation>
    </message>
    <message>
        <source>Distance Background Color:</source>
        <translation type="unfinished">刀前尺寸背景颜色:</translation>
    </message>
    <message>
        <source>Screensaver Time:</source>
        <translation type="unfinished">屏幕保护:</translation>
    </message>
    <message>
        <source>&lt;h3&gt;&lt;font color=red&gt;Operation Guide:&lt;/font&gt;&lt;/h3&gt;&lt;p&gt;&lt;font color=blue&gt;Press button and then select value from the list.&lt;/font&gt;&lt;/p&gt;&lt;p&gt;&lt;font color=blue&gt;To reset settings, press RESET button.&lt;/font&gt;&lt;/p&gt;&lt;p&gt;&lt;font color=blue&gt;To save settings, press SAVE button.&lt;/font&gt;&lt;/p&gt;</source>
        <translation type="unfinished">&lt;h3&gt;&lt;font color=red&gt;操作说明:&lt;/font&gt;&lt;/h3&gt;&lt;p&gt;&lt;font color=blue&gt;按键选择需要修改的设置，并从右边列表框中选择设值.&lt;/font&gt;&lt;/p&gt;&lt;p&gt;&lt;font color=blue&gt;按复位键重新加载系统设置.&lt;/font&gt;&lt;/p&gt;&lt;p&gt;&lt;font color=blue&gt;按保存键保存当前系统设置.&lt;/font&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Please Select:</source>
        <translation type="unfinished">请选择:</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished">系统主题</translation>
    </message>
</context>
<context>
    <name>TouchScreenCalibrateWidget</name>
    <message>
        <source>Press Enter Key to do touch screen calibration.</source>
        <translation type="unfinished">按确认键进入触摸屏校准程序.</translation>
    </message>
    <message>
        <source>&lt;p&gt;No touchscreen device found.&lt;/p&gt;</source>
        <translation type="unfinished">&lt;p&gt;没有找到触摸屏设备.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>VersionDialog</name>
    <message>
        <source>OK</source>
        <translation type="unfinished">确定</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Smart Motor&lt;/h2&gt;&lt;p&gt;Version: </source>
        <translation type="unfinished">&lt;h2&gt;智能切纸机&lt;/h2&gt;&lt;p&gt;版本: </translation>
    </message>
    <message>
        <source>Copyright @ 2010 SmartMotor Inc. All Rights Reserved.</source>
        <translation type="unfinished">版权所有，翻版必究 @ 2010 SmartMotor Inc.</translation>
    </message>
</context>
<context>
    <name>VirtualKeyboard</name>
    <message>
        <source>Enter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>#+=</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Shift</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ABC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
