<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>AdvancedMotorParametersPage</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AverageEditor</name>
    <message>
        <source>Number mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Value mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Average Number Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Average Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Average Value Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Average Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Press button to toggle average mode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BaseSetupDialog</name>
    <message>
        <source>Base Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Motor Base:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Input the real position from pusher to cut and press Enter key.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Calculator</name>
    <message>
        <source>.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>+/-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Backspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sqrt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>x²</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>1/x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>=</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Press F6 to Exit.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Calibration</name>
    <message>
        <source>Touch cross bar to calibrate, press F2 to exit.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CurrentMeter</name>
    <message>
        <source>[Amp]</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DateTimePage</name>
    <message>
        <source>Current Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hour:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minute:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Second:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditDialog</name>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancell</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FsModel</name>
    <message>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Last Modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use Count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cut Count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>First Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>First Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HeaderLabel</name>
    <message>
        <source> ... </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeyboardCheck</name>
    <message>
        <source>Keyboard Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Press any key ... Double press Enter to exit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Backspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Modify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Page Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Page Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Backward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pusher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>AutoCut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Training</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fast Backward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fast Forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open keyboard device error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Keyboard is connected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Keyboard is not connected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LabelEditor</name>
    <message>
        <source>Label Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Label Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unused Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Input length and press ENTER</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MachineSettingDialog</name>
    <message>
        <source>Machine Parameter Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Machine Maintenance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date And Time Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>System Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Advanced Machine Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Machine Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>F1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>F2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>F3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>F5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Touch Screen Calibration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>F6</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainFrame</name>
    <message>
        <source>Select a program file or edit program description.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Program Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Input data between %1 - %2 [mm]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Press RUN button to run this program automatically.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Input cut position and press RUN button.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Manual Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dangerous!!! Pay care to the cut.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Cut Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Run one data to add it to program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Training</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use MODE buttons on the left size to change mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Input Data:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;h2&gt;&lt;font color=red&gt;AutoCut mode is Dangerous!&lt;/font&gt;&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do you really want to enter AutoCut mode?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete all data?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;font color=red&gt;Inputed data is out of range.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete current data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete all flags of current data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Insert one data to current index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Modify current data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Toggle Turn Left flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Toggle Turn Right flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Toggle Turn Around flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete all data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Toggle Press flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Toggle Pusher flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Toggle Air flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Toggle Auto Cut flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative add append data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative sub append data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative multiply append data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative divide append data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Adjust add all data from current index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Adjust sub all data from current index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative add input data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative sub input data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative multiply input data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative divide input data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom in data list font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom out data list font</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MaintenancePage</name>
    <message>
        <source>Motor Alarm/Warning/Error Log:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ManoMeter</name>
    <message>
        <source>Analog Barmeter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MotorLogView</name>
    <message>
        <source>Alarm Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Motor Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cut Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Over Current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Over Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lose Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Communication Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MotorParametersPage</name>
    <message>
        <source>[mm]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>[Meter/Minute]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>[RPM]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;Font Limit Range: 0 ~ %1 mm&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;Middle Limit Range: %1 ~ %2 mm&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;Back Limit Range: %1 ~ %2 mm&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;Pusher Speed Range: %1 ~ %2 mm&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;Jog Speed Range: %1 ~ %2 RPM&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;Air Stop Distance Range: 0 ~ 20 mm&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;Step Size Range: 1 ~ 20 mm&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enable Limits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Front Limit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Middle Limit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Back Limit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pusher Speed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Jog Speed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Air Stop Distance:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Step Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;h2&gt;&lt;font color=red&gt;Help: &lt;/font&gt;&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;Clear the &quot;Enable Limits&quot; box to disable limits.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;Check the &quot;Enable Limits&quot; box to enable limits.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MotorTestWidget</name>
    <message>
        <source>Move Cut Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move Pusher Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Push button to toggle cut or pusher status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move Cut Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move Pusher Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start Current Position Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop Current Position Test</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenFileDialog</name>
    <message>
        <source>Are you sure to delete program file: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Program file delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Load history:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PaperPusherBar</name>
    <message>
        <source>Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Step</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Program</name>
    <message>
        <source>Description load failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text codec load failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Loading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Program</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgramEditor</name>
    <message>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid argument </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>æ æ³æå¼æ°æ®åº
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>éåºæCancelé®</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>query success!
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>query failed!
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SpeedMeter</name>
    <message>
        <source>[RPM]</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StartScreen</name>
    <message>
        <source>Press RUN button to start motor...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No touchscreen device found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>F1: Keyboard Check; F2: Touch Screen Calibration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SystemSettingsPage</name>
    <message>
        <source>System Language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>System Theme:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>System Font Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Position Font Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Position Font Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Position Background Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance Font Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance Font Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance Background Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Screensaver Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;h3&gt;&lt;font color=red&gt;Operation Guide:&lt;/font&gt;&lt;/h3&gt;&lt;p&gt;&lt;font color=blue&gt;Press button and then select value from the list.&lt;/font&gt;&lt;/p&gt;&lt;p&gt;&lt;font color=blue&gt;To reset settings, press RESET button.&lt;/font&gt;&lt;/p&gt;&lt;p&gt;&lt;font color=blue&gt;To save settings, press SAVE button.&lt;/font&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please Select:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TouchScreenCalibrateWidget</name>
    <message>
        <source>Press Enter Key to do touch screen calibration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;No touchscreen device found.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VersionDialog</name>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;h2&gt;Smart Motor&lt;/h2&gt;&lt;p&gt;Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copyright @ 2010 SmartMotor Inc. All Rights Reserved.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VirtualKeyboard</name>
    <message>
        <source>Enter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>#+=</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Shift</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ABC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
