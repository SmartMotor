/*
 * io.c: userspace data read/write tool
 */
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>

#define DEVMEM "/dev/mem"

#define PAGESIZE 4096
#define ALIGN(x) ((x) & 0xFFFFF000)

unsigned char *iomem;
#define IOMEM_b(x) (*(unsigned char  *)(iomem + (x)))
#define IOMEM_w(x) (*(unsigned short *)(iomem + (x)))
#define IOMEM_l(x) (*(unsigned long  *)(iomem + (x)))

int io_read(int, int);
int io_write(int, int, int);

void help(void)
{
	fprintf(stderr, "Usage: io {b|w|l} physical_address [data]\n");
}

int main(int argc, char *argv[])
{
	int fd;
	int is_write = 0;
	int width = 4;
	unsigned long physaddr;
	int pgoff;
	int data;
	char type;

	if (argc < 3) {
		help();
		exit(-1);
	}

	if (argc == 4) {
		data = strtol(argv[3], NULL, 16);
		is_write = 1;
	}

	type = argv[1][0];
	switch(type) {
	case 'b':
		width = 1;
		break;
	case 'w':
		width = 2;
		break;
	case 'l':
		width = 4;
		break;
	default:
		help();
		exit(-1);
	}

	physaddr = strtol(argv[2], NULL, 16);
	pgoff = physaddr % PAGESIZE;

	if ((fd = open(DEVMEM, O_RDWR|O_SYNC)) < 0){
		perror(DEVMEM);
		exit(-1);
	}
	if ((iomem = mmap(NULL, PAGESIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, ALIGN(physaddr))) == MAP_FAILED){
		perror("mmap");
		exit(-1);
	}
	close(fd);

	if (is_write) {
		io_write(width, pgoff, data);

	} else {
		io_read(width, pgoff);
	}

	return 0;
}

int io_read(int width, int pgoff)
{
	printf("iomem=0x%x\n", (unsigned long)iomem);

	switch(width) {
	case 1:
		printf("0x%02x\n", IOMEM_b(pgoff));
		break;
	case 2:
		printf("0x%04x\n", IOMEM_w(pgoff));
		break;
	case 4:
		printf("0x%08x\n", IOMEM_l(pgoff));
		break;
	default:
		break;
	}

	return 0;
}

int io_write(int width, int pgoff, int data)
{
	printf("iomem=0x%x\n", (unsigned long)iomem);

	switch(width) {
	case 1:
		IOMEM_b(pgoff) = (unsigned char)(data & 0xff);
		printf("0x%02x\n", data & 0xff);
		break;
	case 2:
		IOMEM_w(pgoff) = (unsigned short)(data & 0xffff);
		printf("0x%04x\n", data & 0xffff);
		break;
	case 4:
		IOMEM_l(pgoff) = (unsigned long)data;
		printf("0x%08x\n", data);
		break;
	default:
		break;
	}

	return 0;
}
