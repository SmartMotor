#! /bin/sh

#
# dump io from 
#


function usage()
{
    echo "dump-io.sh START_ADDRESS SIZE"
    exit 1
}

function fatal()
{
    echo "$@"
    exit 1
}

if [ $# -lt 2 ]; then
    usage
fi

IO=/usr/bin/io

#test -x $IO || fatal "Can not found $IO"

start_address=$1
length=$2

let rim=start_address%4
if [ $rim -ne 0 ]; then
    fatal "start address $start_address must be 4-byte align"
fi

let rim=length%4
if [ $rim -ne 0 ]; then
    fatal "address region length $length must be 4-byte align"
fi

let end_address=$start_address+$length


exit 
