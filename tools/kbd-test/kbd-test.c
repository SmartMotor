/*
 * keyboard test code
 *
 *   gcc -o kbd-test kbd-test.c
 */
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <linux/input.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>


/* how many bytes were read */
size_t rb;
/* the events (up to 64 at once) */
struct input_event ev[64];

int main(int argc, char **argv)
{

	int fd = -1;
	char name[256]= "Unknown";
	unsigned int version;
	int yalv;

	if ((fd = open(argv[1], O_RDONLY)) < 0) {
		perror("evdev open");
		exit(1);
	}

       /* ioctl() accesses the underlying driver */
	if (ioctl(fd, EVIOCGVERSION, &version)) {
		perror("evdev ioctl");
	}

        /* the EVIOCGVERSION ioctl() returns an int */
        /* so we unpack it and display it */
	printf("evdev driver version is %d.%d.%d\n",
	       version >> 16, (version >> 8) & 0xff,
	       version & 0xff);

	if(ioctl(fd, EVIOCGNAME(sizeof(name)), name) < 0) {
		perror("evdev ioctl");
	}

	printf("The device on %s says its name is %s\n",
	       argv[1], name);

	rb=read(fd,ev,sizeof(struct input_event)*64);

	if (rb < (int) sizeof(struct input_event)) {
		perror("evtest: short read");
		exit (1);
	}

	for (yalv = 0;
	     yalv < (int) (rb / sizeof(struct input_event));
	     yalv++)
	{
		if (EV_KEY == ev[yalv].type) {
			printf("%ld.%06ld ",
			       ev[yalv].time.tv_sec,
			       ev[yalv].time.tv_usec);
			printf("type %d code %d value %d\n",
			       ev[yalv].type,
			       ev[yalv].code, ev[yalv].value);
		}
	}

	close(fd);

	return 0;
}
