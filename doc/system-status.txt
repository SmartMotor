===================================================================
  System status and ChangeLOG

  xianweizeng@gmail.com

$Id: system-status.txt,v 1.1 2008/05/12 13:38:38 souken-i Exp $
===================================================================

TODO:
  1. Add self-reinstall function
     
     Sometimes the user file system(userfs) will be crashed or
     the user data will be lost due to un-correctly operation.
     
     This function use a Keypad which is connected to a CPU's 
     GPIO. When this key is pressed during system is booting,
     The self-reinstall program will be started instead of the
     APP. And later the APP will start automatically. 
	   
  2. Improve touchscreen driver
     
     We use ADS7846 controller here.
