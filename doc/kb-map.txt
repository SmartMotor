=======================================================
  kb-map.txt:  About KeyBoard Mapping

  xianweizeng@gmail.com @ 2008-12-13
=======================================================


1. About
========

   KB_V1_1: old version
   	     
	    Use one "Mode"(code=28) key to change modes
	    beyond "Edit", "Manual" and "Auto" etc modes.

   KB_V1_2: no_ts version, hardwar is same as KB_V1_1.
            This version has 3 modes keys:
	    "Edit":    28
	    "Manual":  23
	    "Auto":    30

   KB_V2:    spain version, hardware changed


2. Keyboard mappings
=====================

  +-----+------------+------------+------------+
   code     KB_V1_1     KB_V1_2       KB_V2          
  +-----+------------+------------+------------+
    23      SHIFT	MD_MANUAL     SHIFT     
    28      MD_CHG	MD_EDIT	      MD_EDIT
    30      EQUAL(=)	MD_AUTO	      EQUAL(=)      
    31 	    MUL		SHIFT	      MUL
    32	    DIV		EQUAL(=)      DIV
    33	    STOP	STOP	      MD_AUTO
    44      (null)	(null)	      MD_MANUA
  +-----+------------+------------+------------+

