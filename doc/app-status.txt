===================================================================
  Application Softwre Status and Change LOG

  xianweizeng@gmail.com

$Id: app-status.txt,v 1.2 2008/06/15 15:24:23 souken-i Exp $
===================================================================

TODO:
	1. Add kerboard check window 
	   (2008/05)
	2. Add new style list window with BMP/JPG icon support.
	   (2008/06)
	3. Move to new GUI
	   (2008/06)
	4. Add Operation-Follow-Me window
	   (2008/07)
	5. The test solution 
	   (2008/07)
  	6. Pre-Release: 2008/08B

用户界面 的图标尺寸主要如下：
    1. 左右两边的按钮:                 42 x 42
    2. 输入框右边的确定，取消和运行：  32 x 32
    3. 列表框里面的推纸等标志：        24 x 24    （字体是24x24的）
    4. 屏幕下面四个状态标志：          50 x 24
    5. 列表框的大下：                  320 x 260
    6. 白色底框的大小：                440 x 280


Schedule:
	1. Ver0.1: 2008/07E
  	   
	   o Windows Operation Flow
	   o List Control with icon
	   o Windows adjust(Position of each control)	 
	   
	2. Ver0.2: 2008/09B

	   o icons: size/appearence/layout
	   o bugfix
	   
	3. Ver0.3: 2008/10E
	    
	   o touchscreen support
	    

Change LOG:


2008-09-27:
	1. Use BS_TOUCH_STYLE in userprogram.c

2008-09-23:
	1. User window demo use new icons and new style listbox.

2008-09-07:
	1. User window fisrt demo.

2008-07-20:
	1. add todo.
	2. SmartMotor V01HS
	   Adding spain language support
	2. SmartMotor V02
	   Plan to put help


2008-04-20
	1. SmartMotor 2.0 
	   - Fix startup flash config.
	     patches/fix-startup-flash-config-20080420.patch
	   - save lasruser and recent_program
	     patches/save-last-user-and-last-program-20080420.patch
        
	2. SmartMotor V01/V01HS/V02
	   - add data limit check in lable/average edit
	    

2008-04-18
	1. Can we use config options to select different functiones?

	   Because in different version of target, the functiones have 
	   a few differences. So it becomes diffcult to manage these versions
	   of program.

	   

2008-04-15
	1. Now the using version is:
	   
	   Release-20070902:
		3 mode-keys version: V01  
		1 mode-key version:  V02
 
	   Release-20080120:
		3 mode-keys version with Highspeed: V01HS

        2. Fix a bug in V01, V02:
	   
	   fix-panic-when-enter-proglist-from-manual-mode-20080415.patch

        3. Fix patch in V01, V01HS
	   add-high-speed-keys-20080120.patch

        4. RELEASE-20080415

	5. TODO:
	   Add data limit check in Label/Average edit.(Total length)
	   
	   
2008-04-13
	1. Create project: SmartMotor-2.0
	2. Fix makefile
	   patches/fix-Makefile-20080413.patch
	3. Move globle variables definition from header files to source files.
           move-globle-varible-to-source-file-20080413.patch
        4. Add these patches:
	   enter-userwindow-directly-from-welcomewindow-20080413.patch
	   fix-program-file-header-structure-20080413.patch
	   fix-debug-config-20080413.patch

	5. Todo
	   
	   The last used program can NOT be remembered. (NEED fix)
	   Mutil-Language support method now is foolish.
	   (done 2008-04-20)
		
2007-11-11
	1. no-ts version:
		Add Hispeed Forward and Highspeed Backward Keys support.
		(New keyboard support)

		This patch changes orgianl key mapping:
			PXA255_KB_MD_EDIT    =>  SCANCODE_EDIT
			PXA255_KB_MD_AUTO    =>  SCANCODE_AUTORUN
			PXA255_KB_MD_MANUAL  =>  SCANCODE_MANUAL
		are defined to change mode.
	
	2. Plan to standard the KeyCode withing Minigui IAL, and user program.

	   Define enough key codes (SCANCODE_XXX and PXA255_KB_XXX) in MiniGUI
	   source IAL layer. and user program responds to these key codes.

	   When adding or removing a key, just change to source code in
	   KeyBoard Micro-controller, eg 8051.	
	

2007-11-09
	1. old-version:
		Add no-time-clock function: use a micro to select this
		function.
		[patches/add-timeclock-switcher-20071104.patch]	

2007-10-08
	1. nots-version:
		Fix bugs: Y mod, Q mode and T mode, only one can be choosed.
		So, when add one mode, other mode flag shoule be cleard.
	
2007-09-30
	1. nots-version:
		
		Add function: save the cut count in user profile. 
		Save it in a single file because it is frequently accessed.
		We save it after every cut is done.

2007-09-02
	1. old version

		Add data check function in label and average edit.

 	2. old version and Nots version

		Add patch to fix the bug of Training Mode. It can't work
		    after one cut is done and then enter this mode.
	   	    (Need to be checked) 
		Fix IME change bug in Edit Mode. When input program name,
		SCANCODE_INSERTDATA can't change the IME mode.


2007-08-30 
	1. Use quilt to manage the source code.
 	2. Build quilt tree for SmartMotor1.6, SmartMotor1.6-old
	3. Add autocut mode for OLD version.


2007-08-29
	1. Used version:
	   1) old version
		Use Mode KEY to change between "Input","Manual","Auto"
		and "Autocut" Mode.
	   2) nots version
		Use seperate KEY(Input,Manual,Auto) to select modes;
		No Autocut mode in this version
        2. Unused version
	   1) ts version

==========
Modify Log:
2007/08/29: 	First version
