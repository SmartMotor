===========================================================================

              SmartMotor V3.2 IO Resources

===========================================================================


1. PXA255 GPIO
---------------

   GPIO  Func
   -----+---------------
   0     CPLD
   1     CPLD
   2     CPLD
   3     SM502 IRQ
   4     LED
   5     LED
   7     CPLD
   9     CPLD
   10    ADS7846 IRQ
   11    ADS7846 BUSY
   12    CPLD
   13    CPLD
   14    CPLD
   21    EXT/SMSC
   22    EXT
   32    CPLD
   81    KEY
   82    KEY
   83    USB HOST 
   84    CPLD
   PWM0  Beeper
   PMM1  NC
 

2. PXA255 Memory Bank
----------------------

   片选       起始地址	     连接器件         功能
   nCS0       0x00000000    Nor Flash        Nor Flash: 32bits 
   nCS1       0x04000000    CPLD             Reversed
   nCS2       0x08000000    CPLD             (NandFlash: 8bits)  
   nCS3       0x0C000000    CPLD	     (CPLD:8bits)
   nCS4       0x10000000    CPLD 	     (SM502:32bits)
   nCS5       0x14000000    CPLD             (Ethernet:16bits)
   nSDCS0     0xA0000000    SDRAM            SDRAM: 32bits
   nSDCS1     0xA4000000    RFU              


3. Xscale 内存配置寄存器设置
----------------------------


Register   Addr          Value        Description
---------+------------+-------------+----------------------------------------------
MDCNFG     0x48000000    0x00001AC9   CS0:EN,CS1:DIS,32bits,9 Col,13 Row,4 bank,etc
MDREFR     0x48000004	 0x0001c01d   SDCLK1 enabled, SDCKE1 enabled,
	   		 	      SDCLK0 disabled, SDCKE0 disabled,
				      DRI=30(0x01D)
MSC0       0x48000008    0x7ff091d0   nCS0: NorFlash, 32bit, RDF=15, RDN=1, RRR=0, Faster Device(RBUFF=1)
	   		 	      nCS1: Reversed,
MSC1       0x4800000C    0xa2a9a2a9   nCS2: Nand, SRAM
	   		 	      nCS3: CPLD, SRAM
MSC2       0x48000010    0x91399139   nCS4: SM502, SRAM,
	   		 	      nCS5: SMSC, SRAM, 
MECR       0x48000014    0x00000000   NOS=0(1 socket), CIT=0(No card insert)
 

4. Xscale  GPIO寄存器设置
-------------------------

Reg	  Value	        Description
-------+--------------+------------------------
GPSRx     0x00008030	cs1 must set high; GP4,5 LEDs on
GPSRy     0x00000002	cs5 set high
GPSRz     0x0001c000    cs2~4 set high
		        Clear Registers, 1:clear, 0:unchange
GPCRx     0x00010000	beep clear
GPCRy     0x00000000	
GPCRz     0x00000000
                         GPIO Direction Registers, 0:input, 1:output
GPDRx     0x03818170	 
GPDRy     0xfcffab82
GPDRz     0x0001FFFF
			GPIO Alternate Function Register: 00: GPIO 
GAFR0_L   0x80011000    0~5:IO,6:mmc,7:IO,8:mmc,15:cs
GAFR0_U   0x005a8010    16~17:IO,18:rdy,19~22:IO,23~27:IO,
GAFR1_L   0x699a9558   	32:IO,33:cs,34~47:af
GAFR1_U   0xAAA5aaaa    48~57-pcmcia,58~63-lcd
GAFR2_L   0xAAAAAAAA    64~77-lcd,78,79-ncs2,3
GAFR2_U   0x00000002    80-ncs4,81~84-io


5. XScale GPIO 使用表
---------------------

  GPIO       功能           说明/AF encoding
  ------+----------------+--------------------------------------
   0	  RFU              IN
   1	  SW1	       	   IN
   2	  SW2	       	   IN
   3	  SM502 IRQ	   IN
   4	  LED1		   OUT 
   5	  LED2		   OUT
   6	  MMC_CLK	   O     AF:01
   7	  SW3		   IN
   8	  MMC_CS0	   O	 AF:01
   9	  SW4		   IN
   10	  ADS7846 IRQ	   IN
   11	  ADS7846 Busy	   IN
   12	  SM502 nHRDY	   IN
   13	  SMSC PME IRQ	   IN
   14	  USB Detect	   IN
   15	  nCS1		   O	 AF:10
   16	  Beeper	   OUT
   17	  RFU		   IN
   18	  RDY_nWAIT	   IN    AF:01
   19	  RFU	           IN
   20	  RFU	           IN
   21	  SMSC IRQ	   IN
   22	  Ext IO, RFU	   IN
   23	  SSP CLK 	   O     AF:10
   24	  SSP SFR	   O     AF:10
   25	  SSP TXD	   O     AF:10
   26	  SSP RXD	   I     AF:01
   27	  SSP EXTCLK	   I     AF:01
   28	  RFU	           IN
   29	  RFU	           IN
   30	  RFU	           IN
   31	  RFU	           IN
   32	  MMC Detect IRQ   IN
   33	  nCS5		   O	AF:10
   34	  FFUART RXD	   IN	AF:01
   35	  FFUART CTS	   IN	AF:01
   36	  FFUART DCD	   IN	AF:01
   37	  FFUART DSR	   IN	AF:01
   38	  FFUART RI	   IN	AF:01
   39	  FFUART TXD	   O	AF:10
   40	  FFUART DTR	   O	AF:10
   41	  FFUART RTS   	   O	AF:10
   42	  BTUART RXD 	   I	AF:01
   43	  BTUART TXD	   O	AF:10
   44	  BTUART CTS	   I	AF:01
   45	  BTUART RTS	   O	AF:10
   46	  STUART RXD	   I	AF:10
   47	  STUART TXD	   O	AF:01
   48	  CF			AF:10
   49	  CF			AF:10
   50	  CF			AF:10
   51	  CF			AF:10
   52	  CF			AF:10
   53	  CF			AF:10
   54	  CF			AF:10
   55	  CF			AF:10
   56	  CF			AF:01
   57	  CF			AF:01	  
   58	  LCD			AF:10
   59	  LCD			AF:10
   60	  LCD			AF:10
   61	  LCD			AF:10
   62	  LCD			AF:10
   63	  LCD			AF:10
   64	  LCD			AF:10
   65	  LCD			AF:10
   66	  LCD			AF:10
   67	  LCD			AF:10
   68	  LCD			AF:10
   69	  LCD			AF:10
   70	  LCD			AF:10
   71	  LCD			AF:10
   72	  LCD			AF:10
   73	  LCD			AF:10
   74	  LCD			AF:10
   75	  LCD			AF:10
   76	  LCD			AF:10
   77	  LCD			AF:10
   78	  nCS2                  AF:10
   79	  nCS3                  AF:10	  
   80	  nCS4                  AF:10
   81	  KEY1		   IN
   82	  KEY2		   IN
   83     USB Status	   IN, Not used now                 
   84	  RFU		   IN


3. CPLD IO Resources
--------------------

   Pin    GPIO   连接信号           用途说明 
   ----+-------+-----------------+-------------------------
   2  	  0	 AVR_INT5
   3  	  1	 XS_RDY_nWAIT
   4	  2 	 XS_nCS1
   5	  3	 XS_nCS2
   6	  4	 XS_nCS4
   7	  5	 XS_nCS5
   8	  6	 XS_nCS3
   15	  7	 CFG_SW4
   16	  8	 CFG_SW3
   17	  9	 CFG_SW2
   18	  10	 CFG_SW1
   19	  11	 XS_nRESET_OUT
   20	  12	 SYS_nRESET
   21	  13	 SYS_RESET
   26	  14	 AVR_nRESET
   27	  15	 XS_GP0
   28	  16	 XS_GP1
   29	  17	 XS_GP2
   30	  18	 NC
   33	  19	 XS_GP12          SM_nHRDY
   34	  20	 XS_GP13 	  SMSC PME IRQ
   35	  21	 XS_GP14          USB slave detect IRQ
   36	  22	 XS_GP7
   38	  23	 XS_GP9
   40	  24	 XS_GP32          MMC detect IRQ
   41	  25	 XS_nOE	 
   42	  26	 XS_nWE
   47	  27	 XS_CF_nPWE
   48	  28	 XS_ADDR22
   49	  29	 XS_ADDR23
   50	  30	 XS_ADDR24
   51	  31	 XS_ADDR25
   52	  32	 XS_DATA0
   53	  33	 XS_DATA1
   54	  34	 XS_DATA2	
   55	  35	 XS_DATA3
   56	  36	 XS_DATA4
   57	  37	 XS_DATA5
   58	  38	 XS_DATA6
   61	  39	 XS_DATA7
   66	  40	 SM_CLKOFF
   67	  41	 SM_nHRDY
   68	  42	 SM_nHCS
   69	  43	 SM_nHBS
   70	  44	 SM_nCPURD
   71	  45	 NAND_nRE
   72	  46	 NAND_nCE
   73	  47	 NAND_CLE
   74	  48	 NAND_ALE
   75	  49	 NAND_nWE
   76	  50	 RUN_LED
   77	  51	 CPLD_IO10
   78	  52	 CPLD_IO9
   81	  53	 CPLD_IO8
   82	  54	 CPLD_IO7
   83	  55	 CPLD_IO6
   84	  56	 CPLD_IO5           SMSC_PME
   85	  57	 CPLD_IO4           SMSC_nReset
   86	  58	 CPLD_IO3           SMSC_nCS
   87	  59	 CPLD_IO2           SMSC_nWR
   89	  60	 CPLD_IO1           SMSC_nRD
   91	  61	 MMC_NCD
   92	  62	 MMC_WP
   95	  63	 BUF_OE	
   96	  64	 NC
   97	  65	 USB_DETECT_INT
   98	  66	 XS_ADDR0
   99	  67	 XS_ADDR1
   100	  68	 BUF_DIR
   1	  69	 AVR_INT4
   12     GLCK0  CLK_32K
   14     GCLK1  XS_GP84

4. SM502 GPIO 配置说明
----------------------

GPIO 0 1 2 : 
     0 1 0 :  Xscale Interface

GPIO3:
     0 : Internal PLL(default)
     1 : TESTCLK

GPIO4: (SH4 only)
     0 : nHRDY Active low
     1 : nHRDY Active high
GPIO 6 5 : Memory Column Size
     0 x : 256
     1 0 : 512
     1 1 : 1024  

     HY57v641620 -> 256

GPIO7:
     0 : Do not reset clock divider 
     1 : Reset clock divider (default)  

GPIO12:
     0 : Internal Memory
     1 : External Memory (default)

GPIO 15 14 13:　Local Memory Size
      0  0  0 : 4M
      0  0  1 : 8M    
      0  1  0 : 16M
      0  1  1 : 32M
      1  0  0 : 64M
      1  0  1 : 2M
      1  1  x : Undefined

GPIO 31 29 : XScale clock source
      0  0 : Internal PLL
      0  1 : SM501 HCLK Pin
      1  x : SM501 GPIO30 Pin
