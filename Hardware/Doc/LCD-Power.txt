- 8" LCD:

  Vgh:  16V
  Igh:  0.2 mA, max: 0.5 mA

  Vgl:  -7V
  Igl:  0.2 mA, max: 1 mA

  AVDD:  10.4 V
  IAVDD: 32 mA, max: 50 mA


- 稳压管整流电路

  * 计算公式
    - http://www.chinadz.com/~wzdz/dianzi/rumen/5-4.htm
    
      (Vinmax - Vout)/Iwmax < R1 < (Vinmin - Vout)/(Iw + Iloadmax)
    
    - Iwmax: 稳压管的最大电流
    - Iw:    稳压管的正常导通电流
    - Iloadmax: 负载最大电流

  * Vgh 16V
    - 1N4745
    - Vin = 22v
    - Iw = 15.5 mA, Iwmax = 57 mA
    - Iloadmax = 0.5 mA
       
      (22-16)*1000/57 < R1 < (22-16)*1000/(15.5+0.5)
                 105  < R1 <  375

    - 选择 R1 = 220

  * Vgl -7V
    - 1N4736
    - Vin = -11v
    - Iw = 37 mA, Iwmax = 133 mA
    - Iloadmax = 1 mA
       
      (11-6.8)*1000/133 < R1 < (11-6.8)*1000/(37+1)
                 31     < R1 <  110

    - 选择 R1 = 100

  * Vcom 3.9V
    - 1N4730
    - Vin = 11v
    - Iw = 64 mA, Iwmax = 234 mA
    - Iloadmax = 10 mA
       
      (11-3.9)*1000/234 < R1 < (11-3.9)*1000/(64+10)
                     30 < R1 <  96

    - 选择 R1 = 47
