===========================================================================

	Serial KeyBoard Mapping for SMT255 Board

===========================================================================


Update @ [2010-08-10 16:57]:

static const unsigned short smtkbd_table[SMTKBD_KEY_MAX] = {
          0, 59, 60, 61, 62, 63, 64,  2,  3,  4, 14,  5,  6,  7, 66,  8,
          9, 10,123, 52, 11,124,  1,  1, 68, 67, 78, 74,120,110,121, 42,
         13,125,111, 65,104,109, 87,103,108,105,106, 88,122, 28,126, 55,
         98,127,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
          1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
          1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
          1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
          1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1
};

  code     function       PC Code   PC Function
  1	   F1		  59 	    F1        
  2	   F2		  60	    F2
  3	   F3		  61	    F3
  4	   F4		  62	    F4
  5	   F5		  63	    F5
  6	   F6		  64	    F6
  7	   1		  2	    1
  8	   2		  3	    2
  9	   3		  4	    3
  10	   Backspace	  14	    Backspace
  11	   4		  5	    4
  12	   5		  6	    5
  13	   6		  7	    6
  14	   Program File	  66 	    F8        
  15	   7		  8	    7
  16	   8		  9	    8
  17	   9		  10	    9
  18	   Pusher	  123	    ???        *** F16
  19	   Dot		  52	    Period
  20	   0		  11	    0
  21	   Cancel	  124	    End        *** F17
  22	   
  23
  24	   Backward	   68	    F10
  25	   Forward	   67	    F9
  26	   +		   78	    Keypad +
  27	   -		   74	    Keypad -
  28	   Edit Mode	   120                 *** F13
  29	   Insert	   110	    Insert
  30	   Manual	   121      ???        *** F14
  31	   Shift	   42	    L_Shift 
  32	   Equal (=)	   13	    Equal
  33	   Stop	 	   125      ???        *** F18
  34	   Delete	   111	    ???
  35       Modify	   65	    F7
  36	   Page Up	   104	    Page Up
  37	   Page Down	   109	    Page Down
  38	   Run	           87	    F11
  39	   Up		   103	    Up
  40	   Down		   108	    Down
  41	   Left		   105	    Left
  42	   Right	   106	    Right
  43       Help		   88	    F12 
  44	   Auto		   122	    ???         *** F15
  45	   Enter	   28	    Enter
  46	   Fast Forward	   126      ???         *** F19
  47	   Mul (*)	   55       *
  48	   Div 		   98       /
  49	   Fast Backward   127      ???         *** F20
  50


Kernel source code:

+static const unsigned short smtkbd_table[SMTKBD_KEY_MAX] = {
+          0, 59, 60, 61, 62, 63, 64,  2,  3,  4, 14,  5,  6,  7, 66,  8,
+          9, 10,112, 52, 11,107, 28, 42, 67, 68, 78, 74, 54,110, 13, 55,
+         98,119,111, 65,104,109, 87,103,108,105,106, 88,  1,  1,  1,  1,
+          1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
+          1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
+          1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
+          1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
+          1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1
+};
+#endif

Scancode Mappings:

  Smt255     PC             PC		 
  Scancode   Keyboard       Scancode      Comment (Smt255 Function)
   1	     59             F1		  F1
   2	     60		    F2		  F2
   3	     61		    F3		  F3
   4	     62		    F4		  F4
   5	     63		    F5		  F5
   6	     64		    F6		  F6
   7	     2		    1		  1
   8	     3		    2		  2
   9	     4		    3		  3
   10	     14		    Backspace	  Backspace
   11	     5		    4		  4
   12	     6		    5		  5
   13	     7		    6		  6
   28	     66		    F8		  Program
   15	     8		    7		  7
   16	     9		    8		  8
   17	     10		    9		  9
   18	     112	    ?		  Pusher   ***
   19	     52		    Period	  Period
   20	     11		    0		  0
   21	     107	    End		  Cancel   ***
   22	     28		    Enter	  Enter
   23	     42		    L_Shift	  Shift
   24	     67		    F9		  Forward
   25	     68		    F10		  Backward
   26	     78		    Keypad_+	  +
   27	     74		    Keypad_-	  -
   28	     54		    R_Shift	  Edit Mode  ***
   29	     110	    Insert	  Insert
   30	     13		    Equal	  Equal
   31	     55		    Keypad_*	  *
   32	     98		    Keypad_/	  /
   33	     119	    ?		  Auto Mode  ***
   34	     111	    ?		  Delete
   35	     65		    F7		  Modify
   36	     104	    PageUp	  PageUp
   37	     109	    PageDown	  PageDown
   38	     87		    F11		  Run	    		  
   39	     103	    Up		  Up
   40	     108	    Down	  Down
   41	     105	    Left	  Left
   42	     106	    Right	  Right
   43	     88		    F12		  Help
