

#include"AT89X51.h"


#define Sys_freq 11059200//系统频率

#define Baud 57600//波特率预设


/********应答数据接收：aa 64 3f 5b 55
               返回：aa k e y b o a r d 0d 55
*************************/

#include "key.h"

bit Ms_flag;

bit Rec_ok_f;

bit Ack_flag;//应答接受开始 标志

uint Mstime;
uchar num,R_check;
uchar n;
uchar Ack_temp[11];
const uchar *Ack="keyboard";


void TargetInit(void);
void Rec_exec(void);


void main(void)
{

TargetInit();
num=0;R_check=0;
Ack_temp[0]=0xAA;
for(n=1;n<9;n++)
   {
   Ack_temp[n]=Ack[n-1];
   R_check^=Ack_temp[n];
   }
Ack_temp[n]=R_check;
Ack_temp[10]=0x55;
   
   
 //AUXR = 0xff; 
 
  
while(1)
    {
	
	if(Ms_flag)
	   {
	    Ms_flag=0;
	     
		 KeyScan();
		 Rec_exec();
		 /*
		 if(RI)
		   {
		    RI=0;
		   if(SBUF=='a')
		     Led=!Led;
		   }
		 */
		 /*
		 Mstime++;
       if(Mstime>=500)
          {
          Mstime=0;
          Led=!Led;
          }
		  */
	   }
	}

}



void TargetInit(void)
{
uint B_temp;

P0=0xff;
P1=1;
P2=0;
P3=0x1f;//uart

IE=0;//禁止所有中断

//**********定时器初始化**
TMOD=0x01;//16位定时
TL0=0x66;
TH0=0xFC;
TR0=1;
ET0=1;//开T0中断
EA=1;// 总中断允许

//************串口初始化*******
TMOD|=0x20;//T1自动重装8位初值
B_temp=(((Sys_freq>>4)/12)/Baud);//计算波特率
TH1=256-B_temp;
//TL1=256-B_temp;
//TH1=0xFD;

TR1=1;

//TI=0;         
RI=0;
SCON=0x50;//8bit UART,接收使能
PCON|=SMOD_;//波特率加倍

ES=1;//开串口中断
}








/*********T0中断处理*******/
void timer0 (void) interrupt 1 using 2
{
TL0=0x66;
TH0=0xFC;//1ms定时
Ms_flag=1;

}

//**************串口中断处理********
void Uart_Exec(void) interrupt 4 using 3 
{
  //TI=0;//需 软件清除中断标志
  
	
  if(RI==1)
    {
	 RI=0;//需 软件清除中断标志
	  if(!Ack_flag)
	     {
		 if(SBUF==0xaa)
		    { 
			Ack_flag=1; 
			Rec_temp[0]=0xaa;//接收数据
			num=1;	
		    }
		 }
	  else
		 {
		 Rec_temp[num++]=SBUF;//接收数据
	     if(num>=5)
		    {
	       Rec_ok_f=1;
		   Ack_flag=0;
		    }
		 }	
	
	        
	
	}
}

/*********接收处理**********/
void Rec_exec(void)
{
if(Rec_ok_f)
   {
   Rec_ok_f=0;
	if(Rec_temp[0]==0xAA)
	   {
		 R_check=0;
		 for(n=1;n<4;n++)
	       R_check^=Rec_temp[n];
		   
	     if((R_check==0)&&(Rec_temp[4]==0x55))//正确
	       {
		   if((Rec_temp[1]==0x64)&&(Rec_temp[2]==0x3f))
		      {
		      Led=!Led;
		      for(n=0;n<11;n++)
		         Send_temp[n]=Ack_temp[n];
	          SendExec(11);//启动发送
		      }
		   }
		num=0;   
	   }
	else
	  num=0;  
   }

}


/*
 if(!Head_f)
	   {
	   if(SBUF==0xAA)
	     {
		 num=0;
		 Rec_temp[num++]=SBUF;
		 R_check=0;

		 Head_f=1;
		 }
	   }
	 else
	   {
	    Rec_temp[num]=SBUF;
		if(num>=5)//尾部
		  {
		   Head_f=0;
		   //if(Rec_temp[num]==0x55)//接收正确
		     //SendExec();//启动发送 
			 
		  }
		else
		  {
		  if(num<4)
		     R_check^=Rec_temp[num++];
		  else
		    {
			 num++;
			//if(R_check!=Rec_temp[num++])//校验错误
			  // Head_f=0;
			}
		  }
	   
	   }
	   
	*/   