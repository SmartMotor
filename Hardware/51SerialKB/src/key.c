
#include "key.h"

#include"intrins.h"
#include "AT89X51.h"
#include"stdio.h"



#define KeyRow P0
#define KeyCol P2
/*
#define Col1_check P2=~(1<<0)
#define Col2_check P2=~(1<<1)
#define Col3_check P2=~(1<<2)
#define Col4_check P2=~(1<<3)
#define Col5_check P2=~(1<<4)
#define Col6_check P2=~(1<<5)
*/
#define Col1_check P2=~(1<<7)
#define Col2_check P2=~(1<<6)
#define Col3_check P2=~(1<<5)
#define Col4_check P2=~(1<<4)
#define Col5_check P2=~(1<<3)
#define Col6_check P2=~(1<<2)

#define WAIT_TIME 2000  //2s

#define DOWN      1
#define RELEASE   0//按键状态

bit Keydown_flag;
bit KeyExec_flag;
bit KeyLongExec_flag; 

uint Keycode;
uint Keycode_save;
uchar Keydown_time;
uchar Shake_time;
uint  KeyLong_Time;//长按计时

uchar Send_temp[11];//
uchar Rec_temp[5];
uchar Check;//


void Delay(void)
{
volatile uchar d=10;
while(d--)
  _nop_();
}

bit Keydown(void)//***********按键判断
{
  //KeyRow=0xff;
  KeyCol=0;   //P2
  Delay();
 if(KeyRow!=0xff)
   return 1;
 else
   return 0; 
}


void GetKeycode(void)//**********键值读取*******（列+行）
{
 uint Key_temp=0;
 Keycode=0;
 
Col1_check;
Delay();
Key_temp=(~KeyRow)&0xff;//P0
if(Key_temp!=0)
  Keycode|=(1<<8)|Key_temp;


Col2_check;
Delay();
Key_temp=(~KeyRow)&0xff;//P0
if(Key_temp!=0)
  Keycode|=(2<<8)|Key_temp; 
 
Col3_check;
Delay();
Key_temp=(~KeyRow)&0xff;//P0
if(Key_temp!=0)
  Keycode|=(4<<8)|Key_temp;
  
Col4_check;
Delay();
Key_temp=(~KeyRow)&0xff;//P0
if(Key_temp!=0)
  Keycode|=(8<<8)|Key_temp; 
  
Col5_check;
Delay();
Key_temp=(~KeyRow)&0xff;//P0
if(Key_temp!=0)
  Keycode|=(16<<8)|Key_temp;  

Col6_check;
Delay();
Key_temp=(~KeyRow)&0xff;//P0
if(Key_temp!=0)
  Keycode|=(32<<8)|Key_temp; 

}


void KeyScan(void)
{
volatile uchar i;

if(!Keydown_flag)
  {
   if(Keydown())
     {
	 Keydown_flag=1;
	 Keydown_time=0;
	 }
  }
else
  {//Keydown
   if(!KeyExec_flag)
     {
	  if(Keydown())
	    {       
		Keydown_time++;
		if(Keydown_time>=20)//去抖动
		  {
		  Keydown_time=0;
		  GetKeycode();
		  Keycode_save=Keycode;
		  
		  /************按键处理********/
		  		  
		  Send_temp[0]=0xaa;//送入数据***
		  KeyExec(Keycode_save);//装入键值
		  Send_temp[2]=DOWN;
		  for(i=1;i<3;i++)
		     Check^=Send_temp[i];//校验
		  Send_temp[3]=Check; 
		  Send_temp[4]=0x55;
		  
		  if(Send_temp[1]!=0)
		     SendExec(5);//启动发送
		  //*******
		  KeyExec_flag=1;
		  }
		}
	  else
	    {//****抖动
		Keydown_flag=0;
		}
	 }
   else//************释放****
     {
	 if(!Keydown())
	   {
	    Keydown_time++;//释放去抖动
		if(Keydown_time>=20)
		  {               //已释放
		   Keydown_time=0;
		   KeyExec_flag=0;
		   Keydown_flag=0;
		   
		   /***********释放处理************/
		   Send_temp[0]=0xaa;//送入数据***
		  KeyExec(Keycode_save);//装入键值
		  Send_temp[2]=RELEASE;
		  for(i=1;i<3;i++)
		     Check^=Send_temp[i];//校验
		  Send_temp[3]=Check; 
		  Send_temp[4]=0x55;
		  
		  if(Send_temp[1]!=0)
		     SendExec(5);//启动发送
		   //*********
		   KeyLong_Time=0;//长按计时
		   Shake_time=0;//组合键去抖动
		   KeyLongExec_flag=0;
		  }
	   }
	 else
	   {   //**未释放*******
	    Keydown_time=0;
	   if(!KeyLongExec_flag)
	     {
		  GetKeycode();
		  if(Keycode==Keycode_save)//长按键
		    {
			  Shake_time=0;
			 KeyLong_Time++;
			 if(KeyLong_Time>=WAIT_TIME)
	           {
			   
		        /********长按处理*******/
			   //Led=0;//******处理完后 KeyLongExec_flag=1
			   //if(Keycode_save==k_F3)KeyLongExec_flag=1;
			   
		       }
			}
		  else  //组合键
		    {
			 Shake_time++;
			 if(Shake_time>=20)//去抖动
			   {
			   Keycode_save=Keycode;
			   /********组合键处理*******/
			   SendData(0xf0);//******处理完后 KeyLongExec_flag=1
			   KeyLongExec_flag=1;
			   }
			}
		 }
	    
	   }
	 }
  }
}

void KeyExec(uint key)
{
switch (key)
   {
    case k_F1://--------------------功能
	    //TI=1;
	   //printf("1");
       Led=!Led;Send_temp[1]=1; 
	    break;
	case k_F2:
       Led=!Led;Send_temp[1]=2;
	    break;
	case k_F3:
       Led=!Led;Send_temp[1]=3;
	    break;
	case k_F4:
       Led=!Led;Send_temp[1]=4;
	    break;
	case k_F5:
       Led=!Led;Send_temp[1]=5;
	    break;
	case k_F6:
       Led=!Led;Send_temp[1]=6;
	    break;
		
	case k_1://--------------------数字1
       Led=!Led;Send_temp[1]=7;
	    break;
	case k_2:
       Led=!Led;Send_temp[1]=8;
	    break;
	case k_3:
       Led=!Led;Send_temp[1]=9;
	    break;
	case k_4:
       Led=!Led;Send_temp[1]=11;//*****************
	    break;
	case k_5:
       Led=!Led;Send_temp[1]=12;
	    break;
	case k_6:
       Led=!Led;Send_temp[1]=13;
	    break;
	case k_7:
       Led=!Led;Send_temp[1]=15;
	    break;
	case k_8:
       Led=!Led;Send_temp[1]=16;
	    break;
	case k_9:
       Led=!Led;Send_temp[1]=17;
	    break;
	case k_0://数字0
       Led=!Led;Send_temp[1]=20;
	    break;
		
	case k_DOT:
       Led=!Led;Send_temp[1]=19;//--------点
	    break;
	case k_CANCEL:
       Led=!Led;Send_temp[1]=21;//--------取消
	    break;
	case k_ADD:
       Led=!Led;Send_temp[1]=26;//--------“+”
	    break;
	case k_SUB:
       Led=!Led;Send_temp[1]=27;//--------“-”
	    break;
	case k_EQUAL:
       Led=!Led;Send_temp[1]=32;//--------“=”
	    break;		
	
	case k_SHIFT:
       Led=!Led;Send_temp[1]=31;//--------上档
	    break;
	case k_INSERT:
       Led=!Led;Send_temp[1]=29;//--------插入
	    break;
	case k_MODIFY:
       Led=!Led;Send_temp[1]=35;//--------修改
	    break;
	case k_DELETE:
       Led=!Led;Send_temp[1]=34;//--------删除
	    break;
	case k_BACKSPACE:
       Led=!Led;Send_temp[1]=10;//--------退格
	    break;
	case k_HELP:
       Led=!Led;Send_temp[1]=43;//--------帮助
	    break;
	case k_PROGRAM:
       Led=!Led;Send_temp[1]=14;//--------编程
	    break;
	case k_PUSHPAPER:
       Led=!Led;Send_temp[1]=18;//----------推纸
	    break;
	case k_PROCEDURE:
       Led=!Led;Send_temp[1]=28;//--------程序
	    break;
	case k_AUTO:
       Led=!Led;Send_temp[1]=44;//--------手动***（调换）
	    break;
	case k_HANDLE:
       Led=!Led;Send_temp[1]=30;//--------自动***（调换）
	    break;
	case k_PAGEUP:
       Led=!Led;Send_temp[1]=36;//--------上翻
	    break;
	case k_PAGEDOWN:
       Led=!Led;Send_temp[1]=37;//--------下翻
	    break;
	case k_UP:
       Led=!Led;Send_temp[1]=39;//--------上
	    break;
	case k_DOWN:
       Led=!Led;Send_temp[1]=40;//--------下
	    break;
	case k_LEFT:
       Led=!Led;Send_temp[1]=41;//--------左
	    break;
	case k_RIGHT:
       Led=!Led;Send_temp[1]=42;//--------右
	    break;
	case k_RUN:
       Led=!Led;Send_temp[1]=38;//--------运行
	    break;
	case k_STOP:
       Led=!Led;Send_temp[1]=33;//--------停止
	    break;
	case k_AHEAD:
       Led=!Led;Send_temp[1]=25;//--------前进互换
	    break;
	case k_BACK:
       Led=!Led;Send_temp[1]=24;//--------后退
	    break;
	
	//以上未使用值（22、23）
	//45~~~~~(以下值>=45)
	case k_MUL:
       Led=!Led;Send_temp[1]=47;//--------“*”************
	    break;
	case k_DIV:
       Led=!Led;Send_temp[1]=48;//--------“/”************
	    break;
	
	case k_FASTFORWARD:
       Led=!Led;Send_temp[1]=46;//--------快进******************互换
	    break;
	case k_FASTBACK:
       Led=!Led;Send_temp[1]=49;//--------快退******************
	    break;
	case k_ENTER:
       Led=!Led;Send_temp[1]=45;//--------确定******************
	    break;
	case k_NULL:
       Led=!Led;Send_temp[1]=50;//----------(待定)**************
	    break;	
	default:
	    break;
   }
}


//************串口发送(查询方式)*******
void SendData(uchar ch)
{
/*
 TI=1;//强制发送：只有TI=1（发送寄存器空）时   才可发送
 printf("%c",ch);
 */
 

SBUF=ch;
while(!TI);//***等待数据发送完毕（TI=1）
TI=0;       //防止开中断时重复进入 




}

void SendExec(uchar Cou)
{
 volatile uchar j;
 for(j=0;j<Cou;j++)
   SendData(Send_temp[j]);
 
 //复位缓冲区**
 for(j=0;j<Cou;j++)
   Send_temp[j]=0;   
 Check=0;          //  
}