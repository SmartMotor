

#ifndef _KEY_H
#define _KEY_H

//#define Led P3_7//指示灯
#define Led P1_0//指示灯

/*
//**************键值定义*********
#define  S1   0x0101
#define  S2   0x0201
#define  S3   0x0401
#define  S4   0x0801
#define  S5   0x1001
#define  S6   0x2001

#define  S7    0x0102
#define  S8    0x0202
#define  S9    0x0402
#define  S10   0x0802
#define  S11   0x1002
#define  S12   0x2002

#define  S13   0x0104
#define  S14   0x0204
#define  S15   0x0404
#define  S16   0x0804
#define  S17   0x1004
#define  S18   0x2004

#define  S19   0x0108
#define  S20   0x0208
#define  S21   0x0408
#define  S22   0x0808//s45双键
#define  S23   0x1008//s44双键
#define  S24   0x2008

#define  S25   0x0110
#define  S26   0x0210
#define  S27   0x0410
#define  S28   0x0810//双键
#define  S29   0x1010
#define  S30   0x2010//双键

#define  S31    0x0120
#define  S32    0x0220
#define  S33    0x0420//双键
#define  S34    0x0820
#define  S35    0x1020
#define  S36    0x2020

#define  S37   0x0140
#define  S38   0x0240//双键
#define  S39   0x0440
#define  S40   0x0840
#define  S41   0x1040
#define  S42   0x2040

#define  S43   0x0180
#define  S44   0x0280//s45双键
#define  S45   0x0480//s44双键
#define  S46   0x0880 
*/
//**************键值定义*********（列+行）
//功能键
#define  k_F1        0x0101               //ROW1
#define  k_F2        0x0201
#define  k_F3        0x0401
#define  k_F4        0x0801
#define  k_F5        0x1001
#define  k_F6        0x2001
//数字键
#define  k_1    0x0102
#define  k_2    0x0202
#define  k_3    0x0402
#define  k_4    0x0802
#define  k_5    0x1002
#define  k_6    0x2002
 
#define  k_7         0x0104
#define  k_8         0x0204
#define  k_9        0x0404 
#define  k_0        0x0804 
#define  k_DOT      0x1004//--------点
#define  k_CANCEL    0x2004//--------取消

#define  k_ADD      0x0108//--------“+”
#define  k_SUB      0x0208//--------“-”
#define  k_MUL      0x0408//--------“*”
#define  k_DIV      0x0808//--------“/”
#define  k_EQUAL    0x1008//--------“=”
#define  k_SHIFT    0x2008//--------上档

#define  k_INSERT      0x0110//--------插入
#define  k_MODIFY      0x0210//--------修改
#define  k_DELETE      0x0410//--------删除
#define  k_BACKSPACE   0x0810//--------退格
#define  k_HELP        0x1010//--------帮助
#define  k_PROGRAM        0x2010//--------编程

#define  k_PUSHPAPER      0x0120//----------推纸
#define  k_PROCEDURE   0x0220//--------程序
#define  k_AUTO        0x0420//--------自动
#define  k_HANDLE      0x0820//--------手动
#define  k_PAGEUP      0x1020//--------上翻
#define  k_PAGEDOWN    0x2020//--------下翻

#define  k_UP          0x0140//--------上
#define  k_DOWN        0x0240//--------下
#define  k_LEFT        0x0440//--------左
#define  k_RIGHT       0x0840//--------右
#define  k_RUN         0x1040//--------运行
#define  k_STOP        0x2040//--------停止

#define  k_AHEAD         0x0180//--------前进
#define  k_BACK          0x0280//--------后退
#define  k_FASTFORWARD   0x0480//--------快进
#define  k_FASTBACK      0x0880 //--------快退
#define  k_ENTER         0x1080 //--------确定
#define  k_NULL          0x2080 //----------(待定)

#define uchar unsigned char
#define uint unsigned int

extern uchar Send_temp[11];//
extern uchar Rec_temp[5];



void Delay(void);

bit Keydown(void);
void KeyScan(void);
void KeyExec(uint key);

void SendData(uchar ch);//发送一字节数据

void SendExec(uchar Cou);//发送处理




#endif