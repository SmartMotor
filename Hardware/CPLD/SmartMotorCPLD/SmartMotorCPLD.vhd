--------------------------------------------------------------------------------
--
--  COPYRIGHT (C) 2009, Zeng Xianwei
--
--  FILENAME: 	SmartMotorCPLD.vhd
--
--  PURPOSE: 	This file contains main control logic for SmartMotor V3.x
--              board. All BLRs are implementted here.
--              
--  Workfile: 	SmartMotorCPLD.vhd
--
--
--  Author:	Zeng Xianwei (xianweizeng@gmail.com)
--
--  History:	 	2009-08-22   Create 
--                  2009-10-03   Use GPIO1,2,7,9 to read the SW status
--                               Change the address of registers
--	                2009-10-07   Use nPWE for SMSC
--	          v1.5  2009-10-13   Fix register address and MMC signals

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity SmartMotorCPLD is 
    Port (
--------------- Globe Signals -------------------------------------------------
-- Inputs
		CLK_32K         :  in std_logic;
		Xs_nReset_Out	:  in std_logic;
-- Outputs
        SYS_nReset      :  out std_logic;
        SYS_Reset       :  out std_logic;
                
--------------- PXA255 Memory Signals -----------------------------------------
-- Inputs
		Xs_nCS		    :  in std_logic_vector(5 downto 1);
		Xs_nOE		    :  in std_logic;
		Xs_nWE          :  in std_logic;	
-- Workaround: A0 is labled to A1 and A1 is labled as A0 in circuit		
		Xs_Addr_L       :  in std_logic_vector(1 downto 0);
		Xs_Addr_H	    :  in std_logic_vector(25 downto 22);
		Xs_CF_nPWE	    :  in std_logic;		
-- Outputs
		Xs_RDY_nWAIT	:  out std_logic;
-- Bi-Direction
		Xs_Data		    :  inout std_logic_vector(7 downto 0);

--------------- PXA255 GPIO Signals -------------------------------------------
-- Bi-Direction
        Xs_GP0		    :  out std_logic;   -- Unused
		Xs_GP1	        :  out std_logic;  -- SW1
		Xs_GP2		    :  out std_logic;  -- SW2
		Xs_GP7		    :  out std_logic;  -- SW3
		Xs_GP9		    :  out std_logic;  -- SW4
		Xs_GP12		    :  out std_logic;  -- RFU
		Xs_GP13		    :  out std_logic;  -- SMSC PME Interrupt
		Xs_GP14 	    :  out std_logic;  -- USB Detect IRQ
		Xs_GP32 	    :  out std_logic;  -- MMC Detect IRQ
		Xs_GP84		    :  out std_logic;   -- Unused
		
--------------- MMC Signals----------------------------------------------------
-- Inputs
		MMC_nCD	        :  in std_logic;
		MMC_WP	        :  in std_logic;
-- Outputs

--------------- Data Bus Buffer Signals  --------------------------------------
-- Outputs
		Buf_nOE	        :  out std_logic;
		Buf_DIR	        :  out std_logic;

--------------- USB Slave Signals --------------------------------------------- 
-- Inputs
		USB_Detect	    :  in std_logic;

--------------- NandFlash Control Signals -------------------------------------
-- Inputs
--              Nand_RDY_nBUSY  :  in std_logic;
-- Outputs
		Nand_nCE        :  out std_logic;
		Nand_nWE        :  out std_logic;
		Nand_nRE        :  out std_logic;
		Nand_CLE        :  out std_logic;
		Nand_ALE        :  out std_logic;

--------------- CFG Button Singals---------------------------------------------
-- Inputs
		CFG_SW 	        :  in std_logic_vector(4 downto 1);

--------------- AVR Signals ---------------------------------------------------
-- Inputs
-- Outputs
		AVR_INT4        :  out std_logic;
		AVR_INT5        :  out std_logic;
		AVR_nReset      :  out std_logic;

--------------- SM502 Signals--------------------------------------------------
-- Inputs
		SM_nHRDY        :  in std_logic;
-- Outputs
		SM_CLKOFF       :  out std_logic;
		SM_nHCS         :  out std_logic;  -- Chip Select
		SM_nHBS         :  out std_logic;  -- Write Enable
		SM_nCPURD       :  out std_logic;  -- Read Enable

--------------- External Signal----------------------------- ------------------
-- Inputs	
-- Output	
-- Bi-Direction
		CPLD_IO1        :  out std_logic;
		CPLD_IO2        :  out std_logic;
		CPLD_IO3        :  out std_logic;
		CPLD_IO4        :  out std_logic;
		CPLD_IO5        :  in std_logic;
		CPLD_IO6        :  out std_logic;
		CPLD_IO7        :  out std_logic;
		CPLD_IO8        :  out std_logic;
		CPLD_IO9        :  out std_logic;
		CPLD_IO10       :  out std_logic;

--------------- LED Signal --------------------------------------------------- 
-- Outputs
		Run_LED		    :  out std_logic
                );
end SmartMotorCPLD;

-------------------------------------------------------------------------------
architecture RTL of SmartMotorCPLD is

constant SYS_ID       : std_logic_vector := "0001";  -- System ID: 1.x
constant CPLD_VERSION : std_logic_vector := "0101";  -- Firmvare Version
  
-- Two Board Control Register address
constant ADDR_REG_VID     : std_logic_vector := "00";  
constant ADDR_REG_BCR 	  : std_logic_vector := "00";
constant ADDR_REG_BSR     : std_logic_vector := "10";

constant ADDR_REG_VID_H   : std_logic_vector := "0000";   -- A22 = 0
constant ADDR_REG_BCR_H   : std_logic_vector := "0001";	  -- A22 = 1
constant ADDR_REG_BSR_H   : std_logic_vector := "0001";   -- A22 = 1

signal System_nReset_node : std_logic;  -- System nReset

signal REG_BCR : std_logic_vector (7 downto 0);   -- Board Control Register
 alias REG_BCR_SM_CLKOFF : std_logic is REG_BCR(3);  -- SM502 Clock OFF
 alias REG_BCR_LED_EN    : std_logic is REG_BCR(4);  -- Run LED Enable
 alias REG_BCR_FLASH_WP  : std_logic is REG_BCR(5);  -- Nor Flash WP
 alias REG_BCR_AVR_STOP0 : std_logic is REG_BCR(6);  -- AVR INT4
 alias REG_BCR_AVR_STOP1 : std_logic is REG_BCR(7);  -- AVR INT5


begin
-------------------------------------------------------------------------------
--  Generate system reset signals
-------------------------------------------------------------------------------

System_nReset_node <= '0' when (Xs_nReset_Out = '0') 
	             else '1';
SYS_nReset         <= System_nReset_node;
SYS_Reset          <= not System_nReset_node;

-------------------------------------------------------------------------------
--  Write Registers
-------------------------------------------------------------------------------

Write_BCR : process (System_nReset_node, Xs_nWE)
begin
    if (System_nReset_node = '0')
    then    REG_BCR(7 downto 0) <= "00000000";	
    elsif (System_nReset_node = '1') 
        then if (Xs_nWE'event) and (Xs_nWE = '1')
             then if  (Xs_nOE = '1') and (Xs_nCS(3) = '0') and (Xs_Addr_H = ADDR_REG_BCR_H) and (Xs_Addr_L = ADDR_REG_BCR)
                  then REG_BCR(7 downto 0) <= Xs_Data(7 downto 0) ;
                  end if;
             end if;
    end if;
end process ;

-------------------------------------------------------------------------------
--  Read Registers
-------------------------------------------------------------------------------
-- Xs_Data(7 downto 0) 
--          <= SYS_ID & CPLD_VERSION       when ((Xs_nWE = '1') and (Xs_nOE = '0') and (Xs_Addr_H(22) = '0') and (Xs_Addr_L = ADDR_REG_VID))
--        else REG_BCR(7 downto 0)	     when ((Xs_nWE = '1') and (Xs_nOE = '0') and (Xs_Addr_H(22) = '1') and (Xs_Addr_L = ADDR_REG_BCR))
--        else "00000" & MMC_WP & (not MMC_nCD) & USB_Detect
--                                         when ((Xs_nWE = '1') and (Xs_nOE = '0') and (Xs_Addr_H(22) = '1') and (Xs_Addr_L = ADDR_REG_BSR))
--        else (others => 'Z');
Xs_Data(7 downto 0) 
          <= SYS_ID & CPLD_VERSION       when ((Xs_nWE = '1') and (Xs_nOE = '0') and (Xs_nCS(3) = '0') and (Xs_Addr_H = ADDR_REG_VID_H) and (Xs_Addr_L = ADDR_REG_VID))
        else REG_BCR(7 downto 0)	     when ((Xs_nWE = '1') and (Xs_nOE = '0') and (Xs_nCS(3) = '0') and (Xs_Addr_H = ADDR_REG_BCR_H) and (Xs_Addr_L = ADDR_REG_BCR))
        else "00000" & MMC_WP & (not MMC_nCD) & USB_Detect
                                         when ((Xs_nWE = '1') and (Xs_nOE = '0') and (Xs_nCS(3) = '0') and (Xs_Addr_H = ADDR_REG_BSR_H) and (Xs_Addr_L = ADDR_REG_BSR))
        else (others => 'Z');

-------------------------------------------------------------------------------
--  Data Bus Buffer Control Logic
-------------------------------------------------------------------------------
--  SM502 and SMSC address range
Buf_nOE <= '0' when ((Xs_nCS(4) = '0') or  -- SM502 Address Range
                     (Xs_nCS(5) = '0')     -- SMSC Address Range 
					)
               else '1';   

-- A(XS), B(VX) : DIR=L : A = B(read)   DIR=H : B=A(write)
-- Buf_DIR 	<= not Xs_RD_nWR;     
-- Buf_DIR 	<= '0' when ((Xs_nWE = '1') and (Xs_nOE = '0'))
--                       else '1';
Buf_DIR 	<= '0' when (Xs_nOE = '0')
                   else '1';

-------------------------------------------------------------------------------
--  Nand Control Logic
-------------------------------------------------------------------------------
Nand_nCE <= '0' when (Xs_nCS(2) = '0') else '1';

Nand_ALE <= '1' when ((Xs_nCS(2) = '0') and (Xs_Addr_H(24) = '0'))
                else '0';
Nand_CLE <= '1' when ((Xs_nCS(2) = '0') and (Xs_Addr_H(24) = '1'))
                else '0';
Nand_nWE <= '0' when ((Xs_nCS(2) = '0') and (Xs_nWE = '0') and (Xs_nOE = '1'))
				else '1';
Nand_nRE <= '0' when ((Xs_nCS(2) = '0') and (Xs_nWE = '1') and (Xs_nOE = '0'))
				else '1';

-------------------------------------------------------------------------------
--  SM502 Control Logic
-------------------------------------------------------------------------------

SM_CLKOFF     <= REG_BCR_SM_CLKOFF;
SM_nHCS       <= Xs_nCS(4);   -- Host CS
SM_nHBS       <= Xs_CF_nPWE;  -- Host Write Enable
SM_nCPURD     <= Xs_nOE;      -- Host Read Enable
Xs_RDY_nWAIT  <= SM_nHRDY;    -- Ready

-------------------------------------------------------------------------------
--  SMSC Control Logic
-------------------------------------------------------------------------------
CPLD_IO1   <=  '0' when ((Xs_nCS(5) = '0') and (Xs_nWE = '1') and (Xs_nOE = '0'))
		  else '1';                  -- SMSC_nRD
CPLD_IO2   <=  '0' when ((Xs_nCS(5) = '0') and (Xs_nWE = '0') and (Xs_nOE = '1'))
		  else '1';                  -- SMSC_nWR
	  
-- Use nPWE @ 2009/10/07
-- SMSC_nRD
-- CPLD_IO1   <=  '0' when (Xs_nCS(5) = '0') and (Xs_CF_nPWE = '1') and (Xs_nOE = '0')
--		  else '1';                  
-- SMSC_nWR		  
-- CPLD_IO2   <=  '0' when (Xs_nCS(5) = '0') and (Xs_CF_nPWE = '0') and (Xs_nOE = '1')
--		  else '1';                  

CPLD_IO3   <=  '0' when (Xs_nCS(5)='0')
	      else '1';                  -- SMSC_nCS

CPLD_IO4   <=  System_nReset_node;   -- SMSC nRESET
Xs_GP13    <=  CPLD_IO5;             -- SMSC PME output  


-------------------------------------------------------------------------------
--  AVR Control Logic
-------------------------------------------------------------------------------

AVR_nReset  <= System_nReset_node;
AVR_INT4    <= REG_BCR_AVR_STOP0;       -- Stop Signal 0, active high
AVR_INT5    <= REG_BCR_AVR_STOP1;       -- Stop Signal 1, active high

-------------------------------------------------------------------------------
--  USB Slave Port Control Logic
-------------------------------------------------------------------------------
Xs_GP14     <= USB_Detect;              -- USB Detect IRQ

-------------------------------------------------------------------------------
--  MMC Control Logic
-------------------------------------------------------------------------------
Xs_GP32     <= MMC_nCD;                 -- MMC Card Detect IRQ

-------------------------------------------------------------------------------
--  SW Keys Control Logic
-------------------------------------------------------------------------------
Xs_GP1     <= CFG_SW(1);                -- SW1
Xs_GP2     <= CFG_SW(2);                -- SW1
Xs_GP7     <= CFG_SW(3);                -- SW1
Xs_GP9     <= CFG_SW(4);                -- SW1

-------------------------------------------------------------------------------
--  Other Signals
-------------------------------------------------------------------------------

Run_LED     <= not REG_BCR_LED_EN;     -- Run LED, Off after power on
	                                   -- EN=1, Led on, which means output = 0

CPLD_IO6    <= 'Z';   -- Unused
CPLD_IO7    <= 'Z';   -- Unused
CPLD_IO8    <= 'Z';   -- Unused
CPLD_IO9    <= 'Z';   -- Unused
CPLD_IO10   <= 'Z';   -- Unused

Xs_GP0      <= 'Z';   -- RFU
Xs_GP12     <= 'Z';   -- RFU
Xs_GP84     <= 'Z';   -- RFU

end RTL;


