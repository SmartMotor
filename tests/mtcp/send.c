/*
 * Send command from serial port
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>

#define UART_PORT  "/dev/ttyS0"

void usage(void);
int open_uart(void);
void close_uart(int fd);
int send_data(int fd, char *data);
char checksum(char *data);

int main(int argc, char *argv[])
{
	int fd;
	int i;

	if (argc < 2) {
		usage();
		exit(1);
	}

	fd = open_uart();

	for (i = 1; i < argc; i++) {
		send_data(fd, argv[i]);
		usleep(50000);
	}

	close_uart(fd);

	return 0;
}

int open_uart(void)
{
	int fd;

	struct termios serialOptions;

	fd = open(UART_PORT, O_RDWR | O_NOCTTY);
	if(fd < 0){
		printf(" Unable to open %s \n", UART_PORT);
	}

	fcntl(fd, F_SETFL, FASYNC);

	tcgetattr(fd,&serialOptions);
	cfsetispeed(&serialOptions,B9600); // 9600 bps
	cfsetospeed(&serialOptions,B9600);
	serialOptions.c_cflag &= ~PARENB;  // 8N1
	serialOptions.c_cflag &= ~CSTOPB;
	serialOptions.c_cflag &= ~CSIZE;
	serialOptions.c_cflag |= CS8;
	serialOptions.c_cflag &= ~CRTSCTS;
	serialOptions.c_cflag |= (CLOCAL | CREAD);

	serialOptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // RAW input

	tcsetattr(fd,TCSANOW,&serialOptions);

	return (fd);
}

void close_uart(int fd)
{
	close(fd);
}

int send_data(int fd, char *data)
{
	char buffer[32];
	int len = strlen(data);

	if (len >= 28) {
		len = 28;
	}

	strncpy(buffer, data, len);
	buffer[len]   = checksum(data);
#if 0
	buffer[len+1] = 'B';
	buffer[len+2] = '\0';
#else
	buffer[len+1] = '\0';
#endif
	printf("send out: %s\n", buffer);
	return write(fd, buffer, len+2);
}

char checksum(char *data)
{
	int i;
	int len;
	char checksum = 0;

	len = strlen(data);
	for (i = 0; i < len; i++) {
		checksum ^= data[i];
	}

	return checksum;
}

void usage(void)
{
	printf("send: Send data via %s\n\n", UART_PORT);
	printf("Usage: send STRING\n");
}


