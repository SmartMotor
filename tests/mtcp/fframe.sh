#!/bin/bash

# send out F Frame

HEADER="%F"
SENDER=send

if [ $# -lt 3 ]; then
    echo "Usage: tframe.sh CURRENT DIRECTION SPEED"
    exit 1
fi

PITCH="0"
ZERO="1"
CURRENT="$1"
DIRECTION="$2"
SPEED="$3"

./${SENDER} ${HEADER}${PITCH}${ZERO}${CURRENT}${DIRECTION}${SPEED}

exit 0
