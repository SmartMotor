#! /bin/bash

# Test speed meter

speed=0

loop=2
while [ $loop -gt 0 ]; do
    
    while [ $speed -le 2000 ]; do
	speed_str=`printf "%04d" $speed`
	./fframe.sh "10" "+" $speed_str

	let "speed = $speed + 100"
    done

    sleep 1

    while [ $speed -ge 0 ]; do
	speed_str=`printf "%04d" $speed`
	./fframe.sh "10" "+" $speed_str

	let "speed = $speed - 100"
    done

    sleep 1
    let "loop = $loop - 1"
done

exit 0
