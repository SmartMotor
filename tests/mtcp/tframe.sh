#!/bin/bash

# send out T Frame

HEADER="%T"
SENDER=send

if [ $# -lt 2 ]; then
    echo "Usage: tframe.sh STATUS DATA"
    exit 1
fi

STATUS="$1"
DATA="$2"

./${SENDER} ${HEADER}${STATUS}${DATA}

exit 0
