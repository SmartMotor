#! /bin/bash

# Test current meter

current=5
speed=0

loop=2
while [ $loop -gt 0 ]; do
    
    while [ $speed -le 2000 ]; do
	speed_str=`printf "%04d" $speed`
	current_str=`printf "%02d" $current`

	./fframe.sh $current_str "+" $speed_str

	let "speed = $speed + 100"
	let "current = $current + 1"
    done

    sleep 1

    while [ $speed -ge 0 ]; do
	speed_str=`printf "%04d" $speed`
	current_str=`printf "%02d" $current`

	./fframe.sh $current_str "+" $speed_str

	let "speed = $speed - 100"
	let "current = $current - 1"
    done

    sleep 1
    let "loop = $loop - 1"
done

exit 0
