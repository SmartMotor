#! /bin/bash

# Test current meter

current=0

loop=2
while [ $loop -gt 0 ]; do
    
    while [ $current -le 30 ]; do
	current_str=`printf "%02d" $current`
	./fframe.sh $current_str "+" "1400"

	let "current = $current + 1"
    done

    sleep 1

    while [ $current -ge 0 ]; do
	current_str=`printf "%02d" $current`
	./fframe.sh $current_str "+" "1400"

	let "current = $current - 1"
    done

    sleep 1

    let "loop = $loop - 1"
done

exit 0
