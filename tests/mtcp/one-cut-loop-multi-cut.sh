#! /bin/bash

# Pusher down, then cut for > 2 times, it should only be one loop

# interval time in second
INTERVAL=1

loop=1
while [ $loop -gt 0 ]; do
    random_data=`cat /dev/urandom|od -N3 -An -i`   
    let "position = $random_data % 150000"
    data=`printf "%06d" $position`

    limit_status="75"

    # 010: cut up, pusher up
    cut_status=2
    ./tframe.sh ${limit_status}${cut_status} $data
    sleep $INTERVAL

    # 011: cut up, pusher down
    cut_status=3
    ./tframe.sh ${limit_status}${cut_status} $data
    sleep $INTERVAL

    # 001: cut down, pusher down
    cut_status=1
    ./tframe.sh ${limit_status}${cut_status} $data
    sleep $INTERVAL

    # 011: cut up, pusher down
    cut_status=3
    ./tframe.sh ${limit_status}${cut_status} $data
    sleep $INTERVAL

    # 001: cut down, pusher down
    cut_status=1
    ./tframe.sh ${limit_status}${cut_status} $data
    sleep $INTERVAL

    # 011: cut up, pusher down
    cut_status=3
    ./tframe.sh ${limit_status}${cut_status} $data
    sleep $INTERVAL

    # 001: cut down, pusher down
    cut_status=1
    ./tframe.sh ${limit_status}${cut_status} $data
    sleep $INTERVAL


    # 011: cut up, pusher down
    cut_status=3
    ./tframe.sh ${limit_status}${cut_status} $data
    sleep $INTERVAL

    # resend to test whether it can be handled correctly
    ./tframe.sh ${limit_status}${cut_status} $data

    # 010: cut up, pusher up
    cut_status=2
    ./tframe.sh ${limit_status}${cut_status} $data
    sleep $INTERVAL

    let "loop = $loop - 1"
done

exit 0
