#! /bin/bash

# Alarm

if [ $# -lt 1 ]; then
    echo "Usage: alarm.sh [OC|OL|LS|RS]"
    exit
fi

X="7"
alarm_code=$1

random_data=`cat /dev/urandom|od -N3 -An -i`   
let "position = $random_data % 150000"
data=`printf "%06d" $position`

./tframe.sh ${X}$alarm_code $data

exit 0
