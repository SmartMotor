#! /bin/bash

# All alarms

# interval time in second
INTERVAL=1
loop=2

while [ $loop -gt 0 ]; do
    # Over Current
    ./alarm.sh "OC"
    sleep $INTERVAL

    # Over Load
    ./alarm.sh "OL"
    sleep $INTERVAL

    # Lose Control
    ./alarm.sh "LS"
    sleep $INTERVAL

    # Comminication Error
    ./alarm.sh "RS"
    sleep $INTERVAL

    let "loop = $loop - 1"
done

exit 0
