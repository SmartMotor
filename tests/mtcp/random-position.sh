#! /bin/bash

# random cut position

# interval time in second
INTERVAL=1

loop=10
while [ $loop -gt 0 ]; do
    random_data=`cat /dev/urandom|od -N3 -An -i`   
    let "position = $random_data % 150000"
    data=`printf "%06d" $position`

    limit_mask=7
    limit_status=5
    let "cut_status = $random_data % 7"

    ./tframe.sh ${limit_mask}${limit_status}${cut_status} $data
    sleep $INTERVAL

    let "loop = $loop - 1"
done

exit 0
