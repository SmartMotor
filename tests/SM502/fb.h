#ifndef __FB_H__
#define __FB_H__

/* 
 *  Function declarations
 */
int fb_open(char *fb_device);
int fb_close(int fd);
int fb_stat(int fd, unsigned int *width, unsigned int *height, unsigned int *depth);
void *fb_mmap(int fd, unsigned int screensize);
int fb_munmap(void *start, size_t length);
int fb_pixel(void *fbmem, int width, int height, int x, int y,
	     unsigned short color);
int fb_fill_region(void *fbmem, int width, int height,
		   int dx, int dy, int w, int h, unsigned short color);
int fb_is_double_buffer(int fb);
int fb_pan(int fd, int screen_id);

#endif /* __FB_H__*/
