/*
 * sm502.c
 *   SM502 graphic controller library
 *
 * Author: Zeng Xianwei
 * ChangeLog:
 *   V0.1    2009/12/15   Initial version
 */
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>

#include "sm502.h"
#include "sm502_internal.h"

#define DEVMEM "/dev/mem"

#define SM502_REG_BASE   0x13e00000
#define SM502_REG_SIZE   0x00200000

typedef unsigned long	 u_long;

struct SM502STRU sm502info;
static char *sm502Reg;
static unsigned char *sm502Mem;

int XAACopyROP[16] =
{
	ROP_0,               /* GXclear */
	ROP_DSa,             /* GXand */
	ROP_SDna,            /* GXandReverse */
	ROP_S,               /* GXcopy */
	ROP_DSna,            /* GXandInverted */
	ROP_D,               /* GXnoop */
	ROP_DSx,             /* GXxor */
	ROP_DSo,             /* GXor */
	ROP_DSon,            /* GXnor */
	ROP_DSxn,            /* GXequiv */
	ROP_Dn,              /* GXinvert*/
	ROP_SDno,            /* GXorReverse */
	ROP_Sn,              /* GXcopyInverted */
	ROP_DSno,            /* GXorInverted */
	ROP_DSan,            /* GXnand */
	ROP_1                /* GXset */
};

int XAAPatternROP[16]=
{
	ROP_0,
	ROP_DPa,
	ROP_PDna,
	ROP_P,
	ROP_DPna,
	ROP_D,
	ROP_DPx,
	ROP_DPo,
	ROP_DPon,
	ROP_PDxn,
	ROP_Dn,
	ROP_PDno,
	ROP_Pn,
	ROP_DPno,
	ROP_DPan,
	ROP_1
};

/*
 * Helper functions, copy from u-boot/drivers/video/sm502.c
 */
u_long regRead32(u_long nOffset)
{
	return *(volatile unsigned long *)(sm502Reg+nOffset);
}

void regWrite32(u_long nOffset, u_long nData)
{
	u_long readback;
	int i=0;

	for(;;)
	{
		*(volatile unsigned long *)(sm502Reg+nOffset) = nData;
		readback = *(volatile unsigned long *)(sm502Reg+nOffset +4);
		if (readback == nData)
			break;
		i++;
		if(i > 2)
			break;
	}
}
void memWrite32(u_long nOffset, u_long nData)
{
	*(volatile unsigned long *)(sm502Mem+nOffset) = nData;
}

void regWrite32DPR(unsigned long nOffset, unsigned long nData)
{
	regWrite32(nOffset + 0x100000, nData);
}

unsigned long regRead32DPR(unsigned long nOffset)
{
	return regRead32(nOffset + 0x100000);
}

void regWrite32SRC(unsigned long nOffset, unsigned long nData)
{
	regWrite32(nOffset, nData);
}

unsigned long regRead32SRC(unsigned long nOffset)
{
	return regRead32(nOffset);
}

static void SMI_DisableClipping(void)
{
	sm502info.ScissorsLeft = 0;
	if (sm502info.bitsPerPixel == 24)
		sm502info.ScissorsRight = (sm502info.height << 16) | (sm502info.width * 3);
	else
		sm502info.ScissorsRight = (sm502info.height << 16) | sm502info.width;
	sm502info.ClipTurnedOn = FALSE;
	WaitQueue();
	regWrite32DPR(0x2C, sm502info.ScissorsLeft);
	regWrite32DPR(0x30, sm502info.ScissorsRight);
}

static void SMI_EngineReset(void)
{
	unsigned long		DEDataFormat = 0;
	int			i;
	int			xyAddress[] = { 320, 400, 512, 640, 800, 1024, 1280, 1600, 2048 };

	sm502info.Stride = (sm502info.width * sm502info.Bpp + 15) & ~15;

	switch (sm502info.bitsPerPixel)
	{
	case 8:
		DEDataFormat = 0x00000000;
		break;

	case 16:
		sm502info.Stride >>= 1;
		DEDataFormat = 0x00100000;
		break;

	case 24:
		DEDataFormat = 0x00300000;
		break;

	case 32:
		sm502info.Stride >>= 2;
		DEDataFormat = 0x00200000;
		break;
	}

	for (i = 0; i < sizeof(xyAddress) / sizeof(xyAddress[0]); i++)
	{
		if (sm502info.rotate)
		{
			if (xyAddress[i] == sm502info.height)
			{
				DEDataFormat |= i << 16;
				break;
			}
		}
		else
		{
			if (xyAddress[i] == sm502info.width)
			{
				DEDataFormat |= i << 16;
				break;
			}
		}
	}

	WaitIdleEmpty();
	regWrite32DPR(0x10, (sm502info.Stride << 16) | sm502info.Stride);
	regWrite32DPR(0x1C, DEDataFormat);
	regWrite32DPR(0x24, 0xFFFFFFFF);
	regWrite32DPR(0x28, 0xFFFFFFFF);
	regWrite32DPR(0x3C, (sm502info.Stride << 16) | sm502info.Stride);
	regWrite32DPR(0x40, 0);
	regWrite32DPR(0x44, 0);
	SMI_DisableClipping();
}

void SMI_GEReset(void)
{
	unsigned int iTempVal;

	WaitIdleEmpty();
	iTempVal = regRead32SRC(0) & ~0x00003000;
	iTempVal = iTempVal & ~0x10000000;
	regWrite32SRC(0, (iTempVal | 0x00003000));
	regWrite32SRC(0, iTempVal);
	WaitIdleEmpty();
	SMI_EngineReset();
}


/* 
 * BitBlit Hardware Acceleration
 */
static int
SMI_SetupForScreenToScreenCopy(int xdir, int ydir, int rop, int trans)
{
	sm502info.AccelCmd = XAACopyROP[rop]
		| SMI_BITBLT
		| SMI_START_ENGINE;

	if ((xdir == -1) || (ydir == -1))
	{
		sm502info.AccelCmd |= SMI_RIGHT_TO_LEFT;
	}

	if (trans != -1)
	{
		sm502info.AccelCmd |= SMI_TRANSPARENT_SRC | SMI_TRANSPARENT_PXL;
		WaitQueue();
		regWrite32DPR(0x20, trans);
	}

	if (sm502info.ClipTurnedOn)
	{
		WaitQueue();
		regWrite32DPR(0x2C, sm502info.ScissorsLeft);
		sm502info.ClipTurnedOn = FALSE;
	}

	return 0;
}

static int
SMI_SubsequentScreenToScreenCopy(int x1, int y1, int x2, int y2, int w, int h)
{
	if (sm502info.AccelCmd & SMI_RIGHT_TO_LEFT)
	{
		x1 += w - 1;
		y1 += h - 1;
		x2 += w - 1;
		y2 += h - 1;
	}

	if (sm502info.bitsPerPixel == 24)
	{
		x1 *= 3;
		x2 *= 3;
		w  *= 3;

		if (sm502info.AccelCmd & SMI_RIGHT_TO_LEFT)
		{
			x1 += 2;
			x2 += 2;
		}
	}

	WaitQueue();
	regWrite32DPR(0x00, (x1 << 16) + (y1 & 0xFFFF));
	regWrite32DPR(0x04, (x2 << 16) + (y2 & 0xFFFF));
	regWrite32DPR(0x08, (w  << 16) + (h  & 0xFFFF));
	regWrite32DPR(0x0C, sm502info.AccelCmd);

	return 0;
}


int sm502_bitblit(int sx, int sy, int dx, int dy, int w, int h)
{
	int ret;
	int xdir = ((sx < dx) && (sy == dy)) ? -1 : 1;
	int ydir = (sy < dy) ? -1 : 1;

	ret = SMI_SetupForScreenToScreenCopy(xdir, ydir, 3, -1);
	if (ret == 0) {
		ret = SMI_SubsequentScreenToScreenCopy(sx, sy, dx, dy, w, h);
	}

	return ret;
}

/* blit from one buffer to another */
int sm502_double_buffer_bitblit(int sx, int sy, int dx, int dy, int w, int h,
				unsigned long src, unsigned long dst)
{
	unsigned long reg1, reg2;
	int ret;

	/* save values */
	reg1 = regRead32DPR(0x40);
	reg2 = regRead32DPR(0x44);

	regWrite32DPR(0x40, src & 0xFFFFFFF0);
	regWrite32DPR(0x44, dst & 0xFFFFFFF0);

	ret = sm502_bitblit(sx, sy, dx, dy, w, h);

	/* restore values */
	regWrite32DPR(0x40, reg1);
	regWrite32DPR(0x44, reg2);

	return ret;
}

/* 
 * Fill Rectangle Hardware Acceleration
 */
static void SMI_SetupForSolidFill(int color, int rop)
{
	sm502info.AccelCmd = XAAPatternROP[rop]
		| SMI_BITBLT
		| SMI_START_ENGINE;

	WaitQueue();
	if (sm502info.ClipTurnedOn)
	{
		regWrite32DPR(0x2C, sm502info.ScissorsLeft);
		sm502info.ClipTurnedOn = FALSE;
	}

	regWrite32DPR(0x14, color);
	regWrite32DPR(0x34, 0xFFFFFFFF);
	regWrite32DPR(0x38, 0xFFFFFFFF);
}

static void SMI_SubsequentSolidFillRect(int x, int y, int w, int h)
{
	if (sm502info.bitsPerPixel == 24)
	{
		x *= 3;
		w *= 3;
	}

	/* Clip to prevent negative screen coordinates */
	if (x < 0)
		x = 0;
	if (y < 0)
		y = 0;

	WaitQueue();

	regWrite32DPR(0x04, (x << 16) | (y & 0xFFFF));
	regWrite32DPR(0x08, (w << 16) | (h & 0xFFFF));
	regWrite32DPR(0x0C, sm502info.AccelCmd);
}

int sm502_rectfill(int dx, int dy, int w, int h, unsigned long color)
{
	SMI_SetupForSolidFill(color, 3); //3=GXcopy
	SMI_SubsequentSolidFillRect(dx, dy, w, h);

	return 0;
}

int sm502_double_buffer_rectfill(int dx, int dy, int w, int h,
				 unsigned long color, unsigned long dst)
{
	unsigned long reg;
	int ret;

	/* save dst register value */
	reg = regRead32DPR(0x44);

	regWrite32DPR(0x44, dst & 0xFFFFFFF0);

	ret = sm502_rectfill(dx, dy, w, h, color);

	/* restore register */
	regWrite32DPR(0x44, reg);

	return ret;
}
/*
 * Draw Line Acceleration
 */
static int sm502_vline(int x, int y, int len, unsigned long color)
{


	return 0;
}

static int sm502_hline(int x, int y, int len, unsigned long color)
{

	return 0;
}


/*
 * Initilization function
 */
int sm502_init(unsigned char *fbmem, int xres, int yres, int bpp)
{
	int fd;

	if ((fd = open(DEVMEM, O_RDWR|O_SYNC)) < 0){
		perror(DEVMEM);
		exit(-1);
	}

	sm502Mem = fbmem;

	sm502info.bitsPerPixel = bpp;
	sm502info.Bpp = bpp / 8;
	sm502info.width = xres;
	sm502info.height = yres;
	sm502info.rotate = 0;

	if (sm502info.bitsPerPixel != 16) {
		printf("Unsupport SM502 BPP %d\n", bpp);
		exit(-1);
	}

	sm502Reg = mmap(NULL, SM502_REG_SIZE, PROT_READ | PROT_WRITE,
			MAP_SHARED, fd, SM502_REG_BASE);
	if ( sm502Reg == MAP_FAILED) {
		perror("SM502 mmap");
		exit(-1);
	}

	close(fd);

	return 0;
}
