/*
 * rectfill.c
 *   Hardware Test program for SM502 graphic controller
 *
 * Author: Zeng Xianwei
 * ChangeLog:
 *   V0.1    2009/12/15   Initial version
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "fb.h"
#include "sm502.h"

#define FB_DEV               "/dev/fb0"

#define RECTFILL_WIDTH       128 /* rectfill region width */
#define RECTFILL_HEIGHT      128 /* rectfill region height */
#define RECTFILL_LOOPS       10  /* loop count */  

unsigned int    fb_width;
unsigned int    fb_height;
unsigned int    fb_depth;
unsigned char   *fbmem;

void usage(void);

int main(int argc, char *argv[])
{
        int             fbdev;
        char           *fb_device;
        unsigned int    screensize;
        unsigned int    x = 0;
        unsigned int    y = 0;
	int             i, j, k;
	int             col, row;
	int             loops = RECTFILL_LOOPS;

        /*
         * check auguments
         */
        if (argc > 2) {
                usage();
                exit(-1);
        } else if (argc == 2) {
		loops = atoi(argv[1]);
		if (loops <= 0) {
			loops = RECTFILL_LOOPS;
		}
	}

        /*
         * open framebuffer device
         */
        if ((fb_device = getenv("FRAMEBUFFER")) == NULL)
                fb_device = FB_DEV;
        fbdev = fb_open(fb_device);

        /*
         * get status of framebuffer device
         */
        fb_stat(fbdev, &fb_width, &fb_height, &fb_depth);
	if (fb_width == 0 || fb_height == 0 || fb_depth == 0) {
		printf("Cann't get framebuffer parameters: %dx%d bpp %d\n",
		       fb_width, fb_height, fb_depth);
		exit(-1);
	}
	
	printf("%s: %dx%d bpp %d\n", fb_device, fb_width, fb_height, fb_depth);

        /*
         * map framebuffer device to shared memory
         */
        screensize = fb_width * fb_height * fb_depth / 8;
        fbmem = fb_mmap(fbdev, screensize);

	col = fb_width / RECTFILL_WIDTH;
	row = fb_height / RECTFILL_HEIGHT;

	sm502_init(fbmem, fb_width, fb_height, fb_depth);

	printf("Test fill region by hardware ... \n");
	i = 0;
	while (i < loops) {
		printf("==== loop %4d ====\n", i);
		sm502_rectfill(0, 0, fb_width - 1, fb_height -1, (0x1000 + (i << 7)) & 0xFFFF);
		usleep(1000);
		x = y = 0;
		for (j = 0; j < row; j++) {
			for (k = 0; k < col; k++) {
				sm502_rectfill(x, y, RECTFILL_WIDTH, RECTFILL_HEIGHT, 
					       (unsigned short)(random() & 0xFFFF));
				x += RECTFILL_WIDTH;
				usleep(1000);
			}
			x = 0;
			y += RECTFILL_HEIGHT;
		}

		i++;
	}

        /*
         * close framebuffer device
         */
        fb_close(fbdev);

        return (0);
}

void usage(void)
{
        printf("Usage: rectfill [loops]\n");
}
