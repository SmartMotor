/*
 * fb.c
 *   framebuffer helper functions
 *
 * Author: Zeng Xianwei
 * ChangeLog:
 *   V0.1    2009/12/15   Initial version
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/fb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <jerror.h>

/*
 * open framebuffer device.
 * return positive file descriptor if success,
 * else return -1.
 */
int fb_open(char *fb_device)
{
        int fd;

        if ((fd = open(fb_device, O_RDWR)) < 0) {
                perror(__func__);
                return (-1);
        }
        return (fd);
}

/*
 * get framebuffer's width,height,and depth.
 * return 0 if success, else return -1.
 */
int fb_stat(int fd, unsigned int *width, unsigned int *height,
	    unsigned int *depth)
{
        struct fb_fix_screeninfo fb_finfo;
        struct fb_var_screeninfo fb_vinfo;

        if (ioctl(fd, FBIOGET_FSCREENINFO, &fb_finfo)) {
                perror(__func__);
                return (-1);
        }

        if (ioctl(fd, FBIOGET_VSCREENINFO, &fb_vinfo)) {
                perror(__func__);
                return (-1);
        }

        *width = fb_vinfo.xres;
        *height = fb_vinfo.yres;
        *depth = fb_vinfo.bits_per_pixel;

        return (0);
}

/* id = 0 : first buffer , 1 : second buffer */
int fb_pan(int fd, int screen_id)
{
        struct fb_var_screeninfo fb_vinfo;

        if (ioctl(fd, FBIOGET_VSCREENINFO, &fb_vinfo)) {
                perror(__func__);
                return (-1);
        }

	if (screen_id == 0){
		fb_vinfo.yoffset = 0;
	} else {
		fb_vinfo.yoffset = fb_vinfo.yres;
	}

        if (ioctl(fd, FBIOPAN_DISPLAY, &fb_vinfo)) {
                perror(__func__);
                return (-1);
        }

        return (0);
}

int fb_is_double_buffer(int fd)
{
	unsigned int xres, yres, xres_v, yres_v;

        struct fb_fix_screeninfo fb_finfo;
        struct fb_var_screeninfo fb_vinfo;

        if (ioctl(fd, FBIOGET_FSCREENINFO, &fb_finfo)) {
                perror(__func__);
                return (-1);
        }

        if (ioctl(fd, FBIOGET_VSCREENINFO, &fb_vinfo)) {
                perror(__func__);
                return (-1);
        }

        xres   = fb_vinfo.xres;
        yres   = fb_vinfo.yres;

	xres_v = fb_vinfo.xres_virtual;
	yres_v = fb_vinfo.yres_virtual;

	printf("fb: %dx%d (virtual %dx%d)\n", xres, yres, xres_v, yres_v);

	return (((xres == xres_v) && (yres == yres_v)) ? 0 : 1);

}

/*
 * map shared memory to framebuffer device.
 * return maped memory if success,
 * else return -1, as mmap dose.
 */
void           *
fb_mmap(int fd, unsigned int screensize)
{
        caddr_t         fbmem;

        if ((fbmem = mmap(0, screensize, PROT_READ | PROT_WRITE,
			  MAP_SHARED, fd, 0)) == MAP_FAILED) {
                perror(__func__);
                return (void *) (-1);
        }

        return (fbmem);
}

/*
 * unmap map memory for framebuffer device.
 */
int
fb_munmap(void *start, size_t length)
{
        return (munmap(start, length));
}

/*
 * close framebuffer device
 */
int fb_close(int fd)
{
        return (close(fd));
}

/*
 * display a pixel on the framebuffer device.
 * fbmem is the starting memory of framebuffer,
 * width and height are dimension of framebuffer,
 * x and y are the coordinates to display,
 * color is the pixel's color value.
 * return 0 if success, otherwise return -1.
 */
int fb_pixel(void *fbmem, int width, int height,
	     int x, int y, unsigned short color)
{
        if ((x >= width) || (y >= height))
                return (-1);

        unsigned short *dst = ((unsigned short *) fbmem + y * width + x);

        *dst = color;
        return (0);
}

/*
 * Fill a framebuffer region with specified color
 *
 *  dx: destination x
 *  dy: destination y
 *  w:  width
 *  h:  height
 *  color: filled color, if color == 0x55AA, then
 *         calculate color for each pixel
 */
int fb_fill_region(void *fbmem, int width, int height,
		   int dx, int dy, int w, int h, unsigned short color)
{
	int x = 0;
	int y = 0;
	int ret = 0;

	while (y < h) {
		while (x < w) {
			unsigned short data;
			if (color != 0x55AA)
				data = color;
			else
				data = 0x1000 + y * 0x80 + x * 0x10;

			ret = fb_pixel(fbmem, width, height, dx + x, dy + y, data);

			if (ret < 0)
				break;

			x ++;
		}

		x = 0;
		y ++;
	}

	return ret;
}
