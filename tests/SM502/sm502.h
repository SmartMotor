/*
 * sm502.h
 *   SM502 library header file
 */
#ifndef __SM502_H__
#define __SM502_H__

int sm502_bitblit(int sx, int sy, int dx, int dy, int w, int h);
int sm502_double_buffer_bitblit(int sx, int sy, int dx, int dy, int w, int h,
				unsigned long src, unsigned long dst);
int sm502_rectfill(int dx, int dy, int w, int h, unsigned long color);
int sm502_double_buffer_rectfill(int dx, int dy, int w, int h,
				 unsigned long color, unsigned long dst);
int sm502_init(unsigned char *fbmem, int xres, int yres, int bpp);

#endif /* __SM502_H__ */
