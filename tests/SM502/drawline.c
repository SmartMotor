/*
 * drawline.c
 *   Hardware Test program for SM502 graphic controller
 *
 * Author: Zeng Xianwei
 * ChangeLog:
 *   V0.1    2009/12/17   Initial version
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "fb.h"
#include "sm502.h"

#define FB_DEV               "/dev/fb0"

#define BITBLIT_WIDTH        48  /* bitblit region width */
#define BITBLIT_HEIGHT       64  /* bitblit region height */
#define BITBLIT_LOOPS        10  /* loop count */  

unsigned int    fb_width;
unsigned int    fb_height;
unsigned int    fb_depth;
unsigned char   *fbmem;

void usage(char *msg);

int main(int argc, char *argv[])
{
        int             fbdev;
        char           *fb_device;
        unsigned int    screensize;
        unsigned int    x = 0;
        unsigned int    y = 0;
	int             i, j ,ret;

        /*
         * check auguments
         */
        if (argc > 1) {
                usage("We don't need options");
                exit(-1);
        }

        /*
         * open framebuffer device
         */
        if ((fb_device = getenv("FRAMEBUFFER")) == NULL)
                fb_device = FB_DEV;
        fbdev = fb_open(fb_device);

        /*
         * get status of framebuffer device
         */
        fb_stat(fbdev, &fb_width, &fb_height, &fb_depth);
	if (fb_width == 0 || fb_height == 0 || fb_depth == 0) {
		printf("Cann't get framebuffer parameters: %dx%d bpp %d\n",
		       fb_width, fb_height, fb_depth);
		exit(-1);
	}
	
	printf("%s: %dx%d bpp %d\n", fb_device, fb_width, fb_height, fb_depth);

        /*
         * map framebuffer device to shared memory
         */
        screensize = fb_width * fb_height * fb_depth / 8;
        fbmem = fb_mmap(fbdev, screensize);

	/* Clear framebuffer */
	printf("==> Test fill region by fbmem ... \n");
	ret = fb_fill_region(fbmem, fb_width, fb_height, 0, 0,
			     fb_width, fb_height, 0x5A5A);
	sleep(3);

	printf("==> Clear framebuffer by memset(fbmem) ... \n");
	memset(fbmem, 0, screensize);
	sleep(3);
	
	sm502_init(fbmem, fb_width, fb_height, fb_depth);

	printf("==> Test rectfill acceleration ... \n");
	i = 0;
	while ( i++ < BITBLIT_LOOPS) {
		printf("==== loop %4d =====\n", i);
		sm502_rectfill(0, 0, fb_width, fb_height, random() & 0xffff);
		sleep(2);
	}

	/*
	 * Loop tests
	 */
	printf("==> Test bitblit acceleration ... \n");
	i = 0;
	while (i++ < BITBLIT_LOOPS) {
		j = 0;
		printf("==== loop %4d =====\n", i);
#if 0
		ret = fb_fill_region(fbmem, fb_width, fb_height, 0, 0,
				     BITBLIT_WIDTH, BITBLIT_HEIGHT, 0x55AA);
		if (ret < 0) {
			printf("fill region failed. loop = %d\n", i);
			continue;
		}
#else
		/* fill background */
		sm502_rectfill(0, 0, fb_width - 1, fb_height -1, random() & 0xffff);
		/* create source region */
		sm502_rectfill(0, 0, BITBLIT_WIDTH, BITBLIT_HEIGHT, i << 8 | (random() & 0xff));
#endif
		
		while (j++ < 1000) {
			x = random() % fb_width;
			y = random() % fb_height;
			
			sm502_bitblit(0, 0, x, y, BITBLIT_WIDTH, BITBLIT_HEIGHT);
			
			/* wait for a while
			 * actually not need in real program
			 */
			usleep(100);
		}
	}

	printf("==> Clear memory using rectfill acceleration ... \n");
	sm502_rectfill(0, 0, fb_width - 1, fb_height - 1, 0);

        /*
         * unmap framebuffer's shared memory
         */
        fb_munmap(fbmem, screensize);

        /*
         * close framebuffer device
         */
        fb_close(fbdev);

        return (0);
}

void usage(char *msg)
{
	printf("\n%s\n", msg);
        printf("Usage: bitblit\n");
}
