#include <stdio.h>
#include <unistd.h>
#include <directfb.h>
#include <string.h>

static IDirectFB *dfb = NULL;
static IDirectFBSurface *primary = NULL;

static int screen_width  = 0;
static int screen_height = 0;

#define DFBCHECK(x...)                                         \
	{						       \
		DFBResult err = x;			       \
							       \
		if (err != DFB_OK)			       \
		{					       \
			fprintf( stderr, "%s <%d>:\n\t", __FILE__, __LINE__ ); \
			DirectFBErrorFatal( #x, err );			\
		}							\
	}

int main (int argc, char **argv)
{
  DFBSurfaceDescription dsc;
  DFBGraphicsDeviceDescription  	dev_dsc;
  int x1, x2, y1, y2;
  IDirectFBImageProvider *provider;
  static IDirectFBSurface *surface1 = NULL;
  
  DFBCHECK (DirectFBInit (&argc, &argv));

  DFBCHECK (DirectFBCreate (&dfb));

  /* Get Device Desciption */
  DFBCHECK (dfb->GetDeviceDescription(dfb, &dev_dsc));

  /* Dump Device info */
  {
	  printf("acceleration_mask:\t%x\n", dev_dsc.acceleration_mask);
	  printf("blitting_flags:\t%x\n", dev_dsc.blitting_flags);
	  printf("drawing_flags:\t%x\n", dev_dsc.drawing_flags);
	  printf("video_memory:\t%x\n", dev_dsc.video_memory);
	  printf("name:\t%s\n", dev_dsc.name);
	  printf("vendor:\t%s\n", dev_dsc.vendor);
  }

  DFBCHECK (dfb->SetCooperativeLevel (dfb, DFSCL_FULLSCREEN));

  dsc.flags = DSDESC_CAPS;

  dsc.caps  = DSCAPS_PRIMARY | DSCAPS_FLIPPING;

  DFBCHECK (dfb->CreateSurface( dfb, &dsc, &primary ));

  DFBCHECK (primary->GetSize (primary, &screen_width, &screen_height));

  DFBCHECK (primary->FillRectangle (primary, 0, 0, screen_width, screen_height));

  DFBCHECK (primary->SetColor (primary, 0xff, 0x00, 0x00, 0xff));

  DFBCHECK (primary->DrawLine (primary, 0 , 0,
			       screen_width -1 , screen_height - 1));

  DFBCHECK (primary->DrawLine (primary, screen_width -1 , 0,
			       0 , screen_height - 1));


  int i=0;
  while (++i < 10) {
	  primary->Blit( primary, surface1, NULL, random() % 1024, random() % 768 );
	  sleep(1);
	  printf("Blitted %d\n",i);
//primary->Flip(primary, NULL, DSFLIP_WAITFORSYNC);
  }
  primary->Flip (primary, NULL, DSFLIP_WAITFORSYNC);
  sleep(5);

  primary->Release( primary );
  surface1->Release(surface1);
  dfb->Release( dfb );

  return 23;
}
