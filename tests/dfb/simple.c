#include <stdio.h>
#include <unistd.h>
#include <directfb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdlib.h>

static IDirectFB *dfb = NULL;
static IDirectFBSurface *primary = NULL;
static IDirectFBSurface *background;

static int screen_width  = 0;
static int screen_height = 0;
#define DFBCHECK(x...)                                         \
  {                                                            \
    DFBResult err = x;                                         \
                                                               \
    if (err != DFB_OK)                                         \
      {                                                        \
        fprintf( stderr, "%s <%d>:\n\t", __FILE__, __LINE__ ); \
        DirectFBErrorFatal( #x, err );                         \
      }                                                        \
  }

static IDirectFBSurface *logo = NULL;

void dump_surface(IDirectFBSurface *surf, int len)
{
	int j, cnt = 0, max_cnt = 4;
	char *p;
	char buf[1024*768*2 + 1024];
	DFBRectangle       rect = {0, 0, 1024, 768};
	DFBCHECK(surf->Read(surf, &rect, buf, 1024 * 2));

	for (p = buf; p < buf + len; p+=4) {
		if (cnt == 0)
			printf("%4x: ", p - buf);
		printf("%8x ", *(unsigned long *)p);
		if (cnt == max_cnt) {
			printf("\n");
			cnt = 0;
		}
		cnt ++;
	}
}

void dump_fb(void *base, unsigned long offset, unsigned long len)
{
	char *p;
	int j, cnt = 0, max_cnt = 4;

	printf("dumping fb, base: 0x%p offset: 0x%lx, len: 0x%lx\n",
	       base, offset, len);
	for (p = (char*)base + offset;
	     p < (char*)base + offset + len;
	     p+=4) {
		if (cnt == 0)
			printf("%4x: ", p - (char*)base);
		printf("%8x ", *(unsigned long *)p);
		if (cnt == max_cnt - 1) {
			printf("\n");
			cnt = 0;
		}
		cnt ++;
	}

}

int main (int argc, char **argv)
{
  int i;

  char buf[1024 * 768 * 2 + 1024];
  DFBRectangle       rect = {0, 0, 1024, 768};
  DFBSurfaceDescription dsc;
  IDirectFBImageProvider *provider;
  int fd;
  void *p;
  int len = 0x00800000;
  int offset = atoi(argv[1]);

  DFBCHECK (DirectFBInit (&argc, &argv));
  DFBCHECK (DirectFBCreate (&dfb));
  DFBCHECK (dfb->SetCooperativeLevel (dfb, DFSCL_FULLSCREEN));
  dsc.flags = DSDESC_CAPS;
  dsc.caps  = DSCAPS_PRIMARY | DSCAPS_FLIPPING;
  DFBCHECK (dfb->CreateSurface( dfb, &dsc, &primary ));
  DFBCHECK (primary->GetSize (primary, &screen_width, &screen_height));

  fd = open("/dev/fb0", O_RDWR);
  if (fd < 0) {
	  printf("open error\n");
	  return -1;
  }

  printf("mapping len: 0x%x\n", len);
  p = mmap( NULL, len,
	    PROT_READ | PROT_WRITE, MAP_SHARED,
	    fd, 0 );
  if (p == MAP_FAILED) {
	  printf("mmap failed\n");
	  return -1;
  }

  /*
   * Create back ground
   */
  DFBCHECK(dfb->CreateImageProvider( dfb, "/wood_andi.jpg",
				     &provider ));
  DFBCHECK (provider->GetSurfaceDescription (provider, &dsc));

  dsc.flags = DSDESC_WIDTH | DSDESC_HEIGHT;

  dsc.width = screen_width;
  dsc.height = screen_height;
  DFBCHECK(dfb->CreateSurface( dfb, &dsc, &background ));
  //dump_surface(background, 1024);

  DFBCHECK(provider->RenderTo( provider, background, NULL ));
  //dump_surface(background, 1024);
  provider->Release( provider );

  DFBCHECK(background->Read(background, &rect, buf, 1024 * 2));

  for (i = 0; i < 5; i++) {
	  //DFBCHECK (primary->FillRectangle (primary, 0, 0, screen_width, screen_height));
	  dump_fb(p, 0, 16);
	  dump_fb(p, 0x67ff80, 16);
	  DFBCHECK (primary->Blit (primary, background, NULL, 0, 0));
	  dump_fb(p, 0, 16);
	  dump_fb(p, 0x67ff80, 16);
	  //DFBCHECK (primary->Write(primary, &rect, buf, 1024 * 2));
	  DFBCHECK (primary->Flip (primary, NULL, DSFLIP_WAITFORSYNC));
	  printf("flipped %d\n", i);
	  sleep(2);
  }

  background->Release (background);
  primary->Release (primary);
  dfb->Release (dfb);
  return 23;
}
