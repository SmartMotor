#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/mman.h>

int main()
{
        int fbfd = 0;
        struct fb_var_screeninfo vinfo;
        struct fb_fix_screeninfo finfo;
        long int screensize = 0;
        char *fbp = 0;
	char dev[] = "/dev/fb0";
        fbfd = open(dev, O_RDWR);
        if (!fbfd) {
                printf("Error: cannot open framebuffer device.\n");
                exit(1);
        }
        printf("The framebuffer device was opened successfully.\n");

        if (ioctl(fbfd, FBIOGET_FSCREENINFO, &finfo)) {
                printf("Error reading fixed information.\n");
                exit(2);
        }

        if (ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo)) {
                printf("Error reading variable information.\n");
                exit(3);
        }

	printf("dev: %s xres: %d yres: %d bpp: %d\n",
	       dev, vinfo.xres, vinfo.yres, vinfo.bits_per_pixel);

        screensize = vinfo.xres * vinfo.yres * vinfo.bits_per_pixel / 8;
        fbp = (char *)mmap(0, screensize, PROT_READ | PROT_WRITE, MAP_SHARED,
fbfd, 0);
        if ((int)fbp == -1) {
                printf("Error: failed to map framebuffer device to memory.\n");
                exit(4);
        }
        printf("The framebuffer device was mapped to memory successfully.\n");
	memset(fbp, 0x0, screensize);
	{
		int i, j;
		int color = 0;
		for (i = 0; i < vinfo.yres; i++) {
			for (j = 0; j < vinfo.xres; j++) {
				fbp[i*800 + j] = color;
			}
			color = (color ++) & 0xff;
		}
	}
	sleep(2);

        munmap(fbp, screensize);
        close(fbfd);
        return 0;
}
