#! /bin/sh

#
# Make auto update images for smt255 target
#
# Coryright 2009 Zeng Xianwei (xianweizeng@gmail.com)
# 
# ChangeLog:
#   v0.1   2009/11/22   The initial version

#
# Image types defined in u-boot/common/image.c
#
# {	IH_TYPE_INVALID,    NULL,	  "Invalid Image",	},
# {	IH_TYPE_FILESYSTEM, "filesystem", "Filesystem Image",	},
# {	IH_TYPE_FIRMWARE,   "firmware",	  "Firmware",		},
# {	IH_TYPE_KERNEL,	    "kernel",	  "Kernel Image",	},
# {	IH_TYPE_MULTI,	    "multi",	  "Multi-File Image",	},
# {	IH_TYPE_RAMDISK,    "ramdisk",	  "RAMDisk Image",	},
# {	IH_TYPE_SCRIPT,     "script",	  "Script",		},
# {	IH_TYPE_STANDALONE, "standalone", "Standalone Program", },
# {	IH_TYPE_FLATDT,     "flat_dt",    "Flat Device Tree",	},
# {	IH_TYPE_KWBIMAGE,   "kwbimage",   "Kirkwood Boot Image",},
# {	-1,		    "",		  "",			},

MKIMAGE=`which mkimage`
OUTPUT_DIR=`pwd`
VERSION="V0.1 by Zeng Xianwei(xianweizeng@gmail.com)"

function usage()
{
    echo "`basename $0`: [${VERSION}]:
    -h/--help          	     show this message
    -v/--version       	     show version
    -b/--uboot  image        create u-boot image
    -k/--kernel image        create kernel image
    -u/--userfs image        create user filesystem image
    -r/--rootfs image        create root filesystem image
    -l/--logofs image        create logo filesystem image
    -s/--system              create system.bin entire flash image
    -o/--output dir          specify the output directory[pwd by default]
" >&2
    exit 1
}

function fatal()
{
    echo "Error: $@"
    exit 1
}

# combine binary and fill gap with 0xFF
function do_system_bin()
{
    echo "Not support yet"
    exit 1
}

function do_mkimage()
{
    case $1 in
	uboot)
	    IMG_TYPE="firmware"
	    IMG_DST=loader.bin
	    IMG_SRC=$UBOOT_SRC
	    IMG_NAME="u-boot"
	    is_uimage=0
	    ;;
	kernel)
	    IMG_TYPE=kernel
	    IMG_DST=kernel.bin
	    IMG_SRC=$KERNEL_SRC
	    IMG_NAME="Linux Kernel"
	    is_uimage=1
	    ;;
	logofs)
	    IMG_TYPE=filesystem
	    IMG_DST=logofs.bin
	    IMG_SRC=$LOGOFS_SRC
	    IMG_NAME="Logo FAT File System"
	    is_uimage=0
	    ;;
	rootfs)
	    IMG_TYPE=filesystem
	    IMG_DST=rootfs.bin
	    IMG_SRC=$ROOTFS_SRC
	    IMG_NAME="Root File System"
	    is_uimage=0
	    ;;
	userfs)
	    IMG_TYPE=filesystem
	    IMG_DST=userfs.bin
	    IMG_SRC=$USERFS_SRC
	    IMG_NAME="User File System"
	    is_uimage=0
	    ;;
	system)
	    IMG_TYPE=multi
	    IMG_DST=system.bin
	    IMG_SRC=${OUTPUT_DIR}/system.bin.src
	    IMG_NAME="System Multi-Image Binary"
	    # Combine the image first
	    do_system_bin $LOGOFS_SRC $KERNEL_SRC $ROOTFS_SRC $USERFS_SRC $IMG_SRC
	    is_uimage=0
	    ;;
	*)
	    usage
	    ;;
    esac

    if [ $is_uimage -eq 0 ]; then
	sudo $MKIMAGE -A arm -O Linux -C none -T $IMG_TYPE -a 0x0 -e 0x0 -n "$IMG_NAME" -d $IMG_SRC ${OUTPUT_DIR}/$IMG_DST
    else
	# Image already is uImage, just rename it
	sudo cp $IMG_SRC ${OUTPUT_DIR}/$IMG_DST
    fi
}

# u-boot
# mkimage -A arm -O Linux -C none -T firmware -a 0x0 -e 0 -n "u-boot" -d u-boot.bin loader.bin
is_system_bin=0

# check options

if [ $# -lt 1 ]; then
    usage
fi

while [ $# -ne 0 ]
do

    case $1 in
	-b | --uboot) 
	    shift
	    MK_LIST="uboot $MK_LIST"
	    UBOOT_SRC=$1
	    ;;
	-k | --kernel)
	    shift
	    MK_LIST="kernel $MK_LIST"
	    KERNEL_SRC=$1
	    ;;
	-r | --rootfs)
	    shift
	    MK_LIST="rootfs $MK_LIST"
	    ROOTFS_SRC=$1
	    ;;
	-l | --logofs)
	    shift
	    MK_LIST="logofs $MK_LIST"
	    LOGOFS_SRC=$1
	    ;;
	-u | --userfs)
	    shift
	    MK_LIST="userfs $MK_LIST"
	    USERFS_SRC=$1
	    ;;
	-s | --system)
	    MK_LIST="system $MK_LIST"
	    is_system_bin=1
	    ;;
	-o | --output)
	    shift
	    OUTPUT_DIR=$1
	    ;;
	-v | --version)
	    echo "`basename $0` $VERSION"
	    exit
	    ;;
	-h | --help)
	    usage
	    ;;
    esac

    shift
done

# check output dir
test -d $OUTPUT_DIR || mkdir -p $OUTPUT_DIR

# check necessary files for system.bin
if [ $is_system_bin -eq 1 ]; then
    if [ -f $LOGOFS_SRC ] && [ -f $KERNEL_SRC ] && [ -f $ROOTFS_SRC ] && [ -f $USERFS_SRC ]; then
	echo "system.bin files are all found"
    else
	echo "Some system.bin files are missing"
	MK_LIST=`echo $MK_LIST | sed -e 's/system//'`
    fi
fi

for img in $MK_LIST; do
    do_mkimage $img
done

echo "Images generated in $OUTPUT_DIR"
exit