#! /bin/sh
#
# mkcelinux.sh: Generate rootfs from CE-Linux
# 
# ChangeLog:
#   V0.1    2009/11/26    Initial version.
#
set -e

export LANG=C

CROSSROOT=/usr/local/arm-sony-linux-gnueabi
STRIP="${CROSSROOT}/devel/bin/arm-sony-linux-gnueabi-dev-strip"

progname=`basename $0`

fatal()
{
    echo "ERROR: $@" >&2
    exit 1
}

test 0 = `id -u` || fatal "$progname: must be super-user"

if [ "$#" -ne 2 ]; then
    cat <<EOF >&2
Usage: $progname <src-dir> <dst-dir>

Make rootfs from CE-Linux userland.
EOF
    exit 1
fi

# Check the target directory
source="$1"
target="$2"

if [ ! -d $source ]; then
    echo "$source: No such directory" >&2
    exit 1
fi

if [ ! -f $source/devel/etc/motd ]; then
    echo "$source: Not a target root directory. /devel/etc/motd is not found." >&2
    exit 1
fi

if [ -d $target ]; then
    echo -n "$target: already exists. Overwrite it? [y/N]? "
    read line
    case $line in
	[yY])
	    echo "rm -rf $target"
	    rm -rf $target
	    break;;
	*)
	    echo "Cancelled."; exit 1;;
    esac
fi

echo "Making target rootfs from $source to $target ... "

# 1. Copy and Change the current directory
echo "cp -a $source $target"
cp -a $source $target
echo "pushd $target"
pushd $target
echo

# 2. Move files which should not be deleted
echo "mv sbin/init init-"
mv sbin/init init-
echo

# 3. Delete directories which contain srel binaries, and make links
dirs="bin lib sbin usr"
for i in $dirs
do
    echo "rm -fr $i; ln -s devel/$i"
    rm -fr $i; ln -s devel/$i
done
echo

# 4. Delete srel development tool under devel direcotry, and make links
echo "pushd devel"
pushd devel
echo rm -rf devel
rm -rf devel
echo "popd"
popd

# 5. Restore the reserved files
echo "mv init- devel/sbin/init"
mv init- devel/sbin/init
echo

# 6. Make links to the devel binaries
links="\
  ../usr/bin/bash#bin/bash \
  ../usr/bin/bash#bin/sh \
  ../usr/bin/stty#bin/stty \
"
for i in $links
do
    base=`echo $i | sed 's/#/ /'`
    echo "ln -s $base"
    ln -s $base
done
echo

#
# Now we have a devel enviroments
#
test -x ${STRIP} || fatal "${STRIP}: File doesn't exists"

# 7. Add device files
echo "Create device files"
pushd dev
test -x ./MAKEDEV && ./MAKEDEV fb mtd input
mknod mmcblk0 b 179 0
mknod mmcblk0p1 b 179 1
mknod mmcblk0p2 b 179 2
test -e rtc && rm -f rtc
mknod rtc0 c 254 0
mknod rtc1 c 254 1
ln -s rtc0 rtc
popd

# 8. Remove temp files
echo "remove temp files"
test -d var && rm -rf var
test -d tmp && rm -rf tmp
test -d home && rm -rf home
mkdir -m 777 tmp
mkdir -m 755 var home mnt 

# 9. Remove header files
echo "remove header files"
pushd devel
rm -rf usr/include
rm -rf usr/share
find . -name "*.h" | xargs rm -rf
popd

# 10. Remove all static library
echo "Remove static libraries"
find . -name "*.a" | xargs rm -f
find . -name "*.la" | xargs rm -f
find . -name "*.o" | xargs rm -f

# 11. Remove other files
pushd devel
test -d var && rm -rf var
test -d usr/arm-sony-linux-gnueabi && rm -rf usr/arm-sony-linux-gnueabi
test -d usr/awk && rm -rf usr/awk
test -d usr/info && rm -rf usr/info
test -d usr/doc && rm -rf usr/doc
test -d usr/libexec && rm -rf usr/libexec && mkdir -p usr/libexec
test -d usr/local/sbin && rm -rf usr/local/sbin
find . -name "*sony*" | xargs rm -rf
# usr/local/share contains directfb data
# test -d usr/local/share && rm -rf usr/local/share
pushd usr/local/share
ls | grep -v directfb | xargs rm -rf
popd

popd

# 12. Remove unused libraries
echo "Remove unused libraries"
pushd devel/lib
ls | grep -v "ld-*" | \
     grep -v "libc.so*" | grep -v "libc-*" | \
     grep -v "lib*" | \
     grep -v "libpthread*" | \
     grep -v "libthread*" | \
     xargs rm -rf
popd

pushd devel/usr/lib
ls | grep -v "libpng*" | \
     grep -v "libjpeg*" | \
     grep -v "libgif-*" | \
     grep -v "libfreetype*" | \
     grep -v "libz.so*" | \
     grep -v "libgcc*" | \
     xargs rm -rf
popd

# 13. Remove unused binaries
echo "remove unused binaries"
pushd devel/usr/bin
# this utils are from arm-sony-linux-gnueabi-dev-coreutils-6.9
# We all use busybox in target, so delete them all
ls | grep -v "stty" | \
     grep -v "cut" | \
     grep -v "bash" | \
     grep -v "gawk" | \
     xargs rm -rf
popd

pushd devel/sbin
ls | grep -v "vfat" | \
     grep -v "dos" | \
     grep -v "init" | \
     grep -v "MAKEDEV" | \
     grep -v "mod" | \
     grep -v "getty" | \
     grep -v "reboot" | \
     grep -v "hwclock" | \
     grep -v "fdisk" | \
     grep -v "freeramdisk" | \
     xargs rm -rf
popd

pushd devel/usr/sbin
ls | grep -v "depmod" | \
     grep -v "modprobe" | \
     grep -v "fsck.cramfs" | \
     grep -v "flash*" | \
     grep -v "nand*" | \
     xargs rm -rf
popd

pushd devel/usr/local/bin
ls | grep -v "dfb*" | \
     grep -v "df_*" | \
     grep -v "directfb*" | \
     xargs rm -rf
popd

find devel/ -type d -name "pkgconfig" | xargs rm -rf

# 14. Strip shared library
echo "strip shared libraries"
pushd devel
find . -type f -name "*.so*" | xargs $STRIP
popd

# 15. Make necessary directory
echo "Make necessary directory"
mkdir -p system/data system/logo system/usr

# 16. Add directfb.rc
mkdir -p devel/usr/local/etc
echo "system=fbdev" > devel/usr/local/etc/directfbrc

# popd $target
popd
echo "Done"

echo "New rootfs size:"
du -hs $target

exit 0
