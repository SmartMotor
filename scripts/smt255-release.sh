#! /bin/sh
#
# Release Scripts
#

. ./smt255-release-functions.sh

images_dir=images

function usage()
{
    echo "Usage: TARGET_NAME [-b] [-k] [-u] [-r] [-l]"
    cat <<EOF
  -b :  Build u-boot
  -k :  Build kernel
  -u :  Build user application (smt_ui)
  -r :  Build rootfs including qt library
  -l :  Build logo filesystem
  -all: Build all
EOF
    exit 1
}

if [ $# -lt 1 ]; then
    usage
fi

target=$1
shift

build_uboot=0
build_kernel=0
build_rootfs=0
build_userfs=0
build_logofs=0

build_none=1

while [ $# -ne 0 ]
do
    case $1 in
	-b ) build_uboot=1; build_none=0;;
	-k ) build_kernel=1; build_none=0;;
	-u ) build_userfs=1; build_none=0;;
	-r ) build_rootfs=1; build_none=0;;
	-l ) build_logofs=1; build_none=0;;
	-all) 
	    build_uboot=1
	    build_kernel=1
	    build_rootfs=1
	    build_userfs=1
	    build_logofs=1
	    build_none=0;;
	*)   usage;;
    esac

    shift
done

if [ $build_none -eq 1 ]; then
    echo "===== Build nothing ====="
    usage
fi

echo "Starting build $build_uboot $build_kernel $build_rootfs $build_userfs $build_logofs"

# create work dir
sudo test -d $install_dir && sudo rm -rf $install_dir
sudo mkdir -p $install_dir

# copy rootfs-rom and rootfs-user
if [ $build_rootfs -ne 0 ]; then
    sudo cp -af ${work_dir}/rootfs-rom  ${install_dir}/
fi

if [ $build_userfs -ne 0 ]; then
    sudo cp -af ${work_dir}/rootfs-user ${install_dir}/
fi

# # 1. build u-boot
if [ $build_uboot -ne 0 ]; then
    ./smt255-release-u-boot.sh $target
    mkimage_options="$mkimage_options -b u-boot.bin"
    target_images="$target_images loader.bin"
fi

# # 2. build kernel
if [ $build_kernel -ne 0 ]; then
    ./smt255-release-kernel.sh $target
    mkimage_options="$mkimage_options -k uImage"
    target_images="$target_images kernel.bin"
fi

# # 3. build rootfs
if [ $build_rootfs -ne 0 ]; then
    ./smt255-release-rootfs.sh $target
    mkimage_options="$mkimage_options -r rootfs.cramfs"
    target_images="$target_images rootfs.bin"
fi

# 4. build userfs
if [ $build_userfs -ne 0 ]; then
    ./smt255-release-userfs.sh $target
    mkimage_options="$mkimage_options -u userfs.jffs2"
    target_images="$target_images userfs.bin"
fi

# # 5. build logofs
if [ $build_logofs -ne 0 ]; then
    ./smt255-release-logofs.sh $target
    mkimage_options="$mkimage_options -l logofs.vfat"
    target_images="$target_images logofs.bin"
fi

# total
pushd $install_dir

sudo rm -f kernel.bin loader.bin logofs.bin rootfs.bin userfs.bin

if [ $build_none -eq 0 ]; then
    /usr/local/bin/mkimage.sh $mkimage_options
    sudo mkdir $images_dir
    sudo mv $target_images $images_dir
fi

popd

exit 0
