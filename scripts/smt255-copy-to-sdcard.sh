#! /bin/sh
#
# Release Scripts
#

# common functions
. ./smt255-release-functions.sh

rootfs_dir=/media/disk-1

HOST_ROOT=/
QT_PATH=usr/local/Trolltech/QtEmbedded-4.6.1-arm
host_qt_dir=${HOST_ROOT}${QT_PATH}


function usage()
{
    echo "Usage: $0 [MOUNT_POINT]"
    exit 1
}

function copy()
{
    # test -d $2 || mkdir -p $2
    echo "sudo cp $@"
    sudo cp -af $@
}

if [ $# -gt 1 ]; then
    usage
fi

if [ "x$1" != "x" ]; then
    rootfs_dir=$1
fi

# uid=`id -u`
# if [ $uid -ne 0 ]; then
#     echo "Please run $0 as superuser"
#     exit 1
# fi

# 3.3. install qt library

#
# library
#
sudo mkdir -p  ${rootfs_dir}/${QT_PATH}/lib/
copy ${host_qt_dir}/lib/libQtCore.so*    ${rootfs_dir}/${QT_PATH}/lib/
copy ${host_qt_dir}/lib/libQtGui.so*     ${rootfs_dir}/${QT_PATH}/lib/
copy ${host_qt_dir}/lib/libQtNetwork.so* ${rootfs_dir}/${QT_PATH}/lib/
copy ${host_qt_dir}/lib/libQtSql.so*     ${rootfs_dir}/${QT_PATH}/lib/

#
# plugin
#
sudo mkdir -p ${rootfs_dir}/${QT_PATH}/plugins/sqldrivers/
copy ${host_qt_dir}/plugins/sqldrivers/libqsqlite.so* ${rootfs_dir}/${QT_PATH}/plugins/sqldrivers/

#
# translations
#
sudo mkdir -p ${rootfs_dir}/${QT_PATH}/translations/
copy ${host_qt_dir}/translations/qt_zh_CN.qm ${rootfs_dir}/${QT_PATH}/translations/

#
# font
#
font1=${host_qt_dir}/lib/fonts/VeraSe.ttf
font2=/home/souken-i/git/SmartMotor/fonts/wqy-microhei-lite.ttc
sudo mkdir -p ${rootfs_dir}/${QT_PATH}/lib/fonts
copy ${font1} ${rootfs_dir}/${QT_PATH}/lib/fonts/
copy ${font2} ${rootfs_dir}/${QT_PATH}/lib/fonts/

# 3.4. install tslib

ts_lib_path=/devel/usr/local/lib
host_ts_lib_dir=$ts_lib_path
host_ts_lib_plugin_dir=${ts_lib_path}/ts

target_ts_lib_dir=${rootfs_dir}${ts_lib_path}
target_ts_lib_plugin_dir=${rootfs_dir}${ts_lib_path}/ts

ts_lib_conf=/home/souken-i/git/SmartMotor/Firmware/tslib/ts.conf
target_ts_lib_conf_dir=${rootfs_dir}/devel/usr/local/etc

sudo mkdir -p $target_ts_lib_plugin_dir
copy ${host_ts_lib_plugin_dir}/input.so    ${target_ts_lib_plugin_dir}/
copy ${host_ts_lib_plugin_dir}/linear.so   ${target_ts_lib_plugin_dir}/
copy ${host_ts_lib_plugin_dir}/pthres.so   ${target_ts_lib_plugin_dir}/
copy ${host_ts_lib_plugin_dir}/variance.so ${target_ts_lib_plugin_dir}/
copy ${host_ts_lib_plugin_dir}/dejitter.so ${target_ts_lib_plugin_dir}/

copy ${host_ts_lib_dir}/libts*.so*  ${target_ts_lib_dir}/

sudo mkdir -p $target_ts_lib_conf_dir
copy ${ts_lib_conf} ${target_ts_lib_conf_dir}/

# 3.5. kernel module

kernel_src_dir=/home/souken-i/git/linux-2.6.31.y-SMT
pushd $kernel_src_dir
sudo make modules_install INSTALL_MOD_PATH=${rootfs_dir}
popd

exit 0
