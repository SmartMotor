#! /bin/sh
#
# Release Scripts
#
# Common Functions
#

set -e

build_user=souken-i

work_dir=/tftpboot/arm
release_date=`date +%Y%m%d`
release_dir=$release_date

install_dir=${work_dir}/${release_dir}

SmartMotor_top_dir=/home/souken-i/git/SmartMotor
uboot_src_dir=/home/souken-i/git/u-boot-smt
kernel_src_dir=/home/souken-i/git/linux-2.6.31.y-SMT

function fatal()
{
    echo "Error: $@"
    exit 1
}

