#! /bin/sh
#
# Release Scripts
#
# Create user filesystem
#

# common functions
. ./smt255-release-functions.sh

userfs_dir=${install_dir}/rootfs-user
target_image=userfs.jffs2

function usage()
{
    echo "Usage: $0 TARGET_NAME"
    exit 1
}

function copy()
{
    # test -d $2 || mkdir -p $2
    echo "sudo cp $@"
    sudo cp -af $@
}

if [ $# -lt 1 ]; then
    usage
fi

# /etc/pointercal
pushd $userfs_dir
default_pointercal=${SmartMotor_top_dir}/Firmware/tslib/pointercal
copy $default_pointercal .
popd

# build application
pushd ${SmartMotor_top_dir}/smt_ui

make clean

/home/souken-i/git/qt-4.6/bin/qmake \
    -spec /home/souken-i/git/qt-4.6/mkspecs/qws/linux-sony-arm-g++ \
    smt_ui.pro 

make -j4

# copy application
test -f smt_ui-${release_date}.tar.gz && rm -f smt_ui-${release_date}.tar.gz
./scripts/tar.sh 
sudo mv smt_ui-${release_date}.tar.gz ${userfs_dir}/
popd

# uncompress application
pushd $userfs_dir

test -d smt_ui && sudo rm -rf smt_ui
sudo tar zxvf smt_ui-${release_date}.tar.gz
sudo chown root:root * -R
sudo chmod a+x smt_ui/smt_ui

sudo rm -f smt_ui-${release_date}.tar.gz

popd

# make jffs2 file system
pushd $install_dir
sudo /usr/local/bin/mkfs.jffs2 -e 256KiB -d rootfs-user -o $target_image

du -hs $target_image
popd

exit 0
