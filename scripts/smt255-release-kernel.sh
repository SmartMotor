#! /bin/sh
#
# Release Scripts
#

# common functions
. ./smt255-release-functions.sh

dst_image=arch/arm/boot/uImage

function usage()
{
    echo "Usage: $0 TARGET_NAME"
    exit 1
}

if [ $# -lt 1 ]; then
    usage
fi

target=$1

# Create u-boot.bin
pushd $kernel_src_dir

test -f ${dst_image} && rm -f ${dst_image}

# update
# cg-update

# clean
make clean

# configure
# ./setup-${target} qt release
./setup-${target} release

# copy config file
sudo cp -f .config ${install_dir}/kconfig


# build
yes '' | make oldconfig

make -j4

# copy
test -f ${dst_image} || fatal "Generate ${dst_image} FAILED"

echo "Install $dst_image to $install_dir"
sudo cp -f $dst_image $install_dir

popd

exit 0
