#! /bin/sh
#
# Release Scripts
#

# common functions
. ./smt255-release-functions.sh

target_image=logofs.vfat

function usage()
{
    echo "Usage: $0 TARGET_NAME"
    exit 1
}

function copy()
{
    # test -d $2 || mkdir -p $2
    echo "sudo cp -f $@"
    sudo cp -f $@
}

if [ $# -lt 1 ]; then
    usage
fi

default_start_gif=${SmartMotor_top_dir}/Firmware/logofs/start.gif
default_logo_bmp=${SmartMotor_top_dir}/Firmware/logofs/logo.bmp

virtual_disk=${SmartMotor_top_dir}/Firmware/logofs/4.5MB.bin
temp_disk_file=logofs-base.bin
mount_point=temp

pushd $install_dir
copy $virtual_disk $temp_disk_file

# mkfs.vfat
sudo /sbin/mkfs.vfat logofs-base.bin

sudo mkdir -p $mount_point
sudo mount -o loop -t vfat $temp_disk_file ${mount_point}/

copy $default_start_gif ${mount_point}/
copy $default_logo_bmp  ${mount_point}/
sync
sync
sudo umount $mount_point

sudo rm -rf $mount_point

# copy
sudo mv $temp_disk_file $target_image

popd

exit 0