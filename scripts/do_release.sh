#! /bin/sh

# do_release: release a package for SmartMotor user
#             application.
#
# Copyright 2008, Zeng Xianwei
#
# Usage: 
#   do_release TARGET_NAME RELEASE_NAME
#     TARGET_NAME:  For which target
#     RELEASE_NAME: Give a name to mark this release
#
# Author:
#   Zeng Xianwei (xianweizeng@gmail.com)
#
# CHANGELOG:
#   0.1.0    2008-12-14

set -e

RELEASE_DATE=`date +%Y%m%d`
RELEASE_BUILD_DIR=/home/souken-i/xscale/Build/

BUILD_OUTPUT=/tmp/smt_release/${RELEASE_DATE}
RELEASE_SERVER="192.168.11.8"
RELEASE_SAVE_DIR=/export/Release/SmartMotor

TARGET_NAME="$1"
RELEASE_NAME="$2"
SMT_APP_NAME="smartmotor-${TARGET_NAME}-${RELEASE_NAME}-${RELEASE_DATE}.tar.gz"

# userfs
USERFS_IMG=MotorUserfs
MOUNT_POINT=motor_userfs
USERFS_NAME="userfs-${TARGET_NAME}-${RELEASE_NAME}-${RELEASE_DATE}.bin"

#
# build MiniGUI library
#
function build_minigui_lib ()
{
    cd ${MINIGUI_LIB_DIR}
    
    echo "==== Build minigui library @ ${MINIGUI_LIB_DIR} ===="

    make clean
    ./setup-arm ${LIB_SETUP_OPTION}
    make

    sudo make install

    # package miniguilib
    mkdir ${BUILD_OUTPUT}/miniguilib

    cp -f /usr/local/arm-linux/lib/libminigui-1.6.so.2.0.0 ${BUILD_OUTPUT}/miniguilib 
    cp -f /usr/local/arm-linux/lib/libmgext-1.6.so.2.0.0 ${BUILD_OUTPUT}/miniguilib
    cp -f /usr/local/arm-linux/lib/libvcongui-1.6.so.2.0.0 ${BUILD_OUTPUT}/miniguilib
    
    arm-linux-strip ${BUILD_OUTPUT}/miniguilib/libminigui-1.6.so.2.0.0
    arm-linux-strip ${BUILD_OUTPUT}/miniguilib/libmgext-1.6.so.2.0.0
    arm-linux-strip ${BUILD_OUTPUT}/miniguilib/libvcongui-1.6.so.2.0.0

    # package and name it as "miniguilib.tar.gz"
    cd ${BUILD_OUTPUT}
    tar zcf miniguilib.tar.gz miniguilib/

}


function build_smt_app()
{
    cd ${SMT_APP_DIR}

    echo "==== Build SmartMotor @ ${SMT_APP_DIR} ===="

    make clean
    ./build-xscale
    make
    
    # package
    cd src
    test -f smartmotor.tar.gz && rm smartmotor.tar.gz
    ./release
    
    cp smartmotor.tar.gz ${BUILD_OUTPUT}/${SMT_APP_NAME}
}

function build_userfs_img()
{
    echo "==== Build SmartMotor userfs ===="

    cd ${RELEASE_BUILD_DIR}

    # mount userfs
    sudo mount -o loop ${USERFS_IMG} ${MOUNT_POINT}

    cd ${MOUNT_POINT}
    
    # smartmotor and miniguilib
    sudo rm -f *.tar.gz
    sudo cp ${BUILD_OUTPUT}/miniguilib.tar.gz .
    sudo cp ${BUILD_OUTPUT}/${SMT_APP_NAME} .

    # unpack
    cd SmartMotor
    sudo rm -rf *
    sudo tar zxf ../${SMT_APP_NAME}

    # chown
    sudo chown root:root * -R
    
    # chmod
    sudo chmod a+x smartmotor

    cd ${RELEASE_BUILD_DIR}
    sudo mkfs.jffs2 -d ${MOUNT_POINT} -o ${USERFS_NAME}

    # umount
    sudo umount ${MOUNT_POINT}

    # install
    sudo cp ${USERFS_NAME} ${BUILD_OUTPUT}/

    echo "==== Build SmartMotor userfs OK ===="
}

# put files to server related directory
function save_release_files()
{
    scp -r ${BUILD_OUTPUT} ${RELEASE_SERVER}:${RELEASE_SAVE_DIR}/
}

function usage()
{
    echo "Usage: "
    echo "$0: TARGET_NAME RELEASE_NAME"
    echo "  TARGET_NAME: old/m3/spain"
    exit 1
}

function do_options ()
{
    if [ $# -lt 2 ]; then
	usage
    fi

    while [ "$#" -ne 0 ]
    do
	case $1 in
	    -h/--help )
		usage
		;;
	    * )
		;;
	esac

	shift
    done
}

# check target_name
function get_target_variables()
{
    case ${TARGET_NAME} in
	old )
	    MINIGUI_LIB_DIR=/home/souken-i/xscale/git/libminigui-1.6.2
	    LIB_SETUP_OPTION="old"
	    SMT_APP_DIR=/home/souken-i/xscale/git/SmartMotor-1.6-old
	    ;;
	m3 )
	    MINIGUI_LIB_DIR=/home/souken-i/xscale/git/libminigui-1.6.2
	    LIB_SETUP_OPTION="m3"
	    SMT_APP_DIR=/home/souken-i/xscale/git/SmartMotor-1.6-M3
	    ;;
	spain )
	    MINIGUI_LIB_DIR=/home/souken-i/xscale/git/libminigui-1.6.2
	    LIB_SETUP_OPTION="spain"
	    SMT_APP_DIR=/home/souken-i/xscale/git/SmartMotor-1.6-M3
	    ;;
	new)
	    MINIGUI_LIB_DIR=/home/souken-i/xscale/git/libminigui-1.6.2-BRANCH_DEV
	    LIB_SETUP_OPTION=""
	    SMT_APP_DIR=/home/souken-i/xscale/git/SmartMotor-2.0
	    ;;
	*)
	    echo "Unknown target name: ${TARGET_NAME}"
	    exit 1
	    ;;
    esac 

    mkdir -p ${BUILD_OUTPUT}

    cd ${BUILD_OUTPUT} && rm -rf *
}

function do_release()
{
    do_options $@
    get_target_variables
    build_minigui_lib
    build_smt_app
    build_userfs_img
    save_release_files

    echo "Release: ${BUILD_OUTPUT}  OK "
}

# main
do_release $@

exit 0