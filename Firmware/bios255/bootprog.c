#include <string.h>

#include "systypes.h"
#include "XsGpioApi.h"
#include "XsGpio.h"
#include "XsIntCtrlApi.h"
#include "utils.h"

typedef	unsigned long u32;
typedef	unsigned short u16;
typedef	unsigned char u8;
typedef	unsigned long ulong;

#define	LINUX_MACHINE_ID	479//89

#define LINUX_PAGE_SHIFT	12
#define LINUX_PAGE_SIZE		(1<<LINUX_PAGE_SHIFT)
/*
 * Usage:
 *  - do not go blindly adding fields, add them at the end
 *  - when adding fields, don't rely on the address until
 *    a patch from me has been released
 *  - unused fields should be zero (for future expansion)
 *  - this structure is relatively short-lived - only
 *    guaranteed to contain useful data in setup_arch()
 */
#define COMMAND_LINE_SIZE 1024

/* This is the old deprecated way to pass parameters to the kernel */
struct param_struct {
    union {
	struct {
	    unsigned long page_size;		/*  0 */
	    unsigned long nr_pages;		/*  4 */
	    unsigned long ramdisk_size;		/*  8 */
	    unsigned long flags;		/* 12 */
#define FLAG_READONLY	1
#define FLAG_RDLOAD	4
#define FLAG_RDPROMPT	8
	    unsigned long rootdev;		/* 16 */
	    unsigned long video_num_cols;	/* 20 */
	    unsigned long video_num_rows;	/* 24 */
	    unsigned long video_x;		/* 28 */
	    unsigned long video_y;		/* 32 */
	    unsigned long memc_control_reg;	/* 36 */
	    unsigned char sounddefault;		/* 40 */
	    unsigned char adfsdrives;		/* 41 */
	    unsigned char bytes_per_char_h;	/* 42 */
	    unsigned char bytes_per_char_v;	/* 43 */
	    unsigned long pages_in_bank[4];	/* 44 */
	    unsigned long pages_in_vram;	/* 60 */
	    unsigned long initrd_start;		/* 64 */
	    unsigned long initrd_size;		/* 68 */
	    unsigned long rd_start;		/* 72 */
	    unsigned long system_rev;		/* 76 */
	    unsigned long system_serial_low;	/* 80 */
	    unsigned long system_serial_high;	/* 84 */
	    unsigned long mem_fclk_21285;       /* 88 */
	} s;
	char unused[256];
    } u1;
    union {
	char paths[8][128];
	struct {
	    unsigned long magic;
	    char n[1024 - sizeof(unsigned long)];
	} s;
    } u2;
    char commandline[COMMAND_LINE_SIZE];
};

void start_kernel(U32 address, U32 initrd)
{
	int i;
	void (*run)(int zero, int arch);
	
	//change ttyS0 to ttyS1, use BTUart
//	char *boot_paramsm = "root=/dev/mtdblock2 init=/linuxrc rootfstype=cramfs load_ramdisk=0 ramdisk_size=16384 console=ttyS1,115200 mem=64m devfs=mount";

	// New parameters for 2.6.23
	char *boot_paramsm = "root=/dev/mtdblock2 rootfstype=cramfs console=ttyS1,115200 mem=64m";
	struct param_struct *params = (struct param_struct *)0xa0000100;
	
	for(i=0; i<(sizeof(struct param_struct)>>2); i++)
		((U32 *)params)[i] = 0;
	params->u1.s.page_size = LINUX_PAGE_SIZE;
	params->u1.s.nr_pages = (0x04000000 >> LINUX_PAGE_SHIFT);

	/* copy command line */
	memcpy(params->commandline, boot_paramsm, strlen(boot_paramsm));

	printf("Starting kernel @ 0x%x\n", address);
	printf("Boot params = %s\n", params->commandline);
	delay(10);
	
//	DisableInt();
//	MMU_DisableICache();
//	MMU_InvalidateICache();


	run = (void (*)(int, int))address;
	run(0, LINUX_MACHINE_ID);
}
