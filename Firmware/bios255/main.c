#include <string.h>

#include "systypes.h"
#include "XsGpioApi.h"
#include "XsGpio.h"
//#include "xsClkMgr.h"
#include "xsuart.h"
#include "xsffuart.h"
//#include "xsost.h"
#include "XsIntCtrlApi.h"
#include "XsIntCtrl.h"
#include "xsClkMgr.h"

#include "utils.h"
#include "norflash.h"

/*
 * debug option
 * 
 * Should be commented out when release
 */
#define BIOS_DEBUG

extern char getConsoleInput(void);

static void irq_handler(void)
{
	int i;
	UINT32 irq_pending = XsIntCtrlRegsP->ICIP;
	
	for(i=XSIC_MIN_SGNL; i<=XSIC_MAX_SGNL; i++) {
		if (irq_pending & (1 << i))        
			if (XsIcIrqHandlerTable[i])
				XsIcIrqHandlerTable[i] (XsIcIrqHandlerParamTable[i]);
	}
}

void enable_irq(UINT32 irqno)
{
	if((irqno>=XSIC_MIN_SGNL)&&(irqno<=XSIC_MAX_SGNL))
		if(XsIcIrqHandlerTable[irqno])
			XsIntCtrlRegsP->ICMR |= 1u<<irqno; // Enable int signal 
}

void disable_irq(UINT32 irqno)
{
	if((irqno>=XSIC_MIN_SGNL)&&(irqno<=XSIC_MAX_SGNL))
		XsIntCtrlRegsP->ICMR &= ~(1u<<irqno); // Disable int signal 
}

extern UINT32 c_irq_handler;

static void irq_init(void)
{
	XsIntCtrlRegsP->ICMR = 0;	//mask all interrupts
	XsIntCtrlRegsP->ICLR = 0;	//all interrupts routed to irq mode
	
	c_irq_handler = (UINT32)irq_handler;
	memset (&XsIcIrqHandlerTable[0], 0, sizeof(XsIcIrqHandlerTable));
	memset (&XsIcIrqHandlerParamTable[0], 0, sizeof(XsIcIrqHandlerParamTable));

#ifdef	USE_OST_IRQ
	XsIcIrqHandlerTable[XSIC_OST_REG1_SGNL]=TimerMsIrq;
	enable_irq(XSIC_OST_REG1_SGNL);
	pOST->OIER |= OIER_E1;
#endif
}

void start_kernel(U32 address, U32 initrd);

XsGpioRegsT *XsGpioRegsP = (XsGpioRegsT *)XS_GPIO_REGISTER_BASE;

static U32 downloadAddress, downloadFileSize;

static int WaitComDownload(U32 address)
{
	U16 dnCs, chkCs;
	U32 i, len;
	char tmp[4];
	puts("Please transmit file by DNW\n");
	
	for(i=0; i<4; i++)
		tmp[i] = getch();
		
	len = (tmp[3]<<24)|(tmp[2]<<16)|(tmp[1]<<8)|tmp[0];
	len -= 6;
	
	chkCs = 0;
	for(i=0; i<len; i++) {
		((char *)address)[i] = getch();
		chkCs += ((char *)address)[i];
	}
	dnCs  = getch();
	dnCs |= (getch()<<8);
	
	if(dnCs!=chkCs) {
		puts("Download checksum error!\n");
		return -1;
	}
	
	printf("Download success, dst 0x%x file size = 0x%x\n", downloadAddress, len);
	downloadFileSize = len;
	return 0;
}

/*
 * Kernel image load address
 */
#define	LINUX_KERNEL_ADDR	0xa0008000

static void ComDownload(U32 a1, U32 a2)
{

	downloadAddress = LINUX_KERNEL_ADDR;
	if(WaitComDownload(downloadAddress))
		return;
}

/*
static void UnprotectFlash(U32 addr, U32 len)
{
	UnlockAllBlks();
}
*/

static void ProgFile2NorFlash(U32 addr, U32 len)
{
	U32 dst;
	char c;
	
	puts("\nwhich part to program?\n1: Bootloader\n2: Kernel\n3: Rootfs\n4: Userfs\nEsc: exit\n");
	while(1) {
		c = getch();
		if((c>='1')&&(c<='4'))
			break;
		if(c==0x1b)
			return;
	}
	if(c=='1')
		dst = 0x00000000;
	if(c=='2')
		dst = 0x00040000;
	if(c=='3')
		dst = 0x00200000;
	if(c=='4')
		dst = 0x01000000;
	
	printf("Are you sure to program NOR flash from 0x%x with data at 0x%x, len=%d\n ?", dst, addr, len);
	if(getyorn()) {
		ProgNorFlash(dst, addr, len);
	}
}

static void RunProgInNorFlash(U32 addr, U32 len)
{

	U32 dst = 0x00040000;

#ifdef BIOS_DEBUG
	printf("Loading kernel from 0x00004000 to 0x%x\n", LINUX_KERNEL_ADDR);
#endif		
	
	memcpy((char *)LINUX_KERNEL_ADDR, (char *)dst, 0x200000-0x40000);	// 2M - 256K
	start_kernel(LINUX_KERNEL_ADDR, 0);
}

#ifdef BIOS_DEBUG
static void RunKernelInSdram(U32 addr, U32 len)
{
	/* kernel has been downloaded to LINUX_KERNEL_ADDR, so
	 * just jump to it.
	 */
	start_kernel(LINUX_KERNEL_ADDR, 0);
}

static void RunProgInSdram(U32 addr, U32 len)
{
	void (*run)(void);

	/* Program has been downloaded to LINUX_KERNEL_ADDR,
	 * so just jump to it.
	 */
	run = (void (*)(void))addr;
	run();
}
#endif


static void EraseFileSystem(U32 addr, U32 len)
{
	U32 dst;
	U32 erase_len;
	char c;
	
	puts("\nwhich part to erase?\n1: Root FS\n2: User FS\nEsc: exit\n");
	while(1) {
		c = getch();
		if((c>='1')&&(c<='2'))
			break;
		if(c==0x1b)
			return;
	}

	if(c=='1'){
		dst = 0x00200000;
		erase_len = 0xE00000;	//14M max
	}
	if(c=='2'){
		dst = 0x01000000;
		erase_len = 0x1000000;	//16M max
	}
	
	printf("Are you sure to Erase NOR flash from 0x%x to 0x%x\n ?", dst, (dst+erase_len));
	if(getyorn()) {
		EraseNorFlashArea(dst,erase_len);
	}
	
}

#ifdef BIOS_DEBUG
static void DumpMemory(U32 addr, U32 len)
{
	U32 dst;
	U32 dump_len;
	char c;
	U32 i, j, val;
	U32 *data;
	
	puts("\nwhich part of memory to dump?\n0: 0xA0008000\n1: 0xA0100000\nEsc: exit\n");
	while(1) {
		c = getch();
		if(((c>='0')&&(c<='9')) || ((c>='a')&&(c<='f')) || ((c>='A')&&(c<='F')))
			break;
		if(c==0x1b)
			return;
	}
	
	dump_len = 0x100;

	if(c=='0'){
		dst = 0xA0008000;
	}
	
	if((c>='1') && (c<='9')){
		dst = 0xA0000000 + 0x100000 * (c - '0');
	}

	if((c>='a') && (c<='f')){
		dst = 0xA0A00000 + 0x100000 * (c - 'a');
	}

	if((c>='A') && (c<='F')){
		dst = 0x00000000 + 0x100000 * (c - 'A');
	}

	/* dump register */
	__asm {
		mov  val, pc
	}
	printf("PC: %0x\n", val);

	__asm {
		mov val, sp
	}
	printf("SP: %0x\n", val);
	
	
	/* dump memory */
	data = (U32 *)dst;
	for(i = 0; i < dump_len; i+=0x10) {
		printf("%x: ", dst + i );
		for(j = 0; j < 0x10; j+=4) {
			printf("%08x ", *data++);
		}
		printf("\n");
	}
}
#endif

static struct {
	void (*FuncAddr)(U32, U32);
	char *str;
} Functions[] = {
		{ComDownload, "Download File By Uart(DNW) to SDRAM"},
		{ProgFile2NorFlash, "Program Downloaded File To NOR Flash"},
		{RunProgInNorFlash, "Boot Kernel from NOR Flash"},
		{EraseFileSystem, "Erase File Systems"},	
//		{UnprotectFlash, "Unprotect NOR Flash"},
#ifdef BIOS_DEBUG
		{RunProgInSdram, "Run Downloaded Program From SDRAM"},
		{RunKernelInSdram, "Boot Kernel From SDRAM"},
		{DumpMemory, "Dump Memory"},
#endif
		{0, 0}
	};

/*	
static int read_boot_key(void)
{
	int i, j, key, try_loops;
	
	try_loops = 5;
	do {
		key = XsGpioRegsP->GPLRs[0]&3;
		for(i=0; i<10; i++) {
			for(j=0; j<20; j++) {
				if(key!=(XsGpioRegsP->GPLRs[0]&3))
					break;
			}
			delay(10);
			if(j<20)
				break;
		}
		if(i>=10)
			return key; 
	} while (try_loops--);
	return 3;	//default
}
*/

int main(void)
{
	int waittime = 1;
	int ledflash = 2;
	char retval;
		
	irq_init();
	console_init();
	
	XsGpioRegsP->GPDRs[0] |= (1<<26)|(1<<16)|(7<<10)|(3<<4);	//4,5,10,11,12,16,26
	
	/* Show LED and BEEP */
	while(ledflash--){
		show_led(1, 3);
		delay(100);
		show_led(1, 0);
		delay(100);
	}
	
/*	xsCMEnableClock(PWM1_CE);
	out_l(300, 0x40c00004);
	out_l(499, 0x40c00008);
	out_l(59,  0x40c00000);
*/	
	puts("\n***************************************\n");
	puts("XScale BIOS Version 3.2\n");		
	puts("Copyright 2009 Xianwei Zeng (xianweizeng@gmail.com)\n");	
	puts("Build @ 2009/09/27 for SmartMotor V3.2 Platform\n");	

	ChkNorFlash();

	//printf("boot key=%d\n", read_boot_key());
	
	while(waittime--){
		retval = getConsoleInput();				// Get User Input
		if(retval > 0)
			break;
	}
	// No key pressed,boot
	if(retval == 0){
		RunProgInNorFlash(LINUX_KERNEL_ADDR,0x200000);
	}
	
	while(1) {
		U16 i;
		U8 c;
			
		puts("Please Select:\n");
		for(i=0; Functions[i].FuncAddr; i++)
			printf("%d : %s\n", i, Functions[i].str);
		putch('\n');	
		c = getch();
		if((c>='0')&&(c<('0'+i)))
			(*Functions[c-'0'].FuncAddr)(downloadAddress, downloadFileSize);
	}
	
	return 0;
}
