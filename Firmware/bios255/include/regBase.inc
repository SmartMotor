;/***********************************************************************
; * Copyright ?2001 Intel Corp.
;************************************************************************
;
;  Processor-specific register block base addresses.
;
;    This is an assembly language include file.
;
;***********************************************************************/

    IF :LNOT: :DEF: __regBase_s
__regBase_s   EQU     1

;------------------------------------------------------------------------

; processor steppings
STEPPING_A0              EQU     (0x0)
STEPPING_A1              EQU     (0x1)
STEPPING_B0              EQU     (0x2)
STEPPING_B1              EQU     (0x3)
STEPPING_B2              EQU     (0x4)
STEPPING_C0              EQU     (0x5)
STEPPING_A0X55           EQU     (0x6)       ; used to indicate the PXA255 A0

;------------------------------------------------------------------------

; sdram bank base addresses
SDRAM0_PHYSICAL_BASE     EQU     0xA0000000
SDRAM1_PHYSICAL_BASE     EQU     0xA4000000
SDRAM2_PHYSICAL_BASE     EQU     0xA8000000
SDRAM3_PHYSICAL_BASE     EQU     0xAC000000

; dma registers base address
DMAREGS_PHYSICAL_BASE    EQU     0x40000000  ; base address in the physical map
DMAREGS_VIRTUAL_BASE     EQU     0x40000000  ; base address in the virtual map

; ffuart registers base address
FFUARTREGS_PHYSICAL_BASE EQU     0x40100000  ; base address in the physical map
FFUARTREGS_VIRTUAL_BASE  EQU     0x40100000  ; base address in the virtual map

; btuart registers base address
BTUARTREGS_PHYSICAL_BASE EQU     0x40200000  ; base address in the physical map
BTUARTREGS_VIRTUAL_BASE  EQU     0x40200000  ; base address in the virtual map

; i2c registers base address
I2CREGS_PHYSICAL_BASE    EQU     0x40301690  ; base address in the physical map
I2CREGS_VIRTUAL_BASE     EQU     0x40301690  ; base address in the virtual map

; i2s registers base address
I2SREGS_PHYSICAL_BASE    EQU     0x40400000  ; base address in the physical map
I2SREGS_VIRTUAL_BASE     EQU     0x40400000  ; base address in the virtual map

; ac97 registers base address
AC97REGS_PHYSICAL_BASE   EQU     0x40500000  ; base address in the physical map
AC97REGS_VIRTUAL_BASE    EQU     0x40500000  ; base address in the virtual map

; udc registers base address
UDCREGS_PHYSICAL_BASE    EQU     0x40600000  ; base address in the physical map
UDCREGS_VIRTUAL_BASE     EQU     0x40600000  ; base address in the virtual map

; stuart registers base address
STUARTREGS_PHYSICAL_BASE EQU     0x40700000  ; base address in the physical map
STUARTREGS_VIRTUAL_BASE  EQU     0x40700000  ; base address in the virtual map

; icp registers base address
ICPREGS_PHYSICAL_BASE    EQU     0x40800000  ; base address in the physical map
ICPREGS_VIRTUAL_BASE     EQU     0x40800000  ; base address in the virtual map

; rtc registers base address
RTCREGS_PHYSICAL_BASE    EQU     0x40900000  ; base address in the physical map
RTCREGS_VIRTUAL_BASE     EQU     0x40900000  ; base address in the virtual map

; os timer registers base address
OSTREGS_PHYSICAL_BASE    EQU     0x40A00000  ; base address in the physical map
OSTREGS_VIRTUAL_BASE     EQU     0x40A00000  ; base address in the virtual map

; pwm 0 registers base address
PWM0REGS_PHYSICAL_BASE   EQU     0x40B00000  ; base address in the physical map
PWM0REGS_VIRTUAL_BASE    EQU     0x40B00000  ; base address in the virtual map

; pwm 1 registers base address
PWM1REGS_PHYSICAL_BASE   EQU     0x40C00000  ; base address in the physical map
PWM1REGS_VIRTUAL_BASE    EQU     0x40C00000  ; base address in the virtual map

; interrupt controller base address
INTREGS_PHYSICAL_BASE    EQU     0x40D00000  ; base address in the physical map
INTREGS_VIRTUAL_BASE     EQU     0x40D00000  ; base address in the virtual map

; gpio registers base address
GPIOREGS_PHYSICAL_BASE   EQU     0x40E00000  ; base address in the physical map
GPIOREGS_VIRTUAL_BASE    EQU     0x40E00000  ; base address in the virtual map

; power mgr / reset cntrl registers base address
PMRCREGS_PHYSICAL_BASE   EQU     0x40F00000  ; base address in the physical map
PMRCREGS_VIRTUAL_BASE    EQU     0x40F00000  ; base address in the virtual map

; ssp registers base address
SSPREGS_PHYSICAL_BASE    EQU     0x41000000  ; base address in the physical map
SSPREGS_VIRTUAL_BASE     EQU     0x41000000  ; base address in the virtual map

; mmc registers base address
MMCREGS_PHYSICAL_BASE    EQU     0x41100000  ; base address in the physical map
MMCREGS_VIRTUAL_BASE     EQU     0x41100000  ; base address in the virtual map

; adc registers base address
ADCREGS_PHYSICAL_BASE    EQU     0x41200000  ; base address in the physical map
ADCREGS_VIRTUAL_BASE     EQU     0x41200000  ; base address in the virtual map

; clock registers base address
CLOCKREGS_PHYSICAL_BASE  EQU     0x41300000  ; base address in the physical map
CLOCKREGS_VIRTUAL_BASE   EQU     0x41300000  ; base address in the virtual map

; lcd controller base address
LCDREGS_PHYSICAL_BASE    EQU     0x44000000  ; base address in the physical map
LCDREGS_VIRTUAL_BASE     EQU     0x44000000  ; base address in the virtual map

; memory controller base address
MEMREGS_PHYSICAL_BASE    EQU     0x48000000  ; base address in the physical map
MEMREGS_VIRTUAL_BASE     EQU     0x48000000  ; base address in the virtual map

;------------------------------------------------------------------------

    ENDIF

    END     ; EOF regBase.inc

