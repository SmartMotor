/******************************************************************************
**
**  COPYRIGHT (C) 2000, 2001 Intel Corporation.
**
**  This software as well as the software described in it is furnished under 
**  license and may only be used or copied in accordance with the terms of the 
**  license. The information in this file is furnished for informational use 
**  only, is subject to change without notice, and should not be construed as 
**  a commitment by Intel Corporation. Intel Corporation assumes no 
**  responsibility or liability for any errors or inaccuracies that may appear 
**  in this document or any software that may be provided in association with 
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by 
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:       XsIntCtrlApi.h
**
**  PURPOSE:        Constants, structure and variable declarations for the 
**                  main processor on-board first level Interrupt Controller
**
**  Valid for    :  Cotulla processor
**
**  EAS VERSION  :  2.1
**
**  $Modtime: 6/29/01 6:55p $ 
******************************************************************************/
#ifndef _XSINTCTRLAPI_H
#define _XSINTCTRLAPI_H

#ifdef  _DEFINING_XSINTCTRLAPI
#define EXTRN
#else
#define EXTRN   extern
#endif


// All IC registers but one use the following shift and mask definitions

// BEGIN Interrupt Controller Register bit shift definitions


typedef enum XsIcInterruptSignalsE 
{
    XSIC_MIN_SGNL         =  8,     // Bits 0..7 are reserved
    XSIC_MAX_SGNL         = 31,

    XSIC_GPIO_0_SGNL      =  8,     // Specified edge detected on GPIO0
    XSIC_GPIO_1_SGNL      =  9,     // Specified edge detected on GPIO1
    XSIC_GPIO_2_OR_80_SGNL= 10,     // OR of specced edge detects on 2..80
    XSIC_USB_SGNL         = 11,     // USB interrupt
    XSIC_PMU_SGNL         = 12,     // Performance Monitor Unit int
    XSIC_I2S_SGNL         = 13,     // I2S Controller svc request + err
    XSIC_AC97_SGNL        = 14,     // AC97 Controller status + err
    XSIC_RSVD8_SGNL       = 15,     // Reserved.
    XSIC_RSVD9_SGNL       = 16,     // Reserved.
    XSIC_LCD_SGNL         = 17,     // LCD Controller status + err
    XSIC_I2C_SGNL         = 18,     // I2C Bus Interface Unit  status + err
                                    // Infrared Communication Port
    XSIC_ICP_SGNL         = 19,      //  Tx, Rx, err
    XSIC_STUART_SGNL      = 20,     // Standard UART: Tx, Rx, err
    XSIC_BTUART_SGNL      = 21,     // Bluetooth UART: Tx, Rx, err
    XSIC_FFUART_SGNL      = 22,     // Full Feature UART: Tx, Rx, err
    XSIC_MMC_SGNL         = 23,     // MultiMedia Card status / error
    XSIC_SSP_SGNL         = 24,     // SSP service request
    XSIC_DMA_SGNL         = 25,     // DMA Channel service request 
    XSIC_OST_REG0_SGNL    = 26,     // OS timer equals match register 0
    XSIC_OST_REG1_SGNL    = 27,     // OS timer equals match register 1
    XSIC_OST_REG2_SGNL    = 28,     // OS timer equals match register 2 
    XSIC_OST_REG3_SGNL    = 29,     // OS timer equals match register 3
    XSIC_1HZ_CLKTCK_SGNL  = 30,     // One Hz clock TIC occurred.
    XSIC_RTC_ALRM_SGNL    = 31      // RTC equals alarm register

} XsIcInterruptSignalsT ; // end enum XsIcInterruptSignalsT

// END Interrupt Controller Register bit shift definitions


// Function pointer types
                                    // Function pointer type required for main 
                                    // processor first level int. handlers.
typedef void (*XsIcL1IntHandlerFnPT)(void *); 


// Context structure definitions

typedef struct XsIntCtrlContextS
{
    VUINT32 loggedError;
    VUINT32 isrError;
    BOOL    hwInitSucceeded;
    VUINT32 oldVectorContent;
    VUINT32 oldVectorHandlerPointerAddress;
    VUINT32 oldVectorHandlerPointer;
    UINT32  desiredHandlerAddress;
    VUINT32 newVectorContent;
    VUINT32 newVectorHandlerPointerAddress;
    VUINT32 newVectorHandlerPointer;

}  XsIntCtrlContextT ;


EXTRN XsIntCtrlContextT XsIntCtrlContext;


/*
*******************************************************************************
    Function prototypes in API of the Cotulla on-board 
        first level Interrupt Controller
*******************************************************************************
*/

extern void   XsIcSWInit                   (void);
extern UINT32 XsIcHWSetup                  (void);
extern UINT32 XsIcRegisterHandler          (XsIcInterruptSignalsT sourceID, 
                                            XsIcL1IntHandlerFnPT handler, 
                                            void *param);
extern UINT32 XsIcUnRegisterHandler        (XsIcInterruptSignalsT sourceID);
extern UINT32 XsIcEnableIrqDeviceInt       (XsIcInterruptSignalsT sourceID);
extern UINT32 XsIcDisableIrqDeviceInt      (XsIcInterruptSignalsT sourceID);
extern UINT32 XsIcDisableIrqDeviceIntAll   (void);

extern UINT32 XsIcDisableInterruptsIrq (void);    // From XsIntCtrla.s
extern void   XsIcEnableInterruptsIrq (void);     // From XsIntCtrla.s
extern void   XsIcRestoreInterruptsIrq (UINT32);  // From XsIntCtrla.s
extern UINT32 XsIcGetCpsr (void);                 // From XsIntCtrla.s

#ifndef  _irqhandle_h
// When cotirqx.h is no long longer used, this exclusion can be removed

int IRQ_DisableInterrupts (int mask);
void IRQ_EnableInterrupts (int mask);

#endif

//---------Debug-----------

void XsIcDbgPrintCurrentState (void);

void XsIcDoVecs (void);

#undef EXTRN

#endif // #ifndef _XSINTCTRLAPI_H
