;/***********************************************************************
; * Copyright � 2001 Intel Corp.
;************************************************************************
;
;  Cotulla GPIO register defines
;
;    This is an assembly language include file.
;
;***********************************************************************/

    IF :LNOT: :DEF: __regGpio_s
__regGpio_s   EQU     1

;------------------------------------------------------------------------

; register offsets from the base address
GPLRx_OFFSET            EQU     (0x00)
GPLRy_OFFSET            EQU     (0x04)
GPLRz_OFFSET            EQU     (0x08)
GPDRx_OFFSET            EQU     (0x0C)
GPDRy_OFFSET            EQU     (0x10)
GPDRz_OFFSET            EQU     (0x14)
GPSRx_OFFSET            EQU     (0x18)
GPSRy_OFFSET            EQU     (0x1C)
GPSRz_OFFSET            EQU     (0x20)
GPCRx_OFFSET            EQU     (0x24)
GPCRy_OFFSET            EQU     (0x28)
GPCRz_OFFSET            EQU     (0x2C)
GRERx_OFFSET            EQU     (0x30)
GRERy_OFFSET            EQU     (0x34)
GRERz_OFFSET            EQU     (0x38)
GFERx_OFFSET            EQU     (0x3C)
GFERy_OFFSET            EQU     (0x40)
GFERz_OFFSET            EQU     (0x44)
GEDRx_OFFSET            EQU     (0x48)
GEDRy_OFFSET            EQU     (0x4C)
GEDRz_OFFSET            EQU     (0x50)
GAFR0x_OFFSET           EQU     (0x54)
GAFR1x_OFFSET           EQU     (0x58)
GAFR0y_OFFSET           EQU     (0x5C)
GAFR1y_OFFSET           EQU     (0x60)
GAFR0z_OFFSET           EQU     (0x64)
GAFR1z_OFFSET           EQU     (0x68)

; register bit masks - for the x register set (except gafr, see below)
GPXXx_GPIO0             EQU     BIT0
GPXXx_GPIO1             EQU     BIT1
GPXXx_GPIO2             EQU     BIT2
GPXXx_GPIO3             EQU     BIT3
GPXXx_GPIO4             EQU     BIT4
GPXXx_GPIO5             EQU     BIT5
GPXXx_GPIO6             EQU     BIT6
GPXXx_GPIO7             EQU     BIT7
GPXXx_GPIO8             EQU     BIT8
GPXXx_GPIO9             EQU     BIT9
GPXXx_GPIO10            EQU     BIT10
GPXXx_GPIO11            EQU     BIT11
GPXXx_GPIO12            EQU     BIT12
GPXXx_GPIO13            EQU     BIT13
GPXXx_GPIO14            EQU     BIT14
GPXXx_GPIO15            EQU     BIT15
GPXXx_GPIO16            EQU     BIT16
GPXXx_GPIO17            EQU     BIT17
GPXXx_GPIO18            EQU     BIT18
GPXXx_GPIO19            EQU     BIT19
GPXXx_GPIO20            EQU     BIT20
GPXXx_GPIO21            EQU     BIT21
GPXXx_GPIO22            EQU     BIT22
GPXXx_GPIO23            EQU     BIT23
GPXXx_GPIO24            EQU     BIT24
GPXXx_GPIO25            EQU     BIT25
GPXXx_GPIO26            EQU     BIT26
GPXXx_GPIO27            EQU     BIT27
GPXXx_GPIO28            EQU     BIT28
GPXXx_GPIO29            EQU     BIT29
GPXXx_GPIO30            EQU     BIT30
GPXXx_GPIO31            EQU     BIT31

; register bit masks - for the y register set (except gafr, see below)
GPYYy_GPIO32            EQU     BIT0
GPYYy_GPIO33            EQU     BIT1
GPYYy_GPIO34            EQU     BIT2
GPYYy_GPIO35            EQU     BIT3
GPYYy_GPIO36            EQU     BIT4
GPYYy_GPIO37            EQU     BIT5
GPYYy_GPIO38            EQU     BIT6
GPYYy_GPIO39            EQU     BIT7
GPYYy_GPIO40            EQU     BIT8
GPYYy_GPIO41            EQU     BIT9
GPYYy_GPIO42            EQU     BIT10
GPYYy_GPIO43            EQU     BIT11
GPYYy_GPIO44            EQU     BIT12
GPYYy_GPIO45            EQU     BIT13
GPYYy_GPIO46            EQU     BIT14
GPYYy_GPIO47            EQU     BIT15
GPYYy_GPIO48            EQU     BIT16
GPYYy_GPIO49            EQU     BIT17
GPYYy_GPIO50            EQU     BIT18
GPYYy_GPIO51            EQU     BIT19
GPYYy_GPIO52            EQU     BIT20
GPYYy_GPIO53            EQU     BIT21
GPYYy_GPIO54            EQU     BIT22
GPYYy_GPIO55            EQU     BIT23
GPYYy_GPIO56            EQU     BIT24
GPYYy_GPIO57            EQU     BIT25
GPYYy_GPIO58            EQU     BIT26
GPYYy_GPIO59            EQU     BIT27
GPYYy_GPIO60            EQU     BIT28
GPYYy_GPIO61            EQU     BIT29
GPYYy_GPIO62            EQU     BIT30
GPYYy_GPIO63            EQU     BIT31

; register bit masks - for the z register set (except gafr, see below)
GPZZz_GPIO64            EQU     BIT0
GPZZz_GPIO65            EQU     BIT1
GPZZz_GPIO66            EQU     BIT2
GPZZz_GPIO67            EQU     BIT3
GPZZz_GPIO68            EQU     BIT4
GPZZz_GPIO69            EQU     BIT5
GPZZz_GPIO70            EQU     BIT6
GPZZz_GPIO71            EQU     BIT7
GPZZz_GPIO72            EQU     BIT8
GPZZz_GPIO73            EQU     BIT9
GPZZz_GPIO74            EQU     BIT10
GPZZz_GPIO75            EQU     BIT11
GPZZz_GPIO76            EQU     BIT12
GPZZz_GPIO77            EQU     BIT13
GPZZz_GPIO78            EQU     BIT14
GPZZz_GPIO79            EQU     BIT15
GPZZz_GPIO80            EQU     BIT16

; gpio alternate function defines
GPIO_CLEAR_ALL          EQU     0x0
GPIO_NORMAL_FUNCT       EQU     0x0
GPIO_ALT_FUNCT_1        EQU     0x1
GPIO_ALT_FUNCT_2        EQU     0x2
GPIO_ALT_FUNCT_3        EQU     0x3
GPIO_ALT_FUNCT_CLR      EQU     0x3

; gafr bit-pair offsets for alternate functions
GAFR0x_GPIO0_OFFSET     EQU     (0)
GAFR0x_GPIO1_OFFSET     EQU     (2)
GAFR0x_GPIO2_OFFSET     EQU     (4)
GAFR0x_GPIO3_OFFSET     EQU     (6)
GAFR0x_GPIO4_OFFSET     EQU     (8)
GAFR0x_GPIO5_OFFSET     EQU     (10)
GAFR0x_GPIO6_OFFSET     EQU     (12)
GAFR0x_GPIO7_OFFSET     EQU     (14)
GAFR0x_GPIO8_OFFSET     EQU     (16)
GAFR0x_GPIO9_OFFSET     EQU     (18)
GAFR0x_GPIO10_OFFSET    EQU     (20)
GAFR0x_GPIO11_OFFSET    EQU     (22)
GAFR0x_GPIO12_OFFSET    EQU     (24)
GAFR0x_GPIO13_OFFSET    EQU     (26)
GAFR0x_GPIO14_OFFSET    EQU     (28)
GAFR0x_GPIO15_OFFSET    EQU     (30)

GAFR1x_GPIO16_OFFSET    EQU     (0)
GAFR1x_GPIO17_OFFSET    EQU     (2)
GAFR1x_GPIO18_OFFSET    EQU     (4)
GAFR1x_GPIO19_OFFSET    EQU     (6)
GAFR1x_GPIO20_OFFSET    EQU     (8)
GAFR1x_GPIO21_OFFSET    EQU     (10)
GAFR1x_GPIO22_OFFSET    EQU     (12)
GAFR1x_GPIO23_OFFSET    EQU     (14)
GAFR1x_GPIO24_OFFSET    EQU     (16)
GAFR1x_GPIO25_OFFSET    EQU     (18)
GAFR1x_GPIO26_OFFSET    EQU     (20)
GAFR1x_GPIO27_OFFSET    EQU     (22)
GAFR1x_GPIO28_OFFSET    EQU     (24)
GAFR1x_GPIO29_OFFSET    EQU     (26)
GAFR1x_GPIO30_OFFSET    EQU     (28)
GAFR1x_GPIO31_OFFSET    EQU     (30)

GAFR0y_GPIO32_OFFSET    EQU     (0)
GAFR0y_GPIO33_OFFSET    EQU     (2)
GAFR0y_GPIO34_OFFSET    EQU     (4)
GAFR0y_GPIO35_OFFSET    EQU     (6)
GAFR0y_GPIO36_OFFSET    EQU     (8)
GAFR0y_GPIO37_OFFSET    EQU     (10)
GAFR0y_GPIO38_OFFSET    EQU     (12)
GAFR0y_GPIO39_OFFSET    EQU     (14)
GAFR0y_GPIO40_OFFSET    EQU     (16)
GAFR0y_GPIO41_OFFSET    EQU     (18)
GAFR0y_GPIO42_OFFSET    EQU     (20)
GAFR0y_GPIO43_OFFSET    EQU     (22)
GAFR0y_GPIO44_OFFSET    EQU     (24)
GAFR0y_GPIO45_OFFSET    EQU     (26)
GAFR0y_GPIO46_OFFSET    EQU     (28)
GAFR0y_GPIO47_OFFSET    EQU     (30)

GAFR1y_GPIO48_OFFSET    EQU     (0)
GAFR1y_GPIO49_OFFSET    EQU     (2)
GAFR1y_GPIO50_OFFSET    EQU     (4)
GAFR1y_GPIO51_OFFSET    EQU     (6)
GAFR1y_GPIO52_OFFSET    EQU     (8)
GAFR1y_GPIO53_OFFSET    EQU     (10)
GAFR1y_GPIO54_OFFSET    EQU     (12)
GAFR1y_GPIO55_OFFSET    EQU     (14)
GAFR1y_GPIO56_OFFSET    EQU     (16)
GAFR1y_GPIO57_OFFSET    EQU     (18)
GAFR1y_GPIO58_OFFSET    EQU     (20)
GAFR1y_GPIO59_OFFSET    EQU     (22)
GAFR1y_GPIO60_OFFSET    EQU     (24)
GAFR1y_GPIO61_OFFSET    EQU     (26)
GAFR1y_GPIO62_OFFSET    EQU     (28)
GAFR1y_GPIO63_OFFSET    EQU     (30)

GAFR0z_GPIO64_OFFSET    EQU     (0)
GAFR0z_GPIO65_OFFSET    EQU     (2)
GAFR0z_GPIO66_OFFSET    EQU     (4)
GAFR0z_GPIO67_OFFSET    EQU     (6)
GAFR0z_GPIO68_OFFSET    EQU     (8)
GAFR0z_GPIO69_OFFSET    EQU     (10)
GAFR0z_GPIO70_OFFSET    EQU     (12)
GAFR0z_GPIO71_OFFSET    EQU     (14)
GAFR0z_GPIO72_OFFSET    EQU     (16)
GAFR0z_GPIO73_OFFSET    EQU     (18)
GAFR0z_GPIO74_OFFSET    EQU     (20)
GAFR0z_GPIO75_OFFSET    EQU     (22)
GAFR0z_GPIO76_OFFSET    EQU     (24)
GAFR0z_GPIO77_OFFSET    EQU     (26)
GAFR0z_GPIO78_OFFSET    EQU     (28)
GAFR0z_GPIO79_OFFSET    EQU     (30)

GAFR1z_GPIO80_OFFSET    EQU     (0x0)

; gafr bit-field masks
GAFR0x_GPIO0            EQU     (3:SHL:GAFR0x_GPIO0_OFFSET)
GAFR0x_GPIO1            EQU     (3:SHL:GAFR0x_GPIO1_OFFSET)
GAFR0x_GPIO2            EQU     (3:SHL:GAFR0x_GPIO2_OFFSET)
GAFR0x_GPIO3            EQU     (3:SHL:GAFR0x_GPIO3_OFFSET)
GAFR0x_GPIO4            EQU     (3:SHL:GAFR0x_GPIO4_OFFSET)
GAFR0x_GPIO5            EQU     (3:SHL:GAFR0x_GPIO5_OFFSET)
GAFR0x_GPIO6            EQU     (3:SHL:GAFR0x_GPIO6_OFFSET)
GAFR0x_GPIO7            EQU     (3:SHL:GAFR0x_GPIO7_OFFSET)
GAFR0x_GPIO8            EQU     (3:SHL:GAFR0x_GPIO8_OFFSET)
GAFR0x_GPIO9            EQU     (3:SHL:GAFR0x_GPIO9_OFFSET)
GAFR0x_GPIO10           EQU     (3:SHL:GAFR0x_GPIO10_OFFSET)
GAFR0x_GPIO11           EQU     (3:SHL:GAFR0x_GPIO11_OFFSET)
GAFR0x_GPIO12           EQU     (3:SHL:GAFR0x_GPIO12_OFFSET)
GAFR0x_GPIO13           EQU     (3:SHL:GAFR0x_GPIO13_OFFSET)
GAFR0x_GPIO14           EQU     (3:SHL:GAFR0x_GPIO14_OFFSET)
GAFR0x_GPIO15           EQU     (3:SHL:GAFR0x_GPIO15_OFFSET)

GAFR1x_GPIO16           EQU     (3:SHL:GAFR1x_GPIO16_OFFSET)
GAFR1x_GPIO17           EQU     (3:SHL:GAFR1x_GPIO17_OFFSET)
GAFR1x_GPIO18           EQU     (3:SHL:GAFR1x_GPIO18_OFFSET)
GAFR1x_GPIO19           EQU     (3:SHL:GAFR1x_GPIO19_OFFSET)
GAFR1x_GPIO20           EQU     (3:SHL:GAFR1x_GPIO20_OFFSET)
GAFR1x_GPIO21           EQU     (3:SHL:GAFR1x_GPIO21_OFFSET)
GAFR1x_GPIO22           EQU     (3:SHL:GAFR1x_GPIO22_OFFSET)
GAFR1x_GPIO23           EQU     (3:SHL:GAFR1x_GPIO23_OFFSET)
GAFR1x_GPIO24           EQU     (3:SHL:GAFR1x_GPIO24_OFFSET)
GAFR1x_GPIO25           EQU     (3:SHL:GAFR1x_GPIO25_OFFSET)
GAFR1x_GPIO26           EQU     (3:SHL:GAFR1x_GPIO26_OFFSET)
GAFR1x_GPIO27           EQU     (3:SHL:GAFR1x_GPIO27_OFFSET)
GAFR1x_GPIO28           EQU     (3:SHL:GAFR1x_GPIO28_OFFSET)
GAFR1x_GPIO29           EQU     (3:SHL:GAFR1x_GPIO29_OFFSET)
GAFR1x_GPIO30           EQU     (3:SHL:GAFR1x_GPIO30_OFFSET)
GAFR1x_GPIO31           EQU     (3:SHL:GAFR1x_GPIO31_OFFSET)

GAFR0y_GPIO32           EQU     (3:SHL:GAFR0y_GPIO32_OFFSET)
GAFR0y_GPIO33           EQU     (3:SHL:GAFR0y_GPIO33_OFFSET)
GAFR0y_GPIO34           EQU     (3:SHL:GAFR0y_GPIO34_OFFSET)
GAFR0y_GPIO35           EQU     (3:SHL:GAFR0y_GPIO35_OFFSET)
GAFR0y_GPIO36           EQU     (3:SHL:GAFR0y_GPIO36_OFFSET)
GAFR0y_GPIO37           EQU     (3:SHL:GAFR0y_GPIO37_OFFSET)
GAFR0y_GPIO38           EQU     (3:SHL:GAFR0y_GPIO38_OFFSET)
GAFR0y_GPIO39           EQU     (3:SHL:GAFR0y_GPIO39_OFFSET)
GAFR0y_GPIO40           EQU     (3:SHL:GAFR0y_GPIO40_OFFSET)
GAFR0y_GPIO41           EQU     (3:SHL:GAFR0y_GPIO41_OFFSET)
GAFR0y_GPIO42           EQU     (3:SHL:GAFR0y_GPIO42_OFFSET)
GAFR0y_GPIO43           EQU     (3:SHL:GAFR0y_GPIO43_OFFSET)
GAFR0y_GPIO44           EQU     (3:SHL:GAFR0y_GPIO44_OFFSET)
GAFR0y_GPIO45           EQU     (3:SHL:GAFR0y_GPIO45_OFFSET)
GAFR0y_GPIO46           EQU     (3:SHL:GAFR0y_GPIO46_OFFSET)
GAFR0y_GPIO47           EQU     (3:SHL:GAFR0y_GPIO47_OFFSET)

GAFR1y_GPIO48           EQU     (3:SHL:GAFR1y_GPIO48_OFFSET)
GAFR1y_GPIO49           EQU     (3:SHL:GAFR1y_GPIO49_OFFSET)
GAFR1y_GPIO50           EQU     (3:SHL:GAFR1y_GPIO50_OFFSET)
GAFR1y_GPIO51           EQU     (3:SHL:GAFR1y_GPIO51_OFFSET)
GAFR1y_GPIO52           EQU     (3:SHL:GAFR1y_GPIO52_OFFSET)
GAFR1y_GPIO53           EQU     (3:SHL:GAFR1y_GPIO53_OFFSET)
GAFR1y_GPIO54           EQU     (3:SHL:GAFR1y_GPIO54_OFFSET)
GAFR1y_GPIO55           EQU     (3:SHL:GAFR1y_GPIO55_OFFSET)
GAFR1y_GPIO56           EQU     (3:SHL:GAFR1y_GPIO56_OFFSET)
GAFR1y_GPIO57           EQU     (3:SHL:GAFR1y_GPIO57_OFFSET)
GAFR1y_GPIO58           EQU     (3:SHL:GAFR1y_GPIO58_OFFSET)
GAFR1y_GPIO59           EQU     (3:SHL:GAFR1y_GPIO59_OFFSET)
GAFR1y_GPIO60           EQU     (3:SHL:GAFR1y_GPIO60_OFFSET)
GAFR1y_GPIO61           EQU     (3:SHL:GAFR1y_GPIO61_OFFSET)
GAFR1y_GPIO62           EQU     (3:SHL:GAFR1y_GPIO62_OFFSET)
GAFR1y_GPIO63           EQU     (3:SHL:GAFR1y_GPIO63_OFFSET)

GAFR0z_GPIO64           EQU     (3:SHL:GAFR0z_GPIO64_OFFSET)
GAFR0z_GPIO65           EQU     (3:SHL:GAFR0z_GPIO65_OFFSET)
GAFR0z_GPIO66           EQU     (3:SHL:GAFR0z_GPIO66_OFFSET)
GAFR0z_GPIO67           EQU     (3:SHL:GAFR0z_GPIO67_OFFSET)
GAFR0z_GPIO68           EQU     (3:SHL:GAFR0z_GPIO68_OFFSET)
GAFR0z_GPIO69           EQU     (3:SHL:GAFR0z_GPIO69_OFFSET)
GAFR0z_GPIO70           EQU     (3:SHL:GAFR0z_GPIO70_OFFSET)
GAFR0z_GPIO71           EQU     (3:SHL:GAFR0z_GPIO71_OFFSET)
GAFR0z_GPIO72           EQU     (3:SHL:GAFR0z_GPIO72_OFFSET)
GAFR0z_GPIO73           EQU     (3:SHL:GAFR0z_GPIO73_OFFSET)
GAFR0z_GPIO74           EQU     (3:SHL:GAFR0z_GPIO74_OFFSET)
GAFR0z_GPIO75           EQU     (3:SHL:GAFR0z_GPIO75_OFFSET)
GAFR0z_GPIO76           EQU     (3:SHL:GAFR0z_GPIO76_OFFSET)
GAFR0z_GPIO77           EQU     (3:SHL:GAFR0z_GPIO77_OFFSET)
GAFR0z_GPIO78           EQU     (3:SHL:GAFR0z_GPIO78_OFFSET)
GAFR0z_GPIO79           EQU     (3:SHL:GAFR0z_GPIO79_OFFSET)

GAFR1z_GPIO80           EQU     (3:SHL:GAFR1z_GPIO80_OFFSET)

;------------------------------------------------------------------------
PLATFORM_GPSRx        EQU     (0x00008030)	;cs must set high!
PLATFORM_GPSRy        EQU     (0x00000002)
PLATFORM_GPSRz        EQU     (0x0001c000)

PLATFORM_GPCRx        EQU     (0x00010000)
PLATFORM_GPCRy        EQU     (0x00000000)
PLATFORM_GPCRz        EQU     (0x00000000)

PLATFORM_GPDRx        EQU     (0x03818170)
PLATFORM_GPDRy        EQU     (0xfcffab82)
PLATFORM_GPDRz        EQU     (0x0001FFFF)

;0-pwr_on(input),1-input/swrst,2~5,7,9,10,11,12,13,14-input,6-mmcclk,8-mmccs0,15-ncs1
PLATFORM_GAFR0x        EQU     (0x80011000)
;16,17,19-output,18-rdy,20,21,23,24,26,27-output,22,25-input,28~31-ac97
PLATFORM_GAFR1x        EQU     (0x005a8010)
;32-input,33-output,34~41-ffuart,42~45-btuart,46,47-irda
PLATFORM_GAFR0y        EQU     (0xa99a9558)
;48~57-pcmcia,58~63-lcd
PLATFORM_GAFR1y        EQU     (0xAAA5aaaa)
;64~77-lcd,78,79-ncs2,3
PLATFORM_GAFR0z        EQU     (0xAAAAAAAA)
;80-ncs4,81~84-io
PLATFORM_GAFR1z        EQU     (0x00000002)
;------------------------------------------------------------------------

    ENDIF

    END     ; EOF regGpio.inc

