#ifndef	__UTILS_H
#define	__UTILS_H

void delay(UINT32 ms);
void console_init(void);
int kbhit(void);
void putchar(char c);
void puts(char *buffer);
int getyorn(void);
int getchar(void);
#define	putch(c)		putchar(c)
#define	PutString(s)	puts(s)
#define	getch()			getchar()
void printf(char *fmt,...);

//#define	out_b(v, p)		(*(volatile unsigned char *)(p) = (v))
//#define	out_w(v, p)		(*(volatile unsigned short *)(p) = (v))
//#define	out_l(v, p)		(*(volatile unsigned int *)(p) = (v))
//#define	in_b(p)			*(volatile unsigned char *)(p)
//#define	in_w(p)			*(volatile unsigned short *)(p)
//#define	in_l(p)			*(volatile unsigned int *)(p)
void out_l(U32 v, U32 p);
U32 in_l(U32 p);

void show_led(int led, int status);

#endif