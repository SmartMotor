;/***********************************************************************
; * Copyright � 2001 Intel Corp.
;************************************************************************
;
;  Cotulla clocks manager register defines
;
;    This is an assembly language include file.
;
;***********************************************************************/

    IF :LNOT: :DEF: __regClock_s
__regClock_s   EQU     1

;------------------------------------------------------------------------

; register offsets from the base address
CCCR_OFFSET           EQU     (0x0)
CKEN_OFFSET           EQU     (0x04)
OSCC_OFFSET           EQU     (0x08)

;------------------------------------------------------------------------

; register bit masks - cccr
CCCR_L                EQU     (0x01F)
CCCR_M                EQU     (0x060)
CCCR_N                EQU     (0x380)

; field values for L, clock-to-memory multiplier
CCCR_L27              EQU     (0x01)
CCCR_L32              EQU     (0x02)
CCCR_L36              EQU     (0x03)
CCCR_L40              EQU     (0x04)
CCCR_L45              EQU     (0x05)

; field values for M, memory-to-run-mode  multiplier
CCCR_M1               EQU     (0x1 :SHL: 5)
CCCR_M2               EQU     (0x2 :SHL: 5)
CCCR_M4               EQU     (0x3 :SHL: 5)

; field values for N, run-mode-to-turbo-mode  multiplier
CCCR_N10              EQU     (0x2 :SHL: 7)
CCCR_N15              EQU     (0x3 :SHL: 7)
CCCR_N20              EQU     (0x4 :SHL: 7)
CCCR_N25              EQU     (0x5 :SHL: 7)
CCCR_N30              EQU     (0x6 :SHL: 7)

;------------------------------------------------------------------------

; register bit masks - cken
CKEN_CKEN0            EQU     BIT0
CKEN_CKEN1            EQU     BIT1
CKEN_CKEN2            EQU     BIT2
CKEN_CKEN3            EQU     BIT3
; bit 4 is reserved
CKEN_CKEN5            EQU     BIT5
CKEN_CKEN6            EQU     BIT6
CKEN_CKEN7            EQU     BIT7
CKEN_CKEN8            EQU     BIT8
; bit 9 is reserved
CKEN_CKEN10           EQU     BIT10
CKEN_CKEN11           EQU     BIT11
CKEN_CKEN12           EQU     BIT12
CKEN_CKEN13           EQU     BIT13
CKEN_CKEN14           EQU     BIT14
; bit 15 is reserved
CKEN_CKEN16           EQU     BIT16

CKEN_PWM0             EQU     CKEN_CKEN0
CKEN_PWM1             EQU     CKEN_CKEN1
CKEN_AC97             EQU     CKEN_CKEN2
CKEN_SSP              EQU     CKEN_CKEN3
CKEN_STUART           EQU     CKEN_CKEN5
CKEN_FFUART           EQU     CKEN_CKEN6
CKEN_BTUART           EQU     CKEN_CKEN7
CKEN_I2S              EQU     CKEN_CKEN8
CKEN_ADC              EQU     CKEN_CKEN10
CKEN_USB              EQU     CKEN_CKEN11
CKEN_MMC              EQU     CKEN_CKEN12
CKEN_ICP              EQU     CKEN_CKEN13
CKEN_I2C              EQU     CKEN_CKEN14
CKEN_LCD              EQU     CKEN_CKEN16

; reserved bits in CKEN must be written as 0,
; use this mask to insure that happens
CKEN_DEFINED          EQU     0x00017DEF

;------------------------------------------------------------------------

; register bit masks - oscc
OSCC_OOK              EQU     BIT0
OSCC_OON              EQU     BIT1

;------------------------------------------------------------------------

    ENDIF

    END     ; EOF regClock.inc

