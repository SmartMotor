/******************************************************************************
**
**  COPYRIGHT (C) 2000 Intel Corporation
**
**  FILENAME:      	xsffuart.h
**
**  PURPOSE:		This file defines the device context structure of FFUART		       
**
**  LAST MODIFIED: 12/20/2000
******************************************************************************/

#ifndef _xsffuart_h
#define _xsffuart_h

/*
************************************************************************************
*							CONSTANTS 
************************************************************************************
*/

#ifdef FFUART_GLOBALS
#define EXTRN
#else
#define EXTRN extern
#endif

/*
************************************************************************************
*							DATA TYPES                
************************************************************************************
*/


/*
************************************************************************************
*                            GLOBAL VARIABLES 
************************************************************************************
*/

EXTRN UartContextT FFUart;

/*
************************************************************************************
*                            FUNCTION PROTOTYPES 
************************************************************************************
*/

void XsFFUartSWInit (void);

#endif /* _xsffuart_h */










