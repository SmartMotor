;/***********************************************************************
; * Copyright � 2001 Intel Corp.
;************************************************************************
;
;  Cotulla power manager / reset control register defines
;
;    This is an assembly language include file.
;
;***********************************************************************/

    IF :LNOT: :DEF: __regPower_s
__regPower_s   EQU     1

;------------------------------------------------------------------------

; register offsets from the base address
PMCR_OFFSET           EQU     (0x0)
PSSR_OFFSET           EQU     (0x04)
PSPR_OFFSET           EQU     (0x08)
PWER_OFFSET           EQU     (0x0C)
PRER_OFFSET           EQU     (0x10)
PFER_OFFSET           EQU     (0x14)
PEDR_OFFSET           EQU     (0x18)
PCFR_OFFSET           EQU     (0x1C)
PGSRx_OFFSET          EQU     (0x20)
PGSRy_OFFSET          EQU     (0x24)
PGSRz_OFFSET          EQU     (0x28)

RCSR_OFFSET           EQU     (0x30)

;------------------------------------------------------------------------

; register bit masks - pmcr
PMCR_IDAE             EQU     (BIT0)

;------------------------------------------------------------------------

; register bit masks - pcfr
PCFR_OPDE             EQU     (BIT0)
PCFR_FP               EQU     (BIT1)
PCFR_FS               EQU     (BIT2)
PCFR_DS               EQU     (BIT3)

;------------------------------------------------------------------------

; register bit masks - pwer
PWER_WE0              EQU     (BIT0)
PWER_WE1              EQU     (BIT1)
PWER_WE2              EQU     (BIT2)
PWER_WE3              EQU     (BIT3)
PWER_WE4              EQU     (BIT4)
PWER_WE5              EQU     (BIT5)
PWER_WE6              EQU     (BIT6)
PWER_WE7              EQU     (BIT7)
PWER_WE8              EQU     (BIT8)
PWER_WE9              EQU     (BIT9)
PWER_WE10             EQU     (BIT10)
PWER_WE11             EQU     (BIT11)
PWER_WE12             EQU     (BIT12)
PWER_WE13             EQU     (BIT13)
PWER_WE14             EQU     (BIT14)
PWER_WE15             EQU     (BIT15)
PWER_WERTC            EQU     (BIT31)

;------------------------------------------------------------------------

; register bit masks - prer
PRER_RE0              EQU     (BIT0)
PRER_RE1              EQU     (BIT1)
PRER_RE2              EQU     (BIT2)
PRER_RE3              EQU     (BIT3)
PRER_RE4              EQU     (BIT4)
PRER_RE5              EQU     (BIT5)
PRER_RE6              EQU     (BIT6)
PRER_RE7              EQU     (BIT7)
PRER_RE8              EQU     (BIT8)
PRER_RE9              EQU     (BIT9)
PRER_RE10             EQU     (BIT10)
PRER_RE11             EQU     (BIT11)
PRER_RE12             EQU     (BIT12)
PRER_RE13             EQU     (BIT13)
PRER_RE14             EQU     (BIT14)
PRER_RE15             EQU     (BIT15)

;------------------------------------------------------------------------

; register bit masks - pfer
PFER_FE0              EQU     (BIT0)
PFER_FE1              EQU     (BIT1)
PFER_FE2              EQU     (BIT2)
PFER_FE3              EQU     (BIT3)
PFER_FE4              EQU     (BIT4)
PFER_FE5              EQU     (BIT5)
PFER_FE6              EQU     (BIT6)
PFER_FE7              EQU     (BIT7)
PFER_FE8              EQU     (BIT8)
PFER_FE9              EQU     (BIT9)
PFER_FE10             EQU     (BIT10)
PFER_FE11             EQU     (BIT11)
PFER_FE12             EQU     (BIT12)
PFER_FE13             EQU     (BIT13)
PFER_FE14             EQU     (BIT14)
PFER_FE15             EQU     (BIT15)

;------------------------------------------------------------------------

; register bit masks - pedr
PEDR_ED0              EQU     (BIT0)
PEDR_ED1              EQU     (BIT1)
PEDR_ED2              EQU     (BIT2)
PEDR_ED3              EQU     (BIT3)
PEDR_ED4              EQU     (BIT4)
PEDR_ED5              EQU     (BIT5)
PEDR_ED6              EQU     (BIT6)
PEDR_ED7              EQU     (BIT7)
PEDR_ED8              EQU     (BIT8)
PEDR_ED9              EQU     (BIT9)
PEDR_ED10             EQU     (BIT10)
PEDR_ED11             EQU     (BIT11)
PEDR_ED12             EQU     (BIT12)
PEDR_ED13             EQU     (BIT13)
PEDR_ED14             EQU     (BIT14)
PEDR_ED15             EQU     (BIT15)

;------------------------------------------------------------------------

; register bit masks - pssr
PSSR_SSS              EQU     (BIT0)
PSSR_BFS              EQU     (BIT1)
PSSR_VFS              EQU     (BIT2)
PSSR_PH               EQU     (BIT4)
PSSR_RDH              EQU     (BIT5)

;------------------------------------------------------------------------

; register bit masks - rcsr
RCSR_HWR              EQU     (BIT0)
RCSR_WDR              EQU     (BIT1)
RCSR_SMR              EQU     (BIT2)
RCSR_GPR              EQU     (BIT3)
RCSR_ALL              EQU     (0xF)

;------------------------------------------------------------------------

    ENDIF

    END     ; EOF regPower.inc

