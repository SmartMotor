/******************************************************************************
**
**  COPYRIGHT (C) 2000, 2001 Intel Corporation.
**
**  This software as well as the software described in it is furnished under 
**  license and may only be used or copied in accordance with the terms of the 
**  license. The information in this file is furnished for informational use 
**  only, is subject to change without notice, and should not be construed as 
**  a commitment by Intel Corporation. Intel Corporation assumes no 
**  responsibility or liability for any errors or inaccuracies that may appear 
**  in this document or any software that may be provided in association with 
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by 
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:       XsClkMgr.h
**
**  PURPOSE:        API for the clocks manager. The clocks manager is responsible 
**                  for enabling and disabling clocks to the peripheral devices.
**                  Note that not all of the registers are handled in this file.
**                  Specifically the OSCC, and CCCR registers are missing from 
**                  this file. The reason is that these registers are supposed to 
**                  be set using the assembly language startup for the system and
**                  really should not be tampered with afterwards. 
**
**  LAST MODIFIED:  $Modtime: 6/12/02 12:06p $
******************************************************************************/
#ifndef _ClkMgr_H_
#define _ClkMgr_H_

/*
*******************************************************************************
The clock enable register CKEN.
The naming of these bits conforms to the standard whereby the action 
is specified in the name when the bit is set. However, these bits will be 
used for both enabling and disabling the clocks. Therefore an alias is created 
for each of these bits. The alias is the next section.
*******************************************************************************
*/
#define PWM0_CE     (0x1u << 0)     // PWM #0 clock enable
#define PWM1_CE     (0x1u << 1)     // PWM #1 clock enable
#define AC97_CE     (0x1u << 2)     // AC97 
#define SSP_CE      (0x1u << 3)     // SSP
#define STUART_CE   (0x1u << 5)     // Standard UART
#define FFUART_CE   (0x1u << 6)     // Full Function UART
#define BTUART_CE   (0x1u << 7)     // Bluetooth UART
#define I2S_CE      (0x1u << 8)     // I2S
#define USB_CE      (0x1u << 11)    // USB
#define MMC_CE      (0x1u << 12)    // MMC
#define ICP_CE      (0x1u << 13)    // ICP
#define I2C_CE      (0x1u << 14)    // I2C
#define LCD_CE      (0x1u << 16)    // LCD 

/*
*******************************************************************************
Aliases for the device clock enables. The enable and disable procedure takes
a single UINT32 as a parameter. This UINT32 contains the bit settings for the 
devices that are being enabled or disabled.
*******************************************************************************
*/

typedef enum periphClkNamesS
{
    CK_PWM0   =  PWM0_CE,
    CK_PWM1   =  PWM1_CE,  
    CK_AC97   =  AC97_CE,  
    CK_SSP    =  SSP_CE,   
    CK_STUART =  STUART_CE,
    CK_FFUART =  FFUART_CE,
    CK_BTUART =  BTUART_CE,
    CK_I2S    =  I2S_CE,   
    CK_USB    =  USB_CE,   
    CK_MMC    =  MMC_CE,   
    CK_ICP    =  ICP_CE,   
    CK_I2C    =  I2C_CE,   
    CK_LCD    =  LCD_CE
} periphClkNamesT;   

/*
*******************************************************************************
Note: The following structure defines all of the Clock manager registers, 
but only the CKEN register is manipulated in this module. The other registers
should have been pre-programmed by the assembly language startup code.
*******************************************************************************
*/
#define CKEN_REGISTER_BASE      0x41300000

typedef struct clkMgrRegsS
{
    VUINT32 CCCR;       // Core clock configuration register
    VUINT32 CKEN;       // Clock Enable register
    VUINT32 OSCC;       // Oscillator Configuration register
}clkMgrRegsT;

/*
*******************************************************************************
function prototypes
*******************************************************************************
*/


#ifdef CKEN_GLOBALS
#define EXTRN
#else
#define EXTRN extern
#endif

EXTRN UINT32 xsCMEnableClock (periphClkNamesT);
EXTRN UINT32 xsCMDisableClock (periphClkNamesT);
EXTRN UINT32 xsCMDisableClockAll(void);

/*****************************************************************************/
#undef EXTRN
#endif //_ClkMgr_H_ 
