/******************************************************************************
**
**  COPYRIGHT (C) 2000, 2001 Intel Corporation.
**
**  This software as well as the software described in it is furnished under 
**  license and may only be used or copied in accordance with the terms of the 
**  license. The information in this file is furnished for informational use 
**  only, is subject to change without notice, and should not be construed as 
**  a commitment by Intel Corporation. Intel Corporation assumes no 
**  responsibility or liability for any errors or inaccuracies that may appear 
**  in this document or any software that may be provided in association with 
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by 
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:      XsDmaApi.h
**
**  PURPOSE:       Contains register defs and the external API for the 
**                 DMA controller.
**
**  LAST MODIFIED: $Modtime: 10/02/01 5:23p $
******************************************************************************/


#ifndef C_DMA_CONTROLLER_API
#define C_DMA_CONTROLLER_API

/*
*******************************************************************************
    Miscellaneous constants
*******************************************************************************
*/

#define XSDMA_CHANNEL_MAX        15
#define XSDMA_CHANNEL_NUM   (XSDMA_CHANNEL_MAX + 1)
#define XSDMA_CHANNEL_UNAVAILABLE    -1


#define XSDMA_MAX_BUF_SIZE  ((1u<<13)-1) // Max the DMA HW can process

/*
*******************************************************************************
Mask version of the DMA Control/Status register bits
*******************************************************************************
*/
        // The first four are also used as Interrupt Reasons in various
        //   contexts.
#define DCSR_BUSERRINTR     (0x1U << 0)  // Bus error status bit
#define DCSR_STARTINTR      (0x1U << 1)  // Descriptor fetch status 
#define DCSR_ENDINTR        (0x1U << 2)  // finish status
#define DCSR_STOPINTR       (0x1U << 3)  // stopped status

#define DCSR_REQPEND        (0x1U << 8)  // Request Pending (read-only)
#define DCSR_STOPIRQEN      (0x1U << 29) // Enable the stopped interrupt
#define DCSR_NOFETCH        (0x1U << 30) // Descriptor fetch mode, 0 = fetch
#define DCSR_RUN            (0x1U << 31) // run, 1=start

        // Mask of all writable bits in DCSR; others must be written as 0
#define DCSR_WRITABLES_MSK  (DCSR_BUSERRINTR |\
                             DCSR_STARTINTR  |\
                             DCSR_ENDINTR    |\
                             DCSR_STOPIRQEN  |\
                             DCSR_NOFETCH    |\
                             DCSR_RUN         )

        // Mask to clear all interrupt reasons in DCSR
#define DCSR_CLEAR_INTS_MSK (DCSR_BUSERRINTR |\
                             DCSR_STARTINTR  |\
                             DCSR_ENDINTR    )

/*
*******************************************************************************
 Mask version of the DMA interrupt register (read-only)
*******************************************************************************
*/

#define DINT_CH0            (0x1U << 0)  // channel 0 interrupt
#define DINT_CH1            (0x1U << 1)  // channel 1 interrupt
#define DINT_CH2            (0x1U << 2)  // channel 2 interrupt
#define DINT_CH3            (0x1U << 3)  // channel 3 interrupt
#define DINT_CH4            (0x1U << 4)  // channel 4 interrupt
#define DINT_CH5            (0x1U << 5)  // channel 5 interrupt
#define DINT_CH6            (0x1U << 6)  // channel 6 interrupt
#define DINT_CH7            (0x1U << 7)  // channel 7 interrupt
#define DINT_CH8            (0x1U << 8)  // channel 8 interrupt
#define DINT_CH9            (0x1U << 9)  // channel 9 interrupt
#define DINT_CH10           (0x1U << 10) // channel 10 interrupt
#define DINT_CH11           (0x1U << 11) // channel 11 interrupt
#define DINT_CH12           (0x1U << 12) // channel 12 interrupt
#define DINT_CH13           (0x1U << 13) // channel 13 interrupt
#define DINT_CH14           (0x1U << 14) // channel 14 interrupt
#define DINT_CH15           (0x1U << 15) // channel 15 interrupt

/*
*******************************************************************************
Mask version of the DMA request to channel map register
*******************************************************************************
*/

#define DRCMR_CHLNUM_SHIFT  0           // channel number register shift
#define DRCMR_CHLNUM_MSK    0x0F         // channel number mask within register
#define DRCMR_MAPVLD        (0x1U << 7)  // request mapped to valid channel


/*
*******************************************************************************
Mask version of the DMA descriptor address register
*******************************************************************************
*/

#define DDADR_STOP          (0x1U << 0)          // stop after processing
#define DDADR_ADDR_SHIFT    4                   // address shift 
#define DDADR_ADDR_MASK     (0xFFFFFFFU << DDADR_ADDR_SHIFT) // address of next

/*
*******************************************************************************
Mask version of the DMA source address register
*******************************************************************************
*/

#define DSADR_PERIPHERAL    (0x1U << 2)          // memory or peripheral, 0=mem
#define DSADR_ADDR_SHIFT    3                   // address shift
#define DSADR_ADDR_MASK     (0xFFFFFFF8U << DSADR_ADDR_SHIFT)  // Source address

/*
*******************************************************************************
Mask version of the DMA target address register
*******************************************************************************
*/

#define DTADR_PERIPHERAL    (0x1U << 2)          // memory or peripheral
#define DTADR_ADDR_SHIFT    3                   // address shift in register
#define DTADR_ADDR_MASK     (0xFFFFFFF8U << DTADR_ADDR_SHIFT)  // target address

/*
*******************************************************************************
Mask version of the DMA command register
*******************************************************************************
*/

#define DCMD_LEN_SHIFT      0               // length shift
#define DCMD_LEN_MASK       (0x1FFFU << 0)   // length of transfer in bytes

#define DCMD_WIDTH_SHIFT    14              // width shift in register
#define DCMD_WIDTH_MASK     (0x3U << DCMD_WIDTH_SHIFT)  // width of peripheral
#define DCMD_WIDTH_RSVD     (0x0U << DCMD_WIDTH_SHIFT)  // reserved value
#define DCMD_WIDTH_1        (0x1U << DCMD_WIDTH_SHIFT)  // 1 byte
#define DCMD_WIDTH_2        (0x2U << DCMD_WIDTH_SHIFT)  // 2 bytes
#define DCMD_WIDTH_4        (0x3U << DCMD_WIDTH_SHIFT)  // 4 bytes

#define DCMD_SIZE_SHIFT     16
#define DCMD_SIZE_MASK      (0x3U << DCMD_SIZE_SHIFT)   // burst size
#define DCMD_SIZE_RSVD      (0x0U << DCMD_SIZE_SHIFT)   // reserved
#define DCMD_SIZE_8         (0x1U << DCMD_SIZE_SHIFT)   // 8 bytes
#define DCMD_SIZE_16        (0x2U << DCMD_SIZE_SHIFT)   // 16 bytes
#define DCMD_SIZE_32        (0x3U << DCMD_SIZE_SHIFT)   // 32 bytes

#define DCMD_BIG_ENDIAN     (0x1U << 18)     // 0=little endian
#define DCMD_FLYBYT         (0x1U << 19)     // fly-by target
#define DCMD_FLYBYS         (0x1U << 20)     // fly-by source
#define DCMD_ENDIRQEN       (0x1U << 21)     // end interupt enable
#define DCMD_STARTIRQEN     (0x1U << 22)     // start interrupt enable
#define DCMD_FLOWTRG        (0x1U << 28)     // target flow control
#define DCMD_FLOWSRC        (0x1U << 29)     // source flow control
#define DCMD_INCTRGADDR     (0x1U << 30)     // increment target address
#define DCMD_INCSRCADDR     (0x1U << 31)     // increment source address



/*
*******************************************************************************
Note: DMA registers are defined in real memory, and in the case of this 
system, virtual memory is set to physical memory for the DMA registers
*******************************************************************************
*/

#define DMA_REGISTER_BASE   0x40000000
#define NUM_OF_PERIPHERALS  22          // number of DMA supported peripherals

#define XSDMA_DESC_SIGNATURE  0x44657363  // ASCII code for "Desc"
#define XSDMA_DMA_TIMEOUT     100           // Timeout on DMA run bit set


//#define PERIPH_TYPE     0x50657269  // ASCII code for "Peri"
//#define MEM_TYPE        0x4D656D6F  // ASCII code for "Memo"

typedef enum XsDmaDeviceTypeE
{
    XSDMA_DT_PERIPH_TYPE    = 0x50657269,  // ASCII code for "Peri"
    XSDMA_DT_MEM_TYPE       = 0x4D656D6F,  // ASCII code for "Memo"
    XSDMA_DT_CTRL_TYPE      = 0x4374726C,  // ASCII code for "Ctrl"    
    XSDMA_DT_BYTE_TYPE      = 0x42797465   // ASCII code for "Byte"
} XsDmaDeviceTypeT;


/*
*******************************************************************************
The following structure is used for the DMA specific data for each of the 
peripheral devices.
*******************************************************************************
*/

typedef struct XsDmatoDeviceS
{
    UINT32 name;
    UINT32 rec_addr;   
    UINT32 trans_addr;
    UINT32 data_width;
    UINT32 best_burst;
}XsDmaToDeviceT;


/*
*******************************************************************************
A structure to define a DMA descriptor
*******************************************************************************
*/

typedef struct XsDmaDescriptorElementsS * XsDmaDescriptorElementsTPT;


typedef struct XsDmaDescriptorElementsS 
{
    UINT32 nextPhysicalAddr;        // actual physical address of next descriptor
    UINT32 sourcePhysicalAddr;      // actual physical address of the source 
    UINT32 targetPhysicalAddr;      // actual physical address of the target
    UINT32 commandRegister;
    
    // elements used for software access
    XsDmaDescriptorElementsTPT nextVirtualAddr;
    PVOID  sourceVirtualAddr;
    PVOID  targetVirtualAddr;
    PVOID  branchedVirtualAddr;
        
    // The next element must be fixed at 32 bytes offset from the top of this 
    // structure. It contains the address of the descriptor that the system 
    // will branch to if a source/target compare bit is set. Not used in 
    // this version of the processor. It is here in preparation for the next 
    // version. 
    UINT32 branchedPhysicalAddr;

    //elements to help me manage the memory

    XsDmaDeviceTypeT sourceDeviceType;
    XsDmaDeviceTypeT targetDeviceType;
    UINT32 signature;    // this is an embedded signature for a descriptor 
    
    // additional elements required for client usage
    UINT32 currentPhysicalAddr; // the physical address of this descriptor
    XsDmaDescriptorElementsTPT prevDescrP;
    UINT32 spare1;
    UINT32 spare2;
    
}XsDmaDescriptorElementsT;



/*
*******************************************************************************
Defines for the device name used in XsDmatoDevice_T structure
*******************************************************************************
*/

typedef enum XsDmaDeviceNamesE
{
    XSDMA_DN_MEMORY,
    XSDMA_DN_I2S,
    XSDMA_DN_BTUART,
    XSDMA_DN_FFUART,
    XSDMA_DN_AC97_MIC,
    XSDMA_DN_AC97_MODEM,
    XSDMA_DN_AC97_AUDIO,
    XSDMA_DN_SSP,
    XSDMA_DN_ICP,
    XSDMA_DN_STUART,
    XSDMA_DN_MMC,
    XSDMA_DN_USB_EP1,        // EP for Endpoint
    XSDMA_DN_USB_EP2,
    XSDMA_DN_USB_EP3,
    XSDMA_DN_USB_EP4,
    XSDMA_DN_USB_EP6,
    XSDMA_DN_USB_EP7,
    XSDMA_DN_USB_EP8,
    XSDMA_DN_USB_EP9,
    XSDMA_DN_USB_EP11,
    XSDMA_DN_USB_EP12,
    XSDMA_DN_USB_EP13,
    XSDMA_DN_USB_EP14
} XsDmaDeviceNamesT;

/*
*******************************************************************************
    Constants for use in identifying deviceName as source or target
*******************************************************************************
*/

typedef enum XsDmaDeviceDirectionE
{
    XSDMA_DEV_DIRN_SOURCE,
    XSDMA_DEV_DIRN_TARGET
    
} XsDmaDeviceDirectionT;

/*
*******************************************************************************
define the usage verbs for DMA channel management
*******************************************************************************
*/

typedef enum ChannelOperationsE
{ 
    CHANNEL_OP_GET, 
    CHANNEL_OP_SET, 
    CHANNEL_OP_RESET 
} ChannelOperationsT;

/*
*******************************************************************************
define priority types for channel acquisition
*******************************************************************************
*/

typedef enum XsDmaChannelPriorityE
{
    XSDMA_CH_PR_HIGH,
    XSDMA_CH_PR_MEDIUM,
    XSDMA_CH_PR_LOW
} XsDmaChannelPriorityT;

/*
*******************************************************************************
define burst size for the DMA transfer
*******************************************************************************
*/

typedef enum XsDmaBurstSizeE
{
    XSDMA_BURST_SIZE_8  = 8,
    XSDMA_BURST_SIZE_16 = 16,
    XSDMA_BURST_SIZE_32 = 32
} XsDmaBurstSizeT;

/*
*******************************************************************************
status values for channel acquisition. 
*******************************************************************************
*/

typedef enum XsDmaChannelStatusE
{
    XSDMA_CS_GOT_EXPECTED,        // if a channel with the requested priority was returned
    XSDMA_CS_LOWER_THAN_EXPECTED,  // if a channel with a lower priority was returned
    XSDMA_CS_HIGHER_THAN_EXPECTED,  // if a channel with a higher priority was returned
    XSDMA_CS_NONE_AVAILABLE        // No channel available
} XsDmaChannelStatusT;

/*
*******************************************************************************
alignment masks
*******************************************************************************
*/

#define BYTE16      0xFFFFFFF0U
#define BIT16       0xFFFFFFFEU
#define BIT32       0xFFFFFFFCU
#define BIT64       0xFFFFFFF8U

/*
*******************************************************************************
Interrupt reasons - used for enable or disable
*******************************************************************************
*/

    // Interrupt if a bus error event occurs on the target channel.
    //  Bus errors will apparently always cause interrupts; there is no
    //  documented way to turn them on or off.  If a channel client does
    //  not  have an ISR, there will be no record of this error because
    //  the DMA ISR will clear it even if there is no client channel ISR.
// Uses DCSR_BUSERRINTR

    // Set DCSR[EndIntr] interrupt for the channel when DCMD[LENGTH] is 
    //  decreased to zero.  Bit 21 in the CMD word of the descriptor.
    // Note that this is different than channel stopped.
// Uses DCSR_ENDINTR

    // Descriptor successful load: Bit 22 in the CMD word of the descriptor.
    //  Valid only for No-Descriptor Fetch Mode.  (Not currently supported by
    //  DMA driver API)
// Uses DCSR_STARTINTR


    // Interrupt if channel is stopped or uninitialized.  DCSR bit 29.
    //  Will cause an initial interrupt if enabled before channel is running.
// Uses DCSR_STOPINTR


/*
*******************************************************************************
interrupt handler callback function type
*******************************************************************************
*/

typedef void (*XsDmaHandlerFnPT) (void *, UINT32);                                         


/*
*******************************************************************************
function prototypes
*******************************************************************************
*/

void   XsDmaSWInit (void);
UINT32 XsDmaHWSetup (void);
UINT32 XsDmaRegisterHandler (UINT, XsDmaHandlerFnPT , void *);
UINT32 XsDmaUnregisterHandler (UINT);
INT XsDmaGetFreeChannel (XsDmaChannelPriorityT , 
                          XsDmaDeviceNamesT , 
                          BOOL ,
                          XsDmaChannelStatusT *); 
UINT32 XsDmaFreeChannel(INT);
UINT32 XsDmaLoadFirstDescriptorAddr (INT, 
                                 XsDmaDescriptorElementsTPT);
UINT32 XsDmaStart (INT channel);
UINT32 XsDmaStop (UINT32);
UINT32 XsDmaWaitUntilStopped (UINT32);
UINT32 XsDmaDestroyChain (XsDmaDescriptorElementsTPT);
UINT32 XsDmaIntReasonEnable(UINT, UINT32);
UINT32 XsDmaIntReasonDisable(UINT, UINT32);


//-----------------------------------
typedef UINT32 (*XsDmaOperateOnDescriptorFnPT)(XsDmaDescriptorElementsTPT);

INT XsDmaGetRemainingLength (INT channel);
UINT32 XsDmaReadChannelStatus (INT channel);
UINT32 XsDmaRangeCheckChannel (INT);
void XsDmaCloseChain  (XsDmaDescriptorElementsTPT);
void XsDmaOpenChain   (XsDmaDescriptorElementsTPT);
UINT32 XsDmaMakeBackLink(XsDmaDescriptorElementsTPT);
UINT32 XsDmaOperateOnChain (XsDmaDescriptorElementsTPT,
                           XsDmaOperateOnDescriptorFnPT);
UINT32 XsDmaMakeBackLinksInChain (XsDmaDescriptorElementsTPT);
XsDmaDescriptorElementsTPT XsDmaCreateDescriptorChain (INT , INT, UINT);
XsDmaDescriptorElementsTPT XsDmaCreateBareDescChain(INT , INT, UINT);
XsDmaDescriptorElementsTPT XsDmaCreateDescriptorFromChan (INT, UINT);
UINT32 XsDmaConfigureDevice (XsDmaChannelPriorityT desiredPriority,
                            XsDmaDeviceNamesT deviceName, 
                            BOOL isTarget,
                            XsDmaDescriptorElementsTPT * firstDescVtP,
                            INT numDescriptors,
                            UINT xferByteCount,
                            XsDmaHandlerFnPT callbackFnP,
                            PVOID param,
                            UINT32 intReasons,
                            PINT returnChannelNumP);
UINT32 XsDmaConfigureMemToMem (XsDmaChannelPriorityT desiredPriority,
                            XsDmaDeviceNamesT deviceName, 
                            XsDmaDescriptorElementsTPT * firstDescVtP,
                            INT numDescriptors,
                            UINT xferByteCount,
                            XsDmaHandlerFnPT callbackFnP,
                            PVOID param,
                            UINT32 intReasons,
                            PINT returnChannelNumP); 
UINT32 XsDmaSetStartIntrReason   (XsDmaDescriptorElementsTPT);
UINT32 XsDmaClearStartIntrReason (XsDmaDescriptorElementsTPT);
UINT32 XsDmaSetEndIntrReason     (XsDmaDescriptorElementsTPT);
UINT32 XsDmaClearEndIntrReason   (XsDmaDescriptorElementsTPT);
UINT32 XsDmaDescrSetStopBit      (XsDmaDescriptorElementsTPT);
UINT32 XsDmaDescrClrStopBit      (XsDmaDescriptorElementsTPT);
VOID XsDmaSetBurstSize (XsDmaDeviceNamesT deviceName, XsDmaBurstSizeT burstSize);
UINT32 XsDmaPopulateChainWithBufs (XsDmaDescriptorElementsTPT);
UINT32 XsDmaGetPointerLastDataBuffer (INT channel);

//------------ Debug utilities ----------------

void XsDmaDumpDescriptor (XsDmaDescriptorElementsTPT, INT);
INT XsDmaDumpDescriptorList (XsDmaDescriptorElementsTPT rootDescP);
INT XsDmaGetNumBytes (XsDmaDescriptorElementsTPT thisDescP);
INT XsDmaDumpDataFromList (XsDmaDescriptorElementsTPT rootDescP, BOOL isTarget, INT localEnabler);

/********************************************************************************/

#endif // C_DMA_CONTROLLER_API
