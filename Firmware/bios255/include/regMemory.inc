;/***********************************************************************
; * Copyright � 2001 Intel Corp.
;************************************************************************
;
;  Cotulla memory controller register defines
;
;    This is an assembly language include file.
;
;***********************************************************************/

    IF :LNOT: :DEF: __regMemory_s
__regMemory_s   EQU     1

;------------------------------------------------------------------------

; register offsets from the base address
MDCNFG_OFFSET           EQU     (0x0)
MDREFR_OFFSET           EQU     (0x04)
MSC0_OFFSET             EQU     (0x08)
MSC1_OFFSET             EQU     (0x0C)
MSC2_OFFSET             EQU     (0x10)
MECR_OFFSET             EQU     (0x14)
SXLCR_OFFSET            EQU     (0x18)
SXCNFG_OFFSET           EQU     (0x1C)
FLYCNFG_OFFSET          EQU     (0x20)
SXMRS_OFFSET            EQU     (0x24)
MCMEM0_OFFSET           EQU     (0x28)
MCMEM1_OFFSET           EQU     (0x2C)
MCATT0_OFFSET           EQU     (0x30)
MCATT1_OFFSET           EQU     (0x34)
MCIO0_OFFSET            EQU     (0x38)
MCIO1_OFFSET            EQU     (0x3C)
MDMRS_OFFSET            EQU     (0x40)
BOOTDEF_OFFSET          EQU     (0x44)

; register bit masks - mdcnfg
MDCNFG_DE0              EQU     (BIT0)
MDCNFG_DE1              EQU     (BIT1)
MDCNFG_DWID0            EQU     (BIT2)
MDCNFG_DCAC0            EQU     (BIT3+BIT4)
MDCNFG_DRAC0            EQU     (BIT5+BIT6)
MDCNFG_DNB0             EQU     (BIT7)
MDCNFG_DTC0             EQU     (BIT8+BIT9)
MDCNFG_DADDR0           EQU     (BIT10)
MDCNFG_DLATCH0          EQU     (BIT11)
MDCNFG_DSA11110         EQU     (BIT12)
MDCNFG_RESERVED0        EQU     (BIT13+BIT14+BIT15)
MDCNFG_DE2              EQU     (BIT16)
MDCNFG_DE3              EQU     (BIT17)
MDCNFG_DWID2            EQU     (BIT18)
MDCNFG_DCAC2            EQU     (BIT19+BIT20)
MDCNFG_DRAC2            EQU     (BIT21+BIT22)
MDCNFG_DNB2             EQU     (BIT23)
MDCNFG_DTC2             EQU     (BIT24+BIT25)
MDCNFG_DADDR2           EQU     (BIT26)
MDCNFG_DLATCH2          EQU     (BIT27)
MDCNFG_RESERVED2        EQU     (BIT28+BIT29+BIT30+BIT31)

; register bit masks - mdrefr
MDREFR_DRI              EQU     (0xFFF)
MDREFR_E0PIN            EQU     (BIT12)
MDREFR_K0RUN            EQU     (BIT13)
MDREFR_K0DB2            EQU     (BIT14)
MDREFR_E1PIN            EQU     (BIT15)
MDREFR_K1RUN            EQU     (BIT16)
MDREFR_K1DB2            EQU     (BIT17)
MDREFR_K2RUN            EQU     (BIT18)
MDREFR_K2DB2            EQU     (BIT19)
MDREFR_APD              EQU     (BIT20)
MDREFR_SLFRSH           EQU     (BIT22)
MDREFR_K0FREE           EQU     (BIT23)
MDREFR_K1FREE           EQU     (BIT24)
MDREFR_K2FREE           EQU     (BIT25)

; register bit masks - sxcnfg
SXCNFG_SXEN0            EQU     (BIT0+BIT1)
SXCNFG_SXEN2            EQU     (BIT16+BIT17)

;------------------------------------------------------------------------

    ENDIF

    END     ; EOF regMemory.inc

