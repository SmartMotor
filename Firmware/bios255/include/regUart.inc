;/***********************************************************************
; * Copyright � 2001 Intel Corp.
;************************************************************************
;
;  Cotulla uart (ffuart, stuart, btuart) register defines
;
;    This is an assembly language include file.
;
;***********************************************************************/

    IF :LNOT: :DEF: __regUart_s
__regUart_s   EQU     1

;------------------------------------------------------------------------

; register offsets from the base address
RBR_OFFSET            EQU     (0x0)
THR_OFFSET            EQU     (0x0)
IER_OFFSET            EQU     (0x04)
IIR_OFFSET            EQU     (0x08)
FCR_OFFSET            EQU     (0x08)
LCR_OFFSET            EQU     (0x0C)
MCR_OFFSET            EQU     (0x10)
LSR_OFFSET            EQU     (0x14)
MSR_OFFSET            EQU     (0x18)
SPR_OFFSET            EQU     (0x1C)
DLL_OFFSET            EQU     (0x0)
DLH_OFFSET            EQU     (0x04)
ISR_OFFSET            EQU     (0x20)

; register bit masks - DLL
DLL_DLL               EQU     (0xFF)

; register bit masks - DLH
DLH_DLH               EQU     (0xFF)

; baud rate divisor values for DLL:
DLL_BAUD_4800         EQU     (192)
DLL_BAUD_9600         EQU     ( 96)
DLL_BAUD_19200        EQU     ( 48)
DLL_BAUD_38400        EQU     ( 24)
DLL_BAUD_57600        EQU     ( 16)
DLL_BAUD_115200       EQU     (  8)
DLL_BAUD_230400       EQU     (  4)
DLL_BAUD_307200       EQU     (  3)
DLL_BAUD_460800       EQU     (  2)
DLL_BAUD_921600       EQU     (  1)

; register bit masks - IER
IER_RAVIE             EQU     (BIT0)
IER_TIE               EQU     (BIT1)
IER_RLSE              EQU     (BIT2)
IER_MIE               EQU     (BIT3)
IER_RTOIE             EQU     (BIT4)
IER_NRZE              EQU     (BIT5)
IER_UUE               EQU     (BIT6)
IER_DMAE              EQU     (BIT7)

; register bit masks - IIR
IIR_IP                EQU     (BIT0)
IIR_IID               EQU     (BIT1+BIT2)
IIR_TOD               EQU     (BIT3)
IIR_IID_ALL           EQU     (IIR_IP+IIR_IID+IIR_TOD)
IIR_FIFOES            EQU     (BIT6+BIT7)

IIR_RXERROR           EQU     (0X6)  ; priority 1 (highest)
IIR_RXDATA            EQU     (0X4)  ; priority 2
IIR_TIMEOUT           EQU     (0X8)  ; priority 2
IIR_TXDATA            EQU     (0X2)  ; priority 3
IIR_MODEM             EQU     (0X0)  ; priority 4
IIR_NONE              EQU     (0X1)
IIR_INT_ALL           EQU     (0XF)

; register bit masks - FCR
FCR_TRFIFOE           EQU     (BIT0)
FCR_RESETRF           EQU     (BIT1)
FCR_RESETTF           EQU     (BIT2)
FCR_ITL               EQU     (BIT6+BIT7)

ITL_01                EQU     (0X0)  ; set fifo trigger level to  1    
ITL_08                EQU     (0X40) ;  "    "     "      "    "  8    
ITL_16                EQU     (0X80) ;  "    "     "      "    " 16    
ITL_32                EQU     (0XC0) ;  "    "     "      "    " 32    

; register bit masks - LCR
LCR_WLS               EQU     (BIT0+BIT1)
LCR_STB               EQU     (BIT2)
LCR_PEN               EQU     (BIT3)
LCR_EPS               EQU     (BIT4)
LCR_STKYP             EQU     (BIT5)
LCR_SB                EQU     (BIT6)
LCR_DLAB              EQU     (BIT7)

WLS_5BIT              EQU     (0X0)  ; set for 5 data bits
WLS_6BIT              EQU     (0X1)  ;  "   "  6  "    "
WLS_7BIT              EQU     (0X2)  ;  "   "  7  "    "
WLS_8BIT              EQU     (0X3)  ;  "   "  8  "    "

; register bit masks - LSR
LSR_DR                EQU     (BIT0)
LSR_OE                EQU     (BIT1)
LSR_PE                EQU     (BIT2)
LSR_FE                EQU     (BIT3)
LSR_BI                EQU     (BIT4)
LSR_TDRQ              EQU     (BIT5)
LSR_TEMT              EQU     (BIT6)
LSR_FIFOE             EQU     (BIT7)

; register bit masks - MCR
MCR_DTR               EQU     (BIT0)
MCR_RTS               EQU     (BIT1)
MCR_OUT1              EQU     (BIT2)
MCR_OUT2              EQU     (BIT3)
MCR_LOOP              EQU     (BIT4)

; register bit masks - MSR
MSR_DCTS              EQU     (BIT0)
MSR_DDSR              EQU     (BIT1)
MSR_TERI              EQU     (BIT2)
MSR_DDCD              EQU     (BIT3)
MSR_CTS               EQU     (BIT4)
MSR_DSR               EQU     (BIT5)
MSR_RI                EQU     (BIT6)
MSR_DCD               EQU     (BIT7)

;------------------------------------------------------------------------

    ENDIF

    END     ; EOF regUart.inc

