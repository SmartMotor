;/***********************************************************************
; * Copyright � 2001 Intel Corp.
;************************************************************************
;
;  Cotulla real-time clock register defines
;
;    This is an assembly language include file.
;
;***********************************************************************/

    IF :LNOT: :DEF: __regRTC_s
__regRTC_s   EQU     1

;------------------------------------------------------------------------

; register offsets from the base address
RCNR_OFFSET           EQU     (0x0)
RTAR_OFFSET           EQU     (0x4)
RTSR_OFFSET           EQU     (0x8)
RTTR_OFFSET           EQU     (0xC)

;------------------------------------------------------------------------

; register bit masks - rtsr
RTSR_AL               EQU     (BIT0)
RTSR_HZ               EQU     (BIT1)
RTSR_ALE              EQU     (BIT2)
RTSR_HZE              EQU     (BIT3)

;------------------------------------------------------------------------

; register bit masks - rttr
RTTR_CDC              EQU     (0x0000FFFF)
RTTR_TDC              EQU     (0x03FF0000)
RTTR_LCK              EQU     (BIT31)

;------------------------------------------------------------------------

    ENDIF

    END     ; EOF regRTC.inc

