/******************************************************************************
**
**  COPYRIGHT (C) 2000, 2001 Intel Corporation.
**
**  This software as well as the software described in it is furnished under 
**  license and may only be used or copied in accordance with the terms of the 
**  license. The information in this file is furnished for informational use 
**  only, is subject to change without notice, and should not be construed as 
**  a commitment by Intel Corporation. Intel Corporation assumes no 
**  responsibility or liability for any errors or inaccuracies that may appear 
**  in this document or any software that may be provided in association with 
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by 
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:       xsost.h
**
**  PURPOSE:        Operating System Timer header
**
**  LAST MODIFIED:  02/09/2001
******************************************************************************/
#ifndef _xsost_h_
#define _xsost_h_

#include "systypes.h"
//#include "XsOstDCS.h"

/*
*******************************************************************************
Crystal constants
*******************************************************************************
*/
#define TICKS_PER_10_MSECS  36864   // number of ticks in 10 milliseconds

/*
*******************************************************************************
OS Timer Watchdog Match Enable Register (OWER)
*******************************************************************************
*/

#define OWER_WME    (0x1u << 0)     // Watchdog match enable

/*
*******************************************************************************
OS Timer Status Register (OSSR)
*******************************************************************************
*/

#define OSSR_M0     (0x1u << 0)     // Match status channel 0
#define OSSR_M1     (0x1u << 1)     // Match status channel 1
#define OSSR_M2     (0x1u << 2)     // Match status channel 2
#define OSSR_M3     (0x1u << 3)     // Match status channel 3

/*
*******************************************************************************
OS Timer Interrupt Enable Register (OIER)
*******************************************************************************
*/

#define OIER_E0     (0x1u << 0)     // Interrupt Enable Channel 0
#define OIER_E1     (0x1u << 1)     // Interrupt Enable Channel 1
#define OIER_E2     (0x1u << 2)     // Interrupt Enable Channel 2
#define OIER_E3     (0x1u << 3)     // Interrupt Enable Channel 3

/*
*******************************************************************************
OS Timer Registers 
*******************************************************************************
*/

typedef struct OSTRegsS
{
    VUINT32 OSMR0;      // OS Timer Match Register 0
    VUINT32 OSMR1;      // OS Timer Match Register 1
    VUINT32 OSMR2;      // OS Timer Match Register 2
    VUINT32 OSMR3;      // OS Timer Match Register 3
    VUINT32 OSCR;       // OS Timer counter register
    VUINT32 OSSR;       // OS Timer status register
    VUINT32 OWER;       // OS Timer Watchdog enable register
    VUINT32 OIER;       // OS Timer interrupt enable register
}OSTRegsT;

#define OST_REGISTER_BASE   0x40A00000

/*
*******************************************************************************
*    Test Constants
*******************************************************************************
*/
#define TEST_INTERVAL 1
#define TEST_EXTRA_INTERVAL 10
#define TEST_EXTRA_COUNT 4
#define TEST_TIMEBASE 3686

#define XS_OST_MAX_CHANNELS 4
#define MAX_TIMER 16

//#include "XsOstApi.h"

#endif //_xsost_h_ 
