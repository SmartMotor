/******************************************************************************
**
**  COPYRIGHT (C) 2000, 2001 Intel Corporation.
**
**  This software as well as the software described in it is furnished under 
**  license and may only be used or copied in accordance with the terms of the 
**  license. The information in this file is furnished for informational use 
**  only, is subject to change without notice, and should not be construed as 
**  a commitment by Intel Corporation. Intel Corporation assumes no 
**  responsibility or liability for any errors or inaccuracies that may appear 
**  in this document or any software that may be provided in association with 
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by 
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:       XsGpio.h
**
**  PURPOSE:        Constants, structure and variable declarations for the 
**                  internal ("PriVaTe") usage of the main processor on-board 
**                  General Purpose Input/Output devices' driver
**                  - Includes initialization materials
**
**  Valid for    :  Cotulla processor
**
**  EAS VERSION  :  2.1
**
**  $Modtime: 5/30/01 3:48p $ 
******************************************************************************/
#ifndef _XSGPIO_H
#define _XSGPIO_H

#ifdef  _DEFINING_XSGPIO
#define EXTRN
#else
#define EXTRN   extern
#endif


// Error sub-location codes for ERR_L_XSGPIO location code

#define ERR_S_XSGPIO_SET_DIR        0x01 // XsGpioSetDirection
#define ERR_S_XSGPIO_EN_PIN_INT     0x02 // XsGpioEnablePinInt
#define ERR_S_XSGPIO_DI_PIN_INT     0x03 // XsGpioDisablePinInt
#define ERR_S_XSGPIO_REG_HANDLER    0x04 // XsGpioRegisterHandler
#define ERR_S_XSGPIO_UNR_HANDLER    0x05 // XsGpioUnRegisterHandler
#define ERR_S_XSGPIO_INT_HANDLER    0x06 // XsGpioInterruptHandler
#define ERR_S_XSGPIO_GET_LEVEL      0x07 // XsGpioGetLevel
#define ERR_S_XSGPIO_SET_LEVEL      0x08 // XsGpioSetLevel
#define ERR_S_XSGPIO_SET_ALT_FN     0x09 // XsGpioSetAlternateFunction

/*
*******************************************************************************
    GPIO Registers Structure Definition
*******************************************************************************
*/


    // Virtual memory address of GPIO register in Cotulla.
    //  System architecture has virtual addresses the same as the 
    //  physical addresses for I/O space.

#define XS_GPIO_REGISTER_BASE   0x40E00000

    // GPIO registers substructures are defined as arrays to simplify
    //  the address calculation task.

typedef struct XsGpioRegsS
{                       // Interrupt Controller Reg usage

    VUINT32 GPLRs [XS_GPIO_REGS_PER_FUNC] ; // GPLR_x, _y, _z: pin-level
    VUINT32 GPDRs [XS_GPIO_REGS_PER_FUNC] ; // GPDR_x, _y, _z: pin direction
    VUINT32 GPSRs [XS_GPIO_REGS_PER_FUNC] ; // GPSR_x, _y, _z: pin output set
    VUINT32 GPCRs [XS_GPIO_REGS_PER_FUNC] ; // GPCR_x, _y, _z: pin output clear
    VUINT32 GRERs [XS_GPIO_REGS_PER_FUNC] ; // GRER_x, _y, _z: rising-edge detect
    VUINT32 GFERs [XS_GPIO_REGS_PER_FUNC] ; // GFER_x, _y, _z: falling-edge detect
    VUINT32 GEDRs [XS_GPIO_REGS_PER_FUNC] ; // GEDR_x, _y, _z: edge detect status
                                // GAFR[0,1]_x, _y, _z: alternate function select
    VUINT32 GAFRs [XS_GPIO_REGS_PER_FUNC_GAFR] ;  

}  XsGpioRegsT ;


/*
*******************************************************************************
    Constants for alternate function initialization table
*******************************************************************************
*/


typedef enum XsGpioInitCategoryS 
{
    GPIO_INIT_ALT      = 1,  // Init this pin using alternate function parameters
    GPIO_INIT_GPIO,          // Use for GPIO
                             // Don't touch this pin, already in use.  Mark in
    GPIO_INIT_NO,            // usage table.  (Only known case: debugger.)
    GPIO_INIT_CHK_FF,        // Need to check switch setting for OK.
    GPIO_INIT_CHK_BT         // Need to check switch setting for OK.

} XsGpioInitCategoryT ;


/*
*******************************************************************************
    Structure typedefs - mostly for individual entries in tables.
*******************************************************************************
*/

// Define one entry in the default GPIO settings table 
//  Note: Default configuration tables are defined in XsGpioPlatform.h

typedef struct XsGpioDefaultEntryS 
{
    XsGpioInitCategoryT defaultCategory;     // GPIO, Alt Fn, don't touch
    XsGpioAltFuncCodeT  altFunctionSelector; // Alt Fn mode
    XsGpioDirT          direction;           // 
    XsGpioPinLevelT     initialLevel;        // 
                                             // Does the device on this pin
    BOOL                simulatedLevelInt;   //  need simultated level ints?

} XsGpioDefaultEntryT ;


// Define one entry in the current GPIO usage table.
//    Includes callback (registered handler) info.


typedef struct XsGpioUsageEntryS 
{
    XsGpioAltFuncCodeT    fnCategory;       // GPIO, Alt Fn[1,2]S, don't touch
    XsGpioDirT            direction;        // GPIO mode
    XsGpioPinLevelT       initialLevel;     // GPIO mode, output direction.

        // The following members are used only for GPIO mode input pins
                                            // Pointer to the client's handler
    XsGpioHandlerFnPT     registeredHandlerFnP; 
    void                 *registeredParamP; // Pass back to registered handler
    XsGpioIntTriggerEdgeT triggerCondition; // Rising edge, falling, both, none
                                            // Enabled in GxERs: 
    BOOL                  interruptEnabled; 
                                            // TRUE gives this device simulated
    BOOL                  simulatedLevelInt;//  level (not edge) ints.  See ISR

} XsGpioUsageEntryT ;


/*
*******************************************************************************
    GPIO.c scope variable definitions
*******************************************************************************
*/

EXTRN XsGpioRegsT *XsGpioRegsP;


/*
*******************************************************************************
  Table to record current usage of each GPIO pin.
  - Also includes interrupt configuration information for pins that are 
    configured for GPIO (rather that alternate function) and for the input
    direction.
*******************************************************************************
*/

EXTRN XsGpioUsageEntryT XsGpioCurrentUsageTable [XS_GPIO_PIN_COUNT];




/*
*******************************************************************************
    Private function prototypes for Cotulla on-board GPIO handler
*******************************************************************************
*/

void XsGpioInterruptHandler (void *);

// End Private function prototypes for Cotulla on-board GPIO handler

#undef EXTRN

#endif // #ifndef _XSGPIO_H
