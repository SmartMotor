;/***********************************************************************
; * Copyright � 2001 Intel Corp.
;************************************************************************
;
;  Cotulla Operating System Timer (OST) register defines
;
;    This is an assembly language include file.
;
;***********************************************************************/

    IF :LNOT: :DEF: __regOst_s
__regOst_s   EQU     1

;------------------------------------------------------------------------

; register offsets from the base address
OSMR0_OFFSET          EQU     (0x0)
OSMR1_OFFSET          EQU     (0x4)
OSMR2_OFFSET          EQU     (0x8)
OSMR3_OFFSET          EQU     (0xC)
OSCR_OFFSET           EQU     (0x10)
OSSR_OFFSET           EQU     (0x14)
OWER_OFFSET           EQU     (0x18)
OIER_OFFSET           EQU     (0x1C)

; register bit definitions
OSSR_M0               EQU     (BIT0)
OSSR_M1               EQU     (BIT1)
OSSR_M2               EQU     (BIT2)
OSSR_M3               EQU     (BIT3)
OSSR_ALL              EQU     (0xF)

OWER_WME              EQU     (BIT0)

OIER_E0               EQU     (BIT0)
OIER_E1               EQU     (BIT1)
OIER_E2               EQU     (BIT2)
OIER_E3               EQU     (BIT3)
OIER_ALL              EQU     (0xF)

;------------------------------------------------------------------------

    ENDIF

    END     ; EOF regOst.inc

