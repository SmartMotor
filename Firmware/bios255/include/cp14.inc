;/***********************************************************************
; * Copyright � 2001 Intel Corp.
;************************************************************************
;
;  Cotulla coprocessor 14 defines
;
;***********************************************************************/

    IF :LNOT: :DEF: __cp14_s
__cp14_s   EQU     1

;------------------------------------------------------------------------

; defines for the CCLKCFG register
CCLKCFG_TURBO         EQU     BIT0
CCLKCFG_FCS           EQU     BIT1
CCLKCFG_DEFINED       EQU     0x3   

;------------------------------------------------------------------------

; defines for the PWRMODE register
PWRMODE_RUN           EQU     0x0
PWRMODE_IDLE          EQU     0x1
PWRMODE_SLEEP         EQU     0x3
PWRMODE_DEFINED       EQU     0x3   

;------------------------------------------------------------------------

    ENDIF

    END  ; EOF cp14.inc
