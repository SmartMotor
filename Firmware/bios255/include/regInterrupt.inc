;/***********************************************************************
; * Copyright � 2001 Intel Corp.
;************************************************************************
;
;  Cotulla interrupt controller register defines
;
;    This is an assembly language include file.
;
;***********************************************************************/

    IF :LNOT: :DEF: __regInterrupt_s
__regInterrupt_s   EQU     1

;------------------------------------------------------------------------

; register offsets from the base address
ICIP_OFFSET             EQU     0x0
ICMR_OFFSET             EQU     0x04
ICLR_OFFSET             EQU     0x08
ICFP_OFFSET             EQU     0x0C
ICPR_OFFSET             EQU     0x10
ICCR_OFFSET             EQU     0x14

; register bit masks - valid in all interrupt controller registers
INTERRUPT_RESERVED      EQU     0xFF    ; bits 0-7 reserved
INTERRUPT_GPIO0         EQU     BIT8
INTERRUPT_GPIO1         EQU     BIT9
INTERRUPT_GPIO          EQU     BIT10
INTERRUPT_USB           EQU     BIT11
INTERRUPT_PMU           EQU     BIT12
INTERRUPT_I2S           EQU     BIT13
INTERRUPT_AC97          EQU     BIT14
INTERRUPT_ADC           EQU     BIT15
INTERRUPT_TOUCH         EQU     BIT16
INTERRUPT_LCD           EQU     BIT17
INTERRUPT_I2C           EQU     BIT18
INTERRUPT_ICP           EQU     BIT19
INTERRUPT_STUART        EQU     BIT20
INTERRUPT_BTUART        EQU     BIT21
INTERRUPT_FFUART        EQU     BIT22
INTERRUPT_MMC           EQU     BIT23
INTERRUPT_SSP           EQU     BIT24
INTERRUPT_DMA           EQU     BIT25
INTERRUPT_OST0          EQU     BIT26
INTERRUPT_OST1          EQU     BIT27
INTERRUPT_OST2          EQU     BIT28
INTERRUPT_OST3          EQU     BIT29
INTERRUPT_TIC           EQU     BIT30
INTERRUPT_RTC           EQU     BIT31
INTERRUPT_ALL           EQU     0xFFFFFF00

;------------------------------------------------------------------------

    ENDIF

    END     ; EOF regInterrupt.inc

