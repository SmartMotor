/******************************************************************************
**
**  COPYRIGHT (C) 2000, 2001 Intel Corporation.
**
**  This software as well as the software described in it is furnished under 
**  license and may only be used or copied in accordance with the terms of the 
**  license. The information in this file is furnished for informational use 
**  only, is subject to change without notice, and should not be construed as 
**  a commitment by Intel Corporation. Intel Corporation assumes no 
**  responsibility or liability for any errors or inaccuracies that may appear 
**  in this document or any software that may be provided in association with 
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by 
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:      xsdma.h
**
**  PURPOSE:       Contains module private declarations and definitions for 
**                 the DMA controller.
**
**  LAST MODIFIED: $Modtime: 10/01/01 3:31p $
******************************************************************************/


#ifndef C_DMA_CONTROLLER
#define C_DMA_CONTROLLER
/*
*******************************************************************************
    Error sub-location codes for ERR_L_XSDMA location code
*******************************************************************************
*/

#define ERR_S_DMA_REG_HANDLER    0x01 // XsDmaRegisterHandler
#define ERR_S_DMA_UNR_HANDLER    0x02 // XsDmaUnregisterHandler
#define ERR_S_DMA_INT_HANDLER    0x03 // XsDmaInterruptHandler
#define ERR_S_DMA_HWSETUP        0x04 // XsDmaHWSetup

/*
*******************************************************************************
Structure for channels and the relative priority. 
*******************************************************************************
*/
typedef struct XsDmaChannelPrioritySearchEntryS
{
    UINT channel;
    XsDmaChannelPriorityT priority;
}XsDmaChannelPrioritySearchEntryT;

/*
*******************************************************************************
This is a structure of DMA register groups that will make accesses more easily
through simple program control instead of long-winded switch constructs.
*******************************************************************************
*/

typedef struct XsDmaDescriptorGroupS {
    VUINT   DDADR;  // descriptor address reg
    VUINT   DSADR;  // source address register
    VUINT   DTADR;  // target address register
    VUINT   DCMD;   // command address register
}XsDmaDescriptorGroupT;

/*
*******************************************************************************
    XsDmaDrcmrIdT for indexing the DRCMR registers.
*******************************************************************************
*/

typedef enum XsDmaDrcmrIdE
{
    XSDMA_DRCMR_ID_DREQ_0         = 0, // companion chip request 0
    XSDMA_DRCMR_ID_DREQ_1         = 1, // companion chip request 1
    XSDMA_DRCMR_ID_I2S_RX         = 2, // I2S receive request
    XSDMA_DRCMR_ID_I2S_TX         = 3, // I2S transmit request
    XSDMA_DRCMR_ID_BTUART_RX      = 4, // BTUART receive request
    XSDMA_DRCMR_ID_BTUART_TX      = 5, // BTUART transmit request
    XSDMA_DRCMR_ID_FFUART_RX      = 6, // FFUART receive request
    XSDMA_DRCMR_ID_FFUART_TX      = 7, // FFUART transmit request
    XSDMA_DRCMR_ID_AC97_MIC_RX    = 8, // AC97 microphone request
    XSDMA_DRCMR_ID_AC97_MODEM_RX  = 9, // AC97 MODEM receive request
    XSDMA_DRCMR_ID_AC97_MODEM_TX  = 10, // AC97 MODEM transmit request
    XSDMA_DRCMR_ID_AC97_AUDIO_RX  = 11, // AC97 Audio receive request
    XSDMA_DRCMR_ID_AC97_AUDIO_TX  = 12, // AC97 Audio transmit request
    XSDMA_DRCMR_ID_SSP_RX         = 13, // SSP receive request
    XSDMA_DRCMR_ID_SSP_TX         = 14, // SSP transmit request
    XSDMA_DRCMR_ID_RSVD_15        = 15, // reserved
    XSDMA_DRCMR_ID_RSVD_16        = 16, // reserved
    XSDMA_DRCMR_ID_FICP_RX        = 17, // ICP receive request
    XSDMA_DRCMR_ID_FICP_TX        = 18, // ICP transmit request
    XSDMA_DRCMR_ID_STUART_RX      = 19, // STUART receive request
    XSDMA_DRCMR_ID_STUART_TX      = 20, // STUART transmit request
    XSDMA_DRCMR_ID_MMC_RX         = 21, // MMC receive request
    XSDMA_DRCMR_ID_MMC_TX         = 22, // MMC transmit request
    XSDMA_DRCMR_ID_RSVD_23        = 23, // reserved
    XSDMA_DRCMR_ID_RSVD_24        = 24, // reserved
    XSDMA_DRCMR_ID_USB_CLNT_EP_1  = 25, // USB endpoint 1 request
    XSDMA_DRCMR_ID_USB_CLNT_EP_2  = 26, // USB endpoint 2 request
    XSDMA_DRCMR_ID_USB_CLNT_EP_3  = 27, // USB endpoint 3 request
    XSDMA_DRCMR_ID_USB_CLNT_EP_4  = 28, // USB endpoint 4 request
    XSDMA_DRCMR_ID_RSVD_29        = 29, // reserved
    XSDMA_DRCMR_ID_USB_CLNT_EP_6  = 30, // USB endpoint 6 request
    XSDMA_DRCMR_ID_USB_CLNT_EP_7  = 31, // USB endpoint 7 request
    XSDMA_DRCMR_ID_USB_CLNT_EP_8  = 32, // USB endpoint 8 request
    XSDMA_DRCMR_ID_USB_CLNT_EP_9  = 33, // USB endpoint 9 request
    XSDMA_DRCMR_ID_RSVD_34        = 34, // reserved
    XSDMA_DRCMR_ID_USB_CLNT_EP_11 = 35, // USB endpoint 11 request
    XSDMA_DRCMR_ID_USB_CLNT_EP_12 = 36, // USB endpoint 12 request
    XSDMA_DRCMR_ID_USB_CLNT_EP_13 = 37, // USB endpoint 13 request
    XSDMA_DRCMR_ID_USB_CLNT_EP_14 = 38, // USB endpoint 14 request
    XSDMA_DRCMR_ID_RSVD_39        = 39, // reserved
    XSDMA_DRCMR_ID_MAX            = XSDMA_DRCMR_ID_RSVD_39,
    XSDMA_DRCMR_ID_NUM            = (XSDMA_DRCMR_ID_MAX+1),
    XSDMA_DRCMR_ID_ILLEGAL        = XSDMA_DRCMR_ID_NUM,
    // Memory doesn't have a DRCMR.  Define this for users of records
    //  where XsDmaDrcmrIdT values are stored.
    XSDMA_DRCMR_ID_MEMORY         = (XSDMA_DRCMR_ID_NUM+1)

} XsDmaDrcmrIdT;

/*
*******************************************************************************
Mask version of the DMA control register set structure
*******************************************************************************
*/

typedef struct XsDmaCtrlS {
    VUINT32   DCSR[XSDMA_CHANNEL_NUM]; //DMA CSRs by channel

    UINT      PAD0[44];  // unused addresses
    VUINT32   DINT;        // DMA interrupt register
    UINT      PAD1[3];   // unused addresses

                           // Indexed by the XsDmaDrcmrIdT enums.
    VUINT32   DRCMR[XSDMA_DRCMR_ID_NUM];
    UINT      PAD2[24];  
    XsDmaDescriptorGroupT DDG[XSDMA_CHANNEL_NUM]; // DMA descriptor group array

} XsDmaCtrlT;

/*
*******************************************************************************
This structure is used to pass a bucket of parameters around when creating 
DMA descriptors.  Descriptors can be for memory to memory transfers,
peripheral to memory, or mem to peripheral.
*******************************************************************************
*/

typedef struct XsDmaDescriptorParamsS
{
    // Information to create and determine the properties of the descriptor
    XsDmaDescriptorElementsTPT thisDescP;
    INT channel;
    INT transferLength;
    BOOL setStopBit;

    // Contents of the descriptor to be built
    UINT32 nextPhysicalAddr;
    UINT32 sourcePhysicalAddr;
    UINT32 targetPhysicalAddr;
    UINT32 commandRegister;
    XsDmaDescriptorElementsTPT nextVirtualAddr;
    PVOID  sourceVirtualAddr;
    PVOID  targetVirtualAddr;
    UINT32 sourceDeviceType;
    UINT32 targetDeviceType;
    UINT32 currentPhysicalAddr; // the physical address of this descriptor
    XsDmaDescriptorElementsTPT prevDescrP;

} XsDmaDescriptorParamsT;

/*
*******************************************************************************
    Constants and structure for main processor DMA Controller's channel 
    configuration table.  Includes members for the registered client interrupt 
    handlers (callbacks).
*******************************************************************************
*/

typedef struct XsDmaChannelConfigEntryS
{
    BOOL        isAssigned;            // Channel has been assigned
    XsDmaDeviceNamesT
                sourceDeviceName;
    XsDmaDeviceNamesT
                targetDeviceName;
    XsDmaDrcmrIdT
                drcmrId;
    XsDmaHandlerFnPT                   // Pointer to the client's registered 
                registeredHandlerFnP;  //  handler for this channel.
    void*       registeredParamP;      // Pass back to registered handler

} XsDmaChannelConfigEntryT;


static
XsDmaChannelConfigEntryT XsDmaChannelConfigTable [XSDMA_CHANNEL_NUM];

/*
*******************************************************************************
    Function prototypes
*******************************************************************************
*/

static void XsDmaInitAll(void);
static INT  XsDmaModifyChannelUsage(ChannelOperationsT, 
                                      INT32, 
                                      XsDmaChannelPriorityT, 
                                      XsDmaChannelStatusT*);
static void XsDmaSetupMem2MemDescriptor (XsDmaDescriptorParamsT* );
static void XsDmaSetupMem2PeriphDescriptor(XsDmaDescriptorParamsT* );
static void XsDmaSetupPeriph2MemDescriptor(XsDmaDescriptorParamsT* );
static void XsDmaInterruptHandler (void *);
static XsDmaDrcmrIdT XsDmaMapDevice2DrcmrId (XsDmaDeviceNamesT, BOOL);
static void XsDmaFillDescriptorStructure (XsDmaDescriptorParamsT*);
static UINT32 XsDmaDestroyLink (XsDmaDescriptorElementsTPT);
static UINT32 XsDmaPopulateDescWithBufs (XsDmaDescriptorElementsTPT );


/********************************************************************************/

#endif // C_DMA_CONTROLLER
