/******************************************************************************
**
**  COPYRIGHT (C) 2000, 2001 Intel Corporation.
**
**  This software as well as the software described in it is furnished under 
**  license and may only be used or copied in accordance with the terms of the 
**  license. The information in this file is furnished for informational use 
**  only, is subject to change without notice, and should not be construed as 
**  a commitment by Intel Corporation. Intel Corporation assumes no 
**  responsibility or liability for any errors or inaccuracies that may appear 
**  in this document or any software that may be provided in association with 
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by 
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:       XsOstDCS.h
**
**  PURPOSE:        <Module description here>
**
**  LAST MODIFIED:  $Modtime: 8/23/01 11:48a $
******************************************************************************/

#ifndef _xsostdcs_h
#define _xsostdcs_h

/*
************************************************************************************
* CONSTANTS 
************************************************************************************
*/

#ifdef OST_GLOBALS
#define EXTRN
#else
#define EXTRN extern
#endif


/*
************************************************************************************
* DATA TYPES 
************************************************************************************
*/
typedef VOID (*OSTCallBackFnPT)(PVOID); 
typedef struct XsOstCallbackFnCtxS
{
	OSTCallBackFnPT	XsOstCallback;
	void * CallbackParam;	
}XsOstCallbackFnCtxT;
typedef UINT32 (*DM_TimerSetup_T) (OSTCallBackFnPT, PVOID);
typedef unsigned int (*DM_TimerCleanup_T) (void);


typedef struct OSTContextS {
    void * regsP;
	int testError;
	DM_TimerSetup_T setup_fnp;
	DM_TimerCleanup_T cleanup_fnp;
    DM_TimerStart_T start_fnp;
    DM_TimerStop_T stop_fnp;
    DM_TimerWait_T wait_fnp;
    DM_TimerGetInterval_T getInterval_fnp;
    DM_TimerGetTimebase_T getTimebase_fnp;
    DM_TimerGetTimer_T getTimer_fnp;
    DM_TimerGetDelta_T getDelta_fnp;

} OSTContextT;

/*
************************************************************************************
* GLOBAL VARIABLES 
************************************************************************************
*/

EXTRN OSTContextT Ost;

#endif /* _xsostdcs_h */

