/******************************************************************************
**
**  COPYRIGHT (C) 2000 Intel Corporation
**
**  FILENAME:      	xsuart.h
**
**  PURPOSE:		This file containes the UART's register definitions		       
**
**  LAST MODIFIED: 12/20/2000
******************************************************************************/

#ifndef _xsuart_h
#define _xsuart_h

/*
************************************************************************************
*                             CONSTANTS 
************************************************************************************
*/


/*
************************************************************************************
*                            DATA TYPES 
************************************************************************************
*/

// UART registers
// This type is used by FFUART, BTUART and STUART

typedef struct UartRegsS {
	VUINT	UDATA;	// Receive Buffer Reg. (RBR), Transmit Holding Reg. (THR) and DLL
	VUINT	IER;	// Interrupt Enable Reg. (IER) and Baud Divisor Higher Byte Reg. (DLH)
	VUINT	FCR;	// Interrupt ID Reg. (read only) and FIFO Control Reg. (write only)
	VUINT	LCR;	// Line Control Reg. 
	VUINT	MCR;	// Modem Control Reg.
	VUINT	LSR;	// Line Status Reg.
	VUINT	MSR;	// Modem Status Reg.
	VUINT	SPR;	// Scratch Pad Reg.
	VUINT	ISR;	// Slow Infrared Select Reg.	
} UartRegsT;

// Masks for IER Reg.
// Note: 
//      - user needs to program the GPIO reg. before enabling the unit
//	    - NRZ is used in UART mode only, not for infrared mode
//		- user needs to make sure that DMA and TIE and RAVIE are not set to 1 at the same time	

#define	IER_RAVIE	(0x1 << 0)
#define	IER_TIE		(0x1 << 1)
#define	IER_RLSE	(0x1 << 2)
#define	IER_MIE		(0x1 << 3)
#define	IER_RTOIE	(0x1 << 4)
#define	IER_NRZE	(0x1 << 5)
#define	IER_UUE		(0x1 << 6)
#define	IER_DMAE	(0x1 << 7)

// Masks for Interrupt Identification Reg. (IIR)
// Note: IIR (read only) is located at the same address as the FCR (write only)

#define IIR_IP  		(0x1 << 0)
#define IIR_RLS     	(0x3 << 1)
#define IIR_RDA			(0x1 << 2)
#define IIR_TOD			(0x3 << 2)
#define IIR_TFIFO		(0x1 << 1)
#define IID_MODS		(0x0 << 3)
#define IID_FIFOES		(0x3 << 6)
  
// Masks for FIFO Control Register

#define FCR_TRFIFOD		(0x0 << 0)
#define FCR_TRFIFOE		(0x1 << 0)
#define FCR_RESETRF		(0x1 << 1)
#define FCR_RESETTF		(0x1 << 2)
#define FCR_ITL1		(0x0 << 7)
#define FCR_ITL8		(0x1 << 6)
#define FCR_ITL16		(0x1 << 7)
#define FCR_ITL32		(0x3 << 6)
#define FCR_ENABLED     (FCR_TRFIFOE | FCR_RESETRF | FCR_RESETTF)

// Masks for Line Control Reg. (LCR)

#define LCR_WLS5		(0x0 << 0)
#define LCR_WLS6		(0x1 << 0)
#define LCR_WLS7		(0x1 << 1)
#define LCR_WLS8		(0x3 << 0)
#define LCR_STB 		(0x1 << 2)
#define LCR_PEN 		(0x1 << 3)
#define LCR_EPSO 		(0x1 << 4)
#define LCR_EPSE 		(0x0 << 4)
#define LCR_STKYP		(0x1 << 5)
#define LCR_SB  		(0x1 << 6)
#define LCR_DLAB1 		(0x1 << 7)

// Masks for Line Status Reg. (LSR)

#define LSR_DR	 		(0x1 << 0)
#define LSR_OE	 		(0x1 << 1)
#define LSR_PE	 		(0x1 << 2)
#define LSR_FE	 		(0x1 << 3)
#define LSR_BI	 		(0x1 << 4)
#define LSR_TDRQ 		(0x1 << 5)
#define LSR_TEMT 		(0x1 << 6)
#define LSR_FIFOE	 	(0x1 << 7)

// Masks for Modem Control Reg.

#define MCR_DTR	 		(0x1 << 0)
#define MCR_RTS	 		(0x1 << 1)
#define MCR_OUT1 		(0x1 << 2)
#define MCR_OUT2 		(0x1 << 3)
#define MCR_LOOP 		(0x1 << 4)

// Masks for Modem Status Reg. (MSR)

#define MSR_DCTS 		(0x1 << 0)
#define MSR_DDSR 		(0x1 << 1)
#define MSR_PE	 		(0x1 << 2)
#define MSR_FE	 		(0x1 << 3)
#define MSR_BI	 		(0x1 << 4)
#define MSR_TDRQ 		(0x1 << 5)
#define MSR_TEMT 		(0x1 << 6)
#define MSR_FIFOE	 	(0x1 << 7)

// Masks for Infrared Selection Reg. (ISR)

#define ISR_XMITIR 		(0x1 << 0)
#define ISR_RCVEIR 		(0x1 << 1)
#define ISR_XMODE 		(0x1 << 2)
#define ISR_TXPL 		(0x1 << 3)
#define ISR_RXPL 		(0x1 << 4)

// Minimum values of divisors programmed in divisor latch reg.

#define FFDIVISOR_MIN	4				// FFUART divisor
#define BTDIVISOR_MIN	1				// BTUART divisor
#define STDIVISOR_MIN	4				// STUART divisor

#define RETRY			10

#define	NUM_BUF_DEFAULT		2	
#define	BUFF_SIZE_DEFAULT	64	
#define	XFER_LEN_DEFAULT	128

#define FFUARTREG_BASE	(0x40100000)	// FFUART Base Register Location
#define BTUARTREG_BASE	(0x40200000)	// BTUART Base Register Location
#define STUARTREG_BASE	(0x40700000)	// STUART Base Register Location

typedef struct bufInfo_S {
	char * buf;
	void * param;
	int len;
} bufInfo_T;


typedef enum UartLoopbackMode_E
{
    UartLoopbackOff = 0,
    UartLoopbackOn = 1
} UartLoopbackMode_T;

typedef enum UartType_E
{
	FFUartType = 0,
	BTUartType,
	STUartType,
	SystemUart = 0xff00
} UartType_T;

// UART configuration structure
// This structure is used by the hardware setup function 
// to confiqure UART
/*
maskFIFO    is used to choose FIFO or non-FIFO mode and
            set interrupt trigger level in FIFO mode,
            this mask will be applied to FCR register

maskInt     is used to enable or disable DMA, NRZ and IRQs,
            this mask will be applied to IER register

maskSerial  is used to congiqure serial data format,
            this mask will be applied to LCR register

maskIRDA    is used to confiqure slow infrared operation,
			this mask will be aplied to ISR register.
			(it will be used only with STUART)

loopback    is used to set test mode
            0 for normal UART operation
            1 for test mode

rate        is used to confiqure the baud rate generator.
            Value of baud rate.

*/

typedef struct UartCfgS {
	UINT maskFIFO;				
	UINT maskInt;				
	UINT maskSerial;
	UINT maskIRDA;			
	UINT loopback;				
	UINT rate;					
} UartCfgT;

// DMA configuration structure
// Used to configure DMA to service UARTs
typedef struct UartDmaCfgS {
	UINT descNum;			// Number of the descriptors in the chain
	UINT bufferSize;		// buffer size for each of the descriptors, bytes
	UINT xferLength;		// Total length of the transfer, bytes
	UINT sourceName;		// Name of the source (see xsdma.h)
	UINT targetName;		// Name of the target (see xsdma.h)
	UINT priority;			// Channel's priority
} UartDmaCfgT;

// DMA Interrupt status structure
typedef struct UartDmaStatusS {
	INT busErrorIntCount;
	INT endIntCount;
	INT startIntCount;
	INT	stopIntCount;
} UartDmaStatusT;

// Rx or Tx type of the DMA config. structure
typedef enum DmaCfgType_E {
	DmaTx,
	DmaRx
} DmaCfgTypeT;


// UART interface
typedef void (*UartDmaIntHandlerT) (void *, UINT32);                                         
typedef UINT32 (*UartSetupT) (void * ctxP);
typedef INT (*UartCleanupT) (void * ctxP);
typedef INT (*UartLoopbackT) (void * ctxP, INT data);
typedef INT (*UartWriteT) (void * ctxP, CHAR *buf, INT len);
typedef INT (*UartReadT) (void * ctxP, CHAR *buf, INT len);
typedef INT (*UartRxRdyT) (void * ctxP);
typedef INT (*UartClearRxT) (void * ctxP);
typedef INT (*UartClearTxT) (void * ctxP);

// UART Context Structure
typedef struct UartContextS {
	UartRegsT * regsP;				    	// Pointer to UART's register base
	UartCfgT * uartCfgP;					// Pointer to UART's configuration structure
	UartDmaCfgT * dmaTxCfgP;				// Pointer to DMA Tx configuration structure
	UartDmaCfgT * dmaRxCfgP;				// Pointer to DMA Rx configuration structure
	UartDmaStatusT * dmaTxIntStatusP;		// Pointer to DMA Tx Interrupt status structure
	UartDmaStatusT * dmaRxIntStatusP;		// Pointer to DMA Rx Interrupt status structure
	INT loggedError;						// Used to report tests and drivers errors
	INT dmaRxChannel;						// DMA channel used to transfer data from UART to memory 
	INT dmaTxChannel;						// DMA channel used to transfer data from memory to UART 
	INT xferTxComplete;						// Used by the dma interrupt handler to inform the test about Tx xfer completion
	INT xferRxComplete;						// Used by the dma interrupt handler to inform the test about Rx xfer completion
	UINT type;								// Used to mark uart as a system device	and pass the Uart ID
	UartSetupT setupUartFnP;				// Pointer to UART's hardware setup function 
  	UartCleanupT shutdownUartFnP;			// Pointer to UART's cleanup function
  	UartLoopbackT loopbackUartFnP;			// Pointer to UART's loopback function	 
  	UartWriteT writeUartFnP;				// Pointer to UART's write function
  	UartReadT readUartFnP;					// Pointer to UART's read function
  	UartRxRdyT RxRdyUartFnp;
  	UartClearRxT clearRxUartFnP;			// Pointer to UART's clear Rx. FIFO function
	UartDmaIntHandlerT intHandlerDmaTxFnP;	// Pointer to UART's DMA Tx int. handler
	UartDmaIntHandlerT intHandlerDmaRxFnP;	// Pointer to UART's DMA Rx int. handler
} UartContextT;

#endif /* _xsuart_h */
