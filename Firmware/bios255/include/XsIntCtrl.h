/******************************************************************************
**
**  COPYRIGHT (C) 2000, 2001 Intel Corporation.
**
**  This software as well as the software described in it is furnished under 
**  license and may only be used or copied in accordance with the terms of the 
**  license. The information in this file is furnished for informational use 
**  only, is subject to change without notice, and should not be construed as 
**  a commitment by Intel Corporation. Intel Corporation assumes no 
**  responsibility or liability for any errors or inaccuracies that may appear 
**  in this document or any software that may be provided in association with 
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by 
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:       XsIntCtrl.h
**
**  PURPOSE:        Constants, structure and variable declarations for the 
**                  internal ("PriVaTe") usage of the Cotulla on-board 
**                  first level Interrupt Controller
**
**  Valid for    :  Cotulla processor
**
**  EAS VERSION  :  2.1
**
**  $Modtime: 4/06/01 3:23p $ 
******************************************************************************/
#ifndef _XSINTCTRL_H
#define _XSINTCTRL_H

// Note: assumes the previous inclusion of XsIntCtrl.h


// Error sub-location codes for ERR_L_XSIC location code

#define ERR_S_XSIC_IH_C_IRQ    0x01 // xsIcInterruptHandlerCIrq
#define ERR_S_XSIC_IIV_FIQ     0x02 // xsIcInstallInterruptVectors for FIQ
#define ERR_S_XSIC_IIV_IRQ     0x03 // xsIcInstallInterruptVectors for IRQ
#define ERR_S_XSIC_REG         0x04 // XsIcRegisterHandler
#define ERR_S_XSIC_UNREG       0x05 // XsIcUnRegisterHandler
#define ERR_S_XSIC_HWSETUP     0x06 // XsIcHWSetup
#define ERR_S_XSIC_EN_IRQ_DEV  0x07 // XsIcEnableIrqDeviceInt
#define ERR_S_XSIC_DIS_IRQ_DEV 0x08 // XsIcDisableIrqDeviceInt

// BEGIN Interrupt Controller Register bit mask definitions

#define XSIC_GPIO_0_MSK         ( 1 << XSIC_GPIO_0_SGNL       )       
#define XSIC_GPIO_1_MSK         ( 1 << XSIC_GPIO_1_SGNL       )       
#define XSIC_GPIO_2_OR_80_MSK   ( 1 << XSIC_GPIO_2_OR_80_SGNL )
#define XSIC_USB_MSK            ( 1 << XSIC_USB_SGNL          )       
#define XSIC_PMU_MSK            ( 1 << XSIC_PMU_SGNL          )       
#define XSIC_I2S_MSK            ( 1 << XSIC_I2S_SGNL          )       
    // Don't bother defining masks for reserved bits
#define XSIC_AC97_MSK           ( 1 << XSIC_AC97_SGNL         )       
#define XSIC_LCD_MSK            ( 1 << XSIC_LCD_SGNL          )       
#define XSIC_I2C_MSK            ( 1 << XSIC_I2C_SGNL          )       
#define XSIC_ICP_MSK            ( 1 << XSIC_ICP_SGNL          )       
#define XSIC_STUART_MSK         ( 1 << XSIC_STUART_SGNL       )       
#define XSIC_BTUART_MSK         ( 1 << XSIC_BTUART_SGNL       )       
#define XSIC_FFUART_MSK         ( 1 << XSIC_FFUART_SGNL       )       
#define XSIC_MMC_MSK            ( 1 << XSIC_MMC_SGNL          )       
#define XSIC_SSP_MSK            ( 1 << XSIC_SSP_SGNL          )       
#define XSIC_DMA_MSK            ( 1 << XSIC_DMA_SGNL          )       
#define XSIC_OST_REG0_MSK       ( 1 << XSIC_OST_REG0_SGNL     )       
#define XSIC_OST_REG1_MSK       ( 1 << XSIC_OST_REG1_SGNL     )       
#define XSIC_OST_REG2_MSK       ( 1 << XSIC_OST_REG2_SGNL     )       
#define XSIC_OST_REG3_MSK       ( 1 << XSIC_OST_REG3_SGNL     )       
#define XSIC_1HZ_CLKTCK_MSK     ( 1 << XSIC_1HZ_CLKTCK_SGNL   )       
#define XSIC_RTC_ALRM_MSK       ( 1 << XSIC_RTC_ALRM_SGNL     )       

// END Interrupt Controller Register bit mask definitions


// Exception vector addresses needed by interrupt controller code 
#define RESET_EXCEPTION 		        (0x00)
#define UNDEFINED_INSTRUCTION_EXCEPTION (RESET_EXCEPTION +0x04)
#define SWI_EXCEPTION_EXCEPTION 	    (UNDEFINED_INSTRUCTION_EXCEPTION +0x04)
#define PREFETCH_ABORT_EXCEPTION 	    (SWI_EXCEPTION_EXCEPTION +0x04)
#define DATA_ABORT_EXCEPTION 		    (PREFETCH_ABORT_EXCEPTION +0x04)
#define UNUSED_VECTOR 			        (DATA_ABORT_EXCEPTION +0x04)
#define IRQ_EXCEPTION 			        (UNUSED_VECTOR +0x04)
#define FIQ_EXCEPTION 			        (IRQ_EXCEPTION +0x04)


// BEGIN Interrupt Controller Control Register (ICCR) definitions
 
    // Note only one non-reserved bit in this register.
#define XSIC_ICCR_DIM_MASK          ( 1 << 0 ) 

    // Only enabled and unmasked interrupts will bring Cotulla out of IDLE Mode
#define XSIC_ICCR_DIM_UNMASKED_ONLY ( 1 << 0 ) 
                                            
    // All enabled interrupts will bring Cotulla out of IDLE Mode
#define XSIC_ICCR_DIM_ALL_ENABLED   ( 0 << 0 ) 

// END Interrupt Controller Control Register (ICCR) definitions


    // Virtual memory address of first interrupt controller register.  
    //  System architecture has virtual addresses the same as the 
    //  physical addresses for I/O space.
#define XS_INTERRUPT_CONTROLLER_REG_BASE    0x40D00000

typedef struct XsIntCtrlRegsS
{                       // Interrupt Controller Reg usage

    VUINT32 ICIP ;      // IRQ pending register 
    VUINT32 ICMR ;      // Mask register  (set (1) bit enables interrupt)
    VUINT32 ICLR ;      // Level register (select 0 = IRQ or 1 = FIQ)
    VUINT32 ICFP ;      // FIQ Pending register
    VUINT32 ICPR ;      // Pending register (all, whether or not enabled)
    VUINT32 ICCR ;      // Control register (wakeup usage)

}  XsIntCtrlRegsT ;


//  Manifest constants for returns to XsIcFiqWrapper and XsIcIrqWrapper
#define XSIC_ALL_HANDLED        0
#define XSIC_SOME_UNHANDLED     1


/*
*******************************************************************************
    Variable declarations and definitions
*******************************************************************************
*/

// From XsIntCtrla.s
extern UINT32 XsIcFiqChain ;
extern UINT32 XsIcIrqChain ;

//  Internal to interrupt controller driver

//  Define and initialize the pointer to the IC register area, using overlay 
//      structure type
static XsIntCtrlRegsT *XsIntCtrlRegsP = 
                        (XsIntCtrlRegsT *) XS_INTERRUPT_CONTROLLER_REG_BASE;

//  Registsered IRQ interrupt handlers table
static XsIcL1IntHandlerFnPT XsIcIrqHandlerTable[XSIC_MAX_SGNL+1];

//  Received parameters to pass to registered IRQ interrupt handlers
static void *XsIcIrqHandlerParamTable[XSIC_MAX_SGNL+1];


/*
*******************************************************************************
    Function prototypes
*******************************************************************************
*/

extern void XsIcFiqWrapper (void);              // From XsIntCtrla.s
extern void XsIcIrqWrapper (void);              // From XsIntCtrla.s

//static UINT32 XsIcInstallInterruptVectors (void);
UINT32 XsIcInterruptHandlerCIrq (void);
//static UINT32 XsIcRangeCheckSource (XsIcInterruptSignalsT);


#endif // #ifndef _XSINTCTRL_H
