/******************************************************************************
**
**  COPYRIGHT (C) 2000, 2001 Intel Corporation.
**
**  This software as well as the software described in it is furnished under 
**  license and may only be used or copied in accordance with the terms of the 
**  license. The information in this file is furnished for informational use 
**  only, is subject to change without notice, and should not be construed as 
**  a commitment by Intel Corporation. Intel Corporation assumes no 
**  responsibility or liability for any errors or inaccuracies that may appear 
**  in this document or any software that may be provided in association with 
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by 
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:       XsGpioApi.h
**
**  PURPOSE:        Constants, structure and variable declarations for the 
**                  public interface of the main processor's on-board 
**                  General Purpose Input/Output devices' driver
**
**  Valid for:      Cotulla processor
**                  Sandgate-based platform
**
**  EAS VERSION  :  2.1
**
**  $Modtime: 6/29/01 5:07p $
******************************************************************************/
#ifndef _XSGPIOAPI_H
#define _XSGPIOAPI_H

#ifdef  _DEFINING_XSGPIOAPI
#define EXTRN
#else
#define EXTRN   extern
#endif


/*
*******************************************************************************
    GPIO pin identifier definitions
*******************************************************************************
*/

typedef enum XsGpioIdE 
{
    XS_GPIO_ID_00     = 0,
    XS_GPIO_ID_01,
    XS_GPIO_ID_02,
    XS_GPIO_ID_03,
    XS_GPIO_ID_04,
    XS_GPIO_ID_05,
    XS_GPIO_ID_06,
    XS_GPIO_ID_07,
    XS_GPIO_ID_08,
    XS_GPIO_ID_09,
    XS_GPIO_ID_10,
    XS_GPIO_ID_11,
    XS_GPIO_ID_12,
    XS_GPIO_ID_13,
    XS_GPIO_ID_14,
    XS_GPIO_ID_15,
    XS_GPIO_ID_16,
    XS_GPIO_ID_17,
    XS_GPIO_ID_18,
    XS_GPIO_ID_19,
    XS_GPIO_ID_20,
    XS_GPIO_ID_21,
    XS_GPIO_ID_22,
    XS_GPIO_ID_23,
    XS_GPIO_ID_24,
    XS_GPIO_ID_25,
    XS_GPIO_ID_26,
    XS_GPIO_ID_27,
    XS_GPIO_ID_28,
    XS_GPIO_ID_29,
    XS_GPIO_ID_30,
    XS_GPIO_ID_31,
    XS_GPIO_ID_32,
    XS_GPIO_ID_33,
    XS_GPIO_ID_34,
    XS_GPIO_ID_35,
    XS_GPIO_ID_36,
    XS_GPIO_ID_37,
    XS_GPIO_ID_38,
    XS_GPIO_ID_39,
    XS_GPIO_ID_40,
    XS_GPIO_ID_41,
    XS_GPIO_ID_42,
    XS_GPIO_ID_43,
    XS_GPIO_ID_44,
    XS_GPIO_ID_45,
    XS_GPIO_ID_46,
    XS_GPIO_ID_47,
    XS_GPIO_ID_48,
    XS_GPIO_ID_49,
    XS_GPIO_ID_50,
    XS_GPIO_ID_51,
    XS_GPIO_ID_52,
    XS_GPIO_ID_53,
    XS_GPIO_ID_54,
    XS_GPIO_ID_55,
    XS_GPIO_ID_56,
    XS_GPIO_ID_57,
    XS_GPIO_ID_58,
    XS_GPIO_ID_59,
    XS_GPIO_ID_60,
    XS_GPIO_ID_61,
    XS_GPIO_ID_62,
    XS_GPIO_ID_63,
    XS_GPIO_ID_64,
    XS_GPIO_ID_65,
    XS_GPIO_ID_66,
    XS_GPIO_ID_67,
    XS_GPIO_ID_68,
    XS_GPIO_ID_69,
    XS_GPIO_ID_70,
    XS_GPIO_ID_71,
    XS_GPIO_ID_72,
    XS_GPIO_ID_73,
    XS_GPIO_ID_74,
    XS_GPIO_ID_75,
    XS_GPIO_ID_76,
    XS_GPIO_ID_77,
    XS_GPIO_ID_78,
    XS_GPIO_ID_79,
    XS_GPIO_ID_80,
    XS_GPIO_ID_MAX    = 80

} XsGpioIdT ; // end enum XsGpioIdE

// End GPIO pin identifier definitions


// Some aliases

#define XS_GPIO_ID_ICPSTUART_RX     XS_GPIO_ID_46
#define XS_GPIO_ID_ICPSTUART_TX     XS_GPIO_ID_47
#define XS_GPIO_ID_SSP_EXTCLK       XS_GPIO_ID_27

/*
*******************************************************************************
    GPIO general and addressing constants
*******************************************************************************
*/

#define XS_GPIO_PIN_COUNT   84

#define XS_GPIO_BITS_PER_REG            32


// Addressing for most GPIO registers

#define XS_GPIO_PINS_PER_REG            XS_GPIO_BITS_PER_REG // One pin per bit

    // Use this mask to determine the bit position in a GPIO register that 
    //  corresponds to a given pin ID.  True for most GPIO registers.
#define XS_GPIO_PIN_POS_IN_REG_MSK      (XS_GPIO_PINS_PER_REG-1)

    // GPIO control or status functions, not "alternate functions"
    //  Add (modulus - 1) to numerator to round up
#define XS_GPIO_REGS_PER_FUNC           ((XS_GPIO_PIN_COUNT + XS_GPIO_BITS_PER_REG-1)\
                                         / XS_GPIO_BITS_PER_REG)


// Special constants for Alternate Function selection registers, because
//  two bits are used for each pin.

#define XS_GPIO_BITS_PER_PIN_GAFR       2
#define XS_GPIO_PINS_PER_REG_GAFR       (XS_GPIO_BITS_PER_REG \
                                         / XS_GPIO_BITS_PER_PIN_GAFR)

    // Use this mask to help determine the base bit position for a
    //  given pin ID in the GAFR registers.  Result must be multiplied
    //  by XS_GPIO_BITS_PER_PIN_GAFR (=2) to obtain base.
#define XS_GPIO_PIN_POS_IN_REG_MSK_GAFR (XS_GPIO_PINS_PER_REG_GAFR-1)

    // GPIO control or status functions, not "alternate functions"
    //  Add (modulus - 1) to numerator to round up
#define XS_GPIO_REGS_PER_FUNC_GAFR      ((XS_GPIO_PIN_COUNT + XS_GPIO_PINS_PER_REG_GAFR-1)\
                                         / XS_GPIO_PINS_PER_REG_GAFR)

#define XS_GPIO_GAFR_PIN_MSK            3 // Mask for pin setting in GAFR

// End GPIO general and addressing constants


/*
*******************************************************************************
    GPIO Pin Direction Definitions
        Note, only applies to pins selected as GPIO, not alternate function
*******************************************************************************
*/

typedef enum XsGpioDirE 
{
    XS_GPIO_DIR_MSK  =  1u,  // For construction of ANDing masks
    XS_GPIO_DIR_IN   =  0u,  // Zero bit level sets pin as input
    XS_GPIO_DIR_OUT  =  1u   // One bit level sets pin as output

} XsGpioDirT ; // end enum XsGpioDirE

// End GPIO Pin Direction Definitions

/*
*******************************************************************************
    GPIO Pin Level Definitions
        For use in specifying the level of an output pin.
*******************************************************************************
*/

typedef enum XsGpioPinLevelE 
{
    XS_GPIO_PIN_LEVEL_MSK   =  1u,  // For construction of ANDing masks
    XS_GPIO_PIN_LEVEL_0     =  0u,  // Set output level of pin to 0
    XS_GPIO_PIN_LEVEL_1     =  1u   // Set output level of pin to 1

} XsGpioPinLevelT ; // end enum XsGpioPinLevelE

// End GPIO Pin Level Definitions


/*
*******************************************************************************
    GPIO Edge Type Definitions for Edge Triggered Interrupts
        Applies only to GPI pins set to the input direction
*******************************************************************************
*/

typedef enum XsGpioIntTriggerEdgeE 
{
    XS_GPIO_INT_EDGE_MSK      =  1u,  // For construction of ANDing masks
    XS_GPIO_INT_NONE          =  0u,  // Do not interrupt on this pin.
    XS_GPIO_INT_RISING_EDGE   =  1u,  // Interrupt on rising edge only
    XS_GPIO_INT_FALLING_EDGE  =  2u,  // Interrupt on falling edge only
    XS_GPIO_INT_BOTH_EDGES    =  3u   // Interrupt on rising edge or falling edge

} XsGpioIntTriggerEdgeT ; // end enum XsGpioIntTriggerEdgelE

// End GPIO Edge Type Definitions for Edge Triggered Interrupts


/*
*******************************************************************************
    GPIO Alternate Function selection codes
*******************************************************************************
*/

typedef enum XsGpioAltFuncCodeE 
{
    XS_GPIO_ALT_FUNC_MSK    =  3u,  // For construction of ANDing masks
    XS_GPIO_ALT_FUNC_GPIO   =  0u,  // GPIO usage, no alternate function
    XS_GPIO_ALT_FUNC_1      =  1u,  // Alternate function 1 
    XS_GPIO_ALT_FUNC_2      =  2u,  // Alternate function 2 
    XS_GPIO_UNAVAILABLE     =  4u   // Internal use only.  Pin used by debugger
//  XS_GPIO_ALT_FUNC_3      =  3u   // Alternate function 3 (None in this processor)

} XsGpioAltFuncCodeT ; // end enum XsGpioAltFuncCodeE

// End GPIO Alternate Function selection codes


/*
*******************************************************************************
    Function pointer types
*******************************************************************************
*/

typedef void (*XsGpioHandlerFnPT) (void *, UINT32);


/*
*******************************************************************************
    Context structure definitions
*******************************************************************************
*/

typedef struct XsGpioContextS
{
    UINT32 loggedError;
    UINT32 isrError;

}  XsGpioContextT ;



EXTRN XsGpioContextT  XsGpioContext;


/*
*******************************************************************************
    Function prototypes in standard driver API of main processor on-board GPIO handler
*******************************************************************************
*/

extern void     XsGpioSWInit (void);
extern UINT32   XsGpioHWSetup (void);



 
//  (Only way to specify the type of edge trigger for an interrupt)
//  (Although setting the edge trigger type in the hardware registers enables interrupts on a pin, 
//    this function only records the specified edge trigger(s).  Activating the hardware is 
//    performed by the XsGpioEnablePinInt() function.)
//  (During run time, clearing the interrupt on the pin will be done by the GPIO driver before
//    calling the registered handler.  When the handler is invoked, it will be given the
//    pin level (0 or 1) as a parameter.)
extern UINT32 XsGpioRegisterHandler (XsGpioIdT, 
                                     XsGpioIntTriggerEdgeT,
                                     XsGpioHandlerFnPT, 
                                     void *);


//         (also disables interrupt and clears any existing edge type specification to XS_GPIO_INT_NONE)
extern UINT32 XsGpioUnRegisterHandler (XsGpioIdT);

// (Accepted only for pins that are GPIO, have had their direction set as input and a handler registered.
// Using the existing specified edge type for triggering the interrupt
extern UINT32 XsGpioEnablePinInt (XsGpioIdT);

// Does not change the edge type that triggers the interrupt
extern UINT32 XsGpioDisablePinInt (XsGpioIdT);

// Rarely needed.  Usually, change setting in init tables in XsGpioPlatform.h
extern void   XsGpioSetSimulatedLevelInt (XsGpioIdT, BOOL);


// End Fn prototypes in standard driver API of main processor on-board GPIO handler

/*
*******************************************************************************
    Function prototypes in non-standard, test-oriented API of main processor on-board 
        GPIO driver.  Lower level access provided.
*******************************************************************************
*/

extern UINT32 XsGpioSetAlternateFunction ( XsGpioIdT pinID,
                                    XsGpioAltFuncCodeT alternateFunction);
//    (This is a register-oriented primitive and does not trigger
//      a general initialization of all pins needed to support
//      the alternate function selected for this pin)

extern UINT32 XsGpioSetDirection (XsGpioIdT pinID, XsGpioDirT direction, XsGpioPinLevelT initialLevel);
//    (Initial level applies only to output pins and will be set before direction is activated, as specified in the EAS)
//    (Accepted only for GPIO pins (no alternate functions.  For alternate function pins, the pin must be set to
//        GPIO, have the direction set, then set the alternate function.))

extern UINT32 XsGpioGetLevel (XsGpioIdT pinID, XsGpioPinLevelT *level);

extern UINT32 XsGpioSetLevel (XsGpioIdT pinID, XsGpioPinLevelT level);
// For general purpose use, performed with IRQ interrupt protection.  Output GPIO pins only.

extern UINT32 XsGpioSetLevelIsr (XsGpioIdT pinID, XsGpioPinLevelT level);
// For speed, assumes interrupt protection.  Intended for use in ISRs.  Output GPIO pins only.


// Set the pins needed by the STUART or ICP to that alternate function.  These functions are expected 
//  to be usedto toggle them betweeen STUART and ICP, and it is assumed that they will never be used 
//  for GPIO in the systems where this function is used.  Also, the only two pins affected in this
//  are Rx and Tx data, so initial levels are not important.  The ICP and STUART devices should both 
//  be disabled when either of these calls are made.

void   XsGpioSetIcp     (void);
void   XsGpioSetStuart  (void);

// Enable the GPIO27 as needed by the SSP EXT CLOCK input
void XsGpioSetSspExtClock (void);
void XsGpioClearSspExtClock (void);

/*
*******************************************************************************
    General GPIO utility function prototypes
*******************************************************************************
*/

// The following two functions are used for navigating in XsGpioRegsT
extern void XsGpioRegsCalcLocIndices (XsGpioIdT , 
                                        PINT , 
                                        PINT );
extern void XsGpioRegsCalcLocIndicesGafr (XsGpioIdT, PINT, PINT);

extern UINT32 XsGpioRangeCheckPinId (XsGpioIdT);

// End Fn prototypes in nonstandard driver API of main processor on-board GPIO handler

#undef EXTRN

#endif // #ifndef _XSGPIOAPI_H
