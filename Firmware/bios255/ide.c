#include "systypes.h"
#include "utils.h"

#define	IDE_DISK_SUPPORT

#ifdef	IDE_DISK_SUPPORT

#define	Delay	delay

#define	inportw(p)		*(U16 *)(p)
#define	outportw(d, p)	(*(U16 *)(p)=(d))

#ifdef	__S3C44B0X__

#define	IDE_DAT		0x4000020
#define	IDE_ERR		0x4000022
#define	IDE_SEC_CNT	0x4000024
#define	IDE_LBA0	0x4000026
#define	IDE_LBA1	0x4000028
#define	IDE_LBA2	0x400002a
#define	IDE_LBA_SEL	0x400002c
#define	IDE_CMD		0x400002e

#elif defined	__AT91RM9200__

#define	IDE_DAT		0x50000000
#define	IDE_ERR		0x50000002
#define	IDE_SEC_CNT	0x50000004
#define	IDE_LBA0	0x50000006
#define	IDE_LBA1	0x50000008
#define	IDE_LBA2	0x5000000a
#define	IDE_LBA_SEL	0x5000000c
#define	IDE_CMD		0x5000000e

#else

#define	IDE_DAT		0x04e00000
#define	IDE_ERR		0x04e00002
#define	IDE_SEC_CNT	0x04e00004
#define	IDE_LBA0	0x04e00006
#define	IDE_LBA1	0x04e00008
#define	IDE_LBA2	0x04e0000a
#define	IDE_LBA_SEL	0x04e0000c
#define	IDE_CMD		0x04e0000e

#endif

#define	IDE_FEATURE	IDE_ERR


#define	GetIdeStatus()	inportw(IDE_CMD)
#define	SetIdeCmd(cmd)	outportw((cmd), IDE_CMD)
#define	GetIdeErr()		inportw(IDE_ERR)
#define	ReadIdeData()	inportw(IDE_DAT)
#define	IdeIsBusy()		(inportw(IDE_CMD)&0x80)
#define	WaitIdeBusy()	while(IdeIsBusy())

#define	IdeRstLow()		//rPDATB &= 0x3ff
#define	IdeRstHigh()	//rPDATB |= 0x400

static struct{
	U16 InstallWord;
	U16 Cylinders;
	U16 Heads;
	U16 nFmtBytesPerTrack;
	U16 nFmtBytesPerSector;
	U16 SectorsPerTrack;
	U16 PID[3];
	char SerString[21];
	U16 SectorBufferType;
	U16 SectorBufferPages;
	U16 LongRdWrECCBytes;
	char FirmwareVersion[9];
	char ProductName[41];
	U16 IntSecCntInMltSecRW;
	U16 DwordIoEn;
	U16 LBA_DMA;
	U16 PIOTimer;
	U16 DMATimer;
	U16 CurCylinder;
	U16 CurHead;
	U16 CurSectorsPerTrack;
	U32 TotalSectors;
	U16 IntTransSectors;
	U32 TotalSectorsLBA;
	U16 SWDMA;
	U16 MWDMA;
}HDPara;

/*****************************************************/
static void ResetIde()
{
	IdeRstLow();
	Delay(100);
	IdeRstHigh();
	
}

void IdeHardReset(void)
{
	int i;
	U16 tmp;
	
	ResetIde();
	//WaitIdeBusy();
	for(i=0; i<4; i++) {
		Delay(100);
		if(!IdeIsBusy())
			break;
	}
	if(i>=4)
		return;
	
	SetIdeCmd(0xec);
	//WaitIdeBusy();
	for(i=0; i<4; i++) {
		Delay(100);
		if(!IdeIsBusy())
			break;
	}
	if(i>=4)
		return;
		
	printf("%x,%x,%x,%x\n",
			inportw(IDE_DAT), 
			inportw(IDE_ERR), 
			inportw(IDE_SEC_CNT), 
			inportw(IDE_LBA0));
	
	printf("%x,%x,%x,%x\n",
			inportw(IDE_LBA1), 
			inportw(IDE_LBA2), 
			inportw(IDE_LBA_SEL), 
			inportw(IDE_CMD));
	
	{
		HDPara.InstallWord = ReadIdeData();		
		HDPara.Cylinders = ReadIdeData();
		tmp = ReadIdeData();
		HDPara.Heads = ReadIdeData();
		HDPara.nFmtBytesPerTrack = ReadIdeData();
		HDPara.nFmtBytesPerSector = ReadIdeData();
		HDPara.SectorsPerTrack = ReadIdeData();
		HDPara.PID[0] = ReadIdeData();
		HDPara.PID[1] = ReadIdeData();
		HDPara.PID[2] = ReadIdeData();
		for(i=0; i<10; i++)
		{
			tmp = ReadIdeData();			
			HDPara.SerString[2*i]   = tmp>>8;
			HDPara.SerString[2*i+1] = tmp;
		}
		HDPara.SectorBufferType = ReadIdeData();
		HDPara.SectorBufferPages = ReadIdeData();
		HDPara.LongRdWrECCBytes = ReadIdeData();
		for(i=0; i<4; i++)
		{
			tmp = ReadIdeData();
			HDPara.FirmwareVersion[2*i]   = tmp>>8;
			HDPara.FirmwareVersion[2*i+1] = tmp;
		}
		for(i=0; i<20; i++)	
		{
			tmp = ReadIdeData();
			HDPara.ProductName[2*i]   = tmp>>8;
			HDPara.ProductName[2*i+1] = tmp;			
		}
		HDPara.IntSecCntInMltSecRW = ReadIdeData();
		HDPara.DwordIoEn = ReadIdeData();
		HDPara.LBA_DMA = ReadIdeData();
		tmp = ReadIdeData();
		HDPara.PIOTimer = ReadIdeData();
		HDPara.DMATimer = ReadIdeData();
		tmp = ReadIdeData();
		HDPara.CurCylinder = ReadIdeData();
		HDPara.CurHead = ReadIdeData();
		HDPara.CurSectorsPerTrack = ReadIdeData();
		HDPara.CurSectorsPerTrack |= ReadIdeData()<<16;
		HDPara.TotalSectors = ReadIdeData();
		HDPara.IntTransSectors = ReadIdeData();
		HDPara.TotalSectorsLBA = ReadIdeData();
		HDPara.TotalSectorsLBA |= ReadIdeData()<<16;
		HDPara.SWDMA = ReadIdeData();
		HDPara.MWDMA = ReadIdeData();				
		
		for(i=192; i; i--)
			tmp = ReadIdeData();

		HDPara.SerString[20] = 0;
		HDPara.ProductName[40] = 0;
		HDPara.FirmwareVersion[8] = 0;			
		printf("Product name : %s\n", HDPara.ProductName);
		printf("Firmware version : %s\n", HDPara.FirmwareVersion);				
		printf("Serial string : %s\n", HDPara.SerString);
		printf("Total sectors : %d\n", HDPara.TotalSectorsLBA);		
			
	}	
		
}

#endif