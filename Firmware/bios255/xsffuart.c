/******************************************************************************
**
**  COPYRIGHT (C) 2000 Intel Corporation
**
**  FILENAME:      xsffuart.c
**
**  PURPOSE:       
**
** $ LAST MODIFIED: 12/26/2000 $
******************************************************************************/

/*
*******************************************************************************
*   HEADER FILES
*******************************************************************************
*/                                                                      

#include "systypes.h"
//#include "dm.h"
//#include "DM_Debug.h"
#include "XsClkMgr.h"
#include "XsDmaApi.h"
#include "xsuart.h"
#define FFUART_GLOBALS  1
#include "xsffuart.h"

#define	DM_WaitMs(a)
#define	DM_CwDbgPrintf(...)	

/*
*******************************************************************************
*   GLOBAL DEFINITIONS
*******************************************************************************
*/

/*
*******************************************************************************
*   LOCAL DEFINITIONS
*******************************************************************************
*/

static UartCfgT defaultFFUartCfg = 
{
	FCR_TRFIFOE,				 // Enable FIFOs
	IER_UUE,    				 // Disable DMA, no NRZ, disable IRQ, and enable UART unit
	LCR_WLS8,                    // One stop bit, no parity, 8 bits
	0,			                 // No IRDA
	UartLoopbackOff,			 // Disable loopback		
	115200,					     // baud rate 115200 
};

// DMA configuration structure to setup the transmit channel
static UartDmaCfgT defaultDmaTxCfg = 
{
	NUM_BUF_DEFAULT,
	BUFF_SIZE_DEFAULT,
	XFER_LEN_DEFAULT,	
	XSDMA_DN_MEMORY,	
	XSDMA_DN_FFUART,
	XSDMA_CH_PR_LOW	
};

// DMA configuration structure to setup the receive channel
static UartDmaCfgT defaultDmaRxCfg = 
{
	NUM_BUF_DEFAULT,		
	BUFF_SIZE_DEFAULT,
	XFER_LEN_DEFAULT,	
	XSDMA_DN_FFUART,	
	XSDMA_DN_MEMORY,
	XSDMA_CH_PR_LOW	
};

static UartDmaStatusT defaultDmaTxStatus = {0,0,0,0};
static UartDmaStatusT defaultDmaRxStatus = {0,0,0,0};

/*
******************************************************************************************
*
* FUNCTION:             XsFFUartHWSetup         
*
* DESCRIPTION:          This function is used for hardware initialization of UART.
*                       It uses the uart configuration structure for coniguring UART's
*                       modes of operation.
*
* INPUT PARAMETERS:     ctxP  is a pointer to UART's context structure
*
* RETURNS:              UINT32
*
* GLOBAL EFFECTS:       none.
*
* ASSUMPTIONS:          GPIO registers need to be programmed before this function
*                       can be called.
*                       On the Sandgate board:
*                             Board Control Register bit BCR[9] = 1 (RS232 power on)
*                             was set on early board initialization as a default. 
*                       
*******************************************************************************************
*/

static
UINT32 XsFFUartHWSetup(UartContextT * ctxP)
{   
    volatile UartRegsT * uartP = (UartRegsT *)ctxP->regsP;
    UartCfgT * cfgP = (UartCfgT *)ctxP->uartCfgP;
    UINT divisor;
    UINT value;
    UINT rate;
    
	// Select the peripheral clock bit for the UART
	xsCMEnableClock (CK_FFUART);

    // Clear all the registers
	uartP->IER = 0;	
	uartP->FCR = 0;	
	uartP->LCR = 0;	
	uartP->MCR = 0;	
	uartP->ISR = 0;
	
    // Clear the Rx FIFO 
    uartP->FCR = ctxP->uartCfgP->maskFIFO | FCR_RESETRF;

    // Clear the Tx FIFO 
    uartP->FCR = ctxP->uartCfgP->maskFIFO | FCR_RESETTF;

    // Set Serial Line Control Register (LCR) 
    // DLAB = 1
    uartP->LCR = (LCR_DLAB1 | cfgP->maskSerial);

    // Configure the baud rate generator
    // Base rate is 14.7456 MHz
    // Baud Rate = base rate/(16*divisor)
    // 230.4 kbps = divisor = 4 (min.)
    // 115.2 kbps = divisor = 8
    // 9.6   kpbs = divisor = 96
    
    // Check the default rate
    if (cfgP->rate == 0)
        rate = 115200;
    else
        rate = cfgP->rate;

    // To Configure the baud rate generators for UART:
    // DLAB = 1 to access DLL and DLH
    // Load DLL and DLH with divisor (divisor = 8 for 115200)
    // DLAB = 0
    divisor = 14745600 / (16 * rate);
    if (divisor < FFDIVISOR_MIN )
        divisor = FFDIVISOR_MIN;

    uartP->UDATA = divisor & 0xff;
	uartP->IER = (divisor & 0xff00) >> 8; 
    uartP->LCR &= ~LCR_DLAB1;

    // Set loopback
    // LOOP = 1 Test mode
    if (cfgP->loopback != 0)
        uartP->MCR |= MCR_LOOP;
    else
    {    
        uartP->MCR &= ~MCR_LOOP;
        // Read MSR once to clear delta bits (bits 3:0)
        value = uartP->MSR;
    }

    // Enable FIFOs, reset FIFOs and set interrupt triger level
    if ((cfgP->maskFIFO & FCR_TRFIFOE) != 0)
    {        
        // Enable FIFOs	and reset FIFOs and set interrupt trigger level
        uartP->FCR = cfgP->maskFIFO | FCR_TRFIFOE| FCR_RESETRF| FCR_RESETTF;
    }
    else
    {
        // non-FIFO mode
        uartP->FCR = FCR_TRFIFOD;
    }

    // Configure UART to operate via programmed I/O or DMA
    if ((cfgP->maskInt & IER_DMAE) == 0)
    { 
        // Disable DMA, configure NRZ, IRQ, and enable UART unit
        // Just make sure that DMA is disabled and UART is enabled
        uartP->IER = (cfgP->maskInt & ~IER_DMAE) | IER_UUE;
    }

    else if ((cfgP->maskFIFO & FCR_TRFIFOE) && (cfgP->maskInt & IER_DMAE))
    {    
        // Enable DMA and enable UART unit
        uartP->IER = (cfgP->maskInt | IER_UUE);
    }

    return 0;
}


/*
*******************************************************************************
*
* FUNCTION:         loopbackFFUart
*
* DESCRIPTION:      This function is used to test UART in polled mode operation.
*
* INPUT PARAMETERS: ctxP    is a pointer to UART context structure. 
*                   data    is a single character sent via loopback path.
* RETURNS:          INT   the byte of data.
*                   -1      if timeout expired and no character has been received.
*
* GLOBAL EFFECTS:   none.
*
* ASSUMPTIONS:      UART has been setup to operate in FIFO or non-FIFO polled mode
*                   and loopback test mode is enabled
*
*******************************************************************************
*/

static
INT loopbackFFUart(UartContextT * ctxP, INT data)
{
    volatile UartRegsT * uartP = (UartRegsT *)ctxP->regsP;
    INT retry = RETRY;

    while((uartP->LSR & LSR_TEMT) == 0);
	DM_WaitMs(10);

    // Write data
    uartP->UDATA = data;   
	DM_WaitMs(10);

    // Wait for the loopback data to arrive
    while (((uartP->LSR & LSR_DR) == 0) && (--retry > 0))
       DM_WaitMs(1);
            
    if (retry > 0)
        return uartP->UDATA;

    return (-1);
}

/*
*******************************************************************************
*
* FUNCTION:         writeFFUart                      
*
* DESCRIPTION:      This function is used to transmit data via UART in polled mode
*                   operation
*
* INPUT PARAMETERS: ctxP    is a pointer to UART context structure 
*                   txbufP  is a pointer to the buffer where the data is going 
*                           to be taken from
*                   len     is number of bytes to be sent
*
* RETURNS:          none.
*
* GLOBAL EFFECTS:   none.
*
* ASSUMPTIONS:      none.
*
*******************************************************************************
*/

static
void writeFFUart(UartContextT * ctxP, CHAR * txbufP, INT len)
{
    volatile UartRegsT * uartP = (UartRegsT *)ctxP->regsP;
    INT i;
  
    for (i=0; i < len; i++)
    {
        // Write data
        uartP->UDATA = *txbufP++;   

        // Wait for UART to complete transmition
        while((uartP->LSR & LSR_TEMT) == 0)
            {;}
    }
}

/*
*******************************************************************************
*
* FUNCTION:         readFFUart        
*
* DESCRIPTION:      This function is used to receive data via UART in polled mode 
*                   operation
*
* INPUT PARAMETERS: ctxP    is a pointer to UART context structure 
*                   rxbufP  is a pointer to the buffer where received data is
*                           going to be placed
*                   len     is a specified number of bytes to read.
*
* RETURNS:          INT an actual number of bytes have been read        
*                   
* GLOBAL EFFECTS:   none.
*                  
* ASSUMPTIONS:      none.
*                  
*******************************************************************************
*/                  

static
INT readFFUart(UartContextT * ctxP, CHAR * rxbufP, INT len)
{
    volatile UartRegsT * uartP = (UartRegsT *)ctxP->regsP;
    INT retry = RETRY;
    INT i;
  
    for (i=0; i < len; i++) 
    {
        // Wait for data to be available
        //while (((uartP->LSR & LSR_DR) == 0) && (--retry > 0))
        //    DM_WaitMs(1);
        while ((uartP->LSR & LSR_DR) == 0);
            
        // Read data   
        if (retry > 0)
            *rxbufP++ = uartP->UDATA;
        else
            break;
    }
    return i;
}

static		//add by hzh
INT FFUartRxRdy(UartContextT * ctxP)
{
    volatile UartRegsT * uartP = (UartRegsT *)ctxP->regsP;

    return (uartP->LSR & LSR_DR);
}

/*
*******************************************************************************
*
* FUNCTION:         clearRxFFUart
*
* DESCRIPTION:      This function is used to clear receive FIFO of the UART
*
* INPUT PARAMETERS: ctxP    is a pointer to UART context structure
*
* RETURNS:          INT     total number of bytes read
*
* GLOBAL EFFECTS:   none.
*
* ASSUMPTIONS:      none.
*
*******************************************************************************
*/

static
INT clearRxFFUart(UartContextT * ctxP)
{
    volatile UartRegsT * uartP = (UartRegsT *)ctxP->regsP;
    UINT data;
    INT total = 0;
    
    // Clear the Rx FIFO 
    uartP->FCR = ctxP->uartCfgP->maskFIFO | FCR_RESETRF;

    // Read data from FIFO until none is left
    while ((uartP->LSR & LSR_DR) != 0)
    {
        data = uartP->UDATA;
        total++;
    }
    
    return total;
}

/*
*******************************************************************************
*
* FUNCTION:         XsFFUartHWShutdown
*
* DESCRIPTION:      This function is used to shutdown the UART
*                   Tx and Rx FIFOs are cleared and and a clock is stopped. 
*
* INPUT PARAMETERS: ctxP    is a pointer to the UART's context structure
*
* RETURNS:          INT     a contence of the Interrupt Enable Register (IER)
*
* GLOBAL EFFECTS:   none.
*
* ASSUMPTIONS:      none.
*
*******************************************************************************
*/

static
INT XsFFUartHWShutdown (UartContextT * ctxP)
{
    volatile UartRegsT * uartP = (UartRegsT *)ctxP->regsP;

	// Disable the UART
	uartP->IER &= ~IER_UUE;

	// Clear the peripheral clock bit for the UART
	xsCMDisableClock (CK_FFUART);

    return uartP->IER;
}


/*
*******************************************************************************
*
* FUNCTION:         InterruptHandlerDmaTxFFUart
*
* DESCRIPTION:      Interrupt handler for DMA Tx transfer.
*					This is callback function, called by the DMA service. 
*
* INPUT PARAMETERS: PVOID param - pointer to the Uart's context structure
*					UINT32 triggeringDcsr - contains the interrupts status
*
* RETURNS:          None
*
* GLOBAL EFFECTS:   None
*
* ASSUMPTIONS:      The DMA service clears the interrupts 
*
* CALLS:            
*
* CALLED BY:        DMA interrupt service
*
* PROTOTYPE:        VOID InterruptHandlerDmaTxFFUart (PVOID param, UINT32 triggeringDcsr);
*
*******************************************************************************
*/

VOID InterruptHandlerDmaTxFFUart (PVOID param, UINT32 triggeringDcsr)
{
    UartContextT * ctxP = (UartContextT *)param;
	
	// Check for the bus error
    if (triggeringDcsr & DCSR_BUSERRINTR)
    {
		DM_CwDbgPrintf(DM_CW_UART_0, "Bus Error Interrupt \n");
		// Increment bus errorcounter
		ctxP->dmaTxIntStatusP->busErrorIntCount++;
    }

	// Check for completion of the current transmition
    if (triggeringDcsr & DCSR_ENDINTR)
    {
        // Increment end interrupt counter
    	ctxP->dmaTxIntStatusP->endIntCount++;
		if (ctxP->dmaTxIntStatusP->endIntCount >= ctxP->dmaTxCfgP->descNum)
		{
		    DM_CwDbgPrintf(DM_CW_UART_0, "End of the Tx transfer");
		    ctxP->xferTxComplete = TRUE;
		}
    }

    if (triggeringDcsr & DCSR_STARTINTR)
    {
    	// Increment start interrupt counter
		ctxP->dmaTxIntStatusP->startIntCount++;
    }
    
    if (triggeringDcsr & DCSR_STOPINTR)
    {
    	// Increment stop interrupt counter
		ctxP->dmaTxIntStatusP->stopIntCount++;
    }
}  

/*
*******************************************************************************
*
* FUNCTION:         InterruptHandlerDmaRxFFUart
*
* DESCRIPTION:      Interrupt handler for DMA Rx transfer.
*					This is callback function, called by the DMA service. 
*
* INPUT PARAMETERS: PVOID param - pointer to the Uart's context structure
*					UINT32 triggeringDcsr - contains the interrupts status
*
* RETURNS:          None
*
* GLOBAL EFFECTS:   None
*
* ASSUMPTIONS:      The DMA service clears the interrupts 
*
* CALLS:            
*
* CALLED BY:        DMA interrupt service
*
* PROTOTYPE:        VOID InterruptHandlerDmaRxFFUart (PVOID param, UINT32 triggeringDcsr);
*
*******************************************************************************
*/

VOID InterruptHandlerDmaRxFFUart (PVOID param, UINT32 triggeringDcsr)
{
    UartContextT * ctxP = (UartContextT *)param;
	
	// Check for the bus error
    if (triggeringDcsr & DCSR_BUSERRINTR)
    {
		DM_CwDbgPrintf(DM_CW_UART_0, "Bus Error Interrupt \n");
		// Increment bus errorcounter
		ctxP->dmaRxIntStatusP->busErrorIntCount++;
    }

	// Check for completion of the current transmition
    if (triggeringDcsr & DCSR_ENDINTR)
    {
   		// Increment end interrupt counter
   		ctxP->dmaRxIntStatusP->endIntCount++;
		if (ctxP->dmaRxIntStatusP->endIntCount >= ctxP->dmaRxCfgP->descNum)
		{
		    DM_CwDbgPrintf(DM_CW_UART_0, "End of the Rx transfer");
			ctxP->xferRxComplete = TRUE;
		}
    }

    if (triggeringDcsr & DCSR_STARTINTR)
    {
    	// Increment start interrupt counter
		ctxP->dmaRxIntStatusP->startIntCount++;
    }
    
    if (triggeringDcsr & DCSR_STOPINTR)
    {
    	// Increment stop interrupt counter
		ctxP->dmaRxIntStatusP->stopIntCount++;
    }
}  

/*
*******************************************************************************
*
* FUNCTION:         XsFFUartSWInit         
*
* DESCRIPTION:      This function is used to initialize FFUART's context structure. 
*                   No hardware setup is done at this point.
*
* INPUT PARAMETERS: none.
*
* RETURNS:          none.
*
* GLOBAL EFFECTS:   none.
*
* ASSUMPTIONS:      none.
*
*******************************************************************************
*/

void XsFFUartSWInit (void)
{
    // Initialize the UART
    UartContextT * ctxP = &FFUart;

    // Initialize the UART's register base
    UartRegsT * regsP = (UartRegsT *)FFUARTREG_BASE;
    ctxP->regsP = regsP;

    // Initialize the UART's setup configuration
    ctxP->uartCfgP = &defaultFFUartCfg;

    // Initialize the UART's Dma configuration
	ctxP->dmaTxCfgP = &defaultDmaTxCfg;
	ctxP->dmaRxCfgP = &defaultDmaRxCfg;

    // Initialize the UART's pointer to the Dma Status structure
	ctxP->dmaTxIntStatusP = &defaultDmaTxStatus;
	ctxP->dmaRxIntStatusP = &defaultDmaRxStatus;

    // Set the context structures functions pointers 
    ctxP->setupUartFnP = (UartSetupT)XsFFUartHWSetup;
    ctxP->loopbackUartFnP = (UartLoopbackT)loopbackFFUart;
    ctxP->writeUartFnP = (UartWriteT)writeFFUart;
    ctxP->readUartFnP = (UartReadT)readFFUart;
    ctxP->RxRdyUartFnp = (UartRxRdyT)FFUartRxRdy;
    ctxP->clearRxUartFnP = (UartClearRxT)clearRxFFUart;
    ctxP->shutdownUartFnP = (UartCleanupT)XsFFUartHWShutdown;
	ctxP->intHandlerDmaTxFnP = (UartDmaIntHandlerT)InterruptHandlerDmaTxFFUart;
	ctxP->intHandlerDmaRxFnP = (UartDmaIntHandlerT)InterruptHandlerDmaRxFFUart;

	// Load Uart ID
	ctxP->type = FFUartType;

	ctxP->loggedError = 0;	
	ctxP->dmaRxChannel = 0;	
	ctxP->dmaTxChannel = 0;	
	ctxP->xferTxComplete = 0;
	ctxP->xferRxComplete = 0;
}
