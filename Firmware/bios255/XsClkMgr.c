/******************************************************************************
**
**  COPYRIGHT (C) 2000, 2001 Intel Corporation.
**
**  This software as well as the software described in it is furnished under 
**  license and may only be used or copied in accordance with the terms of the 
**  license. The information in this file is furnished for informational use 
**  only, is subject to change without notice, and should not be construed as 
**  a commitment by Intel Corporation. Intel Corporation assumes no 
**  responsibility or liability for any errors or inaccuracies that may appear 
**  in this document or any software that may be provided in association with 
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by 
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:       XsClkMgr.c
**
**  PURPOSE:        API for the clocks manager. The clocks manager is responsible 
**                  for enabling and disabling clocks to the peripheral devices
**
**  LAST MODIFIED:  $Modtime: 2/28/01 1:53p $
******************************************************************************/

/*
*******************************************************************************
*   HEADER FILES
*******************************************************************************
*/

#include "systypes.h"
//#include "dm_errors.h"
#define CKEN_GLOBALS
#include "XsClkMgr.h"
#undef CKEN_GLOBALS


/*
*******************************************************************************
*   GLOBAL DEFINITIONS
*******************************************************************************
*/

volatile clkMgrRegsT *clkMgrRegsP = (clkMgrRegsT *)CKEN_REGISTER_BASE;

/*
*******************************************************************************
*   LOCAL DEFINITIONS
*******************************************************************************
*/

/*
*******************************************************************************
*
* FUNCTION:         XsCMEnableClock
*
* DESCRIPTION:      Enables the clock for one or more peripheral devices. Any
*                   devices that have been previously enabled will remain enabled.
*
* INPUT PARAMETERS: UINT32 - a mask describing which peripherals to enable
*
* RETURNS:          UINT32 - 0 means no error
*
* GLOBAL EFFECTS:   Enables a device that will begin to consume power. 
*                   There may be power management schemes in place.
*
* ASSUMPTIONS:      None
*
* CALLS:            None
*
* CALLED BY:        Anyone
*
* PROTOTYPE:        UINT32 xsCMEnableClock (periphClkNamesT mask);
*
*******************************************************************************
*/
UINT32 xsCMEnableClock(periphClkNamesT mask)
{
    UINT32 returnValue = 0;
    UINT32 tempMask;
    
    // preserve any previous state by ORing the enables into the register
    
    tempMask = clkMgrRegsP->CKEN;
    
    clkMgrRegsP->CKEN |= (UINT32)mask;

    // This part is just to verify that the register was changed. 

    if (clkMgrRegsP->CKEN != (clkMgrRegsP->CKEN |= (UINT32)mask))
    {
        //handle the error
        ;//LOGERROR(returnValue, ERR_L_XSCLKMGR, 0, ERR_T_NOBITSET, tempMask, clkMgrRegsP->CKEN, mask);
    }
    return returnValue;
}

/*
*******************************************************************************
*
* FUNCTION:         XsCMDisableClock
*
* DESCRIPTION:      Disables the clock for one or more peripheral devices. Any
*                   devices that have been previously enabled will remain enabled.
*
* INPUT PARAMETERS: UINT32 - a mask describing which peripherals to enable
*
* RETURNS:          UINT32 - 0 means no error
*
* GLOBAL EFFECTS:   disables a device that is consuming power. 
*
* ASSUMPTIONS:      None
*
* CALLS:            None
*
* CALLED BY:        Anyone
*
* PROTOTYPE:        UINT32 xsCMDisableClock (periphClkNamesT mask);
*
*******************************************************************************
*/
UINT32 xsCMDisableClock(periphClkNamesT mask)
{
    UINT32 returnValue = 0;
    UINT32 tempMask;
    
    // preserve any previous state by ANDing the ones compliment of the disables 
    // into the register
    
    tempMask = clkMgrRegsP->CKEN;
    
    clkMgrRegsP->CKEN &= (UINT32)~mask;

    // This part is just to verify that the register was changed. 

    if (clkMgrRegsP->CKEN != (clkMgrRegsP->CKEN &= (UINT32)~mask))
    {
        //handle the error
        ;//LOGERROR(returnValue, ERR_L_XSCLKMGR, 0, ERR_T_NOBITSET, tempMask, clkMgrRegsP->CKEN, mask);
    }
    return returnValue;
}

/*
*******************************************************************************
*
* FUNCTION:         xsCMDisableClockAll
*
* DESCRIPTION:      Disables all peripheral clocks unconditionally.
*                   This function is normally not called and would commonly be 
*                   used in some initial setup function of the system.
*
* INPUT PARAMETERS: void
*
* RETURNS:          UINT32 - 0 means no error
*
* GLOBAL EFFECTS:   Turns off the clocks of all devices
*
* ASSUMPTIONS:      None
*
* CALLS:            None
*
* CALLED BY:        Anyone
*
* PROTOTYPE:        UINT32 xsCMDisableClockAll(void);
*
*******************************************************************************
*/
UINT32 xsCMDisableClockAll(void)
{
    UINT32 returnValue = 0;
    
    clkMgrRegsP->CKEN = 0x0;
    
    // just checking for a change to the register
    
    if (clkMgrRegsP->CKEN != 0x0)
    {
        ;//LOGERROR(returnValue, ERR_L_XSCLKMGR, 0, ERR_T_NOBITSET, clkMgrRegsP->CKEN, 0, 0);
    }

    return returnValue;
}
