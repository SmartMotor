/*
 * Copyright 2005, 2006 Sony Corporation
 *
 * linuxrc.c -- initrd://linuxrc
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/mman.h>
#include <asm/unistd.h>
#include <sys/syscall.h>
#include <sys/reboot.h>  /* reboot() */
//#include <sys/mount.h>   /* mount()  */
#include <sys/wait.h>
#include <linux/unistd.h>
#include <linux/ioctl.h> /* _IO, ioctl() */
#include <linux/loop.h>  /* _IO, ioctl() */
#include <linux/fs.h>    /* BLKFLSBUF */
#include <stdarg.h>

/*
 * These should go to private header.
 */
#define STR_ERR_PROC  	"ERROR: [1]mount proc failed. err=%d\n"
#define STR_ERR_PROC2 	"ERROR: [2]mount proc failed. err=%d\n"
#define STR_ERR_CRAM  	"ERROR: mount cfamfs failed.  err=%d\n"
#define STR_ERR_NAND3 	"ERROR: mount nand3 failed.   err=%d\n"
#define STR_ERR_NAND5 	"ERROR: mount nand5 failed.   err=%d\n"
#define STR_ERR_NAND6 	"ERROR: mount nand6 failed.   err=%d\n"
#define STR_ERR_LOOP  	"ERROR: losetup failed.       err=%d\n"
#define STR_ERR_MOVE  	"ERROR: mount /usr(move)      err=%d\n"
#define STR_ERR_FILE  	"ERROR: losetup(): open file(%s)  err=%d\n"
#define ML_ERR3 		"ERROR: mount_loop(): ioctl(SET_FD) failed err=%d\n"
#define ML_ERR4 		"ERROR: mount_loop(): ioctl(STATUS) failed err=%d\n"


static int main_linuxrc(int argc, char **argv);
#define main_func main_linuxrc

static int init_stdio(void);
static int ko_insmod(char *filename, char *options);
static int my_losetup(char *loop, char *file);
static int get_free_loop(void);

#if defined(DEBUG_PROFILE)
#define perror_msg(fmt,str...) dprintf(fmt, ##str)
static void profile_init(void);
static void profile_entry(char *entry);
static void profile_exit(void);
#else /* DEBUG_PROFILE */
#define perror_msg(fmt,str...)
#define profile_init()
#define profile_entry(str) 
#define profile_exit() 
#endif /* DEBUG_PROFILE */

/* debug option */
#ifdef NOPRINTF
#define dprintf(...)
#else
#define dprintf printf
#endif

/* insmod.def include */
static const char * insmod_table[][2] = {
	#include "../../CreateFirm/bin/insmod.def1"
	{ (char *)NULL, (char *)NULL } 
};

int main(int argc, char **argv)
{
	return main_func(argc, argv);
}

typedef unsigned long cpu_set_t;
static cpu_set_t cpuset;
#define CPU_ZERO(p) *p=0
#define CPU_SET(cpu, p) *p|=(1<<(cpu))
static int sched_setaffinity(pid_t pid, unsigned int len, cpu_set_t *mask)
{
	int ret;
	ret = syscall ( __NR_sched_setaffinity, pid, len, mask );
	return ret;
}

static int sched_getaffinity(pid_t pid, unsigned int len, cpu_set_t *mask)
{
	int ret;
	ret = syscall ( __NR_sched_getaffinity, pid, len, mask );
	return ret;
}

static void set_cpu(void)
{
	int err;
	static cpu_set_t toset;

	err = sched_getaffinity(0, sizeof(cpu_set_t), &cpuset);
	dprintf("err=%d cpuset = %lx\n", err, (unsigned long)cpuset);
	CPU_ZERO(&toset);
	CPU_SET(0, &toset);
	err = sched_setaffinity(0, sizeof(cpu_set_t), &toset);
	dprintf("err=%d cpuset = %lx\n", err, (unsigned long)toset);
}

static void reset_cpu(void)
{
	int err;
	err = sched_setaffinity(0, sizeof(cpu_set_t), &cpuset);
	dprintf("err=%d cpuset = %lx\n", err, (unsigned long)cpuset);
}

static int main_linuxrc(int argc, char **argv)
{
	char lo_dev[] = "/dev/loop0";

	set_cpu();
	dprintf ( "\n" );		/* temporary for kernel/fs's printf */
	dprintf ( "/linuxrc(bin)::running on normal boot initrd...\n" );
	/*
	 * man -s4 initrd says, 
	 * to change root in linuxrc, proc must be mounted.
	 */
	dprintf ( "/linuxrc(bin)::    mounting /proc...\n" );
	if ( mount ( "none", "/proc", "proc", MS_MGC_VAL, 0 ) )
	{
		dprintf ( STR_ERR_PROC, errno );
		return -1;
	}

//	profile_init();
//	profile_entry("<<linuxrc>>");

	dprintf ( "/linuxrc(bin)::    insmod start...\n" );

#if defined(LINUXRC_UNIF)
	ko_insmod ( "/bin/unified_drv2.ko", "" );
	dprintf ( "/linuxrc(bin)::    insmod /bin/unified_drv2.ko...\n" );
#else
	ko_insmod ( "/bin/led-core.ko", "" );
	ko_insmod ( "/bin/led-plugin-gpio.ko", "" );
//	ko_insmod ( "/bin/led-plugin-drvm.ko", "" );
	ko_insmod ( "/bin/led-config-cxd4115.ko", "" );
	ko_insmod ( "/bin/led-dev.ko", "" );
	dprintf ( "/linuxrc(bin)::    insmod /bin/led-*.ko\n" );

	ko_insmod ( "/bin/nand.ko", "" );
	dprintf ( "/linuxrc(bin)::    insmod /bin/nand.ko\n" );
#endif

	dprintf("insmod LIRO\n");
	ko_insmod ( "/bin/ltron.ko", "adr=0x10100000 fn=/avsys/av.bin" );

	int ii = 0;
	while (insmod_table[ii][0] != NULL) { 
		ko_insmod ( (char *)insmod_table[ii][0], (char *)insmod_table[ii][1] ); 
		dprintf ( "/linuxrc(bin)::    insmod %s %s\n",  (char *)insmod_table[ii][0], (char *)insmod_table[ii][1]);
		ii++;
	}
//	profile_entry("insmod END");

//	profile_entry("mount nflasha3");
    dprintf ( "/linuxrc(bin)::    mountidng nflasha3 to initrd:/usr2...\n" );
	if ( mount ( "/dev/nflasha3", "/usr2", "vfat",
			     MS_SYNCHRONOUS | MS_DIRSYNC | MS_NOATIME | MS_MGC_VAL,
			     "posix_attr,shortname=mixed" ) )
	{
		dprintf ( STR_ERR_NAND3, errno );
		return -1;
	}

//	profile_entry("mount nflasha5");
    dprintf ( "/linuxrc(bin)::    mountidng nflasha5 to initrd:/avsys...\n" );
	if ( mount ( "/dev/nflasha5", "/avsys", "vfat",
			     MS_SYNCHRONOUS | MS_DIRSYNC | MS_NOATIME | MS_MGC_VAL,
			     "posix_attr,shortname=mixed" ) )
	{
		dprintf ( STR_ERR_NAND5, errno );
		return -1;
	}

//	profile_entry("mount nflasha6");
    dprintf ( "/linuxrc(bin)::    mountidng nflasha6 to initrd:/usr...\n" );
	if ( mount ( "/dev/nflasha6", "/usr", "vfat",
			     MS_SYNCHRONOUS | MS_DIRSYNC | MS_NOATIME | MS_MGC_VAL,
			     "posix_attr,shortname=mixed" ) )
	{
		dprintf ( STR_ERR_NAND6, errno );
		return -1;
	}

//	profile_entry("mount cramfs");
	dprintf ( "/linuxrc(bin)::    mounting cramfs...\n" );
/*
	 * we use fixed loop device.  So follwoings are commented out.

	if ( 0 == (lo_dev[9] = get_free_loop()) )
	{
		dprintf("cannot found free loop\n");
		return -1;
	}
*/
	if ( my_losetup ( lo_dev, "usr2/boot/rootfs.img" ) )
	{
		 dprintf ( STR_ERR_LOOP, errno );
		 return -1;
	}
	if ( mount ( lo_dev, "/mnt", "cramfs", MS_RDONLY | MS_MGC_VAL, 0 ) )
	{
		dprintf ( STR_ERR_CRAM, errno );
		return -1;
	}

//	profile_entry("pivot_root");
	dprintf ( "/linuxrc(bin)::    pivot to /mnt old goes /initrd...\n" );
	chdir ( "/mnt" );

	syscall ( __NR_pivot_root, ".", "initrd" );

	dprintf ( "/linuxrc(bin)::    changing root...\n" );

	/*
	 * To remove dependency with /initrd,
	 * some partition/dir/file are to be remounted or closed.
	 * 
	 * 1. /initrd/proc, /initrd/usr
	 * 2. /initrd/dev/console
	 * 3. other files.
	 */

	/*
	 * 1. /initrd/proc, /initrd/usr
	 */
//	profile_entry("mount proc");
	if ( mount ( "/initrd/proc", "/proc", "proc", MS_MOVE | MS_MGC_VAL, 0 ) )
	{
		dprintf ( STR_ERR_PROC2, errno );
	}

//	profile_entry("mount usr2");
	dprintf ( "/linuxrc(bin)::    moving /usr2...\n" );
	if ( mount ( "/initrd/usr2", "/usr2", "", MS_MOVE | MS_MGC_VAL, 0 ) )
	{
		dprintf ( STR_ERR_MOVE, errno );
	}

//	profile_entry("mount usr");
	dprintf ( "/linuxrc(bin)::    moving /initrd/usr to /usr/dsc/fsk...\n" );
	if ( mount ( "/initrd/usr", "/usr/dsc/fsk", "", MS_MOVE | MS_MGC_VAL, 0 ) )
	{
		dprintf ( STR_ERR_MOVE, errno );
	}

//	profile_entry("mount avsys");
	dprintf ( "/linuxrc(bin)::    moving /avsys...\n" );
	if ( mount ( "/initrd/avsys", "/avsys", "", MS_MOVE | MS_MGC_VAL, 0 ) )
	{
		dprintf ( STR_ERR_MOVE, errno );
	}


	/*
	 * 2. /initrd/dev/console
	 */
//	profile_entry("chroot");
//	profile_exit();

	chdir  ( "/" );
	chroot ( "." );

	init_stdio ();
	/*
	 * 3. other files.
	 *
	 * fd 3 and 4 are used for some reason. / or /initrd/ ? 
	 * To umount /initrd later, closing these files.
	 */
	close ( 3 );
	close ( 4 );


	/*
	 * last stage of linuxrc.
	 */

	dprintf ( "/linuxrc(bin)::    move to /sbin/init...\n" );
	char *cargv[]={
		"/sbin/init",
		NULL
	};
	char *envp[] = {
		"SHELL=/bin/ash",
		""
	};

	reset_cpu();
	execve ( cargv[0], cargv, envp );

	/* NOTREACHD */
	return 0;
}


/*
 * common sub functions
 */
static int init_stdio(void)
{
	int fd;

	close ( 0 );
	close ( 1 );
	close ( 2 );
	fd = open ( "/dev/console", O_RDWR, 0 );
	dup2 ( fd, 0 );
	dup2 ( fd, 1 );
	dup2 ( fd, 2 );
	if ( 2 < fd ) close ( fd );

	return 0;
}
static int ko_insmod(char *filename, char *options)
{
	int fd;
	int ret;
	struct stat st;
	unsigned long len;
	void *map;
	
	if ( ( fd = open ( filename, O_RDONLY, 0) ) < 0 )
	{
		dprintf ( "open module `%s' error\n", filename );
		return -1;
	}
	
	fstat ( fd, &st );
	len = st.st_size;
	map = mmap ( NULL, len, PROT_READ, MAP_SHARED, fd, 0 );
	if ( map == MAP_FAILED )
	{
		dprintf ( "mmap `%s' error\n", filename );
		return -1;
	}
	
	ret = syscall ( __NR_init_module, map, len, options );
	if ( ret != 0 )
	{
		dprintf ( "init_module `%s': errno(%d)\n", filename, errno );
		return -1;
	}
	
	close ( fd );
	return 0;
}

/*
 * my_losetup()
 *     args:
 *         loop: devnode of loopback device.
 *         file: file path to be associated.
 */
static int my_losetup(char *loop, char *file)
{
	struct loop_info li;
	int fd_file, fd_loop;


	if ( ( fd_loop = open ( loop, O_RDWR, 0 ) ) < 0 )
	{
		dprintf ( STR_ERR_FILE, loop, errno );
		return -1;
	}
	if ( ( fd_file = open ( file, O_RDWR, 0 ) ) < 0 )
	{
		dprintf ( STR_ERR_FILE, file, errno );
		return -1;
	}

	/*dprintf("my_losetup: fd_file=%d, fd_loop=%d\n", fd_file, fd_loop);*/

	if ( ioctl ( fd_loop, LOOP_SET_FD, fd_file ) < 0 )
	{
		dprintf ( ML_ERR3, errno );
		return -1;
	}
	close ( fd_file );

	memset ( &li, 0, sizeof ( li ) );
	strncpy ( li.lo_name, file, LO_NAME_SIZE );

	if ( ioctl ( fd_loop, LOOP_SET_STATUS, &li ) < 0 )
	{
		dprintf ( ML_ERR4, errno );
		ioctl ( fd_loop, LOOP_CLR_FD, 0 );		/* for safe? */
		return -1;
	}

	close ( fd_loop );

	return 0;
}

/*
 * get_free_loop()
 * return: first free loopback device number in ascii char code, or 0.
 */

static int get_free_loop(void)
{
#define NUMBER 9

	int fd_loop;
	char dev_name[] = "/dev/loop0";
	struct loop_info li;
	struct stat s;

	while ( dev_name[NUMBER] < '8' )
	{
		if ( stat ( dev_name, &s ) )
			goto next1;

		if ( !S_ISBLK ( s.st_mode ) )
			goto next1;

		if ( ( fd_loop = open ( dev_name, O_RDONLY, 0 ) ) < 0 )
			goto next2;

		if ( ( ioctl ( fd_loop, LOOP_GET_STATUS, &li ) < 0 ) &&
			 ( ENXIO == errno ) )
		{
			close ( fd_loop );
			return dev_name[NUMBER];
		}
		/* FALLTHROUGH */
	next2:
		close ( fd_loop );
	next1:
		dev_name[NUMBER]++;
	}
	return 0;

#undef NUMBER
}

#if defined(DEBUG_PROFILE)
static int profile_fd=-1;
static void profile_init(void)
{
	/*	mount("none", "/proc", "proc", 0, 0);*/
	profile_fd = open("/proc/sched_clock", O_WRONLY);
}

static void profile_entry(char *entry)
{
	if ( profile_fd < 0 ) return;
	write(profile_fd, entry, strlen(entry));
}

static void profile_exit(void)
{
	if ( !(profile_fd < 0) )
	{
		close(profile_fd);
		/*umount("/proc");*/
		profile_fd=-1;
	}
}
#endif

/* Local variables:        */
/* mode: C                 */
/* tab-width: 4            */
/* indent-tabs-mode: t     */
/* c-basic-offset: 4       */
/* End:                    */
