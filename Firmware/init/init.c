/*
 * Copyright 2005, 2006 Sony Corporation
 *
 * init.c -- initrd://linuxrc and RootFS://sbin/init
 *
 * For 2.6.23 kernel
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/vfs.h>				/* statfs() */
#include <fcntl.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/mman.h>
#include <asm/unistd.h>
#include <sys/syscall.h>
#include <sys/reboot.h>				/* reboot() */
#include <sys/mount.h>				/* mount()  */
#include <sys/wait.h>
#include <linux/unistd.h>
#include <linux/ioctl.h>			/* _IO, ioctl() */
#include <stdarg.h>
#include <termio.h>

/*
 * These should go to private header.
 */
#define STR_ERR_PENV	"ERROR: putenv %s failed. err=%d\n"
#define STR_ERR_SYSFS	"ERROR: mount sysfs failed. err=%d\n"
#define STR_ERR_PROCFS	"ERROR: mount proc failed.  err=%d\n"
#define STR_ERR_ROOTFS	"ERROR: mount root failed.  err=%d\n"
#define STR_ERR_MTD3	"ERROR: mount mtdblock3 failed. err=%d\n"
#define STR_ERR_NAND2S	"ERROR: mount nand2 statfs. err=%d\n"
#define STR_ERR_LINK 	"ERROR: symbol link failed. err=%d\n"
#define STR_ERR_MKNOD 	"ERROR: mknod %s failed. err=%d\n"
#define STR_ERR_MOVE	"ERROR: mount /usr(move)    err=%d\n"
#define STR_ERR_VAR		"ERROR: mount var(tmpfs)    err=%d\n"
#define STR_ERR_FILE	"ERROR: losetup(): open file(%s) err=%d\n"
#define STR_ERR_MKDIR	"ERROR: mkdir() file(%s) err=%d\n"
#define ML_ERR3			"ERROR: mount_loop(): ioctl(SET_FD) failed err=%d\n"
#define ML_ERR4			"ERROR: mount_loop(): ioctl(STATUS) failed err=%d\n"
#define SI_ERR5 		"ERROR: umount /initrd errno=%d\n"
#define SI_ERR6 		"ERROR: free ram disk errno=%d\n"
#define SI_ERR7 		"ERROR: date set errno=%d\n"

#define TMPFS			"tmpfs"
#define MAX_CMDLEN		256

#define RW_PARTITION_MOUNTPOINT		"/boot"
#define EXEC_RECOVER_CMD			"/sbin/dosfsck -Y /dev/nflasha2"
#define ENABLE_RECOVER_CAPACITY		(8*16*1024)
#define ENABLE_HEAP_MIN_FREE		"/proc/sys/vm/min_free_kbytes"

/* oomcatcher	*/
#if defined(OOM_256)
	#define OOM_SIZE ("65918976 -s")
#endif
#if defined(OOM_512)
	#define OOM_SIZE ("32888832 -s")
#endif

static int  main_init_bin(int argc, char **argv);

static void system2(char *cmd,...);
static void term_echo_on(void);

/* debug option */
#ifdef NOPRINTF
#define dprintf(...)
#else
#define dprintf printf
#endif


int main(int argc, char **argv)
{
	return main_init_bin ( argc, argv );
}

static int main_init_bin(int argc, char **argv)
{

	dprintf ( "/sbin/init(bin)::<START>\n" );

	if ( putenv ( "HOME=/" ) )
	{
		dprintf ( STR_ERR_PENV, "HOME", errno );
	}

	if ( putenv ( "PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin" ) )
	{
		dprintf ( STR_ERR_PENV, "PATH", errno );
	}

	if (putenv ( "LD_LIBRARY_PATH=/lib:/usr/lib:/usr/local/lib" ) )
	{
		dprintf ( STR_ERR_PENV, "LD_LIBRARY_PATH", errno );
	}

	dprintf ( "/sbin/init(bin)::  Env(HOME) = %s\n" , getenv ("HOME") );
	dprintf ( "/sbin/init(bin)::  Env(PATH) = %s\n" , getenv ("PATH") );
	dprintf ( "/sbin/init(bin)::  Env(LD_LIBRARY_PATH) = %s\n" , getenv ("LD_LIBRARY_PATH") );

	if ( mount ( "none", "/proc", "proc", 0, 0 ) )
	{
		dprintf ( STR_ERR_PROCFS, errno );
		//return -1;
	}

	dprintf ( "/sbin/init(bin):: running on RootFS...\n" );

	/*
	 * some common mount.
	 */

	dprintf ( "/sbin/init(bin)::  mounting /sys...\n" );
	if ( mount ( "none", "/sys", "sysfs", 0, "" ) )
	{
		dprintf ( STR_ERR_SYSFS, errno );
	}

	dprintf ( "/sbin/init(bin)::  mounting /var...\n" );
	if ( mount ( "none", "/var", "ramfs", 0, 0 ) )
	{
		dprintf ( STR_ERR_VAR, errno );
	}

	dprintf ( "/sbin/init(bin)::  mounting /tmp...\n" );
	if ( mount ( TMPFS, "/tmp", TMPFS, 0, 0 ) )
	{
		dprintf ( STR_ERR_VAR, errno );
	}


	/*******************************************
	 * ---- edit bellow for each product. ---- *
	 *******************************************/

	dprintf ( "/sbin/init(bin)::  mounting mtdblock3 to /user ...\n" );
	if ( mount ( "/dev/mtdblock3", "/user", "jffs2", MS_NOATIME , 0 ) )
	{             
		dprintf ( STR_ERR_MTD3, errno );
	}

	/* if ( statfs ( RW_PARTITION_MOUNTPOINT, &stat )) */
	/* { */
	/* 	dprintf ( STR_ERR_NAND2S, errno ); */
	/* } */


#if 0
	dprintf ( "/sbin/init(bin)::  mkdir /tmp/socket...\n" );
	if ( mkdir ( "/tmp/socket", 0777 ) )
	{
		dprintf ( STR_ERR_MKDIR, "/tmp/socket", errno );
	}
#endif

	// Set Time Zone ------------------

	chdir ( "/" );

	/*
	 * run application here
	 */

	chdir   ( "/user/SmartMotor" );
	
	system2 ( "./smartmotor", NULL );

	chdir   ( "/" );

	dprintf ( "/sbin/init(bin):: GO !\n" );

	/*
	 * following is dummy application
	 */

#if 0
	/*collect zombie process*/
	if (fork() > 0) {
		pid_t child;
		while (1) {
			wait(NULL);
		}
	}
#endif

	while (1)
	{
#if 0
		system ( "/bin/ash --login" );
#else
		pid_t pid;
		term_echo_on ();
		pid = fork();
		if ( 0 == pid ) 		//child
		{
			char *cargv[]={
				"/bin/ash",
				"--login",
				NULL
			};
			execv ( cargv[0], cargv );
			dprintf ( "execve failed: %s: err=%d\n", cargv[0], errno );
			exit (1); 			//on error
		}
		else if ( 0 < pid ) 	// parent
		{
			wait4 ( pid, NULL, 0, NULL );
			sleep (2);
		}
		else
		{
			sleep (2);
		}
#endif
	}
	/* if /sbin/init exits, kernel will get panic. */
	/* NOTREACHED */
	return 0;
}

static void term_echo_on(void)
{
	int ret, fd = STDOUT_FILENO;
	struct termios term;
	
	ret = tcgetattr( fd, &term );
	if ( 0 == ret )
	{
		term.c_lflag |= ECHO;
		ret = tcsetattr( fd, TCSANOW, &term );
	}
	if ( 0 != ret )
	{
		dprintf ( "tcsetattr failed: err=%d\n", errno );
	}
}

static void system2(char *cmd,...)
{
	int 	i;
	pid_t 	pid;
	char 	*list[20];
	va_list args;
	va_start ( args,cmd );

	list[0] = cmd;
	for ( i = 1; i < 19; i++ )
	{
		list[i] = va_arg ( args, char * );
		if ( list[i] == NULL ) break;
	}
	list[19] = NULL;
	va_end ( args );
	signal ( SIGCHLD, SIG_DFL );	// for ifconfig zonbe

	pid = fork();
	if ( 0 == pid ) 				//child
	{
		execvp ( cmd, list );
		dprintf ( "execvp failed: %s err=%d\n", cmd, errno );
		exit ( 1 ); 				//on error
	}
	dprintf ( "/sbin/init(bin)::  execvp Success: %s\n", cmd );
	signal ( SIGCHLD, SIG_IGN );	// for ifconfig zonbe
}


/* Local variables:        */
/* mode: C                 */
/* tab-width: 4            */
/* indent-tabs-mode: t     */
/* c-basic-offset: 4       */
/* End:                    */
